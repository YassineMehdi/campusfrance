package testing;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.hsqldb.rights.Right;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.AssertThrows;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.wcf3d.daoImpl.LanguageTDao;
import com.wcf3d.domain.Candidate;
import com.wcf3d.domain.Contact;
import com.wcf3d.domain.ContratType;
import com.wcf3d.domain.Forum;
import com.wcf3d.domain.Functionality;
import com.wcf3d.domain.LanguageT;
import com.wcf3d.domain.Pays;
import com.wcf3d.domain.Rights;
import com.wcf3d.domain.SecteurAct;
import com.wcf3d.domain.TauxActivite;
import com.wcf3d.domain.UserProfile;
import com.wcf3d.domain.WebSite;
import com.wcf3d.services.impl.CandidateServiceImpl;
import com.wcf3d.services.impl.ContactServiceImpl;
import com.wcf3d.services.impl.ContratTypeServiceImpl;
import com.wcf3d.services.impl.EnterprisePServiceImpl;
import com.wcf3d.services.impl.FunctionalityServiceImpl;
import com.wcf3d.services.impl.LanguageTServiceImpl;
import com.wcf3d.services.impl.PaysServiceImpl;
import com.wcf3d.services.impl.RightsServiceImpl;
import com.wcf3d.services.impl.SecteurActServiceImpl;
import com.wcf3d.services.impl.TauxActiviteServiceImpl;
import com.wcf3d.services.impl.UserProfileServiceImpl;
import com.wcf3d.services.impl.WebSiteServiceImpl;


@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(locations={"/applicationContext.xml"})
public class DataSetsTest extends AbstractTransactionalJUnit4SpringContextTests {
	
	@Autowired
	private LanguageTServiceImpl languageTService;	
	@Autowired
	private PaysServiceImpl paysServiceImpl;	
	@Autowired
	private ContactServiceImpl contactServiceImpl;
	@Autowired
	private ContratTypeServiceImpl contratTypeServiceImpl;
	@Autowired
	private SecteurActServiceImpl secteurActServiceImpl;
	@Autowired
	private TauxActiviteServiceImpl tauxActiviteServiceImpl;
	@Autowired
	private UserProfileServiceImpl userProfileServiceImpl;
	@Autowired
	private CandidateServiceImpl candidateServiceImpl;
	@Autowired
	private EnterprisePServiceImpl enterprisePServiceImpl;
	@Autowired
	private FunctionalityServiceImpl functionalityServiceImpl;
	@Autowired
	private RightsServiceImpl rightsServiceImpl;
	
	@SuppressWarnings("deprecation")
	@Test
	@Transactional	
    @Rollback(false)
	 public void InitDataBase() {
		
		logger.info("===============>"+languageTService.getAllLanguageT().getLanguaget().size());
		
		LanguageT languageT=new LanguageT();
		languageT.setName("French");
		languageT.setPathxml(languageT.getName()+".xml");
		languageTService.addLanguageT(languageT);
		
		LanguageT languageT2=new LanguageT();
		languageT2.setName("English");
		languageT2.setPathxml(languageT2.getName()+".xml");
		languageTService.addLanguageT(languageT2);
		
		LanguageT languageT3=new LanguageT();
		languageT3.setName("Arabic");
		languageT3.setPathxml(languageT3.getName()+".xml");
		languageTService.addLanguageT(languageT3);
		
		LanguageT languageT4=new LanguageT();		
		languageT4.setName("Spanish");
		languageT4.setPathxml(languageT4.getName()+".xml");
		languageTService.addLanguageT(languageT4);
		
		LanguageT languageT5=new LanguageT();				
		languageT5.setName("Deush");
		languageT5.setPathxml(languageT5.getName()+".xml");
		languageTService.addLanguageT(languageT5);
		
		LanguageT languageT6=new LanguageT();
		languageT6.setName("Polonish");
		languageT6.setPathxml(languageT6.getName()+".xml");
		languageTService.addLanguageT(languageT6);
		logger.info("===============>"+languageTService.getAllLanguageT().getLanguaget().size());
		
	 }
	
	@Test
	@Transactional	
    @Rollback(false)  
	public void payFactory() {  
		
	Pays p1=new Pays();
	p1.setLocaltime("GMT+1");
	p1.setName("Frence");
	p1.setZone("ZoneF");
	paysServiceImpl.addPays(p1);
	
	Pays p2=new Pays();
	p2.setLocaltime("GMT");
	p2.setName("Angleterre");
	p2.setZone("ZoneA");
	paysServiceImpl.addPays(p2);
	
	Pays p3=new Pays();
	p3.setLocaltime("GMT+1");
	p3.setName("Allemagne");
	p3.setZone("ZoneA");
	paysServiceImpl.addPays(p3);
	
	Pays p4=new Pays();
	p4.setLocaltime("GMT");
	p4.setName("Maroc");
	p4.setZone("ZoneF");
	paysServiceImpl.addPays(p4);
	
	Pays p5=new Pays();
	p5.setLocaltime("GMT+2");
	p5.setName("Suisse");
	p5.setZone("ZoneS");
	paysServiceImpl.addPays(p5);
	
	}
	
	@Test
	@Transactional	
    @Rollback(false)  
	public void paytFactory() {  
		
	Pays p1=new Pays();
	p1.setLocaltime("GMT+1");
	p1.setName("Frence");
	p1.setZone("ZoneF");
	paysServiceImpl.addPays(p1);
	
	Pays p2=new Pays();
	p2.setLocaltime("GMT");
	p2.setName("Angleterre");
	p2.setZone("ZoneA");
	paysServiceImpl.addPays(p2);
	
	Pays p3=new Pays();
	p3.setLocaltime("GMT+1");
	p3.setName("Allemagne");
	p3.setZone("ZoneA");
	paysServiceImpl.addPays(p3);
	
	Pays p4=new Pays();
	p4.setLocaltime("GMT");
	p4.setName("Maroc");
	p4.setZone("ZoneF");
	paysServiceImpl.addPays(p4);
	
	Pays p5=new Pays();
	p5.setLocaltime("GMT+2");
	p5.setName("Suisse");
	p5.setZone("ZoneS");
	paysServiceImpl.addPays(p5);
	
	}
	
	
	@SuppressWarnings("null")
	@Test
	@Transactional	
    @Rollback(false)   
	public void contactFactory() {  
		
		for(int i=0;i<9;i++)
		{
			Contact contact=new Contact();
			contact.setEmail("contact"+i+"@gmail.com");
			contact.setNom("contact"+i);
			contact.setPhone("064125"+i);
			contactServiceImpl.addContact(contact);
		}		
	}  
	
	
	@SuppressWarnings("null")
	@Test
	@Transactional	
    @Rollback(false)   
	public void contratTypeFactory() {  
		
		ContratType contratType=new ContratType();
		contratType.setDescription("Contrat � dur� d�t�rmin�e");
		contratType.setName("CDI");
		contratTypeServiceImpl.addContratType(contratType);
		
		contratType=new ContratType();
		contratType.setDescription("Contrat � dur� ind�t�rmin�e");
		contratType.setName("CDD");
		contratTypeServiceImpl.addContratType(contratType);
		
		contratType=new ContratType();
		contratType.setDescription("Contrat � en tant que rempla�ant ...");
		contratType.setName("INTERIM");
		contratTypeServiceImpl.addContratType(contratType);
	}  

	@SuppressWarnings("null")
	@Test
	@Transactional	
    @Rollback(false)   
	public void secteurActFactory() {  
		SecteurAct secteur=new SecteurAct();
		secteur.setName("Secteur financier");
		secteur.setDescription("Secteur financier");		
		secteurActServiceImpl.addSecteurAct(secteur);

		secteur=new SecteurAct();
		secteur.setName("Assurance");
		secteur.setDescription("Assurance");		
		secteurActServiceImpl.addSecteurAct(secteur);
		
		secteur=new SecteurAct();
		secteur.setName("Construction automobile");
		secteur.setDescription("Construction automobile");		
		secteurActServiceImpl.addSecteurAct(secteur);
		
		secteur=new SecteurAct();
		secteur.setName("Secteur agroalimentaire");
		secteur.setDescription("Secteur agroalimentaire");		
		secteurActServiceImpl.addSecteurAct(secteur);
		
	} 
	
	@SuppressWarnings("null")
	@Test
	@Transactional	
    @Rollback(false)   
	public void tauxActivityActFactory() {  
		TauxActivite taux=new TauxActivite();
		taux.setDescription("0");
		taux.setPourcent("50%");
		tauxActiviteServiceImpl.addTauxActivite(taux);
		taux=new TauxActivite();
		taux.setDescription("0");
		taux.setPourcent("100%");
		tauxActiviteServiceImpl.addTauxActivite(taux);
		taux=new TauxActivite();
		taux.setDescription("0");
		taux.setPourcent("75%");
		tauxActiviteServiceImpl.addTauxActivite(taux);
	} 
	
	
	@Test
    @Rollback(false)   
	public void rightsOnFunctionnalities()
	{
		Rights right=new Rights();
		right.setFunctionality(functionalityServiceImpl.getAllFunctionality().getFunctionality().get(0));
		right.setRightnumber(7);
		rightsServiceImpl.addRights(right);
	}
	
	@Test
    @Rollback(false)   
	public void functionalities()
	{
		Functionality f=new Functionality();
		f.setDescript("Description Functionnality From Spring-Test JUNIT");
		f.setName("JUNIT");
		functionalityServiceImpl.addFunctionality(f);
	}
	
	@Autowired
	private WebSiteServiceImpl websiteServiceImpl;
	
	@SuppressWarnings("null")
	@Test
	@Transactional	
    @Rollback(false)   
	public void candidateFactory() {  
		UserProfile userProfile=new UserProfile();
		userProfile.setLogin("mjanbar");
		userProfile.setPassword("123");
		//userProfile.setPays("Maroc");
		userProfile.setSecretq("language pr�f�red");
		userProfile.setSecretr("C/C++");
		userProfile.setVille("Mohammedia");
			Set<LanguageT> langs = new HashSet<LanguageT>(0);
			langs.add(languageTService.getAllLanguageT().getLanguaget().get(0));
			langs.add(languageTService.getAllLanguageT().getLanguaget().get(1));
			userProfile.setLanguageTs(langs);
			WebSite ws=new WebSite();
			ws.setIsgame(false);ws.setIssocial(true);ws.setNom("www.facebook.com");ws.setUrl("facebook/mjanbar");
			/*Set<WebSite> listWebsites=new HashSet<WebSite>(0);
			listWebsites.add(ws);*/
			websiteServiceImpl.addWebSite(ws);
			Set<WebSite> websites=new HashSet<WebSite>(0);
			websites.add(ws);
		userProfile.setWebSites(websites);
			Set<Rights> r=new HashSet<Rights>(0);
			r.add(rightsServiceImpl.getAllRights().getRights().get(0));
 		userProfile.setRightses(r);
 					/*			Creation Candidat				*/
 		Candidate candidat=new Candidate();
 			Set<ContratType> contratTypes= new HashSet<ContratType>(0);
 			contratTypes.add(contratTypeServiceImpl.getAllContratType().getContrattype().get(0));
 			contratTypes.add(contratTypeServiceImpl.getAllContratType().getContrattype().get(1));
 		candidat.setContratTypes(contratTypes);
 		candidat.setDatenaissance(new Date());
 		Set<TauxActivite> tauxs=new HashSet<TauxActivite>(0);
 		tauxs.add(tauxActiviteServiceImpl.getAllTauxActivite().getTauxactivite().get(0));
 		tauxs.add(tauxActiviteServiceImpl.getAllTauxActivite().getTauxactivite().get(1));
 		candidat.setTauxActivites(tauxs);
 		
 		//candidat.setUserProfile(userProfile);
		 
		//userProfileServiceImpl.addUserProfile(userProfile);
		candidateServiceImpl.addCandidate(candidat);
	} 
	
	
}
