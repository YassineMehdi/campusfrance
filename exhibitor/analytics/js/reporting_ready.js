function onReadyAnalytics(){
	beginDateAnalyticsHandler=$('#beginDateAnalytics');
	endDateAnalyticsHandler=$('#endDateAnalytics');
	languageAnalyticsHandler=$('#languageAnalytics');
	standReportingLiHandler=$('#standReportingLi');
	reportingSettingsLiHandler=$('#reportingSettings li');
	var	rightGroup = listStandRights[rightGroupEnum.FORUM_DASHBOARD];
	if (rightGroup != null && rightGroup.getRightCount() != 0) {
		$('#forumReportingLi').click(function(e) {
			e.preventDefault();
			reportingSettingsLiHandler.removeClass('active');
			$('#forumReportingLi').addClass('active');
			$('.reportingTabs').css('visibility', 'hidden').css('display', 'none');
			$('#forumReportingTab').css('visibility', 'visible').css('display', 'block');
		});
		$('#forumReportingLi').trigger('click');
		initForumAnalyticsView();
	} else {
		$('#forumReportingLi').remove();
		$('#forumReportingTab').remove();
	}

	var	rightGroup = listStandRights[rightGroupEnum.STAND_DASHBOARD];
	if (rightGroup != null && rightGroup.getRightCount() != 0) {
		standReportingLiHandler.click(function(e) {
			e.preventDefault();
			reportingSettingsLiHandler.removeClass('active');
			standReportingLiHandler.addClass('active');
			$('.reportingTabs').css('visibility', 'hidden').css('display', 'none');
			$('#standReportingTab').css('visibility', 'visible').css('display', 'block');
		});
		var	rightGroup = listStandRights[rightGroupEnum.FORUM_DASHBOARD];
		if (rightGroup == null || rightGroup.getRightCount() == 0)
			standReportingLiHandler.trigger('click');
		initStandAnalyticsView();
	} else {
		standReportingLiHandler.remove();
		$('#standReportingTab').remove();
	}

  var myDateBegin = new Date(2017,12,23);
  var myDateEnd = new Date();
  var minDate = new Date(2017,12,23);
  var maxDate = new Date(2018,12,23);
  beginDateAnalyticsHandler.datepicker({
        minDate : minDate,
        maxDate : maxDate,
        format: LANGUAGEDATEPICKERFORMATLABEL
    }).datepicker('setDate', myDateBegin);
  endDateAnalyticsHandler.datepicker({
        minDate : minDate,
        maxDate : maxDate,
        format: LANGUAGEDATEPICKERFORMATLABEL
    }).datepicker('setDate', myDateEnd);
  $('.datepickeranalytics').change(function() {
	    refreshAnalytics();
  });
  languageAnalyticsHandler.change(function() {
	  	languageIdAnalytics=languageAnalyticsHandler.val();
		parseXmlCandidates(candidatesListXml);
	  	refreshAnalytics();
	  	isRefleshLang=true;
  });
  isRefleshLang=true;
  refreshAnalytics();
}

