
function drawTableReportingZoneViews(rows, menuName, moduleName, typeColumn1,
		valueColumn1, typeColumn2, valueColumn2) {
	//console.log("drawTableReportingZoneViews");
	if (rows && rows.length) {
		// Create the data table.
		var data = new google.visualization.DataTable();
		data.addColumn(typeColumn1, valueColumn1);
		data.addColumn(typeColumn2, valueColumn2);
		for ( var i = 0, row; row = rows[i]; ++i) {
			data.addRow([ analyticsDateToString(row[0]), parseInt(row[1]) ]);
		}
		// Set chart options
		var options = {
			'width' : 600,
			'height' : 200
		};

    createVisitsReportingDiv(menuName, moduleName, valueColumn1);
		// Instantiate and draw our chart, passing in some options.
        data.sort([{column: 2, desc:true}, {column: 0}]);
		var table = new google.visualization.Table(document
				.getElementById(tableId));
		table.draw(data, options);

	} else {
		output.push('<p>No rows found.</p>');
	}
}

function drawTableReportingVisitsDate(rows, menuName, moduleName, typeColumn1,
		valueColumn1, typeColumn2, valueColumn2) {
	if (rows && rows.length) {
		// Create the data table.
		var data = new google.visualization.DataTable();
		data.addColumn(typeColumn1, valueColumn1);
		data.addColumn(typeColumn2, valueColumn2);
		for ( var i = 0, row; row = rows[i]; ++i) {
			//console.log(row[0]+" : "+row[1]);
			data.addRow([ analyticsDateToString(row[0]), parseInt(row[1]) ]);
		}
		// Set chart options
		var options = {
 			'width' : 400,
			'height' : 200
		};
		createVisitsStandReportingDiv(menuName, moduleName, valueColumn1);
		// Instantiate and draw our chart, passing in some options.
		var table = new google.visualization.Table(document
				.getElementById('table_div_'+menuName+'_'+moduleName+'Date'));
		table.draw(data, options);

	} else {
		output.push('<p>No rows found.</p>');
	}
}

function drawArrayTableReportingVisitsDay(rows, menuName, moduleName, typeColumn1,
		valueColumn1, typeColumn2, valueColumn2) {
	if (rows && rows.length) {
		// Create the data table.
		var data = new google.visualization.DataTable();
		data.addColumn(typeColumn1, valueColumn1);
		data.addColumn(typeColumn2, valueColumn2);
		for ( var i = 0, row; row = rows[i]; ++i) {
			data.addRow([ row[0].substring(6, 8), parseInt(row[1]) ]);
		}
		// Set chart options
		var options = {
			'width' : 600,
			'height' : 200
		};

		createVisitsStandReportingDiv(menuName, moduleName, valueColumn1);
		// Instantiate and draw our chart, passing in some options.
		var lineChart = new google.visualization.LineChart(document
				.getElementById('lineChart_div_'+menuName+'_'+moduleName+'Day'));
		lineChart.draw(data, options);
	} else {
		output.push('<p>No rows found.</p>');
	}
}

function drawChartReportingVisitorsVSReturnVisitor(rows, menuName, moduleName,
		typeColumn1, valueColumn1, typeColumn2, valueColumn2) {
	if (rows && rows.length) {
		var data = new google.visualization.DataTable();
		data.addColumn(typeColumn1, valueColumn1);
		data.addColumn(typeColumn2, valueColumn2);
		for ( var i = 0, row; row = rows[i]; ++i) {
			data.addRow([ row[0], parseInt(row[1]) ]);
		}
		// Set chart options
		var options = {
			'title' : valueColumn1,
			'sliceVisibilityThreshold' : 0,
			'width' : 600,
			'height' : 250
		};

		createNewVisitorsVSReturningVisitorsReportingDiv(menuName, moduleName, valueColumn1);
		// Instantiate and draw our chart, passing in some options.
		var pieChart = new google.visualization.PieChart(document
				.getElementById('pieChart_div_'+menuName+'_'+moduleName));
		pieChart.draw(data, options);

	} else {
		output.push('<p>No rows found.</p>');
	}
}

function drawTableVisitorsStand(rows, menuName, moduleName, moduleLabel) {
	if (rows && rows.length) {
		var data = new google.visualization.DataTable();
		data.addColumn('string', USERLABEL);
		data.addColumn('number', VISITSTOTALLABEL);
		data.addColumn('string', FIRSTVISITLABEL);
		data.addColumn('string', LASTVISITLABEL);
		data.addColumn('number', AVGVISITSLABEL);
		var dateLastLogin = null;
		var dateFirstLogin = null;
		var avgTime;
		var row1;
		var row2;
		var totalTime = 0;
		var totalLogin = 0;
		var fullName = '';
		// Put cells in table.
		for ( var i = 0; i < rows.length; ++i) {
				row1 = rows[i];
				//console.log(row1[0]+" : "+row1[1]);
			var login=row1[0].split('/')[0];	
			if(login!=''){
				row2 = rows[i + 1];
				if (totalLogin == 0) {
					/*dateFirstLogin = new Date(row1[1].substring(row1[1]
							.lastIndexOf("|") + 1));*/
					dateFirstLogin = row1[1].substring(row1[1].lastIndexOf("|") + 1);
					//console.log("row1[1] : "+dateFirstLogin);
				}
				
				totalLogin++;
				if (row2 && login == row2[0].split('/')[0]) {
					avgTime = parseFloat(row1[1].split('|')[0]);
					totalTime += avgTime;
				} else {
					/*dateLastLogin = new Date(row1[1].substring(row1[1]
							.lastIndexOf("|") + 1));*/
					dateLastLogin=row1[1].substring(row1[1].lastIndexOf("|") + 1);
					avgTime = parseFloat(row1[1].split('|')[0]);
					totalTime += avgTime;
					fullName=fullNameList[login];
					if(fullName!=null){	
						/*data.addRow([
								htmlDecode(fullName),
								totalLogin,
								dateFirstLogin.toLocaleDateString() + ' '
										+ dateFirstLogin.toLocaleTimeString(),
								dateLastLogin.toLocaleDateString() + ' '
										+ dateLastLogin.toLocaleTimeString(),
								parseFloat((totalTime / totalLogin).toFixed(2)) ]);*/
						var index = data.addRow([
										htmlDecode(fullName),
										totalLogin,
										dateFirstLogin,
										dateLastLogin,
										parseFloat((totalTime / totalLogin).toFixed(2)) ]);
						data.setProperty(index, 0, "objectId", candidateIdList[login]);
						totalTime = 0;
						totalLogin = 0;
					}
				}
			}
		}

		// Set chart options
		var options = {
			'width' : 800,
			'height' : 200
		};
		createModuleStandReportingDiv(menuName, moduleName, moduleLabel);
		data.sort([{column: 1, desc:true}, {column: 0}]);
        // Instantiate and draw our chart, passing in some options.
		var table = new google.visualization.Table(document
				.getElementById('table_div_'+menuName+'_'+moduleName));
		table.draw(data, options);
		
		function selectHandlerProfile() {
			var selectedItem=table.getSelection()[0];
			if(selectedItem!=null) {
				var objectId=data.getProperty(selectedItem.row, 0, "objectId");
				if (objectId != null && objectId != '') {
					viewUserProfile(objectId);
				}
			}
		}
		
		google.visualization.events.addListener(table, 'select', selectHandlerProfile);


	} else {
		output.push('<p>No rows found.</p>');
	}
}

function drawTableReportingFunctionalitiesZoneViews(rows, menuName, moduleName,
		typeColumn1, valueColumn1, typeColumn2, valueColumn2, typeColumn3,
		valueColumn3, typeColumn4, valueColumn4) {
	if (rows && rows.length) {
		var data = new google.visualization.DataTable();
		data.addColumn(typeColumn1, valueColumn1);
		data.addColumn(typeColumn2, valueColumn2);
		data.addColumn(typeColumn3, valueColumn3);
		for ( var i = 0, row; row = rows[i]; ++i) {
			if(row[0].split('/')[2]!=null) data.addRow([ getTranslateValue(capitalize(row[0].split('/')[2].split(':')[1])), parseInt(row[1]),
					parseInt(row[2]) ]);
		}
		var options = {
			'width' : 600,
			'height' : 200
		};

		createModuleStandReportingDiv(menuName, moduleName, valueColumn1);
        data.sort([{column: 1, desc:true}, {column: 0}]);
		// Instantiate and draw our chart, passing in some options.
		var table = new google.visualization.Table(document
				.getElementById('table_div_'+menuName+'_'+moduleName));
		table.draw(data, options);
		var k = 0;
		function handleCoreReportingResultsFunctionalitiesZone(resultsZone) {
				k++;
				var avgTime;
				var totalTime = 0;
				var totalLogin = 0;
				for ( var j = 0, rowZone; rowZone = resultsZone.rows[j]; ++j) {
					totalLogin++;
					avgTime = parseFloat(rowZone[0].split('|')[0]);
					totalTime += avgTime;
				}
				rowZone = resultsZone.rows[0];
				var row = findElement(rows, rowZone[1]);
				if (row != null
						&& (row[0].split('/')[1].split(':')[1] == MULTIMEDIA || row[0]
								.split('/')[1].split(':')[1] == RECEPTION))
					data.addRow([ row[0].split('/')[1].split(':')[1],
							parseInt(row[1]), parseInt(row[2]),
							totalTime / totalLogin ]);
				else
					data.addRow([ row[0].split('/')[1].split(':')[1],
							parseInt(row[1]), parseInt(row[2]), 0 ]);
				if (k == rows.length) {
					// Set chart options
					var options = {
						'width' : 600,
						'height' : 500
					};

					// Instantiate and draw our chart, passing in some options.
					var table = new google.visualization.Table(document
							.getElementById(tableId));
					table.draw(data, options);
				}
		}

		function findElement(arr, str) {
			for ( var l = 0, row; row = arr[l]; ++l) {
				if (row[0] == str)
					return row;
			}
			return null;
		}

	} else {
		output.push('<p>No rows found.</p>');
	}
}

function drawTableReportingMultimedia(rows, menuName, moduleName, typeColumn1,
		valueColumn1, typeColumn2, valueColumn2, typeColumn3, valueColumn3, type) {
	//console.log('drawTableReportingMultimedia(menuName : '+menuName+', moduleName :'+moduleName+', type :'+type+', length : '+rows.length+" )");
	if (rows && rows.length) {
		var objectIdNotExist=0;
		var standId=null;
		var objectId=null;
		var objectInfo=null;
		var standName=null;
		var data = new google.visualization.DataTable();
		data.addColumn(typeColumn1, valueColumn1);
		data.addColumn(typeColumn2, valueColumn2);
		if(typeColumn3 != null) data.addColumn(typeColumn3, valueColumn3);
		for ( var i = 0, row; row = rows[i]; ++i) {
			//console.log(row[0]+' : '+row[1]);
			objectInfo=row[0].split('/')[3];
			if(objectInfo!=null){
				objectId=objectInfo.split(':')[1];
				var title=getValueFromList(objectId, type);
				if(title!=null) {
					title=formattingLongWords(title, MAX_LENGTH_CHARACTER);
					if(typeColumn3 != null) {
						standId=row[0].split('/')[0].split(':')[1];
						standName=getValueFromList(standId, STAND);
						standName=formattingLongWords(standName, MAX_LENGTH_CHARACTER);
						data.addRow([title, standName, parseInt(row[1])]);
					}else {
						data.addRow([title, parseInt(row[1])]);
					}
				}else {
					objectIdNotExist++;
					//console.log("row[0] : "+row[0]+', row[1] :'+row[1]+", type "+type+", objectId "+objectId+ " not exist");
				}
			}else {
				objectIdNotExist++;
			}
		}
		if(objectIdNotExist!=rows.length){
			// Set chart options
			var options = {
				'width' : 600,
				'height' : 150,
			};
			createMultimediaReportingDiv(menuName, moduleName, valueColumn1);
			if(typeColumn3 != null) data.sort([{column: 2, desc:true}, {column: 0}]);
			else data.sort([{column: 1, desc:true}, {column: 0}]);
			// Instantiate and draw our chart, passing in some options.
			var table = new google.visualization.Table(document
					.getElementById('table_div_'+menuName+'_'+moduleName));
			table.draw(data, options);
		}

	} else {
		output.push('<p>No rows found.</p>');
	}
	$('#loadingEds').hide();
}

function drawTableReportingWebsites(rows, menuName, moduleName, typeColumn1,
		valueColumn1, typeColumn2, valueColumn2, typeColumn3, valueColumn3, typeColumn4, valueColumn4, type) {
	//console.log('drawTableReportingWebsites(moduleName : '+moduleName+', type :'+type+')');
	if (rows && rows.length) {
		var objectIdNotExist=0;
		var data = new google.visualization.DataTable();
		data.addColumn(typeColumn1, valueColumn1);
		data.addColumn(typeColumn2, valueColumn2);
		data.addColumn(typeColumn3, valueColumn3);
		if(typeColumn4!=null) data.addColumn(typeColumn4, valueColumn4);
		for ( var i = 0, row; row = rows[i]; ++i) {
			var objectId=row[0].split('/')[3].split(':')[1];
			var url=getValueFromList(objectId, type);
			//console.log("typeWebsite : "+typeWebsite+", url : "+url+", standName : "+standName);
			if(url!=null) {
				var typeWebsite=row[0].split('/')[2].split(':')[1];
				if(typeColumn4 != null) {
					var standId=row[0].split('/')[0].split(':')[1];
					var standName=getValueFromList(standId, STAND);
					standName=formattingLongWords(standName, MAX_LENGTH_CHARACTER);
					data.addRow([url, typeWebsite, standName, parseInt(row[1])]);
				}else
					data.addRow([url, typeWebsite, parseInt(row[1])]);
			}else {
				objectIdNotExist++;
				//console.log("row[0] : "+row[0]+', row[1] :'+row[1]+", objectId "+objectId1+ " not exist");
			}
		}
		if(objectIdNotExist!=rows.length){
			// Set chart options
			var options = {
				'width' : 600,
				'height' : 150
			};
	
			createMultimediaReportingDiv(menuName, moduleName, valueColumn1);
			if(typeColumn4 != null) data.sort([{column: 3, desc:true}, {column: 0}]);
			else data.sort([{column: 2, desc:true}, {column: 0}]);
			// Instantiate and draw our chart, passing in some options.
			var table = new google.visualization.Table(document
					.getElementById('table_div_'+menuName+'_'+moduleName));
			table.draw(data, options);
		}

	} else {
		output.push('<p>No rows found.</p>');
	}
}

function drawTableReportingUnsolicitedJobApplications(results, menuName, moduleName,
		title, typeColumn1, valueColumn1, typeColumn2, valueColumn2) {
	if (results) {
		var data = new google.visualization.DataTable();
		data.addColumn(typeColumn1, valueColumn1);
		data.addColumn(typeColumn2, valueColumn2);
		$(results).find("unsolicitedJobApplication").each(
				function() {
					var fullName = $(this).find('firstName').text() +' '+ $(this).find('secondName').text();
					var login=$(this).find('login').text();
					var candidatureDate=$(this).find('candidatureDate').text().split(' ')[0];
					if(fullName==' ')  fullName=login;
					var index = data.addRow([fullName, candidatureDate]);
					data.setProperty(index, 0, "objectId", candidateIdList[login]);
				});
		// Set chart options
		var options = {
			'width' : 600,
			'height' : 200
		};

		createMultimediaReportingDiv(menuName, moduleName, title);
		// Instantiate and draw our chart, passing in some options.
		var table = new google.visualization.Table(document
				.getElementById('table_div_'+menuName+'_'+moduleName));
		table.draw(data, options);

		function selectHandlerProfile() {
			var selectedItem=table.getSelection()[0];
			if(selectedItem!=null) {
				var objectId=data.getProperty(selectedItem.row, 0, "objectId");
				if (objectId != null && objectId != '') {
					viewUserProfile(objectId);
				}
			}
		}
		
		google.visualization.events.addListener(table, 'select', selectHandlerProfile);
	} else {
		output.push('<p>No rows found.</p>');
	}
}

function drawTableReportingSurveys(rows, menuName, moduleName, typeColumn1, valueColumn1,
		typeColumn2, valueColumn2, typeColumn3, valueColumn3, type) {
	//console.log('drawTableReportingSurveys(menuName : '+menuName+', moduleName : '+moduleName+', type :'+type+')');
	if (rows && rows.length) {
		var objectId;
		var objectIdNotExist=0;
		var index;
		var data = new google.visualization.DataTable();
		data.addColumn(typeColumn1, valueColumn1);
		data.addColumn(typeColumn2, valueColumn2);
		//var objectId_ = 0;
		if(typeColumn3 != null) data.addColumn(typeColumn3, valueColumn3);
		for ( var i = 0, row; row = rows[i]; ++i) {
			objectId=row[0].split('/')[3].split(':')[1];
			/*if(objectId_ == 17) objectId_ = 18;
			else objectId_ = 17;
			objectId = objectId_;*/
			var title=getValueFromList(objectId, type);
			if(title!=null) {
				title=formattingLongWords(title, MAX_LENGTH_CHARACTER);
				if(typeColumn3!=null) {
					var standId=row[0].split('/')[0].split(':')[1];
					var standName=getValueFromList(standId, STAND);
					standName=formattingLongWords(standName, MAX_LENGTH_CHARACTER);
					index = data.addRow([title, standName, parseInt(row[1])]);	
				}else index = data.addRow([title, parseInt(row[1])]);
				data.setProperty(index, 0, "objectId", objectId);
				/*console.log("row : "+r);
				r.objectId=objectId;
				console.log("r.objectId : "+r.objectId);*/				
			}else {
				objectIdNotExist++;
				//console.log("row[0] : "+row[0]+', row[1] :'+row[1]+", objectId "+objectId+ " not exist");
			}
		}
		
		// Set chart options
		var options = {
			'width' : 600,
			'height' : 200
		};
		if(objectIdNotExist!=rows.length){
			createMultimediaReportingDiv(menuName, moduleName, valueColumn1);
			if(typeColumn3 != null) data.sort([{column: 2, desc:true}, {column: 0}]);
			else data.sort([{column: 1, desc:true}, {column: 0}]);
			// Instantiate and draw our chart, passing in some options.
			var table = new google.visualization.Table(document
					.getElementById('table_div_'+menuName+'_'+moduleName));
			table.draw(data, options);
	
			function selectHandler() {
				var selectedItem=table.getSelection()[0];
				if(selectedItem!=null) {
					var objectId=data.getProperty(selectedItem.row, 0, "objectId");
					var parentSelector='#table_div_'+menuName+'_'+moduleName;
					$(parentSelector).nextAll().remove();
					if (objectId != null) {
						genericAjaxCall('GET', staticVars.urlBackEnd
								+ '/survey/get/analytics/id?formId='+objectId, 'application/xml', 'xml', '',
								function(response) {
									if ($(response).text() != '') {
										drawTablesReportingQuestion(response, menuName, moduleName+QUESTON, parentSelector, QUESTIONSLABEL, 'string',
												RESPONSELABEL, 'number', NUMBEROFCHOICES);
									}
									$('#loadingEds').hide();
								}, function(){
						});
					}
				}
			}
			
			google.visualization.events.addListener(table, 'select', selectHandler);
		}
	} else {
		output.push('<p>No rows found.</p>');
	}
}

function drawTableReportingJobOffer(results, menuName, moduleName, typeColumn1,
		valueColumn1, typeColumn2, valueColumn2, typeColumn3, valueColumn3) {
	if (results) {
		var data = new google.visualization.DataTable();
		data.addColumn(typeColumn1, valueColumn1);
		data.addColumn(typeColumn2, valueColumn2);
		if(typeColumn3!=null) data.addColumn(typeColumn3, valueColumn3);
		$(results).find("joboffer").each(function() {
				var jobOfferTitle=$(this).find('title').text();
				jobOfferTitle=formattingLongWords(jobOfferTitle, MAX_LENGTH_CHARACTER);
				var numberSubmittedCV=parseInt($(this).find('numberSubmittedCV').text());
				if(typeColumn3 != null)  {
					var standName = $(this).find('stand > name:first').text();
					standName=formattingLongWords(standName, MAX_LENGTH_CHARACTER);
					data.addRow([jobOfferTitle, standName, numberSubmittedCV]);
				}else data.addRow([jobOfferTitle, numberSubmittedCV]);
			});
		// Set chart options
		var options = {
			'width' : 600,
			'height' : 200
		};

		createMultimediaReportingDiv(menuName, moduleName, valueColumn1);
		if(typeColumn3 != null) data.sort([{column: 2, desc:true}, {column: 0}]);
		else data.sort([{column: 1, desc:true}, {column: 0}]);
		// Instantiate and draw our chart, passing in some options.
		var table = new google.visualization.Table(document
				.getElementById('table_div_'+menuName+'_'+moduleName));
		table.draw(data, options);

	} else {
		output.push('<p>No rows found.</p>');
	}
}

function drawTablesReportingQuestion(results, menuName, moduleName, parent, title, typeColumn1,
		valueColumn1, typeColumn2, valueColumn2) {
	if (results) {
		var i=0;
		var fieldResponsesMap=[];
		var response = null;
		$(results).find('attendDataDTO').each(function() {
			response = fieldResponsesMap[$(this).find("fieldDTO > fieldId").text()];
			if(response == null || response == undefined) {
				response = [];
				fieldResponsesMap[$(this).find("fieldDTO > fieldId").text()] = response;
			}
			response.push($(this).find('value').text());
		});
		$($(results).find('formFieldDTO').get().reverse()).each(
				function() {
					var responses=fieldResponsesMap[$(this).find("fieldDTO > fieldId").text()];
					var inputTypeId = $(this).find("inputTypeDTO > inputTypeId").text();
					var responsesValue = [];
					if (responses != null) {
						var questionContent=$(this).find('formFieldLabel').text();
						var responseValueList = [];
						var responseValue = null;
						if(inputTypeId == InputTypeEnum.INPUT_TEXT || inputTypeId == InputTypeEnum.TEXT_AREA){
							responsesValue = responses;
						}else {
							for(var index in responses){
								var responseValues = responses[index].split(",");
								for(var index in responseValues){
									responsesValue.push(responseValues[index]);
								}
							}
						}
						for(var index in responsesValue){
							responseValue = responsesValue[index];
							if (responseValueList[responseValue] != null)
								responseValueList[responseValue]++;
							else
								responseValueList[responseValue] = 1;
						}
						drawReportingSurvey(responseValueList, menuName, moduleName+i, parent, questionContent, typeColumn1,
							valueColumn1, typeColumn2, valueColumn2);
						i++;
					}
				});
	} else {
		output.push('<p>No rows found.</p>');
	}
}

function drawReportingSurvey(results, menuName, moduleName, parent, title, typeColumn1,
		valueColumn1, typeColumn2, valueColumn2){
	//console.log(menuName+" : "+moduleName+" : "+parent+" : "+title+" : "+typeColumn1+" : "+valueColumn1+" : "+typeColumn2+" : "+valueColumn2);
	drawTableReportingSurvey(results, menuName, moduleName, parent, title, typeColumn1,
			valueColumn1, typeColumn2, valueColumn2);
	drawChartReportingSurvey(results, menuName, moduleName, parent, title, typeColumn1,
			valueColumn1, typeColumn2, valueColumn2);
	
}

function drawTableReportingSurvey(results, menuName, moduleName, parent, title, typeColumn1,
		valueColumn1, typeColumn2, valueColumn2) {
	if (results) {
		var data = new google.visualization.DataTable();
		data.addColumn(typeColumn1, valueColumn1);
		data.addColumn(typeColumn2, valueColumn2);
		for(var index in results){
			data.addRow([ formattingLongWords(index, 15),
							parseInt(results[index]) ]);
		}
		
		// Set chart options
		var options = {
				'width' : 370,
				'height' : 300
			};
        createProfileReportingDiv(menuName, moduleName, title, parent);

		data.sort([{column: 1, desc:true}, {column: 0}]);
        // Instantiate and draw our chart, passing in some options.
        var table = new google.visualization.Table(document.getElementById('table_div_'+menuName+'_'+moduleName));
        table.draw(data, options);
	} else {
		output.push('<p>No rows found.</p>');
	}
}

function drawChartReportingSurvey(results, menuName, moduleName, parent, titleChart, typeColumn1,
		valueColumn1, typeColumn2, valueColumn2) {
	if (results) {
		var data = new google.visualization.DataTable();
		data.addColumn(typeColumn1, valueColumn1);
		data.addColumn(typeColumn2, valueColumn2);
		for(var index in results){
			data.addRow([ formattingLongWords(index, 15),
							parseInt(results[index]) ]);
		}
		// Set chart options
		var options = {
			'title' : titleChart,
			'sliceVisibilityThreshold' : 0,
			'width' : 370,
			'height' : 300
		};
        //createProfileReportingDiv(menuName, moduleName, titleChart, parent);

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('pieChart_div_'+menuName+'_'+moduleName));
        chart.draw(data, options);
	} else {
		output.push('<p>No rows found.</p>');
	}
}

function drawProfileModule(list, menuName, moduleName, moduleLabel){
	drawTableReportingProfile(list, menuName, moduleName,
			'string', moduleLabel, 'number', TOTALLABEL);
	drawChartReportingProfile(list, menuName, moduleName, 
			'string', moduleLabel, 'number', TOTALLABEL);
}

function drawTablesModules(menuName) {
	//console.log('drawTablesForum');
//	drawProfileModule(areaExpertiseList, menuName, AREA_EXPERTISE, AREA_EXPERTISE_LABEL);
//	drawProfileModule(countryOriginList, menuName, COUNTRY_ORIGIN, COUNTRY_ORIGIN_LABEL);
//	drawProfileModule(experienceYearsList, menuName, EXPERIENCEYEARS, EXPERIENCEYEARSLABEL);
	drawProfileModule(functionCandidateList, menuName, FUNCTION, FUNCTIONLABEL);
//	drawProfileModule(jobSoughtList, menuName, JOB_SOUGHT, JOB_SOUGHT_LABEL);
//	drawProfileModule(regionList, menuName, REGION, COUNTRY_RESIDENCE_LABEL);
	drawProfileModule(countryList, menuName, COUNTRY, COUNTRY_LABEL);
//	drawProfileModule(stateList, menuName, STATE, STATE_LABEL);
//	drawProfileModule(cityList, menuName, CITY, CITY_LABEL);
	drawProfileModule(sectorActList, menuName, SECTOR_ACT, ACTIVITYSECTORLABEL);
//	drawProfileModule(candidateLanguageList, menuName, LANGUAGE, LANGUAGELABEL);
	$('#loadingEds').hide();
}

function drawTablesForum() {
	drawTablesModules(PROFILFORUM);
}

function drawTablesStand(){
	//console.log('drawTablesStand');
	drawTablesModules(PROFILSTAND);
};

function drawTableReportingProfile(rows, menuName, moduleName, typeColumn1, valueColumn1, typeColumn2, valueColumn2) {
	//console.log('drawTableReportingProfile : '+valueColumn1);
	  if (rows!=null) {
	    // Create the data table.
		  	var data = new google.visualization.DataTable();
	        data.addColumn(typeColumn1, valueColumn1);
	        data.addColumn(typeColumn2, valueColumn2);
	        for(var index in rows) {
				  //console.log( 'Age : '+index + ": " + rows[index] );
				  if(index!='') data.addRow([index, rows[index]]);
			}
	        
	        // Set chart options
	        var options = {'width':400,
	                       'height':300};

	        createProfileReportingDiv(menuName, moduleName, valueColumn1);
	        data.sort([{column: 1, desc:true}, {column: 0}]);
		    // Instantiate and draw our chart, passing in some options.
	        var table = new google.visualization.Table(document.getElementById('table_div_'+menuName+'_'+moduleName));
	        table.draw(data, options);
	        if(moduleName==LANGUAGE){
	        	var i=0;
	        	for(var index in rows) {
					  var languageLevelList=candidateLanguageLevelList[index];
					  //console.log( 'moduleName : '+index +", length : "+Object.keys(languageLevelList).length);
	        		  if(index!='' && languageLevelList!=null && Object.keys(languageLevelList).length > 0) {
						  drawProfileModule(languageLevelList, menuName, moduleName+i, LANGUAGELEVELLABEL+" : "+index);
						  i++;
					  }
				}
	        }
	  } else {
		  output.push('<p>No rows found.</p>');
	  }
}

function drawChartReportingProfile(rows, menuName, moduleName, typeColumn1, valueColumn1, typeColumn2, valueColumn2) {
	if (rows!=null) {
    // Create the data table.
		var data = new google.visualization.DataTable();
        data.addColumn(typeColumn1, valueColumn1);
        data.addColumn(typeColumn2, valueColumn2);
        for(var index in rows) {
			  if(index!='') data.addRow([index, rows[index]]);
		}
        

	    data.sort([{column: 1, desc:true}, {column: 0}]);
	    // Set chart options
        var options = {'title':valueColumn1,
    				   'sliceVisibilityThreshold' : 0,
                       'width':400,
                       'height':300};

        //createProfileReportingDiv(menuName, moduleName, valueColumn1);
        data.sort([{column: 1, desc:true}, {column: 0}]);
        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('pieChart_div_'+menuName+'_'+moduleName));
        chart.draw(data, options);
  } else {
    output.push('<p>No rows found.</p>');
  }
}

function drawTableReportingGeo(rows, moduleName, typeColumn1, valueColumn1,
		typeColumn2, valueColumn2, typeColumn3, valueColumn3) {
	if (rows && rows.length) {
		// Create the data table.
		var data = new google.visualization.DataTable();
		data.addColumn(typeColumn1, valueColumn1);
		data.addColumn(typeColumn2, valueColumn2);
		data.addColumn(typeColumn3, valueColumn3);
		var totalVisits = 0;
		for ( var i = 0, row; row = rows[i]; ++i) {
			totalVisits += parseInt(row[1]);
		}
		for ( var i = 0, row; row = rows[i]; ++i) {
			data.addRow([
					row[0],
					parseInt(row[1]),
					parseFloat(((parseInt(row[1]) * 100) / totalVisits)
							.toFixed(1)) ]);
		}
		// Set chart options
		var options = {
			'width' : 400,
			'height' : 300
		};
		data.sort([ {
			column : 1,
			desc : true
		}, {
			column : 0
		} ]);
    	createDemographicsReportingDiv(moduleName, valueColumn1);
		// Instantiate and draw our chart, passing in some options.
		var table = new google.visualization.Table(document
				.getElementById('table_div_'+moduleName));
		table.draw(data, options);
	} else {
		output.push('<p>No rows found.</p>');
	}
}

function drawChartReportingGeo(rows, moduleName, typeColumn1,
		valueColumn1, typeColumn2, valueColumn2) {
	if (rows && rows.length) {
		// Create the data table.
		var data = new google.visualization.DataTable();
		data.addColumn(typeColumn1, valueColumn1);
		data.addColumn(typeColumn2, valueColumn2);
		for ( var i = 0, row; row = rows[i]; ++i) {
			data.addRow([ row[0], parseInt(row[1]) ]);
		}
		// Set chart options
		var options = {
			'title' : valueColumn1,
			'sliceVisibilityThreshold' : 0,
			'width' : 400
		};

		data.sort([ {
			column : 1,
			desc : true
		}, {
			column : 0
		} ]);
    	createDemographicsReportingDiv(moduleName, valueColumn1);
		// Instantiate and draw our chart, passing in some options.
		var chart = new google.visualization.PieChart(document
				.getElementById('chart_div_'+moduleName));
		chart.draw(data, options);
	} else {
		output.push('<p>No rows found.</p>');
	}
}

function drawTableReportingSession(rows, moduleName, typeColumn1,
		valueColumn1, typeColumn2, valueColumn2) {
	if (rows && rows.length) {
		// Create the data table.
		var data = new google.visualization.DataTable();
		data.addColumn(typeColumn1, valueColumn1);
		data.addColumn(typeColumn2, valueColumn2);
		for ( var i = 0, row; row = rows[i]; ++i) {
			data.addRow([ row[0], parseInt(row[1]) ]);
		}
		// Set chart options
		var options = {
			'width' : 400,
			'height' : 250
		};

		createOverviewReportingDiv(moduleName, valueColumn2);
		// Instantiate and draw our chart, passing in some options.
		var table = new google.visualization.Table(document
				.getElementById('table_div_'+moduleName+'Day'));
		table.draw(data, options);

		function selectHandler() {
			var selection = table.getSelection();
			var value = getSelectionValue(selection, data);
			if (value != null && value != '') {
				var dateValue = getStringDate(parseInt(value));
				makeApiCallOverviewHour(moduleName, dateValue);
			}
		}

		makeApiCallOverviewHour(moduleName, beginDateAnalytics);

		google.visualization.events.addListener(table, 'select', selectHandler);

	} else {
		output.push('<p>No rows found.</p>');
	}
}

function drawTableReportingAvgTimeSite(rows, moduleName, typeColumn1,
		valueColumn1, typeColumn2, valueColumn2) {
	if (rows && rows.length) {
		// Create the data table.
		var data = new google.visualization.DataTable();
		data.addColumn(typeColumn1, valueColumn1);
		data.addColumn(typeColumn2, valueColumn2);
		for ( var i = 0, row; row = rows[i]; ++i) {
			data.addRow([ row[0],
					parseFloat((parseFloat(row[1]) / 60).toFixed(1)) ]);
		}
		// Set chart options
		var options = {
			'width' : 400,
			'height' : 250
		};

		createOverviewReportingDiv(moduleName, valueColumn2);
		// Instantiate and draw our chart, passing in some options.
		var table = new google.visualization.Table(document
				.getElementById('table_div_'+moduleName+'Day'));
		table.draw(data, options);

		function selectHandler() {
			var selection = table.getSelection();
			var value = getSelectionValue(selection, data);

			if (value != null && value != '') {
				var dateValue = getStringDate(parseInt(value));
				makeApiCallAvgTimeOnSiteHour(0, dateValue);
			}
		}
		
		makeApiCallAvgTimeOnSiteHour(0, beginDateAnalytics);

		google.visualization.events.addListener(table, 'select', selectHandler);

	} else {
		output.push('<p>No rows found.</p>');
	}
}

function drawArrayTableReportingSession(rows, moduleName, typeColumn1,
		valueColumn1, typeColumn2, valueColumn2) {
	if (rows && rows.length) {
		// Create the data table.
		var data = new google.visualization.DataTable();
		data.addColumn(typeColumn1, valueColumn1);
		data.addColumn(typeColumn2, valueColumn2);
		for ( var i = 0, row; row = rows[i]; ++i) {
			data.addRow([ row[0], parseInt(row[1]) ]);
		}
		// Set chart options
		var options = {
			'width' : 600,
			'height' : 250
		};

		// Instantiate and draw our chart, passing in some options.
		var lineChart = new google.visualization.LineChart(document
				.getElementById('lineChart_div_'+moduleName+'Hour'));
		lineChart.draw(data, options);
	} else {
		output.push('<p>No rows found.</p>');
	}
}

function drawArrayTableReportingAvgTimeOnSiteHour(rows, moduleName,
		typeColumn1, valueColumn1, typeColumn2, valueColumn2) {
	if (rows && rows.length) {
		// Create the data table.
		var data = new google.visualization.DataTable();
		data.addColumn(typeColumn1, valueColumn1);
		data.addColumn(typeColumn2, valueColumn2);
		for ( var i = 0, row; row = rows[i]; ++i) {
			data.addRow([ row[0], (parseFloat(row[1]) / 60) ]);
		}
		// Set chart options
		var options = {
			'width' : 600,
			'height' : 250
		};

		// Instantiate and draw our chart, passing in some options.
		var lineChart = new google.visualization.LineChart(document
				.getElementById('lineChart_div_'+moduleName+'Hour'));
		lineChart.draw(data, options);
	} else {
		output.push('<p>No rows found.</p>');
	}
}

function drawTableReportingTrafficSource(rows, moduleName, moduleLabel, typeColumn1,
		valueColumn1, typeColumn2, valueColumn2, typeColumn3, valueColumn3) {
	if (rows && rows.length) {
		// Create the data table.
		var data = new google.visualization.DataTable();
		data.addColumn(typeColumn1, valueColumn1);
		data.addColumn(typeColumn2, valueColumn2);
		data.addColumn(typeColumn3, valueColumn3);
		for ( var i = 0, row; row = rows[i]; ++i) {
			data.addRow([ row[0], parseInt(row[1]),
					parseFloat((parseFloat(row[2]) / 60).toFixed(1)) ]);
		}
		// Set chart options
		var options = {
			'width' : 400,
			'height' : 250
		};

		data.sort([ {
			column : 1,
			desc : true
		}, {
			column : 0
		} ]);
		createTrafficSourcesReportingDiv(moduleName, moduleLabel);
		// Instantiate and draw our chart, passing in some options.
		var table = new google.visualization.Table(document
				.getElementById('table_div_'+moduleName));
		table.draw(data, options);
	} else {
		output.push('<p>No rows found.</p>');
	}
}




