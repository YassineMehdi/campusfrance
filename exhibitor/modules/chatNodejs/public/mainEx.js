// Initialize variables
var $window = $(window);
var $usernameInput = $('.user_email'); // Input for username
var $messages = $('.messages'); // Messages area
var $inputMessage = $('#message-to-send'); // Input message input box

var $loginPage = $('.login.page'); // The login page
var $chatPage = $('.chat.page'); // The chatroom page
var $userSender = $('.listUser');
// Prompt for setting a username
var username;
var connected = false;
var typing = false;
var lastTypingTime;
var $currentInput = $usernameInput.focus();
var blink_flag;
var usermsg = [];
var historicMsg = [];
var i = 0;
var blinkid = {};
var discon = false;


function historic(id) {
    $('#loadingEds').css('display', 'block');
    console.log("historic service")
    console.log(id)
    $.ajax({
        url: 'http://52.213.163.11:3000/historic/' + id,
        dataType: "",

        cache: false,
        timeout: 12000,
        success: function(data) {
            console.log(data)
            historicMsg = data;
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert('error ' + textStatus + " " + errorThrown);
        }
    });
    $('head').append($('<link rel="stylesheet" type="text/css" />').attr('href', 'modules/chatNodejs/public/style.css'));
    setTimeout(function() {
        dressUserHistoric()
    }, 10000);
    setTimeout(function() {
        $('#loadingEds').css('display', 'none');
    }, 15000);
}

function addUserHistoric(user) {

    $('.historic .list').find('#' + user.id).remove();
    console.log($('.list'))
    $('.historic .list').append('<li id="' + user.id + '" class="clearfix">' +
        '<img src="' + user.avatar + '" alt="avatar" />' +
        '<div class="about">' +
        '<div class="name">' + user.username + '</div>' +
        '<div class="status">' +
        '<i class="fa fa-circle online"></i> online' +
        '</div>' +
        '</div>' +
        '</li>');
    if ($('.historic #' + user.id + 'room').length == 0)
        $('.historic .chat').append('<div id="' + user.id + 'room" class="room hide">' +
            '<div class="chat-header clearfix">' +
            '<img src="' + user.avatar + '" alt="avatar" />'

            +
            '<div class="chat-about">' +
            '<div class="chat-with">Chat avec ' + user.username + '</div>' +
            '<div class="chat-num-messages"></div>' +
            '</div>' +

            '</div>'

            +
            '<div class="chat-history">' +
            '<ul>' +
            '</ul>'

            +
            '</div>' +

            '</div>')


    listenForClick();
    sendMessage();
}

function dressUserHistoric() {
    var i = 0;
    $('.historic .chat-history ul').empty();
    /*  console.log(historicMsg.length / 20)
     for (i = 1; i < historicMsg.length / 100; i = i + 1) {
         $('.historicChat.chatexihitor').append('<button>' + i + '</button>');
     }
     i = 0; */
    for (i in historicMsg) {
        console.log("login historic")
        console.log(historicMsg[i].user.login)
        if (historicMsg[i].user.login === undefined) {
            console.log("user undefined" + i)
            console.log(historicMsg[i])

            addUserHistoric(historicMsg[i].user)

            ReceiveMsgDraw(historicMsg[i], "historic")
        } else {
            console.log("user defined" + i)
            console.log(historicMsg[i])

            addUserHistoric(historicMsg[i].send)

            SendmessageDrw(historicMsg[i].send.id, historicMsg[i].msg, historicMsg[i].date, "historic")
        }
    }
}

function listenForClick() {
    console.log()
    $('.chatNew .list li').on('click', function() {

        /* clearInterval(blink_flag);*/
        $('.chatNew .chat .room').addClass('hide');
        var id = $(this).attr('id')
        $('.chatNew #' + id + 'room').removeClass('hide');
        /*         $(".chatNew .chat-history").animate({ scrollTop: $(".chatNew .chat-history")[0].scrollHeight }, 1000);
         */
        //clearInterval(blinkid.id);
        //$("#" + id).removeClass('blink');
        $('#' + id).removeClass("blink_me");

    })

    $('.historic .list li').unbind('click').bind('click', function() {
        /* clearInterval(blink_flag);*/
        console.log("click li ")

        var t0 = performance.now();
        $('.historic .chat .room').addClass('hide');
        var t1 = performance.now();
        console.log("Call to doSomething took " + (t1 - t0) + " milliseconds.")
        var id = $(this).attr('id')
        $('.historic #' + id + 'room').removeClass('hide');

        //clearInterval(blinkid.id);
        //$("#" + id).removeClass('blink');
        // $('#' + id).removeClass("blink_me");

    })
}
var enterpriseP;
this.getEnterprisePInfoSuccess = function(data) {
    enterpriseP = new $.EnterpriseP($(data));
    console.log(enterpriseP)
    if (enterpriseP.getIsAdmin()) {
        //window.location.replace("backend.html?idLang=" + getIdLangParam());
    }
    //that.notifyLoadEnterprisePInfo(enterpriseP);
}

this.getEnterprisePInfoError = function(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}
this.getEnterprisePInfo = function() {
    genericAjaxCall('GET', staticVars.urlBackEnd + 'enterprisep/getInfos', 'application/xml', 'xml',
        '', this.getEnterprisePInfoSuccess, this.getEnterprisePInfoError);
}

//var socket = io();
function disconnect() {
    socket.disconnect();
    discon = true;
}
getEnterprisePInfo()

function connexion() {

    getEnterprisePInfo();
    /*  $('.privateroom').on('click', function() { */
    setTimeout(function() {
        console.log(enterpriseP)
        connected = true;
        var userE = {};
        userE.avatar = enterpriseP.userProfile.avatar;
        userE.username = enterpriseP.userProfile.firstName + " " + enterpriseP.userProfile.secondName;
        userE.login = enterpriseP.login;
        socket.emit('connexionStand', { userE: userE });
    }, 1000);


    console.log(JSON.parse(window.localStorage.getItem('userMsg')))
    if (window.localStorage.getItem('userMsg') === null)
        usermsg = [];
    else
    /*if (usermsg[i].receive  ) {
                    console.log(usermsg[i])
                       if(usermsg[i].id == id){*/

        usermsg = JSON.parse(window.localStorage.getItem('userMsg'));

    /* }) */
}

function SendmessageDrw(id, msg, date, parent) {
    $('.' + parent + ' #' + id + 'room .chat-history ul').append('<li class="clearfix">' +
        '<div class="message-data align-right">' +
        '<span class="message-data-time">' + date + '</span> &nbsp; &nbsp;' +
        '<span class="message-data-name">Moi</span> <i class="fa fa-circle me"></i>'

        +
        '</div>' +
        '<div class="message other-message float-right">' + msg + '</div>' +
        '</li>')


    $('#' + id + 'room .messageValue').val('');

    //$(".chatexihitor .chat-history").animate({ scrollTop: $(".chatexihitor .chat-history")[0].scrollHeight}, 1000);

}

function sendMessage() {

    $('.sendMsg').unbind('click').bind('click', function() {
        var idroom = $(this).parent().parent().attr('id');
        var id = idroom.substring(0, idroom.length - 4);
        if ($('#' + idroom + ' .messageValue').val() != '') {
            var userSendMsg = {};
            var ladate = new Date();
            userSendMsg.msg = $('#' + idroom + ' .messageValue').val();
            userSendMsg.id = id;
            userSendMsg.receive = false;
            userSendMsg.date = ladate.getHours() - 1 + ":" + ladate.getMinutes();

            usermsg.push(userSendMsg);
            console.log($(' .messageValue').val())
            socket.emit('privatemsgToVisitor', { id: id, msg: $('#' + idroom + ' .messageValue').val() });

            console.log($('#' + id + ' .chat-history ul'))
            SendmessageDrw(id, userSendMsg.msg, userSendMsg.date, 'chatNew')
            var val = JSON.stringify(usermsg);
            console.log(val);
            window.localStorage.setItem('userMsg', val);
            console.log(JSON.parse(window.localStorage.getItem('userMsg')))
        }

    })
}
socket.on('users', function(data) {
        console.log(data.users)
        $('.list').empty();
        setTimeout(function() {
            for (i = 0; i < data.users.length; i++) {
                console.log(data.users[i])
                addUserConnexion(data.users[i]);

            }
            listenForClick();
            sendMessage()
        }, 1000);

    })
    /*socket.on('usersVisitor', function(data) {
        console.log(data);
        addUserConnexion(data.user);
    })*/
socket.on('userVisitor', function(data) {
    console.log(data);
    addUserConnexion(data.userV);

})

function addUserConnexion(user) {


    i = i + 1;

    $('.chatNew .list').append('<li id="' + user.id + '" class="clearfix">' +
        '<img src="' + user.avatar + '" alt="avatar" />' +
        '<div class="about">' +
        '<div class="name">' + user.username + '</div>' +
        '<div class="status">' +
        '<i class="fa fa-circle online"></i> online' +
        '</div>' +
        '</div>' +
        '</li>');
    $('.chatNew .chat').append('<div id="' + user.id + 'room" class="room hide">' +
        '<div class="chat-header clearfix">' +
        '<img src="' + user.avatar + '" alt="avatar" />'

        +
        '<div class="chat-about">' +
        '<div class="chat-with">Chat avec ' + user.username + '</div>' +
        '<div class="chat-num-messages"></div>' +
        '</div>' +

        '</div>'

        +
        '<div class="chat-history">' +
        '<ul>' +
        '</ul>'

        +
        '</div>' +
        '<div class="chat-message clearfix">' +
        '<textarea name="message-to-send"  class="messageValue" placeholder="Type your message" rows="3"></textarea>'


        +
        '<button class="sendMsg">Envoyer</button>'


        +
        '</div>' +
        '</div>')
    setTimeout(function() {
        if ($('.chatNew #' + user.id + 'room .chat-history ul').is(':empty')) {
            drawMessageRoomOne(user.id);
            console.log('emptyyyyyyyyy')
        }

    }, 1000);


    listenForClick();
    sendMessage();
}

function ReceiveMsgDraw(user, parent) {
    $('.' + parent + ' #' + user.user.id + 'room .chat-history ul').append(' <li>' +
        '<div class="message-data">' +
        '<span class="message-data-name"><i class="fa fa-circle online"></i> ' + user.user.username + '</span>' +
        '<span class="message-data-time">' + user.date + '</span>' +
        '</div>' +
        '<div class="message my-message">' +
        user.msg +
        '</div>' +
        '</li>')

    // $(".chatexihitor .chat-history").animate({ scrollTop: $(".chatexihitor .chat-history")[0].scrollHeight}, 1000);

}

function blink(idUser) {
    //clearInterval(id);
    //console.log(id)
    /*  id = setInterval(function() {
         $('#' + idUser).toggleClass("blink");
     }, 1000); */
    if ($('#' + idUser + 'room').hasClass('hide'))
        $('#' + idUser).addClass("blink_me");
}
socket.on('new_msg', function(data) {
    console.log(data)
    data.userV.receive = true;
    usermsg.push(data.userV);
    /*blink_flag = setInterval(function() {
        $('#' + data.userV.user.id).toggleClass("blink");
    }, 1000);*/
    $('#' + data.userV.user.id).prependTo('.list')


    ReceiveMsgDraw(data.userV, 'chatNew')
    var val = JSON.stringify(usermsg);
    console.log(val);
    window.localStorage.setItem('userMsg', val);
    console.log(JSON.parse(window.localStorage.getItem('userMsg')))

    //blinkid[data.userV.user.id] = "d" + i;
    blink(data.userV.user.id);


})

function drawMessageRoom() {
    //window.localStorage.setItem('usermsg', usermsg);
    console.log(usermsg)
    if (usermsg != null) {
        console.log(usermsg)
        for (i in usermsg) {
            console.log(i)
            console.log(usermsg[i]);
            if (usermsg[i].receive) {
                console.log(usermsg[i]);
                ReceiveMsgDraw(usermsg[i], 'chatNew')
            } else {
                console.log(usermsg[i]);
                SendmessageDrw(usermsg[i].id, usermsg[i].msg, usermsg[i].date, 'chatNew')
            }

        }


    }
}


function drawMessageRoomOne(id) {
    //window.localStorage.setItem('usermsg', usermsg);
    console.log(usermsg);
    console.log(usermsg.length)
    for (i in usermsg) {

        console.log(i)
        console.log(usermsg[i]);
        if (usermsg[i].receive) {
            if (usermsg[i].id == id) {
                console.log(usermsg[i]);
                ReceiveMsgDraw(usermsg[i], 'chatNew')
            }

        } else
        if (usermsg[i].user.id == id) {
            console.log(usermsg[i]);
            SendmessageDrw(usermsg[i].id, usermsg[i].msg, usermsg[i].date, 'chatNew')
        }

    }
}


// Whenever the server emits 'stand left',
socket.on('userDisconnect', function(data) {
    console.log(data)
    $('.chatNew #' + data.userId.id).remove();
    $('.chatNew #' + data.userId.id + 'room').remove();

})


socket.on('disconnect', function() {

    console.log("deconnecxion")

    var val = JSON.stringify(usermsg);
    console.log(val);
    window.localStorage.setItem('userMsg', val);
    console.log(JSON.parse(window.localStorage.getItem('userMsg')))
        /* if( !discon && $('#chatLi').hasClass('active')){
              $.getScript("modules/chatNodejs/public/socket.io.js");
                       $.getScript("modules/chatNodejs/public/socket.js");
             connexion()
         }*/

    //log('you have been disconnected');
});

socket.on('reconnect', function() {
    console.log("reconnection")
    var val = JSON.stringify(usermsg);
    console.log(val);
    /* window.localStorage.setItem('userMsg', val);
    console.log(usermsg = JSON.parse(window.localStorage.getItem('userMsg'))) */
    /* log('you have been reconnected');
     if (username) {
         socket.emit('add user', username);
     }*/
});

socket.on('reconnect_error', function() {
    log('attempt to reconnect has failed');
});