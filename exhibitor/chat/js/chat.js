var dateBeginHistoryChatHandler = null;
var dateEndHistoryChatHandler = null;
var typeHistoryChatHandler = null;
var languageHistoryChatHandler = null;
var myVarInter = null;
$(document).ready(function() {
    initChat();
    myVarInter = setInterval(
        function() {
            if (isDatepickerLoad) {
                clearInterval(myVarInter);
                initHistory();
            }
        }, 1000); // pour s'assurer que datepicker soit chargé.
});

function initChat() {
    $('.closePopup').click(function(e) {
        e.preventDefault();
        $('.eds-dialog-container').hide();
        $('.backgroundPopup').hide();
    });

    $('#chatSettings a.chat').click(function(e) {
        e.preventDefault();
        if (!isSettingsFilled()) {
            $('.backgroundPopup').show();
            $('.fillSettingsPopup').show();
        } else {
            if (!isConnectToChat) $('#chatComponent').empty();
            factoryChangeChatTab('#chatLi', '#chatTab');
        }
    });

    $('#chatSettings a.settings').click(function(e) {
        e.preventDefault();
        factoryChangeChatTab('#settingsLi', '#settingsTab');
    });

    $('#chatSettings a.moderationChat').click(function(e) {
        e.preventDefault();
        factoryChangeChatTab('#moderationChatLi', '#moderationChatTab');
        $('#loadLogoRecruiterError').hide();
    });

    $('#chatSettings a.historicChat').click(function(e) {
        e.preventDefault();
        factoryChangeChatTab('#historicChatLi', '#historicChatTab');
        getHistoryChat();
    });
    $('#progressLogoRecruiterUpload,#loadLogoRecruiterError').hide();
    $('#logoRecruiterUpload').fileupload({
        beforeSend: function(jqXHR, settings) {
            beforeUploadFile('.saveChatSettingsButton', '#progressLogoRecruiterUpload', '#chatPage .filebutton,#loadLogoRecruiterError');
        },
        datatype: 'json',
        cache: false,
        add: function(e, data) {
            var goUpload = true;
            var uploadFile = data.files[0];
            var fileName = uploadFile.name;
            timeUploadLogoRecruiter = (new Date()).getTime();
            data.url = getUrlPostUploadFile(timeUploadLogoRecruiter, fileName);
            if (!(/\.(gif|jpg|jpeg|tiff|png)$/i).test(fileName)) {
                goUpload = false;
            }
            if (uploadFile.size > MAXFILESIZE) { // 6mb
                goUpload = false;
            }
            if (goUpload == true) {
                data.submit();
            } else $('#loadLogoRecruiterError').show();
        },
        progressall: function(e, data) {
            progressBarUploadFile('#progressLogoRecruiterUpload', data, 98);
        },
        done: function(e, data) {
            var file = data.files[0];
            var fileName = file.name;
            var hashFileName = getUploadFileName(timeUploadLogoRecruiter, fileName);
            console.log("Add logo recruiter chat done, filename :" + fileName + ", hashFileName : " + hashFileName);
            enterprisepPhoto = getUploadFileUrl(hashFileName);
            $('#recruiterPicture').attr('src', enterprisepPhoto);
            afterUploadFile('.saveChatSettingsButton', '#chatPage .filebutton', '#progressLogoRecruiterUpload');
        }
    });

    //validateAccountForm();
    getRecruiterInfosChat();
    //getRecruiterLanguages();

    $('.saveChatSettingsButton').click(function() {
        if ($("#accountForm").valid() && !isDisabled('.saveChatSettingsButton')) {
            sendRecruiterInfosChat();
        }
    });
    $('#historicChat').on('click', function() {
        console.log("historiccccc btn")

        historic(currentEnterpriseP.login)
    })

    this.launchChatNodeJs = function() {
        console.log("nodejssssss")
            // disconnect();
        $('#chatComponent').empty();
        $('#chatComponent').append('<div id="chatFlashContent"></div>');

        /*  $('head').append($('<script> </script>').attr('src', 'modules/chatNodejs/public/socket.io.js'));
        $('head').append($('<script> </script>').attr('src', 'modules/chatNodejs/public/socket.js'));
        $('head').append($('<script> </script>').attr('src', 'modules/chatNodejs/public/mainEx.js'));

 */
        // $.getScript("modules/chatNodejs/public/socket.js");

        connexion();


        $('#chatFlashContent').load('modules/chatNodejs/public/exihbitor.html', function() {
            // initChatHtml5();
            //initAthmosphereListener();
            //$('.chatPublicLoading').css('display', 'none');
            // chatTabs = $('#tabs1').scrollTabs();
            // $('#tchatG .scroll-pane').jScrollPane({ autoReinitialise: true });

            $('head').append($('<link rel="stylesheet" type="text/css" />').attr('href', 'modules/chatNodejs/public/style.css'));
            setTimeout(function() {
                console.log("ddddddddddddddddddd")
                drawMessageRoom();
            }, 4000);
            /*  $.getScript("modules/chatNodejs/public/socket.io.js");
             $.getScript("modules/chatNodejs/public/socket.js");
             $.getScript("modules/chatNodejs/public/mainEx.js"); */
        });
    };
    $('#connectToChatButton').click(function() {
        /*         if (!FlashDetect.installed){
        			var pageHost = ((document.location.protocol == "https:") ? "https://" : "http://"); 
        			$('#chatComponent').html("<a href='http://www.adobe.com/go/getflashplayer' target='_blank'><img src='" 
        		    + pageHost + "www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Télécharger Adobe Flash player' /></a>" );
        		}else {
        			if (!isConnectToChat) {
        			    getCurrentsChat();
        			    //connectToChatFlash();
        			} else {
        			    isConnectToChat = false;
        			    tChatFlashLoaded = false;
        			    $('#connectToChatButton').empty();
        			    $('#connectToChatButton').append('<i class="fui-chat"></i>'+CONNECTTOCHATLABEL+'');
        			    $('#connectToChatButton').removeClass("disconnected");
        			    $('#chatComponent').empty();
        			}
        		}

             */
        launchChatNodeJs();
    });
}

function initLanguageComboBoxHistory(languageHistoryChatHandler) {
    languageHistoryChatHandler.empty();
    languageHistoryChatHandler.append($('<option/>').val(ALL).html(ALLLABEL));
    for (var languageId in listLangsEnum) {
        languageHistoryChatHandler.append($('<option/>').val(languageId).html(getLanguageName(languageId)));
    }
}

function initHistory() {
    dateBeginHistoryChatHandler = $('#dateBeginHistoryChat');
    dateEndHistoryChatHandler = $('#dateEndHistoryChat');
    typeHistoryChatHandler = $('#typeHistoryChat');
    historySendButtonHandler = $('#historySendButton');
    //languageHistoryChatHandler=$('#languageHistoryChat');
    //initLanguageComboBoxHistory(languageHistoryChatHandler);
    var myDateBegin = new Date(2016, 12, 23);
    var myDateEnd = new Date();
    var minDate = new Date(2016, 12, 23);
    var maxDate = new Date(2017, 12, 23);
    dateBeginHistoryChatHandler.datepicker({
        minDate: minDate,
        maxDate: maxDate,
        dateFormat: LANGUAGEDATEPICKERFORMATLABEL
    }).datepicker('setDate', myDateBegin);
    dateEndHistoryChatHandler.datepicker({
        minDate: minDate,
        maxDate: maxDate,
        dateFormat: LANGUAGEDATEPICKERFORMATLABEL
    }).datepicker('setDate', myDateEnd);

    historySendButtonHandler.click(refreshHistoryChat);
    /*dateBeginHistoryChatHandler.change(refreshHistoryChat);
    dateEndHistoryChatHandler.change(refreshHistoryChat);
    typeHistoryChatHandler.change(refreshHistoryChat);
    languageHistoryChatHandler.change(refreshHistoryChat);*/
    //getHistoryChat();
}

function parseChatPublicHistoryListXml(xml) {
    $(xml).find("chatPublicHistory").each(function() {
        var historicChat = new Object();
        historicChat.id = $(this).find('chatPublicHistoryId').text();
        historicChat.title = $(this).find('eventTitle').text();
        historicChat.date = $(this).find('date').text();
        historicChat.path = $(this).find('path').text();
        historicChat.languageId = $(this).find('language > idlangage').text();
        historicChat.type = PUBLIC;
        listHistoricChatCache.push(historicChat);
    });
    refreshHistoryChat();
}

function parseChatPrivateHistoryListXml(xml) {
    $(xml).find("chatPrivateHistory").each(function() {
        var historicChat = new Object();
        historicChat.id = $(this).find('chatPrivateHistoryId').text();
        historicChat.title = $(this).find('userProfileDTO2').find('prenom').text() + " " + $(this).find('userProfileDTO2').find('nom').text();
        historicChat.date = $(this).find('date').text();
        historicChat.path = $(this).find('path').text();
        historicChat.type = PRIVATE;
        listHistoricChatCache.push(historicChat);
    });
    refreshHistoryChat();
}

function getHistoryChat() {
    var dateBeginHistoryChatValue = dateBeginHistoryChatHandler.val();
    var dateEndHistoryChatValue = dateEndHistoryChatHandler.val();

    $("#historyListTable").empty();
    listHistoricChatCache = new Array();
    genericAjaxCall('POST', staticVars["urlBackEnd"] + "chatpublichistory/listchat?dateBegin=" + dateBeginHistoryChatValue + '&dateEnd=' + dateEndHistoryChatValue,
        'application/xml', 'xml', '', parseChatPublicHistoryListXml, getHistoryError);
    genericAjaxCall('POST', staticVars["urlBackEnd"] + "chatprivatehistory/listchat?dateBegin=" + dateBeginHistoryChatValue + '&dateEnd=' + dateEndHistoryChatValue,
        'application/xml', 'xml', '', parseChatPrivateHistoryListXml, getHistoryError);
}

function refreshHistoryChat() {
    //console.log('refreshHistoryChat');
    $("#historyListTable").empty();
    var dateFrom = dateBeginHistoryChatHandler.datepicker('getDate');
    if (dateFrom != null) dateFrom.setHours(0, 0, 0, 0);
    var dateTo = dateEndHistoryChatHandler.datepicker('getDate');
    if (dateTo != null) dateTo.setHours(23, 59, 59, 0);
    var typeHistoryChatValue = typeHistoryChatHandler.val();
    //var languageHistoryChatValue=languageHistoryChatHandler.val();
    var languageHistoryChatValue = ALL;
    var historicChatDate = null;
    $(listHistoricChatCache)
        .each(
            function(index, historicChat) {
                historicChatDate = new Date(historicChat.date);
                if (dateFrom <= historicChatDate && historicChatDate <= dateTo) {
                    if ((languageHistoryChatValue == ALL || languageHistoryChatValue == historicChat.languageId || historicChat.type == PRIVATE) && (typeHistoryChatValue == ALL || typeHistoryChatValue == historicChat.type)) {
                        var typeLabel = "";
                        if (historicChat.type == PRIVATE) typeLabel = PRIVATECHATLABEL;
                        else if (historicChat.type == PUBLIC) typeLabel = PUBLICCHATLABEL;
                        addHistoryChat(historicChat.type + historicChat.id, typeLabel, historicChat.title, historicChat.date, historicChat.path, historicChat.languageId);
                    }
                }
            });
}

function addHistoryChat(id, type, title, date, path, language) {
    language = getLanguageName(language);
    date = new Date(date);
    date = date.toLocaleDateString(LANGAUGETAGLOCALELABEL.replace('_', '-'));
    $("#historyListTable").append("<tr height='10'/>" +
        "<tr>" +
        "<td class='form-label' style='text-align: left; vertical-align: middle;width: 20%;'>" +
        "<span style='color:black;'>" + type + "</span>" +
        "</td>" +
        "<td class='form-label' style='text-align: left; vertical-align: middle;width: 30%;''>" +
        "<span style='color:black;'>" + title + "</span>" +
        "<td class='form-label' style='text-align: left; vertical-align: middle;width: 35%;'>" +
        "<span style='color:black;'>" + date + "</span>" +
        "</td>"
        /*+"<td class='form-label' style='text-align: left; vertical-align: middle;width: 35%;'>"
            		+"<span style='color:black;'>"+language+"</span>"
            	 +"</td>"*/
        +
        "<td class='form-label' style='text-align: center; vertical-align: middle;width: 15%;'>" +
        "<a href='#' class='blueLink' style='color: #E9752A !important; ' src='#' id='openFile" + id + "'> " + OPENLABEL + " </a>" +
        "</td>" +
        "</tr>");
    $("#openFile" + id).click(function() {
        window.open(
            'chathistory.html?path=' + $.base64.encode(path),
            '_blank');
    });
}

function getLanguageName(languageId) {
    if (languageId != "" && languageId != null && listLangsAbrEnum[languageId] != null) return listLangsAbrEnum[languageId];
    else return "";
}

function getCurrentsChat() {
    genericAjaxCall('POST', staticVars["urlBackEnd"] + 'agenda?forChat=true&idLang=' + getIdLangParam(),
        'application/json', 'json', '', getCurrentsChatSuccess, getCurrentsChatError);
}

function getCurrentsChatSuccess(json) {
    //$("#loading").hide();
    chatComponentHanlder = $('#chatComponent');
    chatComponentHanlder.empty();
    chatComponentHanlder.append('<div id="currentChatsPopup"><select id="currentChatsSelect"><option value="0">' + PUBLICCHATLABEL + '</option></select></div><button id="currentChatsButton" style="cursor: pointer; margin-top: 20px;" class="btn btn-info sendLabel">' + SENDLABEL + '</button>');
    //json=JSON.parse(json);
    $('#chatComponent #currentChatsButton').bind('click', function() {
        connectToChatFlash($('#chatComponent #currentChatsSelect').val());
    });
    $.each(json, function(index, eventInfo) {
        $('#chatComponent #currentChatsSelect').append('<option value="' + eventInfo.id + '">' + eventInfo.ack + '</option>');
    });
};

function getCurrentsChatError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
};

function getHistoryError(xhr, status, error) {
    //$("#loading").hide();
    isSessionExpired(xhr.responseText);
};