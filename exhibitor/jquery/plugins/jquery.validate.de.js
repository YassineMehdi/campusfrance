/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: FR
 */
//jQuery.extend(jQuery.validator.messages, {
//    required: "Dieses Feld ist erforderlich.",
//    remote: "Bitte füllen Sie dieses Feld, um fortzufahren.",
//    email: "Bitte geben Sie eine gültige E-Mail-Adresse.",
//    url: "Bitte geben Sie eine gültige URL.",
//    date: "Bitte geben Sie eine gültige Datum.",
//    dateISO: "Bitte geben Sie ein gültiges Datum (ISO).",
//    number: "Bitte geben Sie eine gültige Zahl.",
//    digits: "Bitte geben Sie (nur) einen numerischen Wert.",
//    creditcard: "Bitte geben Sie eine gültige Kreditkartennummer.",
//    equalTo: "Veuillez entrer une nouvelle fois la même valeur.",
//    accept: "Veuillez entrer une valeur avec une extension valide.",
//    maxlength: jQuery.validator.format("Veuillez ne pas entrer plus de {0} caractères."),
//    minlength: jQuery.validator.format("Veuillez entrer au moins {0} caractères."),
//    rangelength: jQuery.validator.format("Veuillez entrer entre {0} et {1} caractères."),
//    range: jQuery.validator.format("Veuillez entrer une valeur entre {0} et {1}."),
//    max: jQuery.validator.format("Veuillez entrer une valeur inférieure ou égale �  {0}."),
//    min: jQuery.validator.format("Veuillez entrer une valeur supérieure ou égale �  {0}.")
//});


/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: DE (German, Deutsch)
 */
 jQuery(document).ready(function($) {
     $.extend($.validator.messages, {
    	 required: "Dieses Feld ist ein Pflichtfeld.",
		 email: "Geben Sie bitte eine gültige E-Mail Adresse ein.",
		 url: "Geben Sie bitte eine gültige URL ein.",
		 date: "Bitte geben Sie ein gültiges Datum ein.",
		 number: "Geben Sie bitte eine Nummer ein.",
		 digits: "Geben Sie bitte nur Ziffern ein.",
		 equalTo: "Bitte denselben Wert wiederholen.",
		 range: $.validator.format("Geben Sie bitte einen Wert zwischen {0} und {1} ein."),
		 max: $.validator.format("Geben Sie bitte einen Wert kleiner oder gleich {0} ein."),
		 min: $.validator.format("Geben Sie bitte einen Wert größer oder gleich {0} ein.")
     });
 });