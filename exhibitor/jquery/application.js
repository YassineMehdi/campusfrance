//var urlServerPHPStand = 'http://127.0.0.1/COMMON_CHAT/AddStandData.php';
var urlServerPHPStand = 'http://192.168.1.35/HTML5_FMS_Project/AddStandData.php';
var ADDSTAND = "AddStand";
var EDITSTAND = "EditStand";
var DELETESTAND = "DeleteStand";
var subSocket=null;
var jsonManager=null;
var logged = false;
var subSocket;

function getHistoric(){
	jQuery.ajax({
		  url: URLSERVERATHMOSPHERE+'/visitor/chat/'+$.cookie('topic')+'/map',  
		  type: "POST",
		  contentType: "application/json",
		  dataType : 'json',
		  success: function(json){
			if (!logged) logged = true;
			jsonManager=json;
			var list_UserStand=jsonManager.list_UserStand;
			$.each(list_UserStand, function(key, stand)
                    {
                        if (stand.type == DELETESTAND)
            				delStand(stand.standName);
            			else if (stand.type == ADDSTAND)
            				addStand(stand.standName, stand.numberRecruiter);
            			else if (stand.type == EDITSTAND)
            				editStand(stand.standName, stand.numberRecruiter);
                    });
		  },
		  error: function(){					  				        
			  alert("error");
			}
		});	
}

function getMessagesByStand(standName){
	var list_MessageStand=jsonManager.list_MessageStand;
	$.each(list_MessageStand, function(standNameKey, manageMessage)
            {
			if(standNameKey==standName) {
					if($('#messages_ui_' + standName)!=null) $('#messages_ui_' + standName).empty();
					$.each(manageMessage.listMessage, function(key, message)
	                {
	    				if(standNameKey==message.receiver) {
	    					if(message.sender==$.cookie('topic')) addMessageAccept(message.messageText, message.receiver);
		    				else addMessage(message.sender, message.messageText, message.dateTime, message.receiver);
	    				}
	                });
	            }
            });
}

function  initChat(){
	//alert("initChat");
	var socket = $.atmosphere;
	var countMessages=0;
	var request = {
		url : URLSERVERATHMOSPHERE+'/visitor/chat/'+$.cookie('topic'),
		contentType : "application/json",
		logLevel : 'debug',
		transport : 'websocket',
		fallbackTransport : 'long-polling'
	};

	request.onOpen = function(response) {
		//inputTextChat.removeAttr('disabled').focus();
	};

	request.onMessage = function(response) {
		//alert("onMessage chat");
		var message = response.responseBody;
		try {
			var jsonResponse = jQuery.parseJSON(message);
		} catch (e) {
			console.log('This doesn\'t look like a valid JSON: ', message.data);
			return;
		}

		//inputTextChat.removeAttr('disabled').focus();
		
		//alert(message);
		if (jsonResponse.standName != null) {
			//alert(message);
			if (jsonResponse.type == DELETESTAND)
				delStand(jsonResponse.standName);
			else if (jsonResponse.type == ADDSTAND)
				addStand(jsonResponse.standName, jsonResponse.numberRecruiter);
			else if (jsonResponse.type == EDITSTAND)
				editStand(jsonResponse.standName, jsonResponse.numberRecruiter);
		} else {
			if(jsonResponse.receiver==$.cookie('topic')){
				addMessageToJsonManager(jsonResponse);
				/*addMessage(jsonResponse.sender, jsonResponse.messageText, jsonResponse.dateTime,
						$.cookie('topic'));*/
				if(jsonResponse.sender==$.cookie('topic')) addMessageAccept(jsonResponse.messageText, jsonResponse.receiver);
				else addMessage(jsonResponse.sender, jsonResponse.messageText, jsonResponse.dateTime,
						jsonResponse.receiver);
				
			}else if(jsonResponse.receiver=="enterprise="+$.cookie('ENTERPRISE_NAME')){//decline
				countMessages++;
				addMessageDeclinedToModeration(jsonResponse.messageText, countMessages,
						jsonResponse.sender);
			}else if(jsonResponse.receiver=='MODERATEUR='+$.cookie('login')){//"moderator:"+$.cookie('login')
				countMessages++;
				addMessageToModeration(formatMessage(jsonResponse.sender, jsonResponse.messageText, jsonResponse.dateTime), countMessages,
						jsonResponse.sender);
			}
		}
	};

	request.onClose = function(response) {
		//alert("Closed (CHAT)"); 
		//alert(response.state); 
		logged = false;
	};

	request.onError = function(response) {
		// content.html($('<p>', { text: 'Sorry, but there\'s some problem with
		// your socket or the server is down' }));
		alert('Sorry, but there\'s some problem with your socket or the server is down');
	};
	
	if(subSocket!=null) subSocket.close();
	subSocket = socket.subscribe(request);
	//alert('initChat');
	
};

function publishMessage(sender, messageText, receiver){
	//alert(URLSERVERMANAGER+'/SpringGreetings/rest/webcastC/enterprise/chatmodule/'+$.cookie('topic'));
	jQuery.ajax({
		  url: URLSERVERMANAGER+'/SpringGreetings/rest/webcastC/enterprise/chatmodule/'+$.cookie('topic'),  
		  type: "POST",
		  contentType: "application/xml",
		  dataType : 'xml',
		  data : '<message__><sender>'+sender+'</sender><messageText>'+messageText+'</messageText><receiver>'+receiver+'</receiver></message__>',
		  success: function(xml){
			//var nom=$(xml).find("nom").text();		  	
		  },
		  error: function(){					  				        
		  }
	});	
	//url: URLSERVERATHMOSPHERE+'/visitor/chat/'+$.cookie('topic')+'/MFS/Lstand', 
	/* alert(URLSERVERJEE+'/SpringGreetings/rest/webcastC/enterprise/chatmodule/'+$.cookie('topic'));
	 jQuery.ajax({
		  url : URLSERVERJEE+'/SpringGreetings/rest/webcastC/enterprise/chatmodule/'+$.cookie('topic'),
		  type: "POST",
		  contentType: "application/xml",
		  dataType : 'xml',
		  data :  '<message__><sender>'+sender+'</sender><messageText>'+messageText+'</messageText><receiver>'+receiver+'</receiver></message__>',
		  success: function(xml){
			//var nom=$(xml).find("nom").text();		  	
		  },
		  error: function(){	
			  alert('Error');
		  }
	});	*/
}

function publishMessageToServer(urlServer, type, contentType, dataType, data){
	jQuery.ajax({
		  url: urlServer,  
		  type: type,
		  contentType: contentType,
		  dataType : dataType,
		  data : data,
		  success: function(xml){
			//var nom=$(xml).find("nom").text();		  	
		  },
		  error: function(){					  				        
		  }
	});
}

function addMessageToJsonManager(message){
	var standName=message.receiver;
	var list_MessageStand=jsonManager.list_MessageStand;
	$.each(list_MessageStand, function(standNameKey, manageMessage){
			if(standNameKey==standName) {
				manageMessage.listMessage.push(message);
	}
 });
}

function sendMessage() {
	if ($("[id='current']") && $("[id='current']").children()[0]) {
		//var sender = fullName;
		var sender = $.cookie('login');
		var receiver = $("[id='current']").children()[0].name;
		var messageText = htmlEncode(inputTextChat.val());
		if($.cookie('ENTERPRISE_NAME')!=undefined && $.cookie('ENTERPRISE_NAME')!='') receiver=$.cookie('topic');
		else receiver=$.cookie('MODERATEUR');
		publishMessage(sender, messageText, receiver);
		//publishMessageToServer(urlServerPHPMessage, "POST", "application//x-www-form-urlencoded", 'text', 'data='+sender+ '|::..::|' + receiver + '|::..::|'+ messageText + '|::..::|');
		inputTextChat.val('');
	}
}

function htmlEncode(value){
	  //create a in-memory div, set it's inner text(which jQuery automatically encodes)
	  //then grab the encoded contents back out.  The div never exists on the page.
	  return $('<div/>').text(value).html();
}

function htmlDecode(value){
  return $('<div/>').html(value).text();
}
	
function addMessage(sender, messageText, dateTime, receiver) {
	$('#messages_ui_' + receiver).append(
			'<p><span style="color:black">'
					+ formatMessage(sender, htmlDecode(messageText), dateTime)
					+ '</span></p>');
	$('#content_tabs').scrollTop($('#messages_ui_' + receiver).height());
	$('#content_tabs').perfectScrollbar();
}

function addMessageAccept(message, receiver) {
	$('#messages_ui_' + receiver).append(
			'<p><span style="color:black">'
					+ htmlDecode(message)
					+ '</span></p>');
	$('#content_tabs').scrollTop($('#messages_ui_' + receiver).height());
	$('#content_tabs').perfectScrollbar();
}

function formatMessage(sender, messageText, dateTime) {
	return dateTime + sender + " : " + messageText;
}

function formatDate(sender, messageText, dateTime) {
	var dtArray = dateTime.split('.');
	var str = dtArray[0];
	var arr = str.split("T");
	arr[0] = arr[0].split("-");
	arr[1] = arr[1].split(":");
	var year = arr[0][0];
	// var month = parseInt(arr[0][1],10)-1;
	var month = arr[0][1];
	var day = arr[0][2];
	var hours = arr[1][0];
	var minutes = arr[1][1];
	return "[" + month + "/" + day + "/" + year + " " + hours + "h"
			+ minutes + "] " + sender + " : " + messageText;
}

function addStand(standName, numberRecruiter) {
	if (!document.getElementById(standName)) {
		$('#list_stand').append(
				'<li id=\'' + standName + '\' class="item1"><a href="javascript:void(0)">'
						+ standName + '<span>' + numberRecruiter
						+ '</span></a></li>');
		$('#list_stand')
				.on(
						'click',
						'#' + standName + '',
						function() {
							inputTextChat=$('#inputTextChat');
							inputTextChat.removeAttr('disabled').focus();
							var itemNameSelected = "";
							if ($("[id='current']") != null)
								if ($("[id='current']").children()[0] != null)
									itemNameSelected = $("[id='current']")
											.children()[0].name;
							if (itemNameSelected != standName) {
								var el = document.getElementById("tabs")
										.getElementsByTagName("a");
								var find = false;
								for ( var i = 0; i < el.length; i++) {
									var tabSelected = el[i];
									if (standName == tabSelected.name)
										find = true;
								}
								if (!find) {
									$("[id='current']").attr('id', '');
									$('#tabs')
											.append(
													'<li id="current" onclick="tabManager(\''
															+ standName
															+ '\')" style="height:100%; margin-top:2px;"><a href="javascript:void(0)" name="'
															+ standName
															+ '" style="padding-top: 2px; padding-bottom: 2px;height:100%">'
															+ standName
															+ '</a> <label class="clearitem"><a href="javascript:void(0)">X</a></label></li>');
									$("[id='current']").addClass(
											'tab_select_' + standName);
									$('#content_tabs')
											.append(
													'<div id="tab_'
															+ standName
															+ '"><div style="width:100%;height:100%;" id="messages_ui_'
															+ standName
															+ '" name="messages_ui_'
															+ standName
															+ '"></div></div>');
									$('#tabs .clearitem')
											.bind(
													"click",
													function() {
														$(this).parent()
																.remove();
														$("#tab_"+standName).remove();
														removeUserFromStand(standName);
													});
									tabManager(standName);
									addUserToStand(standName);
								} else {
									tabManager(standName);
								}
								getMessagesByStand(standName);
							}
						});
	}
}

function editStand(standName, numberRecruiter) {
	if ($('#' + standName) != null & $('#' + standName).css('display')!=null){
		$('#' + standName).html(
				'<a href="javascript:void(0)">' + standName + '<span>' + numberRecruiter
						+ '</span></a>');
	}else{
		addStand(standName, numberRecruiter);
	}
}

function delStand(standName) {
	//alert(standName);
	if ($('#' + standName) != null) $('#' + standName).remove();
}

function addUserToStand(name) {
	publishMessageToServer(urlServerPHPStand, "POST", "application/x-www-form-urlencoded", 'text', 'data='+'true|::..::|'+ name + '|::..::|' + $.cookie("login") + '|::..::|'+photoURL+'|::..::|'+typeUser+'|::..::|');
}

function removeUserFromStand(name) {
	publishMessageToServer(urlServerPHPStand, "POST", "application/x-www-form-urlencoded", 'text', 'data='+'false|::..::|'+ name + '|::..::|' + $.cookie("login") + '|::..::|'+photoURL+'|::..::|'+typeUser+'|::..::|');
}

//Panel Dragible
function tabManager(data){
			$("[id='current']").attr('id','');
			//$("[style=\"display: block;\"]").css("display", "none");
			$(".tab_select_"+data).attr('id','current'); 		
			$(".tab_select_"+data).css("display", "block"); // TAB LI 
			$("[id^=messages_ui]").css("display", "none");
			$("#messages_ui_"+data).css("display", "block"); //TAB Content
			$("#tab_bbb").css("display", "block"); //TAB Content
		}


function closeChatNetconnection(){
	if(subSocket!=null) {
		subSocket.close();
		subSocket=null;
	}
}

function addMessageToModeration(message, id, sender){
	$('#contentModeration').append("<tr id='mod"+id+"' style='font-weight:bold;font-size:11px;width:100%;height:10px;'><td style='width:100%;vertical-align:middle'><label id='modlabel"+id+"' style='cursor:pointer;'>"+htmlDecode(message)+"</label></td><td><button onClick=\"declineMessage('"+id+"','"+sender+"')\" style='margin: 8px;'><label style='cursor:pointer;' class='decline'>Decline</label></<button></td><td><button onClick=\"acceptMessage('"+id+"','"+sender+"')\" style='margin: 8px;'><label style='cursor:pointer;' class='accept'>Accept</label></<button></td></tr>");
	$('#corpsModeration').scrollTop($('#contentModeration').height());
	$('#corpsModeration').perfectScrollbar();
}

function addMessageDeclinedToModeration(message, id, sender){
	$('#contentModeration').append('<tr id="mod'+id+'"><td colspan="2" style="line-height: 16px;">'+message+'</td><td><span onClick="removeDeclinedMessage('+id+')"><a href="javascript:void(0)" style="font-size:15px;font-family:verdana;color:#a7a7a7">X</a></span></td></tr>');
	$('#corpsModeration').scrollTop($('#contentModeration').height());
	$('#corpsModeration').perfectScrollbar();
}

function acceptMessage(id, sender){
	submitModerationResponse(sender, htmlEncode($('#modlabel'+id).text()), $.cookie('topic'));
	$('#mod'+id).remove(); //remove all data of presentation puted in the Q&A component
}

function declineMessage(id, sender){
	declineModerationMessage(sender, htmlEncode($('#modlabel'+id).text()), 'DECLINE');
	$('#mod'+id).remove(); //remove all data of presentation puted in the Q&A component
}

function submitModerationResponse(sender, messageText, receiver){
	publishMessageToServer(URLSERVERMANAGER+'/SpringGreetings/rest/webcastC/enterprise/chatmodule/'+$.cookie('topic'), "POST", "application/xml", 'xml', '<message__><sender>'+sender+'</sender><messageText>'+messageText+'</messageText><receiver>'+receiver+'</receiver></message__>');	
}

function declineModerationMessage(sender, messageText, receiver){
	//publishMessageToServer(URLSERVERATHMOSPHERE+'/visitor/chat/' + topic + '/MFS/Lstand', "POST", "application/xml", 'xml', 'sender='+sender+'&messageText='+messageText+'&receiver=entrepriseName:'+$.cookie('topic'));	
	publishMessageToServer(URLSERVERMANAGER+'/SpringGreetings/rest/webcastC/enterprise/chatmodule/'+$.cookie('topic'), "POST", "application/xml", 'xml', '<message__><sender>'+sender+'</sender><messageText>'+messageText+'</messageText><receiver>'+receiver+'</receiver></message__>');	
}

function removeDeclinedMessage(id){
	$('#mod'+id).remove();
}