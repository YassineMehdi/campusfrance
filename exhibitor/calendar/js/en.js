var month = [
    "January","February","March","April","May","June","July","August","September","October","November","December"
];
var monthAbbr = [
    "Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"
];
var days = [
    "Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"
];
var daysAbbr = [
    "Mon","Tue","Wed","Thu","Fri","Sat","Sun"
];
var deleteString = "You really want to delete this event ?";
var buttonEditStr = "Edit";
var buttonAddStr = "Add";
var descriptionStr = "Title";
var hourStr = "Hour";
var dateStr = "Date";
var eventStr = "event";
var dayStr = "Day";
var weekStr = "Week";
var monthStr = "Month";
var resultStr = "Results";
var requiredStr = "description is required.";
var noRecordStr = "No events found.";
