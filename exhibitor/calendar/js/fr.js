var month = [
    "Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Août","Septembre","Octobre","Novembre","Décembre"
];
var monthAbbr = [
    "Jan","Fev","Mar","Apr","Mai","Jun","Jul","Aoû","Sep","Oct","Nov","Déc"
];
var days = [
    "Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi","Dimanche"
];
var daysAbbr = [
    "Lun","Mar","Mer","Jeu","Ven","Sam","Dim"
];
var deleteString = "Voulez vous vraiment supprimer cet événement ?";
var buttonEditStr = "Modifier";
var buttonAddStr = "Ajouter";
var descriptionStr = "Titre";
var hourStr = "Heure";
var dateStr = "Date";
var eventStr = "événement";
var dayStr = "Jour";
var weekStr = "Semaine";
var monthStr = "Mois";
var resultStr = "résultats";
var requiredStr = "la description est obligatoire.";
var noRecordStr = "Pas d'événement trouvé.";
