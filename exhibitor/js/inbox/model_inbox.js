jQuery.extend({
	
	InboxModel: function(){
		var that = this;
		var idFolder = 1;
		var listeners = new Array();
		
		this.initModel = function(){
			that.fillMessageFolders();
			that.fillReceivedConversations();
		};
		
		this.parseXmlMessageFolders = function(data){
			that.notifyLoadMessageFolder(new $.MessageFolderList($(data)));
		},
		
		this.fillMessageFoldersError = function(xhr, status, error){
			isSessionExpired(xhr.responseText);
		},
		
		this.fillMessageFolders = function(){
			genericAjaxCall('GET', staticVars.urlBackEnd + 'messageFolder/folders',
					'application/xml', 'xml', '', function(xml) {
						that.parseXmlMessageFolders(xml);
					}, that.fillMessageFoldersError);
		},
		
		this.parseReceivedConversationXml = function(data){
			that.notifyLoadConversation(new $.ConversationList($(data)));
		},
		
		this.getConversationsError = function(xhr, status, error) {
			isSessionExpired(xhr.responseText);
		},
		
		this.fillReceivedConversations = function(){
			that.getReceivedConversations(idFolder);
		},
		
		this.getReceivedConversations = function(messageFolderId){
			//console.log('inside Get getReceivedConversations');
			if(messageFolderId==null) messageFolderId=1;
			genericAjaxCall('GET', staticVars.urlBackEnd + 'conversation/stand/received?messageFolderId=' + messageFolderId, 
					'application/xml', 'xml', '', that.parseReceivedConversationXml, that.getConversationsError);	
		},
				
		this.loadArchivedConversationSuccess = function(data){
			that.notifyLoadConversation(new $.ConversationList($(data)));
		},
		
		this.loadArchivedConversationError = function(xhr, status, error) {
			isSessionExpired(xhr.responseText);
		},
		
		this.loadArchivedConversation = function(){
			genericAjaxCall('GET', staticVars.urlBackEnd + 'conversation/stand/archived', 
					'application/xml', 'xml', '', that.loadArchivedConversationSuccess, that.loadArchivedConversationError);	
		},
		
		this.markAsReadSuccess = function(conversationId){
			//console.log(conversationId);
			that.notifyAsRead(conversationId);
		},
		
		this.markAsReadError = function(xhr, status, error) {
			isSessionExpired(xhr.responseText);
		},
		
		this.markAsRead = function(conversationId){
			//console.log('inside Get getReceivedConversations');
			genericAjaxCall('POST', staticVars.urlBackEnd + 'conversation/stand/markAsRead/byId?conversationId=' + conversationId, 
					'application/xml', 'xml', '', function(){ that.markAsReadSuccess(conversationId);}, that.markAsReadError);	
		},
		
		this.sendReplySuccess = function(data){
			that.notifySendReplySuccess(new $.Message($(data)));
		},
		
//		this sendReplyError = function(xhr, status, error) {
//			isSessionExpired(xhr.responseText);
//		},
		
		this.sendReply = function(message,conversationId){
			genericAjaxCall('POST', staticVars.urlBackEnd + 'message/reply?conversationId=' + conversationId, 
					'application/xml', 'xml', message.xmlData(), that.sendReplySuccess, that.sendReplyError);	
		},
		
		this.addNewFolderSuccess = function(data){
			that.notifyAddNewFolder(new $.MessageFolder($(data)));
			//console.log(data);
		},
//		
//		this addNewFolderError = function(xhr) {
//			isSessionExpired(xhr);
//		},
		
		this.addNewFolder = function(folder){
			genericAjaxCall('POST', staticVars.urlBackEnd + 'messageFolder/createFolder', 
					'application/xml', 'xml', folder.xmlData(), that.addNewFolderSuccess, that.addNewFolderError);	
		},	
		
		this.deleteFoldereSuccess = function(data){
			that.notifyDeleteFolder();
		},
		
		this.deleteFolder = function(idFolder){
			genericAjaxCall('POST', staticVars.urlBackEnd + 'messageFolder/deleteFolder?idFolder='+ idFolder,
					'application/xml', 'xml', '', that.deleteFoldereSuccess, that.deleteFoldereError);	
		},	
		
		this.changeFolderSuccess = function(){
			that.notifyChangeFolder();
		};
		this.changeFolder = function(idFolder, idConversationList){
			for(var index in idConversationList){
				genericAjaxCall('POST', staticVars.urlBackEnd + 'conversation/stand/changeFolder/byId?conversationId='+ idConversationList[index]+ '&messageFolderId='+idFolder,
						'application/xml', 'xml', '', that.changeFolderSuccess, '');	
			}
		},	
		
		this.deleteConversation = function(idConversationList){
			for(var index in idConversationList){
				genericAjaxCall('POST', staticVars.urlBackEnd + 'conversation/stand/delete/byId?conversationId='+ idConversationList[index],
						'application/xml', 'xml', '', '', '');	
			}
			that.notifyDeleteConversation();
		},		
				
		this.archiveConversation = function(idConversationList){
			for(var index in idConversationList){
				genericAjaxCall('POST', staticVars.urlBackEnd + 'conversation/stand/archive/byId?conversationId='+ idConversationList[index],
						'application/xml', 'xml', '', '', '');	
			}
			that.notifyArchivedConversation();
		},		
		
		this.notifyLoadMessageFolder = function(messageFolder){
			$.each(listeners, function(i){			
				listeners[i].notifyLoadMessageFolder(messageFolder);
			});
		};
		
		this.notifyLoadConversation = function(conversation){
			$.each(listeners, function(i){			
				listeners[i].notifyLoadConversation(conversation);
			});
		};
		
		this.notifyAsRead = function(conversationId){
			$.each(listeners, function(i){			
				listeners[i].notifyAsRead(conversationId);
			});
		};
		
		this.notifySendReplySuccess = function(message){
			$.each(listeners, function(i){			
				listeners[i].notifySendReplySuccess(message);
			});
		};
		
		this.notifyAddNewFolder = function(folder){
			$.each(listeners, function(i){			
				listeners[i].notifyAddNewFolder(folder);
			});
		};
		
		this.notifyDeleteFolder = function(){
			$.each(listeners, function(i){			
				listeners[i].notifyDeleteFolder();
			});
		};
		
		this.notifyChangeFolder = function(){
			$.each(listeners, function(i){			
				listeners[i].changeFolder();
			});
		};
		
		this.notifyDeleteConversation = function(){
			$.each(listeners, function(i){			
				listeners[i].notifyDeleteConversation();
			});
		};
		
		this.notifyArchivedConversation = function(){
			$.each(listeners, function(i){			
				listeners[i].notifyArchivedConversation();
			});
		};
		
		this.addListener = function(list){
			listeners.push(list);
		};
		
	},
		
	ModelListener: function(list) {
		if(!list) list = {};
		return $.extend(list);
	},
});
