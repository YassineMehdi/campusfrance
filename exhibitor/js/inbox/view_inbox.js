jQuery.extend({
	
	MessageFoldersManage : function(messageFolders, messageFoldersContainer, view){
		//var that=this;
		var dom = messageFoldersContainer;
		
		this.refresh = function(){
			for(var index in messageFolders.getMessageFolders()){
				messageFolderManage = new $.MessageFolderManage(messageFolders.getMessageFolders()[index], view);
				dom.append(messageFolderManage.getDOM());
				$('.selectpicker ').selectpicker();
			}
			
			dom.append('<li class="dropdown"><a style="padding: 15px 10px 20px 10px;" class="add-folder"><i class="icon-plus"></i>'+ADDFOLDERLABEL+'</a>'
					+'</li>');
			
			$('.add-folder').unbind('click').bind('click',function(){
				$('.popupInbox, .backgroundPopup').show();
				
				$('.saveAddFolderButton').click(function(e) {
					if($('#folderForm').valid()){
						var folder = new $.MessageFolder();
						folder.setFolderName($('#folderTitle').val());
						view.addNewFolder(folder);
					}
				});
				
				
				$('.closePopup,.eds-dialog-close').click(function(e) {
					$('.eds-dialog-container').hide();
					$('.backgroundPopup').hide();
				});
			});
			
			$('.changeFolderButton').unbind('click').bind('click',function(){
				var idFolder = $('.folderSelect').val();
				view.changeFolder(idFolder, $idConversationList);
			});
			
			$('.deleteConversationButton').unbind('click').bind('click',function(){
				view.deleteConversation($idConversationList);
			});
			
			$('.archiveConversationButton').unbind('click').bind('click',function(){
				view.archiveConversation($idConversationList);
			});
		};
		
		

		this.getDOM = function(){ 
			return dom;
		},
		
		this.refresh();
	},
	
	MessageFolderManage : function(folder, view){
		//var that=this;
		var dom = $('<li id="folder_'+folder.getIdMessageFolder()+'"><a style="padding: 15px 10px 20px 25px;">'+folder.getFolderName()+'<span class="remove delete" id="removeFolder" style="z-index: 1000;"><img _tmplitem="9" width="15" height="15" src="images/delete.png" style=" cursor: pointer; margin-top: -5px; margin-left: 30px; "></span></a></li>');

		this.refresh = function(){
			$('.folderSelect').append('<option value="'+folder.getIdMessageFolder()+'">'+folder.getFolderName()+'</option>');
		},
		
		dom.unbind("click").bind('click',function(){
			$('.inboxFolders').find('li').removeClass('active');
			$('.inboxFolders').find('#folder_'+folder.getIdMessageFolder()).addClass('active');
			
			$('.email-content-tab').find('.tab-pane').removeClass('active');
			
			$('#preview-email-wrapper').hide();
			
			
			dom.find('#removeFolder').click(function(e) {
				var idFolder = $(this).closest('li').attr('id').split('_')[1];
				view.deleteFolder(idFolder);
			});
			
			view.loadConversationByFolderId(folder.getIdMessageFolder());
		});
		
		this.getDOM = function(){ //retourne une ligne HTML <a .....
			return dom;
		},
		
		this.refresh();
	},
	
	ConversationsManage : function(conversationList, conversationsContainer, view){
		//var that=this;
		var folderId = '';
		if(conversationList.conversations[0] != null) {
			folderId =  conversationList.conversations[0].messageFolder.idMessageFolder;
		}
		
		var dom = $('<div class="tab-pane active" style="padding: 0;"> '
						+'<div class="row-fluid">' 
							+'<div class="span12">'
								+'<div id="email-list" style="border: 1px solid #ebebeb; border-top: 0px;"> '
									+'<table class="table table-fixed-layout table-hover" id="messageFolder_'+folderId+'"> <thead> <tr><th class="small-cell" style="padding-left: 13px; padding-top: 16px; "><input class="selectAllCheckbox" id="deleteAllCheckbox" type="checkbox" /> <label for="deleteAllCheckbox"></label></th> <th class="medium-cell"></th> <th style=" padding-left: 0px; "><div class="actionButtons" style="display:none;"><a class="moveConversation" style="     margin-right: 10px; "><button type="button"class="btn btn-small btn-white "><i class=""></i>Déplacer</button></a><a class="deleteConversation" style="     margin-right: 10px; "><button type="button"class="btn btn-small btn-white "><i class=""></i>Supprimer</button></a><a class="archiveConversation" style="margin-right: 10px; "><button type="button"class="btn btn-small btn-white "><i class=""></i>Archiver</button></a></div> </th> <th class="medium-cell"></th> </tr> </thead>'
									+'</table> '
								+'</div> '
							+'</div> '
						+'</div> '
					+'</div>');
		var conversation = null;
		
		this.refresh = function(){
			$conversationsContainer.empty();
			$conversationsContainer.append(dom);
			for(var index in conversationList.conversations){
				conversation = new $.ConversationManage(conversationList.conversations[index], view);
				dom.find('.table').append(conversation.getDOM());
			}
			
			$('.moveConversation').bind("click").bind("click", function(){
				var idFolder = conversationList.conversations[0].messageFolder.idMessageFolder;
		    	var checkedConversation = $('#messageFolder_'+idFolder).find('.checkboxConversation');
		    	$idConversationList = new Array();
				for (var i = 0; i < checkedConversation.length; i++) {
		    		if($(checkedConversation[i]).is(':checked')){
		    			var idSelected = $(checkedConversation[i]).attr('id').split('_')[1];
		    			$idConversationList.push(idSelected);
		    		}
				}
				$('.backgroundPopup').show();
                $(".popupChangeFolder").show();
				
				$('.closePopup,.eds-dialog-close').click(function(e) {
					$('.eds-dialog-container').hide();
					$('.backgroundPopup').hide();
				});
			});
			
			$('.deleteConversation').click(function(){
				var idFolder = conversationList.conversations[0].messageFolder.idMessageFolder;
		    	var checkedConversation = $('#messageFolder_'+idFolder).find('.checkboxConversation');
		    	$idConversationList = new Array();
				for (var i = 0; i < checkedConversation.length; i++) {
		    		if($(checkedConversation[i]).is(':checked')){
		    			var idSelected = $(checkedConversation[i]).attr('id').split('_')[1];
		    			$idConversationList.push(idSelected);
		    		}
				}
				$('.backgroundPopup').show();
                $(".popupDeleteConversation").show();
				
				$('.closePopup,.eds-dialog-close').click(function(e) {
					$('.eds-dialog-container').hide();
					$('.backgroundPopup').hide();
				});
			});
			
			$('.archiveConversation').click(function(){
				var idFolder = conversationList.conversations[0].messageFolder.idMessageFolder;
		    	var checkedConversation = $('#messageFolder_'+idFolder).find('.checkboxConversation');
		    	$idConversationList = new Array();
				for (var i = 0; i < checkedConversation.length; i++) {
		    		if($(checkedConversation[i]).is(':checked')){
		    			var idSelected = $(checkedConversation[i]).attr('id').split('_')[1];
		    			$idConversationList.push(idSelected);
		    		}
				}
				$('.backgroundPopup').show();
        $(".popupArchiveConversation").show();
				
				$('.closePopup,.eds-dialog-close').click(function(e) {
					$('.eds-dialog-container').hide();
					$('.backgroundPopup').hide();
				});
			});
			
			$('.selectAllCheckbox').click(function(){
				if($('.inboxFolders').find('.active').attr('id') != "archiveFoder"){
					var idFolder = conversationList.conversations[0].messageFolder.idMessageFolder;
					var checkedConversation = $('#messageFolder_'+idFolder).find('.checkboxConversation');
					$idConversationList = new Array();
					if($('.selectAllCheckbox').is(':checked') != true){
						for (var i = 0; i < checkedConversation.length; i++) {
							$(checkedConversation[i]).prop( "checked", false );
						}
						$('.actionButtons').hide();
					}else{
						for (var i = 0; i < checkedConversation.length; i++) {
							$(checkedConversation[i]).prop( "checked", true );
						}
						$('.actionButtons').show();
					}
				}
			});
		},
		
		this.getDOM = function(){ 
			return dom;
		},
		
		this.refresh();
	},

	ConversationManage : function(conversation, view){
		//var that=this;
		var lastUpdate = conversation.lastUpdate;
		var unread = false;
		//console.log(conversation);
		var dom = $('<tr class="new-mail" id="mail_row_'+conversation.idConversation+'">'
						+'<td valign="middle" class="small-cell"><input class="checkboxConversation" id="checkbox_'+conversation.idConversation+'" type="checkbox" /> <label for="checkbox_'+conversation.idConversation+'"></label></td>' 
						+'<td valign="middle" class="clickable">'+htmlEncode(conversation.getCandidate().getFullName())+'</td> <td valign="middle" class="clickable tablefull"><div class="label label-important"></div> <span class="muted" style="margin-top: 5px;">'+htmlEncode(conversation.subject)+'</span></td>' 
						+'<td class="clickable"><span class="timestamp">'+htmlEncode(lastUpdate)+'</span></td> '
					+'</tr>');
		
		
		this.refresh = function(){			
			var messageList = conversation.messages.messages;
			for(var index in messageList){
				var receiver = messageList[index].receiver.userProfileId;
				if(receiver == ''){
					if(messageList[index].unread == true){
						dom.find('.label-important').html(NEWMESSAGELABEL);
						unread = true;
					}
				}else{
					//picture = $(getRecruiterInfo).find('avatar').text();
				}
				
			};
			
			dom.find('.clickable').click(function(){
				$('.email-heading').html(conversation.subject);
				$('.user-recipient').remove();
				$('.response-recipient').remove();
				for(var index in messageList){
					var messagesDetail = new $.MessagesManage(messageList[index], view,conversation);
					$('.resp_container').prepend(messagesDetail.getDOM());
					$('#preview-email-wrapper').show();
				}
				if(unread) 
					view.markAsRead(conversation.idConversation);
				
				$('.mail-buttons').unbind("click").bind("click",function(){
				    //console.log(conversation);
					$('.response-recipient').remove();
					$('.resp_container').prepend('<div class="response-recipient mail-shortcuts" id="response-recipient"  style="width: 98%;   margin-top: 70px;">'
									+'<label for="comment">'+MESSAGELABEL+': *</label>'
										+'<textarea class="form-control email_response" rows="5" id="email_response"></textarea>'
										+'<div class="pull-right" style="margin-right: -25px; ">'
											+'<a><button type="button" class="btn btn-small btn-info send_response">'+SENDLABEL
											+'</button></a>'
										+'</div>'
								+'</div>');
					$('#email_response').attr('style','  width: 100% !important;');
					
					$('.send_response').unbind("click").bind("click",function(){
						//console.log(conversation);
						if($('#email_response').val() == ""){
							$('#email_response').after('<span class="error_mesage">Champs Obligatoire</span>');
						}else{
							var message = new $.Message();
							var userProfile = new $.UserProfile();
							var messageList = conversation.messages.messages;
							var messageSender = messageList[0].sender;
							message.setContent($('#email_response').val());
							message.setIdmessage('');
							message.setUnread(true);
							userProfile.setUserProfileId(messageSender.userProfileId);
							userProfile.setFirstName(messageSender.firstName);
							userProfile.setSecondName(messageSender.secondName);
							message.setReceiver(userProfile);
							view.sendReply(message,conversation.idConversation);
						}
					});
				});
				goToByScroll(".resp_container");
			});
			
			dom.find('.checkboxConversation').click(function(){
				if($('.inboxFolders').find('.active').attr('id') != "archiveFoder"){
					var idFolder = conversation.messageFolder.idMessageFolder;
					if ($(this).is(':checked')) {
						$('.actionButtons').show();
					} else {
						var checkedConversation = $('#messageFolder_'+idFolder).find('.checkboxConversation');
						var showButtons = false;
						for (var i = 0; i < checkedConversation.length; i++) {
							if($(checkedConversation[i]).is(':checked')){
								showButtons = true;
							}
						}
						
						if(showButtons == false){
							$('.actionButtons').hide();
						}else{
							$('.actionButtons').show();
						}
					}
				}
				
			});
		},
		
		this.getDOM = function(){ //retourne une ligne HTML <a .....
			return dom;
		},
		
		this.refresh();
	},
	
	MessagesManage : function(message, view, conversation){
		//var that=this;
		//console.log(conversation);
		var firstName = message.sender.firstName;
		var secondName = message.sender.secondName;
		var receiver = message.receiver.userProfileId;
		var picture = '';
		if(receiver == ''){
			picture = conversation.getCandidate().getUserProfile().getAvatar();
		}else{
			picture = $(getRecruiterInfo).find('avatar').text();
		}
		//console.log(getRecruiterInfo);
		var dom = $('<div class="user-recipient mail-shortcuts" style="width: 100%; ">'
					+'<div class="pull-left">'
						+'<div class="user-profile-pic">'
							+'<img width="50" height="50" src="'+picture+'">'
						+'</div>'                                                 
					+'</div>'
					+'<div class="span12">'
					+'<div class="grid-body arrow-box-recipient" style="padding: 20px; background-color: #fafafa;">'
					+'<div class="pull-left email-sender">'+htmlEncode(firstName+' '+secondName)+'<br /></div>'
					+'<div class="pull-right">'
					+'<div class="mail-time">'
					+'<span>'+htmlEncode(message.messageDate)+'</span>'
					+'<div class="btn-group mail-group">'
					+'</div>'
					+'</div>'
					+'</div>'
					+'<div class="clearfix"></div>'
					+'<br />'
					+'<div class="email-body">'
					+'<p>'+htmlEncode(message.content).replace(/[\n]/gi, "<br/>" )+'</p>'
					+'</div>'
					+'</div>'
					+'</div></div>');
		
		this.refresh = function(){
			
		},
		
		this.getDOM = function(){ //retourne une ligne HTML <a .....
			return dom;
		},
		
		this.refresh();
	},
	
	InboxView: function(){
		var that = this;
		var listeners = new Array();
		$idConversationList = null;
		
		this.initView = function(){
			$messageFoldersContainer = $('.inboxFolders');
			$conversationsContainer = $('.email-content-tab');
			$('#receptionFoder').unbind("click").bind('click',function(){
				$('.inboxFolders').find('li').removeClass('active');
				$('#receptionFoder').addClass('active');
				$('.email-content-tab').find('.tab-pane').removeClass('active');
				$('#preview-email-wrapper').hide();
				that.loadConversationByFolderId(1);
			});
			
			$('#archiveFoder').unbind("click").bind('click',function(){
				$('.inboxFolders').find('li').removeClass('active');
				$('#archiveFoder').addClass('active');
				$('.email-content-tab').find('.tab-pane').removeClass('active');
				$('#preview-email-wrapper').hide();
				that.loadArchivedConversation();
			});
		};
		
		this.initMessageFolder = function(messageFolders){
			new $.MessageFoldersManage(messageFolders, $messageFoldersContainer, that);
		},
		
		this.initConversation = function(conversations){
			// var reverseConversations = conversations.reverse();
			// console.log(reverseConversations);
			new $.ConversationsManage(conversations, $conversationsContainer, that);
			if($('.inboxFolders').find('.active').attr('id') == "archiveFoder" || conversations.conversations.length == 0){
				$('.selectAllCheckbox').hide();
			};
		},
		
		this.notifyAsRead = function(conversationId){
			$('#mail_row_'+conversationId).find('.label-important').html('');
		},
		
		this.notifySendReplySuccess = function(message){
			var messagesDetail = new $.MessagesManage(message, that);
			$('.resp_container').prepend(messagesDetail.getDOM());
			$('#preview-email-wrapper').show();
			$('#response-recipient').remove();
		},
		
		this.notifyAddNewFolder = function(folder){
			$(".closePopup").trigger('click');
			$('#inbox').trigger('click');
		},
		
		this.notifyDeleteFolder = function(){
			$('#inbox').trigger('click');
		},
		
		this.changeFolder = function(){
			$('.backgroundPopup').hide();
            $(".popupChangeFolder").hide();
            $('#receptionFoder').trigger('click');
		},
		
		this.notifyDeleteConversation = function(){
			$('.backgroundPopup').hide();
			$('.eds-dialog-container').hide();
            $('#receptionFoder').trigger('click');
		},
		
		this.notifyArchivedConversation = function(){
			$('.backgroundPopup').hide();
			$('.eds-dialog-container').hide();
            $('#receptionFoder').trigger('click');
		},
		
		this.loadConversationByFolderId = function(folderId){
			$.each(listeners, function(i){
				listeners[i].loadConversationByFolderId(folderId);
			});
		},
		
		this.loadArchivedConversation = function(){
			$.each(listeners, function(i){
				listeners[i].loadArchivedConversation();
			});
		},
		
		this.markAsRead = function(conversationId){
			$.each(listeners, function(i){
				listeners[i].markAsRead(conversationId);
			});
		},

		this.sendReply = function(message,conversationId){
			$.each(listeners, function(i){
				listeners[i].sendReply(message,conversationId);
			});
		},
		
		this.deleteFolder = function(IdFolder){
			$.each(listeners, function(i){
				listeners[i].deleteFolder(IdFolder);
			});
		},
		
		this.addNewFolder = function(folder){
			$.each(listeners, function(i){
				listeners[i].addNewFolder(folder);
			});
		},

		this.changeFolder = function(idFolder, idConversationList){
			$.each(listeners, function(i){
				listeners[i].changeFolder(idFolder,idConversationList);
			});
		},

		this.deleteConversation = function(idConversationList){
			$.each(listeners, function(i){
				listeners[i].deleteConversation(idConversationList);
			});
		},

		this.archiveConversation = function(idConversationList){
			$.each(listeners, function(i){
				listeners[i].archiveConversation(idConversationList);
			});
		},

		this.addListener = function(list){
			listeners.push(list);
		};
		
	},
	
	
	ViewListener: function(list) {
		if(!list) list = {};
		return $.extend(list);
	}
	
});

