jQuery.extend({

	QuestionInfoController: function(model, view){
		//var that = this;
		/**
		 * listen to the view
		 */
		var vlist = $.QuestionInfoViewListener({
			loadQuestionInfos : function(questionInfos){
				view.displayQuestionInfos(questionInfos);
			},
			loadProducts : function(products){
				view.displayProducts(products);
			},
			responseQuestionInfoSuccess : function(){
				view.responseQuestionInfoSuccess();
			},
			readQuestionInfoSuccess : function(index){
				view.readQuestionInfoSuccess(index);
			},
		});
		view.addListener(vlist);

		/**
		 * listen to the model
		 */
		var mlist = $.QuestionInfoModelListener({		
			getQuestionInfos : function(){
				model.getQuestionInfos();
			},
			responseQuestionInfo : function(questionInfo){
				model.responseQuestionInfo(questionInfo);
			},
			readQuestionInfo : function(questionInfoId, index){
				model.readQuestionInfo(questionInfoId, index);
			},
		});
		
		model.addListener(mlist);
		
		this.initController = function(){
			view.initView();
			model.initModel();
		};
	}

});
