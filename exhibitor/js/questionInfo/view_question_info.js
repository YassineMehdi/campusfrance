
jQuery.extend({
	QuestionInfoView: function(){
		var that = this;
		var listeners = new Array();
		var questionInfosList = [];
		that.questionInfoId = 0;
		that.resourceId = 0;
		
		this.initView = function(){
		    traduct();
			$("#questionInfosContent .draggable").draggable({
				cancel : ".eds-dialog-inside"
			});
			$('#questionInfosContent .closePopup, #questionInfosContent .eds-dialog-close').click(function() {
				hidePopups();
			});
			$("#sendResponseQuestionInfo").unbind("click").bind("click", function() {
				if($("#responseQuestionInfoForm").valid())
					that.sendResponseQuestionInfo();
			});
			$("#questionInfoSearchDiv #productSelect").unbind("change").bind("change", 
					that.filterQuestionInfo);
			that.initQuestionInfosTable();
			that.validateQuestionInfoForm();
		};
		this.initQuestionInfosTable = function(){
			$('#listQuestionInfosTable')
			.dataTable(
					{
						"aaData" : [],
						"aoColumns" : [ {
							"sTitle" : FIRSTANDLASTNAMELABEL,
							"sWidth" : "30%",
						},{
							"sTitle" : TITLELABEL,
							"sWidth" : "35%",
						}, {
							"sTitle" : ACTIONSLABEL,
							"sWidth" : "35%",
						}, {
							"sTitle" : "",
							"sWidth" : "0%"
						}, {
							"sTitle" : "",
							"sWidth" : "0%"
						} ],
						"aaSorting" : [ [ 3, "asc" ] ],
						"aoColumnDefs" : [ {
							"bVisible" : false,
							"aTargets" : [ 3, 4 ]
						} ],
						"bAutoWidth" : false,
						"bRetrieve" : false,
						"bDestroy" : true,
						"iDisplayLength" : 10,
						"fnDrawCallback" : function() {
							$('#listQuestionInfosTable .readCandidate').unbind('click');
							// $('#listQuestionInfosTable .viewProfile').bind('click',
							// 		function() {
							// 			that.viewProfileHandler($(this).attr("index"));
							// 		});
							$('#listQuestionInfosTable .viewLinks').bind('click',
									function() {
										that.viewLinksHandler($(this).attr("index"));
									});
							$('#listQuestionInfosTable .viewLetter').bind('click',
									function() {
										that.viewLetterHandler($(this).attr("index"));
									});
							$('#listQuestionInfosTable .readCandidate').bind('click',
									function() {
										that.readCandidateHandler($(this).attr("index"));
									});
						}
					});
		};
		this.validateQuestionInfoForm = function() {
			$("#responseQuestionInfoForm").validate({
				errorPlacement : function errorPlacement(error, element) {
					element.after(error);
				},
				rules : {
					receiverQuestionInfo : {
						required : true
					},
					responseQuestionInfo : {
						required : true
					}
				}
			});
		};
		this.sendResponseQuestionInfo = function(){
			disabledButton('#sendResponseQuestionInfo');
			$('#loadingEds').show();
			var questionInfo = questionInfosList[that.questionInfoId];
			questionInfo.setResponse($('#questionInfosContent #responseQuestionInfo').val());
			that.notifyResponseQuestionInfo(questionInfo);
		};
		this.viewProfileHandler = function(index){
			var questionInfo = questionInfosList[index];
			if (questionInfo != null) {
				viewUserProfile(questionInfo.getCandidate().getCandidateId());
			}
		};
		this.viewLinksHandler = function(index){
			var questionInfo = questionInfosList[index];
			if (questionInfo != null) {
				var urls = questionInfo.getQuestionInfoUrl().split(',');
				for(var index in urls){
					window.open(urls[index], '_blank');
				}
			}
		};
		this.viewLetterHandler = function(index){
			var questionInfo = questionInfosList[index];
			if (questionInfo != null) {
				that.questionInfoId = questionInfo.getQuestionInfoId();
				$('#questionInfosContent #letterContentQuestionInfo').text(questionInfo.getQuestion());
				enabledButton('#questionInfosContent #sendResponseQuestionInfo');
				$('.popupResponseQuestionInfo').hide();
				$("#questionInfosContent .popupMotivationLetterQuestionInfo, .backgroundPopup").show();
				$('#questionInfosContent #letterQuestionInfoReply').unbind('click').bind('click', function() {
					$('#questionInfosContent .popupMotivationLetterQuestionInfo').hide();
					$('#questionInfosContent #subjectQuestionInfo').val(questionInfo.getQuestionInfoTitle());
					$('#questionInfosContent #responseQuestionInfo').val(questionInfo.getResponse());
					$('#questionInfosContent #receiverQuestionInfo').val(questionInfo.getCandidate().getFullName());
					$('#questionInfosContent .popupResponseQuestionInfo').show();
				});
			}
		};
		this.readCandidateHandler = function(index){
			var questionInfo = questionInfosList[index];
			if (questionInfo != null && questionInfo.getUnread()) {
				$('#loadingEds').show();
				that.notifyReadQuestionInfo(questionInfo.getQuestionInfoId(), index);
			}
		};
		this.filterQuestionInfo = function(e){
			var questionInfos = [];
	    	var productId = $("#questionInfoSearchDiv #productSelect").val();
	    	for (var index in questionInfosList) {
				var questionInfo = questionInfosList[index];
				if(productId == 0 || questionInfo.getProduct().getProductId() == productId) 
					questionInfos.push(questionInfo);
			}
	    	that.listingQuestionInfos(questionInfos);
		};
		this.displayQuestionInfos = function(questionInfos){
			questionInfosList = questionInfos;
			that.listingQuestionInfos(questionInfos);
		};
		this.listingQuestionInfos = function(questionInfos) {
			var datas = new Array();
			var questionInfo = null;
			var questionInfoId = null;
			var fullName = null;
			var question = null;
//			var response = null;
			var questionInfoUrl = null;
			var questionInfoTitle = null;
			var btnCv = null;
			//var btnProfileHandler = $("<button class='btn btn-danger btn-middle readCandidate viewProfile'/>");
			var btnCvHandler = $("<button class='btn btn-info btn-middle readCandidate viewLinks'/>");
			var btnLetterHandler = $("<button class='btn btn-success btn-middle readCandidate viewLetter'/>");
			var newHandler = $('<div class="label label-important not-read-candidate"></div>');
			var listActions = $("<div></div>");
			//btnProfileHandler.text(VIEWPROFILELABEL);
			btnCvHandler.text(VIEW_LINKS_LABEL);
			btnLetterHandler.text(VIEWLETTERLABEL);
			newHandler.text(NEWLABEL);
			for (var index in questionInfos) {
				questionInfo = questionInfos[index];
				questionInfoId = questionInfo.getQuestionInfoId();
				fullName = questionInfo.getCandidate().getFullName();
				questionInfoTitle = questionInfo.getQuestionInfoTitle();
				question = questionInfo.getQuestion();
				response = questionInfo.getResponse();
				questionInfoUrl = questionInfo.getQuestionInfoUrl();
				question = substringCustom(formattingLongWords(question, 70), 200);
				//btnProfileHandler.attr("index", index);
				btnLetterHandler.attr("index", index);
				if(questionInfo.getUnread()){
					fullName = getHtml(newHandler.attr("id", "notReaded_"+questionInfoId)) + fullName;
				}
				if(questionInfoUrl!=null && questionInfoUrl.trim()!=''){
					btnCvHandler.attr("index", index);
					btnCv = getHtml(btnCvHandler);
				}else btnCv = null;
				listActions.html(//getHtml(btnProfileHandler)+ 
						getHtml(btnCv)
						+ getHtml(btnLetterHandler));
				datas.push([ fullName, questionInfoTitle, getHtml(listActions), 
				             questionInfo.getQuestionInfoId(), question]);
			}
			addTableDatas('#listQuestionInfosTable', datas);
			$('#loadingEds').hide();
		};

		this.displayProducts = function(products){
			var productSelectHandler=$('#questionInfoSearchDiv #productSelect');
			var product = null;
			productSelectHandler.empty();
			productSelectHandler.append($('<option/>').val(0).html(ALLLABEL));
			products.sort(function(a, b) {
				if(a.getProductTitle() > b.getProductTitle()) {
					return 1;
				}else if(a.getProductTitle() < b.getProductTitle()){
					return -1;
				}else {
					return 0;
				}
			});
			for (var index in products) {
					product = products[index];
					productSelectHandler.append($('<option/>').val(product.getProductId()).html(substringCustom(product.getProductTitle(), 50)));
			}
		};
		
		this.responseQuestionInfoSuccess = function(){
			enabledButton('#saveQuestionInfoBtn');
			hidePopups();
		};
		
		this.readQuestionInfoSuccess = function(index){
			var questionInfo = questionInfosList[index];
			if (questionInfo != null) {
				$('#listQuestionInfosTable #notReaded_'+questionInfo.getQuestionInfoId()).hide();
				questionInfo.setUnread(false);
				hidePopups();
			}
		};
		
		this.deleteResourceSuccess = function(){
			enabledButton('#deleteResourceBtn');
			$("#popupDeleteResource").hide();
		};
		
		this.notifGetQuestionInfos = function(){
			$.each(listeners, function(i) {
				listeners[i].getQuestionInfos();
			});
		};
		
		this.notifyResponseQuestionInfo = function(questionInfo){
			$.each(listeners, function(i){
				listeners[i].responseQuestionInfo(questionInfo);
			});
		};
		
		this.notifyReadQuestionInfo = function(questionInfoId, index){
			$.each(listeners, function(i){
				listeners[i].readQuestionInfo(questionInfoId, index);
			});
		};
		
		this.addListener = function(list){
			listeners.push(list);
		};
		
	},
	
	QuestionInfoViewListener: function(list) {
		if(!list) list = {};
		return $.extend(list);
	}
	
});

