
jQuery.extend({
	
	QuestionInfoModel: function(){
		var that = this;
		var listeners = new Array();
		
		this.initModel = function(){
			that.getQuestionInfos();
			that.getProducts();
		};
		
		this.getQuestionInfosSuccess = function(data){
			var questionInfos = new Array();
			var questionInfo = null;
			$(data).find("questionInfoDTO").each(function() {
				questionInfo = new $.QuestionInfo($(this));
				questionInfos[questionInfo.getQuestionInfoId()] = questionInfo;
			});
			that.notifyLoadQuestionInfos(questionInfos);
		};
		
		this.getQuestionInfosError = function(xhr, status, error){
		};
		
		this.getQuestionInfos = function(){
			genericAjaxCall('GET', staticVars.urlBackEnd+'questionInfo/get?idLang='+getParam('idLang'), 'application/xml', 'xml', 
					'', that.getQuestionInfosSuccess, that.getQuestionInfosError);
		};
		
		this.getProductsSuccess = function(data){
			var products = new Array();
			var product = null;
			$(data).find("productDTO").each(function() {
				product = new $.Product($(this));
				products[product.getProductId()] = product;
			});
			that.notifyLoadProducts(products);
		};
		
		this.getProductsError = function(xhr, status, error){
		};
		
		this.getProducts = function(){
			genericAjaxCall('GET', staticVars.urlBackEnd+'product?idLang='+getParam('idLang'), 'application/xml', 'xml', 
					'', that.getProductsSuccess, that.getProductsError);
		};
		
		this.responseQuestionInfoSuccess = function(data){
			that.notifyResponseQuestionInfoSuccess();
		};
		
		this.responseQuestionInfoError = function(xhr, status, error){
		};
		
		this.responseQuestionInfo = function(questionInfo){
			genericAjaxCall('POST', staticVars.urlBackEnd+'questionInfo/response?idLang='+getParam('idLang'), 'application/xml', 'xml', 
					questionInfo.xmlData(), that.responseQuestionInfoSuccess, that.responseQuestionInfoError);
		};
		
		this.readQuestionInfoSuccess = function(data, index){
			//that.getQuestionInfos();
			that.notifyReadQuestionInfoSuccess(index);
		};
		
		this.readQuestionInfoError = function(xhr, status, error){
		};
		
		this.readQuestionInfo = function(questionInfoId, index){
			genericAjaxCall('POST', staticVars.urlBackEnd+'questionInfo/read?idLang='+getParam('idLang')+"&questionInfoId="+questionInfoId, 
					'application/xml', 'xml', '', function(data){
									that.readQuestionInfoSuccess(data, index);
					}, that.readQuestionInfoError);
		};
		
		this.addListener = function(list){
			listeners.push(list);
		};

		this.notifyLoadQuestionInfos = function(questionInfos) {
			$.each(listeners, function(i) {
				listeners[i].loadQuestionInfos(questionInfos);
			});
		};

		this.notifyLoadProducts = function(products) {
			$.each(listeners, function(i) {
				listeners[i].loadProducts(products);
			});
		};

		this.notifyResponseQuestionInfoSuccess = function() {
			$.each(listeners, function(i) {
				listeners[i].responseQuestionInfoSuccess();
			});
		};
		
		this.notifyReadQuestionInfoSuccess = function(index) {
			$.each(listeners, function(i) {
				listeners[i].readQuestionInfoSuccess(index);
			});
		};
		
	},
		
	QuestionInfoModelListener: function(list) {
		if(!list) list = {};
		return $.extend(list);
	},
	
});

