jQuery.extend({

	SurveyController: function(model, view){
		var that = this;
		/**
		 * listen to the view
		 */
		var vlist = $.ViewListener({
			notifyUpdateSurvey : function(form) {
				model.updateSurvey(form);
			},
			deleteSurvey : function(surveyId) {
				model.deleteSurvey(surveyId);
			},
		});
		view.addListener(vlist);

		/**
		 * listen to the model
		 */
		var mlist = $.ModelListener({			
			notifyLoadSurvey : function(surveys) {
				view.initSurveys(surveys);
			},		
			notifySurveyDelete : function() {
				view.notifySurveyDelete();
			},
		});
		
		model.addListener(mlist);
		
		this.initController = function(){
			view.initView();
			model.initModel();
		};
	}
});