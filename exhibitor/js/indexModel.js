jQuery.extend({

	IndexModel : function() {
		var listeners = new Array();
		var that = this;
		this.addListener = function(list) {
			listeners.push(list);
		};
		this.initModel = function(){
			//that.getSearchProfiles();
			//that.getStandVisitors();
			//that.getSkypeMailTemplate();
			that.getConnectedVisitors();
		};
		this.beforeInitModel = function(){
			getUserFavorites();
			getActivitySectors();
			getCountries();
			//getRegions();
			//getStudyLevels();
			//getFunctionsCandidate();
			//getExperienceYears();
			//getAreaExpertises();
			//getJobsSought();
			//getCandidateStates();
		};
		that.beforeInitModel();
		this.objToString = function(obj) {
		    var str = '';
		    for (var p in obj) {
		        if (obj.hasOwnProperty(p)) {
		            str += p + '::' + obj[p] + '\n';
		        }
		    }
		    return str;
		};
		this.notifyGetCandidatesByCVKeywords = function(candidates) {
			$.each(listeners, function(i) {
				listeners[i].displayGetCandidatesByCVKeywords(candidates);
			});
		};
		this.getCandidatesByCVKeywordsSuccess = function(xml) {
			candidates = new Array();
			$(xml).find("candidate").each(function() {
				var candidate = new $.Candidate($(this));
				candidates[candidate.getCandidateId()] = candidate;
			});
			that.notifyGetCandidatesByCVKeywords(candidates);
		};
		this.getCandidatesByCVKeywords = function(cvKeywordValue) {
			genericAjaxCall("GET", staticVars["urlBackEnd"] + 'cvkeyword/getCandidateByCVKeywordValue?cvKeywordValue='+cvKeywordValue, 'application/xml', 'xml', '', that.getCandidatesByCVKeywordsSuccess, '');
		};
		this.getCandidateListFromXML = function(xml) {
			candidates = new Array();
			$(xml).find("candidateDTO").each(function() {
				var candidate = new $.Candidate($(this));
				candidates[candidate.getCandidateId()] = candidate;
			});
			return candidates;
		};
		this.getExhibitorsListFromXML = function(xml) {
			var exhibitors = new Array();
			$(xml).find("enterprisePDTO").each(function() {
				var enterpriseP = new $.EnterpriseP($(this));
				exhibitors[enterpriseP.getEnterprisePId()] = enterpriseP;
			});
			return exhibitors;
		};
		this.getForumEnrolledSuccess = function(xml) {
			candidates = that.getCandidateListFromXML(xml);
			that.getUsersConnectedForum(candidates);
		};
		this.getFavoriteUsersSuccess = function(xml) {
			that.notifyLoadFavoriteUsers(that.getCandidateListFromXML(xml));
		};
		this.getMyVisitCardsCandidateSuccess = function(xml) {
			that.getMyVisitExhibitors(that.getCandidateListFromXML(xml));
		};
		this.getMyVisitCardsExhibitorSuccess = function(xml, candidates) {
			that.notifyLoadMyVisitCards(candidates, that.getExhibitorsListFromXML(xml));
		};
		this.getExhibitorsSuccess = function(xml) {
			that.notifyLoadExhibitors(that.getExhibitorsListFromXML(xml));
		};
		this.getForumEnrolled = function() {
			genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'candidate/getCandidatesInfo?idLang='+getIdLangParam(), 'application/xml', 'xml', '', that.getForumEnrolledSuccess, '');
		};
		this.getFavoriteUsers = function() {
			genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'candidate/getCandidatesInfo?idLang='+getIdLangParam(), 'application/xml', 'xml', '', that.getFavoriteUsersSuccess, '');
		};
		this.getSearchProfilesSuccess = function(xml) {
			that.notifyLoadSearchProfiles(that.getCandidateListFromXML(xml));
		};
		this.getSearchProfiles = function() {
			genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'candidate/getCandidatesInfo?idLang='+getIdLangParam(), 'application/xml', 'xml', '', that.getSearchProfilesSuccess, '');
		};
		this.getVisitCardsSuccess = function(xml) {
			var listFavorites = [];
			var favorite = null;
			$(xml).find('favorite').each(function () {
					favorite = new $.Favorite(this);
					listFavorites.push(favorite);
			});
			that.notifyLoadListVisitCards(listFavorites);
		};
		this.getVisitCardsError = function(xhr, status, error) {
			isSessionExpired(xhr.responseText);
		};
		this.getMyVisitCards = function() {
			genericAjaxCall('GET', staticVars["urlBackEnd"] + 'favorite/userFavoritesVisitor?idLang=' + getIdLangParam(), 
					'application/xml', 'xml', '', 
					that.getVisitCardsSuccess, that.getVisitCardsError);
		};
		this.getExhibitors = function() {
			genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'enterprisep/get/visitors?idLang='+getIdLangParam(), 'application/xml', 'xml', '', that.getExhibitorsSuccess, '');
		};
		this.getMyVisitExhibitors = function(candidates) {
			genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'enterprisep/get/visitors?idLang='+getIdLangParam(), 'application/xml', 'xml', '', 
					function(xml){
						that.getMyVisitCardsExhibitorSuccess(xml, candidates);
				}, '');
		};
		this.getUsersConnectedForumSuccess = function(xml, candidates) {
			var visitors = new Array();
			$(xml).find("candidate").each(function() {
				var candidate = new $.Candidate($(this));
				visitors[candidate.getCandidateId()] = candidate;
			});
			that.notifyLoadStandVisitors(visitors, candidates);
		};
		this.getUsersConnectedForum = function(candidates){
			genericAjaxCall('GET', staticVars.urlBackEnd+ 'proxyWrapper/GA/get', 'application/json', 'json', {
						dimension : 'ga:eventAction',
						metrics : 'ga:totalEvents',
						filters : 'ga:eventCategory=@Forum/;ga:eventCategory=@/Profile:Candidate',
						startDate : "2016-07-01",
						endDate : "2018-01-01",
						maxResults : 10000
					},
					function(response) {
						if (response.body == 'No entries found') {
						}else {
							var rows = jQuery.parseJSON(response.body);
							visitors = new Array();
							for ( var i = 0, row; row = rows[i]; ++i) {
								visitors.push(row[0].split('/')[0]);
							}
							that.notifyLoadForumEnrolled(visitors, candidates);
						}
					},  function(){
					});
		};
		this.getConnectedUsersSuccess = function(xml, candidates) {
			var visitors = new Array();
			$(xml).find("candidate").each(function() {
				var candidate = new $.Candidate($(this));
				visitors[candidate.getCandidateId()] = candidate;
			});
			that.notifyLoadStandVisitors(visitors, candidates);
		};
		this.getConnectedUsers = function(candidates){
			/*genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'userprofile/getUserConnectedList?standId='+$.cookie('standId'), 'application/xml', 'xml', '', function(xml){
				that.notifyLoadStandVisitors(xml);
			}, '');*/
			genericAjaxCall('GET', staticVars["urlBackEnd"] + 'candidate/standVisitors?idLang='+getIdLangParam(), 'application/xml', 'xml', '', function(xml){
				that.getConnectedUsersSuccess(xml, candidates);
			});
		};
		this.getStandVisitorsSuccess = function(xml) {
			candidates = that.getCandidateListFromXML(xml);
			that.getConnectedUsers(candidates);
		};
		this.getStandVisitorsError = function(xhr, status, error) {
			isSessionExpired(xhr.responseText);
		};
		this.getStandVisitors = function() {
			genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'candidate/getCandidatesInfo?idLang='+getIdLangParam(), 'application/xml', 
					'xml', '', that.getStandVisitorsSuccess, that.getStandVisitorsError);
		};
		this.skypeMailTemplateSuccess = function(xml){
			that.notifSkypeMailTemplateSuccess(xml);
		};
		this.getSkypeMailTemplate = function(){
			genericAjaxCall('GET', staticVars["urlBackEnd"]+'mailTemplate/skype?idLang='+getIdLangParam(), 'application/xml', 
					'xml', '', that.skypeMailTemplateSuccess, that.skypeMailTemplateError);
		};
		this.sendMessageSuccess = function(xml){
			$(".backgroundPopup").hide();
			messageSent();
		};
		this.sendMessageError = function(xhr,status,error){
			isSessionExpired(xhr.responseText);
		};
		this.sendMessage = function(message, candidateId){
			genericAjaxCall('POST', staticVars["urlBackEnd"]+'message/sendMessage?candidateId='+candidateId, 'application/xml', 
					'xml', message.xmlData(), that.sendMessageSuccess, that.sendMessageError);
		};
		this.sendSkypeMessageSuccess = function(xml){
			$(".backgroundPopup").hide();
			messageSent();
		};
		this.sendSkypeMessageError = function(xhr,status,error){
			isSessionExpired(xhr.responseText);
		};
		this.sendSkypeMessage = function(message, candidateId){
			genericAjaxCall('POST', staticVars["urlBackEnd"]+'message/sendSkypeMessage?candidateId='+candidateId+'&idLang='+getIdLangParam(), 'application/xml', 
					'xml', message.xmlData(), that.sendSkypeMessageSuccess, that.sendSkypeMessageError);
		};
		this.sendShareProfileSuccess = function(xml){
			showPopupMessageSent();
		};
		this.sendShareProfileError = function(xhr,status,error){
			isSessionExpired(xhr.responseText);
		};
		this.sendShareProfile = function(email){
			genericAjaxCall('POST', staticVars["urlBackEnd"]+'candidate/sendShareProfile?idLang='+getIdLangParam(), 'application/xml', 
					'xml', email.xmlData(), that.sendShareProfileSuccess, that.sendShareProfileError);
		};
		this.updateCandidateStateSuccess = function(index, candidateStateId){
			that.notifyUpdateCandidateStateSuccess(index, candidateStateId);
		};

		this.updateCandidateStateError = function(xhr, status, error){
		   isSessionExpired(xhr.responseText);
		};
		
		this.updateCandidateState = function(candidateId, candidateStateId, index) {
			genericAjaxCall('POST', staticVars["urlBackEnd"]+' candidate/updateState?candidateId='+candidateId+'&candidateStateId='+candidateStateId+'&idLang='+getIdLangParam(), 
					'application/xml', 'xml','', function(){
					that.updateCandidateStateSuccess(index, candidateStateId);
				}, that.updateCandidateStateError);
		};
		// ------------- Users List -----------//
	    this.getConnectedVisitorsSuccess = function (json, callBack) {
	    	listConnectedUsersCache = [];
	    	$.each(json, function(key, connectedUser){
	    		listConnectedUsersCache.push(connectedUser);
	    	});
	    };
	    this.getConnectedVisitorsError = function (xhr, status, error) {
	      isSessionExpired(xhr.responseText);
	    };
	    this.getConnectedVisitors= function () {
	    	genericAjaxCall('GET', staticVars["urlBackEnd"] + 'chat/get/connectedUsers?idLang=' + getIdLangParam(), 
	      		'application/json', 'json', '', that.getConnectedVisitorsSuccess, that.getConnectedVisitorsError);
	    };
		this.notifyLoadSearchProfiles = function(candidates) {
			$.each(listeners, function(i) {
				listeners[i].displaySearchProfiles(candidates);
			});
		};
		this.notifSkypeMailTemplateSuccess = function(xml){
			$.each(listeners, function(i){
				listeners[i].skypeMailTemplateSuccess(xml);
			});
		};
		this.notifyUpdateCandidateStateSuccess = function(candidateId, candidateStateId){
			$.each(listeners, function(i){
				listeners[i].updateCandidateStateSuccess(candidateId, candidateStateId);
			});
		};
		this.notifyLoadStandVisitors = function(visitors, candidates) {
			$.each(listeners, function(i) {
				listeners[i].displayLoadStandVisitors(visitors, candidates);
			});
		};
		this.notifyLoadForumEnrolled = function(visitors, candidates) {
			$.each(listeners, function(i) {
				listeners[i].displayForumEnrolled(visitors, candidates);
			});
		};
		this.notifyLoadFavoriteUsers = function(candidates) {
			$.each(listeners, function(i) {
				listeners[i].displayFavoriteUsers(candidates);
			});
		};
		this.notifyLoadMyVisitCards = function(candidates, exibitors){
			$.each(listeners, function(i) {
				listeners[i].displayMyVisitCards(candidates, exibitors);
			});
		};
		this.notifyLoadExhibitors = function(exhibitors){
			$.each(listeners, function(i) {
				listeners[i].displayExhibitors(exhibitors);
			});
		};
 		this.notifyLoadListVisitCards = function (listFavorites) {
 	  		$.each(listeners, function (i) {
 	    		listeners[i].loadListVisitCards(listFavorites);
 	  		});
 	  	};
	},

	IndexModelListener : function(list) {
		if (!list)
			list = {};
		return $.extend(list);
	},

});



