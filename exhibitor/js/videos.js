jQuery
		.extend({

			VideosModel : function() {
				var listeners = new Array();
				var that = this;

				this.initModel = function() {
					that.getStandVideos();
				};
				this.getStandVideosSuccess = function(xml) {
					listVideosCache = [];
					$(xml).find("allfiles").each(function() {
						var video = new $.Video($(this));
						listVideosCache[video.idVideo] = video;
					});

					that.notifyLoadVideos(listVideosCache);
				};

				this.getStandVideosError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};

				this.getStandVideos = function() {
					$('#listStandVideos').empty();
					genericAjaxCall('GET', staticVars.urlBackEnd
							+ 'allfiles/standVideos?idLang=' + getIdLangParam(),
							'application/xml', 'xml', '', that.getStandVideosSuccess,
							that.getStandVideosError);

				};

				this.getAnalyticsVideos = function(callBack, path) {
					genericAjaxCall('GET', staticVars.urlBackEnd + path,
							'application/xml', 'xml', '', function(xml) {
								listVideosCache = [];
								$(xml).find("allfiles").each(function() {
									var video = new $.Video($(this));
									listVideosCache[video.idVideo] = video;
								});
								callBack();
							}, that.getStandVideosError);

				};

				this.getVideoById = function(videoId) {
					return listVideosCache[videoId];
				};

				this.addVideoSuccess = function(xml) {
					that.notifyAddVideoSuccess();
				};

				this.addVideoError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};

				this.addVideo = function(video) {
					genericAjaxCall('POST', staticVars.urlBackEnd
							+ 'allfiles/addFile?idLang=' + getIdLangParam(),
							'application/xml', 'xml', video.xmlData(), that.addVideoSuccess,
							that.addVideoError);
				};

				this.updateVideoSuccess = function(xml) {
					that.notifyUpdateVideoSuccess();
				};

				this.updateVideoError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};

				this.updateVideo = function(video) {
					genericAjaxCall('POST', staticVars.urlBackEnd
							+ 'allfiles/updateFile?idLang=' + getIdLangParam(),
							'application/xml', 'xml', video.xmlData(),
							that.updateVideoSuccess, that.updateVideoError);
				};

				this.deleteVideoSuccess = function(xml) {
					that.notifyDeleteVideoSuccess();
				};

				this.deleteVideoError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};

				this.deleteVideo = function(video) {
					genericAjaxCall('GET', staticVars.urlBackEnd
							+ 'allfiles/deleteMedia/' + video.getIdVideo(),
							'application/xml', 'xml', '', that.deleteVideoSuccess,
							that.deleteVideoSuccess);
				};

				this.notifyLoadVideos = function(listVideos) {
					$.each(listeners, function(i) {
						listeners[i].loadVideos(listVideos);
					});
				};

				this.notifyAddVideoSuccess = function() {
					$.each(listeners, function(i) {
						listeners[i].addVideoSuccess();
					});
				};

				this.notifyUpdateVideoSuccess = function() {
					$.each(listeners, function(i) {
						listeners[i].updateVideoSuccess();
					});
				};

				this.notifyDeleteVideoSuccess = function() {
					$.each(listeners, function(i) {
						listeners[i].deleteVideoSuccess();
					});
				};

				this.addListener = function(list) {
					listeners.push(list);
				};
			},

			VideosModelListener : function(list) {
				if (!list)
					list = {};
				return $.extend(list);
			},

			VideosView : function() {
				var listeners = new Array();
				var idVideoToChange = 0;
				var that = this;
				var videoUrlToChange = "";
				var jqXHRVideo = null;

				this.initView = function() {
					idVideoToChange = 0;
					videoUrlToChange = "";
					jqXHRVideo = null;
					traduct();
					$(".draggable").draggable({
						cancel : ".eds-dialog-inside"
					});
					customJqEasyCounter("#videoTitle", 255, 255, '#0796BE',
							LEFTCARACTERES);
					customJqEasyCounter("#videoUrl", 255, 255, '#0796BE',
							LEFTCARACTERES);
					customJqEasyCounter("#videoDescription", 65535, 65535, '#0796BE',
							LEFTCARACTERES);
					$("#videoForm td.jqEasyCounterMsg").css('width', '55%');
					$.validator.addMethod("validUrl", function(value, element) {
						var linkUrl = $("#videoUrl").val();
						if ((compareToLowerString(linkUrl, "youtube.com") !== -1)
								|| (compareToLowerString(linkUrl, "dailymotion.com") !== -1)
								|| (compareToLowerString(linkUrl, "vimeo.com") !== -1)) {
							return true;
						} else {
							return false;
						}
					}, traductLabel("enterValidLinkLabel"));

					$("#videoForm").validate({
						rules : {
							videoUrl : {
							// validUrl : true,
							// custom_url : true
							}
						}
					});
					$('#addVideoLink').click(
							function() {
								$('.popupVideo .editLabel').addClass('addLabel').removeClass(
										'editLabel').text(ADDLABEL);
								that.initPopupAddVideo('', '', 'youtube', '');
								$('#videoLoaded,#progressVideo').hide();
								$('#loadVideo, .popupVideo, .backgroundPopup').show();
								enabledButton('.saveVideoButton');
							});

					$('#videoUrl').addClass('required');
					$('#urlOption').click(function() {
						$('#urlTr').show();
						$('#uploadTr').hide();
						$('#videoUrl').addClass('required');
						$('#loadVideoError,#loadVideo').hide();
						enabledButton('.saveVideoButton');
						if (jqXHRVideo != null)
							jqXHRVideo.abort();
						/*
						 * $('#videoUrl').rules('add', 'custom_url');
						 * $('#videoUrl').rules('add', 'validUrl');
						 */
					});
					$('#uploadOption').click(function() {
						$('#urlTr').hide();
						$('#uploadTr').show();
						$('#videoUrl').removeClass('required');
						$('#loadVideoError,#progressVideo').hide();
						$('#loadVideo').show();
						/*
						 * var settings = $('#videoForm').validate().settings; delete
						 * settings.rules.videoUrl;
						 */
						enabledButton('.saveVideoButton');
					});

					$('.saveVideoButton').click(function() {
						if ($('#videoForm').valid() && !isDisabled('.saveVideoButton')) {
							disabledButton('.saveVideoButton');
							if ($("#videoForm input:checked").attr('id') == "urlOption") {
								videoUrlToChange = $('#videoUrl').val();
							}
							if (videoUrlToChange != '') {
								$('#loadingEds').show();
								if (idVideoToChange != 0) {
									that.updateVideo(idVideoToChange);
								} else
									that.addVideo();
							} else {
								$('#loadVideoError').show();
								enabledButton('.saveVideoButton');
							}
						}
					});
					$('.closePopup,.eds-dialog-close').click(function(e) {
						e.preventDefault();
						$('.eds-dialog-container').hide();
						$('.backgroundPopup').hide();
						$('#playerVideoPopup').empty();
						if (jqXHRVideo != null)
							jqXHRVideo.abort();
					});

					$('.deleteVideoButton').click(function() {
						$('#loadingEds').show();
						disabledButton('.deleteVideoButton');
						that.deleteVideo(idVideoToChange);
					});
					$('#videoUpload').fileupload(
							{
								beforeSend : function(jqXHR, settings) {
									beforeUploadFile('.saveVideoButton', '#progressVideo',
											'#loadVideo,#loadVideoError');
								},
								datatype : 'json',
								cache : false,
								add : function(e, data) {
									var goUpload = true;
									var uploadFile = data.files[0];
									var fileName = uploadFile.name;
									timeUploadVideo = (new Date()).getTime();
									data.url = getUrlPostUploadFile(timeUploadVideo, fileName);
									if (!(/\.(flv|mp4|mov|m4v|f4v|wmv)$/i).test(fileName)) {
										goUpload = false;
									}
									if (uploadFile.size > MAXFILESIZE) {
										goUpload = false;
									}
									if (goUpload == true) {
										jqXHRVideo = data.submit();
									} else
										$('#loadVideoError').show();
								},
								progressall : function(e, data) {
									progressBarUploadFile('#progressVideo', data, 100);
								},
								done : function(e, data) {
									var file = data.files[0];
									var fileName = file.name;
									var hashFileName = getUploadFileName(timeUploadVideo,
											fileName);
									// console.log("Add video done, filename : "+fileName+",
									// hashFileName : "+hashFileName);
									videoUrlToChange = getUploadFileUrl(hashFileName);
									afterUploadFile('.saveVideoButton', '#videoLoaded',
											'#progressVideo');
								}
							});
				};
				this.initPopupAddVideo = function(videoTitle, videoDescription,
						videoType, videoUrl) {
					idVideoToChange = 0;
					videoUrlToChange = '';
					$('#videoTitle').val(videoTitle);
					$('#videoDescription').val(videoDescription);
					$("#videoType").val(videoType);
					$('#videoUrl').val(videoUrl);
					$('#urlOption').click();

				};
				this.factoryClickUrlOption = function(videoType, videoUrl) {
					$('#urlOption').click();
					$("#videoType").val(videoType);
					$('#videoUrl').val(videoUrl);
				};
				this.addListener = function(list) {
					listeners.push(list);
				};

				this.displayVideos = function(listVideos) {
					var rightGroup = listStandRights[rightGroupEnum.VIDEOS];
					if (rightGroup != null) {
						var permittedVideosNbr = rightGroup.getRightCount();
						var videosNbr = Object.keys(listVideos).length;
						if (permittedVideosNbr == -1) {
							$('#addVideoDiv #addVideoLink').text(ADDLABEL);
							$('#addVideoDiv').show();
						} else if (videosNbr < permittedVideosNbr) {
							$('#numberVideos').text(videosNbr);
			        		$('#numberPermittedVideos').text(permittedVideosNbr);
							$('#addVideoDiv').show();
						} else {
							$('#addVideoDiv').hide();
						}
					}
					var video = null;
					for ( var index in listVideos) {
						video = listVideos[index];
						var videoId = video.getIdVideo();
						var videoUrl = video.getVideoUrl();
						var videoTitle = video.getVideoTitle();
						var videoDescription = video.getVideoDescription();
						var data = {
							videoTitle : replaceSpecialChars(formattingLongWords(videoTitle,
									50)),
							videoDescription : replaceSpecialChars(videoDescription),
							videoId : replaceSpecialChars(videoId),
							videoUrl : replaceSpecialChars(videoUrl),
							viewLabel : VIEWLABEL,
							editLabel : EDITLABEL,
							deleteLabel : DELETELABEL
						};
						$('#standVideoTemplate').tmpl(data).appendTo('#listStandVideos');
						$('.deleteVideo' + videoId).bind('click', function() {
							idVideoToChange = $(this).closest('.oneVideo').attr('id');
							$('.popupDeleteVideo,.backgroundPopup').show();
							enabledButton('.deleteVideoButton');
						});
						$('.editVideo' + videoId)
								.bind(
										'click',
										function() {
											idVideoToChange = $(this).closest('.oneVideo').attr('id');
											var video = listVideos[idVideoToChange];
											var videoUrl = video.getVideoUrl();
											var videoTitle = video.getVideoTitle();
											var videoDescription = video.getVideoDescription();
											$('.popupVideo .addLabel').addClass('editLabel')
													.removeClass('addLabel').text(EDITLABEL);
											$('#videoTitle').val(videoTitle);
											$('#videoDescription').val(videoDescription);
											if (compareToLowerString(videoUrl, "youtube") !== -1) {
												that.factoryClickUrlOption("youtube", videoUrl);
											} else if (compareToLowerString(videoUrl, "dailymotion") !== -1) {
												that.factoryClickUrlOption("dailymotion", videoUrl);
											} else if (compareToLowerString(videoUrl, "vimeo") !== -1) {
												that.factoryClickUrlOption("vimeo", videoUrl);
											} else {
												videoUrlToChange = videoUrl;
												$('#uploadOption').click();
											}
											enabledButton('.saveVideoButton');
											$('#videoLoaded,#loadVideoError,#progressVideo').hide();
											$('#loadVideo,.popupVideo,.backgroundPopup').show();
										});
						$('.viewVideo' + videoId).on('click', function() {
							var videoId = $(this).closest('.oneVideo').attr('id');
							var video = listVideos[videoId];
							var videoTitle = video.getVideoTitle();
							var videoUrl = video.getVideoUrl();
							playVideoPopup(videoUrl, videoTitle);
						});
					}
					$('#loadingEds').hide();
				};

				this.addVideoSuccess = function() {
					hidePopups();
					showUpdateSuccess();
					that.notifyVideoUpdated();
					// enabledButton('.saveVideoButton');
				};
				this.updateVideoSuccess = function() {
					hidePopups();
					showUpdateSuccess();
					that.notifyVideoUpdated();
					// enabledButton('.saveVideoButton');
				};
				this.deleteVideoSuccess = function() {
					hidePopups();
					showUpdateSuccess();
					that.notifyVideoUpdated();
					// enabledButton('.deleteVideoButton');
				};
				this.addVideo = function() {
					var videoTitle = $('#videoTitle').val();
					var videoDescription = $('#videoDescription').val();
					var video = new $.Video();
					video.setVideoTitle(videoTitle);
					video.setVideoDescription(videoDescription);
					video.setVideoUrl(videoUrlToChange);
					var fileType = new $.FileType();
					fileType.setFileTypeId(fileTypeEnum.VIDEO);
					video.setFileType(fileType);
					that.notifyAddVideo(video);
				};
				this.updateVideo = function(videoId) {
					var videoTitle = $('#videoTitle').val();
					var videoDescription = $('#videoDescription').val();
					var video = listVideosCache[videoId];
					video.setVideoTitle(videoTitle);
					video.setVideoDescription(videoDescription);
					video.setVideoUrl(videoUrlToChange);
					that.notifyUpdateVideo(video);
				};

				this.deleteVideo = function(videoId) {
					var video = listVideosCache[videoId];
					that.notifyDeleteVideo(video);
				};
				this.notifyAddVideo = function(video) {
					$.each(listeners, function(i) {
						listeners[i].addVideo(video);
					});
				};
				this.notifyUpdateVideo = function(video) {
					$.each(listeners, function(i) {
						listeners[i].updateVideo(video);
					});
				};
				this.notifyDeleteVideo = function(video) {
					$.each(listeners, function(i) {
						listeners[i].deleteVideo(video);
					});
				};
				this.notifyVideoUpdated = function() {
					$.each(listeners, function(i) {
						listeners[i].videoUpdated();
					});
				};

			},

			VideosViewListener : function(list) {
				if (!list)
					list = {};
				return $.extend(list);
			},

			VideosController : function(model, view) {
				/**
				 * listen to the model
				 */
				var videosModelList = new $.VideosModelListener({
					loadVideos : function(listVideos) {
						view.displayVideos(listVideos);
					},
					addVideoSuccess : function() {
						view.addVideoSuccess();
					},
					updateVideoSuccess : function() {
						view.updateVideoSuccess();
					},
					deleteVideoSuccess : function() {
						view.deleteVideoSuccess();
					},
				});
				model.addListener(videosModelList);
				/**
				 * listen to the view
				 */
				var videosViewList = new $.VideosViewListener({
					videoUpdated : function() {
						model.getStandVideos();
					},
					addVideo : function(video) {
						model.addVideo(video);
					},
					updateVideo : function(video) {
						model.updateVideo(video);
					},
					deleteVideo : function(video) {
						model.deleteVideo(video);
					},
				});
				view.addListener(videosViewList);

				this.initController = function() {
					view.initView();
					model.initModel();
				};
				this.getAnalyticsVideos = function(callBack, path) {
					model.getAnalyticsVideos(callBack, path);
				};
				this.getVideoById = function(videoId) {
					return model.getVideoById(videoId);
				};
			}
		});
