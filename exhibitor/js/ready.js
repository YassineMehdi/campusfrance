///*
// * This file contains all js code we are calling when document is ready
// */
//
$(document)
		.ready(
				function() {
					var rightGroup = null;
					$('body #tmps').load("tmps.html");
					$("select").selectpicker({});
					currentPage = PageEnum.BACKEND;
					getAllLangagesDBinit();
					getLanguagePath(getIdLangParam());
					setInterval(sessionExtender, 600000); // 10min
					getUserFavorites();

					getStandRights(function() {
						if ($.cookie('isInbox') != null) {
							setTimeout(function() {
								showTabNotChat('inbox.html', $('.inbox'));
								hideTabChat();
								$.cookie("isInbox", null);
							}, 1000);
						} else if ($.cookie('isAgenda') != null) {
							setTimeout(function() {
								showTabNotChat('agenda.html', $('.agenda'));
								hideTabChat();
								$.cookie("isAgenda", null);
							}, 1000);
						} else {
							showTabNotChat('stand.html', $('.profile'));
							hideTabChat();
						}
						rightGroup = listStandRights[rightGroupEnum.EMAILING];
						if (rightGroup != null && rightGroup.getRightCount() != 0) {
							$('.emailing').click(function() {
								showTabNotChat('emailing.html', $(this));
								hideTabChat();
							});
						} else {
							$('.emailing').hide();
						}
						rightGroup = listStandRights[rightGroupEnum.ENTERPRISES];
						if (rightGroup != null && rightGroup.getRightCount() != 0) {
							$('.enterprises').click(function() {
								showTabNotChat('enterprises.html', $(this));
								hideTabChat();
							});
						} else {
							$('.enterprises').hide();
						}
						rightGroup = listStandRights[rightGroupEnum.PRODUCTS];
						if (rightGroup != null && rightGroup.getRightCount() != 0) {
							$('.products').click(function() {
								showTabNotChat('products.html', $(this));
								hideTabChat();
							});
						} else {
							$('.products').hide();
						}
						rightGroup = listStandRights[rightGroupEnum.ADVICES];
						if (rightGroup != null && rightGroup.getRightCount() != 0) {
							$('.advices').click(function() {
								showTabNotChat('advices.html', $(this));
								hideTabChat();
							});
						} else {
							$('.advices').hide();
						}
						rightGroup = listStandRights[rightGroupEnum.INBOX];
						if (rightGroup != null && rightGroup.getRightCount() != 0) {
							$('.inbox').click(function() {
								showTabNotChat('inbox.html', $(this));
								hideTabChat();
							});
						} else {
							$('.inbox').hide();
						}
						rightGroup = listStandRights[rightGroupEnum.FORUM_DASHBOARD];
						if (rightGroup == null || rightGroup.getRightCount() == 0)
							rightGroup = listStandRights[rightGroupEnum.STAND_DASHBOARD];
						if (rightGroup != null && rightGroup.getRightCount() != 0) {
							$('.dashboard').click(function() {
								showTabNotChat('dashboard.html', $(this));
								hideTabChat();
							});
						} else {
							$('.dashboard').hide();
						}
						rightGroup = listStandRights[rightGroupEnum.QUESTION_INFO];
						if (rightGroup != null && rightGroup.getRightCount() != 0) {
							$('.questionsInfo').click(function() {
								showTabNotChat('questions_info.html', $(this));
								hideTabChat();
							});
						} else {
							$('.questionsInfo').hide();
						}
						rightGroup = listStandRights[rightGroupEnum.REGISTER_VISITORS];
						if (rightGroup == null || rightGroup.getRightCount() == 0)
							rightGroup = listStandRights[rightGroupEnum.FORUM_VISITORS];
						if (rightGroup == null || rightGroup.getRightCount() == 0)
							rightGroup = listStandRights[rightGroupEnum.STAND_VISITORS];
						if (rightGroup == null || rightGroup.getRightCount() == 0)
							rightGroup = listStandRights[rightGroupEnum.FAVORITS_VISITORS];
						if (rightGroup != null && rightGroup.getRightCount() != 0) {
							$('#visitorsSection').click(function() {
								// console.log('visitorsSection');
								showTabNotChat('visitorsSection.html', $(this));
								hideTabChat();
							});
						} else {
							$('#visitorsSection').hide();
						}
						rightGroup = listStandRights[rightGroupEnum.FORUM_PUBLICATIONS];
						if (rightGroup == null || rightGroup.getRightCount() == 0)
							rightGroup = listStandRights[rightGroupEnum.STAND_PUBLICATIONS];
						if (rightGroup != null && rightGroup.getRightCount() != 0) {
							$('.publications').click(function() {
								showTabNotChat('publications.html', $(this));
								hideTabChat();
							});
						} else {
							$('.publications').hide();
						}
					});
					$("#notificationPage").load("notification.html");
					$(".draggable").draggable({
						cancel : ".eds-dialog-inside"
					});
					$('.eds-dialog-close,.backgroundPopup,.eds-dialog-close').click(
							function() {
								hidePopups();
							});
					$('#confirmChangeLanguage').click(
							function() {
								window.location.replace("backend.html?idLang="
										+ $("#languageSelect").val());
							});

					$('#confirmCantDoOperation').click(function() {
						hidePopups();
					});

					$('#logout').click(function() {
						logout();
					});
					$('.profile').click(function() {
						showTabNotChat('stand.html', $(this));
						hideTabChat();
					});
					$('.notifications').click(function() {
						showTabNotChat('notifications.html', $(this));
						hideTabChat();
					});
					$('.prerequisites').click(function() {
						switch (getIdLangParam()) {
							case '3':
								showTabNotChat('prerequisites_fr.html', $(this));
								break;
							case '2':
								showTabNotChat('prerequisites_en.html', $(this));
								break;
							default:
								showTabNotChat('prerequisites_fr.html', $(this));
								break;
						}
						$('#loadingEds').hide();
						hideTabChat();
					});
					$('.account').click(function() {
						showTabNotChat('account.html', $(this));
						hideTabChat();
					});
					$('.chat').click(function() {
						hideTabNotChat();
						showTabChat($(this));
					});
					$('.agenda').click(function() {
						showTabNotChat('agenda.html', $(this));
						hideTabChat();
					});
					
					$('.languagesOptions').click(function() {
						showTabNotChat('languages.html', $(this)); hideTabChat(); 
					});
					 
					$('.backgroundPopup').click(function() {
						$('.eds-dialog-container').hide();
						$('.backgroundPopup').hide();
					});
					$('.seoSection').click(function() {
						showTabNotChat('seo.html', $(this));
						hideTabChat();
					});
					$(document).click(
							function(event) {
								if (($(event.target).parents().index($('.popover')) == -1)
										&& ($(event.target).parents().index(
												$('.iconbar-horizontal')) == -1)) {
									// console.log(($(event.target).parents().index($('.message-container'))
									// == -1));
									$(".popover").slideUp();
								}
							});

					// todo updateVisitorsInfo();
					$('#divChat').load('chat.html');
				});


