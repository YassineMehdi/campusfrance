
jQuery.extend({
	EnterprisesView: function(){
		var that = this;
		var listeners = new Array();
		that.contacts = [];
		that.enterprisePs = [];
		
		this.initView = function(){
			traduct();
			that.initContactsTable();
			$('#loadingEds')
					.show(
							function() {
								$('#popupExportContacts').hide();
								$(".draggable").draggable({
									cancel : ".eds-dialog-inside"
								});
		    });
			$('#exportContactsButton').unbind("click").bind(
				"click",
				function() {
					$('#loadingEds').show(
							function() {
								$('#popupExportContacts, .backgroundPopup').show();
								factoryExportContacts(that.enterprisePs, that.contacts);
								$('#loadingEds').hide();
								$("#popupExportContacts #contactsSortableColumns")
										.sortable().disableSelection();
							});
				});
			$('#popupExportContacts .closePopup, #popupExportContacts .eds-dialog-close').
				unbind("click").bind("click", function(e) {
					e.preventDefault();
					$('.eds-dialog-container').hide();
					$('.backgroundPopup').hide();
				});
		};
		
		this.initContactsTable = function(){
			$('#initContactsTable')
			.dataTable(
					{
						"aaData" : [],
						"aoColumns" : [ {
							"sTitle" : ENTERPRISELABEL,
							"sWidth" : "16%",
						}, {
							"sTitle" : FIRSTNAMELABEL,
							"sWidth" : "16%",
						}, {
							"sTitle" : LASTNAMELABEL,
							"sWidth" : "16%",
						}, {
							"sTitle" : EMAIL_LABEL,
							"sWidth" : "16%"
						}, {
							"sTitle" : CELL_PHONE_LABEL,
							"sWidth" : "16%"
						} , {
							"sTitle" : ACTIVITYSECTORLABEL,
							"sWidth" : "20%"
						} ],
						"aaSorting" : [ [ 0, "asc" ] ],
						"bAutoWidth" : false,
						"bRetrieve" : false,
						"bFilter" : true,
						"bDestroy" : true,
						"iDisplayLength" : 10,
						"fnDrawCallback" : function() {
						}
					});
		};
		
		this.displayEnterprisePs = function(enterprisePs){
			that.enterprisePs = enterprisePs;
			var datas = new Array();
			var enterpriseP = null;
			var userProfile = null;
			for (var index in enterprisePs) {
				enterpriseP = enterprisePs[index];
				userProfile = enterpriseP.getUserProfile();
				datas.push([ enterpriseP.getEnterprise().getNom(), userProfile.getFirstName(), 
				             userProfile.getSecondName(), enterpriseP.getEmail(), enterpriseP.getCellPhone(), enterpriseP.getEnterprise().getNameSectors()]);
			}
			that.addTableDatas('#initContactsTable', datas);
			$('#loadingEds').hide();
		};
		
		this.displayContacts = function(contacts){
			that.contacts = contacts;
			var datas = new Array();
			var contact = null;
			var firstName = null;
			var lastName = null;
			var contactPhone = null;
			var contactArray = null;
			for (var index in contacts) {
				contact = contacts[index];
				firstName = "";
				lastName = "";
				contactName = contact.getContactName();
				contactPhone = contact.getContactPhone();
				if(contactPhone == undefined) contactPhone = "";
				if(contactName != undefined){
					contactArray = contactName.split(" ");
					firstName = contactArray[0];
					lastName = contactArray[1];
					if(lastName == undefined) lastName = "";
				}
				datas.push([ contact.getStand().getName(), firstName, 
				             lastName, contact.getContactEmail(), contactPhone, contact.getStand().getNameSectors()]);
			}
			that.addTableDatas('#initContactsTable', datas);
			$('#loadingEds').hide();
		};
		
		this.addTableDatas = function(selector, datas) {
			var oTable = $(selector).dataTable();
			if (datas.length > 0) {
				oTable.fnAddData(datas);
			}
			oTable.fnDraw();
		};
		
		this.addListener = function(list){
			listeners.push(list);
		};
		
	},
	
	EnterprisesViewListener: function(list) {
		if(!list) list = {};
		return $.extend(list);
	}
	
});

