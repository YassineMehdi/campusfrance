jQuery.extend({

	EnterprisesController: function(model, view){
		//var that = this;
		/**
		 * listen to the view
		 */
		var vlist = $.EnterprisesViewListener({
			loadEnterprisePs : function(enterprisePs){
				view.displayEnterprisePs(enterprisePs);
			},
			loadContacts : function(contacts){
				view.displayContacts(contacts);
			},
		});
		view.addListener(vlist);

		/**
		 * listen to the model
		 */
		var mlist = $.EnterprisesModelListener({		
			sendEmailing : function(email){
				model.sendEmailing(email);
			}
		});
		
		model.addListener(mlist);
		
		this.initController = function(){
			view.initView();
			model.initModel();
		};
	}

});
