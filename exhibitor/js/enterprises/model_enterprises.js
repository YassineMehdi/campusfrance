
jQuery.extend({
	
	EnterprisesModel: function(){
		var that = this;
		var listeners = new Array();
		
		this.initModel = function(){
			that.getEnterprisePs();
			that.getContacts();
		};

		this.getEnterprisePsSuccess = function(data){
			var enterprisePs = new Array();
			var enterpriseP = null;
			$(data).find("enterprisep").each(function() {
				enterpriseP = new $.EnterpriseP($(this));
				enterprisePs[enterpriseP.getEnterprisePId()] = enterpriseP;
			});
			that.notifyLoadEnterprisePs(enterprisePs);
		};
		
		this.getEnterprisePsError = function(xhr, status, error){
		};
		
		this.getEnterprisePs = function(){
			genericAjaxCall('GET', staticVars.urlBackEnd+'enterprisep/get/all?idLang='+getParam('idLang'), 'application/xml', 'xml', 
					'', that.getEnterprisePsSuccess, that.getEnterprisePsError);
		};
		
		this.getContactsSuccess = function(data){
			var contacts = new Array();
			var contact = null;
			$(data).find("contact").each(function() {
				contact = new $.Contact($(this));
				contacts[contact.getContactId()] = contact;
			});
			that.notifyLoadContacts(contacts);
		};
		
		this.getContactsError = function(xhr, status, error){
		};
		
		this.getContacts = function(){
			genericAjaxCall('GET', staticVars.urlBackEnd+'contact/get/all?idLang='+getParam('idLang'), 'application/xml', 'xml', 
					'', that.getContactsSuccess, that.getContactsError);
		};
		
		this.notifyLoadEnterprisePs = function(enterprisePs) {
			$.each(listeners, function(i) {
				listeners[i].loadEnterprisePs(enterprisePs);
			});
		};
		
		this.notifyLoadContacts = function(contacts) {
			$.each(listeners, function(i) {
				listeners[i].loadContacts(contacts);
			});
		};

		this.addListener = function(list){
			listeners.push(list);
		};
	},
		
	EnterprisesModelListener: function(list) {
		if(!list) list = {};
		return $.extend(list);
	},
	
});

