jQuery
		.extend({
			PublicationsModel : function() {
				var listeners = new Array();
				var that = this;

				this.initModel = function() {
					that.getPublications();
				};

				this.getStandPublicationsError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};

				this.getPublicationsSuccess = function(xml) {
					var listPublications = new Array();
					$(xml).find("publication").each(function() {
						var publication = new $.Publication($(this));
						listPublications.push(publication);
					});
					that.notifyLoadPublications(listPublications);
				};

				this.getPublicationsError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};
				this.getPublications = function() {
					genericAjaxCall('GET', staticVars["urlBackEnd"]
							+ 'publication/allPublications?idLang=' + getIdLangParam(),
							'application/xml', 'xml', '', that.getPublicationsSuccess,
							that.getPublicationsError);
				};
				this.notifyLoadPublications = function(listPublications) {
					$.each(listeners, function(i) {
						listeners[i].loadPublications(listPublications);
					});
				};

				this.addListener = function(list) {
					listeners.push(list);
				};
			},

			PublicationsModelListener : function(list) {
				if (!list)
					list = {};
				return $.extend(list);
			},

			PublicationsView : function() {
				var that = this;
				var listeners = new Array();
				var removeOptionVisibilty = false;
				var idPublicationToRemove = null;

				this.addListener = function(list) {
					listeners.push(list);
				};
				this.initView = function() {
					traduct();
					removeOptionVisibilty = false;
					idPublicationToRemove = null;
					$('.updatePublicationButton').click(function() {
						that.updatePublicationsInfo();
					});

					$('.cancelUpdatePublication').click(function() {
						that.notifyGetPublications();
						$('#updatePublicationsDiv').hide();
					});
					$('#addForumPublicationLink').click(function() {
						publicationVisibility = "forum";
						$('#publicationCreationDiv').appendTo('#forumPublicationDiv');
						$('#publicationCreationDiv').show();
					});

					$('#addStandPublicationLink').click(function() {
						publicationVisibility = "stand";
						$('#publicationCreationDiv').appendTo('#standPublicationDiv');
						$('#publicationCreationDiv').show();
					});

					$('.closePopup,.eds-dialog-close').click(function() {
						$('.eds-dialog-container').hide();
						$('.backgroundPopup').hide();
					});
					$("#publicationForm").validate({});
					$('#addPublicationLink').click(function() {
						$("#tempPublication").appendTo("#listPublications");
						$('.removePublicationLink').bind('click', function() {
							$(this).parent().remove();
						});
					});
					$('.savePublicationButton').click(function() {
						if ($("#publicationForm").valid()) {
							that.sendPublicationsInfo();
						}
					});
					$('.deletePublicationButtton').click(function() {
						that.deletePublication(idPublicationToRemove);
					});
					$('.addPublicationLink').click(function() {
						$("#publicationCreationDiv").show();
						$("#addPublicationDiv").hide();
						$('html, body').animate({
							scrollTop : $("#publicationCreationDiv").offset().top
						}, 500);
						if (removeOptionVisibilty) {
							$("#publicationVisibility option[value='forum']").remove();
						}
					});
					$('.cancelAddPublication').click(function() {
						$("#publicationCreationDiv").hide();
						$("#addPublicationDiv").show();
					});
					that.jqEasyCounter("#publicationForm .subjectPublication", 40, 40,
							'#0796BE', LEFTCARACTERES);
					that.jqEasyCounter("#publicationForm .contentPublication", 200, 200,
							'#0796BE', LEFTCARACTERES);
					$('div.jqEasyCounterMsg').css('width', '99%');
				};
				this.jqEasyCounter = function(selector, maxChars, maxCharsWarning,
						msgFontColor, msgCaracters) {
					$(selector).jqEasyCounter({
						'maxChars' : maxChars,
						'maxCharsWarning' : maxCharsWarning,
						'msgFontColor' : msgFontColor,
						'msgCaracters' : msgCaracters,
					});
				};
				this.loadPublications = function(listPublications) {
					var listForumPublications = [];
					var listStandPublications = [];
					$(listPublications)
					.each(
							function(index, publication) {
								if (publication.isForumPublication == "true") {
									listForumPublications.push(publication);
								}else {
									listStandPublications.push(publication);
								}
							});
					var rightGroup = listStandRights[rightGroupEnum.FORUM_PUBLICATIONS];
					if (rightGroup != null) {
						var permittedForumPublicationsNbr = rightGroup.getRightCount();
						var forumPublicationsNbr = Object.keys(listForumPublications).length;
						if (permittedForumPublicationsNbr == -1) {
							$('#addForumPublicationDiv #addForumPublicationLink').text("[ "+ADDLABEL+" ]");
							$('#forumPublicationDiv').show();
						} else if (permittedForumPublicationsNbr == 0){
							$('#forumPublicationDiv').hide();
						} else if (forumPublicationsNbr < permittedForumPublicationsNbr) {
                    		console.log('teeeeeeeeest forum');
							$('#numberForumPublications').text(forumPublicationsNbr);
			        		$('#numberPermittedForumPublications').text(permittedForumPublicationsNbr);
							$('#forumPublicationDiv, #addForumPublicationDiv').show();
						} else $('#addForumPublicationDiv').hide();
					}
					rightGroup = listStandRights[rightGroupEnum.STAND_PUBLICATIONS];
					if (rightGroup != null) {
						var permittedStandPublicationsNbr = rightGroup.getRightCount();
						var standPublicationsNbr = Object.keys(listStandPublications).length;
						if (permittedStandPublicationsNbr == -1) {
							$('#addStandPublicationDiv #addStandPublicationLink').text("[ "+ADDLABEL+" ]");
							$('#standPublicationDiv').show();
						} else if (permittedStandPublicationsNbr == 0){
							$('#standPublicationDiv').hide();
						} else if (standPublicationsNbr < permittedStandPublicationsNbr) {
                    		console.log('teeeeeeeeest stand');
							$('#numberStandPublications').text(standPublicationsNbr);
			        		$('#numberPermittedStandPublications').text(permittedStandPublicationsNbr);
							$('#standPublicationDiv, #addStandPublicationDiv').show();
						} else {
							$('#addStandPublicationDiv').hide();
						}
					}
					removeOptionVisibilty = false;
					$('.listPublications').empty();
					$(listPublications)
							.each(
									function(index, publication) {
										var isForum = publication.isForumPublication;
										var idPublication = publication.idPublication;
										var subjectPublication = publication.subjectPublication;
										var contentPublication = publication.contentPublication;
										var interestdata = {
											idPublication : replaceSpecialChars(idPublication),
											subjectPublication : replaceSpecialChars(subjectPublication),
											contentPublication : replaceSpecialChars(contentPublication),
											typePublication : isForum,
											editLabel : EDITLABEL,
											saveLabel : SAVELABEL,
											cancelLabel : CANCELLABEL,
											deleteLabel : DELETELABEL
										};
										if (isForum == "true") {
											removeOptionVisibilty = true;
											$('.addForumPublication').hide();
											$("#tempPublicationDisplay").tmpl(interestdata)
													.prependTo("#forumPublication");
										} else {
											$("#tempPublicationDisplay").tmpl(interestdata)
													.prependTo("#standPublication");
										}
										$('#editSubjectPublication' + idPublication).bind(
												'click',
												function() {
													$('#subjectPublicationInput' + idPublication).val(
															$('#subjectPublicationLabel' + idPublication)
																	.text());
													$('#subjectPublicationInput' + idPublication).show();
													$('#subjectPublicationLabel' + idPublication).hide();
													$('#restoreSubjectDiv' + idPublication).show();
													$('#editSubjectDiv' + idPublication).hide();
												});

										$('#editContentPublication' + idPublication).bind(
												'click',
												function() {
													$('#contentPublicationInput' + idPublication).val(
															$('#contentPublicationLabel' + idPublication)
																	.text());
													$('#contentPublicationInput' + idPublication).show();
													$('#contentPublicationLabel' + idPublication).hide();
													$('#restoreContentDiv' + idPublication).show();
													$('#editContentDiv' + idPublication).hide();
												});

										$('#confirmEditSubject' + idPublication).bind(
												'click',
												function() {
													if ($("#subjectPublicationForm" + idPublication)
															.valid()) {
														$('#subjectPublicationLabel' + idPublication).text(
																$('#subjectPublicationInput' + idPublication)
																		.val());
														$('#subjectPublicationInput' + idPublication)
																.hide();
														$('#subjectPublicationLabel' + idPublication)
																.show();
														$('#restoreSubjectDiv' + idPublication).hide();
														$('#editSubjectDiv' + idPublication).show();
													}
												});
										$('#restoreEditSubject' + idPublication)
												.bind(
														'click',
														function() {
															$('#subjectPublicationInput' + idPublication)
																	.val("");
															$('#subjectPublicationInput' + idPublication)
																	.hide();
															$('#subjectPublicationLabel' + idPublication)
																	.show();
															$('#restoreSubjectDiv' + idPublication).hide();
															$('#editSubjectDiv' + idPublication).show();
														});

										$('#confirmEditContent' + idPublication).bind(
												'click',
												function() {
													if ($("#contentPublicationForm" + idPublication)
															.valid()) {
														$('#contentPublicationLabel' + idPublication).text(
																$('#contentPublicationInput' + idPublication)
																		.val());
														$('#contentPublicationInput' + idPublication)
																.hide();
														$('#contentPublicationLabel' + idPublication)
																.show();
														$('#restoreContentDiv' + idPublication).hide();
														$('#editContentDiv' + idPublication).show();
													}
												});
										$('#restoreEditContent' + idPublication)
												.bind(
														'click',
														function() {
															$('#contentPublicationInput' + idPublication)
																	.val("");
															$('#contentPublicationInput' + idPublication)
																	.hide();
															$('#contentPublicationLabel' + idPublication)
																	.show();
															$('#restoreContentDiv' + idPublication).hide();
															$('#editContentDiv' + idPublication).show();
														});

										$('#removePublication' + idPublication).bind('click',
												function() {
													idPublicationToRemove = idPublication;
													$('.popupDeletePublication').show();
													$('.backgroundPopup').show();
												});
										$('.modifExist' + idPublication)
												.bind(
														'click',
														function() {
															if ($('#typePublication' + idPublication).val() == "true") {
																$('#updatePublicationsDiv').appendTo(
																		'#forumPublicationDiv');
															} else {
																$('#updatePublicationsDiv').appendTo(
																		'#standPublicationDiv');
															}
															$('#updatePublicationsDiv').show();
														});
										$('.restore').hide();
									});
					that.jqEasyCounter(".subjectPublicationForm .subjectPublication", 40,
							40, '#0796BE', LEFTCARACTERES);
					that.jqEasyCounter(".contentPublicationForm .contentPublication",
							200, 200, '#0796BE', LEFTCARACTERES);
					$('div.jqEasyCounterMsg').css('width', '99%');
					$(".subjectPublicationForm").validate({});
					$(".contentPublicationForm").validate({});
				};

				this.sendPublicationsInfoError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};
				this.sendPublicationsInfoSuccess = function(xml) {
					$("#publicationCreationDiv textarea").val("");
					$("#publicationCreationDiv input").val("");
					$("#publicationCreationDiv").hide();
					$("#addPublicationDiv").show();
					that.notifyGetPublications();
				};
				this.sendPublicationsInfo = function() {
					var publicationXml = "<publication><subjectPublication>"
							+ htmlEncode($('#publicationForm .subjectPublication').val())
							+ "</subjectPublication><contentPublication>"
							+ htmlEncode($('#publicationForm .contentPublication').val())
							+ "</contentPublication><isForumPublication>"
							+ publicationVisibility + "</isForumPublication></publication>";
					genericAjaxCall('POST', staticVars["urlBackEnd"]
							+ 'publication/add?type=' + publicationVisibility + '&idLang='
							+ getIdLangParam(), 'application/xml', 'xml', publicationXml,
							that.sendPublicationsInfoSuccess, that.sendPublicationsInfoError);
				};

				this.deletePublicationError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};
				this.deletePublicationSuccess = function(idPublication) {
					$('.popupDeletePublication').hide();
					$('.backgroundPopup').hide();
					that.notifyGetPublications();
				};
				this.deletePublication = function(idPublication) {
					genericAjaxCall('POST', staticVars["urlBackEnd"]
							+ 'publication/delete?idPublication=' + idPublication,
							'application/xml', 'xml', '', function() {
								that.deletePublicationSuccess(idPublication);
							}, that.deletePublicationError);
				};

				this.updatePublicationsInfoError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};
				this.updatePublicationsInfoSuccess = function(xml) {
					$("#updatePublicationsDiv").hide();
					that.notifyGetPublications();
				};
				this.updatePublicationsInfo = function() {
					var publicationXml = "";
					var i = 0; // to remove
					$('.onePublicationDisplay').each(
							function() {
								var idPublication = $('input.idPub').eq(i).val();
								i++; // to remove
								var subjectPublication = $(
										'#subjectPublicationLabel' + idPublication).text().trim();
								var contentPublication = $(
										'#contentPublicationLabel' + idPublication).text().trim();
								publicationXml = publicationXml
										+ "<publication><idpublication>" + idPublication
										+ "</idpublication><contentPublication>"
										+ htmlEncode(contentPublication)
										+ "</contentPublication><subjectPublication>"
										+ htmlEncode(subjectPublication)
										+ "</subjectPublication></publication>";
							});
					publicationXml = "<publicationDTOList>" + publicationXml
							+ "</publicationDTOList>";
					genericAjaxCall('POST', staticVars["urlBackEnd"]
							+ 'publication/update', 'application/xml', 'xml', publicationXml,
							that.updatePublicationsInfoSuccess,
							that.updatePublicationsInfoError);
				};
				this.notifyGetPublications = function() {
					$.each(listeners, function(i) {
						listeners[i].getPublications();
					});
				};
			},

			PublicationsViewListener : function(list) {
				if (!list)
					list = {};
				return $.extend(list);
			},

			PublicationsController : function(model, view) {
				/**
				 * listen to the model
				 */
				var publicationsModelList = new $.PublicationsModelListener({
					loadPublications : function(listPublications) {
						view.loadPublications(listPublications);
					},
				});
				model.addListener(publicationsModelList);
				/**
				 * listen to the view
				 */
				var publicationsViewList = new $.PublicationsViewListener({
					getPublications : function() {
						model.getPublications();
					},
				});
				view.addListener(publicationsViewList);

				this.initController = function() {
					view.initView();
					model.initModel();
				};
			}
		});
