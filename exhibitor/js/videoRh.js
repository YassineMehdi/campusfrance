jQuery
		.extend({

			VideoRhModel : function() {
				var listeners = new Array();
				var that = this;

				this.initModel = function() {
					that.getStandVideoRh();
				};
				this.getStandVideoRhSuccess = function(xml) {
					listVideoRhCache = [];
					$(xml).find("allfiles").each(function() {
						var video = new $.Video($(this));
						listVideoRhCache[video.idVideo] = video;
					});

					that.notifyLoadVideoRh(listVideoRhCache);
				};

				this.getStandVideoRhError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};

				this.getStandVideoRh = function() {
					$('#listStandVideoRh').empty();
					genericAjaxCall('GET', staticVars.urlBackEnd
							+ 'allfiles/standVideoRh?idLang=' + getIdLangParam(),
							'application/xml', 'xml', '', that.getStandVideoRhSuccess,
							that.getStandVideoRhError);

				};

				this.getAnalyticsVideoRh = function(callBack, path) {
					genericAjaxCall('GET', staticVars.urlBackEnd + path,
							'application/xml', 'xml', '', function(xml) {
								listVideoRhCache = [];
								$(xml).find("allfiles").each(function() {
									var video = new $.Video($(this));
									listVideoRhCache[video.idVideo] = video;
								});
								callBack();
							}, that.getStandVideoRhError);

				};

				this.getVideoRhById = function(videoId) {
					return listVideoRhCache[videoId];
				};

				this.addVideoRhSuccess = function(xml) {
					that.notifyAddVideoRhSuccess();
				};

				this.addVideoRhError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};

				this.addVideoRh = function(video) {
					genericAjaxCall('POST', staticVars.urlBackEnd
							+ 'allfiles/addFile?idLang=' + getIdLangParam(),
							'application/xml', 'xml', video.xmlData(),
							that.addVideoRhSuccess, that.addVideoRhError);
				};

				this.updateVideoRhSuccess = function(xml) {
					that.notifyUpdateVideoRhSuccess();
				};

				this.updateVideoRhError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};

				this.updateVideoRh = function(video) {
					genericAjaxCall('POST', staticVars.urlBackEnd
							+ 'allfiles/updateFile?idLang=' + getIdLangParam(),
							'application/xml', 'xml', video.xmlData(),
							that.updateVideoRhSuccess, that.updateVideoRhError);
				};

				this.deleteVideoRhSuccess = function(xml) {
					that.notifyDeleteVideoRhSuccess();
				};

				this.deleteVideoRhError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};

				this.deleteVideoRh = function(video) {
					genericAjaxCall('GET', staticVars.urlBackEnd
							+ 'allfiles/deleteMedia/' + video.getIdVideo(),
							'application/xml', 'xml', '', that.deleteVideoRhSuccess,
							that.deleteVideoRhSuccess);
				};

				this.notifyLoadVideoRh = function(listVideoRhs) {
					$.each(listeners, function(i) {
						listeners[i].loadVideoRh(listVideoRhs);
					});
				};

				this.notifyAddVideoRhSuccess = function() {
					$.each(listeners, function(i) {
						listeners[i].addVideoRhSuccess();
					});
				};

				this.notifyUpdateVideoRhSuccess = function() {
					$.each(listeners, function(i) {
						listeners[i].updateVideoRhSuccess();
					});
				};

				this.notifyDeleteVideoRhSuccess = function() {
					$.each(listeners, function(i) {
						listeners[i].deleteVideoRhSuccess();
					});
				};

				this.addListener = function(list) {
					listeners.push(list);
				};
			},

			VideoRhModelListener : function(list) {
				if (!list)
					list = {};
				return $.extend(list);
			},

			VideoRhView : function() {
				var listeners = new Array();
				var idVideoRhToChange = 0;
				var that = this;
				var videoRhUrlToChange = "";
				var jqXHRVideoRh = null;

				this.initView = function() {
					traduct();
					idVideoRhToChange = 0;
					videoRhUrlToChange = "";
					jqXHRVideoRh = null;
					$(".draggable").draggable({
						cancel : ".eds-dialog-inside"
					});
					customJqEasyCounter("#videoRhTitle", 255, 255, '#0796BE',
							LEFTCARACTERES);
					customJqEasyCounter("#videoRhUrl", 255, 255, '#0796BE',
							LEFTCARACTERES);
					customJqEasyCounter("#videoRhDescription", 65535, 65535, '#0796BE',
							LEFTCARACTERES);
					$("#videoRhForm td.jqEasyCounterMsg").css('width', '55%');

					$.validator.addMethod("validUrl", function(value, element) {
						var linkUrl = $("#videoRhUrl").val();
						if ((compareToLowerString(linkUrl, "youtube.com") !== -1)
								|| (compareToLowerString(linkUrl, "dailymotion.com") !== -1)
								|| (compareToLowerString(linkUrl, "vimeo.com") !== -1)) {
							return true;
						} else {
							return false;
						}
					}, traductLabel("enterValidLinkLabel"));

					$("#videoRhForm").validate({
						rules : {
							videoRhUrl : {
							// validUrl : true,
							// custom_url : true
							}
						}
					});
					$('#addVideoRhLink').click(
							function() {
								$('.popupVideoRh .editLabel').addClass('addLabel').removeClass(
										'editLabel').text(ADDLABEL);
								that.initPopupAddVideoRh('', '', 'youtube', '');
								$('#videoRhLoaded,#progressVideoRh').hide();
								$('#loadVideoRh, .popupVideoRh, .backgroundPopup').show();
								enabledButton('.saveVideoRhButton');
							});

					$('#videoRhUrl').addClass('required');
					$('#urlOption').click(function() {
						$('#urlTr').show();
						$('#uploadTr').hide();
						$('#videoRhUrl').addClass('required');
						$('#loadVideoRhError,#loadVideoRh').hide();
						enabledButton('.saveVideoRhButton');
						if (jqXHRVideoRh != null)
							jqXHRVideoRh.abort();
						/*
						 * $('#videoUrl').rules('add', 'custom_url');
						 * $('#videoUrl').rules('add', 'validUrl');
						 */
					});
					$('#uploadOption').click(function() {
						$('#urlTr').hide();
						$('#uploadTr').show();
						$('#videoRhUrl').removeClass('required');
						$('#loadVideoRhError,#progressVideoRh').hide();
						$('#loadVideoRh').show();
						/*
						 * var settings = $('#videoRhForm').validate().settings; delete
						 * settings.rules.videoRhUrl;
						 */
						enabledButton('.saveVideoRhButton');
					});

					$('.saveVideoRhButton')
							.click(
									function() {
										if ($('#videoRhForm').valid()
												&& !isDisabled('.saveVideoRhButton')) {
											disabledButton('.saveVideoRhButton');
											if ($("#videoRhForm input:checked").attr('id') == "urlOption") {
												videoRhUrlToChange = $('#videoRhUrl').val();
											}
											if (videoRhUrlToChange != '') {
												$('#loadingEds').show();
												if (idVideoRhToChange != 0) {
													that.updateVideoRh(idVideoRhToChange);
												} else
													that.addVideoRh();
											} else {
												$('#loadVideoRhError').show();
												enabledButton('.saveVideoRhButton');
											}
										}
									});
					$('.closePopup,.eds-dialog-close').click(function(e) {
						e.preventDefault();
						$('.eds-dialog-container').hide();
						$('.backgroundPopup').hide();
						$('#playerVideoPopup').empty();
						if (jqXHRVideoRh != null)
							jqXHRVideoRh.abort();
					});

					$('.deleteVideoRhButton').click(function() {
						$('#loadingEds').show();
						disabledButton('.deleteVideoRhButton');
						that.deleteVideoRh(idVideoRhToChange);
					});
					$('#videoRhUpload').fileupload(
							{
								beforeSend : function(jqXHR, settings) {
									beforeUploadFile('.saveVideoRhButton', '#progressVideoRh',
											'#loadVideoRh,#loadVideoRhError');
								},
								datatype : 'json',
								cache : false,
								add : function(e, data) {
									var goUpload = true;
									var uploadFile = data.files[0];
									var fileName = uploadFile.name;
									timeUploadVideoRh = (new Date()).getTime();
									data.url = getUrlPostUploadFile(timeUploadVideoRh, fileName);
									if (!(/\.(flv|mp4|mov|m4v|f4v|wmv)$/i).test(fileName)) {
										goUpload = false;
									}
									if (uploadFile.size > MAXFILESIZE) {
										goUpload = false;
									}
									if (goUpload == true) {
										jqXHRVideoRh = data.submit();
									} else
										$('#loadVideoRhError').show();
								},
								progressall : function(e, data) {
									progressBarUploadFile('#progressVideoRh', data, 100);
								},
								done : function(e, data) {
									var file = data.files[0];
									var fileName = file.name;
									var hashFileName = getUploadFileName(timeUploadVideoRh,
											fileName);
									videoRhUrlToChange = getUploadFileUrl(hashFileName);
									afterUploadFile('.saveVideoRhButton', '#videoRhLoaded',
											'#progressVideoRh');
								}
							});
				};
				this.initPopupAddVideoRh = function(videoRhTitle, videoRhDescription,
						videoRhType, videoRhUrl) {
					idVideoRhToChange = 0;
					videoRhUrlToChange = '';
					$('#videoRhTitle').val(videoRhTitle);
					$('#videoRhDescription').val(videoRhDescription);
					$("#videoRhType").val(videoRhType);
					$('#videoRhUrl').val(videoRhUrl);
					$('#urlOption').click();

				};
				this.factoryClickUrlOption = function(videoRhType, videoRhUrl) {
					$('#urlOption').click();
					$("#videoRhType").val(videoRhType);
					$('#videoRhUrl').val(videoRhUrl);
				};
				this.addListener = function(list) {
					listeners.push(list);
				};

				this.displayVideoRh = function(listVideoRhs) {
					var rightGroup = listStandRights[rightGroupEnum.VIDEOS_RH];
					if (rightGroup != null) {
						var permittedVideoRhsNbr = rightGroup.getRightCount();
						var videoRhsNbr = Object.keys(listVideoRhs).length;
						if (permittedVideoRhsNbr == -1) {
							$('#addVideoRhDiv #addVideoRhLink').text(ADDLABEL);
							$('#addVideoRhDiv').show();
						} else if (videoRhsNbr < permittedVideoRhsNbr) {
							$('#numberVideoRhs').text(videoRhsNbr);
			        $('#numberPermittedVideoRhs').text(permittedVideoRhsNbr);
							$('#addVideoRhDiv').show();
						} else {
							$('#addVideoRhDiv').hide();
						}
					}
					var videoRh = null;
					for ( var index in listVideoRhs) {
						videoRh = listVideoRhs[index];
						var videoRhId = videoRh.getIdVideo();
						var videoRhUrl = videoRh.getVideoUrl();
						var videoRhTitle = videoRh.getVideoTitle();
						var videoRhDescription = videoRh.getVideoDescription();
						var data = {
							videoRhTitle : replaceSpecialChars(formattingLongWords(
									videoRhTitle, 50)),
							videoRhDescription : replaceSpecialChars(videoRhDescription),
							videoRhId : replaceSpecialChars(videoRhId),
							videoRhUrl : replaceSpecialChars(videoRhUrl),
							viewLabel : VIEWLABEL,
							editLabel : EDITLABEL,
							deleteLabel : DELETELABEL
						};
						$('#standVideoRhTemplate').tmpl(data).appendTo('#listStandVideoRh');
						$('.deleteVideoRh' + videoRhId).bind('click', function() {
							idVideoRhToChange = $(this).closest('.oneVideoRh').attr('id');
							$('.popupDeleteVideoRh,.backgroundPopup').show();
							enabledButton('.deleteVideoRhButton');
						});
						$('.editVideoRh' + videoRhId)
								.bind(
										'click',
										function() {
											idVideoRhToChange = $(this).closest('.oneVideoRh').attr(
													'id');
											var videoRh = listVideoRhs[idVideoRhToChange];
											var videoRhUrl = videoRh.getVideoUrl();
											var videoRhTitle = videoRh.getVideoTitle();
											var videoRhDescription = videoRh.getVideoDescription();
											$('.popupVideoRh .addLabel').addClass('editLabel')
													.removeClass('addLabel').text(EDITLABEL);
											$('#videoRhTitle').val(videoRhTitle);
											$('#videoRhDescription').val(videoRhDescription);
											if (compareToLowerString(videoRhUrl, "youtube") !== -1) {
												that.factoryClickUrlOption("youtube", videoRhUrl);
											} else if (compareToLowerString(videoRhUrl, "dailymotion") !== -1) {
												that.factoryClickUrlOption("dailymotion", videoRhUrl);
											} else if (compareToLowerString(videoRhUrl, "vimeo") !== -1) {
												that.factoryClickUrlOption("vimeo", videoRhUrl);
											} else {
												videoRhUrlToChange = videoRhUrl;
												$('#uploadOption').click();
											}
											enabledButton('.saveVideoRhButton');
											$('#videoRhLoaded,#loadVideoRhError,#progressVideoRh')
													.hide();
											$('#loadVideoRh,.popupVideoRh,.backgroundPopup').show();
										});
						$('.viewVideoRh' + videoRhId).on('click', function() {
							var videoRhId = $(this).closest('.oneVideoRh').attr('id');
							var videoRh = listVideoRhs[videoRhId];
							var videoRhTitle = videoRh.getVideoTitle();
							var videoRhUrl = videoRh.getVideoUrl();
							playVideoPopup(videoRhUrl, videoRhTitle);
						});
					}
					$('#loadingEds').hide();
				};

				this.addVideoRhSuccess = function() {
					hidePopups();
					showUpdateSuccess();
					that.notifyVideoRhUpdated();
					// enabledButton('.saveVideoRhButton');
				};
				this.updateVideoRhSuccess = function() {
					hidePopups();
					showUpdateSuccess();
					that.notifyVideoRhUpdated();
					// enabledButton('.saveVideoRhButton');
				};
				this.deleteVideoRhSuccess = function() {
					hidePopups();
					showUpdateSuccess();
					that.notifyVideoRhUpdated();
					// enabledButton('.deleteVideoRhButton');
				};
				this.addVideoRh = function() {
					var videoRhTitle = $('#videoRhTitle').val();
					var videoRhDescription = $('#videoRhDescription').val();
					var videoRh = new $.Video();
					videoRh.setVideoTitle(videoRhTitle);
					videoRh.setVideoDescription(videoRhDescription);
					videoRh.setVideoUrl(videoRhUrlToChange);
					var fileType = new $.FileType();
					fileType.setFileTypeId(fileTypeEnum.VIDEORH);
					videoRh.setFileType(fileType);
					that.notifyAddVideoRh(videoRh);
				};
				this.updateVideoRh = function(videoRhId) {
					var videoRhTitle = $('#videoRhTitle').val();
					var videoRhDescription = $('#videoRhDescription').val();
					var videoRh = listVideoRhCache[videoRhId];
					videoRh.setVideoTitle(videoRhTitle);
					videoRh.setVideoDescription(videoRhDescription);
					videoRh.setVideoUrl(videoRhUrlToChange);
					that.notifyUpdateVideoRh(videoRh);
				};

				this.deleteVideoRh = function(videoRhId) {
					var videoRh = listVideoRhCache[videoRhId];
					that.notifyDeleteVideoRh(videoRh);
				};
				this.notifyAddVideoRh = function(videoRh) {
					$.each(listeners, function(i) {
						listeners[i].addVideoRh(videoRh);
					});
				};
				this.notifyUpdateVideoRh = function(videoRh) {
					$.each(listeners, function(i) {
						listeners[i].updateVideoRh(videoRh);
					});
				};
				this.notifyDeleteVideoRh = function(videoRh) {
					$.each(listeners, function(i) {
						listeners[i].deleteVideoRh(videoRh);
					});
				};
				this.notifyVideoRhUpdated = function() {
					$.each(listeners, function(i) {
						listeners[i].videoRhUpdated();
					});
				};

			},

			VideoRhViewListener : function(list) {
				if (!list)
					list = {};
				return $.extend(list);
			},

			VideoRhController : function(model, view) {
				/**
				 * listen to the model
				 */
				var videoRhModelList = new $.VideoRhModelListener({
					loadVideoRh : function(listVideoRhs) {
						view.displayVideoRh(listVideoRhs);
					},
					addVideoRhSuccess : function() {
						view.addVideoRhSuccess();
					},
					updateVideoRhSuccess : function() {
						view.updateVideoRhSuccess();
					},
					deleteVideoRhSuccess : function() {
						view.deleteVideoRhSuccess();
					},
				});
				model.addListener(videoRhModelList);
				/**
				 * listen to the view
				 */
				var videoRhViewList = new $.VideoRhViewListener({
					videoRhUpdated : function() {
						model.getStandVideoRh();
					},
					addVideoRh : function(video) {
						model.addVideoRh(video);
					},
					updateVideoRh : function(video) {
						model.updateVideoRh(video);
					},
					deleteVideoRh : function(video) {
						model.deleteVideoRh(video);
					},
				});
				view.addListener(videoRhViewList);
				this.initController = function() {
					view.initView();
					model.initModel();
				};
				this.getAnalyticsVideoRh = function(callBack, path) {
					model.getAnalyticsVideoRh(callBack, path);
				};
				this.getVideoById = function(videoId) {
					return model.getVideoById(videoId);
				};
			}
		});
