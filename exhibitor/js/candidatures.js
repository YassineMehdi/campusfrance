jQuery.extend({

	CandidatureModel : function() {
		var listeners = new Array();
		var that = this;

		this.initModel = function(){
			var rightGroup = listStandRights[rightGroupEnum.SOLICITED_JOB_APPLICATION];
			if (rightGroup != null && rightGroup.getRightCount() != 0) {
				that.getJobApplications();
				that.getStandJobOffers();
			}
			rightGroup = listStandRights[rightGroupEnum.UN_SOLICITED_JOB_APPLICATION];
			if (rightGroup != null && rightGroup.getRightCount() != 0) {
				that.getUnsolicitedJobApplications();
			}
		};

		this.addListener = function(list) {
			listeners.push(list);
		};
		this.getStandJobOffersSuccess = function(xml) {
			var jobOffers = [];
			$(xml).find("joboffer").each(function() {
				var jobOffer = new $.JobOffer($(this));
				jobOffers[jobOffer.getJobId()]=jobOffer;
			});
			that.notifyLoadJobOffers(jobOffers);
		};
		
		this.getStandJobOffersError = function(xhr,status,error) {
			isSessionExpired(xhr.responseText);
		};
		
		this.getStandJobOffers = function() {
			genericAjaxCall('GET', staticVars.urlBackEnd
					+ 'jobOffer/standJobOffers?idLang='+getIdLangParam(), 'application/xml', 'xml', '',
					that.getStandJobOffersSuccess, that.getStandJobOffersError);
		};
		/** 
		 *  Get job application
		 *                                      */
		this.getJobApplicationsSuccess = function(xml) {
			listJobApplicationsCache = new Array();
			$(xml).find("jobApplication").each(function() {
				var jobApplication = new $.JobApplication($(this));
				listJobApplicationsCache[jobApplication.getJobApplicationId()]=jobApplication;
			});
			that.notifyLoadJobApplication(listJobApplicationsCache);
		};

		this.getJobApplicationsError = function(xhr,status,error) {
			isSessionExpired(xhr.responseText);
		};

		this.getJobApplications = function() {
			genericAjaxCall('GET', staticVars.urlBackEnd
					+ 'jobApplication/jobApplications',
					'application/xml', 'xml', '',
					that.getJobApplicationsSuccess, '');
		};

		/** 
		 *  Get unsolicited job application
		 *                                      */
		this.getUnsolicitedJobApplicationsSuccess = function(xml) {
			listUnsolicitedJobApplicationsCache = new Array();
			$(xml).find("unsolicitedJobApplication").each(
							function() {
								var unsolicitedJobApplication = new $.UnsolicitedJobApplication($(this));
								listUnsolicitedJobApplicationsCache[unsolicitedJobApplication.getJobApplicationId()]=unsolicitedJobApplication;
							});
			that.notifyLoadUnsolicitedJobApplication(listUnsolicitedJobApplicationsCache);
		};
		
		this.getUnsolicitedJobApplicationsError = function(xhr,status,error) {
			isSessionExpired(xhr.responseText);
		};

		this.getUnsolicitedJobApplications = function() {
			genericAjaxCall('GET', staticVars.urlBackEnd
					+ 'jobApplication/unSolicitedJobApplications',
					'application/xml', 'xml', '',
					that.getUnsolicitedJobApplicationsSuccess, that.getUnsolicitedJobApplicationsError);
		};

		/** 
		 *  update job application
		 *                                      */

		this.sendMessageSuccess = function() {
			showPopupMessageSent();
		};
		
		this.sendMessageError = function(xhr,status,error) {
			isSessionExpired(xhr.responseText);
		};
		
		this.updateJobApplication = function(jobApplicationXml) {
			genericAjaxCall('POST', staticVars.urlBackEnd
					+ 'jobApplication/updateJobApplication?idLang='+getIdLangParam(), 'application/xml', 'xml',
					jobApplicationXml, that.sendMessageSuccess, that.sendMessageError);
		};

		/** 
		 *  update unsolicited job application
		 *                                      */
		this.updateUnsolicitedJobApplication = function(jobApplicationXml) {
			genericAjaxCall('POST', staticVars.urlBackEnd
					+ 'jobApplication/updateUnsolicitedJobApplication', 'application/xml', 'xml',
					jobApplicationXml, that.sendMessageSuccess, that.sendMessageError);
		};

		/** 
		 *  update unsolicited job application
		 *                                      */
		this.readUnsolicitedJobApplicationSuccess = function(xml) {
		};
		
		this.readUnsolicitedJobApplicationError = function(xhr,status,error) {
			isSessionExpired(xhr.responseText);
		};

		this.readUnsolicitedJobApplication = function(unsolicitedJobApplicationId) {
			genericAjaxCall('POST', staticVars.urlBackEnd + 'jobApplication/readUnsolicitedJobApplication?unsolicitedJobApplicationId='+unsolicitedJobApplicationId, 
					'application/xml', 'xml', '',	that.readUnsolicitedJobApplicationSuccess, that.readUnsolicitedJobApplicationError);
		};

		/** 
		 *  update job application
		 *                                      */
		this.readJobApplicationSuccess = function(xml) {
		};
		
		this.readJobApplicationError = function(xhr,status,error) {
			isSessionExpired(xhr.responseText);
		};

		this.readJobApplication = function(jobApplicationId) {
			genericAjaxCall('POST', staticVars.urlBackEnd + 'jobApplication/readJobApplication?jobApplicationId='+jobApplicationId, 
					'application/xml', 'xml', '',	that.readJobApplicationSuccess, that.readJobApplicationError);
		};
		
		/** 
		 *  notif
		 *                                      */
		this.notifyLoadJobOffers = function(jobOffers){
			$.each(listeners, function(i) {
				listeners[i].loadJobOffers(jobOffers);
			});
		};
		this.notifyLoadJobApplication = function(listJobApplicationsCache) {
			$.each(listeners, function(i) {
				listeners[i].loadJobApplication(listJobApplicationsCache);
			});
		};

		this.notifyLoadUnsolicitedJobApplication = function(listUnsolicitedJobApplicationsCache) {
			$.each(listeners,function(i) {
				listeners[i].loadUnsolicitedJobApplication(listUnsolicitedJobApplicationsCache);
			});
		};

	},

	CandidatureModelList : function(list) {
		if (!list)
			list = {};
		return $.extend(list);
	},

	CandidatureView : function() {
		var listeners = new Array();
		var that = this;
		var listCandidaturesCache = null;
		var listUnsolicitedCandidaturesCache = null;
		var currentCandidaturesCache = null;
		var currentUnsolicitedCandidaturesCache = null;
		var SOLICITED="solicited";
		var UNSOLICITED="unsolicited";
		this.initView = function(){
			$('#loadingEds').show();
      traduct();
      $(".popupMotivationLetter, .popupMessageCandidature, #popupExportCandidatures").draggable({ cancel: ".eds-dialog-inside" });
      customJqEasyCounter($('#messageFormCandidature #messageContentCandidature'), 65535, 65535, '#0796BE', LEFTCARACTERES);
			$('#messageFormCandidature div.jqEasyCounterMsg').css('width', '31%');
      $('#candidaturesTab a').click(function(e) {
				e.preventDefault();
				$(this).tab('show');
			});
			$('.closePopup, .eds-dialog-close').click(function() {
				$('#messageContentCandidature').val("");
				hidePopups();
			});
			$('#messageFormCandidature').validate();
			$('#sendMessageCandidature').click(function(){
				if($('#messageFormCandidature').valid()){
					var messagecontent = $('#messageContentCandidature').val();
					var messageXml=null;
					if(typeJobApplicationToChange == SOLICITED){
						var jobApplication=listJobApplicationsCache[idJobApplicationToChange];
						jobApplication.response=messagecontent;
						messageXml = "<jobApplication><idJobApplication>"+idJobApplicationToChange+"</idJobApplication><response>"+escape(messagecontent)+"</response><jobOfferTitle>"+escape(jobApplication.getJobOfferTitle())+"</jobOfferTitle></jobApplication>";
						that.notifyUpdateJobApplication(messageXml);
					}else if(typeJobApplicationToChange == UNSOLICITED){
						messageXml = "<unSolicitedJobApplication><idJobApplication>"+idJobApplicationToChange+"</idJobApplication><response>"+escape(messagecontent)+"</response></unSolicitedJobApplication>";
						var unsolicitedJobApplication=listUnsolicitedJobApplicationsCache[idJobApplicationToChange];
						unsolicitedJobApplication.response=messagecontent;
						that.notifyUpdateUnsolicitedJobApplication(messageXml);
					}
					$('#messageContentCandidature').val("");
				}
			});
	    $( "#popupExportCandidatures #candidatureSortableColumns" ).sortable().disableSelection();
			var rightGroup = listStandRights[rightGroupEnum.SOLICITED_JOB_APPLICATION];
			if (rightGroup != null && rightGroup.getRightCount() != 0) {
				$('.exportSolicitedJobApplicationsLink').click(function(){
					$('#loadingEds').show(function(){
						$("#candidatureSortableJobOffer input").prop("checked", true);
						$("#candidatureSortableJobOffer, #popupExportCandidatures, .backgroundPopup").show();
						exportVisitors(currentCandidaturesCache);
						$('#loadingEds').hide();
					});
				});
				$("#candidatureSearchDiv #jobOfferSelect").unbind("change").bind("change", 
						that.filterJobApplication);
				that.initSolicitedJobApplicationsTable();
			}else {
				$('.solicitedCandidatures').closest("li").hide();
				$(".unsolicitedCandidatures").tab('show');
			}
			rightGroup = listStandRights[rightGroupEnum.UN_SOLICITED_JOB_APPLICATION];
			if (rightGroup != null && rightGroup.getRightCount() != 0) {
				$('.exportUnsolicitedJobApplicationsLink').click(function(){
					$('#loadingEds').show(function(){
						$("#candidatureSortableJobOffer input").prop("checked", false);
						$("#candidatureSortableJobOffer").hide();
						$("#popupExportCandidatures, .backgroundPopup").show();
						exportVisitors(currentUnsolicitedCandidaturesCache);
						$('#loadingEds').hide();
					});
				});
				that.initUnsolicitedJobApplicationsTable();
			}else {
				$('.unsolicitedCandidatures').closest("li").hide();
			}
		};
    this.addTableDatas = function(selector, datas){
    	var oTable = $(selector).dataTable();
      oTable.fnClearTable();
      if(datas.length > 0) {
      	oTable.fnAddData(datas);
      }
      oTable.fnDraw();
    };
    this.initSolicitedJobApplicationsTable = function(){
    	$('#listCandidaturesTable').dataTable({
				"aaData" : [],
				"aoColumns" : [{
					"sTitle" : FIRSTANDLASTNAMELABEL, "sWidth": "30%"
				},  
				{
					"sTitle" : TITLELABEL, "sWidth": "33%"
				},{
					"sTitle" : ACTIONSLABEL, "sWidth": "37%"
				},{
					"sTitle" : "id job","sWidth": "0%"
				},{
					"sTitle" : "","sWidth": "0%"
				}],
		        "aaSorting": [[ 3, "desc" ]],
				"aoColumnDefs": [{ "bVisible": false, "aTargets": [3, 4] }],
				"bAutoWidth": false,
				"iDisplayLength" : 10, 
				"fnDrawCallback": function() {
										$('#listCandidaturesTable .readCandidate').off("click");
					          $('#listCandidaturesTable .viewProfile').on("click", function () {
					          	var jobApplicationId = $(this).attr('index');
					          	var jobApplication = listCandidaturesCache[jobApplicationId];
					            viewUserProfile(jobApplication.getCandidateId());
					          });
										$('#listCandidaturesTable .viewCv').on("click", function () {
                    	var jobApplicationId = $(this).attr('index');
                    	var jobApplication = listCandidaturesCache[jobApplicationId];
                    	if(jobApplication!=null){
	                        window.open(jobApplication.getUrlCv(), '_blank');
                    	}
                    });
										$('#listCandidaturesTable .viewLetter').on("click", function () {
                    	var jobApplicationId = $(this).attr('index');
                    	var jobApplication = listCandidaturesCache[jobApplicationId];
                    	if(jobApplication!=null){
	                        that.viewLetter(jobApplication.getMotivationLetter(),jobApplication.getUserId(),jobApplication.getFullName(),jobApplication.getJobApplicationId(),SOLICITED, jobApplication.response);
                    	}
                    });
	                  $('#listCandidaturesTable .readCandidate').on("click", function () {
                    	var jobApplicationId = $(this).attr('index');
	                    var jobApplication = listCandidaturesCache[jobApplicationId];
	                    if(jobApplication.getUnread()) {
                				$('#listCandidaturesTable #notReaded_'+jobApplicationId).hide();
                      	that.notifyReadJobApplication(jobApplicationId);
                      	jobApplication.setUnread(false);
                			}
                    });
				}
			});
    };
    this.initUnsolicitedJobApplicationsTable = function(){
			$('#listUnsolicitedCandidaturesTable').dataTable({
				"aaData" : [],
				"aoColumns" : [{
					"sTitle" : FIRSTANDLASTNAMELABEL,"sWidth": "45%",
				}, {
					"sTitle" : ACTIONSLABEL,"sWidth": "55%",
				},{
					"sTitle" : 'id job',
				},{
					"sTitle" : "","sWidth": "0%"
				}],
				"aaSorting": [[ 2, "desc" ]],
				"aoColumnDefs": [{ "bVisible": false, "aTargets": [2, 3] }],
				"bAutoWidth": false,
				"iDisplayLength" : 10,
				 "fnDrawCallback": function() { 
												$('#listUnsolicitedCandidaturesTable .readCandidate').off("click");
			                  $('#listUnsolicitedCandidaturesTable .viewProfile').on("click", function () {
			                  	var unsolicitedJobApplicationId = $(this).attr('index');
			                    var unsolicitedJobApplication = listUnsolicitedCandidaturesCache[unsolicitedJobApplicationId];
			                    viewUserProfile(unsolicitedJobApplication.getCandidateId());
		                    });
												$('#listUnsolicitedCandidaturesTable .viewCv').on("click", function () {
			                  	var unsolicitedJobApplicationId = $(this).attr('index');
				                  var unsolicitedJobApplication = listUnsolicitedCandidaturesCache[unsolicitedJobApplicationId];
		                    	if(unsolicitedJobApplication!=null){
			                        window.open(unsolicitedJobApplication.getUrlCv(), '_blank');
		                    	}
		                    });
					 							$('#listUnsolicitedCandidaturesTable .viewLetter').on("click", function () {
													var unsolicitedJobApplicationId = $(this).attr('index');
													var unsolicitedJobApplication = listUnsolicitedCandidaturesCache[unsolicitedJobApplicationId];
													if(unsolicitedJobApplication!=null){
														that.viewLetter(unsolicitedJobApplication.getMotivationLetter(),unsolicitedJobApplication.getUserId(),unsolicitedJobApplication.getFullName(),unsolicitedJobApplicationId,UNSOLICITED,unsolicitedJobApplication.response);
													}
												 });
			                   $('#listUnsolicitedCandidaturesTable .readCandidate').on("click", function () {
			                  	  var unsolicitedJobApplicationId = $(this).attr('index');
			                    	var unsolicitedJobApplication = listUnsolicitedCandidaturesCache[unsolicitedJobApplicationId];
														if(unsolicitedJobApplication.getUnread()) {
															$('#listUnsolicitedCandidaturesTable #notReaded_'+unsolicitedJobApplicationId).hide();
												  		that.notifyReadUnsolicitedJobApplication(unsolicitedJobApplicationId);
															unsolicitedJobApplication.setUnread(false);
													  }
		                     });
				}
			});
    };
    this.filterJobApplication = function(){
    	var jobApplications = [];
    	var jobOfferId = $("#candidatureSearchDiv #jobOfferSelect").val();
    	for (var index in listCandidaturesCache) {
				var jobApplication = listCandidaturesCache[index];
				if(jobOfferId == 0 || jobApplication.jobOfferId == jobOfferId) jobApplications.push(jobApplication);
			}
    	that.listingJobApplications(jobApplications);
    };
		this.displayJobOffers = function(jobOffers){
			var jobOfferSelectHandler=$('#candidatureSearchDiv #jobOfferSelect');
			jobOfferSelectHandler.empty();
			jobOfferSelectHandler.append($('<option/>').val(0).html(ALLLABEL));
			jobOffers.sort(function(a, b) {
				if(a.getJobTitle() > b.getJobTitle()) {
					return 1;
				}else if(a.getJobTitle() < b.getJobTitle()){
					return -1;
				}else {
					return 0;
				}
			});
			for (var index in jobOffers) {
					var jobOffer = jobOffers[index];
					jobOfferSelectHandler.append($('<option/>').val(jobOffer.getJobId()).html(substringCustom(jobOffer.getJobTitle(), 50)));
			}
		};
		this.displayJobApplications = function(listJobApplications) {
			listCandidaturesCache = listJobApplications;
			that.listingJobApplications(listJobApplications);
		};
		this.listingJobApplications = function(listJobApplications) {
			currentCandidaturesCache = listJobApplications;
			var datas = new Array();
			var btnProfileHandler = $("<button class='btn btn-danger btn-middle readCandidate viewProfile'/>");
			var btnCvHandler = $("<button class='btn btn-info btn-middle readCandidate viewCv'/>");
			var btnLetterHandler = $("<button class='btn btn-success btn-middle readCandidate viewLetter'/>");
			var listActions = $("<div></div>");
			var newHandler = $('<div class="label label-important not-read-candidate"></div>');
			btnProfileHandler.text(VIEWPROFILELABEL);
			btnCvHandler.text(VIEWCVLABEL);
			btnLetterHandler.text(VIEWLETTERLABEL);
			newHandler.text(NEWLABEL);
			for (var index in listJobApplications) {
				var jobApplication = listJobApplications[index];
				var jobApplicationId = jobApplication.getJobApplicationId();
				var urlCv = jobApplication.getUrlCv();
				var btnCv = null;
				var fullName = jobApplication.getFullName();
				if(jobApplication.getUnread()){
					fullName = that.getHtml(newHandler.attr("id", "notReaded_"+jobApplicationId)) + fullName;
				}
				btnProfileHandler.attr("index", index);
				btnLetterHandler.attr("index", index);
				if(urlCv.trim()!='' && urlCv!=null){
					btnCvHandler.attr("index", index);
					btnCv = that.getHtml(btnCvHandler);
				}
				
				listActions.attr("index", jobApplicationId);
				listActions.html(that.getHtml(btnProfileHandler)
						+ that.getHtml(btnCv)
						+ that.getHtml(btnLetterHandler));
				var data = [fullName, jobApplication.getJobOfferTitle(), that.getHtml(listActions), 
				            jobApplicationId, jobApplication.getMotivationLetter()];
				datas.push(data);
			}
			that.addTableDatas('#listCandidaturesTable', datas);
			$('#loadingEds').hide();
		};
		this.getHtml = function(element){
			if(element != null) 
				return $("<div></div>").html(element).html();
		};
		this.displayUnsolicitedJobApplications = function(
				listUnsolicitedJobApplication) {
			listUnsolicitedCandidaturesCache = listUnsolicitedJobApplication;
			that.listingUnsolicitedJobApplications(listUnsolicitedJobApplication);
		};
		this.listingUnsolicitedJobApplications = function(
				listUnsolicitedJobApplication) {
			currentUnsolicitedCandidaturesCache = listUnsolicitedJobApplication;
			var datas = new Array();
			var btnProfileHandler = $("<button class='btn btn-danger btn-middle readCandidate viewProfile'/>");
			var btnCvHandler = $("<button class='btn btn-info btn-middle readCandidate viewCv'/>");
			var btnLetterHandler = $("<button class='btn btn-success btn-middle readCandidate viewLetter'/>");
			var listActions = $("<div></div>");
			var newHandler = $('<div class="label label-important not-read-candidate"></div>');
			btnProfileHandler.text(VIEWPROFILELABEL);
			btnCvHandler.text(VIEWCVLABEL);
			btnLetterHandler.text(VIEWLETTERLABEL);
			newHandler.text(NEWLABEL);
			for (var index in listUnsolicitedJobApplication) {
				var unsolicitedJobApplication = listUnsolicitedJobApplication[index];
				var unsolicitedJobApplicationId = unsolicitedJobApplication.getJobApplicationId();
				var urlCv = unsolicitedJobApplication.getUrlCv();
				var btnCv = null;
				var fullName = unsolicitedJobApplication.getFullName();
				if(unsolicitedJobApplication.getUnread()){
					fullName = that.getHtml(newHandler.attr("id", "notReaded_"+unsolicitedJobApplicationId)) + fullName;
				}
				btnProfileHandler.attr("index", index);
				btnLetterHandler.attr("index", index);
				if(urlCv.trim()!='' && urlCv!=null){
					btnCvHandler.attr("index", index);
					btnCv = that.getHtml(btnCvHandler);
				}

				listActions.attr("index", unsolicitedJobApplicationId);
				listActions.html(that.getHtml(btnProfileHandler)
						+ that.getHtml(btnCv)
						+ that.getHtml(btnLetterHandler));
				var data = [fullName, that.getHtml(listActions), unsolicitedJobApplicationId, 
				            unsolicitedJobApplication.getMotivationLetter()];
				datas.push(data);
			}
			that.addTableDatas('#listUnsolicitedCandidaturesTable', datas);
			$('#loadingEds').hide();
		};

		this.viewLetter = function(motivationLetter, userId, fullName, jobApplicationId, type, response) {
			idJobApplicationToChange = jobApplicationId;
			typeJobApplicationToChange = type;
			$('.popupMotivationLetter,.backgroundPopup').show();
			$('.popupMessageCandidature').hide();
			$('#letterContentCandidature').text(motivationLetter);
			$('#messageContentCandidature').val(response);
			$('#letterReply').unbind('click').bind('click', function() {
				$('.popupMotivationLetter').hide();
				$('#messageReceiverCandidature').val(fullName);
				$('.popupMessageCandidature').show();
			});
		};
		this.addListener = function(list) {
			listeners.push(list);
		};
		
		this.notifyUpdateJobApplication = function(messageXml) {
			$.each(listeners, function(i) {
				listeners[i].updateJobApplication(messageXml);
			});
		};
		
		this.notifyUpdateUnsolicitedJobApplication = function(messageXml) {
			$.each(listeners, function(i) {
				listeners[i].updateUnsolicitedJobApplication(messageXml);
			});
		};

		this.notifyReadJobApplication = function(jobApplicationId) {
			$.each(listeners, function(i) {
				listeners[i].readJobApplication(jobApplicationId);
			});
		};
		
		this.notifyReadUnsolicitedJobApplication = function(unsolicitedJobApplicationId) {
			$.each(listeners, function(i) {
				listeners[i].readUnsolicitedJobApplication(unsolicitedJobApplicationId);
			});
		};
	},

	CandidatureViewList : function(list) {
		if (!list)
			list = {};
		return $.extend(list);
	},
	
	CandidatureController : function(model, view) {
		var that=this;
		/**
		 * listen to the model
		 */
		var candidatureModelList = new $.CandidatureModelList({
			loadJobOffers : function(jobOffers){
				view.displayJobOffers(jobOffers);
			},
			loadJobApplication : function(list) {
				view.displayJobApplications(list);
			},
			loadUnsolicitedJobApplication : function(list) {
				view.displayUnsolicitedJobApplications(list);
			},
		});
		model.addListener(candidatureModelList);
		

		/**
		 * listen to the view
		 */
		var candidatureViewList = new $.CandidatureViewList({
			updateJobApplication : function(messageXml) {
				model.updateJobApplication(messageXml);
			},
			updateUnsolicitedJobApplication : function(messageXml) {
				model.updateUnsolicitedJobApplication(messageXml);
			},
			readJobApplication : function(jobApplicationId) {
				model.readJobApplication(jobApplicationId);
			},
			readUnsolicitedJobApplication : function(unsolicitedJobApplicationId) {
				model.readUnsolicitedJobApplication(unsolicitedJobApplicationId);
			},
		});
		view.addListener(candidatureViewList);
		this.initController = function(){
			view.initView();
			model.initModel();
		};
	}
});
