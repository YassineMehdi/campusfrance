jQuery
		.extend({

			PostersModel : function() {
				var listeners = new Array();
				var that = this;

				this.initModel = function() {
					that.getStandPosters();
				};
				this.getStandPostersSuccess = function(xml) {
					listPostersCache = [];
					$(xml).find("allfiles").each(function() {
						var poster = new $.Poster($(this));
						listPostersCache[poster.idPoster] = poster;
					});
					that.notifyLoadPosters(listPostersCache);
				};

				this.getStandPostersError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};

				this.getStandPosters = function() {
					$('#listStandPosters').empty();
					genericAjaxCall('GET', staticVars.urlBackEnd
							+ 'allfiles/standPosters?idLang=' + getIdLangParam(),
							'application/xml', 'xml', '', that.getStandPostersSuccess,
							that.getStandPostersError);
				};

				this.getAnalyticsPosters = function(callBack, path) {
					genericAjaxCall('GET', staticVars.urlBackEnd + path,
							'application/xml', 'xml', '', function(xml) {
								listPostersCache = [];
								$(xml).find("allfiles").each(function() {
									var poster = new $.Poster($(this));
									listPostersCache[poster.idPoster] = poster;
								});
								callBack();
							}, that.getStandPostersError);
				};

				this.getPosterById = function(posterId) {
					return listPostersCache[posterId];
				};

				this.addPosterSuccess = function(xml) {
					that.notifyAddPosterSuccess();
				};

				this.addPosterError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};

				this.addPoster = function(poster) {
					genericAjaxCall('POST', staticVars.urlBackEnd
							+ 'allfiles/addFile?idLang=' + getIdLangParam(),
							'application/xml', 'xml', poster.xmlData(),
							that.addPosterSuccess, that.addPosterError);
				};

				this.updatePosterSuccess = function(xml) {
					that.notifyUpdatePosterSuccess();
				};

				this.updatePosterError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};

				this.updatePoster = function(poster) {
					genericAjaxCall('POST', staticVars["urlBackEnd"]
							+ 'allfiles/updateFile?idLang=' + getIdLangParam(),
							'application/xml', 'xml', poster.xmlData(),
							that.updatePosterSuccess, that.updatePosterError);
				};

				this.deletePosterSuccess = function(xml) {
					that.notifyDeletePosterSuccess();
				};

				this.deletePosterError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};

				this.deletePoster = function(poster) {
					genericAjaxCall('GET', staticVars["urlBackEnd"]
							+ 'allfiles/deleteMedia/' + poster.getIdPoster(),
							'application/xml', 'xml', '', that.deletePosterSuccess,
							that.deletePosterError);
				};

				this.notifyLoadPosters = function(listPosters) {
					$.each(listeners, function(i) {
						listeners[i].loadPosters(listPosters);
					});
				};

				this.notifyAddPosterSuccess = function() {
					$.each(listeners, function(i) {
						listeners[i].addPosterSuccess();
					});
				};

				this.notifyUpdatePosterSuccess = function() {
					$.each(listeners, function(i) {
						listeners[i].updatePosterSuccess();
					});
				};

				this.notifyDeletePosterSuccess = function() {
					$.each(listeners, function(i) {
						listeners[i].deletePosterSuccess();
					});
				};

				this.addListener = function(list) {
					listeners.push(list);
				};
			},

			PostersModelListener : function(list) {
				if (!list)
					list = {};
				return $.extend(list);
			},

			PostersView : function() {
				var listeners = new Array();
				var idPosterToChange = 0;
				var imagePosterToChange = "";
				var urlPosterToChange = "";
				var fileNamePoster = "";
				var jqXHRPoster = null;
				var that = this;
				this.initView = function() {
					$('#addPosterDiv').hide();
        	traduct();
					idPosterToChange = 0;
					imagePosterToChange = "";
					urlPosterToChange = "";
					fileNamePoster = "";
					jqXHRPoster = null;
					$(".draggable").draggable({
						cancel : ".eds-dialog-inside"
					});
			    customJqEasyCounter("#posterTitle", 255, 255, '#0796BE', LEFTCARACTERES);
			    customJqEasyCounter("#posterDescription", 65535, 65535, '#0796BE', LEFTCARACTERES);
			    $("#posterForm td.jqEasyCounterMsg").css('width', '55%');
					$('#addPosterLink').click(
							function() {
								idPosterToChange = 0;
								urlPosterToChange = "";
								$('.popupPoster .editLabel').addClass('addLabel').removeClass(
										'editLabel').text(ADDLABEL);
								$('#posterForm').find('input').val("");
								$('#posterForm').find('textarea').val("");
								$('#imagePosterContainer').empty();
								$('#posterLoaded,#progressPoster,#loadPosterError').hide();
								$('#loadPoster, .popupPoster, .backgroundPopup').show();
								enabledButton('.savePosterButton');
							});

					$('.savePosterButton').click(function() {
						if ($('#posterForm').valid() && !isDisabled('.savePosterButton')) {
							disabledButton('.savePosterButton');
							if (urlPosterToChange != "") {
								$('#loadingEds').show();
								if (idPosterToChange == 0) {
									that.sendPosterInfos();
								} else {
									that.updatePosterInfos(idPosterToChange);
								}
							} else {
								enabledButton('.savePosterButton');
								$('#loadPosterError').show();
							}
						}
					});
					$('.closePopup,.eds-dialog-close').click(function(e) {
						e.preventDefault();
						$('.eds-dialog-container').hide();
						$('.backgroundPopup').hide();
						if (jqXHRPoster != null)
							jqXHRPoster.abort();
					});

					$('.deletePosterButton').click(function() {
						disabledButton('.deletePosterButton');
						that.deletePosterInfos(idPosterToChange);
					});
					$('#posterUpload').fileupload(
							{
								beforeSend : function(jqXHR, settings) {
									$('#imagePosterContainer').empty();
									beforeUploadFile('.savePosterButton', '#progressPoster',
											'#loadPoster,#loadPosterError');
								},
								datatype : 'json',
								cache : false,
								add : function(e, data) {
									var goUpload = true;
									var uploadFile = data.files[0];
									var fileName = uploadFile.name;
									timeUploadPoster = (new Date()).getTime();
									data.url = getUrlPostUploadFile(timeUploadPoster, fileName)
											+ "&isPoster=true";
									if (!(/\.(pdf|jpg|jpeg|png)$/i).test(fileName)) {
										goUpload = false;
									}
									if (uploadFile.size > MAXFILESIZE) {
										goUpload = false;
									}
									if (goUpload == true) {
										jqXHRPoster = data.submit();
									} else
										$('#loadPosterError').show();
								},
								progressall : function(e, data) {
									progressBarUploadFile('#progressPoster', data, 99);
								},
								done : function(e, data) {
									var file = data.files[0];
									var fileName = file.name;
									var hashFileName = getUploadFileName(timeUploadPoster,
											fileName);
									urlPosterToChange = getUploadFileUrl(hashFileName);
									fileNamePoster = fileName;
									if (getFileExtension(fileName) == "pdf") {
										imagePosterToChange = urlPosterToChange + '.png';
									} else
										imagePosterToChange = urlPosterToChange;
									var img_container = $('<img src="' + imagePosterToChange
											+ '" width="40" height="40"/>');
									$('#imagePosterContainer').prepend(img_container);
									afterUploadFile('.savePosterButton', '#posterLoaded',
											'#progressPoster');
								}
							});
				};

				this.displayPosters = function(listPosters) {
					var rightGroup = listStandRights[rightGroupEnum.POSTERS];
					if (rightGroup != null) {
						var permittedPostersNbr = rightGroup.getRightCount();
						var postersNbr = Object.keys(listPosters).length;
						if (permittedPostersNbr == -1) {
							$('#addPosterDiv #addPosterLink').text(ADDLABEL);
							$('#addPosterDiv').show();
						} else if (postersNbr < permittedPostersNbr) {
							$('#numberPosters').text(postersNbr);
			        $('#numberPermittedPosters').text(permittedPostersNbr);
							$('#addPosterDiv').show();
						} else {
							$('#addPosterDiv').hide();
						}
					}
					var poster = null;
					for ( var index in listPosters) {
						poster = listPosters[index];
						var posterId = poster.getIdPoster();
						var posterUrl = poster.getPosterUrl();
						var posterTitle = poster.getPosterTitle();
						var posterDescription = poster.getPosterDescription();
						var posterImage = poster.getImage();

						var data = {
							posterId : replaceSpecialChars(posterId),
							posterUrl : replaceSpecialChars(posterUrl),
							posterTitle : replaceSpecialChars(formattingLongWords(
									posterTitle, 70)),
							posterDescription : replaceSpecialChars(formattingLongWords(
									posterDescription, 255)),
							posterImage : replaceSpecialChars(posterImage),
							viewLabel : VIEWLABEL,
							editLabel : EDITLABEL,
							deleteLabel : DELETELABEL
						};
						$('#standPosterTemplate').tmpl(data).appendTo('#listStandPosters');
						$('.deletePoster' + poster.getIdPoster()).bind('click', function() {
							idPosterToChange = $(this).closest('.onePoster').attr('id');
							$('.popupDeletePoster,.backgroundPopup').show();
							enabledButton('.deletePosterButton');
						});

						$('.editPoster' + posterId).bind(
								'click',
								function() {
									idPosterToChange = $(this).closest('.onePoster').attr('id');
									var poster = listPosters[idPosterToChange];
									var posterUrl = poster.getPosterUrl();
									var posterTitle = poster.getPosterTitle();
									var posterDescription = poster.getPosterDescription();
									var posterImage = poster.getImage();
									imagePosterToChange = posterImage;
									urlPosterToChange = posterUrl;
									$('.popupPoster .addLabel').addClass('editLabel')
											.removeClass('addLabel').text(EDITLABEL);
									$('#imagePosterContainer').empty();
									$('#loadPoster').show();
									$('#posterLoaded,#loadPosterError').hide();
									var img_container = $('<img src="' + posterUrl
											+ '" width="40" height="40"/>');
									$('#imagePosterContainer').prepend(img_container);
									$('#posterTitle').val(posterTitle);
									$('#posterDescription').val(posterDescription);
									$('#progressPoster,#loadPosterError').hide();
									$('.popupPoster, .backgroundPopup').show();
									enabledButton('.savePosterButton');
								});

						$('.viewPoster' + posterId)
								.bind(
										'click',
										function() {
											var posterId = $(this).closest('.onePoster').attr('id');
											var poster = listPosters[posterId];
											var posterUrl = poster.getPosterUrl();
											var posterTitle = poster.getPosterTitle();
											var formatPosterTitle = formattingLongWords(posterTitle,
													70);
											var htmlPosterTitle = htmlEncode(posterTitle);
											var customPosterTitle = substringCustom(
													formatPosterTitle, 70);
											$('.popupPosterView,.backgroundPopup').show();
											$('#posterTitlePopup').text(customPosterTitle);
											$('#posterTitlePopup').attr('title', posterTitle);
											$('#herePoster').empty();
											var poster_container;
											if (compareToLowerString(posterUrl, ".pdf") != -1) {
												poster_container = $(
														'<object data="mozilla-pdf.js/web/viewer.html?file='
																+ posterUrl
																+ '#locale='
																+ localePDFJS
																+ '" type="text/html" width="100%" height="100%"></object>')
														.css('position', 'relative');
											} else {
												poster_container = $('<div style="width:100%; height:100%; overflow: auto;">'
														+ '<table style=" width: 100%; height: 100%;"><tbody><tr><td style="vertical-align: middle;text-align: center;">'
														+ '<img src="'
														+ posterUrl
														+ '" title="'
														+ htmlPosterTitle
														+ '" alt="'
														+ htmlPosterTitle
														+ '"  style="width:100%;height:auto;">'
														+ '</td></tr></tbody></table>' + '</div>');
											}
											$('#herePoster').prepend(poster_container);

											// $('#herePoster').attr('src','http://docs.google.com/gview?url='+posterUrl+'&embedded=true');
										});
					}
					$('#loadingEds').hide();
				};

				this.sendPosterInfos = function() {
					var posterTitle = $('#posterTitle').val();
					posterTitle = htmlEncode(posterTitle);
					var posterDescription = $('#posterDescription').val();
					posterDescription = htmlEncode(posterDescription);
					var poster = new $.Poster();
					poster.setPosterTitle(posterTitle);
					poster.setPosterDescription(posterDescription);
					poster.setPosterUrl(urlPosterToChange);
					poster.setImage(imagePosterToChange);
					poster.fileName = fileNamePoster;
					var fileType = new $.FileType();
					fileType.setFileTypeId(fileTypeEnum.POSTER);
					poster.setFileType(fileType);
					that.notifyAddPoster(poster);
				};

				this.updatePosterInfos = function(posterId) {
					var posterTitle = $('#posterTitle').val();
					posterTitle = htmlEncode(posterTitle);
					var posterDescription = $('#posterDescription').val();
					posterDescription = htmlEncode(posterDescription);
					var poster = listPostersCache[posterId];
					poster.setPosterTitle(posterTitle);
					poster.setPosterDescription(posterDescription);
					poster.setPosterUrl(urlPosterToChange);
					poster.setImage(imagePosterToChange);
					poster.fileName = fileNamePoster;
					that.notifyUpdatePoster(poster);
				};

				this.deletePosterInfos = function(posterId) {
					var poster = listPostersCache[posterId];
					that.notifyDeletePoster(poster);
				};
				this.addPosterSuccess = function() {
					hidePopups();
					showUpdateSuccess();
					that.posterUpdated();
					// enabledButton('.savePosterButton');
				};

				this.updatePosterSuccess = function() {
					hidePopups();
					showUpdateSuccess();
					that.posterUpdated();
					// enabledButton('.savePosterButton');
				};

				this.deletePosterSuccess = function() {
					hidePopups();
					showUpdateSuccess();
					that.posterUpdated();
					// enabledButton('.deletePosterButton');
				};

				this.notifyAddPoster = function(poster) {
					$.each(listeners, function(i) {
						listeners[i].addPoster(poster);
					});
				};

				this.notifyUpdatePoster = function(poster) {
					$.each(listeners, function(i) {
						listeners[i].updatePoster(poster);
					});
				};

				this.notifyDeletePoster = function(poster) {
					$.each(listeners, function(i) {
						listeners[i].deletePoster(poster);
					});
				};

				this.posterUpdated = function() {
					$.each(listeners, function(i) {
						listeners[i].posterUpdated();
					});
				};

				this.addListener = function(list) {
					listeners.push(list);
				};
			},

			PostersViewListener : function(list) {
				if (!list)
					list = {};
				return $.extend(list);
			},

			PostersController : function(model, view) {
				/**
				 * listen to the model
				 */
				var postersModelList = new $.PostersModelListener({
					loadPosters : function(listPosters) {
						view.displayPosters(listPosters);
					},
					addPosterSuccess : function(photo) {
						view.addPosterSuccess(photo);
					},
					updatePosterSuccess : function(poster) {
						view.updatePosterSuccess(poster);
					},
					deletePosterSuccess : function() {
						view.deletePosterSuccess();
					},
				});
				model.addListener(postersModelList);
				this.initController = function() {
					view.initView();
					model.initModel();
				};
				/**
				 * listen to the view
				 */
				var postersViewList = new $.PostersViewListener({
					addPoster : function(poster) {
						model.addPoster(poster);
					},
					updatePoster : function(poster) {
						model.updatePoster(poster);
					},
					deletePoster : function(poster) {
						model.deletePoster(poster);
					},
					posterUpdated : function() {
						model.getStandPosters();
					},
				});
				view.addListener(postersViewList);

				this.getAnalyticsPosters = function(callBack, path) {
					model.getAnalyticsPosters(callBack, path);
				};
				this.getPosterById = function(posterId) {
					return model.getPosterById(posterId);
				};
			}
		});
