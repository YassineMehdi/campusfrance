 var idContactToChange = 0;
var enterpriseLogo = "";
var jqXHRContactPhoto=null;
var coordinates = [];
var nameFile;
var editor = "";

function sendCoordinates() {
	nbreWSCallsInProgress++;
	$('#loadingEds').show();
	$.ajax({
        type : 'POST',
    url : staticVars["urlBackEnd"] + "stand/cropPoster",
    dataType : 'json',
    cache : false,
    data : ( {
        x1 : $('.input0').val(),
        y1 : $('.input1').val(),
        x2 : $('.input0').val(),
        y2 : $('.input1').val(),
        w : $('.input4').val(),
        h : $('.input5').val(),
        fileName : nameFile
    }),
    success : function(data) {
        $('.popupJracImage').hide();
        $('.backgroundPopup').hide();
        $('#enterpriseLogo').attr('src', data.path);
       /*uploadToAmazon();*/
        },
		statusCode : {
			401: exceptionUnAutorized
		}
    }).complete(function() {
		  nbreWSCallsInProgress--;
		  if(nbreWSCallsInProgress<1) {
			  $('#loadingEds').hide();
			  nbreWSCallsInProgress=0;
		  }
	});
}

function fillForumSectors() {
	genericAjaxCall('GET',staticVars["urlBackEnd"] + 'secteuract?idLang='+getIdLangParam(), 'application/xml', 'xml','',function(xml) {
	    parseXmlSector(xml);
		} ,'');
}

function parseXmlSector(xml) {
    $(xml).find("secteurActDTO").each(function() {
    	$('#sector').append("<option value=" + $(this).find('idsecteur').text() + ">" + $(this).find('name').text() + "</option>");
    });
}

function parseXmlStand(xml) {
    var standDTOlist = $.xml2json(xml);
    standId = standDTOlist.stand.idstande;
    $('#standName').val(formattingLongWords(standDTOlist.stand.name, 50));
	$('#enterpriseName').val(formattingLongWords(standDTOlist.stand.enterpriseDTO.nom, 50));
	$('#enterprisePlace').val(formattingLongWords(standDTOlist.stand.enterpriseDTO.lieu, 50));
	$('#creationDate').val(standDTOlist.stand.enterpriseDTO.datecreation);
	$('#employeeNumber').val(standDTOlist.stand.enterpriseDTO.nbremploye);
	$('#enterpriseSlogan').val(standDTOlist.stand.enterpriseDTO.slogan);
	$('#enterpriseAddress').val(formattingLongWords(standDTOlist.stand.enterpriseDTO.address, 50));
	$('#postalCode').val(standDTOlist.stand.enterpriseDTO.postalCode);
	$('#contactEmail').val(formattingLongWords(standDTOlist.stand.enterpriseDTO.contactEmail, 50));
	//$('#profileDesc').val(standDTOlist.stand.enterpriseDTO.description);
	if(unescape(standDTOlist.stand.enterpriseDTO.description)!=""){
		setTimeout(function(){
			//editordesc.setData(unescape(standDTOlist.stand.enterpriseDTO.description));
			editordesc.val(unescape(standDTOlist.stand.enterpriseDTO.description));
		}, 0);
	}
	//$('#profileSearched').val(standDTOlist.stand.profilesplusrecherche);
	//$editor.setData(unescape(standDTOlist.stand.profilesplusrecherche));
	$('#socialWallMessage').val(formattingLongWords(standDTOlist.stand.socialWallMessage, 50));
	$('#webSite').val(formattingLongWords(standDTOlist.stand.enterpriseDTO.website, 50));
	$('#careerWebsite').val(formattingLongWords(standDTOlist.stand.enterpriseDTO.careerwebsite, 50));
	$('#enterpriseLogo').attr('src', standDTOlist.stand.enterpriseDTO.logo);
	enterpriseLogo = standDTOlist.stand.enterpriseDTO.logo;
	$('#standDescImage').attr('src', standDTOlist.stand.imagedescription);
	$('#siegeName').val(formattingLongWords(standDTOlist.stand.enterpriseDTO.siegeName, 50));
	$('#siegeAddress').val(formattingLongWords(standDTOlist.stand.enterpriseDTO.siegeAddress, 50));
	$('#siegePlace').val(formattingLongWords(standDTOlist.stand.enterpriseDTO.siegePlace, 50));
	$('#siegeNbremployee').val(standDTOlist.stand.enterpriseDTO.siegeNbremployee);
	$('#siegeCreationDate').val(standDTOlist.stand.enterpriseDTO.siegeCreationDate);
	$('#siegePostalCode').val(standDTOlist.stand.enterpriseDTO.siegePostalCode);
	//var contactDTOList = standDTOlist.stand.contactDTOList.contact;
//	$("#listSectors").empty();
//	$(xml).find("secteurActDTO").each(function() {
//	    var idSector = $(this).find("idsecteur").text();
//	    var interestdata = {
//	        sector_val : replaceSpecialChars($(this).find("idsecteur").text()),
//	        sector_text : replaceSpecialChars($(this).find("name").text()),
//	        removeLabel : REMOVELABEL
//	    };
//	    $("#sector option[value=" + $(this).find('idsecteur').text() + "]").remove();
//	    $("#tempSector").tmpl(interestdata).appendTo("#listSectors");
//	    $('.removeSect'+idSector).bind('click', function() {
//	        $("#sector").append($('<option/>').val($(this).parent().parent().attr('class')).html($(this).parent().parent().find('td').eq(0).text()));
//	        $(this).parent().parent().remove();
//	    });
//	});
	$("#listSocial").empty();
	if ($(xml).find("website").find("nom").length > 0) {
	    $(xml).find("website").each(function() {
	        if($(this).find("nom").text()!=""){
	            var websiteName = capitaliseFirstLetter($(this).find("nom").text().trim());
	            var url = $(this).find("url").text().trim();
	            $("#websiteSocial option[value=" + websiteName + "]").remove();
	            var interestdata = {
	                website_social : replaceSpecialChars(websiteName),
	    		url_social : replaceSpecialChars(url),
	    		url_social_formatted : formattingLongWords(replaceSpecialChars(url), 50),
	                saveLabel : SAVELABEL,
	                cancelLabel : CANCELLABEL
	            };
	            var socialHandler = $("#tempSocial").tmpl(interestdata).appendTo("#listSocial");
	            //$('.removeSoc').bind('click', function() {
	            $('#'+websiteName).bind('click', function() {
	                //$(this).parent().parent().remove();
	                $(this).closest('table').remove();
	                $("#websiteSocial").append($('<option/>').val(websiteName).html(websiteName));
	            });
	            $('.edit'+websiteName).bind('click',function(){
	                //console.log('.edit'+websiteName);
	            	$('.input'+websiteName).show();
	                $('.restoreTd'+websiteName).show();
	                $('.editDelete'+websiteName).hide();
	                $('.label'+websiteName).hide();
	            });
	            $('.confirm'+websiteName).bind('click',function(){
	            	//console.log($('.content'+websiteName).val());
	            	if(removeSpaces($('.content'+websiteName).val())=="" || (removeSpaces($('.content'+websiteName).val()).indexOf("http://") == -1 && removeSpaces($('.content'+websiteName).val()).indexOf("https://") == -1) ){
	            		$('.content'+websiteName).focus();
	        	    	$(".errorValidateField").remove();
	        	    	$('.content'+websiteName).after('<label class="error errorValidateField" style="display: block;">Veuillez entrer une URL valide.</label>');
	        	    }else{
	        	    	$('.label'+websiteName).text(formattingLongWords(replaceSpecialChars(removeSpaces($('.content'+websiteName).val())), 50));
	        	    	$('.input'+websiteName).hide();
	        	    	$(".errorValidateField").remove();
	        	    	$('.restoreTd'+websiteName).hide();
	        	    	$('.editDelete'+websiteName).show();
	        	    	$('.label'+websiteName).show();
	        	    }
	            });
	            $('.restore'+websiteName).bind('click',function(){
	                $('.content'+websiteName).val(url);
	                $('.input'+websiteName).hide();
	                $('.restoreTd'+websiteName).hide();
	                $('.editDelete'+websiteName).show();
	                $('.label'+websiteName).show();
	            });
	            customJqEasyCounter(socialHandler.find("input"), 255, 255, '#0796BE', LEFTCARACTERES);
	    		$(socialHandler.find(" div.jqEasyCounterMsg")).css('width', '55%');
	        }
	    });
	    
	}
	$('#loadingEds').hide();
}

 function getStandInfos() {
    $("#listSectors").empty();
    $("#listSocial").empty();
    genericAjaxCall('GET',staticVars["urlBackEnd"] + 'stand/standInfo?idLang='+getIdLangParam(), 'application/xml', 'xml','',function(xml) {
	    parseXmlStand(xml);
	    } ,'');
 } 

function addSocial() {
    	var websiteName = capitaliseFirstLetter($('#websiteSocial').val().trim());
	var urlSocial = $('#urlSocial').val();
	var interestdata = {
	    website_social : replaceSpecialChars(websiteName),
	    url_social : replaceSpecialChars(urlSocial),
	    url_social_formatted : formattingLongWords(replaceSpecialChars(urlSocial), 50),
	    saveLabel : SAVELABEL,
	    cancelLabel : CANCELLABEL
	};
	$("#websiteSocial option[value=" + websiteName + "]").remove();
	$("#tempSocial").tmpl(interestdata).prependTo("#listSocial");
	$('#'+websiteName).bind('click', function() {
	    $("#websiteSocial").append($('<option/>').val(websiteName).html(websiteName));
	    $(this).closest('table').remove();
	});
	$('.edit'+websiteName).bind('click',function(){
	    //console.log('.edit'+websiteName);
		$('.input'+websiteName).show();
	    $('.restoreTd'+websiteName).show();
	    $('.editDelete'+websiteName).hide();
	    $('.label'+websiteName).hide();
	});
	$('.confirm'+websiteName).bind('click',function(){
	    if(removeSpaces($('.content'+websiteName).val())=="" || (removeSpaces($('.content'+websiteName).val()).indexOf("http://") == -1 && removeSpaces($('.content'+websiteName).val()).indexOf("https://") == -1) ){
    		$('.content'+websiteName).focus();
	    	$(".errorValidateField").remove();
	    	$('.content'+websiteName).after('<label class="error errorValidateField" style="display: block;">Ajouter un URL valide</label>');
	    }else{
	    	$(".errorValidateField").remove();
	       	$('.label'+websiteName).text(formattingLongWords(replaceSpecialChars(removeSpaces($('.content'+websiteName).val())), 50));
		$('.input'+websiteName).hide();
		$('.restoreTd'+websiteName).hide();
		$('.editDelete'+websiteName).show();
		$('.label'+websiteName).show();
	    }
	    
	});
	$('.restore'+websiteName).bind('click',function(){
	    $('.content'+websiteName).val(urlSocial);
	    $('.input'+websiteName).hide();
	    $('.restoreTd'+websiteName).hide();
	    $('.editDelete'+websiteName).show();
	    $('.label'+websiteName).show();
	});
	$('#urlSocial').val("");
}

function addSector() {
    var idSector = $('#sector').val();
	var interestdata = {
	    sector_val : replaceSpecialChars($('#sector').val()),
	    sector_text : replaceSpecialChars($('#sector option:selected').text()),
	    removeLabel : REMOVELABEL
	};
	$("#sector option[value=" + $('#sector').val() + "]").remove();
	    $("#tempSector").tmpl(interestdata).appendTo("#listSectors");
	    $('.removeSect'+idSector).bind('click', function() {
        $("#sector").append($('<option/>').val($(this).parent().parent().attr('class')).html($(this).parent().parent().find('td').eq(0).text()));
            $(this).parent().parent().remove();
        });
}



function sendStandInfo() {
    //var profileDesc = editordesc.getData();
    var profileDesc = editordesc.val();
    var sectXml = "";
	var contactXml = "";
	var webSiteXml = "";
	$('#listSectors tr').each(function() {
	    sectXml = sectXml + "<secteurActDTO><idsecteur>" + $(this).attr('class') + "</idsecteur></secteurActDTO>";
	});
	$('#listSocial tr').each(function() {
	    webSiteXml = webSiteXml + "<website><nom>" + htmlEncode($(this).find('.websiteTitle').text().trim()) + "</nom><url>" + htmlEncode($(this).find('.contentSocialInput').val().trim()) + "</url></website>";
	});
	var enterpriseXml = "<logo>"+htmlEncode(enterpriseLogo)+"</logo><postalCode>"+htmlEncode($('#postalCode').val())+"</postalCode><contactEmail>"+htmlEncode($('#contactEmail').val())+"</contactEmail><siegePostalCode>"+htmlEncode($('#siegePostalCode').val())+"</siegePostalCode><website>" + htmlEncode($('#webSite').val()) + "</website><careerwebsite>" + htmlEncode($('#careerWebsite').val()) + "</careerwebsite><address>" + htmlEncode($('#enterpriseAddress').val()) + "</address><datecreation>" + $('#creationDate').val() + "</datecreation><description>" + escape(profileDesc) + "</description><lieu>" + htmlEncode($('#enterprisePlace').val()) + "</lieu><nbremploye>" + htmlEncode($('#employeeNumber').val()) + "</nbremploye><nom>" + htmlEncode($('#enterpriseName').val()) + "</nom><slogan>" + htmlEncode($('#enterpriseSlogan').val()) + "</slogan><siegeName>"+htmlEncode($('#siegeName').val())+"</siegeName><siegeAddress>"+htmlEncode($('#siegeAddress').val())+"</siegeAddress><siegeNbremployee>"+htmlEncode($('#siegeNbremployee').val())+"</siegeNbremployee><siegePlace>"+htmlEncode($('#siegePlace').val())+"</siegePlace><siegeCreationDate>"+htmlEncode($('#siegeCreationDate').val())+"</siegeCreationDate>";
	var standXml = "<stand>"
		//+"<activitiesDTOList>" + sectXml + "</activitiesDTOList>"
		+"<contactDTOList>" + contactXml + "</contactDTOList><enterpriseDTO>"+enterpriseXml+"</enterpriseDTO><name>" + htmlEncode($('#standName').val()) + "</name><socialWallMessage>" + htmlEncode($('#socialWallMessage').val()) + "</socialWallMessage><websiteDTOList>" + webSiteXml + "</websiteDTOList></stand>";
	
	genericAjaxCall('POST',staticVars["urlBackEnd"] + 'stand/update?idLang='+getIdLangParam(), 'application/xml', 'xml',standXml,function(xml) {
		$('#loadingEds').hide();
	    $('html, body').animate({
	        scrollTop : 0
	    }, 'slow');
	    showUpdateSuccess();
	} ,'');
}
function validateEnterpriseForm() {
    $("#enterpriseProfileForm").validate({
        rules : {
            employeeNumber : {
                number : true,
            },
            careerWebsite : {
            	url: true,
            },
            webSite : {
            	url: true,
            }
        }
    });
}

function validateContactForm() {
    $("#contactForm").validate({
    	rules : {
    		contactTel : {
    			number : true,
    		},
    	}
    });
}
function validateSocialForm() {
    $("#urlSocialForm").validate({
    });
}
function addSocialContact() {
    var name = $('#websiteSocialContact').val().trim();
    var interestdata = {
	    website_social : replaceSpecialChars($('#websiteSocialContact').val()),
	    url_social : replaceSpecialChars($('#urlSocialContact').val())
	};
	$("#websiteSocialContact option[value=" + $('#websiteSocialContact').val() + "]").remove();
	$("#tempSocialContact").tmpl(interestdata).appendTo(".listLinks");
	$('.removeSocContact'+name).bind('click', function() {
	    $("#websiteSocialContact").append($('<option/>').val(name).html(name));
	    $(this).parent().parent().remove();
	});
	
	                 $('#edit'+name).bind('click', function() {
	     $('#input'+name).show();
	     $('#label'+name).hide();
	     $('.editDeleteTd'+name).hide();
	     $('.restoreTd'+name).show();
	     
	});
	
	  $('#confirm'+name).bind('click', function() {
	     $('#label'+name).text(removeSpaces($('#input'+name).val()));
	     $('#input'+name).hide();
	     $('#label'+name).show();
	     $('.editDeleteTd'+name).show();
	     $('.restoreTd'+name).hide();
	     
	});
	
	   $('#input'+name).bind('blur', function() {
	     $('#label'+name).text(removeSpaces($('#input'+name).val()));
	     $('#input'+name).hide();
	     $('#label'+name).show();
	     $('.editDeleteTd'+name).show();
	     $('.restoreTd'+name).hide();
	     
	});
	
	 $('#restore'+name).bind('click', function() {
	     $('#input'+name).hide();
	     $('#label'+name).show();
	     $('.editDeleteTd'+name).show();
	     $('.restoreTd'+name).hide();
	     
	});
	
	
	$('#urlSocialContact').val("");
}
$(document).ready(function() {
	$('html, body').animate({
		scrollTop : 0
	}, 'slow');
   $('.confirmDeleteContact').click(function(){
		deleteContact(idContactToChange);
   });
   $('#progressContactPhoto,#loadContactError').hide();
   $('#contactPhotoUpload').fileupload({
	   	beforeSend : function(jqXHR,settings ){
	    	beforeUploadFile('#addContactButton', '#progressContactPhoto', '#contactUpload .filebutton,#loadContactError');
	    },
	    datatype: 'json',
	    cache : false,
	    add : function(e, data) {
	        var goUpload = true;
	        var uploadFile = data.files[0];
			var fileName=uploadFile.name;
			timeUploadContact=(new Date()).getTime();
			data.url = getUrlPostUploadFile(timeUploadContact, fileName);
	        if (!(/\.(gif|jpg|jpeg|tiff|png)$/i).test(fileName)) {
	            goUpload = false;
	        }
	        if (uploadFile.size > MAXFILESIZE) {// 6mb
	            goUpload = false;
	        }
	        if (goUpload == true) {
	        	jqXHRContactPhoto=data.submit();
	        }else $('#loadContactError').show();
	    },
	    progressall: function (e, data) {
	        progressBarUploadFile('#progressContactPhoto', data, 100);
	    },
	    done : function(e, data) {
	    	var file=data.files[0];
	    	var fileName=file.name;
	    	var hashFileName=getUploadFileName(timeUploadContact, fileName);
	    	//console.log("Add contact photo file done, filename :"+fileName+", hashFileName : "+hashFileName);
	    	contactPhoto = getUploadFileUrl(hashFileName);
	    	$('#contactPhoto').attr('src', contactPhoto);
			afterUploadFile('#addContactButton', '#contactUpload .filebutton', '#progressContactPhoto');
	    }
	});
	$('.closePopup,.backgroundPopup').click(function() {
	    hidePopups();
	    if(jqXHRContactPhoto!=null) jqXHRContactPhoto.abort();//Stop upload file
	});
	//var config = {};
	//$editor = CKEDITOR.replace('profileSearched');
  

	 $(".draggable").draggable({ cancel: ".eds-dialog-inside" });
	 $('.addSocContact').click(function(e) {
	    e.preventDefault();
	    if(removeSpaces($('#urlSocialContact').val())!=""){
	        addSocialContact();
	    }
	    else {
	        $('#urlSocialContact').focus();
	    }
	 });

	
	//var currentYear = new Date().getFullYear();
	for(var i = 1850; i <= 2015; ++i) {
	$('#creationDate').append($('<option/>').val(i).html(i));
	}
	for(var i = 1850; i <= 2015; ++i) {
	$('#siegeCreationDate').append($('<option/>').val(i).html(i));
	}
	//$('#creationDate').selectpicker({});

    $('#isSiege').click(function(){
		if($(this).is(':checked')){
			//$('.siegeInfos').hide();
			$('#siegeName').val($('#enterpriseName').val());
			$('#siegeNbremployee').val($('#employeeNumber').val());
			$('#siegePlace').val($('#enterprisePlace').val());
			$('#siegeAddress').val($('#enterpriseAddress').val());
			$('#siegeCreationDate').val($('#creationDate').val());
			$('#siegePostalCode').val($('#postalCode').val());
		}else {
		    $('#siegeName').val("");
			$('#siegeNbremployee').val("");
			$('#siegePlace').val("");
			$('#siegeAddress').val("");
			   // $('#siegeCreationDate').val($('#creationDate').val());
			$('#siegePostalCode').val("");
		}
	});
	$("#profileSearched").jqEasyCounter({
	    'maxChars' : 3000,
	    'maxCharsWarning' : 3000,
	    'msgFontColor' : '#0796BE',
	    'msgCaracters' : LEFTCARACTERES,
	});
	
	$("#profileDesc").jqEasyCounter({
	    'maxChars' : 3000,
	    'maxCharsWarning' : 3000,
	    'msgFontColor' : '#0796BE',
	    'msgCaracters' : LEFTCARACTERES,
	});
	
	$("#enterpriseName, #standName, #enterpriseAddress, " +
			"#enterprisePlace, #contactEmail, #siegePlace, " +
			"#siegeName, #siegeAddress, #enterpriseSlogan, " +
			"#urlSocial, #contactJobTitle").jqEasyCounter({
		'maxChars' : 255,
		'maxCharsWarning' : 255,
		'msgFontColor' : '#0796BE',
		'msgCaracters' : LEFTCARACTERES,
	});
	
	$("#socialWallMessage, #webSite, #careerWebsite, #contactName, " +
			"#emailContact").jqEasyCounter({
		'maxChars' : 200,
		'maxCharsWarning' : 200,
		'msgFontColor' : '#0796BE',
		'msgCaracters' : LEFTCARACTERES,
	});
	
	$("#postalCode, #siegePostalCode,#siegePostalCode").jqEasyCounter({
		'maxChars' : 10,
		'maxCharsWarning' : 10,
		'msgFontColor' : '#0796BE',
		'msgCaracters' : LEFTCARACTERES,
	});

	$("#employeeNumber,#siegeNbremployee").jqEasyCounter({
		'maxChars' : 9,
		'maxCharsWarning' : 9,
		'msgFontColor' : '#0796BE',
		'msgCaracters' : LEFTCARACTERES,
	});

	$("#contactTel").jqEasyCounter({
		'maxChars' : 50,
		'maxCharsWarning' : 50,
		'msgFontColor' : '#0796BE',
		'msgCaracters' : LEFTCARACTERES,
	});
	
	
	$("#enterpriseProfileForm td.jqEasyCounterMsg").css('width', '55%');
	
	validateContactForm();
	validateEnterpriseForm();
	validateSocialForm();
	getContacts();
	setTimeout(getStandInfos, 0);
	fillForumSectors();
	 $('.saveStandButton').click(function() {
		if(!isDisabled('.saveStandButton')) {
		    if ($("#enterpriseProfileForm").valid()) {
		    	$("html, body").animate({
		            scrollTop : 0
		        }, "slow");
		        $('#loadingEds').show();
		    	sendStandInfo();
		    } else {
		    	$('#loadingEds').hide();
		    	$("html, body").animate({
		            scrollTop : 100
		        }, "slow");
		    }
	    } else {
	    	$('#loadingEds').hide();
	    	$("html, body").animate({
	            scrollTop : 600
	        }, "slow");
	    }
	});
 	$('#progressLogoUpload,#loadLogoStandError').hide();
    $('#logoUpload').fileupload({
    	beforeSend : function(jqXHR,settings ){
        	beforeUploadFile('.saveStandButton', '#progressLogoUpload', '#logoUploadStand .filebutton,#loadLogoStandError');
    	},
        datatype: 'json',
        cache : false,
    	add : function(e, data) {
            var goUpload = true;
            var uploadFile = data.files[0];
    		var fileName=uploadFile.name;
    		timeUploadLogo=(new Date()).getTime();
			data.url = getUrlPostUploadFile(timeUploadLogo, fileName);
            if (!(/\.(gif|jpg|jpeg|tiff|png)$/i).test(fileName)) {
                goUpload = false;
            }
            if (uploadFile.size > MAXFILESIZE) {// 6mb
                goUpload = false;
            }
            if (goUpload == true) {
                data.submit();
            }else $('#loadLogoStandError').show();
        },
        progressall: function (e, data) {
            progressBarUploadFile('#progressLogoUpload', data, 100);
        },
        done : function(e, data) {
        	var file=data.files[0];
        	var fileName=file.name;
        	var hashFileName=getUploadFileName(timeUploadLogo, fileName);
        	//console.log("Add logo stand file done, filename :"+fileName+", hashFileName : "+hashFileName);
        	enterpriseLogo = getUploadFileUrl(hashFileName);
            $('#enterpriseLogo').attr('src', enterpriseLogo);
    		afterUploadFile('.saveStandButton', '#logoUploadStand .filebutton', '#progressLogoUpload');
             /*$('.popupJracImage').show();
                $('.backgroundPopup').show();
                nameFile = data.name;
                $('#jracImage').attr('src', data.path);
                $('.pane #jracImage').jrac({
                    'crop_width' : 340,
                    'crop_height' : 100,
                    'crop_x' : 100,
                    'crop_y' : 40,
                    'image_width' : 400,
                    'crop_resize' : false,
                    'viewport_resize' : false,
                    'viewport_onload' : function() {
                        var $viewport = this;
                        var inputs = $viewport.$container.parent('.pane').find('.coords input:text');
                        var events = ['jrac_crop_x', 'jrac_crop_y', 'jrac_crop_width', 'jrac_crop_height', 'jrac_image_width', 'jrac_image_height'];
                        for (var i = 0; i < events.length; i++) {
                            var event_name = events[i];
                            // Register an event with an element.
                            $viewport.observator.register(event_name, inputs.eq(i));
                            // Attach a handler to that event for the element.
                            inputs.eq(i).bind(event_name, function(event, $viewport, value) {
                                $(this).val(value);
                                coordinates[i] = value;
                            })
                            // Attach a handler for the built-in jQuery change event, handler
                            // which read user input and apply it to relevent viewport object.
                            .change(event_name, function(event) {
                                var event_name = event.data;
                                $viewport.$image.scale_proportion_locked = $viewport.$container.parent('.pane').find('.coords input:checkbox').is(':checked');
                                $viewport.observator.set_property(event_name, $(this).val());
                            });
                        }
                    }
                });*/
        }
    });

    $('.cropImage').click(function() {
        sendCoordinates();
    });


   /*function uploadToAmazonContact() {
   	alert('contact amazon');
        $.ajax({
            type : 'POST',
            url : staticVars["urlBackEnd"] + 'contact/uploadAmazon',
            dataType : 'xml',
            success : function() {
            }
        });
    }*/

    $('#addContactButton').click(function(e) {
	    e.preventDefault();
	    if ($("#contactForm").valid() && !isDisabled('#addContactButton')) {
			disabledButton('#addContactButton');
	    	if(isContactUpdate){
	    		updateContact();
	    	}else {
	    		addContact();
	    	}
	    }
	});
//	$('.addSect').click(function(e) {
//	    e.preventDefault();
//	    addSector();
//	});
	$('.addSoc').click(function(e) {
		//console.log('Add a social link');
	    e.preventDefault();
	    if(removeSpaces($('#urlSocial').val())=="" || (removeSpaces($('#urlSocial').val()).indexOf("http://") == -1 && removeSpaces($('#urlSocial').val()).indexOf("https://") == -1) ){
	    	$('#urlSocial').focus();
	    	$(".errorValidateField").remove();
	    	$('#urlSocial').after('<label class="error errorValidateField" style="display: block;">Veuillez entrer une URL valide.</label>');
	    }
	    else {
	    	$(".errorValidateField").remove();
	    	addSocial();
	    }
	});
	$('.addContactLink').click(function(e) {
		contactPhoto = staticVars.defaultImage;
		$('#contactPhoto').attr('src', staticVars.defaultImage);
		isContactUpdate = false;
		$('.popupContact input').val('');
	    e.preventDefault();
	    $('.popupContact,.backgroundPopup,#contactDefaultImage').show();
	    $('#progressContactPhoto,#loadContactError').hide();
	    enabledButton('#addContactButton');
	});
});


