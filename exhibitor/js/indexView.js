jQuery
		.extend({
			VisitCardManage : function (favorite, view) {
				var componentId = favorite.getComponent().getComponentId();
			    var visitor = null;
			    var visitorEmail = "";
			    var visitorPhone = "";
			    var visitorAdress = "";
			    var visitorWebsite = "";
			    var visitorJobTitle = "";
			    var visitorEnterprise = "";
			    var enterprise = "";
				var webSite = "";
			    var address = "";
			    var actions = "";
			    
			    if (componentId == FavoriteIdsEnum.VISIT_CARD_CANDIDATE_ID) {
			    	visitor = favorite.getCandidate();
			        if(visitor.getCountry().getCountryName() != ""){
			        	address = visitor.getPlace() + ", " + visitor.getCountry().getCountryName() ;
			        }
					webSite = visitor.getUserProfile().getWebSite();
					classUserType = "participant-visit-card";
					enterprise = visitor.getCurrentCompany();
			    } else if (componentId == FavoriteIdsEnum.VISIT_CARD_ENTERPRISE_P_ID) {
			    	visitor = favorite.getEnterpriseP();
			        if(visitor.getEnterprise().getAddress() != ""){
			        	address = visitor.getEnterprise().getAddress();
			        }
			      	classUserType = "exibitor-visit-card";
			      	enterprise = visitor.getEnterprise().getSiegName();
			      	webSite = visitor.getEnterprise().getWebsite();
			    } 
			    if(VisitCardStatusEnum.INVITER == favorite.getStatus()){
			    	actions = $('<a href="#" class="btn pending-visit-card">' + DEMAND_IN_PROGRESS_LABEL + '</a>');
			    }else if(VisitCardStatusEnum.GUEST == favorite.getStatus()){
			    	actions = $('<a href="#" class="btn btn-primary accept-visit-card">' + ACCEPT_LABEL + '</a><a href="#" class="btn btn-danger refuse-visit-card">' + REFUSE_LABEL + '</a>');
			    }
			    visitorJobTitle = visitor.getJobTitle();
			    visitorEmail = $('<div class="control-group visitCardEmail"><i class="fa fa-envelope"></i>'+htmlEncode(visitor.getEmail())+'</div>');
			    visitorPhone = $('<div class="control-group visitCardPhone"><i class="fa fa-phone"></i>'+htmlEncode(visitor.getCellPhone())+'</div>');
			    visitorWebsite = $('<div class="control-group visitCardWebSite"><i class="fa fa-chrome"></i>' + htmlEncode(webSite)+'</div>');
			    visitorEnterprise = $('<div class = "control-group visitCardEnterprise"><i class = "fa fa-building-o"></i>'+htmlEncode(enterprise)+'</div>');
			    visitorAdress = $('<div class ="intro-offre-detail"><i class = "fa fa-home"></i>'+htmlEncode(address)+'</div>');
			    if(visitor.getEmail() == "" || visitor.getEmail() == undefined) visitorEmail = $(visitorEmail).hide();
			    if(visitor.getCellPhone() == "" || visitor.getCellPhone() == undefined) visitorPhone = $(visitorPhone).hide();
			    if(webSite == "" || webSite == undefined) visitorWebsite = $(visitorWebsite).hide();
			    if(enterprise == "") visitorEnterprise = $(visitorEnterprise).hide();
			    if(address == "") visitorAdress = $(visitorAdress).hide();

			    var dom = $('<div class="span6 visit-card '+classUserType+'">'
				            + '<div class="visitorHeader row-fluid">'
				            	+'<div class="span8">'
					            	+'<div class="visitorFullName visitor-full-name">'
					            		+ htmlEncode(visitor.getUserProfile().getFullName())
					            	+'</div>'
					            	+'<div class="job-title">'
					            		+ htmlEncode(visitorJobTitle)
					            	+'</div>'
					            +'</div>'
				            	+'<div class="span4 no-padding">'
				            		+'<img class="logo" src="'+htmlEncode(visitor.getUserProfile().getAvatar())+'" alt="">'
				            	+'</div>'
				            + '</div>'
				            + '<div class="visitorDetails row-fluid">'
			                      + getHtml(visitorPhone)
			                      + getHtml(visitorEmail)
			                      + getHtml(visitorEnterprise)
			                      + getHtml(visitorWebsite)
			                      + getHtml(visitorAdress)
			                      + getHtml(actions)
				            + '</div>'
				          + '</div>');
	
			    this.refresh = function () {
			      $(dom).find(".accept-visit-card").unbind("click").bind("click", function(){
		          	var componentName = "";
		          	if (componentId == FavoriteIdsEnum.VISIT_CARD_CANDIDATE_ID) {
		          		componentName = FavoriteEnum.VISIT_CARD_CANDIDATE;
		          	}else if (componentId == FavoriteIdsEnum.VISIT_CARD_ENTERPRISE_P_ID) {
		          		componentName = FavoriteEnum.VISIT_CARD_ENTERPRISE_P;
		          	}
		            var favoriteXml = factoryXMLFavory(visitor.getUserProfile().getUserProfileId(), componentName);
		            addVisitCard(favoriteXml, function(){
		            	$(".myVisitCards").click();
		            });
		          });
		          $(dom).find(".refuse-visit-card").unbind("click").bind("click", function(){
		            var favoriteXml = factoryXMLFavory(visitor.getUserProfile().getUserProfileId(), FavoriteEnum.VISIT_CARD_ENTERPRISE_P);
		            deleteVisitCard(favoriteXml, function(){
		              	$(".myVisitCards").click();
		            });
		          });
			    };
			    this.getDOM = function () { //retourne une ligne HTML <a .....
			      return dom;
			    };
			    this.refresh();
			  },
			
			IndexView : function() {
				var that = this;
				var listeners = new Array();
				var candidatesCache = null;
				var currentCandidatesCache = null;
				var currentExibitorsCache = null;

				this.addListener = function(list) {
					listeners.push(list);
				};

				this.initView = function() {
					$('#cvSearchButton').unbind("click").bind("click", function() {
						// visitorsController.getCandidatesByCVKeywords($('#cvSearchInput').val());
						that.notifyFilterSearchProfiles();
					});
					$('#exportCandidatesButton').unbind("click").bind(
							"click",
							function() {
								$('#loadingEds').show(
										function() {
											$('#popupExportCandidates, .backgroundPopup').show();
											factoryExportVisitors(currentCandidatesCache);
											$('#loadingEds').hide();
											$("#popupExportCandidates #candidateSortableColumns")
													.sortable().disableSelection();
										});
							});
					$(
							'#popupExportCandidates .closePopup, #popupExportCandidates .eds-dialog-close')
							.click(function(e) {
								e.preventDefault();
								$('.eds-dialog-container').hide();
								$('.backgroundPopup').hide();
							});
					$(
							'#candidateProfileKeyWordInput,#formationSearchInput,#experienceSearchInput,#projectSearchInput')
							.unbind('keydown').bind('keydown', function(e) {
								if (e.keyCode == 13) {
									that.notifyFilterSearchProfiles();
								}
							});
					$("#candidateProfileAdvanceCriteria").on('show.bs.collapse',
							function() {
								that.reIntAdvanceSearch();
							});
					$("#candidateProfileAdvanceCriteria").on('hide.bs.collapse',
							function() {
								that.reIntAdvanceSearch();
							});
					// $(document).on(
					// 'keyup',
					// '#candidateProfileKeyWordInput,#formationSearchInput,#experienceSearchInput,#projectSearchInput',
					// that.notifyFilterSearchProfiles);
					// $(document).on('change','.candidateProfileCriteria select',
					// that.notifyFilterSearchProfiles);

					// todo
					$('#loadingEds')
							.show(
									function() {
										$('#popupExportCandidates').hide();
										$(".draggable").draggable({
											cancel : ".eds-dialog-inside"
										});
										traduct();
										that.initCandidatesTable();
										that.initExhibitorsTable();
										var firstTabActive = false;
										rightGroup = listStandRights[rightGroupEnum.REGISTER_VISITORS];
										if (rightGroup != null && rightGroup.getRightCount() != 0) {
											$('#myTab a.searchProfiles').show();
											$('#myTab a.searchProfiles').click(function() {
												that.showSearchProfiles();
											});
											that.showSearchProfiles();
											firstTabActive = true;
										} else {
											$('#myTab a.searchProfiles').closest("li").hide();
										}
										rightGroup = listStandRights[rightGroupEnum.FORUM_VISITORS];
										if (rightGroup != null && rightGroup.getRightCount() != 0) {
											$('#myTab a.forumVisitors').click(function() {
												$("#candidateProfileCriteria").show();
												that.showForumEnrolledUsers();
											});
											if (!firstTabActive) {
												that.showForumEnrolledUsers();
												firstTabActive = true;
											}
										} else {
											$('#myTab a.forumVisitors').closest("li").hide();
										}
										rightGroup = listStandRights[rightGroupEnum.STAND_VISITORS];
										if (rightGroup != null && rightGroup.getRightCount() != 0) {
											$('#myTab a.standVisitors').click(function(e) {
												$("#participantsStat").hide();
												$('#loadingEds').show(function() {
													that.showStandVisitors();
												});
											});
											if (!firstTabActive) {
												that.showStandVisitors();
												firstTabActive = true;
											}
										} else {
											$('#myTab a.standVisitors').closest("li").hide();
										}
										rightGroup = listStandRights[rightGroupEnum.FAVORITS_VISITORS];
										if (rightGroup != null && rightGroup.getRightCount() != 0) {
											$('#myTab a.favoriteUsers').click(function() {
												$("#participantsStat").hide();
												$('#loadingEds').show(function() {
													that.showFavoritsVisitors();
												});
											});
											if (!firstTabActive) {
												that.showFavoritsVisitors();
												firstTabActive = true;
											}
										} else {
											$('#myTab a.favoriteUsers').closest("li").hide();
										}
										rightGroup = listStandRights[rightGroupEnum.VISIT_CARDS_VISITORS];
										if (rightGroup != null && rightGroup.getRightCount() != 0) {
											$('#myTab a.myVisitCards').click(function() {
												$("#participantsStat").hide();
												$("#candidateProfileCriteria").hide();
												$('#loadingEds').show(function() {
													that.showVisitCardsVisitors();
												});
											});
											if (!firstTabActive) {
												that.showVisitCardsVisitors();
												firstTabActive = true;
											}
										} else {
											$('#myTab a.myVisitCards').closest("li").hide();
										}
										rightGroup = listStandRights[rightGroupEnum.EXHIBITORS];
										if (rightGroup != null && rightGroup.getRightCount() != 0) {
											$('#myTab a.exhibitors').click(function() {
												$("#participantsStat").hide();
												$("#candidateProfileCriteria").hide();
												$('#loadingEds').show(function() {
													that.showExhibitors();
												});
											});
											if (!firstTabActive) {
												that.showExhibitors();
												firstTabActive = true;
											}
										} else {
											$('#myTab a.exhibitors').closest("li").hide();
										}
										setTimeout(function() {
											that.fillActivitySectors();
											that.fillCountries();
											/*that.fillCountryResidences();
											that.fillCountryOrigins();
											that.fillFunctionsCandidate();
											that.fillExperienceYears();
											that.fillAreaExpertise();
											that.fillJobSought();
											that.fillCandidateStates();*/
										}, 2000);
									});
				};
				this.showSearchProfiles = function() {
					$("#participantsStat").show();
					$(".visitorContent").hide();
					$("#candidateProfileCriteria, #visitorsContent").show();
					$('#myTab a.searchProfiles').closest('ul').find('li').removeClass(
							'active');
					$('#myTab a.searchProfiles').closest('li').addClass('active');
					$('#loadingEds').show(function() {
						that.notifyGetSearchProfiles();
					});
				};
				this.showForumEnrolledUsers = function() {
					$("#participantsStat, .visitorContent").hide();
					$("#candidateProfileCriteria, #visitorsContent").show();
					$('#myTab a.forumVisitors').closest('ul').find('li')
							.removeClass('active');
					$('#myTab a.forumVisitors').closest('li').addClass('active');
					$('#loadingEds').show(function() {
						that.notifyGetForumEnrolled();
					});
				};
				this.showStandVisitors = function() {
					$("#participantsStat, .visitorContent").hide();
					$("#candidateProfileCriteria, #visitorsContent").show();
					$('#myTab a.standVisitors').closest('ul').find('li')
							.removeClass('active');
					$('#myTab a.standVisitors').closest('li').addClass('active');
					$('#loadingEds').show(function() {
						that.notifyGetStandVisitors();
					});
				};
				this.showFavoritsVisitors = function() {
					$("#participantsStat, .visitorContent").hide();
					$("#candidateProfileCriteria, #visitorsContent").show();
					$('#myTab a.favoriteUsers').closest('ul').find('li')
							.removeClass('active');
					$('#myTab a.favoriteUsers').closest('li').addClass('active');
					$('#loadingEds').show(function() {
						that.notifyGetFavoriteUsers();
					});
				};
				this.showVisitCardsVisitors = function(){
					$("#participantsStat, #candidateProfileCriteria, .visitorContent").hide();
					$("#myVisitCardsContent").show();
					$('#myTab a.myVisitCards').closest('ul').find('li')
							.removeClass('active');
					$('#myTab a.myVisitCards').closest('li').addClass('active');
					$('#loadingEds').show(function() {
						that.notifyGetMyVisitCards();
					});
				};
				this.showExhibitors = function(){
					$("#participantsStat, #candidateProfileCriteria, .visitorContent").hide();
					$("#exhibitorsContent").show();
					$('#myTab a.exhibitors').closest('ul').find('li')
							.removeClass('active');
					$('#myTab a.exhibitors').closest('li').addClass('active');
					$('#loadingEds').show(function() {
						that.notifyGetExhibitors();
					});
				};
				this.beforeInitView = function() {
					$("#shareProfileForm").validate({
						rules : {
							shareProfileTo : {
								required : true,
								email : true
							}
						}
					});

					$('.popupMessage #sendMessage').unbind("click").bind(
							"click",
							function() {
								if ($('#messageForm').valid()) {
									var messageSubject = $('#messageForm #messageSubject').val()
											.trim();
									var messageContent = $('#messageForm #messageContent').val();
									var message = new $.Message();
									message.setSubject(messageSubject);
									message.setContent(messageContent);
									that.notifySendMessage(message, receiverId);
									$('.popupMessage').hide();
								}
							});

					$('.popupSendSkypeInvitation #skypeSendMessage').unbind("click")
							.bind(
									"click",
									function() {
										if ($('.skypeMessageForm').valid()) {
											var messageSubject = $(
													'.skypeMessageForm .skypeMessageSubject').val()
													.trim();
											var messageContent = $(
													'.skypeMessageForm .skypeMessageContent').val();
											var message = new $.Message();
											message.setSubject(messageSubject);
											message.setContent(messageContent);
											that.notifySendSkypeMessage(message, receiverId);
											$('.popupSendSkypeInvitation').hide();
										}
									});

					$('.popupShare  #sendShareProfile').unbind("click").bind(
							"click",
							function() {
								if ($('#shareProfileForm').valid()) {
									var email = new $.Email();
									var address = $('#shareProfileForm #shareProfileTo').val()
											.trim();
									email.setAddress(that.toStringListAddress(address));
									email.setSubject($('#shareProfileForm #shareProfileSubject')
											.val());
									email.setBody($('#shareProfileForm #ShareProfileContent')
											.val().split("\n").join("<br/>"));
									that.notifySendShareProfile(email);
									$('.popupShare').hide();
								}
							});
					$('.popupMessage .cancelSendMessage, .cancelShareProfile').click(
							function() {
								$('.popupMessage, .backgroundPopup, .popupShare').hide();
							});
					$(".popupMessage").draggable({
						cancel : ".borderPopupMessageDiv"
					});
					customJqEasyCounter($('#messageForm #messageContent'), 65535, 65535,
							'#0796BE', LEFTCARACTERES);
					customJqEasyCounter($('#messageForm #messageSubject'), 255, 255,
							'#0796BE', LEFTCARACTERES);
					$('#messageForm div.jqEasyCounterMsg').css('width', '95%');
				};
				this.beforeInitView();
				this.initCandidatesTable = function() {
					$('#listCandidatesTable')
							.dataTable(
									{
										"aaData" : [],
										"aoColumns" : [ /*{
											"sTitle" : STATELABEL,
											"sWidth" : "5%",
										}, */{
											"sTitle" : LASTNAMELABEL,
											"sWidth" : "10%",
										}, {
											"sTitle" : FIRSTNAMELABEL,
											"sWidth" : "10%",
										}, {
											"sTitle" : ACTIVITYSECTORLABEL,
											"sWidth" : "20%",
										}, {
											"sTitle" : ENTERPRISELABEL,
											"sWidth" : "10%",
										}, {
											"sTitle" : FUNCTIONLABEL,
											"sWidth" : "10%",
										}, {
											"sTitle" : COUNTRY_LABEL,
											"sWidth" : "10%",
										}, {
											"sTitle" : ACTIONSLABEL,
											"sWidth" : "35%"
										}, {
											"sTitle" : "",
										} ],
										"aaSorting" : [ [ 7, "desc" ] ],
										"aoColumnDefs" : [ {
											"bVisible" : false,
											"aTargets" : [ 7 ]
										} ],
										"bAutoWidth" : false,
										"bRetrieve" : false,
										"bFilter" : false,
										"bDestroy" : true,
										"iDisplayLength" : 10,
										"fnDrawCallback" : function() {
											$('.addFavoriteButton, .deleteFavoriteButton, .sendMessageButton, .inviteSkypeButton, .approveButton, .disapproveButton, .profileButton, .sendByEmailButton, .exchangeVisitCardButton, .inviteToPivateTChatButton').off('click');
											$('.addFavoriteButton').on(
													'click',
													function() {
														var index = $(this).attr('id');
														var candidate = currentCandidatesCache[index];
														var favoriteXml = factoryXMLFavory(candidate
																.getUserProfile().getUserProfileId(),
																FavoriteEnum.USER);
														addFavorite(favoriteXml);
														$(this).hide().closest('td').find(
																'.deleteFavoriteButton').show();
													});
											$('.deleteFavoriteButton').on(
													'click',
													function() {
														var index = $(this).attr('id');
														var candidate = currentCandidatesCache[index];
														var favoriteXml = factoryXMLFavory(candidate
																.getUserProfile().getUserProfileId(),
																FavoriteEnum.USER);
														deleteFavorite(favoriteXml);
														$(this).hide().closest('td').find(
																'.addFavoriteButton').show();
													});
											$('.sendMessageButton')
													.on(
															'click',
															function() {
																var index = $(this).attr('id');
																var candidate = currentCandidatesCache[index];
																receiverId = candidate.getCandidateId();
																$(
																		'#messageForm #messageContent, #messageForm #messageSubject')
																		.val("");
																$('.popupMessage, .backgroundPopup').show();
															});
											$('.inviteSkypeButton')
													.on(
															'click',
															function() {
																var index = $(this).attr('id');
																var candidate = currentCandidatesCache[index];
																receiverId = candidate.getCandidateId();
																$('.skypeMessageForm #skypeMessageTo').val(candidate.getFullName());
																$('.skypeMessageForm .skypeMessageSubject, .skypeMessageForm .skypeMessageContent')
																		.val("");
																$(
																		'.popupSendSkypeInvitation, .contentOpacity, .backgroundPopup')
																		.show();
															});
											$('.approveButton').on(
													'click',
													function() {
														var index = $(this).attr('id');
														var candidate = currentCandidatesCache[index];
														that.notifyUpdateCandidateState(candidate
																.getCandidateId(), CandidateStateEnum.APPROVED,
																index);
													});
											$('.disapproveButton').on(
													'click',
													function() {
														var index = $(this).attr('id');
														var candidate = currentCandidatesCache[index];
														that.notifyUpdateCandidateState(candidate
																.getCandidateId(),
																CandidateStateEnum.DISAPPROVED, index);
													});
											$('.profileButton').on('click', function() {
												var index = $(this).attr('id');
												var candidate = currentCandidatesCache[index];
												viewUserProfile(candidate.getCandidateId());
												that.openProfile(candidate);
											});
											$('.sendByEmailButton')
													.on(
															'click',
															function() {
																var index = $(this).attr('id');
																var candidate = currentCandidatesCache[index];
																$('#ShareProfileContent').val(
																		staticVars.urlvisitor
																				+ "profileCandidate.html?candidateId="
																				+ candidate.getCandidateId()
																				+ "&idLang=" + getParam('idLang'));
																$(
																		'#shareProfileForm #shareProfileTo, #shareProfileForm #shareProfileSubject')
																		.val("");
																$('.popupShare, .backgroundPopup').show();
															});

											$('.exchangeVisitCardButton').on(
													'click',
													function() {
														var index = $(this).attr('id');
														var candidate = currentCandidatesCache[index];
														var favoriteXml = factoryXMLFavory(candidate
																.getUserProfile().getUserProfileId(),
																FavoriteEnum.VISIT_CARD_CANDIDATE);
														addFavorite(favoriteXml);
														$(this).hide();
													});
											
											$('.inviteToPivateTChatButton').on(
													'click',
													function() {
														var index = $(this).attr('id');
														var candidate = currentCandidatesCache[index];
														launchConnectToPrivateChat(candidate.getUserProfile().getUserProfileId());
													});
										}
									});
				};
				this.initExhibitorsTable = function() {
					$('#listExhibitorsTable')
							.dataTable(
									{
										"aaData" : [],
										"aoColumns" : [ {
											"sTitle" : LASTNAMELABEL,
											"sWidth" : "20%",
										}, {
											"sTitle" : FIRSTNAMELABEL,
											"sWidth" : "20%",
										}, {
											"sTitle" : ENTERPRISELABEL,
											"sWidth" : "20%",
										}, {
											"sTitle" : FUNCTIONLABEL,
											"sWidth" : "20%",
										}, {
											"sTitle" : ACTIONSLABEL,
											"sWidth" : "20%"
										}, {
											"sTitle" : "",
										} ],
										"aaSorting" : [ [ 5, "desc" ] ],
										"aoColumnDefs" : [ {
											"bVisible" : false,
											"aTargets" : [ 5 ]
										} ],
										"bAutoWidth" : false,
										"bRetrieve" : false,
										"bFilter" : false,
										"bDestroy" : true,
										"iDisplayLength" : 10,
										"fnDrawCallback" : function() {
											$('.exchangeVisitCardButton, inviteToPivateTChatButton').off('click');
											$('.exchangeVisitCardButton').on(
													'click',
													function() {
														var index = $(this).attr('id');
														var exibitor = currentExibitorsCache[index];
														var favoriteXml = factoryXMLFavory(exibitor
																.getUserProfile().getUserProfileId(),
																FavoriteEnum.VISIT_CARD_ENTERPRISE_P);
														addFavorite(favoriteXml);
														$(this).hide();
													});
											
											$('.inviteToPivateTChatButton').on(
													'click',
													function() {
														var index = $(this).attr('id');
														var exibitor = currentExibitorsCache[index];
														launchConnectToPrivateChat(exibitor.getUserProfile().getUserProfileId());
													});
										}
									});
				};
				this.reIntAdvanceSearch = function() {
					$(".candidateProfileCriteria #candidateStateSelect").val(0);
					$(".candidateProfileCriteria #sectorSelect").val(0);
					// $(".candidateProfileCriteria #functionSelect").val(0);
					$(".candidateProfileCriteria #countryResidenceSelect").val(0);
					$(".candidateProfileCriteria #countryOriginSelect").val(0);
					$(".candidateProfileCriteria #experienceYearsSelect").val(0);
					$(".candidateProfileCriteria #areaExpertiseSelect").val(0);
					$(".candidateProfileCriteria #jobSoughtSelect").val(0);
					$(".candidateProfileCriteria #countrySelect").val(0);
				};
				this.toStringListAddress = function(listAddress) {
					if (listAddress != null && listAddress != "") {
						listAddress = listAddress.split(";").join(",");
						var addressArray = listAddress.split(",");
						listAddress = addressArray[0];
						var address = null;
						for ( var i = 1; i < addressArray.length; i++) {
							address = addressArray[i];
							if (address != "" && regexEmail.test(address))
								listAddress += ", " + addressArray[i].trim();
						}
						// console.log(listAddress);
					}
					return listAddress;
				};
				this.fillActivitySectors = function() {
					var activitySectorSelectHandler = $('.candidateProfileCriteria #sectorSelect');
					activitySectorSelectHandler.empty();
					activitySectorSelectHandler.append($('<option/>').val(0).html(
							ALLLABEL));
					for (index in listActivitySectorsCache) {
						var sector = listActivitySectorsCache[index];
						activitySectorSelectHandler.append($('<option/>').val(
								sector.idsecteur).html(sector.name));
					}
				};
				this.fillCountries = function() {
					var countrySelectHandler = $('.candidateProfileCriteria #countrySelect');
					countrySelectHandler.empty();
					countrySelectHandler.append($('<option/>').val(0).html(
							ALLLABEL));
					for (index in listCountriesCache) {
						var country = listCountriesCache[index];
						countrySelectHandler.append($('<option/>').val(
								country.getCountryId()).html(country.getCountryName()));
					}
				};
				this.fillCountryResidences = function() {
					var countryResidenceSelectHandler = $('.candidateProfileCriteria #countryResidenceSelect');
					countryResidenceSelectHandler.empty();
					countryResidenceSelectHandler.append($('<option/>').val(0).html(
							ALLLABEL));
					for (index in listRegionsCache) {
						var region = listRegionsCache[index];
						countryResidenceSelectHandler.append($('<option/>').val(
								region.idRegion).html(region.regionName));
					}
				};
				this.fillCountryOrigins = function() {
					var countryOriginSelectHandler = $('.candidateProfileCriteria #countryOriginSelect');
					countryOriginSelectHandler.empty();
					countryOriginSelectHandler.append($('<option/>').val(0)
							.html(ALLLABEL));
					for (index in listRegionsCache) {
						var region = listRegionsCache[index];
						countryOriginSelectHandler.append($('<option/>').val(
								region.idRegion).html(region.regionName));
					}
				};
				this.fillFunctionsCandidate = function() {
					var functionSelectSelectHandler = $('.candidateProfileCriteria #functionSelect');
					functionSelectSelectHandler.empty();
					functionSelectSelectHandler.append($('<option/>').val(0).html(
							ALLLABEL));
					for (index in listFunctionsCandidateCache) {
						var functionCandidate = listFunctionsCandidateCache[index];
						functionSelectSelectHandler.append($('<option/>').val(
								functionCandidate.idFunction).html(
								functionCandidate.functionName));
					}
				};
				this.fillExperienceYears = function() {
					var experienceYearsSelectHandler = $('.candidateProfileCriteria #experienceYearsSelect');
					experienceYearsSelectHandler.empty();
					experienceYearsSelectHandler.append($('<option/>').val(0).html(
							ALLLABEL));
					for (index in listExperienceYearsCache) {
						var experienceYears = listExperienceYearsCache[index];
						experienceYearsSelectHandler.append($('<option/>').val(
								experienceYears.getIdExperienceYears()).html(
								experienceYears.getExperienceYearsName()));
					}
				};
				this.fillAreaExpertise = function() {
					var areaExpertiseSelectHandler = $('.candidateProfileCriteria #areaExpertiseSelect');
					areaExpertiseSelectHandler.empty();
					areaExpertiseSelectHandler.append($('<option/>').val(0)
							.html(ALLLABEL));
					for (index in listAreaExpertisesCache) {
						var areaExpertise = listAreaExpertisesCache[index];
						areaExpertiseSelectHandler.append($('<option/>').val(
								areaExpertise.getAreaExpertiseId()).html(
								areaExpertise.getAreaExpertiseName()));
					}
				};
				this.fillJobSought = function() {
					var jobSoughtSelectHandler = $('.candidateProfileCriteria #jobSoughtSelect');
					jobSoughtSelectHandler.empty();
					jobSoughtSelectHandler.append($('<option/>').val(0).html(ALLLABEL));
					for (index in listJobsSoughtCache) {
						var jobSought = listJobsSoughtCache[index];
						var disabled = "";
						if (jobSought.enable == "false") {
							disabled = " disabled=disabled";
						}
						jobSoughtSelectHandler.append($('<option' + disabled + '/>').val(
								jobSought.getJobSoughtId()).html(jobSought.getJobSoughtName()));
					}
				};
				this.fillCandidateStates = function() {
					var candidateStateSelectHandler = $('.candidateProfileCriteria #candidateStateSelect');
					candidateStateSelectHandler.empty();
					candidateStateSelectHandler.append($('<option/>').val(0).html(
							ALLLABEL));
					if (listCandidateStatesCache != null)
						listCandidateStatesCache
								.sort(function(a, b) {
									if (a.getCandidateStateName() > b.getCandidateStateName()) {
										return 1;
									} else if (a.getCandidateStateName() < b
											.getCandidateStateName()) {
										return -1;
									} else {
										return 0;
									}
								});
					for (index in listCandidateStatesCache) {
						var candidateState = listCandidateStatesCache[index];
						if (candidateState.getCandidateStateId() != CandidateStateEnum.DISAPPROVED)
							candidateStateSelectHandler.append($('<option/>').val(
									candidateState.getCandidateStateId()).html(
									candidateState.getCandidateStateName()));
					}
				};
				this.addTableDatas = function(selector, datas) {
					var oTable = $(selector).dataTable();
					oTable.fnClearTable();
					if (datas.length > 0) {
						oTable.fnAddData(datas);
					}
					oTable.fnDraw();
				};
				this.getHtml = function(element) {
					if (element != null)
						return $("<div></div>").html(element).html();
				};
				this.displayCandidates = function(candidates, defaultSorting) {
					currentCandidatesCache = candidates;
					var datas = new Array();
					var nbrRegisteredParticipants = 0;
					var nbrApprovedParticipants = 0;
					var nbrParticipantsDisapproved = 0;
					var nbrNewParticipants = 0;
					var nbrOldParticipants = 0;
					var nbrUpdatedParticipants = 0;
					var userFavorites = [];
					var exchangeVisitCardFavorites = [];
					var connectedVisitors = [];
					for ( var i in listUserFavorites) {
						var userFavorite = listUserFavorites[i];
						if(userFavorite.getComponent().getComponentName() == FavoriteEnum.USER) 
							userFavorites[userFavorite.getComponent().getComponentId()] = true;
						else if(userFavorite.getComponent().getComponentName() == FavoriteEnum.VISIT_CARD_CANDIDATE || userFavorite.getComponent().getComponentName() == FavoriteEnum.VISIT_CARD_ENTERPRISE_P){
							exchangeVisitCardFavorites[userFavorite.getComponent().getComponentId()] = true;
						}
					}
					for ( var i in listConnectedUsersCache) {
						var connectedUser = listConnectedUsersCache[i];
						if(connectedUser != undefined && connectedUser.canChatOnPrivate) 
							connectedVisitors[connectedUser.userId] = true;
					}
					var btnFavHandler = $("<button class='btn btn-primary btn-middle addFavoriteButton'/>");
					var btnDelFavHandler = $("<button class='btn btn-danger btn-middle deleteFavoriteButton'/>");
					//var btnProfileHandler = $("<button class='btn btn-danger btn-middle profileButton'/>");
					var btnExchangeVisitCardHandler = $("<button class='btn btn-info btn-middle exchangeVisitCardButton'/>");
					var btnSendMsgHandler = $("<button class='btn btn-success btn-middle sendMessageButton'/>");
					var btnApproveHandler = $("<button class='btn btn-info btn-middle approveButton'/>");
					var btnDisapproveHandler = $("<button class='btn btn-warning btn-middle disapproveButton'/>");
					//var btnShareProfileHandler = $("<button class='btn btn-warning btn-middle sendByEmailButton'/>");
					//var btnInviteSkypeHandler = $("<button class='btn btn-warning btn-middle inviteSkypeButton'/>");
					var btnInviteToPivateTChatHandler = $("<button class='btn btn-tchat btn-middle inviteToPivateTChatButton'/>");
					var listActions = $("<div></div>");
//					var newBadgeHandler = $('<div class="new-badge">'
//							+ htmlEncode(NEWLABEL) + '</div>');
					/*
					 * updatedHtml = '<div class="new-badge">' + htmlEncode(NEWLABEL) + '</div><i
					 * class="material-icons" title="' + htmlEncode(UPDATELABEL) + '"><i
					 * class="material-icons">person_add</i></i><div
					 * class="caption-updated">' + htmlEncode(NEWLABEL) + '</div>';
					 */
//					var updateBadgeHandler = $('<div class="updated-badge">'
//							+ htmlEncode(UPDATEDLABEL) + '</div>');
					/*
					 * updatedHtml = '<div class="update-badge">' +
					 * htmlEncode(UPDATELABEL) + '</div><i class="material-icons"
					 * title="' + htmlEncode(UPDATELABEL) + '">update</i><div
					 * class="caption-updated">' + htmlEncode(UPDATELABEL) + '</div>';
					 */
//					var notUpdateBadgeHandler = $('<div class="not-updated-badge">'
//							+ htmlEncode(NOTUPDATEDLABEL) + '</div>');
					btnFavHandler.text(ADDTOFAVORITELABEL);
					btnDelFavHandler.text(DELETEFROMFAVORITELABEL);
					//btnProfileHandler.text(VIEWPROFILELABEL);
					btnExchangeVisitCardHandler.text(EXCHANGE_LABEL).attr("title", EXCHANGE_VISIT_CARD_LABEL);
					btnSendMsgHandler.text(SENDMESSAGELABEL);
					//btnShareProfileHandler.text(SHARE_WITH_FRIEND_LABEL);
					//btnInviteSkypeHandler.text(SKYPEINVITATIONLABEL);
					btnApproveHandler.text(APPROVELABEL);
					btnDisapproveHandler.text(DISAPPROVELABEL);
					btnInviteToPivateTChatHandler.text(PRIVATECHATLABEL);
					var candidatesNbr = Object.keys(candidates).length;
					for (index in candidates) {
						var candidate = candidates[index];
						if(candidate != null){						
						var candidateState = candidate.getCandidateState();
						if (candidateState.getCandidateStateId() != CandidateStateEnum.DISAPPROVED) {
							var userProfile = candidate.getUserProfile();
							var candidateId = candidate.getCandidateId();
							var userProfileId = userProfile.getUserProfileId();
							var firstName = userProfile.getFirstName();
							var secondName = userProfile.getSecondName();
							var enterpriseName = candidate.getCurrentCompany();
							var functionName = candidate.getJobTitle();
							var countryName = candidate.getCountry().getCountryName();
							//var enterpriseName = candidate.;
//							var experienceYearsName = candidate.getExperienceYears()
//									.getExperienceYearsName();
							var secteuract = candidate.getNameSectors();
//							var candidateFunctionName = candidate.getFunctionCandidate()
//									.getFunctionName();
//							var updatedHtml = "";
//							var btnActive = "";
//							var btnProfile = "";
							var btnExchangeVisitCard = "";
							var btnInviteToPivateTChat = "";
							var sortIndex = null;
							if(defaultSorting == true) sortIndex = candidatesNbr - index;
							else sortIndex = candidateId;
							//btnInviteSkypeHandler.attr("id", index);
							//btnShareProfileHandler.attr("id", index);
							btnSendMsgHandler.attr("id", index);
							if (userFavorites[userProfileId] == true) {
								btnFavHandler.attr("id", index).hide();
								btnDelFavHandler.attr("id", index).show();
							} else {
								btnFavHandler.attr("id", index).show();
								btnDelFavHandler.attr("id", index).hide();
							}
//							if (that.checkProfile(candidate)) {
//								btnProfileHandler.attr("id", index);
//								btnProfile = that.getHtml(btnProfileHandler);
//							}
							if (!(exchangeVisitCardFavorites[userProfileId] == true)) {
								btnExchangeVisitCardHandler.attr("id", index);
								btnExchangeVisitCard = that.getHtml(btnExchangeVisitCardHandler);
							}
//							if (candidateState.getCandidateStateId() != CandidateStateEnum.APPROVED) {
//								btnApproveHandler.attr("id", index).show();
//								btnDisapproveHandler.attr("id", index).show();
////								btnActive = that.getHtml(btnApproveHandler)
////										+ that.getHtml(btnDisapproveHandler);
//							} else
//								nbrApprovedParticipants++;
							if (connectedVisitors[userProfileId] == true) {
								btnInviteToPivateTChatHandler.attr("id", index);
								btnInviteToPivateTChat = that.getHtml(btnInviteToPivateTChatHandler);
							}
							listActions.attr("id", userProfileId);
							listActions.html(
									//btnProfile + 
									that.getHtml(btnFavHandler)
									+ that.getHtml(btnDelFavHandler)
									+ that.getHtml(btnSendMsgHandler)
									+ btnExchangeVisitCard
									+ btnInviteToPivateTChat
									//+ that.getHtml(btnInviteSkypeHandler) 
									//+ btnActive
								  //+ that.getHtml(btnShareProfileHandler)
									);
//							if (candidateState.getCandidateStateId() == CandidateStateEnum.REGISTER){
//								updatedHtml = that.getHtml(newBadgeHandler);
//								nbrNewParticipants++;
//							}else if (candidateState.getCandidateStateId() == CandidateStateEnum.UPDATED){
//								updatedHtml = that.getHtml(updateBadgeHandler);
//								nbrUpdatedParticipants++;
//							}else{
//								updatedHtml = that.getHtml(notUpdateBadgeHandler);
//								if (candidateState.getCandidateStateId() == CandidateStateEnum.OLD) nbrOldParticipants++;
//							}
//							datas.push([ updatedHtml, firstName, secondName, secteuract,
//									candidateFunctionName, experienceYearsName,
//									that.getHtml(listActions), sortIndex ]);

							datas.push([ firstName, secondName, secteuract,
							             enterpriseName, functionName, countryName, 
							             that.getHtml(listActions), sortIndex ]);
						} else
							nbrParticipantsDisapproved++;
						nbrRegisteredParticipants++;
						}
					}
					$(".nbrRegisteredParticipants").text(nbrRegisteredParticipants);
					$(".nbrApprovedParticipants").text(nbrApprovedParticipants);
					$(".nbrParticipantsDisapproved").text(nbrParticipantsDisapproved);
					$(".nbrNewParticipants").text(nbrNewParticipants);
					$(".nbrUpdatedParticipants").text(nbrUpdatedParticipants);
					$(".nbrOldParticipants").text(nbrOldParticipants);
					that.addTableDatas('#listCandidatesTable', datas);
					$('#loadingEds').hide();
				};
				this.displayExhibitors = function(exhibitors, defaultSorting){
					currentExibitorsCache = exhibitors;
					var datas = new Array();
					var userFavorites = [];
					var exchangeVisitCardFavorites = [];
					var connectedVisitors = [];
					for ( var i in listUserFavorites) {
						var userFavorite = listUserFavorites[i];
						if(userFavorite.getComponent().getComponentName() == FavoriteEnum.USER) 
							userFavorites[userFavorite.getComponent().getComponentId()] = true;
						else if(userFavorite.getComponent().getComponentName() == FavoriteEnum.VISIT_CARD_CANDIDATE || userFavorite.getComponent().getComponentName() == FavoriteEnum.VISIT_CARD_ENTERPRISE_P){
							exchangeVisitCardFavorites[userFavorite.getComponent().getComponentId()] = true;
						}
					}
					for ( var i in listConnectedUsersCache) {
						var connectedUser = listConnectedUsersCache[i];
						if(connectedUser != undefined && connectedUser.canChatOnPrivate) 
							connectedVisitors[connectedUser.userId] = true;
					}
					var btnExchangeVisitCardHandler = $("<button class='btn btn-info btn-middle exchangeVisitCardButton'/>");
					var btnInviteToPivateTChatHandler = $("<button class='btn btn-warning btn-middle inviteToPivateTChatButton'/>");
					var listActions = $("<div></div>");
					btnExchangeVisitCardHandler.text(EXCHANGE_LABEL).attr("title", EXCHANGE_VISIT_CARD_LABEL);
					btnInviteToPivateTChatHandler.text(PRIVATECHATLABEL);
					var exhibitorsNbr = Object.keys(exhibitors).length;
					for (var index in exhibitors) {
						var exhibitor = exhibitors[index];
							var userProfile = exhibitor.getUserProfile();
							var enterprisePId = exhibitor.getEnterprisePId();
							var userProfileId = userProfile.getUserProfileId();
							var firstName = userProfile.getFirstName();
							var secondName = userProfile.getSecondName();
							var enterpriseName = exhibitor.getEnterprise().getNom();
							var functionName = exhibitor.getJobTitle();
//							var countryName = candidate.getCountry().getCountryName();
//							var secteuract = candidate.getNameSectors();
							var btnExchangeVisitCard = "";
							var btnInviteToPivateTChat = "";
							var sortIndex = null;
							if(defaultSorting == true) sortIndex = exhibitorsNbr - index;
							else sortIndex = enterprisePId;
							if (!(exchangeVisitCardFavorites[userProfileId] == true)
									&& currentEnterpriseP.getEnterprisePId() != exhibitor.getEnterprisePId()) {
								btnExchangeVisitCardHandler.attr("id", index);
								btnExchangeVisitCard = that.getHtml(btnExchangeVisitCardHandler);
							}
							if (connectedVisitors[userProfileId] == true
									&& currentEnterpriseP.getEnterprisePId() != exhibitor.getEnterprisePId()) {
								btnInviteToPivateTChatHandler.attr("id", index);
								btnInviteToPivateTChat = that.getHtml(btnInviteToPivateTChatHandler);
							}
							listActions.attr("id", userProfileId);
							listActions.html(btnExchangeVisitCard
									+ btnInviteToPivateTChat);

							datas.push([ firstName, secondName, enterpriseName, 
							             functionName, that.getHtml(listActions), sortIndex ]);
					}
					that.addTableDatas('#listExhibitorsTable', datas);
					$('#loadingEds').hide();
				};
				this.displaySearchProfiles = function(registerCandidates) {
					candidatesCache = registerCandidates;
					that.displayCandidates(candidatesCache);
				};
				this.displayForumEnrolled = function(forumVisitors) {
					candidatesCache = forumVisitors;
					that.displayCandidates(forumVisitors);
				};
				this.displayLoadStandVisitors = function(standVisitors) {
					candidatesCache = standVisitors;
					that.displayCandidates(candidatesCache, true);
				};
				this.displayFavoriteUsers = function(favoriteUsers) {
					candidatesCache = favoriteUsers;
					that.displayCandidates(candidatesCache);
				};
				this.displayMyVisitCards = function(favorites){
					$("#listMyVisitCards").empty();
				    if (favorites != null && Object.keys(favorites).length > 0) {
						$.each(favorites, function(j, favorite) {
							var visitCardManage = new $.VisitCardManage(favorite, that);
							$("#listMyVisitCards").append(visitCardManage.getDOM());
						});
				    }else $("#listMyVisitCards").html('<p class="empty noVisitCardLabel"></p>');
				    traduct();
					$('#loadingEds').hide();
				};
				this.getCandidatesByCVKeywords = function(cvKeywordValue) {
					// console.log('cvKeywordValue: '+cvKeywordValue);
				};
				this.checkProfile = function(candidate) {
					if (candidate != null) {
						var pdfCvs = candidate.getPdfCvs();
						if (pdfCvs != null) {
							if (pdfCvs.length > 0) {
								return true;
							}
						}
					}
					return false;
				};
				this.openProfile = function(candidate) {
					if (candidate != null) {
						var pdfCvs = candidate.getPdfCvs();
						if (pdfCvs != null) {
							if (pdfCvs.length > 0) {
								var pdfCv = null;
								for ( var i = 0; i < pdfCvs.length; i++) {
									pdfCv = pdfCvs[i];
									window.open(pdfCv.getUrlCv(), '_blank');
								}
							}
						}
					}
				};
				this.skypeMailTemplateSuccess = function(xml) {
					$('.skypeMessageForm .skypeMessageContent').val("");
				};
				this.updateCandidateStateSuccess = function(index, candidateStateId) {
					if (candidateStateId == CandidateStateEnum.APPROVED) {
						$("#" + index + ".approveButton, #" + index + ".disapproveButton")
								.hide();
						$(".nbrApprovedParticipants").text(
								parseInt($(".nbrApprovedParticipants").text()) + 1);
					} else if (candidateStateId == CandidateStateEnum.DISAPPROVED) {
						$(".nbrParticipantsDisapproved").text(
								parseInt($(".nbrParticipantsDisapproved").text()) + 1);
						$("#" + index + ".approveButton, #" + index + ".approveButton")
								.hide();
						$("#" + index + ".approveButton, #" + index + ".approveButton")
								.closest("tr").remove();
					}
				};
				this.notifySendMessage = function(message, candidateId) {
					$.each(listeners, function(i) {
						listeners[i].sendMessage(message, candidateId);
					});
				};
				this.notifySendSkypeMessage = function(message, candidateId) {
					$.each(listeners, function(i) {
						listeners[i].sendSkypeMessage(message, candidateId);
					});
				};
				this.notifySendShareProfile = function(email) {
					$.each(listeners, function(i) {
						listeners[i].sendShareProfile(email);
					});
				};
				this.notifyUpdateCandidateState = function(candidateId,
						candidateStateId, index) {
					$.each(listeners, function(i) {
						listeners[i].updateCandidateState(candidateId, candidateStateId,
								index);
					});
				};
				this.notifyFilterSearchProfiles = function() {
					$('#loadingEds')
							.show(
									function() {
										var keyWord = $(
												".candidateProfileCriteria #candidateProfileKeyWordInput")
												.val();
//										var formation = $(
//												".candidateProfileCriteria #formationSearchInput")
//												.val();
//										var experience = $(
//												".candidateProfileCriteria #experienceSearchInput")
//												.val();
//										var project = $(
//												".candidateProfileCriteria #projectSearchInput").val();
//										var candidateStateId = $(
//												".candidateProfileCriteria #candidateStateSelect option:selected")
//												.val();
										var activitySectorId = $(
												".candidateProfileCriteria #sectorSelect option:selected")
												.val();
//										var functionCandidateId = $(
//												".candidateProfileCriteria #functionSelect option:selected")
//												.val();
//										var countryResidenceIds = [];
//										$(".candidateProfileCriteria #countryResidenceSelect option:selected").each(function(i, selected){ 
//											countryResidenceIds[i] = $(selected).val(); 
//										});
//										
//										var countryOriginId = $(
//												".candidateProfileCriteria #countryOriginSelect option:selected")
//												.val();
//										var experienceYearsId = $(
//												".candidateProfileCriteria #experienceYearsSelect option:selected")
//												.val();
//										var areaExpertiseId = $(
//												".candidateProfileCriteria #areaExpertiseSelect option:selected")
//												.val();
//										var jobSoughtId = $(
//												".candidateProfileCriteria #jobSoughtSelect option:selected")
//												.val();

										var countryId = $(
												".candidateProfileCriteria #countrySelect option:selected")
												.val();
										// console.log(keyWord+" : "+formation+" : "+experience+" :
										// "+project + " : " +
										// candidateStateId + " : "
										// + activitySectorId+" : " +functionCandidateId+" :
										// "+countryResidenceId+" :
										// "+countryOriginId
										// + " : " + experienceYearsId + " : " + jobSoughtId + " : "
										// + jobSoughtId);
										var listFilterCandidates = new Array();
										var candidate = null;
										for ( var index in candidatesCache) {
											candidate = candidatesCache[index];
											if (candidate.searchWithKeyWord(keyWord)
//													&& candidate.searchInCandidateState(candidateStateId)
													&& candidate
															.searchInActivitySectors(activitySectorId)
													&& candidate
															.searchInCountry(countryId)
//													&& candidate
//															.searchInFunctionCandidate(functionCandidateId)
//													&& candidate
//															.searchInCountryResidence(countryResidenceIds)
//													&& candidate.searchInCountryOrigin(countryOriginId)
//													&& candidate
//															.searchInExperienceYears(experienceYearsId)
//													&& candidate.searchInAreaExpertise(areaExpertiseId)
//													&& candidate.searchInJobSought(jobSoughtId)
//													&& candidate.searchInExperience(experience)
//													&& candidate.searchInProject(project)
//													&& candidate.searchInFormation(formation)
													)
												listFilterCandidates.push(candidate);
										}
										that.displayCandidates(listFilterCandidates);
									});
				};
				this.notifyGetFavoriteUsers = function() {
					$.each(listeners, function(i) {
						listeners[i].getFavoriteUsers();
					});
				};
				this.notifyGetForumEnrolled = function() {
					$.each(listeners, function(i) {
						listeners[i].getForumEnrolled();
					});
				};
				this.notifyGetStandVisitors = function() {
					$.each(listeners, function(i) {
						listeners[i].getStandVisitors();
					});
				};
				this.notifyGetSearchProfiles = function() {
					$.each(listeners, function(i) {
						listeners[i].getSearchProfiles();
					});
				};
				this.notifyGetMyVisitCards = function() {
					$.each(listeners, function(i) {
						listeners[i].getMyVisitCards();
					});
				};
				this.notifyGetExhibitors = function() {
					$.each(listeners, function(i) {
						listeners[i].getExhibitors();
					});
				};
			},

			IndexViewListener : function(list) {
				if (!list)
					list = {};
				return $.extend(list);
			},

		});




