/*
 * This file contains all global variables
 */

/*
 * Labels for translation
 */

var listLangsEnum = []; //cet un arrays qui contient la liste des langs ID+Name ...
var listLangsAbrEnum = []; //cet un arrays qui contient la liste des abbréviations des langues...
var standId = '';
var currentPage = "";
var HOSTNAME_URL = window.location.hostname;
//var HOSTNAME_URL = "jobadvices.eventds.net";
//var HOSTNAME_URL = "54.203.252.23";
var staticVars = {
    urlBackEnd: "http://" + HOSTNAME_URL + "/BackEndEnterprise/rest/",
    urlBackEndCandidate: "http://" + HOSTNAME_URL + "/BackEndCandidate/rest/",
    urlvisitor: "http://" + HOSTNAME_URL + "/visitor/",
    urlexhibitor: "http://" + HOSTNAME_URL + "/exhibitor/",
    urlBackEndChat: "http://" + HOSTNAME_URL + "/BackEndChat/",
    urlServerUploader: "http://52.213.163.11:81/amazon_uploader1/php_uploader/server/php/", //"http://"+HOSTNAME_URL+":81/amazon_uploader/prx.php"
    urlServerStorage: "https://s3-eu-west-1.amazonaws.com/campusfrancestorage/",
    urlServerUpload: "http://52.213.163.11:81/amazon_uploader1/prx.php", //"http://"+HOSTNAME_URL+":81/amazon_uploader/prx.php"
    defaultImage: "http://" + HOSTNAME_URL + "/exhibitor/images/default_avatar.jpg",
    urlProfile: "http://" + HOSTNAME_URL + "/visitor/profileCandidate.html?candidateId="
};
var regexEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
var UPDATE_CANDIDATES_CACHE_TIME_STAMP = 3600000; //60min:3600000
var localePDFJS = getLocalePDFJS();
var FILE_EXTENSION_REGEX = /\.(pdf|ppt|pptx|doc|docx|xls|xlsx|avi|flv|mp4|MP4|wmv|WMV|webm|jpg|jpeg|png|PNG|gif|svg)$/i;
var DOC_EXTENSION_REGEX = /\.(pdf|ppt|pptx|doc|docx|xls|xlsx)$/i;
var VIDEO_EXTENSION_REGEX = /\.(avi|flv|mp4|MP4|wmv|WMV|webm)$/i;
var IMAGE_EXTENSION_REGEX = /\.(jpg|jpeg|png|PNG|gif|svg)$/i;
var MAX_FILE_SIZE = 6 * 1024 * 1024;
var MAX_VIDEO_SIZE = 50 * 1024 * 1024;
var tChatFlashLoaded = false;

var resourceTypeEnum = {
    DOCUMENT: 1,
    VIDEO: 2,
    IMAGE: 3
};

var standRightsIds = {
    BRONZE: 1,
    SILVER: 2,
    GOLD: 3,
    STANDINFO: 4
};

var rightGroupEnum = {
    ADVICES: 2,
    JOB_OFFERS: 5,
    POSTERS: 6,
    VIDEOS: 7,
    FORUM_PUBLICATIONS: 8,
    STAND_PUBLICATIONS: 9,
    DOCUMENTS: 10,
    PHOTOS: 11,
    TESTIMONIES: 12,
    SURVEYS: 13,
    INBOX: 14,
    VIDEOS_RH: 15,
    FORUM_DASHBOARD: 16,
    STAND_DASHBOARD: 17,
    SOLICITED_JOB_APPLICATION: 18,
    UN_SOLICITED_JOB_APPLICATION: 19,
    REGISTER_VISITORS: 20,
    FORUM_VISITORS: 21,
    STAND_VISITORS: 22,
    FAVORITS_VISITORS: 23,
    PRODUCTS: 24,
    QUESTION_INFO: 25,
    VISIT_CARDS_VISITORS: 26,
    EXHIBITORS: 27,
    EMAILING: 28,
    ENTERPRISES: 29
};

var InputTypeEnum = {
    TEXT_AREA: '1',
    SELECT: '2',
    RADIO: '3',
};

var SurveyTypeEnum = {
    PRIVATE: '1',
    PUBLIC: '2',
};

var FormTypeEnum = {
    REGISTER: '1',
    SURVEY: '2',
};

var CandidateStateEnum = {
    REGISTER: '1',
    APPROVED: '2',
    DISAPPROVED: '3',
    UPDATED: '4',
    OLD: '5',
};

var InputTypes = ['TEXT_AREA', 'SELECT', 'RADIO'];

var listStandRights = null;

var contactPhoto = "http://" + HOSTNAME_URL + "/exhibitor/images/default_avatar.jpg";
var enterprisepPhoto = "http://" + HOSTNAME_URL + "/exhibitor/images/default_avatar.jpg";

var getRecruiterInfo = null;
var currentEnterpriseP = null;
var isContactUpdate = false;
var listVideosCache = null;
var listPhotosCache = null;
var listDocumentsCache = null;
var listPostersCache = null;
var listAdvicesCache = null;
var listJobOffersCache = null;
var listSurveysCache = null;
var listStandsCache = null;
var listWebsitesCache = null;
var listHistoricChatCache = null;
var listActivitySectorsCache = null;
var listRegionsCache = null;
var listStudyLevelsCache = null;
var listFunctionsCandidateCache = null;
var listExperienceYearsCache = null;
var listAreaExpertisesCache = null;
var listJobsSoughtCache = null;
var listCandidateStatesCache = null;
var listJobApplicationsCache = null;
var listUnsolicitedJobApplicationsCache = null;
var listLanguageUserProfilesCache = null;
var listLanguageLevelsCache = null;
var listProductsCache = null;
var listCountriesCache = null;
var idJobApplicationToChange = 0;
var typeJobApplicationToChange = "";
var agendaLoaded = 0;
var chatLoaded = 0;
var dashboardLoaded = 0;
var analyticsLoaded = 0;
var candidates = new Array();
var visitors = new Array();
var listUserFavorites = new Array();
var favoriteUsers = new Array();
var receiverId = 0;
var listConversationsCache = new Array();
var listConnectedUsersCache = new Array();
var indexCurrentConversation = 0;
var sender = null;
var receiver = null;
var listReceivedConversations = [];
var listSentConversations = [];
var jobOfferController = null;
var candidatureController = null;
var adviceController = null;
var testimonyController = null;
var documentController = null;
var photosController = null;
var postersController = null;
var videosController = null;
var videoRhController = null;
var publicationsController = null;
var visitorsController = null;
var notificationController = null;
var inboxController = null;
var chatController = null;
var productController = null;
var questionInfoController = null;
var seoController = null;
var emailingController = null;
var enterprisesController = null;
var CHARGEMENT = "Chargement";
var SEPARATEUPLOAD = "_EDSSEPEDS_";
var translateUrl = "";
var profileSearchedEditor = null;

var languagesIds = {
    DEUTSCH: 1,
    ENGLISH: 2,
    FRENCH: 3,
};

var fileTypeEnum = {
    POSTER: 1,
    VIDEO: 2,
    PHOTO: 3,
    DOCUMENT: 4,
    VIDEORH: 5,
};

var FavoriteEnum = {
    MEDIA: "AllFiles",
    JOBOFFER: "JobOffer",
    CONTACT: "Contact",
    ADVICE: "Advice",
    TESTIMONY: "Testimony",
    STAND: "Stand",
    USER: "User",
    JOBAPPLCATION: "JobApplication",
    PRODUCT: "Product",
    SOCIAL_NETWORK: "SocialNetwork",
    VISIT_CARD_CANDIDATE: "VisitCardCandidate",
    VISIT_CARD_ENTERPRISE_P: "VisitCardEnterpriseP",
};
var FavoriteIdsEnum = {
    VISIT_CARD_CANDIDATE_ID: "12",
    VISIT_CARD_ENTERPRISE_P_ID: "13",
};
var VisitCardStatusEnum = {
    INVITER: "INVITER",
    GUEST: "GUEST"
};
var PageEnum = {
    BACKEND: "Backend",
    LOGIN: "login",
    CHAT: "chat"
};
var InputTypeEnum = {
    INPUT_TEXT: '1',
    TEXT_AREA: '2',
    SELECT: '3',
    RADIO: '4',
    FILE: '5'
};
var candidateColumnsEnum = {
    LAST_NAME: 1,
    FIRST_NAME: 2,
    EMAIL: 3,
    GENDER: 4,
    CELL_PHONE: 5,
    FIXE_PHONE: 6,
    PROFIL: 7,
    ACTIVITY_SECTOR: 8,
    EXPERIENCE_YEARS: 9,
    COUNTRY_RESIDENCE: 10,
    COUNTRY_ORIGIN: 11,
    AREA_EXPERTISE: 12,
    JOB_SOUGHT: 13,
    CVS: 14,
    ENTERPRISE: 15,
    FUNCTION: 16,
    WEB_SITE: 17,
    COUNTRY: 18,
    CITY: 19
};
var candidatureColumnsEnum = {
    FULL_NAME: 1,
    CV: 2,
    PROFIL: 3,
    LETTER: 4,
    JOB_OFFER: 5
};
var contactColumnsEnum = {
    FIRST_NAME: 1,
    LAST_NAME: 2,
    EMAIL: 3,
    ENTERPRISE: 4,
    CELL_PHONE: 5,
    ACTIVITY_SECTOR: 6,
};
var timeUploadLogo = null;
var timeUploadContact = null;
var timeUploadPhoto = null;
var timeUploadAdvice = null;
var timeUploadDocument = null;
var timeUploadPhoto = null;
var timeUploadPoster = null;
var timeUploadTestimony = null;
var timeUploadVideo = null;
var timeUploadLogoRecruiter = null;

var MAXFILESIZE = 6291456; // 6mb //6291456
var cacheAjax = true;
/******************Chat flex begin ******************/

var FMS_URL = "rtmp://:ip:/"; //50.112.243.218, 127.0.0.1, 52.49.92.74
//var FMS_URL = "rtmp://52.49.92.74/";
var FMS_APPLICATION_CHAT = "chatfmscompusFrance";
var HISTORY_URL = "http://" + HOSTNAME_URL + ":81/HistoricChatCompusFrance/"; //192.168.1.17
var IMAGES_URL = "";
//var TIME_CHECK_HISTORY = 5*1000;//5min : 5*60*1000 A modifier dans déploiement
var flashvars = {};
var userType = "Recruiter";
var isConnectToChat = false;
var tryToReconnect = 0;
var customColor1 = "0x69085A";
var EXTENDERTIMESTAMP = 10 * 60 * 1000; //10min:600000 A modifier dans déploiement
var PUBLIC = "public";
var PRIVATE = "private";
var isDatepickerLoad = false;
var LANGUAGES_NUMBER = 2;
var MAX_LENGTH_CHARACTER = 28;
/******************Chat flex end ******************/
/*******************Agenda begin********************/
var forumId = 1;
var editordesc = null;
/********************Agenda End*********************/

var LANGUAGENAME = ":LANGUAGENAME";
var ALL = "all";
/*****************************traduction terms***************************************/
var VIEWLABEL = "";
var EDITLABEL = "";
var DELETELABEL = "";
var CONNECTTOCHATLABEL = "";
var SKYPEINVITATIONLABEL = "";
var APPROVELABEL = "";
var DISAPPROVELABEL = "";
var DISCONNECTFROMCHATLABEL = "";
var SAVELABEL = "";
var CANCELLABEL = "";
var PHOTOLABEL = "";
var FIRSTANDLASTNAMELABEL = "";
var JOBTITLELABEL = "";
var TESTIMONYLABEL = "";
var ACTIONSLABEL = "";
var RESPONSELABEL = "";
var QUESTIONLABEL = "";
var CHARGEMENTLABEL = "";
var ADDLABEL = "";
var SAVECHANGESLABEL = "";
var LEFTCARACTERES = "";
var REMOVELABEL = "";
var REFERENCELABEL = "";
var TITLELABEL = "";
var DESCRIPTIONLABEL = "";
var SKILLSLABEL = "";
var REQUIREDLABEL = "";
var OKLABEL = "";
var FILESIZENOTEXCEEDLABEL = "";
var CONTENTLABEL = "";
var PUBLICCHATLABEL = "";
var PRIVATECHATLABEL = "";
var LANGAUGETAGLOCALELABEL = "";
var LANGUAGEDATEPICKERFORMATLABEL = "";
var OPENLABEL = "";
var AGELABEL = "";
var TOTALLABEL = "";
var CANTONLABEL = "";
var STUDYLEVELLABEL = "";
var FUNCTIONLABEL = "";
var EXPERIENCEYEARSLABEL = "";
var ACTIVITYSECTORLABEL = "";
var USERLABEL = "";
var VISITSTOTALLABEL = "";
var FIRSTVISITLABEL = "";
var LASTVISITLABEL = "";
var AVGVISITSLABEL = "";
var VISITSLABEL = "";
var LANGUAGELABEL = "";
var LANGUAGESLABEL = "";
var COUNTRYTERRITORYLABEL = "";
var CITYLABEL = "";
var CITIESLABEL = "";
var BROWSERLABEL = "";
var BROWSERSLABEL = "";
var OPERATINGSYSTEMSLABEL = "";
var OPERATINGSYSTEMLABEL = "";
var DAYLABEL = "";
var HOURLABEL = "";
var BOUNCESLABEL = "";
var NEWVISITSLABEL = "";
var AVGTIMEONSITELABEL = "";
var PHOTOSLABEL = "";
var ENTERPRISELABEL = "";
var NUMBERVIEWSLABEL = "";
var DOCUMENTSLABEL = "";
var VIDEOSLABEL = "";
var POSTERSLABEL = "";
var ADVICESLABEL = "";
var JOBOFFERSLABEL = "";
var SURVEYLABEL = "";
var DATELABEL = "";
var AREALABEL = "";
var AREAVIEWSLABEL = "";
var UNIQUEAREAVIEWSLABEL = "";
var VISITORSLABEL = "";
var VISITORTYPELABEL = "";
var AVGTIMEAREAVIEWSLABEL = "";
var TRAFFICSOURCESLABEL = "";
var DEMOGRAPHICSDATALABEL = "";
var OVERVIEWLABEL = "";
var PROFILELABEL = "";
var VISITSBYFUNCTIONLABEL = "";
var NEWVISITORSVSRETURNINGVISITORSLABEL = "";
var BOOTHVISITORSLABEL = "";
var WEBSITESLABEL = "";
var TESTIMONIESLABEL = "";
var APPLYLABEL = "";
var SUBMITCVLABEL = "";
var SENDMESSAGELABEL = "";
var CVSUBMITTEDLABEL = "";
var DATEOFAPPLICATIONLABEL = "";
var SENDLABEL = "";
var YOUDONTHAVERIGHTTODOMODIF = "";
var FIRSTNAMELABEL = "";
var LASTNAMELABEL = "";
var ADDTOFAVORITELABEL = "";
var DELETEFROMFAVORITELABEL = "";
var VIEWPROFILELABEL = "";
var CHOOSELABEL = "";
var VIEWCVLABEL = "";
var VIEWLETTERLABEL = "";
var TRADUCTLABEL = "";
var MODIFY_PRODUCT = "";
var TRANSLATE_PRODUCT = "";
var TRADUCTIONLABEL = "";
var ACTIVITYSECTORSLABEL = "";
var RETURNLABEL = "";
var URLLABEL = "";
var SURVEYSLABEL = "";
var CVSSUBMITEDNBRLABEL = "";
var QUESTIONSLABEL = "";
var ALLLABEL = "";
var REPLACELANGUAGELABEL = "";
var LANGUAGELEVELLABEL = "";
var NUMBEROFCHOICES = "";
var UNSOLICITEDAPPLICATIONLABEL = "";
var NEWLABEL = "";
var UPDATEDLABEL = "";
var STATELABEL = "";
var NOTUPDATEDLABEL = "";
var EMAIL_LABEL = "";
var CELL_PHONE_LABEL = "";
var FIXE_PHONE_LABEL = "";
var PROFILLABEL = "";
var COUNTRY_RESIDENCE_LABEL = "";
var COUNTRY_ORIGIN_LABEL = "";
var AREA_EXPERTISE_LABEL = "";
var JOB_SOUGHT_LABEL = "";
var GENDER_LABEL = "";
var CVS_LABEL = "";
var DOWNLOADING_LABEL = "";
var SHARE_WITH_FRIEND_LABEL = "";
var CV_LABEL = "";
var PROFIL_LABEL = "";
var LETTER_LABEL = "";
var ACCEPT_PRODUCT_RESOURCE = "";
var ATTACH_FILES = "";
var TYPE_LABEL = "";
var PRODUCTS_LABEL = "";
var COUNTRY_LABEL = "";
var STATE_LABEL = "";
var CITY_LABEL = "";
var LOGO_LABEL = "";
var VIEW_LINKS_LABEL = "";
var WEB_SITE_LABEL = "";
var EXCHANGE_VISIT_CARD_LABEL = "";
var EXCHANGE_LABEL = "";
var COMPANY_PROFILE_LABEL = "";
var ADDSURVEYLABEL = "";
var ADDTITLELABEL = "";
var PRIVATELABEL = "";
var PUBLICLABEL = "";
var ADDQUESTIONLABEL = "";
var RESPONSESLABEL = "";
var ADDQUESTION = "";
var YOURQUESTIONLABEL = "";
var INPUTSURVEYLABEL = "";
var MULTIPLECHOICELABEL = "";
var SINGLECHOICELABEL = "";
var ATLEASTERRORLABEL = "";
var FILESLABEL = "";
var NEWMESSAGELABEL = "";
var MESSAGELABEL = "";
var ADDFOLDERLABEL = "";
var DEMAND_IN_PROGRESS_LABEL = "";
var ACCEPT_LABEL = "";
var REFUSE_LABEL = "";
/****************************end traduction term*************************************/
var pageTitle = "Gestion du stand";
var conversationIndex = -1;
var notificationConversationIndex = -1;
var nbreWSCallsInProgress = 0;
var nbreWSCallsLoginInProgress = 0;
var nbreLoginWSCallsInProgress = 0;