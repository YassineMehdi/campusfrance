function isIE() {
	var myNav = navigator.userAgent.toLowerCase();
	return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1])
			: false;
}
var isIe = isIE();
if (isIe && (isIe == 8 || isIe == 7 || isIe == 6)) {
	alert('La version de votre navigateur n\'est compatible avec le salon virtuel. Merci d\'utiliser les navigateur suivants :Chrome 35+, Firefox 31+, Safari 7+, IE 10+ Merci !');
}
function get_browser_info() {
	var ua = navigator.userAgent, tem, M = ua
			.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i)
			|| [];
	if (/trident/i.test(M[1])) {
		tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
		return {
			name : 'IE',
			version : (tem[1] || '')
		};
	}
	if (M[1] === 'Chrome') {
		tem = ua.match(/\bOPR\/(\d+)/);
		if (tem != null) {
			return {
				name : 'Opera',
				version : tem[1]
			};
		}
	}
	M = M[2] ? [ M[1], M[2] ] : [ navigator.appName, navigator.appVersion, '-?' ];
	if ((tem = ua.match(/version\/(\d+)/i)) != null) {
		M.splice(1, 1, tem[1]);
	}
	return {
		name : M[0],
		version : M[1]
	};
}
var browser = get_browser_info();
if ((browser.name == "Firefox" && browser.version < 31) || (browser.name == "Chrome" && browser.version < 35) 
		|| (browser.name == "Safari" && browser.version < 7)) {
	alert('La version de votre navigateur n\'est compatible avec le salon virtuel. Merci d\'utiliser les navigateur suivants :Chrome 35+, Firefox 31+, Safari 7+, IE 10+ Merci !');
}
// console.log(browser);
switch (getIdLangParam()) {
	case '3':
		$.getScript("jquery/plugins/jquery.validate.fr.js", function() {
		});
		$.getScript("js/datepicker/jquery-datepicker-fr.js", function() {
			isDatepickerLoad = true;
		});
		$.getScript("dataTable/jquery.dataTables.min.fr.js", function() {
			$.getScript("dataTable/bootstrap_dt.js", function() {
			});
		});
		$.getScript("jquery/plugins/jquery.ui.timepicker.fr.js", function() {
		});
		break;
	case '2':
		isDatepickerLoad = true;
		$.getScript("dataTable/jquery.dataTables.min.js", function() {
			$.getScript("dataTable/bootstrap_dt.js", function() {
			});
		});
		break;
	default:
		$.getScript("jquery/plugins/jquery.validate.fr.js", function() {
		});
		$.getScript("js/datepicker/jquery-datepicker-fr.js", function() {
			isDatepickerLoad = true;
		});
		$.getScript("dataTable/jquery.dataTables.min.fr.js", function() {
			$.getScript("dataTable/bootstrap_dt.js", function() {
			});
		});
		$.getScript("jquery/plugins/jquery.ui.timepicker.fr.js", function() {
		});
	break;
}

function showTabChat(tabElement) {
	$(".sidebar-button").removeClass("active");
	tabElement.addClass("active");
	$('#divChat').css({
		"visibility" : "visible",
		"height" : "100%"
	});
	$('#divNotChat').empty();
	isSettingsFilled();
}
function hideTabChat() {
	$('#divChat').css({
		"visibility" : "hidden",
		"height" : "0px"
	});
	$('.chatTabs').css({
		"visibility" : "hidden",
		"height" : "0px"
	});
}
function showTabNotChat(file, tabElement) {
	$(".sidebar-button").removeClass("active");
	tabElement.addClass("active");
	$('#loadingEds').show();
	$('#divNotChat').load(file);
	$('#divNotChat').show();

}
function hideTabNotChat() {
	$('#divNotChat').hide();
}
