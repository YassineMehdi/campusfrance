
jQuery.extend({
	SEOView: function(){
		var that = this;
		var listeners = new Array();
		
		this.initView = function(){
		    traduct();
	     	customJqEasyCounter("#seoTitle", 254, 254, '#0796BE', LEFTCARACTERES);//60
	     	customJqEasyCounter("#seoMetadata", 254, 254, '#0796BE', LEFTCARACTERES);//160
	     	customJqEasyCounter("#seoKeywords", 254, 254, '#0796BE', LEFTCARACTERES);//254
		    $("#seoForm td.jqEasyCounterMsg").css('width', '55%');
			$("#seoContent .draggable").draggable({
				cancel : ".eds-dialog-inside"
			});
			$('#seoContent .closePopup, #seoContent .eds-dialog-close').click(function() {
				hidePopups();
			});
			$("#saveSeoBtn").unbind("click").bind("click", function() {
				if($("#seoForm").valid())
					that.updateSEO();
			});
			switch($.cookie('groupId')) {
				case '1':
					$("#posterLink2").closest('tr').hide();
				break;
				case '2':
					$("#posterLink2").closest('tr').hide();
				break;
				case '3':
					$("#posterLink2").closest('tr').hide();
				break;
				case '4':
					$("#posterLink2").closest('tr').hide();
				break;
			}
			if($.cookie('standId') == '13'){
				$("#posterLink2").closest('tr').show();
			}
			that.validateSEOForm();
		};
		this.validateSEOForm = function() {
			/*$("#seoForm").validate({
				errorPlacement : function errorPlacement(error, element) {
					element.after(error);
				},
				rules : {
					seoTitle : {
						required : true
					},
					seoMetadata : {
						required : true
					},
					seoKeywords : {
						required : true
					}
				}
			});*/
		};
		this.displaySEO = function(seo){
			$("#seoTitle").val(seo.getSeoTitle());
			$("#seoMetadata").val(seo.getSeoMetadata());
			$("#seoKeywords").val(seo.getSeoKeywords());
			$("#posterLink1").val(seo.getSeoFirstLink());
			$("#posterLink2").val(seo.getSeoSecondeLink());
			$("#posterLink3").val(seo.getSeoThirdLink());
		};
		this.updateSEO = function(){
			var seo = new $.SEO();
			seo.setSeoTitle($("#seoTitle").val());
			seo.setSeoMetadata($("#seoMetadata").val());
			seo.setSeoKeywords($("#seoKeywords").val());
			seo.setSeoFirstLink($("#posterLink1").val());
			seo.setSeoSecondeLink($("#posterLink2").val());
			seo.setSeoThirdLink($("#posterLink3").val());
			that.notifyUpdateSEO(seo);
		};
		this.updateSEOSuccess = function(seo){
			hidePopups();
		};
		this.notifyUpdateSEO = function(seo){
			$.each(listeners, function(i){
				listeners[i].updateSEO(seo);
			});
		};
		this.addListener = function(list){
			listeners.push(list);
		};
		
	},
	
	SEOViewListener: function(list) {
		if(!list) list = {};
		return $.extend(list);
	}
	
});

