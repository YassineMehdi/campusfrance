
jQuery.extend({
	
	SEOModel: function(){
		var that = this;
		var listeners = new Array();
		
		this.initModel = function(){
			that.getSEO();
		};
		
		this.getSEOSuccess = function(data){
			var seo = new $.SEO(data);
			that.notifyLoadSEO(seo);
		};
		
		this.getSEOError = function(xhr, status, error){
		};
		
		this.getSEO = function(){
			genericAjaxCall('GET', staticVars.urlBackEnd+'seo/get?idLang='+getParam('idLang'), 'application/xml', 'xml', 
					'', that.getSEOSuccess, that.getSEOError);
		};
		
		this.updateSEOSuccess = function(data){
			var seo = new $.SEO(data);
			that.notifyUpdateSEOSuccess(seo);
		};
		
		this.updateSEOError = function(xhr, status, error){
		};
		
		this.updateSEO = function(seo){
			genericAjaxCall('POST', staticVars.urlBackEnd+'seo/update?idLang='+getParam('idLang'), 'application/xml', 'xml', 
					seo.xmlData(), that.updateSEOSuccess, that.updateSEOError);
		};
		
		this.addListener = function(list){
			listeners.push(list);
		};

		this.notifyLoadSEO = function(seo) {
			$.each(listeners, function(i) {
				listeners[i].loadSEO(seo);
			});
		};

		this.notifyUpdateSEOSuccess = function(seo) {
			$.each(listeners, function(i) {
				listeners[i].updateSEOSuccess(seo);
			});
		};
		
	},
		
	SEOModelListener: function(list) {
		if(!list) list = {};
		return $.extend(list);
	},
	
});
