jQuery.extend({

	NotificationModel : function() {
		var listeners = new Array();
		var that = this;
		var tryToReconnectToNotifMsg=0;
		var MAX_TRY_TO_RECONNECT_TO_NOTIF_MSG=8;
		var PUBLIC_TOPIC="allNFE";
		this.addListener = function(list) {
			listeners.push(list);
		};
		this.initModel = function(){
			that.initNotifMessageAthmosphere(PUBLIC_TOPIC);
			that.initNotifMessageAthmosphere($.cookie('token'));
			that.getNotifsCache();
		};
		this.getNotifsCacheSuccess = function(json){
			//console.log("getNotifsSuccess");
			$.each(json, function(key, notifMessage){
				//console.log(JSON.stringify(notifMessage));
				that.notifyNotif(notifMessage);
			});
		};
		this.getNotifsCache = function(){
			genericAjaxCall('GET', staticVars["urlBackEndCandidate"]+'notif/cache/token?token='+$.cookie('token')+'&langId='+getIdLangParam(), 'application/json', 'json','', that.getNotifsCacheSuccess,'');
		};	
		this.showNotif = function(idNotif){
			genericAjaxCall('POST', staticVars["urlBackEndCandidate"]+'notif/update/token?idNotif='+idNotif+'&token='+$.cookie('token'), 'application/xml', 'xml','', '', '');
		};
		this.initNotifMessageAthmosphere = function(topic){
			var socket = $.atmosphere;
			var url = staticVars["urlBackEndChat"]+"chat/notif";
			if(topic!=null && topic!="") url=url+"/"+topic;
			var request = {
				url : url,
				contentType : "application/json",
				logLevel : 'debug',
				transport : 'websocket',
				fallbackTransport : 'long-polling'
			};
		
			request.onOpen = function(response) {
				tryToReconnectToNotifMsg--;
				//console.log("Notification on open, topic : "+topic);
				//console.log("********* Connection with topic Opening ***********");
			};
			request.onReconnect = function (request, response) {
				//console.log("Notification Reconnecting");
				//console.log("******** Reconnecting **************");
		    };
			request.onMessage = function(response) {
				//console.log('onMessage : '+response.state);
				var messageData = response.responseBody;
				var notifMessage=null;
				try {
					notifMessage = jQuery.parseJSON(messageData);
				} catch (e) {
					console.log('This doesn\'t look like a valid JSON: ', messageData.data);
					return;
				}
				if(notifMessage != null) {
					//console.log('onMessage : '+JSON.stringify(notifMessage));
					that.getNotif(notifMessage);
				}
			};
		
			request.onClose = function(response) {
				//console.log('onClose : Sorry, but there\'s some problem with your socket or the server is down state : '+response.state + ", topic : "+topic);
			};
		
			request.onError = function(response) {
				//console.log('onError : Sorry, but there\'s some problem with your socket or the server is down state : '+response.state+ ", topic : "+topic);
				var state=response.state;
				if(state=="error") {
					//console.log("****** Reconnect to Notification Message BUS ******");
					tryToReconnectToNotifMsg++;
					if(that.tryToReconnectToNotifMsg < MAX_TRY_TO_RECONNECT_TO_NOTIF_MSG){
						setTimeout(function(){
								that.initNotifMessageAthmosphere(topic);
							}
							,1000
						);
					}
				}
			};
			
			socket.subscribe(request);
		};
		
		this.getNotif = function(object){
			if(typeof object != 'undefined'){
				that.notifyNotif(object);
			}
		};
		
		this.notifyNotif = function(object){
			$.each(listeners, function(i) {
				listeners[i].notifLoaded(object);
			});
		};
		
		this.getNotifs = function(xhr,status,error){
			$.each(listeners, function(i) {
				listeners[i].candidateLoginError(xhr,status,error);
			});
		};
	},
	NotifModelListener : function(list) {
		if (!list)
			list = {};
		return $.extend(list);
	},

});