
jQuery.extend({
	
	ProductModel: function(){
		var that = this;
		var listeners = new Array();
		
		this.initModel = function(){
			that.getProducts();
		};
		
		this.getProductsSuccess = function(data){
			var products = new Array();
			var product = null;
			$(data).find("productDTO").each(function() {
				product = new $.Product($(this));
				products[product.getProductId()] = product;
			});
			that.notifyLoadProducts(products);
		};
		
		this.getProductsError = function(xhr, status, error){
		};
		
		this.getProducts = function(){
			genericAjaxCall('GET', staticVars.urlBackEnd+'product?idLang='+getParam('idLang'), 'application/xml', 'xml', 
					'', that.getProductsSuccess, that.getProductsError);
		};
		
		this.addProductSuccess = function(data){
			that.getProducts();
			that.notifyAddProductSuccess();
		};
		
		this.addProductError = function(xhr, status, error){
		};
		
		this.addProduct = function(product){
			genericAjaxCall('POST', staticVars.urlBackEnd+'product/add?idLang='+getParam('idLang'), 'application/xml', 'xml', 
					product.xmlData(), that.addProductSuccess, that.addProductError);
		};
		
		this.updateProductSuccess = function(data){
			that.getProducts();
			that.notifyUpdateProductSuccess();
		};
		
		this.updateProductError = function(xhr, status, error){
		};
		
		this.updateProduct = function(product){
			genericAjaxCall('POST', staticVars.urlBackEnd+'product/update?idLang='+getParam('idLang'), 'application/xml', 'xml', 
					product.xmlData(), that.updateProductSuccess, that.updateProductError);
		};
		
		this.traductProductSuccess = function(data){
			that.getProducts();
			that.notifyUpdateProductSuccess();
		};
		
		this.traductProductError = function(xhr, status, error){
		};
		
		this.traductProduct = function(product, langId){
			genericAjaxCall('POST', staticVars.urlBackEnd+'product/traduct?langId='+langId, 'application/xml', 'xml', 
					product.xmlData(), that.traductProductSuccess, that.traductProductError);
		};
		
		this.deleteProductSuccess = function(data){
			that.getProducts();
			that.notifyDeleteProductSuccess();
		};
		
		this.deleteProductError = function(xhr, status, error){
		};
		
		this.deleteProduct = function(productId){
			genericAjaxCall('POST', staticVars.urlBackEnd+'product/delete?idLang='+getParam('idLang')+"&productId="+productId, 
					'application/xml', 'xml', '', that.deleteProductSuccess, that.deleteProductError);
		};
		
		this.getProductSuccess = function(data){
			that.notifyLoadProduct(new $.Product(data));
		};
		
		this.getProductError = function(xhr, status, error){
		};
		
		this.getProduct = function(productId){
			genericAjaxCall('GET', staticVars.urlBackEnd+'product/get?idLang='+getParam('idLang')+"&productId="+productId, 
					'application/xml', 'xml', '', that.getProductSuccess, that.getProductError);
		};
		
		this.getResourcesSuccess = function(data){
			var resources = new Array();
			var resource = null;
			$(data).find("resourceDTO").each(function() {
				resource = new $.Resource($(this));
				resources[resource.getResourceId()] = resource;
			});
			that.notifyLoadResources(resources);
		};
		
		this.getResourcesError = function(xhr, status, error){
		};
		
		this.getResources = function(productId){
			genericAjaxCall('GET', staticVars.urlBackEnd+'resource/by/product?idLang='+getParam('idLang')+"&productId="+productId, 
					'application/xml', 'xml', '', that.getResourcesSuccess, that.getResourcesError);
		};

		this.addResourceSuccess = function(data, productId){
			that.getResources(productId);
		};
		
		this.addResourceError = function(xhr, status, error){
		};
		
		this.addResource = function(resource, productId){
			genericAjaxCall('POST', staticVars.urlBackEnd+'product/add/resource?idLang='+getParam('idLang')+"&productId="+productId, 
					'application/xml', 'xml', resource.xmlData(), function(data){
						that.addResourceSuccess(data, productId);
				}, that.addResourceError);
		};
		
		this.deleteResourceSuccess = function(data, productId){
			that.getResources(productId);
			that.notifyDeleteResourceSuccess();
		};
		
		this.deleteResourceError = function(xhr, status, error){
		};
		
		this.deleteResource = function(resourceId, productId){
			genericAjaxCall('POST', staticVars.urlBackEnd+'product/delete/resource?idLang='+getParam('idLang')+"&productId="+productId
					+"&resourceId="+resourceId, 'application/xml', 'xml', '', function(data){
					that.deleteResourceSuccess(data, productId);
				}, that.deleteResourceError);
		};
		
		this.getAnalyticsProductsSuccess = function(data){
			var products = new Array();
			var product = null;
			$(data).find("productDTO").each(function() {
				product = new $.Product($(this));
				products[product.getProductId()] = product;
			});
			that.notifyLoadProducts(products);
		};
		
		this.getAnalyticsProductsError = function(xhr, status, error){
		};

		this.getAnalyticsProducts = function(callBack, path) {
			// console.log('getAnalyticsJobOffers : '+path);
			listProductsCache = [];
			genericAjaxCall('GET', staticVars.urlBackEnd + path,
					'application/xml', 'xml', '', function(data) {
						var product = null;
						$(data).find("productDTO").each(function() {
							product = new $.Product($(this));
							listProductsCache[product.getProductId()] = product;
						});
						callBack();
					}, that.getAnalyticsProductsError);
		};

		this.getProductById = function(productId) {
			return listProductsCache[productId];
		};
		
		this.addListener = function(list){
			listeners.push(list);
		};

		this.notifyLoadProducts = function(products) {
			$.each(listeners, function(i) {
				listeners[i].loadProducts(products);
			});
		};

		this.notifyAddProductSuccess = function() {
			$.each(listeners, function(i) {
				listeners[i].addProductSuccess();
			});
		};
		
		this.notifyUpdateProductSuccess = function() {
			$.each(listeners, function(i) {
				listeners[i].updateProductSuccess();
			});
		};
		
		this.notifyDeleteProductSuccess = function() {
			$.each(listeners, function(i) {
				listeners[i].deleteProductSuccess();
			});
		};
		
		this.notifyLoadProduct = function(product){
			$.each(listeners, function(i){
				listeners[i].loadProduct(product);
			});
		};
		
		this.notifyLoadResources = function(resources){
			$.each(listeners, function(i){
				listeners[i].loadResources(resources);
			});
		};
		
		this.notifyDeleteResourceSuccess = function() {
			$.each(listeners, function(i) {
				listeners[i].deleteResourceSuccess();
			});
		};
	},
		
	ProductModelListener: function(list) {
		if(!list) list = {};
		return $.extend(list);
	},
	
});

