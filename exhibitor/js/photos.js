jQuery.extend({

	PhotosModel : function() {
		var listeners = new Array();
		var that = this;
		this.initModel = function(){
			that.getStandPhotos();
		};
		this.getStandPhotosSuccess = function(xml) {
			listPhotosCache = new Array();
			$(xml).find("allfiles").each(function() {
				var photo = new $.Photo($(this));
				listPhotosCache[photo.idPhoto]=photo;
			});
			that.notifyLoadPhotos(listPhotosCache);
		};
		
		this.getStandPhotosError = function(xhr,status,error) {
			isSessionExpired(xhr.responseText);
		};

		this.getStandPhotos = function() {
			$('#listStandPhotos').empty();
			genericAjaxCall('GET', staticVars.urlBackEnd
					+ 'allfiles/standPhotos?idLang='+getIdLangParam(), 'application/xml', 'xml', '',
					that.getStandPhotosSuccess,that.getStandPhotosError);
		};

		this.getAnalyticsPhotos = function(callBack, path) {
			// console.log('getAnalyticsPhotos : '+path);
			genericAjaxCall('GET', staticVars.urlBackEnd
					+ path, 'application/xml', 'xml', '',
					function(xml){
						listPhotosCache = new Array();
						$(xml).find("allfiles").each(function() {
							var photo = new $.Photo($(this));
							listPhotosCache[photo.idPhoto]=photo;
						});
						callBack();
					}, that.getStandPhotosError);
		};


		this.addPhotoSuccess = function(xml) {
			that.notifyAddPhotoSuccess();
		};

		this.addPhotoError = function(xhr,status,error) {
			isSessionExpired(xhr.responseText);
		};

		this.addPhoto = function(photo) {
			genericAjaxCall('POST', staticVars.urlBackEnd
					+ 'allfiles/addFile?idLang='+getIdLangParam(), 'application/xml', 'xml', photo.xmlData(),
					that.addPhotoSuccess, that.addPhotoError);
		};
		
		this.updatePhotoSuccess = function(xml) {
			that.notifyUpdatePhotoSuccess();
		};

		this.updatePhotoError = function(xhr,status,error) {
			isSessionExpired(xhr.responseText);
		};
		
		this.updatePhoto = function(photo) {
			genericAjaxCall('POST', staticVars["urlBackEnd"]
					+ 'allfiles/updateFile?idLang='+getIdLangParam(), 'application/xml', 'xml', photo.xmlData(),
					that.updatePhotoSuccess, that.updatePhotoError);
		};

		this.deletePhotoSuccess = function(xml) {
			that.notifyDeletePhotoSuccess();
		};

		this.deletePhotoError = function(xhr,status,error) {
			isSessionExpired(xhr.responseText);
		};
		
		this.deletePhoto = function(photo) {
			genericAjaxCall('GET', staticVars["urlBackEnd"]
					+ 'allfiles/deleteMedia/' + photo.getIdPhoto(), 'application/xml', 'xml', '',
					that.deletePhotoSuccess, that.deletePhotoError);
		};
		
		this.getPhotoById = function(photoId) {
			return listPhotosCache[photoId];
		};
		
		this.notifyLoadPhotos = function(listPhotos) {
			$.each(listeners, function(i) {
				listeners[i].loadPhotos(listPhotos);
			});
		};

		this.notifyAddPhotoSuccess = function() {
			$.each(listeners, function(i) {
				listeners[i].addPhotoSuccess();
			});
		};

		this.notifyUpdatePhotoSuccess = function() {
			$.each(listeners, function(i) {
				listeners[i].updatePhotoSuccess();
			});
		};

		this.notifyDeletePhotoSuccess = function() {
			$.each(listeners, function(i) {
				listeners[i].deletePhotoSuccess();
			});
		};
		
		this.addListener = function(list) {
			listeners.push(list);
		};
	},

	PhotosModelListener : function(list) {
		if (!list)
			list = {};
		return $.extend(list);
	},

	PhotosView : function() {
		var listeners = new Array();
		var that = this;
		var idPhotoCache = 0; 
		var urlPhotoCache = "";
		
		this.addListener = function(list) {
			listeners.push(list);
		};

		this.initView = function(){
      traduct();
			idPhotoCache = 0; 
			urlPhotoCache = "";
    	var jqXHRPhoto=null;
    	$(".draggable").draggable({ cancel: ".eds-dialog-inside" });
     	customJqEasyCounter("#photoTitle", 255, 255, '#0796BE', LEFTCARACTERES);
	    customJqEasyCounter("#photoDescription", 65535, 65535, '#0796BE', LEFTCARACTERES);
	    $("#photoForm td.jqEasyCounterMsg").css('width', '55%');
	    $('.closePopup,.eds-dialog-close').click(function() {
				$('#loadPhotoError').hide();
				$('.eds-dialog-container, .backgroundPopup').hide();
                if(jqXHRPhoto != null) jqXHRPhoto.abort();
			});
			
			$('#addPhotoLink').click(function() {
				idPhotoCache = 0;
				urlPhotoCache = "";
				enabledButton('#savePhotoButton');
				$('#loadPhotoError').hide();
				$('#photoTitle').val('');
				$('#photoDescription').val('');
				$('#imageEdited').empty();
				$('.popupPhoto .editLabel').addClass('addLabel').removeClass('editLabel').text(ADDLABEL);
				$('#loadPhoto').show();
				$('#photoLoaded,#progressPhoto').hide();
				$('.popupPhoto,.backgroundPopup').show();
			});
	
			$('#savePhotoButton').click(function() {
				if ($('#photoForm').valid() && !isDisabled('#savePhotoButton')) {
		    		disabledButton('#savePhotoButton');
					if(urlPhotoCache!=''){
						$('#loadingEds').show();
		                if (idPhotoCache != 0) {
							that.updatePhotoInfos(idPhotoCache);
						} else {
							that.sendPhotoInfos();
						}
					}else {
						$('#loadPhotoError').show();
			    		enabledButton('#savePhotoButton');
					}
				}else {
		    		enabledButton('#savePhotoButton');
				}
			});
	
			$('.deletePhotoButton').click(function() {
	    		disabledButton('.deletePhotoButton');
	    		that.deletePhotoInfos(idPhotoCache);
			});
			
		  $('#progressPhoto').hide();
			$('#photoUpload')
					.fileupload(
							{
								beforeSend : function(jqXHR, settings) {
									$('#imageEdited').empty();
				                	beforeUploadFile('#savePhotoButton', '#progressPhoto', '#loadPhoto,#loadPhotoError');
								},
			                    datatype: 'json',
			                    cache : false,
								add : function(e, data) {
									var goUpload = true;
									var uploadFile = data.files[0];
					        		var fileName=uploadFile.name;
					        		timeUploadPhoto=(new Date()).getTime();
									data.url = getUrlPostUploadFile(timeUploadPhoto, fileName);
									if (!(/\.(jpg|jpeg|png|gif)$/i)
											.test(fileName)) {
										goUpload = false;
									}
					                if (uploadFile.size > MAXFILESIZE) {
										goUpload = false;
									}
									if (goUpload == true) {
										jqXHRPhoto=data.submit();
									} else $('#loadPhotoError').show();
								},
						        progressall: function (e, data) {
				    	            progressBarUploadFile('#progressPhoto', data, 100);
						        },
								done : function(e, data) {
									var file=data.files[0];
				                	var fileName=file.name;
				                	var hashFileName=getUploadFileName(timeUploadPhoto, fileName);
				                	// console.log("Add photo done, filename :
													// "+fileName+", hashFileName : "+hashFileName);
				                	urlPhotoCache = getUploadFileUrl(hashFileName);
				                	$('#imageEdited').empty();
				            		$("<img width='50' src='"+ urlPhotoCache+ "' />").appendTo('#imageEdited');
									afterUploadFile('#savePhotoButton', '#photoLoaded', '#progressPhoto');
								}
							});
		};
		
		this.displayPhotos = function(listPhotos) {
			var rightGroup = listStandRights[rightGroupEnum.PHOTOS];
			if (rightGroup != null) {
				var permittedPhotosNbr = rightGroup.getRightCount();
				var photosNbr = Object.keys(listPhotos).length;
				if (permittedPhotosNbr == -1) {
					$('#addPhotoDiv #addPhotoLink').text(ADDLABEL);
					$('#addPhotoDiv').show();
				} else if (photosNbr < permittedPhotosNbr) {
					$('#numberPhotos').text(photosNbr);
	        		$('#numberPermittedPhotos').text(permittedPhotosNbr);
					$('#addPhotoDiv').show();
				} else {
					$('#addPhotoDiv').hide();
				}
			}
			$('#listStandPhotos').empty();
			var photo = null;
			for (var index in listPhotos) {
						photo=listPhotos[index];
                    	var photoId=photo.getIdPhoto();
                    	// console.log('photoId: '+photoId);
                    	var photoUrl=photo.getPhotoUrl();
                    	var photoTitle=photo.getPhotoTitle();
                    	var photoDescription=photo.getPhotoDescription();
						var data = {
							photoTitle : replaceSpecialChars(formattingLongWords(photoTitle, 50)),
							photoDescription : replaceSpecialChars(photoDescription),
							photoUrl : replaceSpecialChars(photoUrl),
							photoId : replaceSpecialChars(photoId),
							viewLabel : VIEWLABEL,
							editLabel : EDITLABEL,
							deleteLabel : DELETELABEL
						};
						$('#standPhotoTemplate').tmpl(data)
								.appendTo('#listStandPhotos');

						$('.viewPhoto' + photoId)
								.bind('click',
										function() {
											var photoId = $(this).closest('.onePhoto').attr('id');
											var photo=listPhotos[photoId];
                    	var photoUrl=photo.getPhotoUrl();
                    	var photoTitle=photo.getPhotoTitle();
                    	var formatPhotoTitle = formattingLongWords(photoTitle, 70);
					        		var customPhotoTitle = substringCustom(formatPhotoTitle, 70);
					    				$('#photoViewImage').attr('src', photoUrl);
											$('#photoTitlePopup').text(customPhotoTitle);
											$('#photoTitlePopup').attr('title', photoTitle);
											$('.popupPhotoViewer,.backgroundPopup').show();
										});

						$('.editPhoto' + photoId)
								.bind('click',function() {
											idPhotoCache = $(this).closest('.onePhoto').attr('id');
											var photo=listPhotos[idPhotoCache];
					                    	var photoUrl=photo.getPhotoUrl();
					                    	var photoTitle=photo.getPhotoTitle();
					                    	var photoDescription=photo.getPhotoDescription();
					                    	urlPhotoCache = photoUrl;
											$('.popupPhoto .addLabel').addClass('editLabel').removeClass('addLabel').text(EDITLABEL);
											$('#photoTitle').val(photoTitle);
											$('#photoDescription').val(photoDescription);
											$('#imageEdited').empty();
											$("<img width='50' src='"+ photoUrl+ "' />").appendTo('#imageEdited');
											$('#loadPhotoError, #photoLoaded, #progressPhoto').hide();
											$('#loadPhoto,.popupPhoto,.backgroundPopup').show();
								    		enabledButton('#savePhotoButton');
										});

						$('.deletePhoto' + photoId)
								.bind('click',
										function() {
											idPhotoCache = $(this).closest('.onePhoto').attr('id');
											$('.popupDeletePhoto,.backgroundPopup').show();
											enabledButton('.deletePhotoButton');
										});
					}
			$('#loadingEds').hide();
		};

		this.sendPhotoInfos = function() {
			var photoTitle = $('#photoTitle').val();
			var photoDescription = $('#photoDescription').val();
			var photo = new $.Photo();
			photo.setPhotoTitle(photoTitle);
			photo.setPhotoDescription(photoDescription);
			photo.setPhotoUrl(urlPhotoCache);
			var fileType = new $.FileType();
			fileType.setFileTypeId(fileTypeEnum.PHOTO);
			photo.setFileType(fileType);
			that.notifyAddPhoto(photo);
		};

		this.updatePhotoInfos = function(photoId) {
			var photoTitle = $('#photoTitle').val();
			var photoDescription = $('#photoDescription').val();
			var photo = listPhotosCache[photoId];
			photo.setPhotoTitle(photoTitle);
			photo.setPhotoDescription(photoDescription);
			photo.setPhotoUrl(urlPhotoCache);
			that.notifyUpdatePhoto(photo);
		};

		this.deletePhotoInfos = function(photoId) {
			var photo = listPhotosCache[photoId];
			that.notifyDeletePhoto(photo);
		};
		
		this.addPhotoSuccess = function() {
			hidePopups();
			showUpdateSuccess();
			that.notifyPhotoUpdated();
    		// enabledButton('#savePhotoButton');
		};
		
		this.updatePhotoSuccess = function() {
			hidePopups();
			showUpdateSuccess();
			that.notifyPhotoUpdated();
    		// enabledButton('#savePhotoButton');
		};
		
		this.deletePhotoSuccess = function() {
			hidePopups();
			showUpdateSuccess();
			that.notifyPhotoUpdated();
    		// enabledButton('.deletePhotoButton');
		};

		this.notifyAddPhoto = function(photo) {
			$.each(listeners, function(i) {
				listeners[i].addPhoto(photo);
			});
		};
		
		this.notifyUpdatePhoto = function(photo) {
			$.each(listeners, function(i) {
				listeners[i].updatePhoto(photo);
			});
		};

		this.notifyDeletePhoto = function(photo) {
			$.each(listeners, function(i) {
				listeners[i].deletePhoto(photo);
			});
		};
		
		this.notifyPhotoUpdated = function() {
			$.each(listeners, function(i) {
				listeners[i].photoUpdated();
			});
		};
		

	},

	PhotosViewListener : function(list) {
		if (!list)
			list = {};
		return $.extend(list);
	},
	
	PhotosController : function(model, view) {
		/**
		 * listen to the model
		 */
		var photosModelList = new $.PhotosModelListener({
			loadPhotos : function(listPhotos) {
				view.displayPhotos(listPhotos);
			},
			addPhotoSuccess : function() {
				view.addPhotoSuccess();
			},
			updatePhotoSuccess : function() {
				view.updatePhotoSuccess();
			},
			deletePhotoSuccess : function() {
				view.deletePhotoSuccess();
			},
		});
		model.addListener(photosModelList);
		this.initController = function(){
			view.initView();
			model.initModel();
		};
		/**
		 * listen to the view
		 */
		var photosViewList = new $.PhotosViewListener({
			addPhoto : function(photo) {
				model.addPhoto(photo);
			},
			updatePhoto : function(photo) {
				model.updatePhoto(photo);
			},
			deletePhoto : function(photo) {
				model.deletePhoto(photo);
			},
			photoUpdated : function() {
				model.getStandPhotos();
			},
		});
		view.addListener(photosViewList);

        this.getAnalyticsPhotos = function(callBack, path){
        	model.getAnalyticsPhotos(callBack, path);
        };
        this.getPhotoById = function(photoId){
        	return model.getPhotoById(photoId);
        };
	}
	
});


