jQuery
		.extend({

			AdviceModel : function() {
				var listeners = new Array();
				var that = this;

				this.getStandAdvicesSuccess = function(xml) {
					var listAdvices = new Array();
					$(xml).find("advice").each(function() {
						var advice = new $.Advice($(this));
						listAdvices[advice.idAdvice] = advice;
					});
					that.notifyLoadAdvices(listAdvices);
				};

				this.getStandAdvicesError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};

				this.getStandAdvices = function() {
					genericAjaxCall('GET', staticVars.urlBackEnd
							+ 'advice/standAdvices?idLang=' + getIdLangParam(),
							'application/xml', 'xml', '', that.getStandAdvicesSuccess,
							that.getStandAdvicesError);
				};

				this.addAdviceSuccess = function() {
					that.notifyAddAdviceSuccess();
				};

				this.addAdviceError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};

				this.addAdvice = function(advice) {
					genericAjaxCall('POST', staticVars.urlBackEnd
							+ 'advice/addOrUpdateAdvice?idLang=' + getIdLangParam(),
							'application/xml', 'xml', advice.xmlData(),
							that.addAdviceSuccess, that.addAdviceError);
				};

				this.updateAdviceSuccess = function() {
					that.notifyUpdateAdviceSuccess();
				};

				this.updateAdviceError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};
				this.updateAdvice = function(advice) {
					genericAjaxCall('POST', staticVars.urlBackEnd
							+ 'advice/addOrUpdateAdvice?idLang=' + getIdLangParam(),
							'application/xml', 'xml', advice.xmlData(),
							that.updateAdviceSuccess, that.updateAdviceError);
				};

				this.deleteAdviceSuccess = function() {
					that.notifyDeleteAdviceSuccess();
				};

				this.deleteAdviceError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};

				this.deleteAdvice = function(advice) {
					genericAjaxCall('POST',
							staticVars.urlBackEnd + 'advice/deleteAdvice', 'application/xml',
							'xml', advice.getIdAdvice(), that.deleteAdviceSuccess,
							that.deleteAdviceError);
				};

				this.getAnalyticsAdvicesSuccess = function(xml) {
					var listAdvices = new Array();
					$(xml).find("advice").each(function() {
						var advice = new $.Advice($(this));
						listAdvices[advice.idAdvice] = advice;
					});
					that.notifyLoadAdvices(listAdvices);
				};

				this.getAnalyticsAdvices = function(callBack, path) {
					// console.log('getAnalyticsAdvices : '+path);
					genericAjaxCall('GET', staticVars.urlBackEnd + path,
							'application/xml', 'xml', '', function(xml) {
								listAdvicesCache = new Array();
								$(xml).find("advice").each(function() {
									var advice = new $.Advice($(this));
									listAdvicesCache[advice.idAdvice] = advice;
								});
								callBack();
							}, that.getStandAdvicesError);
				};

				this.notifyAddAdviceSuccess = function() {
					$.each(listeners, function(i) {
						listeners[i].addAdviceSuccess();
					});
				};

				this.notifyUpdateAdviceSuccess = function() {
					$.each(listeners, function(i) {
						listeners[i].updateAdviceSuccess();
					});
				};

				this.notifyDeleteAdviceSuccess = function() {
					$.each(listeners, function(i) {
						listeners[i].deleteAdviceSuccess();
					});
				};

				this.notifyLoadAdvices = function(listAdvices) {
					$.each(listeners, function(i) {
						listeners[i].loadAdvices(listAdvices);
					});
				};

				this.addListener = function(list) {
					listeners.push(list);
				};

			},

			AdviceModelListener : function(list) {
				if (!list)
					list = {};
				return $.extend(list);
			},

			AdviceView : function() {

				var that = this;
				var listeners = new Array();
				this.idAdviceToChange = 0;
				this.urlDocument = "";
				this.nameDocument = "";
				this.idAdviceToChange = 0;
				var jqXHRAdvice = null;
				var $adviceTitle = null;
				var $adviceCreationForm = null;
				var $editorAdvice = null;
				this.initAdviceView = function() {
					traduct();
					$(".draggable").draggable({
						cancel : ".eds-dialog-inside"
					});
					that.idAdviceToChange = 0;
					that.urlDocument = "";
					that.nameDocument = "";
					that.idAdviceToChange = 0;
					that.listAdvicesCache = new Array();
					$adviceTitle = $('#adviceTitle');
					$adviceCreationForm = $('#adviceCreationForm');
					customJqEasyCounter($adviceTitle, 255, 255, '#0796BE', LEFTCARACTERES);
					$adviceTitle.find("div.jqEasyCounterMsg").css('width', '55%');
					/*
					 * switch(getIdLangParam()){ case '3': $editorAdvice =
					 * CKEDITOR.replace('adviceContent',{ language: 'fr'}); break; case
					 * '2': $editorAdvice = CKEDITOR.replace('adviceContent',{ language:
					 * 'en'}); break; case '1': $editorAdvice =
					 * CKEDITOR.replace('adviceContent',{ language: 'de'}); break;
					 * default: }
					 */
					$editorAdvice = $("#adviceContent");

					$('#cancelDeleteAdvice').click(function() {
						hidePopups();
					});
					$('#confirmDeleteAdvice').click(function() {
						disabledButton('#confirmDeleteAdvice');
						that.deleteAdvice(that.idAdviceToChange);
					});

					$('#textOption').click(function() {
						$('#uploadOptionDiv').hide();
						$('#textOptionDiv').show();
						$('#loadAdviceError,#loadAdviceTextError').hide();
						enabledButton('#saveAdvice');
						if (jqXHRAdvice != null)
							jqXHRAdvice.abort();
					});

					$('#uploadOption').click(function() {
						$('#textOptionDiv').hide();
						$('#uploadOptionDiv,#loadAdvice').show();
						$('#loadAdviceTextError,#loadAdviceError,#progressAdvice').hide();
						enabledButton('#saveAdvice');
					});

					$('.closePopup,.eds-dialog-close').click(function() {
						hidePopups();
					});

					$('#cancelAddingAdvice').click(function() {
						$('#adviceCreationDiv').hide();
						$('#listAdvicesDiv').show();
						$('#addAdviceLink').show();
						$('#loadAdvice').show();
						$('#adviceLoaded').hide();
						if (jqXHRAdvice != null)
							jqXHRAdvice.abort();
					});

					$("#addAdviceLink").click(function() {
						$('#progressAdvice').hide();
						that.nameDocument = "";
						that.urlDocument = "";
						that.idAdviceToChange = 0;
						// $editorAdvice.setData('');
						$editorAdvice.val('');
						$('#adviceCreationDiv input').val('');
						$('#documentToUpdateSpan').text("");
						$('#adviceCreationDiv,#loadAdvice').show();
						$('#addAdviceLink, #listAdvicesDiv, #adviceLoaded').hide();
						$('#textOption').click();
						enabledButton('#saveAdvice');
					});

					$("#saveAdvice")
							.click(
									function() {
										if ($adviceCreationForm.valid()
												&& !isDisabled('#saveAdvice')) {
											disabledButton('#saveAdvice');
											// if($("#adviceCreationForm
											// input:checked").attr('id')=="textOption" &&
											// $editorAdvice.getData().trim()=='') {
											if ($("#adviceCreationForm input:checked").attr('id') == "textOption"
													&& $editorAdvice.val().trim() == '') {
												$('#loadAdviceTextError').show();
												enabledButton('#saveAdvice');
											} else if ($("#adviceCreationForm input:checked").attr(
													'id') == "uploadOption"
													&& that.urlDocument == '') {
												$('#loadAdviceError').show();
												enabledButton('#saveAdvice');
											} else {
												$('#loadingEds').show();
												if (that.idAdviceToChange != 0) {
													that.updateAdvice(that.idAdviceToChange);
												} else {
													that.addAdvice();
												}
											}
										}
									});

					$('#cancelDeleteAdvice').click(function() {
						hidePopups();
					});

					$('#adviceUpload').fileupload(
							{
								beforeSend : function(jqXHR, settings) {
									beforeUploadFile('#saveAdvice', '#progressAdvice',
											'#loadAdvice,#loadAdviceError');
								},
								datatype : 'json',
								cache : false,
								add : function(e, data) {
									var goUpload = true;
									var uploadFile = data.files[0];
									var fileName = uploadFile.name;
									timeUploadAdvice = (new Date()).getTime();
									data.url = getUrlPostUploadFile(timeUploadAdvice, fileName);
									if (!(/\.(pdf)$/i).test(fileName)) {
										goUpload = false;

									}
									if (uploadFile.size > MAXFILESIZE) {
										goUpload = false;
									}
									if (goUpload == true) {
										jqXHRAdvice = data.submit();
									} else
										$('#loadAdviceError').show();
								},
								progressall : function(e, data) {
									progressBarUploadFile('#progressAdvice', data, 99);
								},
								done : function(e, data) {
									var file = data.files[0];
									var fileName = file.name;
									var hashFileName = getUploadFileName(timeUploadAdvice,
											fileName);
									// console.log("Add advice file done, filename :"+fileName+",
									// hashFileName : "+hashFileName);
									that.urlDocument = getUploadFileUrl(hashFileName);
									that.nameDocument = htmlEncode(fileName);
									;
									afterUploadFile('#saveAdvice', '#adviceLoaded',
											'#progressAdvice,#loadAdviceError');
								}
							});
				};
				this.displayAdvices = function(listAdvices) {
					var rightGroup = listStandRights[rightGroupEnum.ADVICES];
					if (rightGroup != null) {
						var permittedAdvicesNbr = rightGroup.getRightCount();
						var advicesNbr = Object.keys(listAdvices).length;
						if (permittedAdvicesNbr == -1) {
							$('#addAdviceDiv #addAdviceLink').text(ADDLABEL);
							$('#addAdviceDiv').show();
						} else if (advicesNbr < permittedAdvicesNbr) {
							$('#numberAdvices').text(advicesNbr);
			        $('#numberPermittedAdvices').text(permittedAdvicesNbr);
							$('#addAdviceDiv').show();
						} else {
							$('#addAdviceDiv').hide();
						}
					}
					that.listAdvicesCache = listAdvices;
					var adviceList = [];
					for (index in listAdvices) {
						var advice = listAdvices[index];
						var adviceId = advice.getIdAdvice();
						var adviceTitle = advice.getTitle();
						var formatAdviceTitle = formattingLongWords(adviceTitle, 70);
						var customAdviceTitle = substringCustom(formatAdviceTitle, 200);
						var htmlAdviceTitle = htmlEncode(customAdviceTitle);
						var adviceContent;
						if (advice.fileName != null
								&& (advice.getContent() == null || advice.getContent() == ''))
							adviceContent = unescape(advice.fileName);
						else
							adviceContent = unescape(advice.getContent());
						/*
						 * var formatAdviceContent = formattingLongWords(adviceContent, 70);
						 * var customAdviceContent = substringCustom(formatAdviceContent,
						 * 255);
						 */
						// var adviceUrl = advice.getUrl();
						// var listActions = "<div id='"+adviceId+"'><a
						// href='javascript:void(0)' class='editAdviceLink'
						// style='font-weight:bold;margin-right:18px;text-decoration:none;'><img
						// width='20' height='20' src='images/edit.png' alt='edit'></a><a
						// href='javascript:void(0)' class='deleteAdviceLink'
						// style='font-weight:bold;'><img width='18' height='18'
						// src='images/delete.png' alt='delete'></a></div>";
						var listActions = "<div id='"
								+ adviceId
								+ "'><button class='btn btn-success btn-middle viewAdviceLink' style='font-weight:bold;margin-right:20px;'>"
								+ VIEWLABEL
								+ "</button><button class='btn btn-primary btn-middle editAdviceLink' style='font-weight:bold;margin-right:20px;'>"
								+ EDITLABEL
								+ "</button><button class='btn btn-danger btn-middle deleteAdviceLink' style='font-weight:bold;'>"
								+ DELETELABEL + "</button></div>";
						var data = [
								htmlAdviceTitle,
								"<div class='contentTruncate'>"
										+ adviceContent.replace(/<(?:.|\n)*?>/gm, '') + "</div>",
								listActions ];
						adviceList.push(data);
					}
					$('#listAdvicesTable')
							.dataTable(
									{
										"aaData" : adviceList,
										"aaSorting" : [],
										"aoColumns" : [ {
											"sTitle" : TITLELABEL
										}, {
											"sTitle" : CONTENTLABEL
										}, {
											"sTitle" : ACTIONSLABEL
										} ],
										"bAutoWidth" : false,
										"bRetrieve" : false,
										"bDestroy" : true,
										"iDisplayLength" : 10,
										"fnDrawCallback" : function() {
											$('.viewAdviceLink').off();
											$('.viewAdviceLink')
													.on(
															"click",
															function() {
																var adviceId = $(this).closest('div')
																		.attr('id');
																$('#hereDocument').empty();
																var advice = listAdvices[adviceId];
																var adviceTitle = advice.getTitle();
																var adviceUrl = advice.getUrl();
																var adviceContent = advice.getContent();
																var formatAdviceTitle = formattingLongWords(
																		adviceTitle, 70);
																var customAdviceTitle = substringCustom(
																		formatAdviceTitle, 50);
																$('#documentTitlePopup')
																		.text(customAdviceTitle);
																$('#documentTitlePopup').attr('title',
																		adviceTitle);
																if (adviceUrl.trim().length != 0) {
																	var img_container = $(
																			'<object data="mozilla-pdf.js/web/viewer.html?file='
																					+ adviceUrl
																					+ '#locale='
																					+ localePDFJS
																					+ '" type="text/html" width="100%" height="100%"></object>')
																			.css('position', 'relative');
																	$('#hereDocument').prepend(img_container);
																	$('.popupDocumentView,.backgroundPopup')
																			.show();
																} else {
																	var img_container = $(
																			'<div style="width:660px;height:460px;padding:20px;overflow-y:auto;">'
																					+ unescape(adviceContent) + '</div>')
																			.css('position', 'relative');
																	$('#hereDocument').prepend(img_container);
																	$('.popupDocumentView,.backgroundPopup')
																			.show();
																}
															});

											$('.editAdviceLink').off();
											$('.editAdviceLink').on("click", function() {
												var adviceId = $(this).closest('div').attr('id');
												that.idAdviceToChange = adviceId;
												var advice = listAdvices[adviceId];
												var adviceTitle = advice.getTitle();
												var adviceUrl = advice.getUrl();
												var adviceContent = advice.getContent();
												$('#listAdvicesDiv').hide();
												$('#addAdviceLink,#progressAdvice').hide();
												$('#documentToUpdateSpan').text("");
												$adviceTitle.val(adviceTitle);
												if (adviceUrl.trim().length != 0) {
													that.nameDocument = adviceContent;
													that.urlDocument = adviceUrl;
													$('#documentToUpdateSpan').text(adviceContent);
													$('#uploadOption').click();
													setTimeout(function() {
														// $editorAdvice.setData('');
														$editorAdvice.val('');
													}, 0);
												} else {
													that.nameDocument = "";
													that.urlDocument = "";
													$('#textOption').click();
													setTimeout(function() {
														// $editorAdvice.setData(unescape(adviceContent));
														$editorAdvice.val(unescape(adviceContent));
													}, 0);
												}
												$('#loadAdvice').show();
												$('#adviceLoaded').hide();
												$('#adviceCreationDiv').show();
												enabledButton('#saveAdvice');
											});

											$('.deleteAdviceLink').off();
											$('.deleteAdviceLink').on(
													"click",
													function() {
														that.idAdviceToChange = $(this).closest('div')
																.attr('id');
														$('.popupDeleteAdvice,.backgroundPopup').show();
														enabledButton('#confirmDeleteAdvice');
													});
										}
									});

					// plugin which allow as to truncate text
					$('.contentTruncate').expander({
						slicePoint : 80, // default is 100
						expandPrefix : ' ', // default is '... '
						expandText : '[...]', // default is 'read more'
					// collapseTimer: 5000, // re-collapses after 5 seconds; default is 0,
					// so no re-collapsing
					// userCollapseText: '[^]' // default is 'read less'
					});
					$('#loadingEds').hide();
				};

				this.addAdvice = function() {
					var adviceTitle = htmlEncode($adviceTitle.val().trim());
					// var adviceContent = $editorAdvice.getData().trim();
					var adviceContent = $editorAdvice.val().trim();
					var advice = new $.Advice();
					advice.setTitle(adviceTitle);
					// console.log(adviceTitle);
					if ($("#adviceCreationForm input:checked").attr('id') == "textOption") {
						advice.setContent(escape(adviceContent));
					} else {
						advice.setUrl(that.urlDocument);
						advice.fileName = htmlEncode(that.nameDocument);
					}
					that.notifyAddAdvice(advice);
				};

				this.updateAdvice = function() {
					var adviceTitle = htmlEncode($adviceTitle.val().trim());
					// var adviceContent = $editorAdvice.getData().trim();
					var adviceContent = $editorAdvice.val().trim();
					var advice = new $.Advice();
					advice.setAdviceId(that.idAdviceToChange);
					advice.setTitle(adviceTitle);
					if ($("#adviceCreationForm input:checked").attr('id') == "textOption") {
						advice.setContent(escape(adviceContent));
					} else {
						advice.fileName = htmlEncode(that.nameDocument);
						advice.setUrl(that.urlDocument);
					}
					that.notifyUpdateAdvice(advice);
				};

				this.deleteAdvice = function(adviceId) {
					var advice = new $.Advice();
					advice.setAdviceId(adviceId);
					that.notifyDeleteAdvice(advice);
				};

				this.addAdviceSuccess = function() {
					that.refresh();
					// enabledButton('#saveAdvice');
				};

				this.updateAdviceSuccess = function() {
					that.refresh();
					// enabledButton('#saveAdvice');
				};

				this.deleteAdviceSuccess = function() {
					hidePopups();
					that.refresh();
					// enabledButton('#confirmDeleteAdvice');
				};

				this.refresh = function() {
					showUpdateSuccess();
					$('#adviceCreationDiv').hide();
					$('#listAdvicesDiv').show();
					$('#addAdviceLink').show();
					that.notifyRefresh();
				};

				this.addListener = function(list) {
					listeners.push(list);
				};

				this.notifyRefresh = function() {
					$.each(listeners, function(i) {
						listeners[i].adviceListRefresh();
					});
				};

				this.notifySaveAdviceClicked = function(adviceXml) {
					$.each(listeners, function(i) {
						listeners[i].saveAdvice(adviceXml);
					});
				};

				this.notifyAddAdvice = function(advice) {
					$.each(listeners, function(i) {
						listeners[i].addAdvice(advice);
					});
				};

				this.notifyUpdateAdvice = function(advice) {
					$.each(listeners, function(i) {
						listeners[i].updateAdvice(advice);
					});
				};

				this.notifyDeleteAdvice = function(advice) {
					$.each(listeners, function(i) {
						listeners[i].deleteAdvice(advice);
					});
				};
			},

			AdviceViewListener : function(list) {
				if (!list)
					list = {};
				return $.extend(list);
			},

			AdviceController : function(model, view) {
				/**
				 * listen to the model
				 */
				var adviceModelList = new $.AdviceModelListener({
					loadAdvices : function(listAdvices) {
						view.displayAdvices(listAdvices);
					},
					addAdviceSuccess : function() {
						view.addAdviceSuccess();
					},
					updateAdviceSuccess : function() {
						view.updateAdviceSuccess();
					},
					deleteAdviceSuccess : function() {
						view.deleteAdviceSuccess();
					},
				});
				model.addListener(adviceModelList);
				this.initAdviceController = function() {
					view.initAdviceView();
					model.getStandAdvices();
				};
				/**
				 * listen to the view
				 */

				var adviceViewList = new $.AdviceViewListener({
					addAdvice : function(advice) {
						model.addAdvice(advice);
					},
					updateAdvice : function(advice) {
						model.updateAdvice(advice);
					},
					deleteAdvice : function(advice) {
						model.deleteAdvice(advice);
					},
					adviceListRefresh : function(adviceXml) {
						model.getStandAdvices();
					},
				});
				this.getAnalyticsAdvices = function(callBack, path) {
					model.getAnalyticsAdvices(callBack, path);
				};
				view.addListener(adviceViewList);
			}
		});
