
jQuery.extend({
	
	EmailingModel: function(){
		var that = this;
		var listeners = new Array();
		
		this.initModel = function(){
			that.getStands();
		};

		this.getStandsSuccess = function(data){
			var stands = new Array();
			var stand = null;
			$(data).find("stand").each(function() {
				stand = new $.Stand($(this));
				stands[stand.getIdstande()] = stand;
			});
			that.notifyLoadStands(stands);
		};
		
		this.getStandsError = function(xhr, status, error){
		};
		
		this.getStands = function(){
			genericAjaxCall('GET', staticVars.urlBackEnd+'stand/emailingStands?idLang='+getParam('idLang'), 'application/xml', 'xml', 
					'', that.getStandsSuccess, that.getStandsError);
		};
		
		this.sendEmailingSuccess = function(data){
			that.notifySendEmailingSuccess();
		};
		
		this.sendEmailingError = function(xhr, status, error){
			that.notifySendEmailingError();
		};
		
		this.sendEmailing = function(email){
			 genericAjaxCall('POST', staticVars.urlBackEnd+'message/emailing?idLang='+getParam('idLang'), 'application/xml', 'xml', 
			 		email.xmlData(), that.sendEmailingSuccess, that.sendEmailingError);
		};
		
		this.notifyLoadStands = function(stands) {
			$.each(listeners, function(i) {
				listeners[i].loadStands(stands);
			});
		};
		
		this.notifySendEmailingSuccess = function() {
			$.each(listeners, function(i) {
				listeners[i].sendEmailingSuccess();
			});
		};

		this.notifySendEmailingError = function() {
			$.each(listeners, function(i) {
				listeners[i].sendEmailingError();
			});
		};

		this.addListener = function(list){
			listeners.push(list);
		};
	},
		
	EmailingModelListener: function(list) {
		if(!list) list = {};
		return $.extend(list);
	},
	
});

