
jQuery.extend({
	StandsEmailingManage : function(stands, view){
		//var that=this;
		var dom = $("<ul></ul>");
		
		this.getStands = function(){
			return stands;
		};
		
		this.refresh = function(){
			dom.empty();
			if(stands != null && stands != undefined){
				var stand = new $.Stand();
				stand.setIdstande(-1);
				stand.setName(ALLLABEL);
				var standRowManage = new $.StandRowManage(stand);
				dom.append(standRowManage.getDOM());
				for(var index in stands){
					var stand = stands[index];
					var standRowManage = new $.StandRowManage(stand);
					dom.append(standRowManage.getDOM());
				}
			}
		};
		
		this.getDOM = function(){ 
			return dom;
		};
		
		this.refresh();
	},
	StandRowManage : function(stand, view){
		//var that=this;
		var dom = $("<li><table><tbody>"
				+"<tr>"
					+"<td class='span1'>"
						+ "<input type='checkbox' name='emailing_stand' class='emailingStandChBox'>"
					+"</td>"
					+"<td class='span7'>"
						+"<label for='emailing_stand' class='emailingStandName'></label>"
					+"</td>"
				+"</tr>"
				+"</tbody></table></li>");
		
		this.getStand = function(){
			return stand;
		};
		
		this.refresh = function(){
			dom.find(".emailingStandChBox").val(stand.getIdstande());
			dom.find(".emailingStandChBox").attr("name", "emailing_stand_"+stand.getIdstande());
			dom.find(".emailingStandChBox").attr("data-value", stand.getName());
			dom.find(".emailingStandChBox").attr("data-id", stand.getIdstande());
			dom.find(".emailingStandName").text(stand.getName());
			dom.find(".emailingStandName").attr("for", "emailing_stand_"+stand.getIdstande());
			dom.find('.emailingStandChBox').unbind('click').bind('click', function() {
				if($(this).val() == -1){
					if ($(this).prop('checked') == true) {
						$('.emailingStandChBox').prop('checked', true);
					} else {
						$('.emailingStandChBox').prop('checked', false);
					}
				}else {
					if ($(this).prop('checked') == false) 
						$('.emailingStandChBox[data-id="-1"]').prop('checked', false);
				}
		    	var standNames = $(".emailingStandChBox:checkbox:checked").map(function(){
		    	      return $(this).attr("data-value");
	    	    }).get();
		    	$("#emailingEmail").val(standNames.join(", "));
			});
		};
		
		this.getDOM = function(){ 
			return dom;
		};
		
		this.refresh();
	},
	EmailingView: function(){
		var that = this;
		var listeners = new Array();
		that.emailingDescriptionEditor = null;
		
		this.initView = function(){
			$("#emailingStandsTo").hide();
		    traduct();
			$("#emailingEmail").unbind("focus").bind("focus", function() {
				$("#emailingStandsTo").show();
			});
			$(document).mouseup(function (e){
			    var container = $("#emailingEmail, #emailingStandsTo");

			    if (!container.is(e.target) // if the target of the click isn't the container...
			        && container.has(e.target).length === 0){ // ... nor a descendant of the container
			    	var standNames = $(".emailingStandChBox:checkbox:checked").map(function(){
			    	      return $(this).attr("data-value");
		    	    }).get();
			    	$("#emailingEmail").val(standNames.join(", "));
			    	$("#emailingStandsTo").hide();
			    }
			});
			$(".cancelEmailingButton").unbind("click").bind("click", function() {
				that.reinitForm();
			});
			$(".sendTestEmailingButton").unbind("click").bind("click", function() {
				$("#emaillingDescription").val(that.emailingDescriptionEditor.getData());
				$("#emailingEmail").val("a");
				if ($('#emailingForm').valid()
						&& !isDisabled('.sendTestEmailingButton')) {
					that.sendTestEmailing();
				} else {
					enabledButton('.sendTestEmailingButton');
				}
				$("#emailingEmail").val("");
				$('.emailingStandChBox').prop('checked', false);
			});
			$("#saveEmailingBtn").unbind("click").bind("click", function() {
				$("#emaillingDescription").val(that.emailingDescriptionEditor.getData());
				if ($('#emailingForm').valid()
						&& !isDisabled('#saveEmailingBtn')) {
					that.saveEmailing();
				} else {
					enabledButton('#saveEmailingBtn');
				}
			});
			that.validateEmailingForm();
			that.initDescriptionEditor();
		};
		this.reinitForm = function(){
			$("#emailingForm")[0].reset();
			that.emailingDescriptionEditor.setData("");
		};
		this.initDescriptionEditor = function(){
			switch (getIdLangParam()) {
				case '3':
					that.emailingDescriptionEditor = CKEDITOR.replace('emaillingDescription', {
						language : 'fr'
					});
					break;
				case '2':
					that.emailingDescriptionEditor = CKEDITOR.replace('emaillingDescription', {
						language : 'en'
					});
					break;
				default:
					that.emailingDescriptionEditor = CKEDITOR.replace('emaillingDescription', {
						language : 'fr'
					});
					break;
			}
		};
		this.validateEmailingForm = function() {
			$("#emailingForm").validate({
				errorPlacement : function errorPlacement(error, element) {
					element.after(error);
				},
				rules : {
					emailingEmail : {
						required : true
					},
					emailingSubject : {
						required : true
					},
					emaillingDescription : {
						required : true
					}
				}
			});
		};
		this.saveEmailing = function(){
			$('#loadingEds').show();
			var email = new $.Email();
			email.setSubject($("#emailingSubject").val());
			email.setBody(that.emailingDescriptionEditor.getData());
			var standIds = $(".emailingStandChBox:checkbox:checked").map(function(){
	    	      return $(this).val();
    	    }).get();
			var i = 0;
			var MAX_STAND = 15;
			var size = standIds.length;
			while(i<size){
		        var nextInc = Math.min(size-i, MAX_STAND);
				email.setAddress(standIds.slice(i,i+nextInc).join(", "));
				that.notifSendEmailling(email);
				i = i + nextInc;
			}
		};
		this.sendTestEmailing = function(){
			$('#loadingEds').show();
			var email = new $.Email();
			email.setAddress("-2");
			email.setSubject($("#emailingSubject").val());
			email.setBody(that.emailingDescriptionEditor.getData());
			that.notifSendEmailling(email);
		};
		this.displayStands = function(stands){
			var standsEmailingManage = new $.StandsEmailingManage(stands, that);
			$("#emailingStandsTo").append(standsEmailingManage.getDOM());
		},
		this.sendEmailingSuccess = function(){
			enabledButton('#saveEmailingBtn,.sendTestEmailingButton');
			hidePopups();
			messageSent();
		};
		
		this.sendEmailingError = function(){
			enabledButton('#saveEmailingBtn,.sendTestEmailingButton');
			hidePopups();
		};
		
		this.notifSendEmailling = function(email){
			$.each(listeners, function(i) {
				listeners[i].sendEmailing(email);
			});
		};
		
		this.addListener = function(list){
			listeners.push(list);
		};
		
	},
	
	EmailingViewListener: function(list) {
		if(!list) list = {};
		return $.extend(list);
	}
	
});

