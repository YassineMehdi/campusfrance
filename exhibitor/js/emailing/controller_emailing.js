jQuery.extend({

	EmailingController: function(model, view){
		//var that = this;
		/**
		 * listen to the view
		 */
		var vlist = $.EmailingViewListener({
			loadStands : function(stands){
				view.displayStands(stands);
			},
			sendEmailingSuccess : function(){
				view.sendEmailingSuccess();
			},
			sendEmailingError : function(){
				view.sendEmailingError();
			}
		});
		view.addListener(vlist);

		/**
		 * listen to the model
		 */
		var mlist = $.EmailingModelListener({		
			sendEmailing : function(email){
				model.sendEmailing(email);
			}
		});
		
		model.addListener(mlist);
		
		this.initController = function(){
			view.initView();
			model.initModel();
		};
	}

});
