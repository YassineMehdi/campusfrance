jQuery
		.extend({

			JobModel : function() {
				var listeners = new Array();
				var that = this;

				this.getStandJobOffersSuccess = function(xml) {
					$(xml).find("joboffer").each(function() {
						var jobOffer = new $.JobOffer($(this));
						listJobOffersCache[jobOffer.getJobId()] = jobOffer;
					});
					that.notifyLoadJobs(listJobOffersCache);
				};

				this.getStandJobOffersError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};

				this.getStandJobOffers = function() {
					listJobOffersCache = [];
					genericAjaxCall('GET', staticVars.urlBackEnd
							+ 'jobOffer/standJobOffers?idLang=' + getIdLangParam(),
							'application/xml', 'xml', '', that.getStandJobOffersSuccess,
							that.getStandJobOffersError);
				};

				this.getAnalyticsJobOffers = function(callBack, path) {
					// console.log('getAnalyticsJobOffers : '+path);
					listJobOffersCache = [];
					genericAjaxCall('GET', staticVars.urlBackEnd + path,
							'application/xml', 'xml', '', function(xml) {
								$(xml).find("joboffer").each(function() {
									var jobOffer = new $.JobOffer($(this));
									listJobOffersCache[jobOffer.getJobId()] = jobOffer;
								});
								callBack();
							}, that.getStandJobOffersError);
				};

				this.addJobofferSuccess = function() {
					listJobOffersCache = [];
					that.notifyJobOfferSaved();
				};

				this.addJobofferError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};

				this.addJoboffer = function(jobOfferXml) {
					genericAjaxCall('POST', staticVars.urlBackEnd
							+ 'jobOffer/addJobOffer?idLang=' + getIdLangParam(),
							'application/xml', 'xml', jobOfferXml, that.addJobofferSuccess,
							that.addJobofferError);
				};

				this.updateJobofferSuccess = function() {
					that.notifyJobOfferSaved();
				};

				this.updateJobofferError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};

				this.updateJoboffer = function(jobOfferXml) {
					genericAjaxCall('POST', staticVars.urlBackEnd
							+ 'jobOffer/updateJobOffer', 'application/xml', 'xml',
							jobOfferXml, that.updateJobofferSuccess, that.updateJobofferError);
				};

				this.traductJoboffer = function(jobOfferXml) {
					genericAjaxCall('POST', staticVars.urlBackEnd
							+ 'jobOffer/traductJobOffer', 'application/xml', 'xml',
							jobOfferXml, that.updateJobofferSuccess, that.updateJobofferError);
				};

				this.deleteJobOfferSuccess = function() {
					that.notifyJobOfferDeleted();
				};

				this.deleteJobOfferError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};

				this.deleteJobOffer = function(jobOfferId) {
					genericAjaxCall('POST', staticVars.urlBackEnd
							+ 'jobOffer/deleteJobOffer', 'application/xml', 'xml',
							jobOfferId, that.deleteJobOfferSuccess, that.deleteJobOfferError);
				};

				this.getJobOfferById = function(jobOfferId) {
					return listJobOffersCache[jobOfferId];
				};

				this.addListener = function(list) {
					listeners.push(list);
				};

				this.notifyJobOfferDeleted = function() {
					$.each(listeners, function(i) {
						listeners[i].jobOfferDeleted();
					});
				};

				this.notifyLoadSector = function(sector) {
					$.each(listeners, function(i) {
						listeners[i].loadSector(sector);
					});
				};
				this.notifyJobOfferSaved = function() {
					$.each(listeners, function(i) {
						listeners[i].jobOfferSaved();
					});
				};

				this.notifyLoadContract = function(contractType) {
					$.each(listeners, function(i) {
						listeners[i].loadContract(contractType);
					});
				};

				this.notifyLoadStudyLevel = function(studyLevel) {
					$.each(listeners, function(i) {
						listeners[i].loadStudyLevel(studyLevel);
					});
				};

				this.notifyLoadRegion = function(region) {
					$.each(listeners, function(i) {
						listeners[i].loadRegion(region);
					});
				};

				this.notifyLoadJob = function(jobOffer) {
					$.each(listeners, function(i) {
						listeners[i].loadJob(jobOffer);
					});
				};

				this.notifyLoadJobs = function(listJobOffers) {
					$.each(listeners, function(i) {
						listeners[i].loadJobs(listJobOffers);
					});
				};

			},

			JobModelListener : function(list) {
				if (!list)
					list = {};
				return $.extend(list);
			},

			JobView : function() {
				var listeners = new Array();
				var that = this;
				var listOffers = [];
				this.idJobOfferToChange = 0;
				this.isUpdate = false;
				this.isTraduct = false;
				var $jobCreationForm = null;
				var $jobTitle = null;
				var $jobUrl = null;
				var $jobSkills = null;
				var $jobUrlDiv = null;
				var $externalUrl = null;
				var $editorJobOffer = null;
				this.initJobOfferView = function() {
					traduct();
					that.idJobOfferToChange = 0;
					that.isUpdate = false;
					customJqEasyCounter("#jobTitle", 255, 255, '#0796BE', LEFTCARACTERES);

					$("#jobCreationForm div.jqEasyCounterMsg").css('width', '99%');

					/*
					 * switch(parseInt(getIdLangParam())){ case languagesIds.FRENCH:
					 * $editorJobOffer = CKEDITOR.replace('jobDescription',{ language:
					 * 'fr'}); break; case languagesIds.ENGLISH: $editorJobOffer =
					 * CKEDITOR.replace('jobDescription',{ language: 'en'}); break; case
					 * languagesIds.DEUTSCH: $editorJobOffer =
					 * CKEDITOR.replace('jobDescription',{ language: 'de'}); break;
					 * default:break; }
					 */
					$editorJobOffer = $("#jobDescription");

					$jobCreationForm = $('#jobCreationForm');
					$jobTitle = $('#jobTitle');
					$jobUrl = $('#jobUrl');
					$jobSkills = $('#jobSkills');
					$jobUrlDiv = $('#jobUrlDiv');
					$externalUrl = $('#externalUrl');

					customJqEasyCounter($('#jobSkills, #jobUrl'), 255, 255, '#0796BE',
							LEFTCARACTERES);
					$('#jobSkills, #jobUrl').find("div.jqEasyCounterMsg").css('width',
							'55%');
					// $editorJobOffer = CKEDITOR.replace('jobDescription');
					$('#applicationDeadline').datepicker();
					$('.closePopup,.eds-dialog-close').click(function() {
						hidePopups();
					});
					$(".draggable").draggable({
						cancel : ".eds-dialog-inside"
					});
					$('#cancelAddingJob').click(function() {
						$('#jobCreationDiv').hide();
						$('#listJobOffersDiv').show();
						$('#addJobOfferLink').show();
						that.isUpdate = false;
					});
					$('#cancelDeleteJob').click(function() {
						hidePopups();
					});
					$('#confirmDeleteJob').click(function() {
						if (!isDisabled('#confirmDeleteJob')) {
							disabledButton('#confirmDeleteJob');
							that.notifyDeleteJobOffer(that.idJobOfferToChange);
						}
					});

					$externalUrl.click(function() {
						if ($(this).is(':checked')) {
							$jobUrlDiv.show();
							$jobUrl.addClass('required');
							$jobUrl.addClass('url');
						} else {
							$jobUrlDiv.hide();
							$jobUrl.removeClass('required');
							$jobUrl.removeClass('url');
						}
					});

					$("#saveJob")
							.click(
									function() {
										if ($jobCreationForm.valid() && !isDisabled('#saveJob')) {
											disabledButton('#saveJob');
											$.cookie("isJobOffer", true);
											var jobTitle = $jobTitle.val().trim();
											var jobUrl = $jobUrl.val().trim();
											// var jobDescription = $editorJobOffer.getData().trim();
											var jobDescription = $editorJobOffer.val().trim();
											var jobSkills = $jobSkills.val().trim();
											var internalOffer = true;
											if ($externalUrl.is(':checked'))
												internalOffer = false;
											var sectXml = "";
											var cantonXml = "";
											$('#listActivitySectors tr').each(
													function() {
														sectXml = sectXml + "<secteurActDTO><idsecteur>"
																+ $(this).attr('class')
																+ "</idsecteur></secteurActDTO>";
													});
											$('#listCantons tr').each(
													function() {
														cantonXml = cantonXml + "<regionDTO><idRegion>"
																+ $(this).attr('class')
																+ "</idRegion></regionDTO>";
													});

											var jobOfferXml = "<internalApplication>"
													+ internalOffer
													+ "</internalApplication><secteurActs>"
													+ sectXml
													+ "</secteurActs><regions>"
													+ cantonXml
													+ "</regions><studyLevelDTO><idStudyLevel>"
													+ $('#studyLevelSelect').val()
													+ "</idStudyLevel></studyLevelDTO><functionCandidateDTO><idFunction>"
													+ $('#functionSelect').val()
													+ "</idFunction></functionCandidateDTO><experienceYearsDTO><idExperienceYears>"
													+ $('#experienceYearsSelect').val()
													+ "</idExperienceYears></experienceYearsDTO><title>"
													+ htmlEncode(jobTitle) + "</title>><description>"
													+ escape(jobDescription) + "</description><url>"
													+ htmlEncode(jobUrl) + "</url><tags>"
													+ htmlEncode(jobSkills) + "</tags>";
											if (that.isTraduct) {
												var languageId = $('#languageTranslateSelect').val();
												jobOfferXml = "<jobOffer><languageId>" + languageId
														+ "</languageId><idmetier>"
														+ that.idJobOfferToChange + "</idmetier>"
														+ jobOfferXml + "</jobOffer>";
												that.notifyTraductJobClicked(jobOfferXml);
											} else {
												if (that.isUpdate) {
													var languageId = parseInt(getIdLangParam());
													jobOfferXml = "<jobOffer><languageId>" + languageId
															+ "</languageId><idmetier>"
															+ that.idJobOfferToChange + "</idmetier>"
															+ jobOfferXml + "</jobOffer>";
													that.notifyUpdateJobClicked(jobOfferXml);
												} else {
													jobOfferXml = "<jobOffer>" + jobOfferXml
															+ "</jobOffer>";
													that.notifySaveJobClicked(jobOfferXml);
												}
											}
										}
									});

					$("#addJobOfferLink").click(function() {
						enabledButton('#saveJob');
						$("#listCantons").empty();
						$("#listActivitySectors").empty();
						$('#listJobOffersDiv').hide();
						$('#jobCreationDiv').show();
						$('#addJobOfferLink').hide();
						$jobTitle.val('');
						setTimeout(function() {
							$editorJobOffer.val('');
						}, 0);
						$jobSkills.val('');
						$externalUrl.prop("checked", false);
						$jobUrl.val("");
						$jobUrlDiv.hide();
						$jobUrl.removeClass('required');
						$jobUrl.removeClass('url');
						that.fillCombobox();
					});

					$('#activitySectorSelect')
							.change(
									function() {
										var activitySectorHandler = $('#jobCreationDiv #activitySectorSelect');
										if (activitySectorHandler.val() != 0)
											that.manageJobOfferSector(activitySectorHandler.val(),
													activitySectorHandler.find('option:selected').text());
									});

					$('#cantonSelect').change(
							function() {
								var cantonHandler = $('#jobCreationDiv #cantonSelect');
								if (cantonHandler.val() != 0)
									that.manageJobOfferRegion(cantonHandler.val(), cantonHandler
											.find('option:selected').text());
							});

				};

				this.displayJobs = function(listJobOffers) {
					var rightGroup = listStandRights[rightGroupEnum.JOB_OFFERS];
					if (rightGroup != null) {
						var permittedJobOffersNbr = rightGroup.getRightCount();
						var jobOffersNbr = Object.keys(listJobOffers).length;
						if (permittedJobOffersNbr == -1) {
							$('#addJobOfferDiv #addJobOfferLink').text(ADDLABEL);
							$('#addJobOfferDiv').show();
						} else if (jobOffersNbr < permittedJobOffersNbr) {
							$('#numberJobOffers').text(jobOffersNbr);
							$('#numberPermittedJobOffers').text(permittedJobOffersNbr);
							$('#addJobOfferDiv').show();
						} else {
							$('#addJobOfferDiv').hide();
						}
					}
					listOffers = [];
					for (index in listJobOffers) {
						var jobOffer = listJobOffers[index];
						var languages = jobOffer.getLanguagesId();
						var jobOfferId = jobOffer.getJobId();
						var jobOfferTitle = jobOffer.getJobTitle();
						var jobDescription = jobOffer.getJobDescription();
						var listActions = "";
						var isModifiable = false;
						if (languages[getIdLangParam()] != null) {
							isModifiable = true;
						}

						if (isModifiable) {
							listActions = "<div class='jobOfferActionDiv' id='"
									+ jobOfferId
									+ "'><button class='btn btn-success btn-middle viewJobOfferLink' style='font-weight:bold;margin-right:10px;'>"
									+ VIEWLABEL
									+ "</button><button class='btn btn-primary btn-middle editJobOfferLink' style='font-weight:bold;margin-right:10px;'>"
									+ EDITLABEL
									+ "</button><button class='btn btn-danger btn-middle deleteJobOfferLink' style='font-weight:bold; margin-right: 10px;'>"
									+ DELETELABEL + "</button></div>";
						} else {
							listActions = "<div class='jobOfferActionDiv' id='"
									+ jobOfferId
									+ "'><button class='btn btn-success btn-middle viewJobOfferLink' style='font-weight:bold;margin-right:10px;'>"
									+ VIEWLABEL
									+ "</button><button class='btn btn-danger btn-middle deleteJobOfferLink' style='font-weight:bold;'>"
									+ DELETELABEL + "</button></div>";
						}

						var data = [ jobOfferTitle,
								"<div class='contentTruncate'>" + jobDescription + "</div>",
								listActions ];
						listOffers.push(data);
					}
					$('#listJobOffersTable').dataTable(
							{
								"aaData" : listOffers,
								"aoColumns" : [ {
									"sTitle" : TITLELABEL
								}, {
									"sTitle" : DESCRIPTIONLABEL
								}, {
									"sTitle" : ACTIONSLABEL
								} ],
								"bAutoWidth" : false,
								"bRetrieve" : false,
								"bDestroy" : true,
								"iDisplayLength" : 10,
								"fnDrawCallback" : function() {

									$('.editJobOfferLink,.viewJobOfferLink').off();
									$('.viewJobOfferLink').on(
											"click",
											function() {
												var jobOfferId = $(this).closest('.jobOfferActionDiv')
														.attr('id');
												var jobOffer = listJobOffers[jobOfferId];
												console.log(jobOffer);
												that.showJobOffer(jobOffer);
											});
									$('.editJobOfferLink').on(
											"click",
											function() {
												var jobOfferId = $(this).closest('.jobOfferActionDiv')
														.attr('id');
												var jobOffer = listJobOffers[jobOfferId];
												that.isTraduct = false;
												that.factoryManageJobOffer(jobOffer);
											});

									$('.deleteJobOfferLink').off();
									$('.deleteJobOfferLink').on("click", function() {
										enabledButton('#confirmDeleteJob');
										var jobOfferId = $(this).closest('div').attr('id');
										that.idJobOfferToChange = jobOfferId;
										$('.popupDeleteJobOffer,.backgroundPopup').show();
									});
								}
							});
					$('.contentTruncate').expander({
						slicePoint : 80, // default is 100
						expandPrefix : ' ', // default is '... '
						expandText : '[...]', // default is 'read more'
						userCollapseText : '[^]' // default is 'read less'
					});
					$('#loadingEds').hide();
				};
				this.factoryManageJobOffer = function(jobOffer) {
					enabledButton('#saveJob');
					$("#listCantons").empty();
					$("#listActivitySectors").empty();
					that.fillCombobox();
					that.idJobOfferToChange = jobOffer.getJobId();
					that.isUpdate = true;
					var languages = jobOffer.getLanguagesId();
					$("#languageTranslateSelect").empty();
					// console.log(languages);
					for ( var languageId in listLangsEnum) {
						if (languages[languageId] == null) {
							$("#languageTranslateSelect").append(
									$('<option/>').val(languageId)
											.html(listLangsEnum[languageId]));
						}
					}
					$('#jobTitle').val(jobOffer.getJobTitle());
					$('#jobSkills').val(jobOffer.getJobSkills());
					$('#studyLevelSelect')
							.val(jobOffer.getStudyLevel().getIdStudyLevel());
					$('#functionSelect').val(
							jobOffer.getFunctionCandidate().getIdFunction());
					$('#experienceYearsSelect').val(
							jobOffer.getExperienceYears().getIdExperienceYears());
					setTimeout(function() {
						// $editorJobOffer.setData(jobOffer.getJobDescription());
						$editorJobOffer.val(jobOffer.getJobDescription());
					}, 0);
					if (jobOffer.isInternalApplication() == "false") {
						$externalUrl.prop("checked", "checked");
						$jobUrl.val(jobOffer.getJobUrl());
						$jobUrlDiv.show();
						$jobUrl.addClass('required');
						$jobUrl.addClass('url');
					} else {
						$externalUrl.prop("checked", false);
						$jobUrl.val("");
						$jobUrlDiv.hide();
						$jobUrl.removeClass('required');
						$jobUrl.removeClass('url');
					}

					that.manageJobOfferSectors(jobOffer.getSectors());
					that.manageJobOfferRegions(jobOffer.getRegions());
					$('#listJobOffersDiv').hide();
					$('#jobCreationDiv').show();
					$('#addJobOfferLink').hide();
					if (that.isTraduct == true) {
						$('#jobCreationForm jobUrlDiv').closest('div').hide();
						$('#jobCreationForm select').closest('div').hide();
						$('#listCantons,#listActivitySectors').hide();
						$jobUrlDiv.hide();
						$externalUrl.closest('div').hide();
					} else {
						$('#jobCreationForm select').closest('div').show();
						$('#listCantons,#listActivitySectors').show();
						$('#jobTranslationDiv').hide();
					}
					$jobCreationForm.valid();
				};
				this.fillCombobox = function() {
					fillActivitySectors();
					fillRegions();
					fillStudyLevels();
					fillFunctionsCandidate();
					fillExperienceYears();
				};
				this.manageJobOfferSectors = function(listSectors) {
					for (index in listSectors) {
						var value = listSectors[index];
						var idSector = value.getIdsecteur();
						var sectorName = value.getName();
						that.manageJobOfferSector(idSector, sectorName);
					}
				};
				this.manageJobOfferSector = function(idSector, sectorName) {
					var interestdata = {
						sector_val : replaceSpecialChars(idSector),
						sector_text : replaceSpecialChars(sectorName),
						removeLabel : REMOVELABEL
					};
					$(
							"#jobCreationDiv #activitySectorSelect option[value=" + idSector
									+ "]").remove();
					$("#tempSector").tmpl(interestdata).appendTo("#listActivitySectors");
					$('.removeSect' + idSector).bind(
							'click',
							function() {
								// console.log("Remove sector");
								$("#jobCreationDiv #activitySectorSelect").append(
										$('<option/>').val(idSector).html(sectorName));
								$(this).closest('table').remove();
							});
				};
				this.manageJobOfferRegions = function(listRegions) {
					for (index in listRegions) {
						var value = listRegions[index];
						var idCanton = value.getIdRegion();
						var cantonName = value.getRegionName();
						that.manageJobOfferRegion(idCanton, cantonName);
					}
				};
				this.manageJobOfferRegion = function(idCanton, cantonName) {
					var interestdata = {
						canton_val : replaceSpecialChars(idCanton),
						canton_text : replaceSpecialChars(cantonName),
						removeLabel : REMOVELABEL
					};
					$("#jobCreationDiv #cantonSelect option[value=" + idCanton + "]")
							.remove();
					$("#tempCanton").tmpl(interestdata).appendTo("#listCantons");
					$('.removeCanton' + idCanton).bind(
							'click',
							function() {
								$("#jobCreationDiv #cantonSelect").append(
										$('<option/>').val(idCanton).html(cantonName));
								$(this).closest('table').remove();
							});
				};

				this.jobOfferDeleted = function() {
					enabledButton('#confirmDeleteJob');
					$('.popupDeleteJobOffer,.backgroundPopup').hide();
					that.notifyRefresh();
				};

				this.showJobOffer = function(jobOffer) {
					$("#jobOfferViewContent").empty();
					$("#jobTitleView").text(
							formattingLongWords(jobOffer.getJobTitle(), 50));
					var data = {
						jobDescription : replaceSpecialChars(unescape(jobOffer
								.getJobDescription())),
						activitySectors : replaceSpecialChars(jobOffer.getNameSectors()),
						studyLevelName : replaceSpecialChars(jobOffer.getStudyLevelName()),
						cantons : replaceSpecialChars(jobOffer.getRegionNames()),
						functionCandidate : replaceSpecialChars(jobOffer
								.getFunctionCandidateName()),
						experienceYears : replaceSpecialChars(jobOffer
								.getExperienceYearsName()),
						jobTags : replaceSpecialChars(jobOffer.getJobSkills()),
						jobUrl : replaceSpecialChars(jobOffer.getJobUrl()),
						activitySectorsLabel : ACTIVITYSECTORLABEL,
						cantonLabel : CANTONLABEL,
						studyLevelLabel : STUDYLEVELLABEL,
						functionLabel : FUNCTIONLABEL,
						experienceYearsLabel : EXPERIENCEYEARSLABEL,
						skillsLabel : SKILLSLABEL,
						urlLabel : URLLABEL,
						returnLabel : RETURNLABEL
					};
					$('#jobOfferDetailsTemplate').tmpl(data).appendTo(
							"#jobOfferViewContent");
					$('.backgroundPopup').show();
					$('.popupJobOfferView').show();
					$('#jobOfferViewContent #returnToJobOffer').off();
					$('#jobOfferViewContent #returnToJobOffer').on("click", function() {
						$('.closePopup').trigger("click");
					});
				};

				this.refresh = function() {
					$.cookie('isJobOffer', true);
					enabledButton('#saveJob');
					window.location.reload();
				};

				this.addListener = function(list) {
					listeners.push(list);
				};

				this.notifyRefresh = function() {
					$.each(listeners, function(i) {
						listeners[i].jobListRefresh();
					});
				};
				this.notifyDeleteJobOffer = function(jobOfferId) {
					$.each(listeners, function(i) {
						listeners[i].deleteJobOffer(jobOfferId);
					});
				};

				this.notifyJobRowClicked = function(jobOfferId) {
					$.each(listeners, function(i) {
						listeners[i].jobRowClicked(jobOfferId);
					});
				};

				this.notifySaveJobClicked = function(jobOfferXml) {
					$.each(listeners, function(i) {
						listeners[i].saveJob(jobOfferXml);
					});
				};

				this.notifyUpdateJobClicked = function(jobOfferXml) {
					$.each(listeners, function(i) {
						listeners[i].updateJob(jobOfferXml);
					});
				};

				this.notifyTraductJobClicked = function(jobOfferXml) {
					$.each(listeners, function(i) {
						listeners[i].traductJob(jobOfferXml);
					});
				};

			},

			JobViewListener : function(list) {
				if (!list)
					list = {};
				return $.extend(list);
			},

			JobController : function(model, view) {
				// var that = this;

				var jobModelList = new $.JobModelListener({
					loadSector : function(sector) {
						view.addSector(sector);
					},
					loadContract : function(contract) {
						view.addContract(contract);
					},
					loadStudyLevel : function(studyLevel) {
						view.addStudyLevel(studyLevel);
					},
					loadRegion : function(region) {
						view.addRegion(region);
					},
					loadJob : function(jobOffer) {
						view.displayJob(jobOffer);
					},
					jobOfferSaved : function(jobOffer) {
						view.refresh();
					},
					jobOfferDeleted : function() {
						view.jobOfferDeleted();
					},
					loadJobs : function(listJobOffers) {
						// $('#numberJobOffers').text(listJobOffers.length);
						view.displayJobs(listJobOffers);
					},
				});
				model.addListener(jobModelList);
				this.initJobOfferController = function() {
					view.initJobOfferView();
					validateJobOfferForm();
					getActivitySectors();
					getRegions();
					getStudyLevels();
					getFunctionsCandidate();
					getExperienceYears();
					model.getStandJobOffers();
				};
				/**
				 * listen to the view
				 */
				var jobViewList = new $.JobViewListener({
					saveJob : function(jobOfferXml) {
						model.addJoboffer(jobOfferXml);
					},
					updateJob : function(jobOfferXml) {
						model.updateJoboffer(jobOfferXml);
					},
					traductJob : function(jobOfferXml) {
						model.traductJoboffer(jobOfferXml);
					},
					jobRowClicked : function(jobOfferId) {
						var jobOffer = model.getJobOfferById(jobOfferId);
						view.showJobOffer(jobOffer);
					},
					jobListRefresh : function() {
						model.getStandJobOffers();
					},
					deleteJobOffer : function(jobOfferId) {
						model.deleteJobOffer(jobOfferId);
					},
				});
				view.addListener(jobViewList);

				this.getAnalyticsJobOffers = function(callBack, path) {
					model.getAnalyticsJobOffers(callBack, path);
				};
				this.getJobOfferById = function(jobOfferId) {
					return model.getJobOfferById(jobOfferId);
				};
			}
		});
