jQuery
		.extend({
			DocumentsModel : function() {
				var listeners = new Array();
				var that = this;
				this.getStandDocumentsSuccess = function(xml) {
					listDocumentsCache = [];
					var document = null;
					$(xml).find("allfiles").each(function() {
						document = new $.Document($(this));
						listDocumentsCache[document.idDocument] = document;
					});
					that.notifyLoadDocuments(listDocumentsCache);
				};
				this.getStandDocumentsError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};

				this.getStandDocuments = function() {
					$('#listStandDocuments').empty();
					genericAjaxCall('GET', staticVars.urlBackEnd
							+ 'allfiles/standDocuments?idLang=' + getIdLangParam(),
							'application/xml', 'xml', '', that.getStandDocumentsSuccess,
							that.getStandDocumentsError);

				};
				this.getAnalyticsDocuments = function(callBack, path) {
					// console.log('getAnalyticsDocuments : ' + path);
					genericAjaxCall('GET', staticVars.urlBackEnd + path,
							'application/xml', 'xml', '', function(xml) {
								listDocumentsCache = [];
								var document = null;
								$(xml).find("allfiles").each(function() {
									document = new $.Document($(this));
									listDocumentsCache[document.idDocument] = document;
								});
								callBack();
							}, that.getStandDocumentsError);
				};
				this.addDocumentSuccess = function() {
					that.notifyAddDocumentSuccess();
				};
				this.addDocumentError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};

				this.addDocument = function(document) {
					genericAjaxCall('POST', staticVars.urlBackEnd
							+ 'allfiles/addFile?idLang=' + getIdLangParam(),
							'application/xml', 'xml', document.xmlData(),
							that.addDocumentSuccess, that.addDocumentError);
				};
				this.updateDocumentSuccess = function() {
					that.notifyUpdateDocumentSuccess();
				};
				this.updateDocumentError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};
				this.updateDocument = function(document) {
					genericAjaxCall('POST', staticVars.urlBackEnd
							+ 'allfiles/updateFile?idLang=' + getIdLangParam(),
							'application/xml', 'xml', document.xmlData(),
							that.updateDocumentSuccess, that.updateDocumentError);
				};

				this.deleteDocumentSuccess = function() {
					that.notifyDeleteDocumentSuccess();
				};
				this.deleteDocumentError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};
				this.deleteDocument = function(document) {
					genericAjaxCall('GET', staticVars.urlBackEnd
							+ 'allfiles/deleteMedia/' + document.getIdDocument(),
							'application/xml', 'xml', '', that.deleteDocumentSuccess,
							that.deleteDocumentError);
				};
				this.getDocumentById = function(documentId) {
					return listDocumentsCache[documentId];
				};

				this.notifyDocumentUpdated = function() {
					$.each(listeners, function(i) {
						listeners[i].documentUpdated();
					});
				};
				this.notifyLoadDocuments = function(listDocuments) {
					$.each(listeners, function(i) {
						listeners[i].loadDocuments(listDocuments);
					});
				};
				this.notifyAddDocumentSuccess = function() {
					$.each(listeners, function(i) {
						listeners[i].addDocumentSuccess();
					});
				};
				this.notifyUpdateDocumentSuccess = function() {
					$.each(listeners, function(i) {
						listeners[i].updateDocumentSuccess();
					});
				};
				this.notifyDeleteDocumentSuccess = function() {
					$.each(listeners, function(i) {
						listeners[i].deleteDocumentSuccess();
					});
				};

				this.addListener = function(list) {
					listeners.push(list);
				};
			},

			DocumentsModelListener : function(list) {
				if (!list)
					list = {};
				return $.extend(list);
			},

			DocumentsView : function() {
				var listeners = new Array();
				var that = this;
				var idDocumentToChange = 0;
				var documentUrlTochange = "";
				var jqXHRDocument = null;
				this.initDocumentView = function() {
					traduct();
					idDocumentToChange = 0;
					documentUrlTochange = "";
					jqXHRDocument = null;

					customJqEasyCounter("#documentTitle", 255, 255, '#0796BE',
							LEFTCARACTERES);
					customJqEasyCounter("#documentUrl", 255, 255, '#0796BE',
							LEFTCARACTERES);
					customJqEasyCounter("#documentDescription", 65535, 65535, '#0796BE',
							LEFTCARACTERES);
					$("#documentForm td.jqEasyCounterMsg").css('width', '55%');

					$('#addDocumentLink').click(
							function() {
								$('.popupDocument .editLabel').addClass('addLabel')
										.removeClass('editLabel').text(ADDLABEL);
								$('#documentLoaded,#progressDocument').hide();
								that.initPopupAddDocument('', '', '');
								$('#loadDocument, .popupDocument, .backgroundPopup').show();
								enabledButton('.saveDocumentButton');
							});
					$(".draggable").draggable({
						cancel : ".eds-dialog-inside"
					});

					$('#documentUrl').addClass('required');
					$('#urlOption').click(function() {
						$('#urlTr').show();
						$('#uploadTr').hide();
						$('#documentUrl').addClass('required');
						enabledButton('.saveDocumentButton');
						if (jqXHRDocument != null)
							jqXHRDocument.abort();
						$('#loadDocumentError').hide();
					});
					$('#uploadOption').click(function() {
						$('#urlTr').hide();
						$('#uploadTr').show();
						$('#documentUrl').removeClass('required');
						enabledButton('.saveDocumentButton');
						$('#loadDocumentValidError,#progressDocument').hide();
						$('#loadDocument').show();
					});

					$('.saveDocumentButton')
							.click(
									function() {
										if ($('#documentForm').valid()
												&& !isDisabled('.saveDocumentButton')) {
											disabledButton('.saveDocumentButton');
											if ($("#documentForm input:checked").attr('id') == "urlOption") {
												documentUrlTochange = $('#documentUrl').val();
											}
											if (documentUrlTochange != '') {
												$('#loadingEds').show();
												if (idDocumentToChange != 0) {
													that.updateDocument(idDocumentToChange);
												} else {
													that.addDocument();
												}
											} else {
												$('#loadDocumentError').show();
												enabledButton('.saveDocumentButton');
											}
										}
									});

					$('.closePopup,.eds-dialog-close').click(function(e) {
						e.preventDefault();
						$('.eds-dialog-container').hide();
						$('.backgroundPopup').hide();
						if (jqXHRDocument != null)
							jqXHRDocument.abort();
					});

					$('.deleteDocumentButton').click(function() {
						disabledButton('.deleteDocumentButton');
						that.deleteDocument(idDocumentToChange);
					});
					$('#documentUpload')
							.fileupload(
									{
										beforeSend : function(jqXHR, settings) {
											beforeUploadFile('.saveDocumentButton',
													'#progressDocument',
													'#loadDocument,#loadDocumentError');
										},
										datatype : 'json',
										cache : false,
										add : function(e, data) {
											var goUpload = true;
											var uploadFile = data.files[0];
											var fileName = uploadFile.name;
											timeUploadDocument = (new Date()).getTime();
											data.url = getUrlPostUploadFile(timeUploadDocument,
													fileName);
											if (!(/\.(pdf)$/i).test(fileName)) {
												goUpload = false;
											}
											if (uploadFile.size > MAXFILESIZE) {
												goUpload = false;
											}
											if (goUpload == true) {
												jqXHRDocument = data.submit();
											} else
												$('#loadDocumentError').show();
										},
										progressall : function(e, data) {
											progressBarUploadFile('#progressDocument', data, 99);
										},
										done : function(e, data) {
											var file = data.files[0];
											var fileName = file.name;
											var hashFileName = getUploadFileName(timeUploadDocument,
													fileName);
											// console.log("Add document done, filename :
											// "+fileName+", hashFileName : "+hashFileName);
											documentUrlTochange = getUploadFileUrl(hashFileName);
											afterUploadFile('.saveDocumentButton', '#documentLoaded',
													'#progressDocument,#loadDocument');
										}
									});
				};

				this.initPopupAddDocument = function(documentTitle, documentUrl,
						documentDescription) {
					idDocumentToChange = 0;
					documentUrlTochange = '';
					$('#documentTitle').val(documentTitle);
					$('#documentDescription').val(documentDescription);
					$('#documentUrl').val(documentUrl);
					$('#urlOption').click();
					$('#loadDocumentValidError').hide();

				};
				this.displayDocuments = function(listDocuments) {
					var rightGroup = listStandRights[rightGroupEnum.DOCUMENTS];
					if (rightGroup != null) {
						var permittedDocumentsNbr = rightGroup.getRightCount();
						var documentsNbr = Object.keys(listDocuments).length;
						if (permittedDocumentsNbr == -1) {
							$('#addDocumentDiv #addDocumentLink').text(ADDLABEL);
							$('#addDocumentDiv').show();
						} else if (documentsNbr < permittedDocumentsNbr) {
							$('#numberDocuments').text(documentsNbr);
			        $('#numberPermittedDocuments').text(permittedDocumentsNbr);
							$('#addDocumentDiv').show();
						} else {
							$('#addDocumentDiv').hide();
						}
					}
					var document = null;
					for ( var index in listDocuments) {
						document = listDocuments[index];
						var documentId = document.getIdDocument();
						var documentUrl = document.getDocumentUrl();
						var documentTitle = document.getDocumentTitle();
						var documentDescription = document.getDocumentDescription();
						var data = {
							documentTitle : replaceSpecialChars(formattingLongWords(
									documentTitle, 50)),
							documentDescription : replaceSpecialChars(documentDescription),
							documentId : replaceSpecialChars(documentId),
							documentUrl : replaceSpecialChars(documentUrl),
							viewLabel : VIEWLABEL,
							editLabel : EDITLABEL,
							deleteLabel : DELETELABEL
						};
						$('#standDocumentTemplate').tmpl(data).appendTo(
								'#listStandDocuments');
						$('.deleteDocument' + documentId).bind('click', function() {
							idDocumentToChange = $(this).closest('.oneDocument').attr('id');
							$('.popupDeleteDocument,.backgroundPopup').show();
							enabledButton('.deleteDocumentButton');
						});
						$('.editDocument' + documentId)
								.bind(
										'click',
										function() {
											idDocumentToChange = $(this).closest('.oneDocument')
													.attr('id');
											var document = listDocuments[idDocumentToChange];
											var documentTitle = document.getDocumentTitle();
											var documentDescription = document
													.getDocumentDescription();
											documentUrlTochange = document.getDocumentUrl();
											$('.popupDocument .addLabel').addClass('editLabel')
													.removeClass('addLabel').text(EDITLABEL);
											$('#documentTitle').val(documentTitle);
											$('#documentDescription').val(documentDescription);
											$('#documentUrl').val('');
											$('#uploadOption').click();
											$(
													'#documentLoaded, #loadDocumentError, #progressDocument,#loadDocumentValidError')
													.hide();
											$('#loadDocument, .popupDocument,.backgroundPopup')
													.show();
											enabledButton('.saveDocumentButton');
										});
						$('.viewDocument' + documentId)
								.bind(
										'click',
										function() {
											idDocumentToChange = $(this).closest('.oneDocument')
													.attr('id');
											var document = listDocuments[idDocumentToChange];
											var documentUrl = document.getDocumentUrl();
											var documentTitle = document.getDocumentTitle();
											var formatDocumentTitle = formattingLongWords(
													documentTitle, 70);
											var customDocumentTitle = substringCustom(
													formatDocumentTitle, 70);
											$('.popupDocumentView').show();
											$('.backgroundPopup').show();
											$('#documentTitlePopup').text(customDocumentTitle);
											$('#documentTitlePopup').attr('title', documentTitle);
											$('#hereDocument').empty();
											var img_container = $(
													'<object data="mozilla-pdf.js/web/viewer.html?file='
															+ documentUrl
															+ '#locale='
															+ localePDFJS
															+ '" type="text/html" width="100%" height="100%"></object>')
													.css('position', 'relative');
											$('#hereDocument').prepend(img_container);
										});

					}
					$('#loadingEds').hide();
				};

				this.addDocumentSuccess = function() {
					hidePopups();
					showUpdateSuccess();
					that.notifyDocumentUpdated();
					// enabledButton('.saveDocumentButton');
				};
				this.updateDocumentSuccess = function() {
					hidePopups();
					showUpdateSuccess();
					that.notifyDocumentUpdated();
					// enabledButton('.saveDocumentButton');
				};
				this.deleteDocumentSuccess = function() {
					hidePopups();
					showUpdateSuccess();
					that.notifyDocumentUpdated();
					// enabledButton('.deleteDocumentButton');
				};
				this.addDocument = function() {
					var documentTitle = $('#documentTitle').val();
					var documentDescription = $('#documentDescription').val();
					var document = new $.Document();
					document.setDocumentTitle(documentTitle);
					document.setDocumentDescription(documentDescription);
					var fileType = new $.FileType();
					var fileTypeId=$('#typefile').val();
					//fileType.setFileTypeId(fileTypeEnum.DOCUMENT);
					console.log(fileTypeId);
					fileType.setFileTypeId(fileTypeId);
					document.setFileType(fileType);
					if ($("#documentForm input:checked").attr('id') == "urlOption") {
						$('#loadDocumentValidError').hide();
						if (getExtension(documentUrlTochange) == "pdf") {
							genericAjaxCall('GET', staticVars.urlServerUpload + "?url="
									+ documentUrlTochange, 'text/plain', 'text', '', function(
									filename) {
								// console.log("Response : "+filename);
								if (filename != 'ko') {
									documentUrlTochange = staticVars.urlServerStorage + filename;
									document.setDocumentUrl(documentUrlTochange);
									that.notifyAddDocument(document);
								} else {
									$('#loadDocumentValidError').show();
									enabledButton('.saveDocumentButton');
									$('#loadingEds').hide();
								}
							}, function(xml) {
								$('#loadDocumentValidError').show();
								enabledButton('.saveDocumentButton');
								$('#loadingEds').hide();
							});
						} else {
							$('#loadDocumentValidError').show();
							enabledButton('.saveDocumentButton');
							$('#loadingEds').hide();
						}
					} else {
						document.setDocumentUrl(documentUrlTochange);
						that.notifyAddDocument(document);
					}
				};
				this.updateDocument = function(documentId) {
					var documentTitle = $('#documentTitle').val();
					documentTitle = htmlEncode(documentTitle);
					var documentDescription = $('#documentDescription').val();
					documentDescription = htmlEncode(documentDescription);
					var document = listDocumentsCache[documentId];
					document.setDocumentTitle(documentTitle);
					document.setDocumentDescription(documentDescription);
					if ($("#documentForm input:checked").attr('id') == "urlOption") {
						$('#loadDocumentValidError').hide();
						if (getExtension(documentUrlTochange) == "pdf") {
							genericAjaxCall('GET', staticVars.urlServerUpload + "?url="
									+ documentUrlTochange, 'text/plain', 'text', '', function(
									filename) {
								if (filename != 'ko') {
									documentUrlTochange = staticVars.urlServerStorage + filename;
									document.setDocumentUrl(documentUrlTochange);
									that.notifyUpdateDocument(document);
								} else {
									$('#loadDocumentValidError').show();
									enabledButton('.saveDocumentButton');
									$('#loadingEds').hide();
								}
							}, function(xml) {
								$('#loadDocumentValidError').show();
								enabledButton('.saveDocumentButton');
								$('#loadingEds').hide();
							});
						} else {
							$('#loadDocumentValidError').show();
							enabledButton('.saveDocumentButton');
							$('#loadingEds').hide();
						}
					} else {
						document.setDocumentUrl(documentUrlTochange);
						that.notifyUpdateDocument(document);
					}
				};
				this.deleteDocument = function(documentId) {
					var document = listDocumentsCache[documentId];
					that.notifyDeleteDocument(document);
				};
				this.notifyDocumentUpdated = function() {
					$.each(listeners, function(i) {
						listeners[i].documentUpdated();
					});
				};
				this.notifyAddDocument = function(document) {
					$.each(listeners, function(i) {
						listeners[i].addDocument(document);
					});
				};
				this.notifyUpdateDocument = function(document) {
					$.each(listeners, function(i) {
						listeners[i].updateDocument(document);
					});
				};

				this.notifyDeleteDocument = function(document) {
					$.each(listeners, function(i) {
						listeners[i].deleteDocument(document);
					});
				};

				this.addListener = function(list) {
					listeners.push(list);
				};
			},

			DocumentsViewListener : function(list) {
				if (!list)
					list = {};
				return $.extend(list);
			},

			DocumentsController : function(model, view) {
				/**
				 * listen to the model
				 */
				var documentsModelList = new $.DocumentsModelListener({
					loadDocuments : function(listDocuments) {
						view.displayDocuments(listDocuments);
					},
					documentUpdated : function() {
						view.documentUpdated();
					},
					addDocumentSuccess : function() {
						view.addDocumentSuccess();
					},
					updateDocumentSuccess : function() {
						view.updateDocumentSuccess();
					},
					deleteDocumentSuccess : function() {
						view.deleteDocumentSuccess();
					}
				});
				model.addListener(documentsModelList);
				this.initDocumentController = function() {
					view.initDocumentView();
					model.getStandDocuments();
				};
				/**
				 * listen to the view
				 */
				var documentsViewList = new $.DocumentsViewListener({
					documentUpdated : function() {
						model.getStandDocuments();
					},
					addDocument : function(document) {
						model.addDocument(document);
					},
					updateDocument : function(document) {
						model.updateDocument(document);
					},
					deleteDocument : function(document) {
						model.deleteDocument(document);
					}
				});
				view.addListener(documentsViewList);

				this.getAnalyticsDocuments = function(callBack, path) {
					model.getAnalyticsDocuments(callBack, path);
				};
				this.getDocumentById = function(documentId) {
					return model.getDocumentById(documentId);
				};
			}
		});
