jQuery
		.extend({

			TestimonyModel : function() {
				var listeners = new Array();
				var that = this;

				this.initModel = function() {
					that.getStandTestimonies();
				};
				this.getStandTestimoniesSuccess = function(xml) {
					var listTestimonies = new Array();
					$(xml).find("testimony").each(function() {
						var testimony = new $.Testimony($(this));
						listTestimonies.push(testimony);
					});
					that.notifyLoadTestimonies(listTestimonies);
				};

				this.getStandTestimoniesError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};

				this.getStandTestimonies = function() {
					genericAjaxCall('GET', staticVars.urlBackEnd
							+ 'testimony/standTestimonies?idLang=' + getIdLangParam(),
							'application/xml', 'xml', '', that.getStandTestimoniesSuccess,
							that.getStandTestimoniesError);
				};

				this.addTestimonySuccess = function() {
					that.notifyTestimonySaved();
					showComponent($('#saveTestimony'));
				};

				this.addTestimonyError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};

				this.addTestimony = function(testimonyXml) {
					hideComponent($('#saveTestimony'));
					genericAjaxCall('POST', staticVars.urlBackEnd
							+ 'testimony/addTestimony?idLang=' + getIdLangParam(),
							'application/xml', 'xml', testimonyXml, that.addTestimonySuccess,
							that.addTestimonyError);
				};

				this.updateTestimony = function(testimonyXml) {
					hideComponent($('#saveTestimony'));
					genericAjaxCall('POST', staticVars.urlBackEnd
							+ 'testimony/updateTestimony', 'application/xml', 'xml',
							testimonyXml, that.addTestimonySuccess, that.addTestimonyError);
				};

				this.deleteTestimonySuccess = function() {
					that.notifyTestimonyDeleted();
				};

				this.deleteTestimonyError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};

				this.deleteTestimony = function(testimonyId) {
					genericAjaxCall('POST', staticVars.urlBackEnd
							+ 'testimony/deleteTestimony', 'application/xml', 'xml',
							testimonyId, that.deleteTestimonySuccess,
							that.deleteTestimonyError);
				};

				this.addListener = function(list) {
					listeners.push(list);
				};

				this.notifyTestimonyDeleted = function() {
					$.each(listeners, function(i) {
						listeners[i].testimonyDeleted();
					});
				};

				this.notifyTestimonySaved = function() {
					$.each(listeners, function(i) {
						listeners[i].testimonySaved();
					});
				};

				this.notifyLoadTestimonies = function(listTestimonies) {
					$.each(listeners, function(i) {
						listeners[i].loadTestimonies(listTestimonies);
					});
				};

			},

			TestimonyModelListener : function(list) {
				if (!list)
					list = {};
				return $.extend(list);
			},

			TestimonyView : function() {

				var that = this;
				var listeners = new Array();
				this.idTestimonyToChange = 0;
				this.testimonyPhoto = staticVars.defaultImage; // default image for
				// user if no one was
				// chosen
				this.listTestimoniesCache = null;
				this.isUpdate = false;
				var jqXHRTestimony = null;
				var $testimonyName = null;
				var $testimonyJobTitle = null;
				var $testimonyCreationForm = null;
				this.initView = function() {
					traduct();
					that.idTestimonyToChange = 0;
					that.testimonyPhoto = staticVars.defaultImage; // default image for
					// user if no one was
					// chosen
					that.listTestimoniesCache = new Array();
					that.isUpdate = false;
					jqXHRTestimony = null;
					$testimonyName = $('#testimonyName');
					$testimonyJobTitle = $('#testimonyJobTitle');
					$testimonyCreationForm = $('#testimonyCreationForm');
					/*
					 * switch (getIdLangParam()) { case '3': $editorTestimony =
					 * CKEDITOR.replace('testimonyContent', { language : 'fr' }); break;
					 * case '2': $editorTestimony = CKEDITOR.replace('testimonyContent', {
					 * language : 'en' }); break; case '1': $editorTestimony =
					 * CKEDITOR.replace('testimonyContent', { language : 'de' }); break;
					 * default: break; }
					 */
					$editorTestimony = $("#testimonyContent");
					$(".draggable").draggable({
						cancel : ".eds-dialog-inside"
					});

					customJqEasyCounter("#testimonyName", 255, 255, '#0796BE',
							LEFTCARACTERES);
					customJqEasyCounter("#testimonyJobTitle", 255, 255, '#0796BE',
							LEFTCARACTERES);
					customJqEasyCounter("#testimonyContent", 65535, 65535, '#0796BE',
							LEFTCARACTERES);
					$("#testimonyCreationDiv form.div.jqEasyCounterMsg").css('width',
							'55%');
					$("#addTestimonyLink")
							.click(
									function() {
										that.isUpdate = false;
										$('#testimonyPhoto').attr('src',
												'images/default_avatar.jpg');
										that.testimonyPhoto = staticVars.defaultImage;
										$('#listTestimoniesDiv').hide();
										$('#testimonyCreationDiv input').val('');
										setTimeout(function() {
											// $editorTestimony.setData('');
											$editorTestimony.val('');
										}, 0);
										$(
												'#testimonyCreationDiv,#testimonyPhoto,#testimonyCreationForm .filebutton')
												.show();
										$(
												'#addTestimonyLink,#progressPhotoTestimony,#loadPhotoTestimonyError')
												.hide();
										enabledButton('#saveTestimony');
									});

					$("#saveTestimony")
							.click(
									function() {
										if ($testimonyCreationForm.valid()
												&& !isDisabled('#saveTestimony')) {
											disabledButton('#saveTestimony');
											if (that.isUpdate) {
												var testimonyJobTitle = $testimonyJobTitle.val().trim(), testimonyName = $testimonyName
														.val().trim(), testimonyContent = $editorTestimony
														.val().trim(), testimonyXml = "<testimony><idTestinomy>"
														+ that.idTestimonyToChange
														+ "</idTestinomy><photo>"
														+ that.testimonyPhoto
														+ "</photo><name>"
														+ htmlEncode(testimonyName)
														+ "</name><jobTitle>"
														+ htmlEncode(testimonyJobTitle)
														+ "</jobTitle><testimonyContent>"
														+ escape(testimonyContent)
														+ "</testimonyContent></testimony>";
												that.notifyUpdateTestimonyClicked(testimonyXml);
											} else {
												var testimonyJobTitle = $testimonyJobTitle.val().trim(), testimonyName = $testimonyName
														.val().trim(), testimonyContent = $editorTestimony
														.val().trim(), testimonyXml = "<testimony><photo>"
														+ that.testimonyPhoto + "</photo><name>"
														+ htmlEncode(testimonyName) + "</name><jobTitle>"
														+ htmlEncode(testimonyJobTitle)
														+ "</jobTitle><testimonyContent>"
														+ escape(testimonyContent)
														+ "</testimonyContent></testimony>";
												that.notifySaveTestimonyClicked(testimonyXml);
											}
										}
									});
					$('#cancelDeleteTestimony').click(function() {
						hidePopups();
					});
					$('#confirmDeleteTestimony').click(function() {
						that.deleteTestimony(that.idTestimonyToChange);
					});
					$('#testimonyBeginDate').datepicker();
					$('.closePopup,.eds-dialog-close').click(function() {
						hidePopups();
					});
					$('#cancelAddingTestimony').click(function() {
						$('#testimonyCreationDiv').hide();
						$('#listTestimoniesDiv').show();
						$('#addTestimonyLink').show();
						if (jqXHRTestimony != null)
							jqXHRTestimony.abort();
					});
					$('#cancelDeleteTestimony').click(function() {
						hidePopups();
					});
					$('#testimonyPhotoUpload')
							.fileupload(
									{
										beforeSend : function(jqXHR, settings) {
											beforeUploadFile('#saveTestimony',
													'#progressPhotoTestimony',
													'#testimonyCreationForm .filebutton,#loadPhotoTestimonyError');
										},
										datatype : 'json',
										cache : false,
										add : function(e, data) {
											var goUpload = true;
											var uploadFile = data.files[0];
											var fileName = uploadFile.name;
											timeUploadTestimony = (new Date()).getTime();
											data.url = getUrlPostUploadFile(timeUploadTestimony,
													fileName);
											if (!(/\.(gif|jpg|jpeg|tiff|png)$/i).test(fileName)) {
												goUpload = false;
											}
											if (uploadFile.size > MAXFILESIZE) {
												goUpload = false;
											}
											if (goUpload == true) {
												jqXHRTestimony = data.submit();
											} else
												$('#loadPhotoTestimonyError').show();
										},
										progressall : function(e, data) {
											progressBarUploadFile('#progressPhotoTestimony', data,
													100);
										},
										done : function(e, data) {
											var file = data.files[0];
											var fileName = file.name;
											var hashFileName = getUploadFileName(timeUploadTestimony,
													fileName);
											// console.log("Add testimony done, filename :
											// "+fileName+", hashFileName : "+hashFileName);
											urlPhotoCache = getUploadFileUrl(hashFileName);
											that.testimonyPhoto = urlPhotoCache;
											$('#testimonyPhoto').attr('src', urlPhotoCache);
											afterUploadFile('#saveTestimony',
													'#testimonyCreationForm .filebutton',
													'#progressPhotoTestimony');
										}
									});
				};

				this.deleteTestimony = function(testimonyId) {
					nbreWSCallsInProgress++;
					$('#loadingEds').show();
					$.ajax({
						url : staticVars["urlBackEnd"] + 'testimony/deleteTestimony',
						type : 'POST',
						data : testimonyId,
						cache : cacheAjax,
						success : function() {
							$('.popupDeleteTestimony,.backgroundPopup').hide();
							that.testimonyDeleted();
						},
						statusCode : {
							401 : exceptionUnAutorized
						}
					}).complete(function() {
						nbreWSCallsInProgress--;
						if (nbreWSCallsInProgress < 1) {
							$('#loadingEds').hide();
							nbreWSCallsInProgress = 0;
						}
					});
				};

				this.testimonyDeleted = function() {
					$.each(listeners, function(i) {
						listeners[i].testimonyListRefresh();
					});
				};
				this.displayTestimonies = function(listTestimonies) {
					var rightGroup = listStandRights[rightGroupEnum.TESTIMONIES];
					if (rightGroup != null) {
						var permittedTestimoniesNbr = rightGroup.getRightCount();
						var testimoniesNbr = Object.keys(listTestimonies).length;
						if (permittedTestimoniesNbr == -1) {
							$('#addTestimoniesDiv #addTestimonyLink').text(ADDLABEL);
							$('#addTestimoniesDiv').show();
						} else if (testimoniesNbr < permittedTestimoniesNbr) {
							$('#numberTestimonies').text(testimoniesNbr);
			        $('#numberPermittedTestimonies').text(permittedTestimoniesNbr);
							$('#addTestimoniesDiv').show();
						} else {
							$('#addTestimoniesDiv').hide();
						}
					}
					$('#loadingEds').show();
					var photoLabel = PHOTOLABEL;
					var firstAndLastNameLabel = FIRSTANDLASTNAMELABEL;
					var jobTitleLabel = JOBTITLELABEL;
					var testimonyLabel = TESTIMONYLABEL;
					var viewLabel = VIEWLABEL;
					var editLabel = EDITLABEL;
					var deleteLabel = DELETELABEL;
					var actionsLabel = ACTIONSLABEL;
					that.listTestimoniesCache = listTestimonies;
					var testimonyList = [];
					for (index in listTestimonies) {
						var testimony = listTestimonies[index];
						var testimonyId = testimony.getTestimonyId();
						var testimonyName = testimony.getName();
						var testimonyJobTitle = testimony.getJobTitle();
						var testimonyContent = unescape(testimony.getTestimonyContent());
						var testimonyPhoto = testimony.getPhoto();
						testimonyPhoto = "<img width='60' src='" + testimonyPhoto
								+ "' alt='photo' />";
						var listActions = "<div id='"
								+ testimonyId
								+ "'><button class='btn btn-success btn-middle viewTestimonyLink' style='font-weight:bold;margin-right:10px;'>"
								+ viewLabel
								+ "</button><button class='btn btn-primary btn-middle editTestimonyLink' style='font-weight:bold;margin-right:10px;'>"
								+ editLabel
								+ "</button><button class='btn btn-danger btn-middle deleteTestimonyLink' style='font-weight:bold;'>"
								+ deleteLabel + "</button></div>";
						var data = [
								testimonyPhoto,
								testimonyName,
								testimonyJobTitle,
								"<div class='contentTruncate'>"
										+ testimonyContent.replace(/<(?:.|\n)*?>/gm, '') + "</div>",
								listActions ];
						testimonyList.push(data);
					}
					$('#listTestimoniesTable')
							.dataTable(
									{
										"aaData" : testimonyList,
										"aoColumns" : [ {
											"sTitle" : photoLabel
										}, {
											"sTitle" : firstAndLastNameLabel,
											"sWidth" : "10%",
										}, {
											"sTitle" : jobTitleLabel,
											"sWidth" : "20%",
										}, {
											"sTitle" : testimonyLabel,
											"sWidth" : "20%",
										}, {
											"sTitle" : actionsLabel,
											"sClass" : "alignRight",
										} ],
										"bAutoWidth" : false,
										"bRetrieve" : false,
										"bDestroy" : true,
										"iDisplayLength" : 10,
										"fnDrawCallback" : function() {
											$('.viewTestimonyLink')
													.off("click")
													.on(
															"click",
															function() {
																var testimonyId = $(this).closest('div').attr(
																		'id');
																$('.popupTestimonyView, .backgroundPopup')
																		.show();
																for (index in listTestimonies) {
																	var testimony = listTestimonies[index];
																	if (testimony.getTestimonyId() == testimonyId) {
																		$('#testimonyPhotoView').attr('src',
																				testimony.getPhoto());
																		$('#testimonyNameView').text(
																				testimony.getName());
																		$('#testimonyJobTitleView').text(
																				testimony.getJobTitle());
																		$('#testimonyContentView').html(
																				unescape(testimony
																						.getTestimonyContent()));
																		break;
																	}
																}

															});
											$('.editTestimonyLink')
													.off("click")
													.on(
															"click",
															function() {
																var testimonyId = $(this).closest('div').attr(
																		'id');
																that.idTestimonyToChange = testimonyId;
																that.isUpdate = true;
																$(
																		'#listTestimoniesDiv,#addTestimonyLink,#progressPhotoTestimony,#loadPhotoTestimonyError')
																		.hide();
																$(
																		'#testimonyCreationDiv,#testimonyPhoto,#testimonyCreationForm .filebutton')
																		.show();
																enabledButton('#saveTestimony');
																for (index in listTestimonies) {
																	var testimony = listTestimonies[index];
																	if (testimony.getTestimonyId() == testimonyId) {
																		$testimonyName.val(testimony.getName());
																		that.testimonyPhoto = testimony.getPhoto();
																		$('#testimonyPhoto').attr('src',
																				testimony.getPhoto());
																		$testimonyJobTitle.val(testimony
																				.getJobTitle());
																		setTimeout(function() {
																			// $editorTestimony.setData(unescape(testimony.getTestimonyContent()));
																			$editorTestimony.val(unescape(testimony
																					.getTestimonyContent()));
																		}, 0);
																		break;
																	}
																}

															});

											$('.deleteTestimonyLink').off("click")
													.on(
															"click",
															function() {
																var testimonyId = $(this).closest('div').attr(
																		'id');
																that.idTestimonyToChange = testimonyId;
																$('.popupDeleteTestimony,.backgroundPopup')
																		.show();
															});
										}
									});

					// function to truncate text
					$('.contentTruncate').expander({
						slicePoint : 80, // default is 100
						expandPrefix : ' ', // default is '... '
						expandText : '[...]', // default is 'read more'
						// collapseTimer: 5000, // re-collapses after 5 seconds; default is
						// 0, so no re-collapsing
						userCollapseText : '[^]' // default is 'read less'
					});
					$('#loadingEds').hide();

				};

				this.refresh = function() {
					$('#testimonyCreationDiv').hide();
					$('#listTestimoniesDiv').show();
					$('#addTestimonyLink').show();
					enabledButton('#saveTestimony');
					that.notifyRefresh();
				};

				this.addListener = function(list) {
					listeners.push(list);
				};

				this.notifyRefresh = function() {
					$.each(listeners, function(i) {
						listeners[i].testimonyListRefresh();
					});
				};

				this.notifySaveTestimonyClicked = function(testimonyXml) {
					$.each(listeners, function(i) {
						listeners[i].saveTestimony(testimonyXml);
					});
				};

				this.notifyUpdateTestimonyClicked = function(testimonyXml) {
					$.each(listeners, function(i) {
						listeners[i].updateTestimony(testimonyXml);
					});
				};

			},

			TestimonyViewListener : function(list) {
				if (!list)
					list = {};
				return $.extend(list);
			},

			TestimonyController : function(model, view) {
				/**
				 * listen to the model
				 */
				var testimonyModelList = new $.TestimonyModelListener({
					testimonySaved : function() {
						view.refresh();
					},
					loadTestimonies : function(listTestimonies) {
						view.displayTestimonies(listTestimonies);
					},
				});
				model.addListener(testimonyModelList);

				/**
				 * listen to the view
				 */

				var testinomyViewList = new $.TestimonyViewListener({
					saveTestimony : function(testinomyXml) {
						model.addTestimony(testinomyXml);
					},
					updateTestimony : function(testinomyXml) {
						model.updateTestimony(testinomyXml);
					},
					testimonyListRefresh : function(testinomyXml) {
						model.getStandTestimonies();
					},
				});
				view.addListener(testinomyViewList);

				this.initController = function() {
					view.initView();
					model.initModel();
				};
			}

		});

