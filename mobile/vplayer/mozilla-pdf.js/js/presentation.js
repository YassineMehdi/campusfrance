var tryNbrUpdatePDFView = 0;
var MAX_TRY_UPDATE_PDF_VIEW = 10;
var TIME_OUT_UPDATE_PDF_VIEW=1000;

jQuery.extend({
	ResourcePresentation: function(data){
		var that = this;
		this.ressourceUrl='';
		this.pageNumber='';
		this.pageZoom='';
		this.pageRotation='';

		this.resourcePresentation = function(data){
			if(data){
				if($(data).find("ressourceUrl")) that.ressourceUrl=$(data).find("ressourceUrl").text();
				if($(data).find("pageNumber")) that.pageNumber=$(data).find("pageNumber").text();
				if($(data).find("pageZoom")) that.pageZoom=$(data).find("pageZoom").text();
				if($(data).find("pageRotation")) that.pageRotation=$(data).find("pageRotation").text();
			}
		},
		
		this.getRessourceUrl= function(){	
			return that.ressourceUrl;
		},
		
		this.getPageNumber = function(){	
			return that.pageNumber;
		},
		
		this.getPageZoom = function(){	
			return that.pageZoom;
		},
		
		this.getPageRotation = function(){	
			return that.pageRotation;
		},
		
		this.setRessourceUrl = function(ressourceUrl){
			that.ressourceUrl = ressourceUrl;
		},

		this.setPageNumber = function(pageNumber){
			that.pageNumber = pageNumber;
		},

		this.setPageZoom = function(pageZoom){
			that.pageZoom = pageZoom;
		},

		this.setPageRotation = function(pageRotation){
			that.pageRotation = pageRotation;
		},
		
		this.update = function(newData){
			that.resourcePresentation(newData);
		};
		
		this.xmlData = function(){
			var xml='<resourcePresentationDTO>';
			xml+='<ressourceUrl>'+that.ressourceUrl+'</ressourceUrl>';
			xml+='<pageNumber>'+that.pageNumber+'</pageNumber>';
			xml+='<pageZoom>'+that.pageZoom+'</pageZoom>';
			xml+='<pageRotation>'+that.pageRotation+'</pageRotation>';
			xml+='</resourcePresentationDTO>';
			return xml;
		};
		
		this.resourcePresentation(data);
	}	
}); 

$(function() {
	$('#toolbarViewer').css('visibility', 'hidden');
	$('#toolbarViewer, #toolbarContainer, #toolbar').css('height', '0px');
	$('#viewerContainer').css('top', '0px');
});

function initPresentation(affectationWidget){
	var widget = affectationWidget.getWidget(); 
	var resourceUrl = affectationWidget.getAffectationWidgetValue();
	var right = widget.getWidgetRight();
	if(right == rightEnum.MANAGER){
		$("#userProfileAvatar").attr("src", "images/icons/presentator-avatar.png");
		$('#containerPresentation').css('visibility', 'visible');
		$('#viewerContainer').css('overflow', 'hidden');
		$('#toolbarViewer').css('visibility', 'visible');
		$('#toolbarViewer,#toolbarContainer, #toolbar').css('height', '32px');
		$('#viewerContainer').css('top', '32px');
		isPresentator=true;
		openPDFFile_(resourceUrl);
	} else if (right == rightEnum.VIEWER) {
		$('#containerPresentation').css('visibility', 'visible');
		$('#toolbarViewer').css('visibility', 'hidden');
		$('#toolbarViewer,#toolbarContainer, #toolbar').css('height', '0px');
		$('#viewerContainer').css('top', '0px');
		$('#viewerContainer').css('overflow', 'hidden');
		openPDFFile_(resourceUrl);
	} else {
		$('#containerPresentation').css('visibility', 'hidden');
	}
	factoryOnResize();
	setTimeout(factoryOnResize, 1500);
	mozL10n.setLanguage("fr-FR");
}

function updatePDFFile(ressourceUrl, pageNumber, pageZoom, pageRotation){
	setTimeout(function(){
		openPDFFile_(ressourceUrl, pageNumber, pageZoom, pageRotation);
		}, DELAY_TIME_WEB_CAM);
}

function publishInfo(ressourceUrl, pageNumber, pageZoom, pageRotation){	
	if($.cookie("g") == groupEnum.MANAGER && isPresentator){
		var resourcePresentation = new $.ResourcePresentation();
		resourcePresentation.setRessourceUrl(ressourceUrl);
		resourcePresentation.setPageNumber(pageNumber);
		resourcePresentation.setPageZoom(pageZoom);
		resourcePresentation.setPageRotation(pageRotation);
		genericAjaxCall('POST', staticVars.urlBackEnd + 'webcast/presentation/publish?webcastToken='+$.cookie("webcastToken"), 'application/xml', 'xml', resourcePresentation.xmlData(), 
			function(xml) {
		}, function() {
		});
	}
}
function openPDFFile_(url_, currentNumPage, currentZoom, currentRotation)
{
	if(!isPresentator || isFirstTime){
		if(PDFViewerApplication.url != url_){
			if(currentNumPage == null || currentNumPage == '') currentNumPage = 1;
			if(currentZoom == null || currentZoom == '') {
				/*var width = $("body").width();
				if(width > 1270 && width < 1330 ) currentZoom = 0.7; 
				else currentZoom = "page-fit";//1*/
				//currentZoom = 0.7; 
				currentZoom = "page-fit"; 
			}
			if(currentRotation == null || currentRotation == '') currentRotation = 0;
			PDFViewerApplication.open(url_);
			tryNbrUpdatePDFView = 0;
			updatePDFView(url_, currentNumPage, currentZoom, currentRotation);
		}else {
			if(PDFViewerApplication.page != currentNumPage && currentNumPage!=undefined) {
				PDFViewerApplication.page=currentNumPage;
			}
			if(PDFViewerApplication.currentScaleValue != currentZoom && currentZoom!=undefined) {
				PDFViewerApplication.setScale(currentZoom, true);
			}
			if(PDFViewerApplication.pageRotation != currentRotation && currentRotation!=undefined) {
				PDFView.rotatePages(0);
			}
		}
		isFirstTime=false;
	}
}
function updatePDFView(url_, currentNumPage, currentZoom, currentRotation){
	setTimeout(function(){
		if(!PDFViewerApplication.loading) {
			isFirstTime = true;
			openPDFFile_(url_, currentNumPage, currentZoom, currentRotation);
		}else if(tryNbrUpdatePDFView < MAX_TRY_UPDATE_PDF_VIEW) updatePDFView(url_, currentNumPage, currentZoom, currentRotation);
	}, TIME_OUT_UPDATE_PDF_VIEW);
	tryNbrUpdatePDFView++;
}