jQuery
		.extend({
			NotificationView : function() {
				var listeners = new Array();
				var that = this;
				var MAX_NOTIFICATION_SHOW = 3;
				var MAIL_FROM_RECRUITER_SEND = "mail_from_recruiter_send";
				var UNSOLICITED_CANDIDATURE = "unsolicited_candidature";
				var SOLICITED_CANDIDATURE = "solicited_candidature";
				var ALERT_STAND_MESSAGE = "alert_stand_message";
				var ALERT_FORUM_MESSAGE = "alert_forum_message";
				var ALERT_STAND_CONNECTED = "alert_stand_connected";
				var QUESTION_INFO = "question_info";
				var MAX_CHARACTERS_SENTENCE_NOTIF = 40;
				var MAX_CHARACTERS_WORD_NOTIF = 20;
				var mailNotifHandler = null;
				var messageContainerHandler = null;
				var noMailNotificationsHandler = null;
				var homeNotifHandler = null;
				var forumConnectedContainerHandler = null;
				var noDribbleNotificationsHandler = null;
				var listNotifMessages = null;
				var listNotifStandConnected = null;

				var displayMoreButton = null;
				var displayMoreMessageButton = null;
				var notificationListDisplay = null;

				this.initView = function() {

					messageContainerHandler = $('#message-container');
					noMailNotificationsHandler = $('#no-mail-notifications');
					forumConnectedContainerHandler = $('#forum-connected-container');
					noDribbleNotificationsHandler = $('#no-dribbble-notifications');

					listNotifStandConnected = $('.fui-radio-checked-content');
					listNotifMessages = $('.notif.msg');

					displayMoreButton = $('.display-all-alerts');
					displayMoreMessageButton = $('.display-all-messages');
					notificationListDisplay = $('#notification tbody');

					displayMoreButton.click(function() {
						//document.title = FORUM_TITLE;
						$('.modal-loading').show();
						$('.genericContent').hide();
						$("#content_notification").show();
						$('#page-content').removeClass('no-padding');
						//$('.receive-connected-alert').remove();
						$('.categList').hide();       
                      	$('#content').show();
						$('.modal-loading').hide();
					});

					displayMoreMessageButton.click(function() {
						//document.title = FORUM_TITLE;
						//$('#mail-notif-index').remove();
						//listNotifMessages.empty();
						$('.userMessagesLink').trigger('click');
					});
					$('#content_notification .scroll-pane').jScrollPane({
						autoReinitialise : true
					});
				};

				this.displayNotif = function(notifMessage) {
					var type = notifMessage.notifType;
					if (type != null)
						type = type.toLowerCase();
					that.decodeURINotif(notifMessage);
					switch (type) {
						case MAIL_FROM_RECRUITER_SEND:
							that.receivedMailFromRecruiter(notifMessage);
							break;
						case QUESTION_INFO:
							that.receivedQuestionInfo(notifMessage);
							break;
						case ALERT_STAND_MESSAGE:
						case ALERT_FORUM_MESSAGE:
							if (notifMessage.languageId == getIdLangParam()){
								that.receivedAlertStandConnected(notifMessage);
								that.addNotification(type, notifMessage, 'class="icon-notif1"');
							}
							break;
						case ALERT_STAND_CONNECTED:
								that.receivedAlertStandConnected(notifMessage);
								that.addNotification(type, notifMessage, 'class="icon-notif2"');
							break;
						}

				};
				this.receivedMailFromRecruiter = function(notifMail) {
					// console.log("receivedMailFromRecruiter");
					var id = notifMail.id;
					var notifId = notifMail.idNotif;
					var subject = notifMail.subject;
					var body = notifMail.body;
					var notifDate = notifMail.dateTime;
					subject = that.factoryFormattingString(subject);
					body = that.factoryFormattingString(body);
					console.log(body);
					noMailNotificationsHandler.hide();

					// ////////////////////////////////////////////////

					listNotifMessages
							.prepend('<li id="linkMailNotif'
									+ id
									+ '"  ><a href="#" class="notifMessageElement"><div class="notificationID" style="display:none;">'
									+ notifId + '</div>' + '<h3>' + subject + '<span>' + body
									+ '</span></h3>' + '<div class="date">'
									+ timestampsToDate(notifDate) + '</div>' + '</a></li>');
										
					var counter = messageContainerHandler.find(".notifMessageElement").length;
					if (counter > MAX_NOTIFICATION_SHOW) {
						$(messageContainerHandler.find(".notifMessageElement")[MAX_NOTIFICATION_SHOW]).closest('li').hide();
					}

					mailNotifHandler = $('#mail-notif-index');
					if(mailNotifHandler.length == 0){
						mailNotifHandler = $('<span id="mail-notif-index" class="nbr iconbar-unread"></span>');
						messageContainerHandler
						.prepend(mailNotifHandler);
					}
					mailNotifHandler.text(counter);

					// ////////////////////////////////////////////////

					that.updateTitle();

					$('#linkMailNotif' + id)
							.click(
									function() {
										var notificationClickedID = $(this).find('.notificationID')
												.text();
										notificationConversationIndex = notificationClickedID;
										$('.userMessagesLink').trigger('click');
										$(this).remove();
										var counter = messageContainerHandler.find(".notifMessageElement").length;
										if (counter == 0) {
											mailNotifHandler.remove();
											noMailNotificationsHandler.show();
										} else{
											mailNotifHandler.text(counter);
											if(counter >= MAX_NOTIFICATION_SHOW){
												$(messageContainerHandler.find(".notifMessageElement")[MAX_NOTIFICATION_SHOW - 1]).closest('li').show();
											}
										}
										that.updateTitle();
										that.notifyShowNotif(id);
									});
				};
				this.receivedQuestionInfo = function(notifUnsolicitedC) {
					// console.log("receivedQuestionInfo");
					var type = notifUnsolicitedC.notifType;
					if (type != null)
						type = type.toLowerCase();
					var id = notifUnsolicitedC.id;
					var notifId = notifUnsolicitedC.idNotif;
					var subject = unescape(notifUnsolicitedC.subject);
					var body = unescape(notifUnsolicitedC.body);
					var notifDate = notifUnsolicitedC.dateTime;
					subject = that.factoryFormattingString(subject);
					body = that.factoryFormattingString(body);
					noMailNotificationsHandler.hide();

					// ////////////////////////////////////////////////

					listNotifMessages
							.prepend('<li id="linkMailNotif'
									+ id
									+ '" ><a href="#" class="notifMessageElement"><div class="notificationID" style="display:none;">'
									+ notifId + '</div>' + '<h3>' + subject + '<span>' + body
									+ '</span></h3>' + '<div class="date">'
									+ timestampsToDate(notifDate) + '</div>' + '</a></li>');

					var counter = messageContainerHandler.find(".notifMessageElement").length;
					if (counter > MAX_NOTIFICATION_SHOW) {
						$(messageContainerHandler.find(".notifMessageElement")[MAX_NOTIFICATION_SHOW]).closest('li').hide();
					}

					mailNotifHandler = $('#mail-notif-index');
					if(mailNotifHandler.length == 0){
						mailNotifHandler = $('<span id="mail-notif-index" class="nbr iconbar-unread"></span>');
						messageContainerHandler.prepend(mailNotifHandler);
					}
					mailNotifHandler.text(counter);

					// ////////////////////////////////////////////////

					that.updateTitle();

					$('#linkMailNotif' + id)
							.click(
									function() {
										toQuestionInfoResponse = true;
										$('.userQuestionInfoLink').trigger('click');

										$(this).remove();
										var counter = messageContainerHandler.find(".notifMessageElement").length;
										if (counter == 0) {
											mailNotifHandler.remove();
											noMailNotificationsHandler.show();
										} else{
											mailNotifHandler.text(counter);
											if(counter >= MAX_NOTIFICATION_SHOW){
												$(messageContainerHandler.find(".notifMessageElement")[MAX_NOTIFICATION_SHOW - 1]).closest('li').show();
											}
										}

										that.updateTitle();
										that.notifyShowNotif(id);

									});
				};
				this.receivedAlertStandConnected = function(notifStandConnect) {
					// console.log("receivedAlertStandConnected");
					var id = notifStandConnect.id;
					var notifId = notifStandConnect.idNotif;
					var subject = notifStandConnect.subject;
					var body = "";
					if (notifStandConnect.body == ADD_STAND)
						body = STAND_CONNECTED_LABEL;
					else if (notifStandConnect.body == REMOVE_STAND)
						body = STAND_DISCONNECTED_LABEL;
					else
						body = notifStandConnect.body;
					var notifDate = notifStandConnect.dateTime;
					subject = that.factoryFormattingString(subject);
					body = that.factoryFormattingString(body);
					noDribbleNotificationsHandler.hide();

					// ////////////////////////////////////////////////

					var standNotifHtml = '<li id="linkStandConnectedNotif'
								+ id
								+ '"><a href="#" class="notifStandConnectedElement"><div class="notificationID" style="display:none;">'
							+ notifId
							+ '</div>'
							+ '<h3>'
							+ subject
							+ '<span>'
							+ body
							+ '</span></h3>'
							+ '<div class="date"><img src="img/icon-notif2.png" alt="" class="left">'
							+ timestampsToDate(notifDate) + '</div>' + '</a></li>';

					listNotifStandConnected.prepend(standNotifHtml);
					var counter = forumConnectedContainerHandler.find(".notifStandConnectedElement").length;
					if (counter > MAX_NOTIFICATION_SHOW) {
						$(forumConnectedContainerHandler.find(".notifStandConnectedElement")[MAX_NOTIFICATION_SHOW])
								.closest('li').hide();
					}

					homeNotifHandler = $('#radio-checked-notif-index');
					if(homeNotifHandler.length == 0){
						homeNotifHandler = $('<span id="radio-checked-notif-index" class="nbr receive-connected-alert iconbar-unread"></span>');
						forumConnectedContainerHandler.prepend(homeNotifHandler);
					}
					homeNotifHandler.text(counter);

					// ////////////////////////////////////////////////

					that.updateTitle();

					$('#linkStandConnectedNotif' + id)
							.click(
									function() {
										var notificationClickedID = $(this).find('.notificationID')
												.text();
										currentStandId = parseInt(notificationClickedID);
										$('#listStandsLink').trigger('click');
										$('.categList').hide();       
				                      	$('#content').show();
										that.readNotif(id);
									});
				};
				this.addNotification = function(type, notifMessage, picture){
					var notifPictureType = "";
					var subject = that.factoryFormattingString(notifMessage.subject);
					var body = that.factoryFormattingString(notifMessage.body);
					if(type == ALERT_STAND_CONNECTED){
						if (body == ADD_STAND)
							body = STAND_CONNECTED_LABEL;
						else if (body == REMOVE_STAND)
							body = STAND_DISCONNECTED_LABEL;
					}
				
					$('.noDataFound').hide();
					notificationListDisplay.prepend('<tr id="notificationID_'
							+ notifMessage.id + '" class="notificationDetail">'
							+ '<td class="cel-logo-company linkToDetailsNotif_'
							+ notifMessage.id + '">' + '<div class="logo-company-wrapper">'
							+ '<a href="#" class="logo-company">' + '<img class="" src="'
							+ notifMessage.picture + '" alt="" />' + '</a>' + '</div>'
							+ '</td>' + '<td class="linkToDetailsNotif_' + notifMessage.id
							+ '">' + '<a class="detail-notif" href="#">' + '<h3><span '
							+ notifPictureType + '></span>' + subject + '</h3>' + '<p>'
							+ body + '<span class="date">'
							+ timestampsToDate(notifMessage.dateTime) + '</span></p>'
							+ '</a>' + '</td>' + '<td>'
							+ '<a class="visiter-action" href="#" id="deleteNotification_'
							+ notifMessage.id + '">'
							+ '<img alt="" src="img/icons/icon-delete.png" class="">'
							+ '</a>' + '</td>' + '</tr>');

					$('.linkToDetailsNotif_' + notifMessage.id).click(
							function() {
								$('#linkStandConnectedNotif' + notifMessage.id).trigger("click");
							});
					$('#deleteNotification_' + notifMessage.id).click(
							function() {
								$('.modal-loading').show();
								that.readNotif(notifMessage.id);
								$('.modal-loading').hide();
							});
				};
				this.readNotif = function(id){
					if(document.getElementById("linkStandConnectedNotif"+ id) != undefined){
						$("#linkStandConnectedNotif"+ id).remove();
					}
					if(document.getElementById("notificationID_"+ id) != undefined){
						$("#notificationID_"+ id).remove();
					}
					var counter = forumConnectedContainerHandler.find(".notifStandConnectedElement").length;
					if (counter == 0) {
						homeNotifHandler.remove();
						noDribbleNotificationsHandler.show();
					} else{
						homeNotifHandler.text(counter);
						if(counter >= MAX_NOTIFICATION_SHOW){
							$(forumConnectedContainerHandler.find(".notifStandConnectedElement")[MAX_NOTIFICATION_SHOW - 1]).closest('li').show();
						}
					}
					if($("#notification .notificationDetail").length <= 0) 
						$('.noDataFound').show();
					
					that.updateTitle();
					that.notifyShowNotif(id);
				};
				this.updateTitle = function(){
					var notificationGlobalCounter = forumConnectedContainerHandler.find(".notifStandConnectedElement").length
																					+ messageContainerHandler.find(".notifMessageElement").length;
					if(notificationGlobalCounter > 0) document.title = "(" + notificationGlobalCounter+ ") " + FORUM_TITLE;
					else {
						notificationGlobalCounter = 0;
						document.title = FORUM_TITLE;
					}
				};
				this.factoryFormattingString = function(str) {
					if (str != null) {
						str = that.replaceAllAdd(str);
						str = substringCustom(str, MAX_CHARACTERS_SENTENCE_NOTIF);
						str = formattingLongWords(str, MAX_CHARACTERS_WORD_NOTIF);
						str = htmlEncode(str);
					}
					return str;
				};
				this.factoryFormattingStringToDetails = function(str) {
					if (str != null) {
						str = htmlEncode(str);
					}
					return str;
				};
				this.replaceAllAdd = function(str) {
					if (str != null)
						return str.split('+').join(" ");
					else
						return str;
				};
				this.decodeURINotif = function(notif) {
					try {
						notif.subject = decodeURIComponent(notif.subject);
					} catch (err) {
						console.log("Error decode");
					}
					try {
						notif.body = decodeURIComponent(notif.body);
					} catch (err) {
						console.log("Error decode");
					}
					try {
						notif.picture = decodeURIComponent(notif.picture);
					} catch (err) {
						console.log("Error decode");
					}
				};
				this.notifLoaded = function(object) {
					that.displayNotif(object);
				};

				this.notifyNotif = function() {
					$.each(listeners, function(i) {
						listeners[i].getNotifyNotif();
					});
				};
				this.notifyShowNotif = function(idNotif) {
					// console.log('inside here');
					$.each(listeners, function(i) {
						listeners[i].showNotif(idNotif);
					});
				};

				this.addListener = function(list) {
					listeners.push(list);
				};
				// this.initView();
			},
			NotifViewList : function(list) {
				if (!list)
					list = {};
				return $.extend(list);
			},
		});


