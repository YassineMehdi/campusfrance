jQuery.extend({
  NotificationModel: function () {
    var listeners = new Array();
    var that = this;
    var tryToReconnectToNotifMsg = 0;
    var MAX_TRY_TO_RECONNECT_TO_NOTIF_MSG = 30;
    var PUBLIC_TOPIC = "allNFC";
    this.addListener = function (list) {
      listeners.push(list);
    };
    this.initModel = function () {
      var token = '';
      if($.cookie('token') != undefined){
      	token = $.cookie('token');
      }
      if(localStorage.getItem('token') != null){
      	token = localStorage.getItem('token');
      }
      getAtmosphereIp(function(ip){
          that.initNotifMessageAthmosphere(PUBLIC_TOPIC, ip);
      });
      getAtmosphereIp(function(ip){
        that.initNotifMessageAthmosphere(token, ip);
      });
      that.getNotifsCache();
    };
    this.getNotifsCacheSuccess = function (json) {
      //console.log("getNotifsSuccess");
      //console.log(json);
      $.each(json, function (key, notifMessage) {
        that.notifyNotif(notifMessage);
      });
    };
    this.getNotifsCache = function () {
      var token = '';
      if($.cookie('token') != undefined){
      	token = $.cookie('token');
      }
      if(localStorage.getItem('token') != null){
      	token = localStorage.getItem('token');
      }
      genericAjaxCall('GET', staticVars["urlBackEndEnterprise"] + 'notif/cache/token?token=' + token + '&langId=' + getIdLangParam(), 'application/json', 'json', '', that.getNotifsCacheSuccess, '');
    };
    this.showNotif = function (idNotif) {
    	var token = '';
      if($.cookie('token') != undefined){
      	token = $.cookie('token');
      }
      if(localStorage.getItem('token') != null){
      	token = localStorage.getItem('token');
      }
      genericAjaxCall('POST', staticVars["urlBackEndEnterprise"] + 'notif/update/token?idNotif=' + idNotif + '&token=' + token, 'application/xml', 'xml', '', '', '');
    };
    this.initNotifMessageAthmosphere = function (topic, ip) {
      var socket = $.atmosphere;
      //var url = "http://"+ip+"/BackEndChat/chat/notif";
      var url = HTML_URL.replace(":ip:", ip) + "chat/notif";
      if (topic != null && topic != "")
        url = url + "/" + topic;
      var request = {
        url: url,
        contentType: "application/json",
        logLevel: 'debug',
        transport: 'websocket',
        fallbackTransport: 'long-polling'
      };

      request.onOpen = function (response) {
        tryToReconnectToNotifMsg--;
        incrementIpNbrUsed(ip, topic, pushToken);
        //console.log("Notification on open, topic : "+topic);
        console.log("********* Connection with topic Opening ***********");
      };
      request.onReconnect = function (request, response) {
        //console.log("Notification Reconnecting");
        console.log("******** Reconnecting **************");
      };
      request.onMessage = function (response) {
        //console.log('onMessage : ' + response.state);
        var messageData = response.responseBody;
        var notifMessage = null;
        try {
          notifMessage = jQuery.parseJSON(messageData);
        } catch (e) {
          console.log('This doesn\'t look like a valid JSON: ', messageData.data);
          return;
        }
        if (notifMessage != null) {
          that.getNotif(notifMessage);
        }
      };

      request.onClose = function (response) {
        console.log('onClose : Sorry, but there\'s some problem with your socket or the server is down state : ' + response.state + ", topic : " + topic);
      };

      request.onError = function (response) {
        console.log('onError : Sorry, but there\'s some problem with your socket or the server is down state : ' + response.state + ", topic : " + topic);
        var state = response.state;
        if (state == "error") {
          console.log("****** Reconnect to Notification Message BUS ******");
          decrementIpNbrUsed(ip, topic, pushToken);
          tryToReconnectToNotifMsg++;
          if (that.tryToReconnectToNotifMsg < MAX_TRY_TO_RECONNECT_TO_NOTIF_MSG) {
            setTimeout(function () {
              that.initNotifMessageAthmosphere(topic, ip);
            }
            , 60000
                    );
          }
        }
      };

      socket.subscribe(request);

      $(window).bind('beforeunload', function(){
        decrementIpNbrUsed(ip, topic, pushToken);
        return "";
      });
    };

    this.getNotif = function (object) {
      if (typeof object != 'undefined') {
        that.notifyNotif(object);
      }
    };

    this.notifyNotif = function (object) {
      //console.log('yes notif');
      $.each(listeners, function (i) {
        listeners[i].notifLoaded(object);
      });
    };

    this.getNotifs = function (xhr, status, error) {
      $.each(listeners, function (i) {
        listeners[i].candidateLoginError(xhr, status, error);
      });
    };
  },
  NotifModelListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  },
});

