jQuery.extend({

	NotificationController : function(model, view) {
		var that=this;
		//listen to the model
		var NotifModelListener = new $.NotifModelListener({
			notifLoaded : function(object) {
               view.notifLoaded(object);
			},
		});
		model.addListener(NotifModelListener);
		
		// listen to the view
		var notifViewList = new $.NotifViewList({
			getNotifyNotif : function(object) {
               model.getNotif(object);
			},
			showNotif : function(idNotif){
				model.showNotif(idNotif);
			}
		});
		view.addListener(notifViewList);
		
		this.getNotif = function(object) {
			model.getNotif(object);			
		};
		
		this.initController=function (){
			view.initView();
			model.initModel();
		};
		that.initController();
	},

});