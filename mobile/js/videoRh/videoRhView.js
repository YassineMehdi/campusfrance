jQuery.extend({
  VideoRhManage: function (video, documentsContainer, view) {
    $galleriesVideoRh.push(video);
    var videoId = video.getAllFilesId();

    var isFavorite = factoryFavoriteExist(video.getAllFilesId(), FavoriteEnum.MEDIA);
    var displayDeleteFavorite = "block";
    var displayAddFavorite = "block";
    if (isFavorite)
      displayAddFavorite = "none";
    else
      displayDeleteFavorite = "none";

    var dom = $('<li>'
            + '<div class="favoris">'
            + '<ul>'
            + '<li><a class="icon-etoile addVideoToFavorite addVideoRhFavoriteId_' + videoId + '" style="display:' + displayAddFavorite + '"></a><a class="icon-delete deleteVideoFromFavorite deleteVideoRhFavoriteId_' + videoId + '" style="display:' + displayDeleteFavorite + '"></a></li>'
            + '<li><a class="icon-galerie openCurrentVideo"></a></li>'
            + '</ul>'
            + '</div>'
            + '<div class="visuel">'
            + '<a class="openCurrentVideo">'
            + '<img src="img/visuel2.jpg" alt="">'
            + '<span>' + video.getTitle() + '</span>'
            + '<img class="logo-company" src="' + video.getEnterpriseLogo() + '" alt="">'
            + '</a>'
            + '</div>'
            + '</li>');

    this.refresh = function () {
      dom.find(".openCurrentVideo").unbind();
      dom.find(".openCurrentVideo").bind('click', function () {
        var videoContainerRh = $($(this).closest('.modal ')).attr('id');
        openVideo(video,$container,$galleryTitleHandler);
      });

      var addVideoFavoriteLink = dom.find(".addVideoToFavorite");
      var deleteVideoFavoriteLink = dom.find(".deleteVideoFromFavorite");

      addVideoFavoriteLink.click(function () {
        if (connectedUser) {
        	
          var idEntity = video.getAllFilesId();
          var componentName = FavoriteEnum.MEDIA;
          var favoriteXml = factoryXMLFavory(idEntity, componentName);
          addFavorite(favoriteXml);
          $(this).hide();
          $(this).closest('div').find('.deleteVideoFromFavorite').show();
          $('.addVideoRHToFavoriteButton_' + videoId).hide();
          $('.deleteVideoRHFromFavoriteButton_' + videoId).show();
        } else {
          getStatusNoConnected();
        }
      });

      deleteVideoFavoriteLink.click(function () {
        var idEntity = video.getAllFilesId();
        var componentName = FavoriteEnum.MEDIA;
        var favoriteXml = factoryXMLFavory(idEntity, componentName);
        deleteFavorite(favoriteXml);
        $(this).hide();
        $(this).closest('div').find('.addVideoToFavorite').show();
        $('.addVideoRHToFavoriteButton_' + videoId).show();
        $('.deleteVideoRHFromFavoriteButton_' + videoId).hide();
      });
      
    },
    this.getDOM = function () { //retourne une ligne HTML <a .....
      return dom;
    },
    this.refresh();
  },
  VideoRhView: function () {
    var listeners = new Array();
    var that = this;
    var videoRHCache = [];

    this.displayVideos = function (listVideos, keyWord) {
      that.addVideoRHDatas(listVideos);
      /*$videosRhContainer.empty();
      for (index in listVideos) {
        var videoContainerList = new $.VideoRhManage(listVideos[index], $videosRhContainer, that);
        var docDom = videoContainerList.getDOM();
        $videosRhContainer.append(docDom);
      }

      that.openVideoByIndex(0);
      setTimeout(function(){
        $('#content_video_rh .scroll-pane').jScrollPane({ autoReinitialise: true });
        $('.modal-loading').hide();
		  }, 1000);*/
    };

    this.initView = function () {
      $galleriesVideoRh = new Array();
      $('.genericContent').hide();
      $('#content_video_rh').show();
      $('#page-content').removeClass('no-padding');
      $videosRhContainer = $('.ul-videos-rh');
      $activitySelectVideoRh = $(".activity-select");
      that.notifyLaunchSearchVideoRh('videoRh', '');
      $(".second-menu li a").removeClass("active");
      $container = $('#content_video_rh').find('.ad-image-wrapper');
      $galleryTitleHandler = $('#content_video_rh').find('.videoTitle');
      that.initListVideoTable();
    };
    
    $("#activity-select-video-rh").change(function () {
      var activity = $("#activity-select option:selected").text();

      if ($("#activity-select").val() != null) {
        if ($("#activity-select").val().indexOf("all") !== -1) {
          activity = "";
        }
      } else {
        activity = "";
      }

      that.notifyVideoRhCriteriaSearch($('#video-rh-key').val(), $('#video-rh-enterprise').val());
    });

    $('#video-rh-key,#video-rh-enterprise').keyup(function () {
      var activity = $("#activity-select-video-rh option:selected").text();

      if ($("#activity-select").val() != null) {
        if ($("#activity-select").val().indexOf("all") !== -1) {
          activity = "";
        }
      } else {
        activity = "";
      }

      that.notifyVideoRhCriteriaSearch($('#video-rh-key').val(), $('#video-rh-enterprise').val());
    });

    this.addVideoRHDatas = function(listVideos){
      videoRHCache = listVideos;
      var videoRhDatas = [];
      $(listVideos).each(function (index, media) {
        var mediaTitle = media.title;
        var enterpriseLogo = media.enterpriseLogo;
        var standId = media.standId;
        var fileType = media.getFileType().getFileTypeName();
        var enterpriseInfo = '<div class="logo-company-wrapper"> <a href="#" class="logo-company"> <img class="" src="' + enterpriseLogo + '" alt="" /> </a> <span class="name-company" >' + htmlEncode(media.getEnterpriseName()) + '</span> </div>';
        var listActions =  "<a id='" + index + "' class='visiter-action viewVideoRH' title='"+VIEWLABEL+"'>"
                + "<img src='img/icons/icon-apercu.png' alt='' /></a>";

        var data = [enterpriseInfo, formattingLongWords(mediaTitle, 30), listActions];
        videoRhDatas.push(data);
      });
      that.addVideoRHDatasList(".conferenceList", videoRhDatas);
      setTimeout(function(){
        $('#content_video_rh .scroll-pane').jScrollPane({ autoReinitialise: true });
        $('.modal-loading').hide();
      }, 1000);
    };

    this.addVideoRHDatasList = function(selector, favoritesData){
      var oTable = $(selector).dataTable();
      oTable.fnClearTable();
      if(favoritesData.length > 0) oTable.fnAddData(favoritesData);
      oTable.fnDraw();
    };

    this.initListVideoTable = function(){
      $('.conferenceList').dataTable({
        "aaData": [],
        "aoColumns": [{
            "sTitle": ENTERPRISELABEL,
            "sWidth": "30%",
          }, {
            "sTitle": TITLELABEL,
            "sWidth": "50%",
          }, {
            "sTitle": ACTIONSLABEL,
            "sWidth": "20%",
          }],
        "bAutoWidth": false,
        "bRetrieve": false,
        "bDestroy": true,
        "bFilter": false,
        "bLengthChange": false,
        "iDisplayLength": 50,
        "language": {
          "url": "dataTable/i18n/"+dataTableLang+".json"
        },
        "fnDrawCallback": function () {
          /*$('.viewFavoriteStandMedia').off();
          $('.viewFavoriteStandMedia')
                  .on("click",
                          function () {
                            that.visitStand($(this).attr('standId'));
                          });
          $('.deleteFavoriteMediaButton').off();
          $('.deleteFavoriteMediaButton')
                  .on('click',
                          function () {
                            var index = $(this).attr('id');
                            var media = videoRHCache[index];
                            factoryDeleteFavorite(this, media.allFilesId, FavoriteEnum.MEDIA);
                          });*/
          $('.viewVideoRH').off();
          $('.viewVideoRH')
                  .on('click',
                          function () {
                            var index = $(this).attr('id');
                            that.openMedia(videoRHCache[index], $("#modal-videoRh-lecteur .document-lecteur"), 
                                $("#modal-videoRh-lecteur .document-lecteur-title"));
                            $('.modal').hide();
                            $("#modal-videoRh-lecteur .document-rh-title").text(videoRHCache[index].title);
                            $("#modal-videoRh-lecteur").modal();
                          });
                          $("thead").find('th:last').removeClass('sorting');
        }
      });
    };

    this.openMedia = function(media, containerHandler, titleHandler){
      if (media != null) {
        var mediaUrl = media.url;
        titleHandler.text(formattingLongWords(media.getTitle(), 30));
        if(media.getFileType().getFileTypeId() != 2 && media.getFileType().getFileTypeId() != 5){
          containerHandler.empty();
          var mediaContainer = "";
          if (mediaUrl.toLowerCase().indexOf(".pdf") !== -1) {
            if (!isIE) {
              mediaContainer = $(
                      '<iframe height="100%" width="100%" scrolling="no" allowfullscreen webkitallowfullscreen src="vplayer/mozilla-pdf.js/web/viewer.html?file='
                      + mediaUrl
                      + '" type="text/html" class="ad-wrapper" style="position: relative; height: 100%; width: 100%;"></iframe>'
                      + '<div id="" class="captionCon" style="overflow-y: auto; overflow-x: hidden;">'
                      + '</div>')
                      .css('position', 'relative');
            } else {
              var filename = getFileName(mediaUrl);
              mediaContainer = $(
                      '<object id="mySWF" style="width:100%;height: 100%;position:relative;" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000">'
                      + '<param name="movie" value="' + staticVars.urlServerStorageSWF + filename + '.swf"  style="position: relative; height: 100%; width: 100%;"/></object>'
                      + '<div id="" class="captionCon" style="overflow-y: auto; overflow-x: hidden;">'
    
                      + '</div>');
            }
          } else {
            mediaContainer = $('<img src="' + mediaUrl + '" class="ad-wrapper2"/>');
          }
          containerHandler.html(mediaContainer);
        }else {
          var videoIdName = "playerVideoFavoris";
          mediaContainer = $('<div id="'+videoIdName+'" class="ad-wrapper"></div>'
              + '<div class="captionCon" style="overflow-y: auto; overflow-x: hidden;"></div>');
          containerHandler.html(mediaContainer);
          playJwPlayer(videoIdName, mediaUrl);
        }
      }
    };

    this.notifyVideoRhCriteriaSearch = function (jobTitle,
            jobEnterpriseName, sectorName) {
      $.each(listeners, function (i) {
        listeners[i].videoRhCriteriaSearch(jobTitle,
                jobEnterpriseName);
      });
    };

    this.openVideoByIndex = function (index) {
      openVideo($galleriesVideoRh[index],$container,$galleryTitleHandler);
    };

    this.notifyLaunchSearchVideoRh = function (optionVal, searchInput) {
      $.each(listeners, function (i) {
        listeners[i].launchSearchVideoRh(optionVal, searchInput);
      });
    };

    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  ViewListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }
});

