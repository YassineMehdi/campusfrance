jQuery.extend({

	FavoriesController : function(model, view) {
		var that=this;
		/**
		 * listen to the view
		 */
		
		var vlist = $.ViewListener({
		});
		
		view.addListener(vlist);

		/**
		 * listen to the model
		 */
		var mlist = $.ModelListener({
			userFavoritesLoaded : function(jobOffersFavorites, contactsFavorites, testimoniesFavorites, 
					advicesFavorites, mediasFavorites, productsFavorites) {
				view.favoritesLoaded(jobOffersFavorites, contactsFavorites, testimoniesFavorites, 
						advicesFavorites, mediasFavorites, productsFavorites);
			},
		});
		
		model.addListener(mlist);
		
		this.initController=function (){
			view.initView();
			model.initModel();
		};
		that.initController();
	},

});