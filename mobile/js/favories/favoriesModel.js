jQuery.extend({

	FavoriesModel : function() {
		var listeners = new Array();
		var that = this;
		
		this.initModel = function () {
			this.getUserFavoritesComplete();
	  	};
		
	    //------------- get All Favorites -----------//
	  	this.getUserFavoritesCompleteSuccess = function(xml) {
			var jobOffersFavorites = [];
			var contactsFavorites = [];
			var testimoniesFavorites = [];
			var advicesFavorites = [];
			var mediasFavorites = [];
			var productsFavorites = [];
	      	$(xml).find('favorite').each(function () {
		        var value = $(this);
		        var componentName = value.find('component').find('componentName').text();
		        if (compareToLowerString(componentName, FavoriteEnum.JOBOFFER) !== -1) {
		          var jobOffer = new $.JobOffer(value.find('jobOffer'));
		          jobOffersFavorites.push(jobOffer);
		        } else if (compareToLowerString(componentName, FavoriteEnum.CONTACT) !== -1) {
		          var contact = new $.FavoriteContact(value.find('contact'));
		          contactsFavorites.push(contact);
		        } else if (compareToLowerString(componentName, FavoriteEnum.TESTIMONY) !== -1) {
		          var testimony = new $.Testimony(value.find('testimony'));
		          testimoniesFavorites.push(testimony);
		        } else if (compareToLowerString(componentName, FavoriteEnum.ADVICE) !== -1) {
		          var advice = new $.Advice(value.find('advice'));
		          advicesFavorites.push(advice);
		        } else if (compareToLowerString(componentName, FavoriteEnum.MEDIA) !== -1) {
		          var allFiles = new $.AllFiles(value.find('allfilles'));
		          mediasFavorites.push(allFiles);
		        } else if (compareToLowerString(componentName, FavoriteEnum.PRODUCT) !== -1) {
		          var product = new $.Product(value.find('product'));
		          productsFavorites.push(product);
		        }
	      	});
			that.notifyUserFavoritesLoaded(jobOffersFavorites, contactsFavorites, testimoniesFavorites, advicesFavorites, mediasFavorites, productsFavorites);
		};

		this.getUserFavoritesCompleteError = function(xhr, status, error) {
			isSessionExpired(xhr.responseText);
		};
		
		this.getUserFavoritesComplete = function() {
			genericAjaxCall('GET', staticVars["urlBackEndCandidate"]+'favorite/userFavoritesComplete?idLang='+getIdLangParam(), 
					'application/xml', 'xml', '', that.getUserFavoritesCompleteSuccess, that.getUserFavoritesCompleteError);
		};
		
		this.notifyUserFavoritesLoaded = function(jobOffersFavorites, contactsFavorites, testimoniesFavorites, 
				advicesFavorites, mediasFavorites, productsFavorites) {
			$.each(listeners, function(i) {
				listeners[i].userFavoritesLoaded(jobOffersFavorites, contactsFavorites, testimoniesFavorites, 
						advicesFavorites, mediasFavorites, productsFavorites);
			});
		};
		
		this.addListener = function(list){
			listeners.push(list);
		};
	},
	
	ModelListener: function(list) {
		if(!list) list = {};
		return $.extend(list);
	}

});