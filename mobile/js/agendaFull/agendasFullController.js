jQuery.extend({
  AgendaFullsController: function (model, view) {
    var that = this;
    var agendaFullModelListener = new $.AgendaFullModelListener({
      agendaFullsSuccess: function (events) {
        view.displayAgendaFulls(events);
      },
      agendaFullsError: function (xhr) {
        view.displayAgendaFullsError(xhr);
      }
    });
    model.addListener(agendaFullModelListener);

    // listen to the view
    var agendaFullViewListener = new $.AgendaFullViewListener({
    });
    view.addListener(agendaFullViewListener);

    this.initController = function () {
      view.initView();
      model.initModel();
    };
    that.initController();
  }

});
