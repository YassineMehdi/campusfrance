jQuery.extend({
  AgendaFullsModel: function () {
    var that = this;
    var listeners = new Array();
    this.initModel = function () {
      that.getAgendaFulls();
    };
//--------  AgendaFulls --------------//
    this.getAgendaFullsSuccess = function (xml) {
    	var events = [];
    	$(xml).find('event').each(function() {
    		events.push(new $.EventAgenda($(this)));
			});
      that.notifyAgendaFullsSuccess(events);
    };

    this.getAgendaFullsError = function (xhr, status, error) {
      that.notifyAgendaFullsError(xhr);
    };
    this.getAgendaFulls = function () {
      genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'agenda?idLang=' + getIdLangParam(),
              'application/xml', 'xml', '', that.getAgendaFullsSuccess,
              that.getAgendaFullsError);
    };
    this.notifyAgendaFullsSuccess = function (events) {
      $.each(listeners, function (i) {
        listeners[i].agendaFullsSuccess(events);
      });
    };
    this.notifyAgendaFullsError = function (xhr) {
      $.each(listeners, function (i) {
        listeners[i].agendaFullsError(xhr);
      });
    };
    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  AgendaFullModelListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }
});
