jQuery.extend({
  ProductDocumentManage: function (document, documentsContainer, view) {
    var dom = $('<li>'
            + '<div class="infos">'
            + '<ul>'
            + '<li>'
            + '<div class="col-md-10 col-sm-10 no-padding">'
            + '<a class="document-name">'
            + htmlEncode(substringCustom(formattingLongWords(document.getResourceName(), 20), 40))
            + '</a>'
            + '</div>'
            + '</li>'
            + '<li><a class="btn btn-info col-md-12 openCurrentDocument viewLabel">'+VIEWLABEL+'</a></li>'
            + '</ul>'
            + '</div>'
            + '</li>');

    this.refresh = function () {
      dom.find(".openCurrentDocument").unbind();
      dom.find(".openCurrentDocument").bind('click', function () {
        galleryDocumentHandler.empty();
        //galleryTitleHandler = document.getResourceName();
        //console.log(galleryTitleHandler);
        switch (document.getResourceType().getResourceTypeId()) {
          case '1':
            openProductDocument(document,galleryDocumentHandler,galleryTitleHandler);
            $('.viewProductScrollStyle').hide();
            $('.viewProductDocumentContainer').show();
          break;
          case '2':
            openProductVideo(document,galleryDocumentHandler,galleryTitleHandler);
            $('.viewProductScrollStyle').hide();
            $('.viewProductDocumentContainer').show();
          break;
          case '3':
            galleryTitleHandler.text(substringCustom(formattingLongWords(document.getResourceName(), 20), 60));
            galleryDocumentHandler.append('<img height="250px" width="450px" alt="update" src="'+document.getResourceUrl()+'">');
            $('.viewProductScrollStyle').hide();
            $('.viewProductDocumentContainer').show();
          break;
        }
        //view.openDocument(document);
      });
    };
    this.getDOM = function () { //retourne une ligne HTML <a .....
      return dom;
    };
    this.refresh();
  },
  VisitorApercuManage: function (visitor, view) {
    var visitorId = visitor.getUserProfile().getUserProfileId();
    var visitorEmail = "";
    var visitorPhone = "";
    var visitorAdress = "";
    var visitorWebsite = "";
    var visitorJobTitle = "";
    var visitorEnterprise = "";
    var enterprise = "";
    var address = "";
    var webSite = "";
    var isFavorite = false;
    var displayAddFavorite = "block";

    if(visitor.type == "Candidate"){
      isFavorite = factoryFavoriteExist(visitorId, FavoriteEnum.VISIT_CARD_CANDIDATE);
      if(visitor.getCountry().getCountryName() != ""){
        address = visitor.getPlace() + ", " + visitor.getCountry().getCountryName() ;
      }
      webSite = visitor.getUserProfile().getWebSite();
      enterprise = visitor.getCurrentCompany();
    }else{
      isFavorite = factoryFavoriteExist(visitorId, FavoriteEnum.VISIT_CARD_ENTERPRISE_P);
      if(visitor.getEnterprise().getAddress() != ""){
        address = visitor.getEnterprise().getAddress();
      }
      classUserType = "exibitor-visit-card";
      enterprise = visitor.getEnterprise().getSiegName();
      webSite = visitor.getEnterprise().getWebsite();
    }

    if (isFavorite)
      displayAddFavorite = "none";
    
    visitorJobTitle = visitor.getJobTitle();
    visitorEmail = $('<div class ="intro-offre-detail"><i class = "fa fa-envelope "></i>'+htmlEncode(visitor.getEmail())+'</div>');
    visitorPhone = $('<div class ="intro-offre-detail"><i class = "fa fa-phone"></i>'+htmlEncode(visitor.getCellPhone())+'</div>');
    visitorWebsite = $('<div class ="intro-offre-detail"><i class = "fa fa-chrome"></i>'+htmlEncode(webSite)+'</div>');
    visitorEnterprise = $('<div class ="intro-offre-detail"><i class = "fa fa-building-o"></i>'+htmlEncode(enterprise)+'</div>');
    visitorAdress = $('<div class ="intro-offre-detail"><i class = "fa fa-home"></i>'+htmlEncode(address)+'</div>');
    if(visitor.getEmail() == "") visitorEmail = $(visitorEmail).hide();
    if(visitor.getCellPhone() == "") visitorPhone = $(visitorPhone).hide();
    if(visitor.getUserProfile().getWebSite() == "") visitorWebsite = $(visitorWebsite).hide();
    if(enterprise == "") visitorEnterprise = $(visitorEnterprise).hide();
    if(address == "") visitorAdress = $(visitorAdress).hide();
    
    var dom = $('<div>'
                  +'<div class="scroll-pane viewVisitorScrollStyle viewVisitorGeneralInfo">'
                    + '<div class="visitorHeader">'
                      + '<div class="logo col-md-12 col-sm-12 no-padding">'
                        + '<img src="' + htmlEncode(visitor.getUserProfile().getAvatar()) + '" alt="">'
                      + '</div>'
                      + '<div class="visitorFullName">' + htmlEncode(visitor.getUserProfile().getFullName()) + '</div>'
                      + '<div class = "intro-offre">'+htmlEncode(visitorJobTitle)+'</div>'
                    + '</div>'
                    + '<div class="visitorDetails">'
                    + getHtml(visitorPhone)
                    + getHtml(visitorEmail)
                    + getHtml(visitorEnterprise)
                    + getHtml(visitorWebsite)
                    + getHtml(visitorAdress)
                    + '</div>'
                + '</div>'
                + '<a class="btn col-md-11 col-sm-11 active addVisitorToFavorite addVisitorToFavoriteButton_' + visitorId + '" style="display:' + displayAddFavorite + '"><i class="glyphicon glyphicon-star" aria-hidden="true"></i>'+EXCHANGE+'</a>'
              + '</div>'
            );
    this.refresh = function () {
      var addVisitorFavoriteLink = dom.find(".addVisitorToFavorite");
      var closeView = dom.find(".close-view");

      addVisitorFavoriteLink.unbind();
      addVisitorFavoriteLink.bind('click',function () {
        if (connectedUser) {
          if(visitor.type == "Candidate"){
            var componentName = FavoriteEnum.VISIT_CARD_CANDIDATE;
          }else{
            var componentName = FavoriteEnum.VISIT_CARD_ENTERPRISE_P;
          }
          var favoriteXml = factoryXMLFavory(visitor.getUserProfile().getUserProfileId(), componentName);
          addFavorite(favoriteXml);
          $('.addVisitorToFavoriteButton_' + visitorId).hide();
        } else {
          getStatusNoConnected();
        }
      });
    };
    this.getDOM = function () { //retourne une ligne HTML <a .....
      return dom;
    };
    this.refresh();
  },
  ProductApercuManage: function (product, view) {
    var productId = product.getProductId();
    var isFavorite = factoryFavoriteExist(productId, FavoriteEnum.PRODUCT);
    var displayDeleteFavorite = "block";
    var displayAddFavorite = "block";
    if (isFavorite)
      displayAddFavorite = "none";
    else
      displayDeleteFavorite = "none";
    var productImage = product.getProductImage();
    if(productImage == "" || productImage == "#" || productImage == undefined) 
    	productImage = product.getStand().getEnterprise().getLogo();
    var dom = $('<div class="scroll-pane viewProductScrollStyle viewProductGeneralInfo">'
                + '<div class="logo col-md-12 col-sm-12 no-padding">'
                  + '<img src="' + productImage + '" alt="">'
                + '</div>'
                + '<h3>' + product.getProductTitle() + '</h3>'
                + '<br>'
                + '<p class = "intro-offre"  id = "productDescription">' + unescape(product.getProductDescription()) + '</p>'
                + '<div class="cel-btn ">'
                  + '<br>'
                  + '<a class="btn col-md-11 col-sm-11 active addProductToFavorite addProductToFavoriteButton_' + productId + '" style="display:' + displayAddFavorite + '"><i class="glyphicon glyphicon-star" aria-hidden="true"></i><span class="productActionLabel">'+ADDTOFAVORITELABEL+'<span></a><a class="btn col-md-11 col-sm-11 active deleteProductFromFavorite deleteProductFromFavoriteButton_' + productId + '" style="background-color: #b3081b;display:' + displayDeleteFavorite + '"><i class="glyphicon glyphicon-star" aria-hidden="true"></i><span class="productActionLabel">' + DELETEFROMFAVORITELABEL + '</span></a>'
                  + '<a href="#" class="btn col-md-11 col-sm-11 blue postuler-job" id="infoRequest_' + productId + '"><i class="fa fa-info-circle" aria-hidden="true"></i><span class="productActionLabel">'+INFOREQUESTLABEL+'</span></a>'
                + '</div>'
            + '</div>'
            + '<div class="scroll-pane viewProductScrollStyle viewProductDocumentContainer" style="display:none;">'
              +'<h3 class="documentTitle-'+productId+'" style="padding: 10px 0px; "></h3>'
              +'<span class="close-view">×</span>'
              +'<div id="galleryDocument">'
                +'<div id="ressourceContainer" class="ad-image-wrapper-'+productId+'">'
                +'</div>'
              +'</div>'
            +'</div>'
            + '<div class="scroll-pane col-md-4 col-sm-4 viewMediaProductScrollStyle" id="content_products">'
              + '<div class="listing ">'
                + '<ul class="ul-docs-'+productId+'"></ul>'
              + '</div>'
            + '</div>');
    this.refresh = function () {
      var addProductFavoriteLink = dom.find(".addProductToFavorite");
      var deleteProductFavoriteLink = dom.find(".deleteProductFromFavorite");
      var sendRequestLink = dom.find("#infoRequest_" + productId);
      var closeView = dom.find(".close-view");

      $('#inputFileProduct').fileupload(getDocumentUpload());

      galleryDocumentHandler = dom.find('.ad-image-wrapper-'+productId);
      documentsContentHandler = dom.find('.ul-docs-'+productId);
      galleryTitleHandler = dom.find(".documentTitle-"+productId);
      var documentProductManage = null;
      var listResources = product.getResources();
      galleriesDocuments = listResources;
      //console.log(listResources.length);

      if(listResources.length == 0){
        documentsContentHandler.append('<p class="empty noDataFound">'+NODOCUMENTSLABEL+'</p>');
      }else{
        for (var index in listResources) {
          documentProductManage = new $.ProductDocumentManage(listResources[index], documentsContentHandler, view);
          documentsContentHandler.append(documentProductManage.getDOM());
        }
        $('#modal-product-apercu .scroll-pane').jScrollPane({ autoReinitialise: true });
      }

      sendRequestLink.click(function () {
        $('#documents-product-list').text("");
        $('#documents-product-list-uploaded').empty();
        $('#modal-send-request-product .file-path').val("");
        $('#modal-send-request-product textarea').val("");
        var productId = product.getProductId();
        if (connectedUser) {
          $('#modal-send-request-product').modal('show');
          //productStandController.getView().notifyGetJobOffersList(productId);
          
          $('#submit-document-product').removeClass('productId_'+productId);
          $('#submit-document-product').addClass('productId_'+productId);
          $('.productId_'+productId).unbind();
          $('.productId_'+productId).bind('click', function () {
            $('.modal-loading').show();
            if ($('#documents-product-list').text().replace(/\s/g, '') != "") {
              var url = $('#documents-product-list').text();
              var messageToSend = $('#modal-send-request-product textarea').val();

              var questionInfo = new $.QuestionInfo();
              questionInfo.setQuestionInfoTitle(product.getProductTitle());
              questionInfo.setQuestion(messageToSend);
              questionInfo.setQuestionInfoUrl(url);
              questionInfo.setProduct(product);

              genericSearchController.getView().notifySendProduction(questionInfo);              
              return false;
            } else {
              swal(SENDERRORLABEL, CHOOSEDOCUMENTLABEL, "error");
              $('.modal-loading').hide();
              return false;
            }
          });
        } else {
          $('.modal-loading').hide();
          getStatusNoConnected();
        }
        analyticsView.viewStandZone(QUESTION_INFORMATION);
      });

      closeView.click(function () {
        $('.viewProductScrollStyle').hide();
        $('.viewProductGeneralInfo').show();
      });

      addProductFavoriteLink.click(function () {
        if (connectedUser) {
          var componentName = FavoriteEnum.PRODUCT;
          var favoriteXml = factoryXMLFavory(product.getProductId(), componentName);
          addFavorite(favoriteXml);

          $('.addProductToFavoriteButton_' + productId).hide();
          $('.deleteProductFromFavoriteButton_' + productId).show();
        } else {
          getStatusNoConnected();
        }
      });
      deleteProductFavoriteLink.click(function () {
        var componentName = FavoriteEnum.PRODUCT;
        var favoriteXml = factoryXMLFavory(productId, componentName);
        deleteFavorite(favoriteXml);
        $('.addProductToFavoriteButton_' + productId).show();
        $('.deleteProductFromFavoriteButton_' + productId).hide();
      });
      analyticsView.viewStandZoneById(PRODUCT+'/'+PRODUCT_ID, productId, product.getStand().getIdStande());
    };
    this.getDOM = function () { //retourne une ligne HTML <a .....
      return dom;
    };
    this.refresh();
  },
  GenericSearchView: function () {
    var listeners = new Array();
    var that = this;
    var listProductsCache = [];
    var listVisitorsCache = [];
    var listVisitorsEnterpriseCache = [];
    var galleriesDocuments = null;
    var galleryDocumentHandler = null;
    var galleryTitleHandler = null;
    this.displayStand = function (listStands, keyWord, sectorText) {
      $('.categList').hide();       
      $('#content').show();
    
    	var listDisplayedStand = [];
      for (index in listStands) {
        var stand = listStands[index];
        var standId = stand.getIdStande();
        var enterpriseInfo = '<div class="logo-company-wrapper"> <a href="#" class="logo-company"> <img class="" src="' + stand.getEnterprise().getLogo() + '" alt="" /> </a> <span class="name-company" >' + stand.getName() + '</span> </div>';
        var sectors = stand.getNameSectors();
        var connected = null;
        var lang = "";
        if (stand.isConnected() == 'yes') {
          connected = '<div class="status-connexion" title="Oui"><i class="glyphicon glyphicon-triangle-top green"></i></div>';
          var languages = stand.getConnectTChatLanguages();
          var languageId = null;
          var languageName = null;
          for(var index in languages){
          	languageId = languages[index];
          	switch (languageId) {
			        case '3':
			        	languageName = FRENCHLABEL;
			        	break;
			        case '1':
			        	languageName = ARABICLABEL;
			        	break;
			        case '5':
			        	languageName = GERMANLABEL;
			        	break;
			        case '4':
			        	languageName = RUSSIANLABEL;
			        	break;
			        case '6':
			        	languageName = SPANISHLABEL;
			        	break;
			        default:
			        	languageName = ENGLISHLABEL;
			        break;
			      }
          	lang += '<img class="language-drapeau" src="img/drapeau.png" alt=""><span class="language-name">'+languageName+'</span>';
          }
        }else {
        	connected = '<div class="status-connexion" title="Non"><i class="glyphicon glyphicon-triangle-bottom red"></i></div>';
        }
        var listActions = "<a id='" + standId + "' href='#' class='visiter-action stands'  standId='" + standId + "' title='"+VISITSTANDLABEL+"'>"
                + "<img src='img/icons/icon-visiter.png' alt='' /></a>";

        //if(getWidth() <= 450 ){
          var data = [enterpriseInfo, connected, listActions];
        /*}else{
          var data = [enterpriseInfo, sectors, connected, lang, listActions];
        }*/
        
        listDisplayedStand.push(data);
      }
      addTableDatas('.table-liste-generic-search-stand', listDisplayedStand);
      $('#content_search_stand .scroll-pane').jScrollPane({ autoReinitialise: true });
      $('.modal-loading').hide();
    };
    this.displayVisitorsCards = function (listVisitors, keyWord) {
      listVisitorsCache = listVisitors;
      var listDisplayedVisitor = [];
      for (index in listVisitors) {
        var visitor = listVisitors[index];
        var visitorId = visitor.getUserProfile().getUserProfileId();
        var visitorFirstName = visitor.getUserProfile().getFirstName();
        var visitorLastName  = visitor.getUserProfile().getSecondName();
        var visitorSectorsName = "";
        var favEnum = "";
        var visitorFunction = null;
        var visitorCurrentCompany = null;
        visitorFunction  = visitor.getJobTitle();
        if(visitor.type == "Candidate"){
          visitorCurrentCompany  = visitor.getCurrentCompany();
          visitorSectorsName = visitor.getNameSectors();
          favEnum = FavoriteEnum.VISIT_CARD_CANDIDATE;
        }else{
          //todo
          //console.log(visitor);
          visitorCurrentCompany  = visitor.getEnterprise().getNom();
          visitorSectorsName = "";
          favEnum = FavoriteEnum.VISIT_CARD_ENTERPRISE_P; 
        }
        var inviteToTchat = "";
        
        //console.log(visitor);
        var enterpriseInfo = '<div class="logo-company-wrapper"> <a href="#" class="logo-company"> <img class="" src="' + visitor.getUserProfile().getAvatar() + '" alt="" /> </a> <span class="name-company" >' + visitorFirstName+" "+ visitorLastName + '</span> </div>';
        var isFavorite = factoryFavoriteExist(visitorId, favEnum);
        
        var displayAddFavorite = "";
        if (isFavorite || candidatGlobal.getUserProfile().getUserProfileId()
        		== visitor.getUserProfile().getUserProfileId()) {
          displayAddFavorite = "none";
        }
        var listActions = inviteToTchat 
                + "<a id='" + index + "' class='visiter-action addFavoriteButton addVisitorToFavoriteButton_" + visitorId + "' style='display:" + displayAddFavorite + ";' title='"+ADDLABEL+"'>"
                + "<img src='img/icons/visitCard.png' alt='' /></a>"
                + "<a id='" + index + "' class='visiter-action viewVisitor' title='"+VIEWLABEL+"'>"
                + "<img src='img/icons/icon-apercu.png' alt='' /></a>";

        var data = null;
        if(getWidth() > 767){
          data = [enterpriseInfo, visitorCurrentCompany, listActions];
        }else{
          data = [enterpriseInfo, listActions];
        }

        listDisplayedVisitor.push(data);
      }
      that.addTableDatas('.table-liste-generic-search-visitCard', listDisplayedVisitor);
      $('#visitCardsList .scroll-pane').jScrollPane({ autoReinitialise: true });
      $('.modal-loading').hide();
    };
    this.displayVisitors = function (listVisitors, keyWord) {
      listVisitorsCache = listVisitors;
      var listDisplayedVisitor = [];
      for (index in listVisitors) {
        var visitor = listVisitors[index];
        var visitorId = visitor.getUserProfile().getUserProfileId();
        var visitorFirstName = visitor.getUserProfile().getFirstName();
        var visitorLastName  = visitor.getUserProfile().getSecondName();
        var visitorSectorsName = "";
        var favEnum = "";
        var visitorFunction = null;
        var visitorCurrentCompany = null;
        visitorFunction  = visitor.getJobTitle();
        if(visitor.type == "Candidate"){
          visitorCurrentCompany  = visitor.getCurrentCompany();
          visitorSectorsName = visitor.getNameSectors();
          favEnum = FavoriteEnum.VISIT_CARD_CANDIDATE;
        }else{
        	//todo
        	//console.log(visitor);
          visitorCurrentCompany  = visitor.getEnterprise().getNom();
          visitorSectorsName = "";
          favEnum = FavoriteEnum.VISIT_CARD_ENTERPRISE_P; 
        }
        var connect = null;
        var inviteToTchat = "";
        if (visitor.isConnected()) {
        		connect = '<div class="status-connexion"><i class="glyphicon glyphicon-triangle-top green"></i></div>';
        		if(visitor.getUserProfile().getCanChatOnPrivate() && 
        				candidatGlobal.getUserProfile().getUserProfileId()
            		!= visitor.getUserProfile().getUserProfile){
            	inviteToTchat = "<a id='" + index + "' class='visiter-action inviteToTChatVisitor' title='"+INVITE_TO_TCHAT_LABEL+"'><img src='img/icons/icon-tchats.png' alt='' /></a>";
            }
        }else connect = '<div class="status-connexion"><i class="glyphicon glyphicon-triangle-bottom red"></i></div>';
        
        //console.log(visitor);
        var enterpriseInfo = '<div class="logo-company-wrapper"> <a href="#" class="logo-company"> <img class="" src="' + visitor.getUserProfile().getAvatar() + '" alt="" /> </a> <span class="name-company" >' + visitorFirstName+" "+ visitorLastName + '</span> </div>';
        var isFavorite = factoryFavoriteExist(visitorId, favEnum);
        
        var displayAddFavorite = "";
        if (isFavorite || candidatGlobal.getUserProfile().getUserProfileId()
        		== visitor.getUserProfile().getUserProfileId()) {
          displayAddFavorite = "none";
        }
        var listActions = inviteToTchat 
        				+ "<a id='" + index + "' class='visiter-action addFavoriteButton addVisitorToFavoriteButton_" + visitorId + "' style='display:" + displayAddFavorite + ";' title='"+ADDLABEL+"'>"
                + "<img src='img/icons/visitCard.png' alt='' /></a>"
                + "<a id='" + index + "' class='visiter-action viewVisitor' title='"+VIEWLABEL+"'>"
                + "<img src='img/icons/icon-apercu.png' alt='' /></a>";
        var data = [enterpriseInfo, connect, listActions];
        listDisplayedVisitor.push(data);
      }
      $('.categList').hide(); 
      $('#content').show();
      that.addTableDatas('.table-liste-generic-search-visitor', listDisplayedVisitor);
      $('#content_search_visitor .scroll-pane').jScrollPane({ autoReinitialise: true });
      $('.modal-loading').hide();
    };
    this.displayProducts = function (listProducts, keyWord) {
      listProductsCache = listProducts;
      var listDisplayedProduct = [];
      var enterpriseNames = [];
      for (index in listProducts) {
        var product = listProducts[index];
        var productId = product.getProductId();
        var productTitle = formattingLongWords(product.getProductTitle(), 30);
        var enterpriseName = product.getStand().getEnterprise().getNom();
        var productDescription = substringCustom(formattingLongWords(product.getProductDescription(), 40),100);
        var enterpriseInfo = '<div class="logo-company-wrapper"> <a href="#" class="logo-company"> <img class="" src="' + product.getStand().getEnterprise().getLogo() + '" alt="" /> </a> <span class="name-company" >' + enterpriseName + '</span> </div>';
        if($.inArray(enterpriseName, enterpriseNames) === -1) 
          enterpriseNames.push(enterpriseName);
        var isFavorite = factoryFavoriteExist(productId, FavoriteEnum.PRODUCT);
        //console.log(product);
        var displayDeleteFavorite = "";
        var displayAddFavorite = "";
        if (isFavorite) {
          displayAddFavorite = "none";
        } else {
          displayDeleteFavorite = "none";
        }
        var listActions = "<a id='" + product.getStand().idstande + "' class='visiter-action stands visitStand' standId='" + product.getStand().idstande + "' title='"+VISITSTANDLABEL+"'>"
                + "<img src='img/icons/icon-visiter.png' alt='' /></a>"
                + "<a id='" + index + "' href='#' class='visiter-action deleteFavoriteButton deleteProductFromFavoriteButton_" + productId + "' style='display:" + displayDeleteFavorite + ";' title='"+DELETEFROMFAVORITELABEL+"'>"
                + "<img src='img/icons/icon-delete.png' alt='' /></a>"

                + "<a id='" + index + "' class='visiter-action addFavoriteButton addProductToFavoriteButton_" + productId + "' style='display:" + displayAddFavorite + ";' title='"+ADDTOFAVORITELABEL+"'>"
                + "<img src='img/icons/icon-favoris.png' alt='' /></a>"

                + "<a id='" + index + "' class='visiter-action viewProduct' title='"+VIEWLABEL+"'>"
                + "<img src='img/icons/icon-apercu.png' alt='' /></a>";
        var data = [enterpriseInfo, productTitle, listActions];
        listDisplayedProduct.push(data);
      }
      addTableDatas('.table-liste-generic-search-product', listDisplayedProduct);
      if(displayProductsFistTime){
        that.genericLoadProductSelect($('#enterpriseProduct'), enterpriseNames, ENTERPRISELABEL); 
        displayProductsFistTime = false;
      }
      $('#content_search_product .scroll-pane').jScrollPane({ autoReinitialise: true });
      $('.modal-loading').hide();
    };
    this.genericLoadProductSelect = function (handler, names, label) {
      var data = [];
      handler.empty();

      for (index in names) {
        var name = names[index];
        data.push({
          value: replaceSpecialChars(name),
          text: replaceSpecialChars(name)
        }); 
      }
      //var selectFieldHandler = handler.closest(".selectFieldRow");
      //selectFieldHandler.empty();
      //selectFieldHandler.append(handler);
      generateOptionSelect(data, handler, label);
      handler.material_select("update");
      handler.parent().parent().find(" > span.caret:first-child").remove();
    };
    this.beforeInitView = function () {
      $('#product-key').keyup(function () {
        that.launchProductCriteriaSearch();
      });
      $('#visitor-card-key').keyup(function () {
      	that.launchVisitorCardCriteriaSearch();
      });    
      $("#visitorCardSelector").change(function () {
        that.launchVisitorCardCriteriaSearch();
      });
      $('#visitor-key').keyup(function () {
        that.launchVisitorCriteriaSearch();
      });
      $("#visitorSelector").change(function () {
      	that.launchVisitorCriteriaSearch();
      });  
      $("#activity-select-stand, #connecter-select").change(function () {
      	that.launchStandCriteriaSearch();
      });
      $('#stand-key').keyup(function () {
      	that.launchStandCriteriaSearch();
      });
      $("#enterpriseProduct, #sectorProduct").change(function () {
        that.launchProductCriteriaSearch();
      });
      $('.allProductsLink').on('click', function () {
        displayProductsFistTime = true;
        $('#contentG').removeClass('salle-exposition');
        $('.genericContent').hide();
        $('#content_search_product').show();
        $('#page-content').removeClass('no-padding');
        $('#product-key').val("");
        that.notifyLaunchSearch('product', '');
        $('#data-table-filters .col-sm-12.col-md-3.no-padding >.caret').attr('style', 'display:none;');
        $('a.icon-format3').addClass('active');                       
        $('.headerTop').hide();
        $('.mainContentAbsolutePage').addClass('staticPosition');   
        $('body').addClass('bodyStyleCustom');                        
        $("#mainPageContent").show();
        $(".icon-tchat,.menu-btn-container .fa-search,.icon-reorder,.fa-bell ,#message-container .icon-envelope").addClass('greenColor');       
        $('.categList').hide();       
        $('#content').show();       
        $('.categList,.backToListStand').hide();                         
        //$(".menu-container").animate({'left': '-400px'},350);
        //$(".menu-container-cover").hide();    
				analyticsView.viewStandZone(PRODUCTS);
      });
      $('.closeListPage').on('click', function () {
        $('#hall1MainContainer').attr('style','background: url(img/halls/hall_asmex_'+getIdLangParam()+'.jpg);background-position: 0px -252px;background-size: 100%;');  
        var idHall = $.cookie('currentHall');
        $('.genericContent').attr('style', 'display: none;');
        $('#contentG').attr('class', '');
        $('#contentG').addClass('salle-exposition');
        $('#page-content').removeClass('no-padding');
        $('#home-page,#info-stand').addClass('hide');        
        $('.categList').show();   
        $('#hall1MainContainer').removeClass('hallCoverCustom');     
        $('#content.page-load').hide();
        //$('#'+idHall).show();        
      });
      $('.backToListStand').on('click', function () {  
        $(".genericContent,.slogo").hide();  
        $('#content_search_stand').show(); 
        $('#hall1MainContainer').attr('style','background: url(img/halls/hall_asmex_'+getIdLangParam()+'.jpg);background-position: 0px -252px;background-size: 100%;');
        window.history.pushState("test", "Title", "/mobile/new-home.html?idLang="+getIdLangParam());
      });
      $('.listStandsLink').on('click', function () {
      	$(".closeListPage").show();
        $('.categList').hide();
        var sectorText = $("#activity-select-stand option[value='"+this.id+"']").text();
        $("#activity-select-stand option:selected").text();
        $('#contentG').removeClass('salle-exposition');
        $('.genericContent').hide();
        $('#content_search_stand').show();
        $('#page-content').removeClass('no-padding');
        $secteurStand = $("#activity-select-stand");
        $connecteStand = $("#connecter-select");
        that.notifyLaunchSearch('stand', "", sectorText);
        if(this.id != "listStandsLink") 
        	$("#content_search_stand #activity-select-stand").val(this.id).material_select("update");
      	$("#activity-select, #activity-select-stand, #visitorSelector, #connecter-select").closest(".form-group").find("div:first-child > span.caret:first-child").remove();
        $('#stand-key').val("");
        //genericSearchController.getView().notifyStandCriteriaSearch('',searchCriteria,'');
        //$('#data-table-filters .col-sm-12.col-md-3.no-padding >.caret').attr('style', 'display:none;');
      });
      $('.goToStandList').on('click', function () {
      	$(".closeListPage").hide();
        $("#activity-select-stand").val('');
        $('.genericContent').hide();
        $('#content_search_stand').show();
        $('#page-content').removeClass('no-padding');
        $secteurStand = $("#activity-select-stand");
        $connecteStand = $("#connecter-select");
        that.notifyLaunchSearch('stand', '');
        $('#connecter-select').material_select();
        $('#data-table-filters .col-sm-12.col-md-3.no-padding >.caret').attr('style', 'display:none;');
        if($('#content_search_stand .col-sm-12.col-md-4.no-padding >.caret')[0] != undefined)
          $('#content_search_stand .col-sm-12.col-md-4.no-padding >.caret')[0].remove()
        $('#content_search_stand .scroll-pane').jScrollPane({ autoReinitialise: true });
      });
      $("#content_search").click(function () {
	      $('#content_search .scroll-pane').jScrollPane({ autoReinitialise: true });
	    });
      that.initProductDataTable();
      that.initStandDataTable();
      that.initVisitorDataTable();
      that.initVisitorCardsDataTable();    
      that.genericLoadProductSelect($('#enterpriseProduct'), [], ENTERPRISELABEL);    
    };

    this.initVisitorCardsDataTable = function(){
      var test = [];
      if(getWidth() > 767){
        test = [{
                'sTitle': FIRSTNAMELABEL +"/"+ LASTNAMELABEL,
                'sWidth': "30%",
              },{
                "sTitle": ENTERPRISELABEL,
                "sWidth": "40%",
              },{
                'sTitle': ACTIONSLABEL,
                'sWidth': "30%",
              }];
      }else{
        test = [{
                'sTitle': FIRSTNAMELABEL +"/"+ LASTNAMELABEL,
                'sWidth': "70%",
              },{
                'sTitle': ACTIONSLABEL,
                'sWidth': "30%",
              }];
      }
      $('.table-liste-generic-search-visitCard').dataTable({
        "aaData": [],
        "aoColumns": test,
        "bAutoWidth": false,
        "bRetrieve": false,
        "bDestroy": true,
        "bFilter": false,
        "bSort": false,
        "bLengthChange": false,
        "iDisplayLength": 10,
        "language": {
          "url": "dataTable/i18n/"+dataTableLang+".json"
        },
        "fnDrawCallback": function () {
          $('.addFavoriteButton, .deleteFavoriteButton, .viewVisitor, .inviteToTChatVisitor').off("click");
          $('.addFavoriteButton').on('click',function () {
            if (connectedUser) {
              var index = $(this).attr('id');
              var visitor = listVisitorsCache[index];
              var favEnum = "";
              if(visitor.type == "Candidate"){
                favEnum = FavoriteEnum.VISIT_CARD_CANDIDATE;
              }else{
                favEnum = FavoriteEnum.VISIT_CARD_ENTERPRISE_P;                  
              }
              var favoriteXml = factoryXMLFavory(visitor.getUserProfile().getUserProfileId(), favEnum);
              addFavorite(favoriteXml);
              $(this).hide();
              //$(this).closest('td').find('.deleteFavoriteButton').show();
            } else {
              getStatusNoConnected();
            }
          });
          $('.viewVisitor').on('click',function () {
            $('.modal-loading').show();
            var index = $(this).attr('id');
            var visitor = listVisitorsCache[index];
            var visitorApercuManage = new $.VisitorApercuManage(visitor, that);
            $('#modal-visitor-apercu #info-visitor-apercu').html('<div id="apercu" class="row no-margin"></div>')
              .find('#apercu').html(visitorApercuManage.getDOM());
            $('#info-stand').removeClass('hide');
            $('#info-stand').show();
            $('#contentG').attr('class', 'info-stand');
            $('#info-stand .menu-stand,#info-stand #vue-aerienne-info-stand').hide();
            $('#modal-visitor-apercu').modal('show');
            //$('#info-produit .scroll-pane').jScrollPane({ autoReinitialise: true });
            $('.modal-loading').hide();
          });
          $('.inviteToTChatVisitor').on('click',function () {
            var index = $(this).attr('id');
            var visitor = listVisitorsCache[index];
            setTimeout(function(){
              $('.icon-tchat').trigger("click");
              connectToPrivateChat(visitor.getUserProfile().getUserProfileId());
            }, 0);
          });
          $("thead").find('th:last')
                  .removeClass('sorting');
        }
      });
    };

    this.initVisitorDataTable = function(){
      $('.table-liste-generic-search-visitor').dataTable({
        "aaData": [],
        "aoColumns": [{
          "sTitle": FIRSTNAMELABEL +"/"+ LASTNAMELABEL,
          "sWidth": "60%",
        },{
          "sTitle": CONNECTEDLABEL,
          "sWidth": "10%",
        }, {
          "sTitle": ACTIONSLABEL,
          "sWidth": "30%",
        }],
        "bAutoWidth": false,
        "bRetrieve": false,
        "bDestroy": true,
        "bFilter": false,
        "bSort": false,
        "bLengthChange": false,
        "iDisplayLength": 10,
        "language": {
          "url": "dataTable/i18n/"+dataTableLang+".json"
        },
        "fnDrawCallback": function () {
          $('.addFavoriteButton, .deleteFavoriteButton, .viewVisitor, .inviteToTChatVisitor').off("click");
          $('.addFavoriteButton').on('click',function () {
            if (connectedUser) {
              var index = $(this).attr('id');
              var visitor = listVisitorsCache[index];
              var favEnum = "";
              if(visitor.type == "Candidate"){
                favEnum = FavoriteEnum.VISIT_CARD_CANDIDATE;
              }else{
                favEnum = FavoriteEnum.VISIT_CARD_ENTERPRISE_P;                  
              }
              var favoriteXml = factoryXMLFavory(visitor.getUserProfile().getUserProfileId(), favEnum);
              addFavorite(favoriteXml);
              $(this).hide();
              //$(this).closest('td').find('.deleteFavoriteButton').show();
            } else {
              getStatusNoConnected();
            }
          });
          $('.viewVisitor').on('click',function () {
            $('.modal-loading').show();
            var index = $(this).attr('id');
            var visitor = listVisitorsCache[index];
            var visitorApercuManage = new $.VisitorApercuManage(visitor, that);
            $('#modal-visitor-apercu #info-visitor-apercu').html('<div id="apercu" class="row no-margin"></div>')
              .find('#apercu').html(visitorApercuManage.getDOM());
            $('#info-stand').removeClass('hide');
            $('#info-stand').show();
            $('#contentG').attr('class', 'info-stand');
            $('#info-stand .menu-stand,#info-stand #vue-aerienne-info-stand').hide();
            $('#modal-visitor-apercu').modal('show');
            //$('#info-produit .scroll-pane').jScrollPane({ autoReinitialise: true });
            $('.modal-loading').hide();
          });
          $('.inviteToTChatVisitor').on('click',function () {
            var index = $(this).attr('id');
            var visitor = listVisitorsCache[index];
            setTimeout(function(){
            	$('.icon-tchat').trigger("click");
            	connectToPrivateChat(visitor.getUserProfile().getUserProfileId());
            }, 0);
          });
          $("thead").find('th:last')
                  .removeClass('sorting');
        }
      });
    };
    this.initProductDataTable = function(){
      $('.table-liste-generic-search-product').dataTable({
        "aaData": [],
        "aoColumns": [{
          "sTitle": ENTERPRISELABEL,
          "sWidth": "30%",
        }, {
          "sTitle": TITLELABEL,
          "sWidth": "30%",
        },{
          "sTitle": ACTIONSLABEL,
          "sWidth": "40%",
        }],
        "bAutoWidth": false,
        "bRetrieve": false,
        "bDestroy": true,
        "bFilter": false,
        "bSort": false,
        "bLengthChange": false,
        "iDisplayLength": 10,
        "language": {
          "url": "dataTable/i18n/"+dataTableLang+".json"
        },
        "fnDrawCallback": function () {
          $('.addFavoriteButton, .deleteFavoriteButton, .viewProduct,.visitStand').off("click");
          $('.visitStand').on("click", function () {
            var standId = $(this).attr('standId');
            //todo
            //standId = 7;
            $('#contentG').attr('class', '').attr('class', 'info-stand stand' + standId);
            $(".main-menu .sub-main-menu, .second-menu li a").removeClass("active");
            $('#page-content').addClass("white-back"); 
            $('.backToListStand').show(); 
            if (standId == 7) {
              //alert('stand7');
              $('a.icon-format3').addClass('active');                       
              $('.headerTop').hide();
              $('.mainContentAbsolutePage').addClass('staticPosition');   
              $('body').addClass('bodyStyleCustom');                        
              $("#mainPageContent").show();
              $(".icon-tchat,.menu-btn-container .fa-search,.icon-reorder,.fa-bell ,#message-container .icon-envelope").addClass('greenColor');        
              $('.categList').hide();       
              $('#content').show();                      
              $(".menu-container").animate({'left': '-400px'},350);
              $(".menu-container-cover").hide();    
            }
            $('#modal-forgot-password').modal('hide');
            $('#modal-forgot-password-Success').modal('hide');
            $('#modal-sign-in').modal('hide');
            $('#page-content').removeClass('no-padding');
            $('#vue-aerienne-info-stand').removeClass('hide');
            $('.genericContent').attr('style', 'display: none;');
            $('#videoHome').hide();
            $('#info-stand').removeClass('hide');
            $('#info-stand').attr('style', 'display: block;');
            $('.menu-stand a,.vue-aerienne-info-stand a').attr('standId', 'standId=' + standId);
            
            $('#hall1MainContainer').attr('style','background: url(img/stand-'+standId+'.jpg);background-position: 0px -168px;background-size: 100%;');
            initStandInfoController(standId);
          });
          $('.addFavoriteButton').on('click',function () {
            if (connectedUser) {
              var index = $(this).attr('id');
              var product = listProductsCache[index];
              var favoriteXml = factoryXMLFavory(product.getProductId(), FavoriteEnum.PRODUCT);
              addFavorite(favoriteXml);
              $(this).hide();
              $(this).closest('td').find('.deleteFavoriteButton').show();
            } else {
              getStatusNoConnected();
            }
          });
          $('.deleteFavoriteButton').on('click',function () {
            if (connectedUser) {
              var index = $(this).attr('id');
              var product = listProductsCache[index];
              var favoriteXml = factoryXMLFavory(product.getProductId(), FavoriteEnum.PRODUCT);
              deleteFavorite(favoriteXml);
              $(this).hide();
              $(this).closest('td').find('.addFavoriteButton').show();
            } else {
              getStatusNoConnected();
            }
          });
          $('.viewProduct').on('click',function () {
            $('.modal-loading').show();
            var index = $(this).attr('id');
            var product = listProductsCache[index];
            var productApercuManage = new $.ProductApercuManage(product, that);
            $('#modal-product-apercu #info-produit-apercu').html('<div id="apercu" class="row no-margin"></div>')
              .find('#apercu').html(productApercuManage.getDOM());
            $('#info-stand').removeClass('hide');
            $('#info-stand').show();
            $('#contentG').attr('class', 'info-stand');
            $('#info-stand .menu-stand,#info-stand #vue-aerienne-info-stand').hide();
            $('#modal-product-apercu').modal('show');
            //$('#info-produit .scroll-pane').jScrollPane({ autoReinitialise: true });
            $('.modal-loading').hide();
          });
          $("thead").find('th:last')
                  .removeClass('sorting');
        }
      });
    };
    this.initStandDataTable = function(){
    	$('.table-liste-generic-search-stand')
      .dataTable(
              {
                "aaData": [],
                "aoColumns": [{
                    "sTitle": ENTERPRISELABEL,
                    "sWidth": "60%",
                  },{
                    "sTitle": CONNECTEDLABEL,
                    "sWidth": "20%",
                  },{
                    "sTitle": ACTIONSLABEL,
                    "sWidth": "20%",
                  }],
                "bAutoWidth": false,
                "bRetrieve": false,
                "bDestroy": true,
                "bFilter": false,
                "bSort": false,
                "bLengthChange": false,
                "iDisplayLength": 10,
                "language": {
                  "url": "dataTable/i18n/"+dataTableLang+".json"
                },
                "fnDrawCallback": function () {
                  $('.stands').off();
                  $('.stands').on("click", function () {
                    var standId = $(this).attr('standId');
                    //todo
                    //standId = 7;
                    $('#contentG').attr('class', '').attr('class', 'info-stand stand' + standId);
                    $(".main-menu .sub-main-menu, .second-menu li a").removeClass("active");
                    $('#page-content').addClass("white-back"); 
                    $('.backToListStand').show(); 
                    if (standId == 7) {
                      //alert('stand7');
                      $('a.icon-format3').addClass('active');                       
                      $('.headerTop').hide();
                      $('.mainContentAbsolutePage').addClass('staticPosition');   
                      $('body').addClass('bodyStyleCustom');                        
                      $("#mainPageContent").show();
                      $(".icon-tchat,.menu-btn-container .fa-search,.icon-reorder,.fa-bell ,#message-container .icon-envelope").addClass('greenColor');        
                      $('.categList').hide();       
                      $('#content').show();       
                      $('.categList,.backToListStand').hide();       
                      $('#content').show();                        
                      $(".menu-container").animate({'left': '-400px'},350);
                      $(".menu-container-cover").hide();    
                    }
                    $('#modal-forgot-password').modal('hide');
                    $('#modal-forgot-password-Success').modal('hide');
                    $('#modal-sign-in').modal('hide');
                    $('#page-content').removeClass('no-padding');
                    $('#vue-aerienne-info-stand').removeClass('hide');
                    $('.genericContent').attr('style', 'display: none;');
                    $('#videoHome').hide();
                    $('#info-stand').removeClass('hide');
                    $('#info-stand').attr('style', 'display: block;');
                    $('.menu-stand a,.vue-aerienne-info-stand a').attr('standId', 'standId=' + standId);
                    
                    $('#hall1MainContainer').attr('style','background: url(img/stand-'+standId+'.jpg);background-position: 0px -168px;background-size: 100%;');
                    initStandInfoController(standId);
                  });
                  $("thead").find('th:last')
                          .removeClass('sorting');
                }
              });
    };
    
    this.initView = function () {
    };
    
    this.reInitView = function () {
    	$("#content_search #activity-select, #content_search_stand #activity-select-stand, " +
    			"#content_search_stand #connecter-select, #content_search_visitor #visitorSelector, #enterpriseProduct, #sectorProduct").val("").material_select("update");
    	$('#visitor-key').val("");
    	$("#activity-select, #activity-select-stand, #visitorSelector, #connecter-select, #enterpriseProduct, #sectorProduct").closest(".form-group").find("div:first-child > span.caret:first-child").remove();
    };
    
    this.launchStandCriteriaSearch = function(){
      var activity = $("#activity-select-stand option:selected").text();
      var connected = $("#connecter-select option:selected").val();
      if ($("#activity-select-stand option:selected").val() == "") {
          activity = "";
      }
      that.notifyStandCriteriaSearch($('#stand-key').val(), activity, connected);
    };
    this.launchVisitorCriteriaSearch = function(){
      var oTable = $(".table-liste-generic-search-visitor").dataTable();
      oTable.fnClearTable();
      var visitorSector = $("#visitorSelector option:selected").text();
      that.notifyVisitorCriteriaSearch($('#visitor-key').val(), visitorSector);
    };
    this.launchVisitorCardCriteriaSearch = function(){
      var oTable = $(".table-liste-generic-search-visitCard").dataTable();
      oTable.fnClearTable();
      var visitorSector = $("#visitorCardSelector option:selected").text();
      that.notifyVisitorCardCriteriaSearch($('#visitor-card-key').val(), visitorSector);
    };
    this.launchProductCriteriaSearch = function(){
      var productKey = $('#product-key').val();
      var enterpriseName = $("#enterpriseProduct option:selected").text();
      var enterpriseNameVal = $("#enterpriseProduct option:selected").val();
      var sectorName = $("#sectorProduct option:selected").text();
      var sectorNameVal = $("#sectorProduct option:selected").val();
      if (enterpriseNameVal == "" || enterpriseNameVal == "all") {
          enterpriseName = "";
      }
      if (sectorNameVal == "" || sectorNameVal == "all") {
          sectorName = "";
      }

      that.notifyProductCriteriaSearch(productKey, enterpriseName, sectorName);
    };
    this.beforeInitView();

    this.notifyProductCriteriaSearch = function (productKey, enterpriseName, sectorName) {
      $.each(listeners, function (i) {
        listeners[i].productCriteriaSearch(productKey, enterpriseName, sectorName);
      });
    };

    this.notifyVisitorCriteriaSearch = function (key, visitorSector) {
      $.each(listeners, function (i) {
        listeners[i].visitorCriteriaSearch(key, visitorSector);
      });
    };

    this.notifyVisitorCardCriteriaSearch = function (key, visitorSector) {
      $.each(listeners, function (i) {
        listeners[i].visitorCardCriteriaSearch(key, visitorSector);
      });
    };

    this.notifyStandCriteriaSearch = function (standKey, activity, connected) {
      $.each(listeners, function (i) {
        listeners[i].standCriteriaSearch(standKey, activity, connected);
      });
    };
    this.notifyNotif = function () {
      $.each(listeners, function (i) {
        listeners[i].getNotifyNotif();
      });
    };  
    this.notifyLaunchSearch = function (optionVal, searchInput, selectVal1) {
    	that.reInitView();
      $.each(listeners, function (i) {
        listeners[i].launchSearch(optionVal, searchInput, selectVal1);
      });
    };

    this.notifySendProduction= function (QuestionInfo) {
      $.each(listeners, function (i) {
        listeners[i].notifySendProduction(QuestionInfo);
      });
    };
    this.addTableDatas = function(selector, datas){
      var oTable = $(selector).dataTable();
      if(datas.length > 0) oTable.fnAddData(datas);
      oTable.fnDraw();
    };
    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  ViewListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }
});



