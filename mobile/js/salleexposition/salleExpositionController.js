jQuery.extend({
  SalleExpositionController: function (model, view) {
    var that = this;
    var salleExpositionModelListener = new $.SalleExpositionModelListener({
      standliste: function () {
        model.standliste();
      }
    });
    model.addListener(salleExpositionModelListener);

    // listen to the view
    var salleExpositionViewListener = new $.SalleExpositionViewListener({
      loadStandliste: function (liststand) {

        view.displayLoadStand(liststand);
      }
    });
    view.addListener(salleExpositionViewListener);

    this.initController = function () {
      view.initView();
      model.initModel();
    };
    that.initController();
  }

});