jQuery.extend({
  MyProfilController: function (model, view) {
    var that = this;
    /**
     * listen to the view
     */

    var vlist = $.ViewListener({
      profileFunctionCandidates: function (functionCandidates) {
        view.loadProfileFunctionCandidates(functionCandidates);
      },
      profileActivitySectors: function (activitySectors) {
        view.loadProfileActivitySectors(activitySectors);
      },
      getCandidateInfoSuccess: function (candidate) {
        view.displayCandidateInfoSuccess(candidate);
      },
      candidateSuccess: function () {
        view.displayCandidateSuccess();
      },
      loadCountries : function(countries) {
        view.displayCountries(countries);
      },
      loadCities : function(cities) {
        view.displayCities(cities);
      },
      loadStates : function(states) {
        view.displayStates(states);
      },
    });

    view.addListener(vlist);

    /**
     * listen to the model
     */
    var mlist = $.ModelListener({
  	    registerCandidate: function (candidate) {
        	model.registerCandidate(candidate);
      	},
        getStatesByCountryId : function(countryId){
          model.fillStates(countryId);
        },
        getCitiesByStateId : function(stateId){
          model.fillCities(stateId);
        },
    });

    model.addListener(mlist);

    this.initController = function () {
      view.initView();
      model.initModel();
    };

  },
});
