jQuery.extend({
  InboxModel: function () {
		var listeners = new Array();
		var that = this;
		
		this.initModel = function () {
		  this.getReceivedConversations();
		};
	
		//------------- Get Inbox List Messages -----------//	
		this.getReceivedConversationsSuccess = function(xml){
			var listReceivedConversations = [];
			$(xml).find('conversations').each(function(){
				var conversation=new $.Conversation($(this));
				listReceivedConversations[conversation.idConversation]=conversation;
			});
			that.notifyReceivedConversationsLoaded(listReceivedConversations);
		};
		this.getReceivedConversationsError = function(xhr,status,error){
			isSessionExpired(xhr.responseText);
		};
		
		this.getReceivedConversations = function(){
			genericAjaxCall('GET', staticVars["urlBackEndCandidate"]+'conversation/received', 'application/xml', 
					'xml', '', that.getReceivedConversationsSuccess, that.getReceivedConversationsError);
		};
		
		this.notifyReceivedConversationsLoaded = function(listReceivedConversations){
			$.each(listeners, function(i) {
				listeners[i].receivedConversationsLoaded(listReceivedConversations);
			});
		};
		
	  //------------- Set Message Inbox -----------//
		this.messageReplySuccess = function(xml){
			that.notifyMessageReplySuccess();
		};
		this.messageReplyError = function(xhr,status,error){
			isSessionExpired(xhr.responseText);
		};
		this.messageReply = function(conversationId, xmlReply){
		genericAjaxCall('POST', staticVars["urlBackEndCandidate"]+'conversation/reply?conversationId='+conversationId, 'application/xml', 
				'xml', xmlReply, that.messageReplySuccess, that.messageReplyError);
		};
	
	  this.notifyLoadCandidatePersonalInfos = function (candidate) {
	    $.each(listeners, function (i) {
	      listeners[i].loadCandidateAccount(candidate);
	    });
	  };

	  this.notifyMessageReplySuccess = function () {
	    $.each(listeners, function (i) {
	      listeners[i].messageReplySuccess();
	    });
	  };
	  this.addListener = function (list) {
	    listeners.push(list);
	  };
  },
  ModelListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }

});