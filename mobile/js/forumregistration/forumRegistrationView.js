jQuery
    .extend({
        ForumRegistrationView: function() {
            var that = this;
            var listeners = new Array();
            this.validator = '';
            this.inputAvatarSelector = '';
            this.SecteurActuel = '';
            this.Country = '';
            this.State = '';
            this.City = '';
            this.languageSelector = '';
            this.submitSelector = '';
            this.emailHandler = null;
            this.token = null;
            this.stateId = null;
            this.cityId = null;
            // ----- init view ------------//
            this.initView = function() {
                switch (getIdLangParam()) {
                    case '3':
                        $("#langDrop").append('<span class="">' + FRENCHLABEL + '</span><span class="caret"></span>');
                        break;
                    case '1':
                        $('footer .content li a').addClass('styleCustomArabic');
                        $('body').addClass('customArabicStyle');
                        $('footer').attr('dir', 'rtl');
                        $("#langDrop").append('<span class="">' + ARABICLABEL + '</span><span class="caret"></span>');
                        break;
                    case '5':
                        $("#langDrop").append('<span class="">' + GERMANLABEL + '</span><span class="caret"></span>');
                        break;
                    case '4':
                        $("#langDrop").append('<span class="">' + RUSSIANLABEL + '</span><span class="caret"></span>');
                        break;
                    case '6':
                        $("#langDrop").append('<span class="">' + SPANISHLABEL + '</span><span class="caret"></span>');
                        break;
                    default:
                        $("#langDrop").append('<span class="">' + ENGLISHLABEL + '</span><span class="caret"></span>');
                        break;
                }
                this.inputAvatarSelector = ".file-avatar";
                this.submitSelector = '#envoyer';
                that.emailHandler = $('#email');
                this.SecteurActuel = '#secteur-actuelle';
                this.City = '#city';
                this.Country = '#country';
                this.State = '#state';
                this.defaultLanguageSelector = '#defaultLanguage';
                this.descriptionSelector = '#description';
                this.candidateFunctionSelector = '#candidateFunction';
                that.Avatar(this.inputAvatarSelector);
                that.actionSubmitHandler(this.submitSelector);
                that.emailHandler.unbind('blur').bind('blur', function(e) {
                    that.notifyCheckEmail(that.emailHandler.val().trim());
                });

                $('#homePage').on('click', function() {
                    location.replace("http://" + HOSTNAME_URL + "/visitor/new-home.html?idLang=" + getIdLangParam());
                });
                $('.inscription').show();
                if (defaultBrowser)
                    $("select:not([multiple='multiple'])").addClass("browser-default");
                else
                    $('select').attr('style', 'display:none');
                generateOption([], that.Country);
                that.Select(that.Country);
                /*generateOption([], that.City);
                that.Select(that.City);
                generateOption([], that.State);
                that.Select(that.State);
                $(that.Country).unbind('change').bind('change', function(e) {
                	that.notifyStatesByCountryId($(this).val());
                });
                $(that.State).unbind('change').bind('change', function(e) {
                	that.notifyCitiesByStateId($(this).val());
                });*/
            };
            this.MultiSelect = function(Selector) {
                $(Selector).multiselect({
                    noneSelectedText: SELECTLABEL,
                    selectedList: 3,
                    header: false,
                    selectedText: '# ' + SELECTED_LABEL,
                });
            };
            this.Select = function(SelectSelector) {
                $(SelectSelector).material_select();
                // that.scrollbarMousedown(SelectSelector);
            };
            this.actionSubmitHandler = function(submitSelector) {
                $(submitSelector).on('click', function() {
                    that.validateRegisterForm();
                    $('select').attr('style', 'display:block');
                    $("#g-recaptcha-response").show();
                    var $valid = $("#inscription").valid();
                    if (!$valid) {
                        if (defaultBrowser)
                            $("select[multiple='multiple']").attr('style', 'display:none');
                        else
                            $('select').attr('style', 'display:none');
                        $("#g-recaptcha-response").hide();
                        return false;
                    }
                    $("#g-recaptcha-response").hide();
                    $('select').attr('style', 'display:none');
                    $("body").addClass("loading");
                    var candidate = new $.Candidate();
                    var userProfile = new $.UserProfile();
                    var functionCandidate = new $.FunctionCandidate();
                    /*********************** Liste Object ***************/
                    var secteurActs = [];
                    /************************** END ********************/
                    // ---- Set token
                    if (that.token != null)
                        candidate.setToken(that.token);
                    // ---- Set password
                    candidate.setPassword($.sha1($('#password').val().trim()));
                    // ---- Set passwordClear
                    candidate.setPasswordClear($('#password').val().trim());
                    // ---- Set Phone Number
                    candidate.setCellPhone($('#phonePortable').val());
                    // ---- Set Current Company
                    candidate.setCurrentCompany($('#currentCompany').val());
                    // ---- Set Email
                    candidate.setEmail($('#email').val().trim());
                    // ---- Set JobTitle
                    candidate.setJobTitle($('#jobTitle').val().trim());
                    // ---- Set Description
                    candidate.setDescription($('#description').val().trim());
                    // ---- Set Place
                    candidate.setPlace($('#place').val().trim());
                    /** ************ User profile ******************** */
                    // ---- Set FirstName
                    userProfile.setFirstName($('#firstName').val());
                    // ---- Set SecondName
                    userProfile.setSecondName($('#secondName').val());
                    // ---- Set Avatar
                    userProfile.setAvatar($('#avatar').attr('src'));
                    // ---- Set Web site
                    userProfile.setWebSite($('#website').val());
                    // ---- Set Profile to candidate
                    candidate.setUserProfile(userProfile);
                    /** *********** END ****************************** */
                    /** *********** Secteur Acts ******************** */
                    if ($(that.SecteurActuel).val()) {
                        var secteurAct = null;
                        var selectSecteur = $(that.SecteurActuel).val();
                        for (index in selectSecteur) {
                            secteurAct = new $.SecteurAct();
                            secteurAct.setIdsecteur(selectSecteur[index]);
                            secteurActs.push(secteurAct);
                        }
                        // ---- Set Secteur Acts to candidate
                        candidate.setSecteurActs(secteurActs);
                    }
                    /** *********** END ****************************** */
                    /** *********** City ******************** */
                    /*var city = new $.City();
                    city.setCityId($('#city').val());
                    // ---- Set City to candidate
                    candidate.setCity(city);*/
                    /** *********** END ****************************** */
                    /** *********** Country ******************** */
                    var country = new $.Country();
                    country.setCountryId($('#country').val());
                    // ---- Set Country to candidate
                    candidate.setCountry(country);
                    /** *********** END ****************************** */
                    /** *********** City ******************** */
                    var language = new $.Language();
                    language.setLanguageId($('#defaultLanguage').val());
                    // ---- Set Language to candidate
                    userProfile.setDefaultLanguage(language);
                    /** *********** END ****************************** */
                    // console.log(candidate);
                    /************** Function Candidate *********************/
                    //---- Set Id Function
                    functionCandidate.setIdFunction($(that.candidateFunctionSelector).val());
                    //---- Set FunctionCandidate to candidate
                    candidate.setFunctionCandidate(functionCandidate);
                    /** *********** END ****************************** */
                    //that.notifyRegisterCandidate(candidate);
                    that.notifyVerifyCaptchaRegister(candidate, $("#g-recaptcha-response").val());
                });
            };
            this.validateRegisterForm = function() {
                $.validator.addMethod("noSpace", function(value, element) {
                    return value.indexOf(" ") < 0 && value != "";
                }, THANK_YOU_REMOVE_SPACES_LABEL);
                $.validator.addMethod("datevalid", function(value, element) {
                    if (value.length > 0) {
                        return isValidDate(value, '/');
                    } else {
                        return true;
                    }
                }, THANK_YOU_RESPECT_INDICATED_FORMAT_LABEL);
                $.validator
                    .addMethod(
                        "phoneNumbre",
                        function(phone_number, element) {
                            phone_number = phone_number.replace(/\s+/g, "");
                            return this.optional(element) ||
                                phone_number.length > 9 &&
                                (phone_number
                                    .match(/\(?\+([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/) || phone_number
                                    .match(/\(?(00[0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/));
                        }, THANK_YOU_RESPECT_INDICATED_FORMAT_LABEL);
                $("#inscription").validate({
                    errorPlacement: function errorPlacement(error, element) {
                        element.before(error);
                    },
                    rules: {
                        firstName: {
                            required: true
                        },
                        secondName: {
                            required: true
                        },
                        email: {
                            required: true,
                            email: true,
                            minlength: 3,
                            noSpace: true
                        },
                        password: {
                            required: true,
                            minlength: 5,
                            noSpace: true
                        },
                        confirmPassword: {
                            required: true,
                            minlength: 5,
                            equalTo: "#password",
                            noSpace: true
                        },
                        phonePortable: {
                            required: true,
                            minlength: 10
                        },
                        currentCompany: {
                            required: true
                        },
                        jobTitle: {
                            required: true
                        },
                        country: {
                            required: true
                        },
                        state: {
                            required: true
                        },
                        city: {
                            required: true
                        },
                        secteur_actuelle: {
                            required: true
                        },
                        defaultLanguage: {
                            required: true
                        },
                        "g-recaptcha-response": {
                            required: true
                        },
                        place: {
                            required: true
                        },
                        candidateFunction: {
                            required: true
                        },
                    }
                });
            };
            /* steps form */
            this.Avatar = function(inputAvatarSelector) {
                /* file input */
                $(inputAvatarSelector).fileupload(that.getCaptureUpload());
            };
            // -------- Activity Sectors ------------//
            this.displayActivitySectors = function(activitySectors) {
                var data = [];
                for (var i in activitySectors) {
                    var activitySector = activitySectors[i];
                    var secteurId = activitySector.idsecteur;
                    var secteurName = activitySector.name;
                    data.push({
                        value: replaceSpecialChars(secteurId),
                        text: replaceSpecialChars(secteurName)
                    });
                }
                generateOption(data, that.SecteurActuel);
                that.MultiSelect(that.SecteurActuel);
                $('.ui-multiselect-checkboxes li span').click(function() {
                    if ($(this).hasClass('active')) {
                        $(this).removeClass('active');
                    } else {
                        $(this).addClass('active');
                    }
                });
            };
            //-------- Display Function Candidate ------------//
            this.displayFunctionCandidates = function(functionCandidates) {
                var data = [];
                for (var i in functionCandidates) {
                    var functioncandidate = functionCandidates[i];
                    var functioncandidateId = functioncandidate.idFunction;
                    var functionName = functioncandidate.functionName;
                    data.push({
                        value: replaceSpecialChars(functioncandidateId),
                        text: replaceSpecialChars(functionName)
                    });
                }
                generateOption(data, that.candidateFunctionSelector);
                that.Select(that.candidateFunctionSelector);
            };
            // -------- Display Countries ------------//
            this.displayCountries = function(countries) {
                var data = [];
                for (var i in countries) {
                    var country = countries[i];
                    data.push({
                        value: replaceSpecialChars(country.getCountryId()),
                        text: replaceSpecialChars(country.getCountryName())
                    });
                }
                generateOption(data, that.Country);
                that.Select(that.Country);
            };
            //-------- Display States ------------//
            this.displayStates = function(states) {
                var data = [];
                for (var i in states) {
                    var state = states[i];
                    data.push({
                        value: replaceSpecialChars(state.getStateId()),
                        text: replaceSpecialChars(state.getStateName()),
                    });
                }
                generateOption(data, that.State);
                that.Select(that.State);
                if (that.stateId != undefined)
                    $(that.State).val(that.stateId).material_select('update');
            };
            //-------- Display Cities ------------//
            this.displayCities = function(cities) {
                var data = [];
                for (var i in cities) {
                    var city = cities[i];
                    data.push({
                        value: replaceSpecialChars(city.getCityId()),
                        text: replaceSpecialChars(city.getCityName()),
                    });
                }
                generateOption(data, that.City);
                that.Select(that.City);
                if (that.cityId != undefined)
                    $(that.City).val(that.cityId).material_select('update');
            };
            // -------- Display Candidate Success Save ------------//
            //-------- Display Languages ------------//
            this.displayLanguages = function(languages) {
                var data = [];
                for (var i in languages) {
                    var language = languages[i];
                    data.push({
                        value: replaceSpecialChars(language.getLanguageId()),
                        text: replaceSpecialChars(language.getLanguageName()),
                    });
                }
                generateOption(data, that.defaultLanguageSelector);
                that.Select(that.defaultLanguageSelector);
            };
            // -------- Display Candidate Success Save ------------//
            this.displayCandidateSuccess = function() {
                $("body").removeClass("loading");
                $("#inscription").remove();
                var msg = null;
                if (that.token == null)
                    msg = CREATEACCOUNTSUCCESSLABEL;
                else
                    msg = UPDATEACCOUNTSUCCESSLABEL;
                $('.bannier')
                    .after(
                        '<div class="inscription"><h2>' +
                        msg +
                        '<br/>' + EACCETEAMLABEL + '</h2><br/><br/><br/><br/></div>');
                setTimeout(function() {
                    window.location.replace("new-home.html?idLang=" + getIdLangParam());
                }, 5000);
            };
            // -------- Display Candidate Error Save ------------//
            this.displayCandidateError = function(xhr) {
                $("body").removeClass("loading");
                if (xhr.status === 409) {
                    swal(SAVEMESSAGELABEL, ALREADYEXISTEMAILLABEL, "error");
                } else {
                    swal(SAVEMESSAGELABEL,
                        SAVEERRORLABEL, "error");
                }
                // $('.previous a').trigger('click');
            };
            // ------------- File Uploade IMG-------------//
            this.getCaptureUpload = function() {
                var timeUpload = null;
                return {
                    beforeSend: function(jqXHR, settings) {},
                    datatype: 'json',
                    cache: false,
                    add: function(e, data) {
                        $("body").addClass("loading");
                        var goUpload = true;
                        var uploadFile = data.files[0];
                        var fileName = uploadFile.name;
                        timeUpload = (new Date()).getTime();
                        data.url = getUrlPostUploadFile(timeUpload, fileName);
                        if (!regexImg.test(fileName)) {
                            swal(DOWNERRORLABEL, DOWNEXTENSIONERRORLABEL,
                                "error");
                            $("body").removeClass("loading");
                            goUpload = false;
                        }

                        if (goUpload == true) {
                            if (uploadFile.size > MAXFILESIZE) { // 6mb
                                swal(DOWNERRORLABEL,
                                    FILE_SIZE_NOT_ALLOWED_LABEL, "error");
                                $("body").removeClass("loading");
                                goUpload = false;
                            }
                        }
                        if (goUpload == true) {
                            jqXHRResource = data.submit();
                        }
                    },
                    progressall: function(e, data) {},
                    done: function(e, data) {
                        var file = data.files[0];
                        var fileName = file.name;
                        var hashFileName = getUploadFileName(timeUpload, fileName);
                        console.log("Add file done, filename : " + fileName +
                            ", hashFileName : " + hashFileName);
                        var urlCache = getUploadFileUrl(hashFileName);
                        $("#avatar").attr("src", urlCache);
                        $("body").removeClass("loading");
                        swal(DOWNLOADLABEL, DOWNLOADSUCCESSLABEL, "success");
                    }
                };
            };

            this.displayCheckEmailSuccess = function(exist) {
                // text: 'Votre adresse e-mail exist déjà!'+'<br/><br/><div> <div
                // class="col-md-5 no-padding"> <label for="message-text"
                // class="control-label">Mot de passe:</label> </div> <div
                // class="col-md-7 no-padding"> <input type="password"
                // class="form-control" id="recipient_password"
                // placeholder="**********" style=" "> </div> </div>',
                if (exist == 'true') {
                    swal({
                        html: true,
                        title: ALREADYREGISTREDLABEL,
                        text: EMAILEXISTTAPPASSLABEL,
                        type: "input",
                        showCancelButton: true,
                        closeOnConfirm: false,
                        animation: "slide-from-top",
                        cancelButtonText: CANCELLABEL,
                        inputPlaceholder: "**********"
                    }, function(inputValue) {
                        if (inputValue === false)
                            return false;
                        // if (inputValue != undefined) inputValue =
                        // inputValue.trim();
                        if (inputValue === "") {
                            swal.showInputError(REQUIREDLABEL);
                            return false;
                        }
                        // swal("Nice!", "You wrote: " + inputValue, "success");
                        that.notifyGetCandidateInfo($('#email').val().trim(),
                            inputValue);
                        swal({
                            title: PLEASWAITLABEL,
                            text: "",
                            timer: 1500,
                            showConfirmButton: false
                        });
                    });
                    $(".sweet-alert.show-input").find("input").attr("type", "password");
                }
            };
            // -------- Check Email Error ------------//
            this.checkEmailError = function(xhr) {
                $("body").removeClass("loading");
                if (xhr.status === 412) {
                    swal("", ACCOUNT_NOT_YET_APPROVED_LABEL, "error");
                }
            };
            // -------- Display Candidate Info Success ------------//
            this.displayCandidateInfoSuccess = function(candidate) {
                that.token = candidate.getToken();
                var userProfile = candidate.getUserProfile();
                // ---- Get Phone Number
                $('#phone').val(candidate.getPhoneNumber());
                // ---- Get Phone Number
                $('#phonePortable').val(candidate.getCellPhone());
                // ---- Get Current Company
                $('#currentCompany').val(candidate.getCurrentCompany());
                /** ************ User profile ******************** */
                // ---- Get FirstName
                $('#firstName').val(userProfile.getFirstName());
                // ---- Get SecondName
                $('#secondName').val(userProfile.getSecondName());
                // ---- Get JobTitle
                $('#jobTitle').val(candidate.getJobTitle());
                // ---- Get Description
                $('#description').val(candidate.getDescription());
                // ---- Get Place
                $('#place').val(candidate.getPlace());
                // ---- Get WebSite
                $('#website').val(userProfile.getWebSite());
                // ---- Get Avatar
                if (userProfile.getAvatar() != "")
                    $('#avatar').attr('src', userProfile.getAvatar());
                // ---- Get Pseudo Skype
                $('#password, #confirmPassword').val(candidate.getPasswordClear());

                var secteurActs = candidate.getSecteurActs();
                if (secteurActs != null) {
                    var secteurActIds = [];
                    for (var i = 0; i < secteurActs.length; i++) {
                        var secteurAct = secteurActs[i];
                        secteurActIds.push(secteurAct.getIdsecteur());
                    }
                    $(that.SecteurActuel).val(secteurActIds).multiselect("refresh");
                }
                /*if(candidate.getCity() != null) {
                	if(candidate.getCity().getState() != null) {
                		that.stateId = candidate.getCity().getState().getStateId();
                		if(candidate.getCity().getState().getCountry() != null) {
                			that.cityId = candidate.getCity().getCityId();
                			$(that.Country).val(candidate.getCity().getState().getCountry().getCountryId()).material_select('update');
                		}
                	}
                }*/
                if (userProfile.getDefaultLanguage() != null && userProfile.getDefaultLanguage().getLanguageId() != '')
                    $(that.defaultLanguageSelector).val(userProfile.getDefaultLanguage().getLanguageId()).material_select('update');
                if (candidate.getCountry() != null && candidate.getCountry().getCountryId() != '') {
                    $(that.Country).val(candidate.getCountry().getCountryId()).material_select('update');
                }
                var functionCandidate = candidate.getFunctionCandidate();
                if (functionCandidate != null) {
                    $(candidateFunction).val(functionCandidate.getIdFunction()).material_select('update');
                }
                //that.notifyStatesByCountryId($(that.Country).val());
                //that.notifyCitiesByStateId(that.stateId);
                $(".tab-content .input-field > span.caret").remove();
                /** *********** END ****************************** */
            };
            this.displayCandidateInfoError = function(xhr) {
                if (xhr.status == 404) {
                    swal(INCORRECTPASSLABEL, "", "error");
                    $("#email").focus();
                }
            };
            this.notifyRegisterCandidate = function(candidate) {
                $.each(listeners, function(i) {
                    listeners[i].registerCandidate(candidate);
                });
            };
            this.notifyCheckEmail = function(email) {
                $.each(listeners, function(i) {
                    listeners[i].checkEmail(email);
                });
            };
            this.notifyGetCandidateInfo = function(email, pwd) {
                $.each(listeners, function(i) {
                    listeners[i].getCandidateInfo(email, pwd);
                });
            };
            this.notifyStatesByCountryId = function(countryId) {
                $.each(listeners, function(i) {
                    listeners[i].getStatesByCountryId(countryId);
                });
            };
            this.notifyCitiesByStateId = function(stateId) {
                $.each(listeners, function(i) {
                    listeners[i].getCitiesByStateId(stateId);
                });
            };
            this.notifyVerifyCaptchaRegister = function(candidate, response) {
                $.each(listeners, function(i) {
                    listeners[i].verifyCaptchaRegister(candidate, response);
                });
            };
            this.addListener = function(list) {
                listeners.push(list);
            };
        },
        ForumRegistrationViewListener: function(list) {
            if (!list)
                list = {};
            return $.extend(list);
        }
    });