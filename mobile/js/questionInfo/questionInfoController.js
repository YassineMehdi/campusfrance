jQuery.extend({

	QuestionInfoController: function(model, view){
		//var that = this;
		/**
		 * listen to the view
		 */
		var vlist = $.QuestionInfoViewListener({
			loadQuestionInfos : function(questionInfos){
				view.displayQuestionInfos(questionInfos);
			},
		});
		view.addListener(vlist);

		/**
		 * listen to the model
		 */
		var mlist = $.QuestionInfoModelListener({		
			getQuestionInfos : function(){
				model.getQuestionInfos();
			},
		});
		
		model.addListener(mlist);
		
		this.initController = function(){
			view.initView();
			model.initModel();
		};
	}

});