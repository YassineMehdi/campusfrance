
jQuery.extend({
	
	QuestionInfoModel: function(){
		var that = this;
		var listeners = new Array();
		
		this.initModel = function(){
			that.getQuestionInfos();
		};
		
		this.getQuestionInfosSuccess = function(data){
			var questionInfos = new Array();
			var questionInfo = null;
			$(data).find("questionInfoDTO").each(function() {
				questionInfo = new $.QuestionInfo($(this));
				questionInfos[questionInfo.getQuestionInfoId()] = questionInfo;
			});
			that.notifyLoadQuestionInfos(questionInfos);
		};
		
		this.getQuestionInfosError = function(xhr, status, error){
		};
		
		this.getQuestionInfos = function(){
			genericAjaxCall('GET', staticVars["urlBackEndCandidate"]+'questionInfo/get?idLang='+getIdLangParam(), 'application/xml', 'xml', 
					'', that.getQuestionInfosSuccess, that.getQuestionInfosError);
		};
		
		this.addListener = function(list){
			listeners.push(list);
		};

		this.notifyLoadQuestionInfos = function(questionInfos) {
			$.each(listeners, function(i) {
				listeners[i].loadQuestionInfos(questionInfos);
			});
		};
		
	},
		
	QuestionInfoModelListener: function(list) {
		if(!list) list = {};
		return $.extend(list);
	},
	
});

