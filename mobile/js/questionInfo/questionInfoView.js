
jQuery.extend({
	
	ReponseInfoManage: function (questionInfo, view) {
  	var dom = $('');
  	
    this.refresh = function () {
    	var questionInfoDate = questionInfo.getQuestionInfoDate().toLocaleDateString(LANGAUGETAGLOCALELABEL.replace('_', '-'));
    	dom = $('<li>'
		          +'<div class="date-event">'
		              +'<div class="logo-event"><img src="'+questionInfo.getProduct().getStand().getEnterprise().getLogo()+'" alt=""></div>'
		              +'<span>'+htmlEncode(questionInfo.getProduct().getStand().getName())+'<br>'+htmlEncode(questionInfo.getProduct().getProductTitle())+'</span>'
		              +'<span class="date">'+htmlEncode(questionInfoDate)+'</span>'
		          +'</div>'
		          +'<div class="intro">'
		              +'<div class="intro-paragraphe">'
		              + htmlEncode(questionInfo.getResponse())
		              +'</div>'
		          +'</div>'
		      +'</li>');
    },
    
    this.getDOM = function () { //retourne une ligne HTML <a .....
      return dom;
    },
    
    this.refresh();
  },
  
  QuestionInfoManage: function (questionInfo, view) {
    var dom = $('');
    
    this.refresh = function () {
    	var questionInfoDate = questionInfo.getQuestionInfoDate().toLocaleDateString(LANGAUGETAGLOCALELABEL.replace('_', '-'));
    	dom = $('<li>'
          +'<div class="logo-event"><img src="'+questionInfo.getProduct().getStand().getEnterprise().getLogo()+'" alt=""></div>'
          +'<span>'+htmlEncode(questionInfo.getProduct().getStand().getName())+'<br>'+htmlEncode(questionInfo.getProduct().getProductTitle())+'</span>'
          +'<span class="date-event">'+htmlEncode(questionInfoDate)+'</span>'
      +'</li>');
    },
    
    this.getDOM = function () { //retourne une ligne HTML <a .....
      return dom;
    },
    
    this.refresh();
  },	
  
	QuestionInfoView: function(){
    var listeners = new Array();
    var that = this;
    $questionInfoListContainer = null;
		$responseQuestionInfoListContainer = null;

    this.initView = function () {
    	$('.modal-loading').show();
      $('.genericContent').hide();
      $('#candidateQuestionInfos').show();
  		$('#page-content').removeClass('no-padding');
  		$questionInfoListContainer = $('.questionInfoList');
  		$responseQuestionInfoListContainer = $('.responseQuestionInfoList');
  		$questionInfoListContainer.empty();
  		$responseQuestionInfoListContainer.empty();
      if(toQuestionInfoResponse){
				$('#answersQuestionInfosLink').trigger('click');
				toQuestionInfoResponse = false;
			}else {
				$('#questionInfosLink').trigger('click');
			}
    };
    
  	this.checkToDisplayEmpty = function(){
      if($questionInfoListContainer.html() == ""){
      	$questionInfoListContainer.html('<p class="empty">'+NOREQUESTDEMANDELABEL+'</p>');
    	}
      if($responseQuestionInfoListContainer.html() == ""){
      	$responseQuestionInfoListContainer.html('<p class="empty">'+NORESPONSELABEL+'</p>');
    	}
  	};
  	
    this.displayQuestionInfos = function(questionInfos){
      $('.categList').hide(); 
      $('#content').show();
    	var questionInfo = null;
    	var questionInfoManage = null;
    	var responseInfoManage= null;
  		for (var index in questionInfos) {
  			questionInfo = questionInfos[index];
  			questionInfoManage = new $.QuestionInfoManage(questionInfo, that);
  			$questionInfoListContainer.append(questionInfoManage.getDOM());
        if(questionInfo.response != ''){
        	responseInfoManage = new $.ReponseInfoManage(questionInfo, that);
        	$responseQuestionInfoListContainer.append(responseInfoManage.getDOM());
        }	        
  		}
  		that.checkToDisplayEmpty();
    	$('.career-question-info .scroll-pane').jScrollPane({ autoReinitialise: true });
    	$('.modal-loading').hide();
    };
    
    //---------- User Ma career Candidature Main Menu -----//
    $('#questionInfosLink').click(function () {
      $('.modal-loading').show();
      $('.candidateQuestionInfoContainer').hide();
      $('#candidateQuestionInfoContainer').show();
      $("#candidateQuestionInfos .glyphicon").removeClass('active');
      $(this).closest('li').addClass('active');
      $('.career-question-info .scroll-pane').jScrollPane({ autoReinitialise: true });
      $('.modal-loading').hide();
    });

    //---------- User Ma career Reponse  Main Menu -----//
    $('#answersQuestionInfosLink').click(function () {
      $('.modal-loading').show();
      $('.candidateQuestionInfoContainer').hide();
      $('#candidateQuestionInfoResponseContainer').show();
      $("#candidateQuestionInfos .glyphicon").removeClass('active');
      $(this).closest('li').addClass('active');
      $('.career-reponse-question-info .scroll-pane').jScrollPane({ autoReinitialise: true });
      $('.modal-loading').hide();
    });

    this.addListener = function (list) {
      listeners.push(list);
    };
		
	},
	
	QuestionInfoViewListener: function(list) {
		if(!list) list = {};
		return $.extend(list);
	}
	
});
