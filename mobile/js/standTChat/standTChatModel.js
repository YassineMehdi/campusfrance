jQuery.extend({
	StandTChatModel: function () {
    var listeners = new Array();
    var that = this;
    var tChatEvent = null;

    this.initModel = function () {
    };
    
    this.getInfoForStandTChatSuccess = function(xml){
    	tChatEvent = new $.TChatEvent(xml);
    	//console.log(tChatEvent);
    	if (tChatEvent != null) {    		
    		var langsubstitution=null;
    		$(xml).find('langsubstitution > idlangage').each(function(){
    						langsubstitution= $(this).text();
    	  			});
    		//console.log("*******************langsubstitution:"+langsubstitution);
    		var langId=null;
    		if(langsubstitution == null){
    			langId=getIdLangParam();
    		}else{	
    			langId=langsubstitution;	
    		}
    		//console.log( "===>>>"+SEPARATEPUBLICCHATSO + langsubstitution + SEPARATEPUBLICCHATSO );
    		that.topicBuilder(tChatEvent, langId);
    	}
    	//that.notifyGetInfoForStandTChatSuccess(tChatEvent);
    };

    this.getInfoForStandTChatError = function(xhr, status, error) {
    	isSessionExpired(xhr.responseText);
    };
    
    this.getInfoForStandTChat = function(standFullId) {
    	genericAjaxCall('POST', staticVars["urlBackEndCandidate"]
    			+ 'chat/infoChatStand?' + standFullId +'&langID='+getIdLangParam(), //ici il faut changer idLang par idSub ...
    			'application/xml', 'xml', '', that.getInfoForStandTChatSuccess, that.getInfoForStandTChatError);
    };

    this.buildTopicSuccess = function(xml, tChatEvent, langIdToConnect){
    	var standLangs = $(xml).find('body').text();
    	if(standLangs != ""){
    		var langs = standLangs.split(SEPARATESTANDANDLANGS)[1];
    		if(langs != "" && langs != null){
    			var standId=tChatEvent.getEvent().getStand().getIdStande();
    			var eventId=tChatEvent.getEvent().getIdEvent();
    			var j=0;
    			var langsArray = langs.split(SEPARATELANGSSTAND);
    			for(j in langsArray){   
    				var langId = langsArray[j];
    				//console.log(langId+" : "+getIdLangParam());
    				if(langId == langIdToConnect){
    					var topic = standId+ SEPARATEPUBLICCHATSO + langId + SEPARATEPUBLICCHATSO +eventId;
    					//console.log(" topic : "+topic);
    					that.notifyStartChat(standId, langId, topic, tChatEvent);
    					return;
    				}
    			}	
    		}else that.notifyStandNotConnected(tChatEvent);
    	}else that.notifyStandNotConnected(tChatEvent);
    };
    
    this.buildTopicError = function(xhr, status, error) {
    	that.notifyStandNotConnected(tChatEvent);
    };
    
    this.topicBuilder = function(tChatEvent, langId) {
  		genericAjaxCall('GET', staticVars["urlBackEndEnterprise"] + 'languaget/topicBuilder/?idStand=' + tChatEvent.getEvent().getStand().getIdStande() + '&idLang='+langId,
  		'application/xml', 'xml', '', function(xml){
  			that.buildTopicSuccess(xml, tChatEvent, langId);			
  		}, that.buildTopicError);
    };
    this.getCacheStand = function(){
    	getCacheStand();
    };
    this.addListener = function (list) {
      listeners.push(list);
    };

    this.notifyGetInfoForStandTChatSuccess = function(tChatEvent){
    	$.each(listeners, function (i) {
        listeners[i].getInfoForStandTChatSuccess(tChatEvent);
      });
    };

    this.notifyStartChat = function(standId, currentLangId, topic, tChatEvent){
    	$.each(listeners, function (i) {
        listeners[i].startChat(standId, currentLangId, topic, tChatEvent);
      });
    };

    this.notifyStandNotConnected = function(tChatEvent){
    	$.each(listeners, function (i) {
        listeners[i].standNotConnected(tChatEvent);
      });
    };
    
  },
  StandTChatModelListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }

});