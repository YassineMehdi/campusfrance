jQuery.extend({
	StandTChatView: function () {
    var listeners = new Array();
    var that = this;
    var infoStandTChatHandler = null;
    var currentChatStand = null;
    
    this.initView = function () {
      infoStandTChatHandler =  $('#modal-stand-tchat .info-stand-tchat');
      $('#tchat-buttons').unbind('click').bind('click', function () {
        if(connectedUser){
	        if(!($("#modal-stand-tchat").data('bs.modal') || {}).isShown) {
		        factoryActivateFaIcon(this);
		        $('.modal-loading').show();
		        $('.modal').modal('hide');
		        $('#modal-stand-tchat').modal('show');
		        that.notifyGetInfoForStandTChat($('#tchat-buttons').attr("standid"));
	        }
	  		}else {
	  			$('#modal-forgot-password').modal('hide');
	  		  $('#modal-forgot-password-Success').modal('hide');
	  		  $("#modal-sign-in").modal();
	  		}
      });
    };

    this.initChatStandComponent = function (){
    	var inputTextChat_CSHandler= $('#inputTextChat'+CSTAG);
    	var sendMessageStandButton_CSHandler= $('#sendMessageButton'+CSTAG);
    	var chatReduceStandsButton_CSHandler= $('#chatReduceStandsButton'+CSTAG);
    	var fullScreenChatSpan_CSHandler= $('#fullScreenChatSpan'+CSTAG);
    	var exitFullScreenChatSpan_CSHandler= $('#exitFullScreenChatSpan'+CSTAG);
    	var settingsChat_CSHandler= $('#settingsChat'+CSTAG);
    	var viewHistory_CSHandler= $('#viewHistory'+CSTAG);
    	var reduceChat_CSHandler= $('#reduceChat'+CSTAG);
    	//checkboxChat_CSHandler= $('#checkboxChat'+CSTAG);
    	saveChatButton_CSHandler=$('#saveChatButton'+CSTAG);
    	stopSaveChatButton_CSHandler=$('#stopSaveChatButton'+CSTAG);
    	sendMessageStandButton_CSHandler.bind("click", function() {
    		if (inputTextChat_CSHandler.val() != '' && currentStand_CS != null) {
    			sendMessage(currentStand_CS, inputTextChat_CSHandler.val());
    			inputTextChat_CSHandler.val('');
    		}
    	});

    	inputTextChat_CSHandler.bind('keydown', function(e) {
    		if (e.keyCode == 13) {
          if(!e.shiftKey){
          	if (inputTextChat_CSHandler.val() != '' && currentStand_CS != null) {
      				sendMessage(currentStand_CS, inputTextChat_CSHandler.val());
      				inputTextChat_CSHandler.val('');
  						setTimeout(function(){
  							inputTextChat_CSHandler.val('');
  						}, 500);
  					}
  				}
  			}
    	});
    	chatReduceStandsButton_CSHandler.bind("click", function() {
    		extendScreenChat('#chatListStands_CS .standsConnect', '#chatListStands_CS .separateStandsConnectChat', '#chatListStands_CS .coprsChat', '#fullScreenChatSpan_CS', '#exitFullScreenChatSpan_CS');
    	});
    	
    	fullScreenChatSpan_CSHandler.bind("click", function() {
    		extendScreenChat('#chatListStands_CS .standsConnect', '#chatListStands_CS .separateStandsConnectChat', '#chatListStands_CS .coprsChat', '#fullScreenChatSpan_CS', '#exitFullScreenChatSpan_CS');
    	});
    	
    	exitFullScreenChatSpan_CSHandler.bind("click", function() {
    		//console.log("exitFullScreenChatSpan_CSHandler");
    		reduceScreenChat('#chatListStands_CS .standsConnect', '#chatListStands_CS .separateStandsConnectChat', '#chatListStands_CS .coprsChat', '#fullScreenChatSpan_CS', '#exitFullScreenChatSpan_CS');
    	});
    	/*checkboxChat_CSHandler.bind("change", function() {
            var check = $(this).is(":checked");
            if(currentStand_CS!=null){
            	var currentStandArray=currentStand_CS.split(SEPARATEPUBLICCHATSO);
                standId = currentStandArray[0];
                languageId = currentStandArray[1];
                chatId = currentStandArray[2];
                updateRegisterChatCall(check, standId, languageId, chatId);
            }else checkboxChat_CSHandler.prop('checked', !check);
    	});*/
    	settingsChat_CSHandler.click(function(){
  			doSettings();
    	});
    	viewHistory_CSHandler.click(function(){
    		viewMyChats();
    	});
    	reduceChat_CSHandler.click(function(){
    		reduceStandChat();
    	});
    	saveChatButton_CSHandler.click(function(){
    		factoryUpdateRegisterChatCall(currentStand_CS, true);
    	});
    	stopSaveChatButton_CSHandler.click(function(){
    		factoryUpdateRegisterChatCall(currentStand_CS, false);
    	});
    };
    this.startChat = function(standId, currentLangId, topicStand, tChatEvent) {
    	/*if(FlashDetect.installed){
        infoStandTChatHandler.html('<div id="chatStandFlashContent"/>');   
        getAMSIp(function(ip){
           if(ip != undefined && ip != "")
            that.initSwfChatStand(standId, currentLangId, topicStand, tChatEvent, 'chatStandFlashContent', ip); 
           else that.launchChatStandHtml5(standId, currentLangId, topicStand, tChatEvent);
        });
      }else{*/
          that.launchChatStandHtml5(standId, currentLangId, topicStand, tChatEvent);
      //}
    };
    this.launchChatStandHtml5 = function(standId, currentLangId, topicStand, tChatEvent){
      //console.log(tChatEvent);
      var candidate=tChatEvent.getCandidate();
      var userProfile=candidate.getUserProfile();
      //console.log(userProfile);
      userId = userProfile.getUserProfileId();
      fullName=userProfile.getFullName();
      photoURL = userProfile.getAvatar();
      profileUrl = PROFILE_URL+candidate.getCandidateId();
      infoStandTChatHandler.load('modules/chat/chatHtml5/chatStandHtml5Component.html', function () {
        currentStandId = standId;
        currentChatStandLanguageId = currentLangId;
        traductChatHtml5Stand();
        //initChatHtml5();
        //initAthmosphereListener();
        //$('.chatPublicLoading').css('display', 'none');
        //chatTabs = $('#tabs1').scrollTabs();
        //$('#tchatG .scroll-pane').jScrollPane({ autoReinitialise: true });
        $('#chatListStands_CS').show();
        currentChatStand = topicStand;
        that.initChatStandComponent();
        that.notifyGetCacheStand();
        $('.modal-loading').hide();
      });
    };
    this.initSwfChatStand = function (currentStandId, currentLangId, topicStand, tChatEvent, zoneDIV, ip) {
      standflashvars.serverUrl = SERVER_BC_URL;
      standflashvars.fmsUrl = FMS_URL.replace(":ip:", ip) + FMS_APPLICATION_CHAT;
      standflashvars.profileUrl = PROFILE_URL;
      standflashvars.imagesUrl = IMAGES_URL;
      standflashvars.standId = currentStandId;
      standflashvars.state = 'stand';
      standflashvars.serverManagerUrl = SERVER_BE_URL;
      standflashvars.standName = tChatEvent.getEvent().getStand().getName();
      standflashvars.translateUrl = getTranslatePath();
      standflashvars.languageId = getIdLangParam();
      standflashvars.standLanguageId = currentLangId;
      standflashvars.topicStand = topicStand;
      standflashvars.userType = userType;
      standflashvars.extenderTimestamp = EXTENDERTIMESTAMP;
      //standflashvars.urlStyleSWF = getStyleChatSWFPath();

      var swfVersionStr = "11.1.0";
      var xiSwfUrlStr = "";
      var params = {};
      params.quality = "high";
      params.bgcolor = "#ffffff";
      params.allowscriptaccess = "always";
      params.allowfullscreen = "true";
      var attributes = {};
      attributes.id = "ChatApp";
      attributes.name = "ChatApp";
      attributes.align = "middle";
      var date = new Date();
      var time = date.getTime();
      swfobject.embedSWF(
              "modules/chat/chatFlex/ChatApp.swf?version=" + time, zoneDIV,
              "100%", "100%",
              swfVersionStr, xiSwfUrlStr,
              standflashvars, params, attributes, function () {
                setTimeout(function () {
                	$('.modal-loading').hide();
                	$('#modal-stand-tchat .info-stand-tchat object').height("90%");
                }, 3000);
                setTimeout(function () {
                	$('#modal-stand-tchat .info-stand-tchat object').height("100%");
                }, 5000);
              });
      swfobject.createCSS('#' + zoneDIV, "display:block;text-align:left;");

      /*$('#signOut').bind('exitforum', function () {
        $('#listStandchatters').empty();
      });*/
    };

    this.standNotConnected = function(tChatEvent) {
        infoStandTChatHandler
          .html('<p class="empty">' + STANDNOTYETCONNECTEDLABEL.replace(":standName:", tChatEvent.getEvent().getStand().getName()) + '</p>'
              +'<span class="btn let-message">' + LET_MESSAGE_LABEL + '</span>');
        infoStandTChatHandler.find(".let-message").bind("click", function(){
        $("#send-message-link").trigger('click');
      });
      $('.modal-loading').hide();
    };
    
    this.addListener = function (list) {
      listeners.push(list);
    };
    
    this.notifyGetInfoForStandTChat = function(standFullId){
    	$.each(listeners, function (i) {
        listeners[i].getInfoForStandTChat(standFullId);
      });
    };
    
    this.notifyGetCacheStand = function(){
    	$.each(listeners, function (i) {
        listeners[i].getCacheStand();
      });
    };
    
  },
  StandTChatViewListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }
});
