jQuery.extend({
  SondagesStandController: function (model, view) {
    var that = this;
    var listSondagessInCache = [];
    /**
     * listen to the view
     */

    var vlist = $.ViewListener({
      launchSondagesStand: function (standId) {
        model.getSondagesStand(standId);
      },
      addReponse: function (attend,formId) {
        model.addReponse(attend,formId);
      },
    });

    view.addListener(vlist);

    /**
     * listen to the model
     */
    var mlist = $.ModelListener({
      loadSondagesStand: function (listSondagess) {
        view.displaySondagesStand(listSondagess);
      },
      notifyAddResponseComplete: function (sondageFormId) {
        view.addResponseComplete(sondageFormId);
      },
    });

    model.addListener(mlist);

    this.initController = function () {
      view.initView();
      model.initModel();
    };
    that.initController();
  }
});