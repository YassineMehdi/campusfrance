jQuery.extend({
  SondagesStandModel: function () {
    var listeners = new Array();
    var that = this;
	var sondageFormId = 0;
    this.initModel = function () {
      //this.getSondagesStand();
    };
    
    //------------- get All Sondagess -----------//
    
    this.addReponseSuccess = function(data){
		that.notifyAddResponseComplete(sondageFormId);
	};
	
	this.addReponseError = function(xhr, status, error){
		isSessionExpired(xhr.responseText);
	};
    
    this.addReponse =  function(attend,formId) {
    	sondageFormId = formId;
    	var attendDtoList = "";
    	
    	//console.log(attend);
    	//console.log(attend.attendsData);
    	
    	attendDtoList+='<attendDataDTOList>';
		for (index in attend) {
			var attendData = attend[index];
			attendDtoList+=attendData.xmlData();
		}
		attendDtoList+='</attendDataDTOList>';
		genericAjaxCall('POST', staticVars["urlBackEndCandidate"] +'survey/update/reply?' + $('#sondage-link').attr('standId') + '&idLang=' + getIdLangParam(), 'application/xml', 'xml', attendDtoList, 
				that.addReponseSuccess, that.addReponseError);
	};

    //------------- get All Sondagess -----------//
    this.getSondagesStandError = function (xhr, status, error) {
      isSessionExpired(xhr.responseText);
    };

    this.getSondagesStand = function (standId) {
      listSondagessCache = [];
      genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'survey/get/stand?' + standId + '&idLang=' + getIdLangParam(), 'application/xml', 'xml', '', function (xml) {
        listSondagessCache = new Array();
        $(xml).find("formDTO").each(function () {
          var sondages = new $.Form($(this));
          listSondagessCache[sondages.getFormId()] = sondages;
        });
        that.notifyLoadSondagesStand(listSondagessCache);
      }, that.getSondagesStandError);
    };

    this.notifyLoadSondagesStand = function (listSondagess) {
      $.each(listeners, function (i) {
        listeners[i].loadSondagesStand(listSondagess);
      });
    };

    this.notifyAddResponseComplete = function () {
      $.each(listeners, function (i) {
        listeners[i].notifyAddResponseComplete(sondageFormId);
      });
    };

    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  ModelListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }

});