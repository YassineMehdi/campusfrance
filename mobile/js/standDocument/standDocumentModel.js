jQuery.extend({
  StandDocumentModel: function () {
    var listeners = new Array();
    var that = this;

    this.initModel = function () {
    };
 
    //------------- get All Documents -----------//
    this.getDocumentsByStandSuccess = function (xml) {
      var documents = new Array();
      $(xml).find("allfiles").each(function () {
        var document = new $.AllFiles($(this));
        documents.push(document);
      });
      that.notifyLoadStandDocuments(documents);
    };
    this.getDocumentsByStandError = function (xhr, status, error) {
      isSessionExpired(xhr.responseText);
    };
    this.getDocumentsByStand = function (standId, fileTypeId) {
      genericAjaxCall('GET', staticVars["urlBackEndCandidate"]
              + 'allfiles/documentsByType?' + standId + '&idLang=' + getIdLangParam()+'&fileTypeId='+fileTypeId,
              'application/xml', 'xml', '',
              that.getDocumentsByStandSuccess,
              that.getDocumentsByStandError);
    };
    this.notifyLoadStandDocuments = function (documents) {
      $.each(listeners, function (i) {
        listeners[i].loadStandDocuments(documents);
      });
    };

    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  ModelListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }

});
