jQuery.extend({
  StandDocumentController: function (model, view) {
    var that = this;
    /**
     * listen to the view
     */
 
    var vlist = $.ViewListener({
    		getDocumentsByStand: function(standId,fileTypeId){
    	  model.getDocumentsByStand(standId,fileTypeId);
    	}
    });

    view.addListener(vlist);

    /**
     * listen to the model
     */
    var mlist = $.ModelListener({
      loadStandDocuments: function (documents) {
        view.loadStandDocuments(documents);
      },
    });

    model.addListener(mlist);

    this.initController = function () {
      view.initView();
      model.initModel();
    };
  },
});
