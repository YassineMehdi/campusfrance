jQuery.extend({
  DocumentsManage: function (document, documentsContainer, view) {
    var isFavorite = factoryFavoriteExist(document.getAllFilesId(), FavoriteEnum.MEDIA);
    var displayDeleteFavorite = "block";
    var displayAddFavorite = "block";
    if (isFavorite)
      displayAddFavorite = "none";
    else
      displayDeleteFavorite = "none";

    var dom = $('<li>' 
            + '<div class="infos">'
            + '<ul>'
            + '<li>'
            + '<div class="col-md-10 col-sm-10 no-padding">'
            + '<a class="document-name">'
            + htmlEncode(substringCustom(formattingLongWords(document.getTitle(), 20), 60))
            + '</a>'
            + '</div>'
            + '<div class="  col-md-2 col-sm-2 no-padding-left">'
            + '</div>'
            + '</li>'
            + '<li><a class="btn green col-md-12 addDocumentToFavorite" style="display:' + displayAddFavorite + '" ><i aria-hidden="true" class="glyphicon glyphicon-star"></i><span class="productActionLabel">' + ADDTOFAVORITELABEL + '</span></a><a class="btn btn-primary col-md-12 deleteDocumentFromFavorite" style="display:' + displayDeleteFavorite + ';background-color: #b3081b;"><i aria-hidden="true" class="glyphicon glyphicon-star"></i><span class="productActionLabel">' + DELETEFROMFAVORITELABEL + '</span></a></li>'
            + '<li><a class="btn btn-primary col-md-12 blue openCurrentDocument"><i class="fa fa-eye" aria-hidden="true"></i><span class="productActionLabel">'+VIEWLABEL+'</span></a></li>'
            + '</ul>'
            + '</div>'
            + '</li>');

    this.refresh = function () {
      dom.find(".openCurrentDocument").unbind();
      dom.find(".openCurrentDocument").bind('click', function () {
        $(".agrandir-document-stand").bind('click', function () {
          $('#modal-document-lecteur-big').modal();
          openDocument(document, $("#modal-document-lecteur-big").find('.ad-image-wrapper'), $("#modal-document-lecteur-big").find('.modal-title'));
        });
      	view.openDocument(document);
      });

      var addDocumentFavoriteLink = dom.find(".addDocumentToFavorite");
      var deleteDocumentFavoriteLink = dom.find(".deleteDocumentFromFavorite");

      addDocumentFavoriteLink.click(function () {
        if (connectedUser) {
          var idEntity = document.getAllFilesId();
          var componentName = FavoriteEnum.MEDIA;
          var favoriteXml = factoryXMLFavory(idEntity, componentName);
          addFavorite(favoriteXml);
          $(this).hide();
          $(this).closest('div').find('.deleteDocumentFromFavorite').show();
        } else {
          getStatusNoConnected();
        }
      });

      deleteDocumentFavoriteLink.click(function () {
        var idEntity = document.getAllFilesId();
        var componentName = FavoriteEnum.MEDIA;
        var favoriteXml = factoryXMLFavory(idEntity, componentName);
        deleteFavorite(favoriteXml);
        $(this).hide();
        $(this).closest('div').find('.addDocumentToFavorite').show();
      });
    };
    this.getDOM = function () { //retourne une ligne HTML <a .....
      return dom;
    };
    this.refresh();
  },
  StandDocumentView: function () {
    var listeners = new Array();
    var that = this;
    var galleriesDocuments = null;
    var infoStandDocumentHandler = null;
    var galleryDocumentHandler = null;
    var galleryTitleHandler = null;
    var documentsContentHandler = null;

    $('#modal-document').on('hidden.bs.modal', function () {
    	$(this).find('.info-stand-document').empty();
    });
    this.initView = function () {
	    infoStandDocumentHandler = $('#modal-document .info-stand-document');
        $(".menu-stand .documentType").unbind('click').bind('click', function(){
          if(!($("#modal-document").data('bs.modal') || {}).isShown) {
	        factoryActivateFaIcon(this);
			    $('.modal').modal('hide');
			    $('.modal-loading').show();
			    that.notifyGetDocumentsByStand($('.documentType').attr('standId'),$(this).attr('fileType'));
			    $('#modal-document').modal('show');
	      }
      });
    };
    this.loadStandDocuments = function (documents) {
        galleriesDocuments = documents;
        if (documents != null && Object.keys(documents).length > 0) {
        	infoStandDocumentHandler.html('<div class="col-md-9 col-sm-8 no-padding">'
	            +'<h3 class="documentTitle"></h3>'
		            +'<div id="galleryDocument">'
			            +'<div class="ad-image-wrapper">'
			            +'</div>'
		            +'</div>'
	            +'</div>'
	            +'<div class="listing scroll-pane col-md-3 col-sm-3">'
		            +'<ul class="ul-docs">'
		            +'</ul>'
	            +'</div>');
        galleryDocumentHandler = infoStandDocumentHandler.find('.ad-image-wrapper');
        galleryTitleHandler = $('.document-title-head');
        documentsContentHandler = infoStandDocumentHandler.find('.ul-docs');
        var documentManage = null;
        for (var index in documents) {
          documentManage = new $.DocumentsManage(documents[index], documentsContentHandler, that);
          documentsContentHandler.append(documentManage.getDOM());
        }
        that.openDocumentByIndex(0);
        infoStandDocumentHandler.find('.scroll-pane').jScrollPane({ autoReinitialise: true });
       } else {
    	   infoStandDocumentHandler.html('<p class="empty">'+NODOCUMENTSLABEL+'</p>');
       }
       $('.modal-loading').hide();
    };

    this.openDocumentByIndex = function (index) {
      if(galleriesDocuments != null){
        $(".agrandir-document-stand").bind('click', function () {
          $('#modal-document-lecteur-big').modal();
          openDocument(galleriesDocuments[index], $("#modal-document-lecteur-big").find('.ad-image-wrapper'), $("#modal-document-lecteur-big").find('.modal-title'));
        });
	      that.openDocument(galleriesDocuments[index]);
      }
    };
    this.openDocument = function (document) {
  	  if(document != null){
  	    openDocument(document, galleryDocumentHandler, galleryTitleHandler);
  	  }
    };

    this.notifyGetDocumentsByStand = function (standId, fileTypeId) {
      $.each(listeners, function (i) {
        listeners[i].getDocumentsByStand(standId, fileTypeId);
      });
    };

    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  ViewListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }
});

