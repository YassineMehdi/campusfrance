jQuery.extend({
  HomePageView: function () {
    var that = this;
    var listeners = new Array();
    this.playerSelector = '';
    this.initView = function () {
      isStandURL();
      $("#searchSelect").material_select();
      switch(getIdLangParam()) {
		      case "1":
		          moment.locale('en');
		          $("#langDrop").append('<span class="">'+ARABICLABEL+'</span><span class="caret"></span>');
		      break;
		      case "2":
		          moment.locale('en');
		          $("#langDrop").append('<span class="">'+ENGLISHLABEL+'</span><span class="caret"></span>');
		      break;
		      case "3":
		          moment.locale('fr');
		          $("#langDrop").append('<span class="">'+FRENCHLABEL+'</span><span class="caret"></span>');
		      break;
		      case "4":
		          moment.locale('ru');
		          $("#langDrop").append('<span class="">'+RUSSIANLABEL+'</span><span class="caret"></span>');
		      break;
		      case "5":
		          moment.locale('de');
		          $("#langDrop").append('<span class="">'+GERMANLABEL+'</span><span class="caret"></span>');
		      break;
		      case "6":
		          moment.locale('es');
		          $("#langDrop").append('<span class="">'+SPANISHLABEL+'</span><span class="caret"></span>');
		      break;
		  }
      updateCurrentTime('#date-session');
      updateSessionTime('#session_label', 0, 0, 0);
      //$("#languageSelect").material_select();
      //$( window ).load(function() {
        if (FlashDetect.installed) {
          if ($.cookie('cookieBar')) {
            $('.cookieContainer').hide();
          } else {
            $('.cookieContainer').show();
            $('.accepteCookie').on('click', function () {
              $.cookie('cookieBar', 'true');
              $('.cookieContainer').hide();
            });
          }
        } else {
          $('.cookieContainer').hide();
        }
      //});
      $('.registerButton').on('click', function () {
        window.open("http://" + HOSTNAME_URL + "/visitor/forumregistration.html?idLang="+getIdLangParam(), "_blank");
      });
      $('#logoEvent').on('click', function () {
      	location.replace("http://" + HOSTNAME_URL + "/mobile/new-home.html?idLang="+getIdLangParam());
      });
      $('.getModal').on('click', function () {
        var target = $(this).attr('data-target');
        if (target && !$(target).hasClass('in')) {
          $('.modal').modal('hide');
          $(target).modal('show');
        }
      });

      $('.menu-btn-container .icon-reorder').on('click',function() {
        $(".menu-container").animate({'left': '0px'},350);
        $(".menu-container-cover").show();
      });

      $('.close-main-menu').on('click',function() {
        $(".menu-container").animate({'left': '-400px'},350);
        $(".menu-container-cover").hide();
      });

      $(document).mouseup(function (e){
        if($(".menu-container").attr('style') == 'left: 0px;' || $(".menu-container").attr('style') == undefined){
          var container = $(".menu-container");
          if (!container.is(e.target) && container.has(e.target).length === 0) {
            $(".menu-container").animate({'left': '-400px'},350);
            $(".menu-container-cover").hide();
          }
        }        
      });

      $('.search-button').on('click',function() {
        $("#modal-search-home").modal();
      });

      $('#searchButton').click(function() {
        that.searchByKeyword();
      });
      $('#searchInput').keypress(function(e) {
        if(e.which == 13) {
            that.searchByKeyword();
        }
      });
      
      $('.icon-format2,.icon-format3,.icon-format4,#full-agenda-show,.allProductsLink,.allDocumentsLink,.icon-format5,.icon-format6,.icon-format7,.userFavoritesLink,.userMessagesLink,.userCareerLink,.userQuestionInfoLink,.userMyChatsLink,.userProfileLink,.accountSettingsLink,.myAccountLink,.listVisitorsLink,.display-all-alerts,#listStandsLink,.goToStandList').on('click', function () {
        //alert('azz');
        $('.sliderG').show();
        $('.slogo').hide();
        reloadSponsorJcarousel();
        $('#modal-videoRh-lecteur').find('.close').click();
        $('.headerTop').hide();
        $('.mainContentAbsolutePage').addClass('staticPosition');   
        $('body').addClass('bodyStyleCustom');
        $(".menu-container").animate({'left': '-400px'},350);
        $(".menu-container-cover").hide();
        $("#mainPageContent,.categList").show();
        $(".icon-tchat,.menu-btn-container .fa-search,.icon-reorder,.fa-bell ,#message-container .icon-envelope").addClass('greenColor');  
        $('#hall1MainContainer').attr('style','background: url(img/halls/hall_asmex_'+getIdLangParam()+'.jpg);background-position: 0px -252px;background-size: 100%;');
      }); 
      $('#full-agenda-show,.icon-format5,.icon-format6,.icon-format7,.icon-stand').click(function () {
        $('#contentG').removeClass('salle-exposition');
      });
      $('.goToStandList').click(function () {
        $('#contentG').removeClass('faq');
      });
      $('.userFavoritesLink a,.userMessagesLink a,.userCareerLink a,.userQuestionInfoLink a,.userMyChatsLink a,.userProfileLink a,.accountSettingsLink a,.myAccountLink a').click(function () {
        $('#contentG').removeClass('salle-exposition');
      });

      that.slider();
      $profileSelect = $(".profil-select");
      $activitySelect = $(".activity-select");
      $standSector = $(".sector-select");
      $regions = $(".region-select");
      $anneesExperiences = $(".experience-select");
      $('.sliderG').show();
      $('.slogo').hide();
      reloadSponsorJcarousel();
      /* tchat */
      if (chatController == null) {
        chatController = new $.ChatController(
                new $.ChatModel(), new $.ChatView());
      }
      chatController.initController();
      /* end tchat*/
      if (analyticsView == null)
        analyticsView = new $.AnalyticsView();

      if (salleExpositionController == null) {
        salleExpositionModel = new $.SalleExpositionModel();
        salleExpositionView = new $.SalleExpositionView();
        salleExpositionController = new $.SalleExpositionController(
                salleExpositionModel, salleExpositionView);

      } else
        salleExpositionController.initController();
      if (agendasFullController == null) {
        agendasFullModel = new $.AgendaFullsModel();
        agendasFullView = new $.AgendaFullsView();
        agendasFullController = new $.AgendaFullsController(
                agendasFullModel, agendasFullView);

      } else
        agendasFullController.initController();
//---------- Job Offers Main Menu Candidat -----//
      if (genericSearchController == null) {
        genericSearchController = new $.GenericSearchController(
                new $.GenericSearchModel(), new $.GenericSearchView());
      } else
        genericSearchController.initController();
      	$('#full-agenda-show').on('click', function () {
          $('.modal').hide();
          $('.genericContent').hide();
          $('#page-content').removeClass('no-padding');
          $('#full-agenda').show();
          $('#content').show();  
          $('.categList').hide();  
          $('.fc-month-button').trigger('click');
      });
      that.playerSelector = '#content_video_transition';
      that.playerExite = '#content_video_transition_exite';
      that.mainMenu();
      that.connexionAction();
      that.forgotPasswordAction();
      that.ForgotPassword();
      that.logoutAction();
      that.Player();
      $('.faqLink').on('click', function () {
        $('.modal-loading').show();
        $('.genericContent').attr('style', 'display:none;');
        $('.modal').hide();
        $('#faq').attr('style', 'display:block;');
        $('#contentG').addClass('faq');
        $('#contentG').removeClass('salle-exposition');
        $('#videoHome').hide();
        $('.categList').hide();       
        $('#content').show();
        $('#page-content').removeClass('no-padding');
        $('#faq .scroll-pane').jScrollPane({ autoReinitialise: true });
        $('.modal-loading').hide();
      });
      //console.log("End initView");
      $('.allDocumentsLink, .allJobOffersLink, #full-agenda-show, .icon-format2, .icon-format1, .userFavoritesLink, .icon-messages, .userCareerLink, .userQuestionInfoLink, .userMyChatsLink, .userProfileLink, .accountSettingsLink, .myAccountLink, #logout, .icon-stand, .icon-efq').bind("click", function () {
        //genericAjaxCall('POST', staticVars["urlBackEndCandidate"] + 'stand/removeCurrentStand', 'application/xml', 'xml', '', '', '');
        if (analyticsView != null)
          analyticsView.exitFromStand();
      });
    };
    this.searchByKeyword = function(){
      var searchWord = $('#searchInput').val();
      switch ($('#searchSelect').val()){
            case 'stand':
              $('#listStandsLink').click();
              $(".modal-loading").show();
              setTimeout(function(){ 
                $(".modal-loading").show();
                $('#stand-key').val(searchWord);
                genericSearchController.searchStandByKey(searchWord, "", ""); 
                $(".modal-loading").hide();
                $("#modal-search-home .close").click();
              }, 3000);
              break;
            case 'document':
              $('.allDocumentsLink').click();
              $(".modal-loading").show();
              setTimeout(function(){ 
                $(".modal-loading").show();
                genericSearchController.searchDocumentByKey(searchWord, "", "", "", "", ""); 
                $(".modal-loading").hide();
                $("#modal-search-home .close").click();
              }, 3000);
              break;
            case 'product':
              $('.allProductsLink').click();
              $(".modal-loading").show();
              setTimeout(function(){ 
                $(".modal-loading").show();
                $('#product-key').val(searchWord);
                genericSearchController.searchProductByKey(searchWord, "", ""); 
                $(".modal-loading").hide();
                $("#modal-search-home .close").click();
              }, 3000);
              break;
        }
    };
    this.Player = function () {
      $('.palyer-home').click(function () {
        flowAutoPlay('media/video/2016/intro_exterieur_interieur', 'img/loading.jpg', function () {
          $('#contentG').attr('class', '');
          $('#contentG').addClass('salle-exposition');
          $(".main-menu .sub-main-menu, .second-menu li a").removeClass("active");
          $('a.icon-format2').addClass('active');
          $('#modal-forgot-password').modal('hide');
          $('#modal-forgot-password-Success').modal('hide');
          $('#videoHome').hide();
          $('#modal-sign-in').modal('hide');
          $('.receptionButton').hide();
        }, that.playerSelector);
      });
      $('#webcastPage').click(function () {
        $('#conference-room').hide();
        flowAutoPlay('media/video/2016/conference_digi_event', 'img/conference_digi event.jpg', function () {
          $(".main-menu .sub-main-menu, .second-menu li a").removeClass("active");
          //$('a.icon-format3').addClass('active');
          $('#modal-forgot-password').modal('hide');
          $('#modal-forgot-password-Success').modal('hide');
          $('#modal-sign-in').modal('hide');
          $('#videoHome').hide();
          $('#vue-aerienne').hide();
          $('a.icon-format7').addClass('active');
        }, '#content_video_transition_webcast');
      });
      $('#salle-conference').click(function () {
        flowAutoPlay('media/video/2016/exterieur_batiment_to_conference', 'img/loading.jpg', function () {
          $('#contentG').attr('class', '');
          $('#contentG').addClass('salle-exposition');
          $(".main-menu .sub-main-menu, .second-menu li a").removeClass("active");
          $('a.icon-format7').addClass('active');
          $('#modal-forgot-password').modal('hide');
          $('#modal-forgot-password-Success').modal('hide');
          $('#videoHome').hide();
          $('#modal-sign-in').modal('hide');
          $('.receptionButton').hide();
        }, '#content_video_transition_to_conference');
      }); 
      $('.homeToHall').click(function () {
        console.log('here');
        var roomCategory = this.id;
        flowAutoPlay('media/video/2016/homeToHalls/hall_b_asmex_'+getIdLangParam(), 'img/halls/hall_asmex_'+getIdLangParam()+'.jpg', function () {
          $('#contentG').attr('class', '');
          $('#contentG').addClass('salle-exposition');
          $(".main-menu .sub-main-menu, .second-menu li a").removeClass("active");
          $('a.icon-format2').addClass('active');
          $('#modal-forgot-password').modal('hide');
          $('#modal-forgot-password-Success').modal('hide');
          $('#videoHome').hide();
          $('#modal-sign-in').modal('hide');
          $('.receptionButton').hide();
          $('#principal-halls .img-salle').hide();
          $('.vue-aerienne').hide();
          $('#hall1MainContainer').attr('style','background: url(img/halls/ExpositionsRoom/'+getIdLangParam()+'/'+roomCategory+'_'+getIdLangParam()+'.jpg);background-position: 0px 0px;background-size: 100%;');
        }, "#content_video_transition_from_hall_to_B", roomCategory);
      });  
      $('.homeToHallA').click(function () {
        var roomCategory = this.id;
        flowAutoPlay('media/video/2016/homeToHalls/hall_a_asmex_'+getIdLangParam(), 'img/halls/hall_asmex_'+getIdLangParam()+'.jpg', function () {
          $('#contentG').attr('class', '');
          $('#contentG').addClass('salle-exposition');
          $(".main-menu .sub-main-menu, .second-menu li a").removeClass("active");
          $('a.icon-format2').addClass('active');
          $('#modal-forgot-password').modal('hide');
          $('#modal-forgot-password-Success').modal('hide');
          $('#videoHome').hide();
          $('#modal-sign-in').modal('hide');
          $('.receptionButton').hide();
          $('#principal-halls .img-salle').hide();
          $('.vue-aerienne').hide();
          $('#'+roomCategory+"ExpoRoom").attr('src','img/halls/ExpositionsRoom/'+getIdLangParam()+'/'+roomCategory+'_'+getIdLangParam()+'.jpg');
        }, "#content_video_transition_from_hall_to_A", roomCategory);
      });     
      $('.icone-sortie a').click(function () {
        $('.genericContent').hide();
        flowAutoPlay('media/video/2016/exit', 'img/salle-exposition.jpg', function () {
        	$(".main-menu .sub-main-menu, .second-menu li a").removeClass("active");
          $('a.icon-format1').addClass('active');
        }, that.playerExite);
      });
    };

    this.loadAreaexpertise = function (areaExpertises) {
      // var data = [];
      // $areaExpertise.empty();
//
      // for (index in areaExpertises) {
      // var areaExpertise = areaExpertises[index];
      // var areaExpertiseId = areaExpertise.getAreaExpertiseId();
      // var areaExpertiseName = areaExpertise.getAreaExpertiseName();
      // data.push({
      // value: replaceSpecialChars(areaExpertiseId),
      // text: replaceSpecialChars(areaExpertiseName)
      // });
      // }
      // generateOption(data, $areaExpertise);
      // that.Select($areaExpertise);
      if(localStorage.getItem('tutorial') == null){
        introJs().start();
        localStorage.setItem('tutorial','done');
      }                
    };

    this.loadAnneesExperiences = function (anneesExperiences) {
      var data = [];
      $anneesExperiences.empty();

      for (index in anneesExperiences) {
        var anneesExperience = anneesExperiences[index];
        var anneesExperienceId = anneesExperience.getIdExperienceYears();
        var anneesExperienceName = anneesExperience.getExperienceYearsName();
        data.push({
          value: replaceSpecialChars(anneesExperienceId),
          text: replaceSpecialChars(anneesExperienceName)
        });
      }
      generateOptionSelect(data, $anneesExperiences, 'Années d\'experience');
      that.Select($anneesExperiences);
    };

    this.loadJobSought = function (jobSoughts) {
      // var data = [];
      // $jobSought.empty();
//
      // for (index in jobSoughts) {
      // var jobSought = jobSoughts[index];
      // var jobSoughtId = jobSought.getJobSoughtId();
      // var jobSoughtName = jobSought.getJobSoughtName();
      // data.push({
      // value: replaceSpecialChars(jobSoughtId),
      // text: replaceSpecialChars(jobSoughtName)
      // });
      // }
      // generateOption(data, $jobSought);
      // that.Select($jobSought);
    };

    this.loadRegions = function (regions) {
      var data = [];
      $regions.empty();
      for (index in regions) {
        var region = regions[index];
        var regionId = region.getIdRegion();
        var regionName = region.getRegionName();
        data.push({
          value: replaceSpecialChars(regionId),
          text: replaceSpecialChars(regionName)
        });
      }
      generateOptionSelect(data, $regions, 'Pays');
      that.Select($regions);
    };

    this.loadActivitySectors = function (activitySectors) {
      var data = [];
      $activitySelect.empty();

      for (index in activitySectors) {
        var activitySector = activitySectors[index];
        var activitySectorId = activitySector.getIdsecteur();
        var activitySectorName = activitySector.getName();
        data.push({
          value: replaceSpecialChars(activitySectorId),
          text: replaceSpecialChars(activitySectorName)
        }); 
      }
      generateOptionSelect(data, $activitySelect, ACTIVITYSECTORLABEL);
      that.Select($activitySelect);



    };

    this.loadStandSectors = function (standSectors) {
      var data = [];
      $standSector.empty();
      $('.categList').empty();
      for (index in standSectors) {
        var standSector = standSectors[index];
        var standSectorId = standSector.getSectorId();
        var standSectorName = standSector.getSectorName();
        data.push({
          value: replaceSpecialChars(standSectorId),
          text: replaceSpecialChars(standSectorName)
        }); 

        $('.categList').append('<a href="#" id="'+sectorEnum[standSectorId]+'" class="lien homeToHall homeTo'+sectorEnum[standSectorId]+'"><div class="list-item">'
                                +'<div class="item">'
                                    +'<div class="item-description sectorStyle'+standSectorId+'">'
                                        +'<div class="item-name">'+standSectorName+'</div>'
                                    +'</div>'
                                    +'<div class="item-logo green-business">'
                                        +'<img src="img/categ/'+standSectorId+'.jpg" alt="">'
                                    +'</div>'
                                +'</div>'
                            +'</div></a>');
      }
      generateOptionSelect(data, $standSector, ACTIVITYSECTORLABEL);
      that.Select($standSector);
    };

    this.loadFunctionCandidates = function (functionCandidates) {
      var data = [];
      $profileSelect.empty();
      for (index in functionCandidates) {
        var functionCandidate = functionCandidates[index];
        var functionCandidateId = functionCandidate.getIdFunction();
        var functionCandidateName = functionCandidate.getFunctionName();
        data.push({
          value: replaceSpecialChars(functionCandidateId),
          text: replaceSpecialChars(functionCandidateName)
        });
      }
      generateOptionSelect(data, $profileSelect, 'Profile');
      that.Select($profileSelect);
    };

    this.Select = function (Select) {
      Select.material_select();
    };
    this.MultiSelect = function (Selector) {
      $(Selector).multiselect({
        noneSelectedText: SELECTLABEL,
        selectedList: 3,
        header: false,
        selectedText: '# ' + SELECTED_LABEL,
      });
    };

    //--------- Main Menu --------//
    this.mainMenu = function () {
      TriggerClick = 0;
      $(".main-menu .icon-nav").click(function () {
        if (TriggerClick == 0) {
          setTimeout(function(){
              $('.visitorsCustom').removeClass('hideUsersLabel');
          }, 50);
          TriggerClick = 1;
          $(".main-menu").animate({
            width: "228px"
          }, 100);
          $(".main-menu .profil").animate({
            width: "93%"
          }, 100);
        } else {
          TriggerClick = 0;
          $(".main-menu").animate({width: '60px'}, 100);
          $(".main-menu .profil").animate({width: "0"}, 100);
          $('.visitorsCustom').addClass('hideUsersLabel');
        };

      });
      $(".icon-nav").click(function () {
        $(".main-menu").toggleClass('open');
        $('.visitorsCustom').addClass('hideUsersLabel');
      });
      $(".menu .btn-plus a").click(function () {
        $(this).toggleClass('active');
      });
      $(".menu .btn-plus a").click(function () {
        $(".second-menu").slideToggle({
          duration: 1000,
        });
      });
    };
    //---------- Home Page Main Menu Candidat -----//
    $('.icon-format1,#headerMainContainer').unbind('click').bind('click', function () {
      $("#mainPageContent").hide();
      $(".icon-tchat,.menu-btn-container .fa-search,.icon-reorder,.fa-bell ,#message-container .icon-envelope").removeClass('greenColor'); 
      $('.headerTop').show();
      $('.mainContentAbsolutePage').removeClass('staticPosition');   
      $('.home').removeClass('bodyStyleCustom');
      $(".menu-container").animate({'left': '-400px'},350);
      $(".menu-container-cover").hide();
      $(".closeListPage").click();
    });

    $('.icon-format7').unbind('click').bind('click', function () {
      $('#contentG').attr('class', '');
      $('#modal-forgot-password').modal('hide');
      $('#modal-forgot-password-Success').modal('hide');
      $('#modal-sign-in').modal('hide');
      $('#page-content').addClass('no-padding');
      $('.genericContent').attr('style', 'display: none;');
      $('#conference-room').show();
      setTimeout(function(){$('#webcastPage').click();$('.categList').hide();$('#content').show();}, 0);
      window.history.pushState("test", "Title", "/mobile/new-home.html?idLang="+getIdLangParam());
    });

    $('.stands').unbind('click').bind('click', function () {
      //alert('HERE');
      var standId = $(this).attr('standId');
      $('#contentG').attr('class', '');
      $('#contentG').attr('class', 'info-stand stand' + standId);
      $(".main-menu .sub-main-menu, .second-menu li a").removeClass("active");
      if (standId == 7) {
        $('a.icon-format3').addClass('active');
      }
      $('#modal-forgot-password').modal('hide');
      $('#modal-forgot-password-Success').modal('hide');
      $('#modal-sign-in').modal('hide');
      $('#page-content').removeClass('no-padding');
      $('#vue-aerienne-info-stand').removeClass('hide');
      $('.genericContent').attr('style', 'display: none;');
      $('#videoHome').hide();
      $('#info-stand').removeClass('hide');
      $('#info-stand').attr('style', 'display: block;');
      $('.menu-stand a,.vue-aerienne-info-stand a').attr('standId', 'standId=' + standId);
      initStandInfoController(standId);
    });
    //---------- Close left Main Menu Candidat -----//
    $('#page-content,.menu').click(function () {
      if ($('.main-menu').attr('class') == 'main-menu open') {
        $(".icon-nav").trigger('click');
      }
    });
    //---------- Vue Liste Main Menu Candidat -----//
    $('.listStandsLink').on('click', function () {
      window.history.pushState("test", "Title", "/mobile/new-home.html?idLang="+getIdLangParam());
      if (genericSearchController == null) {
        genericSearchController = new $.GenericSearchController(
                new $.GenericSearchModel(), new $.GenericSearchView());
      } else
        genericSearchController.initController();
    });

    //---------- Product Main Menu Candidat -----//
    $('.allProductsLink').on('click', function () {
      window.history.pushState("test", "Title", "/mobile/new-home.html?idLang="+getIdLangParam());
      if (genericSearchController == null) {
        genericSearchController = new $.GenericSearchController(
                new $.GenericSearchModel(), new $.GenericSearchView());
      } else
        genericSearchController.initController();
    });
    //---------- Documents Main Menu Candidat -----//
    $('.allDocumentsLink').on('click', function () {
      window.history.pushState("test", "Title", "/mobile/new-home.html?idLang="+getIdLangParam());
      if (documentsController == null) {
        documentsController = new $.DocumentController(
                new $.DocumentModel(), new $.DocumentView());
      }
      documentsController.initController();
    });
    //---------- Videos Rh Main Menu Candidat -----//
    $('.allVideoRhLink').on('click', function () {
      window.history.pushState("test", "Title", "/mobile/new-home.html?idLang="+getIdLangParam());
      if (videoRhController == null) {
        videoRhController = new $.VideoRhController(
                new $.VideoRhModel(), new $.VideoRhView());
      }
      videoRhController.initController();
    });
    //---------- User Mes Chats Main Menu Candidat -----//
    $('.userMyChatsLink').on('click', function () {
      window.history.pushState("test", "Title", "/mobile/new-home.html?idLang="+getIdLangParam());
      //console.log("inside userMyChatsLink");
      if (connectedUser) {
        if (userMyChatsController == null) {
          userMyChatsController = new $.UserMyChatsController(new $.UserMyChatsModel(), new $.UserMyChatsView());
        }
        userMyChatsController.initController();
      } else {
        getStatusNoConnected();
      }
    });
    //---------- User Mon CV Main Menu Candidat -----//
    $('.userProfileLink').on('click', function () {
      window.history.pushState("test", "Title", "/mobile/new-home.html?idLang="+getIdLangParam());
      if (connectedUser) {
        if (myCvController == null) {
          myCvController = new $.MyCvController(new $.MyCvModel(), new $.MyCvView());
        }
        myCvController.initController();
      } else {
        getStatusNoConnected();
      }
    });
    $('.docsCVsLink').click(function () {
      window.history.pushState("test", "Title", "/mobile/new-home.html?idLang="+getIdLangParam());
      $('.userProfileLink').trigger('click');
      $('#docsCVsLink').trigger('click');
    });
    $('.main-menu .sub-main-menu').on('click', function () {
      if (connectedUser) {
	      $(".main-menu .sub-main-menu, .second-menu li a").removeClass("active");
	      $(this).addClass("active");
	  	}
  	});
    $('.second-menu li a').on('click', function () {
      window.history.pushState("test", "Title", "/mobile/new-home.html?idLang="+getIdLangParam());
    	$(".main-menu .sub-main-menu, .second-menu li a").removeClass("active");
      $(this).addClass("active");
  	});
    $('.profil .docsCVsLink').on('click', function () {
      $(".main-menu .sub-main-menu, .second-menu li a").removeClass("active");
	    $(".main-menu .userProfileLink").addClass("active");
	  });
    $('#menu1 .myAccountLink').on('click', function () {
      window.history.pushState("test", "Title", "/mobile/new-home.html?idLang="+getIdLangParam());
      $(".main-menu .sub-main-menu, .second-menu li a").removeClass("active");
	    $(".main-menu .myAccountLink").addClass("active");
	  });
    $('#menu1 .accountSettingsLink, .profil .accountSettingsLink').on('click', function () {
      window.history.pushState("test", "Title", "/mobile/new-home.html?idLang="+getIdLangParam());
      $(".main-menu .sub-main-menu, .second-menu li a").removeClass("active");
	    $(".main-menu .accountSettingsLink").addClass("active");
	  });
    $('#logout, .info-efq li').on('click', function () {
    	$(".main-menu .sub-main-menu, .second-menu li a").removeClass("active");
    });
    //---------- User Favorite Main Menu Candidat -----//
    $('.userFavoritesLink').on('click', function () {
      if (connectedUser) {
        if (userFavoritesController == null) {
          userFavoritesController = new $.FavoriesController(
                  new $.FavoriesModel(), new $.FavoriesView());
        } else
          userFavoritesController.initController();
      } else {
        getStatusNoConnected();
      }
    });
    //---------- User Job Offer Favorite Main Menu -----//
    $('#productsFavLink').click(function () {
      window.history.pushState("test", "Title", "/mobile/new-home.html?idLang="+getIdLangParam());
      $('.favoritesContainers').hide();
      $('#productsFav').show();
      $("#favories .glyphicon").removeClass('active');
      $(this).closest('li').addClass('active');
      $('#productsFav .scroll-pane').jScrollPane({ autoReinitialise: true });
      $('.modal-loading').hide();
    });
    //---------- User Products Favorite Main Menu -----//
    $('#contactsFavLink').click(function () {
      window.history.pushState("test", "Title", "/mobile/new-home.html?idLang="+getIdLangParam());
      $('.favoritesContainers').hide();
      $('#contactsFav').show();
      $("#favories .glyphicon").removeClass('active');
      $(this).closest('li').addClass('active');
      $('#contactsFav .scroll-pane').jScrollPane({ autoReinitialise: true });
      $('.modal-loading').hide();
    });
    //---------- User Cantacts Favorite Main Menu -----//
    $('#contactsFavLink').click(function () {
      window.history.pushState("test", "Title", "/mobile/new-home.html?idLang="+getIdLangParam());
      $('.favoritesContainers').hide();
      $('#contactsFav').show();
      $("#favories .glyphicon").removeClass('active');
      $(this).closest('li').addClass('active');
      $('#contactsFav .scroll-pane').jScrollPane({ autoReinitialise: true });
      $('.modal-loading').hide();
    });
    //---------- User Media Favorite Main Menu -----//
    $('#mediaFavLink').click(function () {
      window.history.pushState("test", "Title", "/mobile/new-home.html?idLang="+getIdLangParam());
      $('.favoritesContainers').hide();
      $('#mediaFav').show();
      $("#favories .glyphicon").removeClass('active');
      $(this).closest('li').addClass('active');
      $('#mediaFav .scroll-pane').jScrollPane({ autoReinitialise: true });
      $('.modal-loading').hide();
    });

    //---------- User Testimony Favorite Main Menu -----//
    $('#testimoniesFavLink').click(function () {
      window.history.pushState("test", "Title", "/mobile/new-home.html?idLang="+getIdLangParam());
      $('.favoritesContainers').hide();
      $('#testimoniesFav').show();
      $("#favories .glyphicon").removeClass('active');
      $(this).closest('li').addClass('active');
      $('#testimoniesFav .scroll-pane').jScrollPane({ autoReinitialise: true });
      $('.modal-loading').hide();
    });
    //---------- User Advice Favorite Main Menu -----//
    $('#advicesFavLink').click(function () {
      window.history.pushState("test", "Title", "/mobile/new-home.html?idLang="+getIdLangParam());
      $('.favoritesContainers').hide();
      $('#advicesFav').show();
      $("#favories .glyphicon").removeClass('active');
      $(this).closest('li').addClass('active');
      $('#advicesFav .scroll-pane').jScrollPane({ autoReinitialise: true });
      $('.modal-loading').hide();
    });
    //---------- User Mon Profile Main Menu Candidat -----//
    $('.accountSettingsLink').on('click', function () {
      window.history.pushState("test", "Title", "/mobile/new-home.html?idLang="+getIdLangParam());
      if (connectedUser) {
        if (myProfilController == null) {
          myProfilController = new $.MyProfilController(new $.MyProfilModel(), new $.MyProfilView());
        }
        myProfilController.initController();
      } else {
        getStatusNoConnected();
      }
    });
    //---------- User Mon Compte Main Menu Candidat -----//
    $('.myAccountLink').on('click', function () {
      window.history.pushState("test", "Title", "/mobile/new-home.html?idLang="+getIdLangParam());
      if (connectedUser) {
        if (myAccountController == null) {
          myAccountController = new $.MyAccountController(new $.MyAccountModel(), new $.MyAccountView());
        }
        myAccountController.initController();
      } else {
        getStatusNoConnected();
      }
    });
    $('.listVisitorsLink').on('click', function () {
      window.history.pushState("test", "Title", "/mobile/new-home.html?idLang="+getIdLangParam());
      if (connectedUser) {
        $('#contentG').removeClass('salle-exposition');
        $('.genericContent').hide();
        $('#content_search_visitor').show();
        $('#page-content').removeClass('no-padding');
        genericSearchController.getView().notifyLaunchSearch('visitors', '');
        $('#data-table-filters .col-sm-12.col-md-3.no-padding >.caret').attr('style', 'display:none;');
        analyticsView.viewStandZone(VISITORS);
      } else {
        getStatusNoConnected();
      }
    });
    //---------- User Ma carriere Main Menu Candidat -----//
    $('.userCareerLink').on('click', function () {
      window.history.pushState("test", "Title", "/mobile/new-home.html?idLang="+getIdLangParam());
      if (connectedUser) {
        if (myCareerController == null) {
          myCareerController = new $.MyCareerController(new $.MyCareerModel(), new $.MyCareerView());
        }
        myCareerController.initController();
      } else {
        getStatusNoConnected();
      }
    });
    //---------- User Question Info Menu Candidat -----//
    $('.userQuestionInfoLink').on('click', function () {
      window.history.pushState("test", "Title", "/mobile/new-home.html?idLang="+getIdLangParam());
      if (connectedUser) {
        if (myQuestionInfoController == null) {
        	myQuestionInfoController = new $.QuestionInfoController(new $.QuestionInfoModel(), new $.QuestionInfoView());
        }
        myQuestionInfoController.initController();
      } else {
        getStatusNoConnected();
      }
    });

    //---------- User Boite de reception Main Menu Candidat -----//
    $('.userMessagesLink').on('click', function () {
      window.history.pushState("test", "Title", "/mobile/new-home.html?idLang="+getIdLangParam());
      if (connectedUser) {
        if (inboxController == null) {
          inboxController = new $.InboxController(new $.InboxModel(), new $.InboxView());
        }
        inboxController.initController();
      } else {
        getStatusNoConnected();
      }
    });

    //---------- Candidat logout -----//
    this.logoutAction = function () {
      $('#logout').on('click', function () {
        that.notifyCandidateLogout();
        return false;
      });
    };
    //---------- Candidat logout -----//
    this.authentificate = function () {
        $("body").addClass("loading");
        if (!regexEmail.test($('#recipient_name').val())) {
          $("body").removeClass("loading");
          $('.error-login').removeClass('hide');
          return false;
        }
        if ($('#recipient_password').val() == '') {
          $("body").removeClass("loading");
          $('.error-login').removeClass('hide');
          return false;
        }
        $('.error-login').addClass('hide');
        var login = $('#recipient_name').val();
        login = htmlEncode($('#recipient_name').val());
        var password = $('#recipient_password').val();
        password = $.sha1(password);
        that.notifyCandidateLogin(login, password);
        return false;
    };
    //------- Connexion Action -------//
    this.connexionAction = function () {
      $('#loginButton').unbind('click').bind('click', function(e){
      	that.authentificate();
      });
      $('#recipient_name,#recipient_password').unbind('keydown').bind('keydown', function(e){
      	if (e.keyCode == 13) {
      		that.authentificate();
				}
      });
    };
    
    //------- Slider ---------------//
    this.slider = function () {
      /*slide sponssor*/
      var largeur_fenetre = $(window).width();

      if ($(".sliderG").length > 0) {
        if (largeur_fenetre <= 980) {
          $('.slider').bxSlider({
            slideWidth: 123,
            minSlides: 2,
            maxSlides: 2,
            auto: true,
            slideMargin: 19,
            controls: false,
            pager: false
          });
        } else {
          $('.slider').bxSlider({
            slideWidth: 123,
            minSlides: 2,
            maxSlides: 3,
            auto: true,
            slideMargin: 19,
            controls: false,
            pager: false
          });
        }

        $(window).on('resize', function () {
          if ($(".scroll-pane").length > 0) {
            $('.scroll-pane').jScrollPane({ autoReinitialise: true });
          }
          var win = $(this); //this = window
//          if (win.width() <= 980) {
//            slider.reloadSlider({
//              slideWidth: 123,
//              minSlides: 2,
//              maxSlides: 2,
//              auto: true,
//              slideMargin: 19,
//              controls: false,
//              pager: false
//            });
//          } else {
//            slider.reloadSlider({
//              slideWidth: 123,
//              minSlides: 2,
//              maxSlides: 3,
//              auto: true,
//              slideMargin: 19,
//              controls: false,
//              pager: false
//            });
//          }
        });
      }
    };
    //------- End Slider ---------------//
    this.CandidatStatusLoged = function (candidatXml) {
      $('#modal-sign-in').modal('hide');
      var candidat = new $.Candidate(candidatXml);
      candidatGlobal = new $.Candidate(candidatXml);
      getUserFavorites();
      $('.home-buttons').addClass('hide');
      $('a.candidat-fullname').html(candidat.userProfile.firstName + ' ' + candidat.userProfile.secondName + '<span class="caret"></span>');
      $('#container header').removeClass('non-connecter');
      $('#container header').addClass('connecter');
      $('.main-menu-items-user,.main-menu-container-user,.main-menu-disconenct').removeClass('hide');
      $('p.candidat-fullname').html(candidat.userProfile.firstName + ' ' + candidat.userProfile.secondName);
      $('.main-menu #no-conneted-link').removeClass('no-conneted');
      $('.candidat-avatar').attr('src', candidat.userProfile.avatar);
      $("body").removeClass("loading");
      connectedUser = true;
      analyticsView.candidateIsConnected(candidat);
      $.cookie('token', candidat.token);
      localStorage.setItem('token', candidat.token);
      if (notificationsController == null) {
        notificationsController = new $.NotificationController(
                new $.NotificationModel(), new $.NotificationView());
      } else {
        notificationsController.initController();
      }
      sessionExtenderInterval = setInterval(sessionExtender, 10*60*1000);
      //$('.icon-tchat').click();
    };
    this.CandidatStatusErrorLoged = function (xhr) {
      $("body").removeClass("loading");
      if (xhr.status !== 401) {
        getStatusNoConnected();
        $('.error-login').removeClass('hide');
      	if (xhr.status == 412){
	        $('.error-login').text("Votre compte n'a pas encore été approuvé.");
	      }else {
	        $('.error-login').text(EMAILLOGINERRORLABEL);
        }
      }
    };
    this.ForgotPassword = function () {
      $('.pass-forgotten').on('click', function () {
        $('#modal-sign-in').modal('hide');
        $('#modal-forgot-password').modal('show');

      });
    };
    this.forgotPasswordAction = function () {
      $('#forgotPasswordButton').on('click', function () {
        if (!regexEmail.test($('#forgot-password-email').val())) {
          $('.error-forgot-password').removeClass('hide');
          return false;
        }
        $('.error-forgot-password').addClass('hide');
        var login = htmlEncode($('#forgot-password-email').val());
        that.notifyCandidateForgotPassword(login);
        return false;
        //return false;
      });
    };
    //-------- Load Session ------------//
    this.displayloadSession = function (loadSession) {
      that.CandidatStatusLoged(loadSession);
    };
    //----------- Candidat login --------//
    this.displaycandidatLoginSuccess = function (candidatXml) {
      that.CandidatStatusLoged(candidatXml);
    };
    this.displaycandidatLoginError = function (xhr) {
      that.CandidatStatusErrorLoged(xhr);
    };
    //----------- Candidat Forgot Password --------//
    this.displaycandidateForgotPasswordSuccess = function (candidatXml) {
      $('.error-forgot-password').addClass('hide');
      $('#modal-forgot-password').modal('hide');
      $('#modal-forgot-password-Success').modal('show');
    };
    this.displaycandidateForgotPasswordError = function (xhr) {
      $('.error-forgot-password').removeClass('hide');
    };
    //------------- Candidat Logout --------------//
    this.displaycandidatLogout = function (xhr) {
      connectedUser = false;
      getStatusNoConnected(false);
      $.cookie("token", null);
      localStorage.setItem('token', null);
      notificationGlobalCounter = 0;
      //document.title = "Salon en ligne du 04 au 7 Avril 2016";
      clearInterval(sessionExtenderInterval);
      $(".icon-format1").click();
    };
    //----------- Notify Candidat Login --------//
    this.notifyCandidateLogin = function (login, password) {
      $.each(listeners, function (i) {
        listeners[i].candidateLogin(login, password);
      });
    };
    //----------- Notify Candidat Forgot Password --------//
    this.notifyCandidateForgotPassword = function (login) {
      $.each(listeners, function (i) {
        listeners[i].candidateForgotPassword(login);
      });
    };
    //----------- Notify Candidat Logout --------//
    this.notifyCandidateLogout = function () {
      $.each(listeners, function (i) {
        listeners[i].candidateLogout();
      });
    };
    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  HomePageViewListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }
});
