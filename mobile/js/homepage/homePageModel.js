jQuery.extend({
  HomePageModel: function () {
    var that = this;
    var listeners = new Array();
    this.initModel = function () {
    this.fillFunctionCandidates();
	  this.fillActivitySectors();
	  this.fillStandSectors();
	  this.fillAnneesExperiences();
	  this.fillRegions();
	  this.fillJobSought();
      this.fillAreaExpertise();
      // this.fillLanguageLevel();
      that.loadSession();
    };
    
    //------------- Function Candidates -----------//
		this.fillFunctionCandidatesSuccess = function(xml) {
			var functionCandidates = [];
			$(xml).find('functionCandidateDTO').each(function() {
				var functionCandidate = new $.FunctionCandidate($(this));
				functionCandidates.push(functionCandidate);
			});
			that.notifyLoadFunctionCandidates(functionCandidates);
		};
		
		this.fillFunctionCandidatesError = function(xhr, status, error) {
			isSessionExpired(xhr.responseText);
		};
		
		this.fillFunctionCandidates = function() {
			genericAjaxCall('GET', staticVars["urlBackEndCandidate"]
					+ 'functionCandidate?idLang=' + getIdLangParam(),
					'application/xml', 'xml', '',
					that.fillFunctionCandidatesSuccess,
					that.fillFunctionCandidatesError);
		};
		
		//------------- Activity Sector -----------//
	    this.fillStandSectorsSuccess = function(xml) {
			var standSectors = [];
			$(xml).find('sectorDTO').each(function() {
				var standSector = new $.StandSector($(this));
				standSectors.push(standSector);
			});
			that.notifyLoadStandSectors(standSectors);
		};
		
		this.fillStandSectorsError = function(xhr, status, error) {
			isSessionExpired(xhr.responseText);
		};
		
		this.fillStandSectors = function() {
			genericAjaxCall('GET', staticVars["urlBackEndCandidate"]
					+ 'sector?idLang=' + getIdLangParam(),
					'application/xml', 'xml', '',
					that.fillStandSectorsSuccess,
					that.fillStandSectorsError);
		};

		//------------- Activity Sector -----------//
	    this.fillActivitySectorsSuccess = function(xml) {
			var activitySectors = [];
			$(xml).find('secteurActDTO').each(function() {
				var secteurAct = new $.SecteurAct($(this));
				activitySectors.push(secteurAct);
			});
			that.notifyLoadActivitySectors(activitySectors);
		};
		
		this.fillActivitySectorsError = function(xhr, status, error) {
			isSessionExpired(xhr.responseText);
		};
		
		this.fillActivitySectors = function() {
			genericAjaxCall('GET', staticVars["urlBackEndCandidate"]
					+ 'secteuract?idLang=' + getIdLangParam(),
					'application/xml', 'xml', '',
					that.fillActivitySectorsSuccess,
					that.fillActivitySectorsError);
		};
		

		// ------------- Area Expertise -----------//
		this.fillJobSoughtSuccess = function(xml) {
			var jobSoughts = [];
			$(xml).find('jobSoughtDTO').each(function() {
				var jobSought = new $.JobSought($(this));
				jobSoughts.push(jobSought);
			});
			that.notifyLoadJobSought(jobSoughts);
		};
		
		this.fillJobSoughtError = function(xhr, status, error) {
			isSessionExpired(xhr.responseText);
		};

		this.fillJobSought = function() {
			genericAjaxCall('GET', staticVars["urlBackEndCandidate"]
					+ 'jobSought?idLang=' + getIdLangParam(),
					'application/xml', 'xml', '', that.fillJobSoughtSuccess,
					that.fillJobSoughtError);
		};
		

		// ------------- Annees Experiences -----------//
		this.fillAnneesExperiencesSuccess = function(xml) {
			var anneesExperiences = [];
			$(xml).find('experienceYearsDTO').each(function() {
				var anneesExperience = new $.ExperienceYears($(this));
				anneesExperiences.push(anneesExperience);
			});
			that.notifyLoadAnneesExperiences(anneesExperiences);
		};
		
		this.fillAnneesExperiencesError = function(xhr, status, error) {
			isSessionExpired(xhr.responseText);
		};
		
		this.fillAnneesExperiences = function() {
			genericAjaxCall('GET', staticVars["urlBackEndCandidate"]
					+ 'experienceYears?idLang=' + getIdLangParam(),
					'application/xml', 'xml', '',
					that.fillAnneesExperiencesSuccess,
					that.fillAnneesExperiencesError);
		};

		// ------------- Regions List -----------//
		this.fillRegionsSuccess = function(xml) {
			var regions = [];
			$(xml).find('regionDTO').each(function() {
				var region = new $.Region($(this));
				regions.push(region);
			});
			that.notifyLoadRegions(regions);
		};
		this.fillRegionsError = function(xhr, status, error) {
			isSessionExpired(xhr.responseText);
		};
		
		this.fillRegions = function() {
			genericAjaxCall('GET', staticVars["urlBackEndCandidate"]
					+ 'region?idLang=' + getIdLangParam(), 'application/xml',
					'xml', '', that.fillRegionsSuccess, that.fillRegionsError);
		};
		
		//------------- AreaExpertise -----------//
	    this.fillAreaExpertiseSuccess = function (xml) {
	      var areaExpertises = [];
	      $(xml).find('areaExpertiseDTO').each(function () {
	        var areaExpertise = new $.AreaExpertise($(this));
	        areaExpertises.push(areaExpertise);
	      });
	      that.notifyAreaExpertise(areaExpertises);
	    };
	    this.fillAreaExpertiseError = function (xhr, status, error) {
	      isSessionExpired(xhr.responseText);
	    };
		// AreaExpertise page scripts
	    this.fillAreaExpertise = function () {
	      genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'areaExpertise?idLang=' + getIdLangParam(),
	              'application/xml', 'xml', '', that.fillAreaExpertiseSuccess,
	              that.fillAreaExpertiseError);
	    };
	    this.notifyAreaExpertise = function (areaExpertise) {
	      $.each(listeners, function (i) {
	        listeners[i].areaExpertise(areaExpertise);
	      });
	    };
		
    //------------- Load Session -----------//
    this.loadSessionSuccess = function (xml) {
      that.notifyLoadSession(xml);
    };

    this.loadSessionError = function (xhr, status, error) {
//      that.notifyLoadSession(xhr);
    };
    this.loadSession = function () {
      genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'candidate/personalInfos',
              'application/xml', 'xml', '', that.loadSessionSuccess,
              that.loadSessionError);
    };
    //-------------------------------------//
    //------------- Candidat Create Session -----------//
    this.candidateLoginSuccess = function (xml) {
      that.notifyCandidatLoginSuccess(xml);
    };

    this.candidateLoginError = function (xhr, status, error) {
      that.notifyCandidatLoginError(xhr);
    };
    this.candidateLogin = function (login, password) {
      genericAjaxCall('POST', staticVars["urlBackEndCandidate"] + 'candidate/login?login=' + login + '&pwd=' + password,
              'application/xml', 'xml', '', that.candidateLoginSuccess,
              that.candidateLoginError);
    };
    //-------------------------------------//
    //------------- Candidat Forgo tPassword -----------//
    this.candidateForgotPasswordSuccess = function (xml) {
      that.notifyCandidateForgotPasswordSuccess(xml);
    };

    this.candidateForgotPasswordError = function (xhr, status, error) {
      that.notifyCandidateForgotPasswordError(xhr);
    };
    this.candidateForgotPassword = function (login) {
      genericAjaxCall('POST', staticVars["urlBackEndCandidate"] + 'candidate/forgotPassword/sendEmail?email=' + login
              + '&idLang=' + getIdLangParam(), 'application/xml', 'xml', '', that.candidateForgotPasswordSuccess,
              that.candidateForgotPasswordError);
    };
    //-------------------------------------//
    //------------- Candidat Logout -----------//
    this.candidateLogoutSuccess = function (xml) {
      that.notifyLogoutResult();
    };
    this.candidateLogoutError = function (xhr, status, error) {
      isSessionExpired(xhr.responseText);
    };

    this.candidateLogout = function () {
      genericAjaxCall('POST', staticVars["urlBackEndCandidate"] + 'candidate/logout', 'application/xml', 'xml', '', that.candidateLogoutSuccess, that.candidateLogoutError);
    };
    //-------------------------------------//
    
    this.notifyLoadFunctionCandidates = function(functionCandidates){
		$.each(listeners, function(i) {
			listeners[i].notifyLoadFunctionCandidates(functionCandidates);
		});
	};
	this.notifyLoadActivitySectors = function(activitySectors){
		$.each(listeners, function(i) {
			listeners[i].notifyLoadActivitySectors(activitySectors);
		});
	};
	this.notifyLoadStandSectors = function(standSectors){
		$.each(listeners, function(i) {
			listeners[i].notifyLoadStandSectors(standSectors);
		});
	};
	this.notifyLoadJobSought = function(jobSoughts){
		$.each(listeners, function(i) {
			listeners[i].notifyLoadJobSought(jobSoughts);
		});
	};
	this.notifyLoadAnneesExperiences = function(anneesExperiences){
		$.each(listeners, function(i) {
			listeners[i].notifyLoadAnneesExperiences(anneesExperiences);
		});
	};
	this.notifyLoadRegions = function(regions) {
		$.each(listeners, function(i) {
			listeners[i].notifyLoadRegions(regions);
		});
	};
	
    this.notifyLoadSession = function (loadSession) {
      $.each(listeners, function (i) {
        listeners[i].loadSession(loadSession);
      });
    };
    this.notifyCandidatLoginSuccess = function (candidatXml) {
      $.each(listeners, function (i) {
        listeners[i].candidatLoginSuccess(candidatXml);
      });
    };
    this.notifyCandidatLoginError = function (xhr) {
      $.each(listeners, function (i) {
        listeners[i].candidatLoginError(xhr);
      });
    };
    this.notifyCandidateForgotPasswordSuccess = function (candidatXml) {
      $.each(listeners, function (i) {
        listeners[i].candidateForgotPasswordSuccess(candidatXml);
      });
    };
    this.notifyCandidateForgotPasswordError = function (xhr) {
      $.each(listeners, function (i) {
        listeners[i].candidateForgotPasswordError(xhr);
      });
    };
    this.notifyLogoutResult = function (xhr) {
      $.each(listeners, function (i) {
        listeners[i].candidatLogout(xhr);
      });
    };
    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  HomePageModelListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }
});

