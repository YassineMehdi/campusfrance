jQuery.extend({
  SendMessageController: function (model, view) {
    var that = this;
    /**
     * listen to the view
     */

    var vlist = $.ViewListener({
      sendMessage : function(standId, message) {
            model.sendMessage(standId, message);
      },
      sendVisitedMessage : function(standId, message) {
          model.sendVisitedMessage(standId, message);
      },
    });

    view.addListener(vlist);

    /**
     * listen to the model
     */
    var mlist = $.ModelListener({
    	
    });

    model.addListener(mlist);

    this.initController = function () {
      view.initView();
      model.initModel();
    };
    that.initController();
  }
});