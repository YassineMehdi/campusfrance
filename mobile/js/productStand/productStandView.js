jQuery.extend({
  ProductStandManage: function (product, view) {
    var productId = product.getProductId();

    var isFavorite = factoryFavoriteExist(productId, FavoriteEnum.PRODUCT);
    var displayDeleteFavorite = "block";
    var displayAddFavorite = "block";
    if (isFavorite)
      displayAddFavorite = "none";
    else
      displayDeleteFavorite = "none";
    var productImage = product.getProductImage();
    if(productImage == "" || productImage == "#" || productImage == undefined) 
    	productImage = product.getStand().getEnterprise().getLogo();
    var dom = $('<div class="row no-margin info-p">'
            + '<div class="cel-info">'
            + '<div class="logo col-md-12 col-sm-12 no-padding">'
            + '<img src="' + productImage + '" alt="">'
            + '</div>'
            + ' <h3>' + product.getProductTitle() + '</h3>'
            + '<ul>'
            + '<li>' + substringCustom(formattingLongWords(product.getProductDescription(), 40),100) + '</li>'
            + '</ul>'
            + '</div>'
            + '<div class="cel-btn ">'
            + '<a class="btn col-md-11 col-sm-11 active addProductToFavorite .addProductToFavoriteButton_' + productId + '" style="display:' + displayAddFavorite + '"><i class="glyphicon glyphicon-star" aria-hidden="true"></i><span class="productActionLabel">'+ADDTOFAVORITELABEL+'</span></a><a class="btn col-md-11 col-sm-11 active deleteProductFromFavorite  .deleteProductToFavoriteButton_' + productId + '" style="background-color: #b3081b;display:' + displayDeleteFavorite + '"><i class="glyphicon glyphicon-star" aria-hidden="true"></i><span class="productActionLabel">'+DELETEFROMFAVORITELABEL+'</span></a>'
//            + '<a href="#" class="btn col-md-11 col-sm-11 active"><i class="glyphicon glyphicon-star" aria-hidden="true"></i> Favoris</a>'
            + '<a href="#" class="btn col-md-11 col-sm-11 green viewLabel" id="viewProductStand"><i class="fa fa-eye" aria-hidden="true"></i><span class="productActionLabel">'+VIEWLABEL+'</span></a>'
            + '<a href="#" class="btn col-md-11 col-sm-11 blue requestInfoLabel" id="infoRequest_' + productId + '"><i class="fa fa-info-circle" aria-hidden="true"></i><span class="productActionLabel">'+INFOREQUESTLABEL+'</span></a>'
            + '</div>'
            + '</div>');

    this.refresh = function () {

      var addProductFavoriteLink = dom.find(".addProductToFavorite");
      var deleteProductFavoriteLink = dom.find(".deleteProductFromFavorite");
      var viewProductStand = dom.find("#viewProductStand");
      var requestInfoProductStand = dom.find("#infoRequest_"+productId);

      addProductFavoriteLink.click(function () {
        if (connectedUser) {
          var idEntity = productId;
          var componentName = FavoriteEnum.PRODUCT;
          var favoriteXml = factoryXMLFavory(idEntity, componentName);
          addFavorite(favoriteXml);
          $(this).hide();
          $(this).closest('div').find('.deleteProductFromFavorite').show();
          $('.addProductToFavoriteButton_' + productId).hide();
          $('.deleteProductFromFavoriteButton_' + productId).show();
        } else {
          getStatusNoConnected();
        }
      });

      deleteProductFavoriteLink.click(function () {
        var idEntity = productId;
        var componentName = FavoriteEnum.PRODUCT;
        var favoriteXml = factoryXMLFavory(idEntity, componentName);
        deleteFavorite(favoriteXml);
        $(this).hide();
        $(this).closest('div').find('.addProductToFavorite').show();
        $('.addProductToFavoriteButton_' + productId).show();
        $('.deleteProductFromFavoriteButton_' + productId).hide();
      });

      viewProductStand.click(function () {
        $('.modal-loading').show();
        var productApercuManage = new $.ProductApercuManage(product, view);
        $('#modal-product-apercu #info-produit-apercu').html('<div id="apercu" class="scroll-pane"></div>')
        		.find('#apercu').html(productApercuManage.getDOM());
        $('#modal-product-apercu').modal('show');
        $('#info-produit .scroll-pane').jScrollPane({ autoReinitialise: true });
        $('.modal-loading').hide();
        analyticsView.viewStandZoneById(PRODUCT+'/'+PRODUCT_ID, product.getProductId(), product.getStand().getIdStande());
      });      

      requestInfoProductStand.click(function () {
        $('#documents-product-list').text("");
        $('#modal-send-request-product .file-path').val("");
        $('#modal-send-request-product textarea').val("");
        var productId = product.getProductId();
        if (connectedUser) {
          $('#modal-send-request-product').modal('show');
          //productStandController.getView().notifyGetJobOffersList(productId);
          
          $('#submit-document-product').removeClass('productId_'+productId);
          $('#submit-document-product').addClass('productId_'+productId);
          $('.productId_'+productId).unbind();
          $('.productId_'+productId).bind('click', function () {
            $('.modal-loading').show();
            if ($('#documents-product-list').text().replace(/\s/g, '') != "") {
              var url = $('#documents-product-list').text();
              var messageToSend = $('#modal-send-request-product textarea').val();

              var questionInfo = new $.QuestionInfo();
              questionInfo.setQuestionInfoTitle(product.getProductTitle());
              questionInfo.setQuestion(messageToSend);
              questionInfo.setQuestionInfoUrl(url);
              questionInfo.setProduct(product);

              genericSearchController.getView().notifySendProduction(questionInfo);              
              return false;
            } else {
              swal(SENDERRORLABEL, CHOOSEDOCUMENTLABEL, "error");
              $('.modal-loading').hide();
              return false;
            }
          });
        } else {
          $('.modal-loading').hide();
          getStatusNoConnected();
        }
        analyticsView.viewStandZone(QUESTION_INFORMATION);
      });
    };
    this.getDOM = function () { //retourne une ligne HTML <a .....
      return dom;
    };
    this.refresh();
  },
  ProductStandView: function () {
    var listeners = new Array();
    var that = this;

    this.displayProductStand = function (listProducts) {
      $("#infoStandProducts").empty();
      if (listProducts != null && listProducts.length > 0) {
        var products = [];
        $("#infoStandProducts").append('<div class="row info-stand-product info-stand-apercu no-margin scroll-pane" id="info-stand-product-list"></div>');
        for (index in listProducts) {
          if (listProducts[index] != undefined) {
            products.push(listProducts[index]);
            var productsContainerList = new $.ProductStandManage(listProducts[index], that);
            var docDom = productsContainerList.getDOM();
            $('#info-stand-product-list').append(docDom);
          }
        }
      } else {
        $('#infoStandProducts').append('<p class="empty noProductYetLabel">'+NOPRODUCTLABEL+'</p>');
      }
      $('#modal-product .scroll-pane').jScrollPane({ autoReinitialise: true });
      $('.modal-loading').hide();
    };
    
    this.initView = function () {
      var standId = null;
      if ($('#product').attr('standId') == undefined) {
        standId = 'standId=7';
      } else {
        standId = $('#product').attr('standId');
      }
      $('#product').unbind().bind('click', function () {
        if(!($("#modal-product").data('bs.modal') || {}).isShown) {
	      	factoryActivateFaIcon(this);
	        $('.modal').modal('hide');
	        $('.modal-loading').show();
	        $('#info-stand-product-list').empty();
	        that.notifyProductStand(standId);
	        $('#modal-product').modal('show');
        }
      });
      $('#inputFileProduct').fileupload(getDocumentUpload());
    };
    this.notifyProductStand = function (standId) {
      $.each(listeners, function (i) {
        listeners[i].launchProductStand(standId);
      });
    };

    this.notifySendDocsCVs = function (url, messageToSend, jobId, jobTitle) {
      $.each(listeners, function (i) {
        listeners[i].sendDeposer_DocumentsStand(url, messageToSend, jobId, jobTitle);
      });
    };

    this.notifyGetJobOffersList = function (jobOfferId) {
      $.each(listeners, function (i) {
        listeners[i].notifyGetJobOffersList(jobOfferId);
      });
    };

    this.notifyLoadDocsCVs = function () {
      $('.modal-loading').show();
      $.each(listeners, function (i) {
        listeners[i].LoadDocs_CVs();
      });
    };
    this.addListener = function (list) {
      listeners.push(list);
    };

  },
  ViewListener : function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }
});


