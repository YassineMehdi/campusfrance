jQuery.extend({
    ProductStandModel: function () {
        var listeners = new Array();
        var that = this;
        this.initModel = function () {
        };

        this.getProductStandError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.getProductStand = function (standId) {
            listProductsCache = [];
            genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'product?' + standId + '&idLang=' + getIdLangParam(), 'application/xml', 'xml', '', function (xml) {
                listProductsCache = new Array();
                $(xml).find("productDTO").each(function () {
                    var product = new $.Product($(this));
                    listProductsCache[product.getProductId()] = product;
                });
                that.notifyLoadProductStand(listProductsCache);
            }, that.getProductStandError);
        };

        this.notifyLoadProductStand = function (listProducts) {
            $.each(listeners, function (i) {
                listeners[i].loadProductStand(listProducts);
            });
        };

        this.addListener = function (list) {
            listeners.push(list);
        };
    },
    ModelListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }

});
