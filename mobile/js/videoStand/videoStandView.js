jQuery.extend({
  VideoStandManage: function (video, view) {
    var videoId = video.getAllFilesId();
    var isFavorite = factoryFavoriteExist(video.getAllFilesId(), FavoriteEnum.MEDIA);
    var displayDeleteFavorite = "block";
    var displayAddFavorite = "block";
    if (isFavorite)
      displayAddFavorite = "none";
    else
      displayDeleteFavorite = "none";

    var dom = $('<li>'
            + '<div class="favoris">'
            + '<ul>'
            + '<li><a class="icon-etoile addVideoToFavorite addVideoRhFavoriteId_' + videoId + '" style="display:' + displayAddFavorite + '"></a><a class="icon-delete deleteVideoFromFavorite deleteVideoRhFavoriteId_' + videoId + '" style="display:' + displayDeleteFavorite + '"></a></li>'
            + '<li><a class="icon-galerie openCurrentVideo"></a></li>'
            + '</ul>'
            + '</div>'
            + '<div class="visuel">'
            + '<a class="openCurrentVideo">'
            + '<img src="img/visuel2.jpg" alt="">'
            + '</a>'
            + '</div>'
            + '<div>' + htmlEncode(substringCustom(formattingLongWords(video.getTitle(), 20), 60)) + '</div>'
            + '</li>');

    this.refresh = function () {
      dom.find(".openCurrentVideo").unbind();
      dom.find(".openCurrentVideo").bind('click', function () {
        $(".agrandir-video").bind('click', function () {
          $('#modal-document-lecteur-big').modal();
          openVideo(video, $("#modal-document-lecteur-big").find('.ad-image-wrapper'), $("#modal-document-lecteur-big").find('.modal-title'));
        });
        view.openVideo(video);
      });

      var addVideoFavoriteLink = dom.find(".addVideoToFavorite");
      var deleteVideoFavoriteLink = dom.find(".deleteVideoFromFavorite");

      addVideoFavoriteLink.click(function () {
        if (connectedUser) {
          var favoriteXml = factoryXMLFavory(video.getAllFilesId(), FavoriteEnum.MEDIA);
          addFavorite(favoriteXml);
          $(this).hide();
          $(this).closest('div').find('.deleteVideoFromFavorite').show();
          $('.addVideoRHToFavoriteButton_' + videoId).hide();
          $('.deleteVideoRHFromFavoriteButton_' + videoId).show();
        } else {
          getStatusNoConnected();
        }
      });

      deleteVideoFavoriteLink.click(function () {
        var favoriteXml = factoryXMLFavory(video.getAllFilesId(), FavoriteEnum.MEDIA);
        deleteFavorite(favoriteXml);
        $(this).hide();
        $(this).closest('div').find('.addVideoToFavorite').show();
        $('.addVideoRHToFavoriteButton_' + videoId).show();
        $('.deleteVideoRHFromFavoriteButton_' + videoId).hide();
      });
    };
    this.getDOM = function () { //retourne une ligne HTML <a .....
      return dom;
    };
    this.refresh();
  },
  VideoStandView: function () {
    var listeners = new Array();
    var that = this;
    var galleriesVideos = null;
    var infoStandVideoHandler = null;
    var galleryVideoHandler = null;
    var galleryTitleHandler = null;
    var videosContentHandler = null;
    $('#modal-video, #modal-video-reception').on('hidden.bs.modal', function () {
    	$(this).find('.info-stand-video').empty();
    });
    this.initView = function (standId) {
      var standId = $('#temoignage').attr('standId');
      //$('#modal-video .info-stand-video, #modal-video-reception .info-stand-video').empty();
      $('#stand-video-link').unbind('click').bind('click', function () {
        infoStandVideoHandler =  $('#modal-video .info-stand-video');
        that.addModalVideoListener('#modal-video', standId);
      });
      $('#stand-video-recep-link').unbind('click').bind('click', function () {
        infoStandVideoHandler =  $('#modal-video-reception .info-stand-video');
        that.addModalVideoListener('#modal-video-reception', standId);
      });

      $('#modal-document-lecteur-big').find('.close').unbind('click').bind('click', function () {
        $('#modal-document-lecteur-big').find('.ad-image-wrapper').empty();
        that.openVideoByIndex(0);
      });
    };
    this.addModalVideoListener = function(selector, standId){
    	if(!($(selector).data('bs.modal') || {}).isShown) {
        factoryActivateFaIcon(this);
        $('.modal-loading').show();
        $('.modal').modal('hide');
        $(selector).modal('show');
        that.notifyVideoStand(standId);
      }
    };
    this.displayVideoStand = function (videos) {
      galleriesVideos = videos;
      if (videos != null && Object.keys(videos).length > 0) {
    	  infoStandVideoHandler.html('<div class="visuelG col-md-8 col-sm-8 no-padding">'
	            +'<div id="galleryVideo">'
		            +'<div class="ad-image-wrapper">'
		            +'</div>'
	            +'</div>'
            +'</div>'
            +'<div class="listing scroll-pane col-md-3 col-sm-3">'
	            +'<ul class="ul-video">'
	            +'</ul>'
            +'</div>');
    	  galleryTitleHandler = $('.video-title-head');
    	  galleryVideoHandler = infoStandVideoHandler.find('.ad-image-wrapper');
    	  videosContentHandler = infoStandVideoHandler.find('.ul-video');
      	var videoStandManage = null;
      	for (var index in videos) {
      		videoStandManage = new $.VideoStandManage(videos[index], that);
    			videosContentHandler.append(videoStandManage.getDOM());
      	}
      	that.openVideoByIndex(0);
      	infoStandVideoHandler.find('.scroll-pane').jScrollPane({ autoReinitialise: true });
      } else {
      	infoStandVideoHandler.html('<p class="empty">'+NOVIDEOLABEL+'</p>');
     	}
      $('.modal-loading').hide();
    };
    this.openVideoByIndex = function (index) {
      if(galleriesVideos != null){
        $(".agrandir-video").bind('click', function () {
          $('#modal-document-lecteur-big').modal();
          openVideo(galleriesVideos[index], $("#modal-document-lecteur-big").find('.ad-image-wrapper'), $("#modal-document-lecteur-big").find('.modal-title'));
        });
	      that.openVideo(galleriesVideos[index]);
      }
    };
    this.openVideo = function (video) {
  	  if(video != null){
  	  	openVideo(video, galleryVideoHandler, galleryTitleHandler);
  	  }
    };
    this.notifyVideoStand = function (standId) {
      $.each(listeners, function (i) {
        listeners[i].launchVideoStand(standId);
      });
    };

    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  ViewListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }
});


