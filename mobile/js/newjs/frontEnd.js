/*
 * this file contains all utils functions used in the frontend candidate
 */

// test if session expired
function isSessionExpired(response) {
  if (compareToLowerString(response, "nosession") !== -1) {
    chat_loaded = false;
    $.cookie('connected', "false");
    localStorage.setItem('connected', null);
    //window.location.reload();
  }
}
function traduct() {
  $(xmlLanguage).find("component").each(function () {
    var itemId = $(this).attr('id');
    var itemValue = $(this).attr('value');
    $("#" + itemId + ":not(input, textarea)").text(itemValue);
    $("." + itemId + ":not(input, textarea)").text(itemValue);
    $('input.' + itemId).attr('placeholder', itemValue);
    $('textarea.' + itemId).attr('placeholder', itemValue);
    //$('input.' + itemId).attr('value', itemValue);
    $('a.' + itemId).attr('title', itemValue);
    $('#'+itemId+'Steps').attr('data-intro', itemValue);
  });
  $('#loadingEds').hide();
}

function traductLabel(label) {
  return $(xmlLanguage).find("[id=" + label + "]").attr('value');
}

function traductAllLabel() {
  STANDCHATNOTDISPO = traductLabel("standChatNotDispo");
  TESTIMONIESLABEL = traductLabel("testimoniesLabel");
  LOGINNOTVALIDLABEL = traductLabel("loginNotValidLabel");
  ACTIVATACCOUNTLABEL = traductLabel("activatAccountLabel");
  YESLABEL = traductLabel("yesLabel");
  EXHIBITIONHALLLABEL = traductLabel("exhibitionHallLabel");
  ADDTOFAVORITELABEL = traductLabel("addToFavoriteLabel");
  DELETEFROMFAVORITELABEL = traductLabel("deleteFromFavoriteLabel");
  CANTONLABEL = traductLabel("cantonLabel");
  WORKTYPELABEL = traductLabel("workTypeLabel");
  TYPELABEL = traductLabel("typeLabel");
  STUDYLEVELLABEL = traductLabel("studyLevelLabel");
  FUNCTIONLABEL = traductLabel("functionLabel");
  WORKPERMITLABEL = traductLabel("workPermitLabel");
  EXPERIENCEYEARSLABEL = traductLabel("experienceYearsLabel");
  ACTIVITYSECTORSLABEL = traductLabel("activitySectorsLabel");
  VIEWLABEL = traductLabel("viewLabel");
  BEGINLABEL = traductLabel("beginLabel");
  ANSWEREDLABEL = traductLabel("answeredLabel");
  ENTERPRISELABEL = traductLabel("enterpriseLabel");
  ENTERPRISESLABEL = traductLabel("enterprisesLabel");
  JOBTITLELABEL = traductLabel("jobTitleLabel");
  ACTIONSLABEL = traductLabel("actionsLabel");
  VISITSTANDLABEL = traductLabel("visitStandLabel");
  CHARGEMENTLABEL = traductLabel("loadingLabel");
  CHARGEMENT = CHARGEMENTLABEL;
  STANDLABEL = traductLabel("standLabel");
  STANDSLABEL = traductLabel("standsLabel");
  CONNECTEDLABEL = traductLabel("connectedLabel");
  TITLELABEL = traductLabel("titleLabel");
  DATELABEL = traductLabel("dateLabel");
  PERIODLABEL = traductLabel("periodLabel");
  DOCUMENTSLABEL = traductLabel("documentsLabel");
  PUBLICCHATLABEL = traductLabel("publicChatLabel");
  VIDEOSLABEL = traductLabel("videosLabel");
  SKILLSLABEL = traductLabel("skillsLabel");
  WORKTYPELABEL = traductLabel("workTypeLabel");
  APPLYLABEL = traductLabel("applyLabel");
  RETURNLABEL = traductLabel("returnLabel");
  USERNAMELABEL = traductLabel("userNameLabel");
  FIRSTNAMELABEL = traductLabel("firstNameLabel");
  LASTNAMELABEL = traductLabel("nameLabel");
  EMAILLABEL = traductLabel("emailLabel");
  PASSWORDLABEL = traductLabel("passwordLabel");
  SEARCHLABEL = traductLabel("searchLabel");
  CONNECTEDSTANDSLABEL = traductLabel("connectedStandsLabel");
  SEARCHBYSECTORLABEL = traductLabel("searchBySectorLabel");
  CONNECTTOCHATLABEL = traductLabel("connectToChatLabel");
  REDUCECHATLABEL = traductLabel("reduceChatLabel");
  REGISTERLABEL = traductLabel("registerLabel");
  MYACCOUNTLABEL = traductLabel("myAccountLabel");
  MYPROFILELABEL = traductLabel("myProfileLabel");
  FAVORITESLABEL = traductLabel("favoritesLabel");
  MESSAGESLABEL = traductLabel("messagesLabel");
  CAREERLABEL = traductLabel("careerLabel");
  MYCVLABEL = traductLabel("myCVLabel");
  FROMLABEL = traductLabel("fromLabel");
  TOLABEL = traductLabel("toLabel");
  OBJECTLABEL = traductLabel("objectLabel");
  REQUIREDLABEL = traductLabel("requiredLabel");
  PHOTOLABEL = traductLabel("photoLabel");
  FIRSTANDLASTNAMELABEL = traductLabel("fullnameLabel");
  DELETELABEL = traductLabel("deleteLabel");
  WITHDRAWLABEL = traductLabel("withdrawLabel");
  DATEMUSTGREATERLABEL = traductLabel("dateMustGreaterLabel");
  OKNEXTLABEL = traductLabel("okNextLabel");
  PLEASESELECTDATEBIRTHLABEL = traductLabel("pleaseSelectDateBirthLabel");
  UNSOLICITEDAPPLICATIONLABEL = traductLabel("unsolicitedApplicationLabel");
  CHOOSELABEL = traductLabel("chooseLabel");
  LANGUESLABEL = traductLabel("languagesLabel");
  NOLABEL = traductLabel("noLabel");
  STANDNOTYETCONNECTEDLABEL = traductLabel("standNotYetConnectedLabel");
  EMAILNOTVALIDLABEL = traductLabel("emailNotValidLabel");
  LOGINALREADYEXISTSLABEL = traductLabel("loginAlreadyExistsLabel");
  EMAILALREADYEXISTSLABEL = traductLabel("emailAlreadyExistsLabel");
  ALLRECRUITERSSTANDDISCONNECTEDLABEL = traductLabel("allRecruitersStandDisconnectedLabel");
  OKLABEL = traductLabel("okLabel");
  ENTERPRISESCONNECTED = traductLabel("enterprisesConnectedLabel");
  EXTENDCHATSCREENLABEL = traductLabel("extendChatScreenLabel");
  REDUCECHATSCREENLABEL = traductLabel("reduceChatScreenLabel");
  CHATLABEL = traductLabel("chatLabel");
  SENDLABEL = traductLabel("sendLabel");
  CLOSELABEL = traductLabel("closeLabel");
  RECRUITERSNUMBERCHATLABEL = traductLabel("recruitersNumberChatLabel");
  LANGUECHATLABEL = traductLabel("langueChatLabel");
  LANGAUGETAGLOCALELABEL = traductLabel("languageTagLocaleLabel");
  LANGUAGEDATEPICKERFORMATLABEL = traductLabel("languageDatePickerFormatLabel");
  OPENLABEL = traductLabel("openLabel");
  PUBLICCHATLABEL = traductLabel("publicChatLabel");
  PRIVATECHATLABEL = traductLabel("privateChatLabel");
  MYCHATSLABEL = traductLabel("myChatsLabel");
  CHATTYPELABEL = traductLabel("chatTypeLabel");
  SAVETHISCHATLABEL = traductLabel("saveThisChatLabel");
  STOPSAVINGLABEL = traductLabel("stopSavingLabel");
  PLEASETRYAGAINLABEL = traductLabel("pleaseTryAgainLater");
  JOBOFFERSLABEL = traductLabel("jobOffersLabel");
  ALLLABEL = traductLabel("allLabel");
  LEFTCARACTERES = traductLabel("leftCarachters");
  STAND_CONNECTED_LABEL = traductLabel("standConnectedLabel");
  STAND_DISCONNECTED_LABEL = traductLabel("standDisconnectedLabel");
  CVNAMELABEL = traductLabel("cvNameLabel");
  PLEASEUPLOADCVLABEL = traductLabel("pleaseUploadCVLabel");
  TODAYLABEL = traductLabel("todayLabel");
  DEPOTDOCUMENTSUCCESSLABEL = traductLabel("depotDocumentSuccessLabel");
  SENDERRORLABEL = traductLabel("sendErrorLabel");
  CHOOSEDOCUMENTLABEL = traductLabel("chooseDocumentLabel");
  DOWNLOADERRORLABEL = traductLabel("downloadErrorLabel");
  DOWNLOADEXTENSIONERRORLABEL = traductLabel("downloadExtensionErrorLabel");
  DOWNLOADSIZEERRORLABEL = traductLabel("downloadSizeErrorLabel");
  DOWNLOADLABEL = traductLabel("downloadLabel");
  DOWNLOADSUCCESSLABEL = traductLabel("downloadSuccessLabel");
  // RECEPTION = traductLabel("receptionLabel");
  // MULTIMEDIA = traductLabel("multimediaLabel");
  // PRESSE = traductLabel("pressLabel");
  // ZONE = traductLabel("areaLabel");
  // PROFILE = traductLabel("profileLabel");
  // POSTERS = traductLabel("postersTabLabel");
  // ADVICES = traductLabel("advicesLabel");
  // SENDMESSAGE = traductLabel("sendMessageLabel");
  // CONSTACTS = traductLabel("sendMessageLabel");
  // WEBSITES = traductLabel("webSitesLabel");
  // AGENDA = traductLabel("agendaLabel");
  // SURVEYS = traductLabel("surveysLabel");
  // CHATTER = traductLabel("chatterLabel");
  // PRODUCTS = traductLabel("productsLabel");
  // POSTER = traductLabel("postersTabLabel"); 
  // SURVEY = traductLabel("surveyLabel");
  // ADVICE = traductLabel("advicesLabel");
  // PRODUCT = traductLabel("productsLabel");
  // PUBLIC = traductLabel("publicLabel");
  // PRIVATE = traductLabel("privateLabel");
  // FORUM = traductLabel("forumLabel");
  ACCOUNT_NOT_YET_APPROVED_LABEL = traductLabel("AccountNotYetApprovedLabel");
  NOPOSTER = traductLabel("noPosterLabel");
  NOCONTACT = traductLabel("noContactLabel");
  DELETEFROMFAV = traductLabel("deleteFromFavoriteLabel");
  VIEWLABEL = traductLabel("viewLabel");
  VISITSTANDLABEL = traductLabel("visitStandLabel");
  CREATEACCOUNTSUCCESSLABEL = traductLabel("createAccountSuccessLabel");
  UPDATEACCOUNTSUCCESSLABEL = traductLabel("updateAccountSuccessLabel");
  EACCETEAMLABEL = traductLabel("asmexTeamLabel");
  THANKSLABEL = traductLabel("thanksLabel");
  SAVEMESSAGELABEL = traductLabel("saveMessageLabel");
  ALREADYEXISTEMAILLABEL = traductLabel("alreadyExistEmailLabel");
  SAVEERRORLABEL = traductLabel("saveErrorLabel");
  DOWNEXTENSIONERRORLABEL = traductLabel("downloadExtensionErrorLabel");
  DOWNERRORLABEL = traductLabel("downloadErrorLabel");
  DOWNLOADLABEL = traductLabel("downloadLabel");
  DOWNLOADSUCCESSLABEL = traductLabel("downloadSuccessLabel");
  ALREADYREGISTREDLABEL = traductLabel("alredyRegistredLabel");
  EMAILEXISTTAPPASSLABEL = traductLabel("emailExistTapPassLabel");
  CANCELLABEL = traductLabel("cancelLabel");
  PLEASWAITLABEL = traductLabel("pleasWaitLabel");
  INCORRECTPASSLABEL = traductLabel("incorrectPassLabel"); 
  NODOCUMENTSLABEL = traductLabel("noDocumentsLabel"); 
  LANGUAGELABEL = traductLabel("languageLabel");   
  MESSAGESENTLABEL = traductLabel("messageSentLabel");        
  NOPROFILSEARCHEDLABEL = traductLabel("noProfilSearchedLabel");  
  NODEMANDELABEL = traductLabel("noDemandeLabel");  
  NORESPONSELABEL = traductLabel("noResponseLabel");  
  UPDATESUCCESSLABEL = traductLabel("updateSuccessLabel");  
  PICACCEPTEDEXTENSIONLABEL = traductLabel("picturesAcceptedExtensionLabel");  
  PICSIZELABEL = traductLabel("pictSizeLabel");  
  NOPICTURELABEL = traductLabel("noPictureLabel");  
  INFOREQUESTLABEL = traductLabel("infoRequestLabel");  
  NOPRODUCTLABEL = traductLabel("noProductLabel");  
  DOCUMENTSENTLABEL = traductLabel("documentSentLabel");  
  NOREQUESTDEMANDELABEL = traductLabel("noRequestDemandeLabel");    
  PARTICIPATELABEL = traductLabel("participateLabel");  
  CHOOSEONERESPONSEATLEASTLABEL = traductLabel("chooseOneResponseAtLeastLabel");    
  NOSURVEYLABEL = traductLabel("noSurveyLabel");
  SEEMORELABEL = traductLabel("seeMoreLabel"); 
  SEELESSLABEL = traductLabel("seeLessLabel");   
  NOTESTIMONYLABEL = traductLabel("noTetimonyLabel"); 
  NOVIDEOLABEL = traductLabel("noVideoLabel"); 
  SESSIONLABEL = traductLabel("sessionLabel"); 
  ACTIVITYSECTORLABEL = traductLabel("activitySectorLabel"); 
  SELECTLABEL = traductLabel("selectLabel"); 
  FRENCHLABEL = traductLabel("frenchLabel"); 
  ENGLISHLABEL = traductLabel("englishLabel"); 
  ARABICLABEL = traductLabel("arabicLabel"); 
  RUSSIANLABEL = traductLabel("russianLabel"); 
  GERMANLABEL = traductLabel("germenLabel"); 
  SPANISHLABEL = traductLabel("spanishLabel"); 
  REQUESTSUCCESSLABEL = traductLabel("requestSuccessSendLabel"); 
  DATASIZELABEL = traductLabel("dataSizeHintLabel"); 
  DOCSIZELABEL = traductLabel("docSizeLabel"); 
  AVATARLABEL = traductLabel("photoLabel"); 
  EXCHANGE = traductLabel("exchangeLabel");  
  INVITE_TO_TCHAT_LABEL = traductLabel("privateChatLabel");
  DESCRIPTIONLABEL = traductLabel("descriptionLabel");
  EMAILLOGINERRORLABEL = traductLabel("invalidEmailOrPassLabel");
  NEXTLABEL = traductLabel("nextLabel");
  BACKLABEL = traductLabel("previousLabel"); 
  SKIPLABEL = traductLabel("closeLabel"); 
  DONELABEL = traductLabel("okLabel");
  ADDLABEL  = traductLabel("addVisitCardLabel"); 
  THANK_YOU_REMOVE_SPACES_LABEL = traductLabel("thankYouToRemoveSpacesLabel");
  THANK_YOU_RESPECT_INDICATED_FORMAT_LABEL = traductLabel("thankYouToRespectIndicatedFormatLabel");  
  FILE_SIZE_NOT_ALLOWED_LABEL = traductLabel("fileSizeNotAllowedLabel");  
  SELECTED_LABEL  = traductLabel("selectedLabel");
  HOME_PAGE_LABEL = traductLabel("homePageLabel");  
  DEMAND_IN_PROGRESS_LABEL = traductLabel("demandInProgressLabel");
  ACCEPT_LABEL = traductLabel("acceptLabel");
  REFUSE_LABEL = traductLabel("refuseLabel");
  LET_MESSAGE_LABEL = traductLabel("letMessageLabel");
  FAIR_SHOW_LABEL = traductLabel("fairShowLabel");
  SPONSORS_LABEL = traductLabel("sponsorsLabel");
  
  affectTraduct();
}
function affectTraduct() {
  $('#searchBySectorsLink').attr('title', SEARCHBYSECTORLABEL);
  $('#searchByStandConnectedLink').attr('title', CONNECTEDSTANDSLABEL);
  $('.icon-search').attr('title', SEARCHLABEL);
  $('#logoEvent').attr('title', HOME_PAGE_LABEL);
}
function wait() {
  $('#loadingEds').show();
  $.blockUI({
    css: {
      border: 'none',
      backgroundColor: '',
      '-webkit-border-radius': '10px',
      '-moz-border-radius': '10px',
      opacity: 0.5,
    },
    message: '',
    overlayCSS: {
      backgroundColor: ''
    }
  });
}

function showUpdateSuccess() {
  $('html, body').animate({
    scrollTop: 0
  }, 'slow');

  $('#updateSuccess').fadeIn(function () {
    $('#updateSuccess').delay(1200).fadeOut();
  });

}

function showJobApplicationSaved() {
  hidePopups();
  $('html, body').animate({
    scrollTop: 0
  }, 'slow');

  $('#applySuccessMessage').fadeIn(function () {
    $('#applySuccessMessage').delay(1200).fadeOut();
  });

}

function registrationSuccess() {
  $('.contentPages,#content_video_transition').hide();
  stopVideoPlayerTransition();
  $('#content_registrationSuccess').show();

}

function showSurveySaved() {
  $('.contentOpacity').hide();
  $('html, body').animate({
    scrollTop: 0
  }, 'slow');

  $('#surveyFinished').fadeIn(function () {
    $('#surveyFinished').delay(1400).fadeOut();
  });

}

function favoriteAdded() {
  $('html, body').animate({
    scrollTop: 0
  }, 'slow');
  $('#favoriteAdded').fadeIn(function () {
    $('#favoriteAdded').delay(1200).fadeOut();
  });

}

function favoriteDeleted() {
  $('html, body').animate({
    scrollTop: 0
  }, 'slow');
  $('#favoriteDeleted').fadeIn(function () {
    $('#favoriteDeleted').delay(1200).fadeOut();
  });

}

function stopJwPlayer() {
  $("#playerVideoPopup").empty();
}

function removeSpaces(string) {
  if (string != undefined) {
    if (string != "") {
      return string.split(' ').join('');
    }
  } else
    return string;
}

//htmlEncode is function used to encode html
function htmlEncode(value) {
  if (value) {
    return $('<div />').text(value).html();
  }
  return value;
}

// genericAjaxCall is used to centralize all ajax calls
function genericAjaxCall(method, url, contentType, dataType, data, successCallBack, errorCallBack, loading) {
	/*nbreWSCallsInProgress++;
  if (loading == null || loading == true)
    $('#loadingEds').show();*/
  $.ajax({
    type: method,
    url: url,
    contentType: contentType,
    dataType: dataType,
    data: data,
    crossDomain: false,
    cache: cacheAjax,
    xhrFields: {
      withCredentials: true
    },
    headers: {'Access-Control-Allow-Origin': '*'},
    success: successCallBack,
    error: errorCallBack,
    statusCode: {
      401: exceptionUnAutorized
    }
  }).complete(function () {
    /*nbreWSCallsInProgress--;
    if (nbreWSCallsInProgress < 1) {
      $('#loadingEds').hide();
      nbreWSCallsInProgress = 0;
    }*/
  });
}

function exceptionUnAutorized() {
  var cookies = document.cookie.split(";");
  for (var i = 0; i < cookies.length; i++) {
    var equals = cookies[i].indexOf("=");
    var name = equals > -1 ? cookies[i].substr(0, equals) : cookies[i];
    if(name != "cookieBar") 
    	document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    //$.cookie(name, null);
    localStorage.setItem(name, null);
  }
  getStatusNoConnected(false);
}

function getCandidatePersonalInfos() {
  genericAjaxCall('GET', staticVars["urlBackEndCandidate"]
          + 'candidate/personalInfos', 'application/xml', 'xml', '',
          candidatePersonalInfosSuccess, candidatePersonalInfosError);
}

function sessionManagerSuccess(xml) {
  var bornDate = $(xml).find('bornDate').text();
  var address = $(xml).find('address').text();
  $('#candidateFirstName').text($(xml).find('prenomUser').text());
  $('#candidateLastName').text($(xml).find('nomUser').text());
  $('#candidateBornDate').text(bornDate);
  $('#candidateAddress').text(address);
}
function sessionManagerError(xhr, status, error) {
  isSessionExpired(xhr.responseText);
}

function sessionManagerWS() {
  genericAjaxCall('GET', staticVars["urlBackEndCandidate"]
          + 'candidate/SessionManager', 'application/xml', 'xml', '',
          sessionManagerSuccess, sessionManagerError);
}

function factoryFormattingString(str) {
  if (str != null) {
    str = factoryReplaceAllAdd(str);
    str = substringCustom(str, 160);
    str = formattingLongWords(str, 30);
    str = htmlEncode(str);
  }
  return str;
}
function factoryReplaceAllAdd(str) {
  if (str != null)
    return str.split('+').join(" ");
  else
    return str;
}
function factoryDecodeURIComponent(str) {
  try {
    return decodeURIComponent(str);
  }
  catch (err) {
    console.log("Error decode");
    return str;
  }
}

function getUpLoadedCvsSuccess(xml) {
  var numberUploadedCvs = $(xml).find('pdfCv').size();
  if (numberUploadedCvs > MAXUPLOADCV) {
    $('#cvUploadBtn').hide();
  } else {
    $('#cvUploadBtn').show();
  }
  $('#numberUploadedCvs').text(numberUploadedCvs);
  listPdfCvs = new Array();
  var listActions = null;
  var data = null;
  $(xml).find('pdfCv').each(
          function () {
            var cvId = $(this).find('idPdfCv').text();
            var cvName = $(this).find('cvName').text();
            var cvUrl = $(this).find('urlCv').text();
            cvName = formattingLongWords(cvName, 30);
            listActions = "<a id='" + cvId + "' class='btn btn-success btn-small' href='" + cvUrl + "'  target='_blank'>" + VIEWLABEL + "</a>"
                    + "<a id='" + cvId + "' style='margin-left: 10px;' class='btn btn-small btn-danger removePdfCv'>" + DELETELABEL + "</a>";
            data = [cvName, listActions];
            listPdfCvs.push(data);
          });
  $('#listUploadedCvs').dataTable(
          {
            "aaData": listPdfCvs,
            "aoColumns": [{
                "sTitle": CVNAMELABEL
              }, {
                "sTitle": ACTIONSLABEL
              }],
            "bAutoWidth": false,
            "bRetrieve": false,
            "bDestroy": true,
            "bFilter": false,
            "bLengthChange": false,
            "iDisplayLength": 10,
            "fnDrawCallback": function () {
              $('.removePdfCv').off();
              $('.removePdfCv')
                      .on('click',
                              function () {
                                idCvToChange = $(this).attr('id');
                                showPopupDeleteCv();
                              });
              $("thead").find('th:last')
                      .removeClass('sorting');
            }
          });
  $('#loadingEds,.contentOpacity').hide();
}

function getUpLoadedCvsError(xhr, status, error) {
  isSessionExpired(xhr.responseText);
  $('#loadingEds,.contentOpacity').hide();
  $('#loadingEds,.contentOpacity').hide();
}

function getUpLoadedCvs() {
  $('#listUploadedCvs').empty();
  genericAjaxCall('GET', staticVars["urlBackEndCandidate"]
          + 'pdfCv/uploadedCvs', 'application/xml', 'xml', '',
          getUpLoadedCvsSuccess, getUpLoadedCvsError);
}

function addUploadCv(cvName, cvUrl, callBack) {
  cvName = $.base64.encode(encode_utf8(cvName));
  genericAjaxCall('POST', staticVars["urlBackEndCandidate"] + 'cvcontent/addCv?cvName=' + cvName + "&cvUrl=" + cvUrl, 'application/xml,charset=utf-8', 'xml', '', function (data) {
    callBack();
  }, function (xhr, status, error) {
    isSessionExpired(xhr.responseText);
  });
}
function showPopupDeleteCv() {
  $('.backgroundPopup,.popupDeleteCv').show();
}

function showPopupImpossibleDelete() {
  $('.backgroundPopup,.popupDeleteCvImpossible').show();
}

function deleteCvSuccess(data) {
  //hidePopups();
  if (data == true) {
    factoryGetCandidateCvs();
  } else {
    showPopupImpossibleDelete();
  }
}

function deleteCvError(xhr, status, error) {
  isSessionExpired(xhr.responseText);
}

function deleteCv() {
  /*genericAjaxCall('POST', staticVars["urlBackEndCandidate"]
   + 'pdfCv/deleteCv', '', '', idCvToChange, deleteCvSuccess,
   deleteCvError);*/
  genericAjaxCall('POST', staticVars["urlBackEndCandidate"]
          + 'pdfCv/deleteCv?idCv=' + idCvToChange, '', '', '', deleteCvSuccess,
          deleteCvError);
}

function flowPlayerVideo(videoUrl, backgroundImageUrl) {
  $f("playerVideo", "flowplayer/flowplayer-3.2.15.swf", {
    clip: {
      url: videoUrl, // 'videos/wildlife.wmv', // put video url here
      autoPlay: true,
      autoBuffering: true
    },
    plugins: {
      audio: {
        url: 'flowplayer/flowplayer.audio.swf'
      }
    },
    canvas: {
      // configure background properties
      // background: '#000000 url('+backgroundImageUrl+') no-repeat 30
      // 10',//'#000000 url(images/rec1.jpg) no-repeat 30 10',

      // remove default canvas gradient
      backgroundGradient: 'none',
    }
  });
}

function supportsVideo() {
  var elem = document.createElement('video'),
          bool = false;

  // IE9 Running on Windows Server SKU can cause an exception to be thrown, bug #224
  try {
    if (bool = !!elem.canPlayType) {
      bool = new Boolean(bool);
      bool.ogg = elem.canPlayType('video/ogg; codecs="theora"');

      // Workaround required for IE9, which doesn't report video support without audio codec specified.
      //   bug 599718 @ msft connect
      var h264 = 'video/mp4; codecs="avc1.42E01E';
      bool.h264 = elem.canPlayType(h264 + '"') || elem.canPlayType(h264 + ', mp4a.40.2"');

      bool.webm = elem.canPlayType('video/webm; codecs="vp8, vorbis"');
    }

  } catch (e) {
  }

  return bool;
}

function stopVideoPlayerTransition() {
  //console.log('stopVideoPlayerTransition');
  if (videoLaunching) {
    $('#content_video_transition').empty();
    videoLaunching = false;
  }
  $('.youtubeHolder').empty();
}

function flowAutoPlay(videoUrl, backgroundImageUrl, callBack, selector, sector) {
  //console.log(selector);
  //console.log(videoUrl);
  $('.modal-loading-video').show();
  $('#page-content').addClass('video-player');
  $('#video_tran_content').removeClass('hide');
  $('.video_tran').hide();
  if (isIe_Ver() || FlashDetect.installed == false) {
    callBack();
    executeAfterPlayer(videoUrl, selector,sector);
  }
  else {
    var supportVideo = supportsVideo();
    videoLaunching = true;
    if (supportVideo) {
      $(selector).empty();
      $(selector).show();
      $(selector).append('<video id="playerHtml5" class="video-js vjs-default-skin" preload="auto" autoplay width="100%" height="100%" data-setup="{}" poster="' + backgroundImageUrl + '">'
              + ' <source type="video/webm"'
              + ' src="' + videoUrl + '.webm"">'
              + '<source type="video/mp4"'
              + ' src="' + videoUrl + '.mp4">'
              + '<source type="video/ogg"'
              + 'src="' + videoUrl + '.ogv">'
              + ' <source type="video/flash"'
              + ' src="mp4:' + videoUrl + '.mp4"></video><div class="loading-video"><img src="img/loading-video.gif"/></div>');
      videojs.options.flash.swf = "videoplayer/video-js.swf";
      $('#playerHtml5').on("play", function () {
      	$(selector).find(".loading-video").remove();
      	if(selector!= null && selector.indexOf("video_transition_from_hall_to") != -1) 
      		$(selector).addClass("video_transition_from_hall_to");
        callBack();
      });
      $('#playerHtml5').on("ended", function () {
        executeAfterPlayer(videoUrl, selector,sector);
      	if(selector!= null && selector.indexOf("video_transition_from_hall_to") != -1) 
      		$(selector).removeClass("video_transition_from_hall_to");
      });
    } /*else if (FlashDetect.installed) {
      if (document.getElementById('playerStand') == null) {
      	$("#playerStand").remove();
      }
      $(selector).empty();
      $(selector).append('<div id="playerStand" style="width:100%;max-width:100%;height:100%;max-height:100%;"></div>');
      $(selector).show();
      $f("playerStand", "flowplayer/flowplayer-3.2.15.swf",
              {
                onLoad: function () {
                },
                onBeforePause: function ()
                {
                  return false;
                },
                onStart: function (clip) {
                  setTimeout(function () {
                    callBack();
                  }, 500);
                },
                clip: {
                  url: videoUrl + '.mp4', // 'videos/wildlife.wmv', // put video url
                  autoPlay: true,
                  autoBuffering: true
                },
                plugins: {
                  controls: null
                },
                canvas: {
                  backgroundGradient: 'none'
                },
                play: {
                  opacity: 0.0,
                  label: null, // label text; by default there is no text
                  replayLabel: null // label text at end of video clip
                },
                onFinish: function () {
                  executeAfterPlayer(videoUrl, selector);
                }
              });
    } */else {
      callBack();
      executeAfterPlayer(videoUrl, selector,sector);
    }
    $(".skip-v a").unbind("click").bind("click", function () {
      callBack();
      executeAfterPlayer(videoUrl, selector,sector);
    });
  }

}

function executeAfterPlayer(videoUrl, selector, sector) {
  $(selector).empty();
  videoLaunching = false;
  $(selector).hide();
  $('.receptionButton').show();
  $('#vue-aerienne').show();
	if (selector == '#content_video_transition') {
    $('#contentG').attr('class', '');
    $('#principal-halls').find('.img-salle').attr('src','img/halls/hall_asmex_'+getIdLangParam()+'.jpg');
    $('#page-content').removeClass('no-padding');
    $('#home-page,#info-stand').addClass('hide');
    $('#contentG').addClass('principal-halls');
    $('#principal-halls').removeClass('hide');
    $('#principal-halls').show();
    $('.vue-aerienne').show();    
    $('#principal-halls .img-salle').show();
  } else if (selector == "#content_video_transition_from_hall_to_B") {
    $('#page-content').removeClass('no-padding');
    $('#home-page,#info-stand').addClass('hide');
    $('#hall'+sector).removeClass('hide');
    $('#hall'+sector).show();
    $.cookie('currentHall','hall'+sector);
  } else if (selector == "#content_video_transition_from_hall_to_A") {
    $('#page-content').removeClass('no-padding');
    $('#home-page,#info-stand').addClass('hide');
    $('#hall'+sector).removeClass('hide');
    $('#hall'+sector).show();
    $.cookie('currentHall','hall'+sector);
  } else if (selector == '#content_video_transition_info_stand') {
    $('#page-content').removeClass('no-padding');
    $('#home-page,#salle-exposition').addClass('hide');
    $('#info-stand').removeClass('hide');
    $('#contentG').attr('class', '').addClass('stand7').addClass('info-stand');
    $('#info-stand, #websitesNetwork').show();
  } else if (selector == '#content_video_transition_exite_to_info') {
    $('#page-content').removeClass('no-padding');
    $('#home-page,#salle-exposition').addClass('hide');
    $('#info-stand').removeClass('hide');
    $('#contentG').attr('class', '').addClass('stand7').addClass('info-stand');
    $('#info-stand, #websitesNetwork').show();
  } else if (selector.indexOf('to_multimedia') != -1 || selector.indexOf('to_press') != -1) {
    $('#video_tran_content').addClass('hide');
    $('#info-stand-multimedia, #websitesNetwork').show();
  } else if (selector.indexOf('to_reception') != -1) {
    $('#video_tran_content').addClass('hide');
    $('#info-stand, #websitesNetwork').show();
  } else if (selector == '#content_video_transition_exite') {
    $('#contentG').attr('class', '');
    $('#principal-halls').find('.img-salle').attr('src','img/halls/hall_asmex_'+getIdLangParam()+'.jpg');
    $('#page-content').removeClass('no-padding');
    $('#home-page,#info-stand').addClass('hide');
    $('#contentG').addClass('principal-halls');
    $('#principal-halls').removeClass('hide');
    $('#principal-halls').show();
    $('.vue-aerienne').show();    
    $('#principal-halls .img-salle').show();
  }else if (selector == '#content_video_transition_webcast') {
    if (videoRhController == null) {
      videoRhController = new $.VideoRhController(
              new $.VideoRhModel(), new $.VideoRhView());
    }
    videoRhController.initController();
  }else if (selector == '#content_video_transition_to_conference') {
    $('.icon-format7').click();
  }else if (selector == '#content_video_transition_to_digievent' || selector == "#content_video_transition_to_digievent_batiment") {
    $('.icon-format1').click();
    window.open('http://digifact.co/virtualevents/', '_blank');
  }else if (selector == '#content_video_transition_to_digifact' || selector == "#content_video_transition_to_digifact_batiment"){
    $('.icon-format1').click();
    window.open('http://digifact.co', '_blank');
  }
  $('#page-content').removeClass('video-player');
  $('#video_tran_content').addClass('hide');
  $('.modal-loading-video').hide();
}
// search page scripts
function playYoutubeVideo(path, w, h) {
  $('.youtubeHolder').empty();
  var isSafari = !!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/);
  //console.log("********** isSafari : "+isSafari+" *************");
  if (isSafari) {
    $('.youtubeHolder').append('<iframe id="videoYoutub_" style="width: ' + w + '; height: ' + h + ';" frameborder="0" volume="0" mute="1" '
            + 'src="http://www.youtube.com/embed/' + path + '?&playerapiid=ytplayer&amp;amp;controls=1&amp;amp;showinfo=0&amp;amp;rel=0&amp;amp;enablejsapi=1&amp;amp;theme=dark&amp;amp;wmode=transparent&amp;amp;autoplay=1&amp;amp;volume=0&amp;amp;vol=0&amp;amp;mute=1&amp;amp;loop=1&amp;amp;playlist=' + path + ';"></iframe>');
  } else {
    var paramsYoutube = {}, attributesYoutube = {};
    paramsYoutube.quality = "high";
    paramsYoutube.width = w;
    paramsYoutube.height = h;
    paramsYoutube.scale = "noscale";
    paramsYoutube.salign = "tl";
    paramsYoutube.wmode = "transparent";
    paramsYoutube.devicefont = "false";
    paramsYoutube.allowfullscreen = "false";
    paramsYoutube.allowscriptaccess = "always";
    attributesYoutube.id = "playerYoutubeWrapper";
    swfobject.embedSWF('http://www.youtube.com/v/' + path + '&version=3&controls=0&rel=0&showinfo=0&enablejsapi=1&playerapiid=ytplayer&autoplay=1&theme=dark&wmode=transparent&loop=1&playlist=' + path,
            "playerYoutubeWrapper", w, h, "8", null, null, paramsYoutube, attributesYoutube);
  }
}


function onYouTubePlayerReady(playerId) {
  ytplayer = document.getElementById("playerYoutubeWrapper");
  ytplayer.mute();
}
function parseXmlCustom(xml) {
  if ($.browser.msie) {
    var xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
    xmlDoc.loadXML(xml);
    xml = xmlDoc;
  }
  return xml;
}
function getXmlStringCustom(xml) {
  if (window.ActiveXObject) {
    return xml.xml;
  }
  return new XMLSerializer().serializeToString(xml);
}
function translateSuccess(xml) {
  xml = parseXmlCustom(xml);
  xmlLanguage = xml;
  traduct();
  traductAllLabel();
  if (currentPage == PageEnum.FORUM_REGISTRATION)
    initForumRegistrationPage();
  else if (currentPage == PageEnum.HOME_PAGE)
    initHomePage();
  else if (currentPage == PageEnum.PROFILE_CANDIDATE)
    initProfileCandidate();
}

function translateError(xhr, status, error) {
  isSessionExpired(xhr.responseText);
}

function translate(pathFile) {
  translateUrl = pathFile;
  genericAjaxCall('GET', pathFile, 'application/xml', ($.browser.msie) ? 'text' : 'xml', '', translateSuccess,
          translateError);
}

function getLanguagePathSuccess(xml) {
  try {
    translate($(xml).find('path').text());
  } catch (e) {
    console.log("*********Exception : " + e + "********");
    catchTryAgainException();
  }
}
function getLanguagePathError(xhr, status, error) {
  manageStatus(xhr.status, error);
}

function getLanguagePath(idLang) {
  //console.log('getLanguagePath');
  genericAjaxCall('GET', staticVars["urlBackEndCandidate"]
          + 'languaget/translate/?idLang=' + idLang, 'application/xml', 'xml', '',
          getLanguagePathSuccess, getLanguagePathError);
}

function getSeoByStandIdSuccess(xml) {
  var seo = new $.SEO(xml);
  $("meta[name='description']").attr("content", seo.getSeoMetadata());
  $("meta[name='keywords']").attr("content", seo.getSeoKeywords());  
  document.title = seo.getSeoTitle(); 
  $('.lienAffiche1').attr('href',seo.getSeoFirstLink());
  $('.lienAffiche2').attr('href',seo.getSeoFirstLink());
  $('.lienAffiche3').attr('href',seo.getSeoFirstLink());
  if(getParam('standId') == '13'){    
    $('.fomagriPressLeft').attr('href',seo.getSeoFirstLink());
    $('.fomagriPressCenter').attr('href',seo.getSeoSecondeLink());
    $('.lienAffiche1').attr('href',seo.getSeoSecondeLink());
    $('.lienAffiche2').attr('href',seo.getSeoSecondeLink());
  }
}

function getSeoByStandIdError(xhr, status, error) {
  manageStatus(xhr.status, error);
}

function getSeoByStandId(idLang,standId) {
  //console.log('getLanguagePath');
  genericAjaxCall('GET', staticVars["urlBackEndCandidate"]
          + 'seo/get?idLang=' + idLang+'&'+standId, 'application/xml', 'xml', '',
          getSeoByStandIdSuccess, getSeoByStandIdError);
}
//*********************Favorites begin*****************************

getUserFavoritesSuccess = function (xml) {
  listUserFavorites = [];
  $(xml).find("favorite").each(
          function () {
            var favorite = new $.Favorite($(this));
            listUserFavorites[favorite.getEntityId() + SEPARATE_FAVORITE_KEY + favorite.getComponent().getComponentName()] = favorite;
          });
};

getUserFavoritesError = function (xhr, status, error) {
  isSessionExpired(xhr.responseText);
};

getUserFavorites = function () {
  genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'favorite/userFavorites?idLang=' + getIdLangParam(), 'application/xml', 'xml', '', getUserFavoritesSuccess, getUserFavoritesError);
};

function addFavoriteSuccess() {
  currentPage = PageEnum.FORUM;
  getUserFavorites();
  favoriteAdded();
}

function addFavoriteError(xhr, status, error) {
  isSessionExpired(xhr.responseText);
}

function addFavorite(favoriteXml, callBack) {
  if (connectedUser) {
    genericAjaxCall('POST', staticVars["urlBackEndCandidate"]
            + 'favorite/addFavorite', 'application/xml', 'xml', favoriteXml,
            addFavoriteSuccess, addFavoriteError);
  } else {
    getStatusNoConnected();
  }
}

function deleteFavoriteSuccess() {
  getUserFavorites();
  favoriteDeleted();
}

function deleteFavoriteError(xhr, status, error) {
  isSessionExpired(xhr.responseText);
}

function deleteFavorite(favoriteXml) {
  if (connectedUser) {
    genericAjaxCall('POST', staticVars["urlBackEndCandidate"]
            + 'favorite/deleteFavorite', 'application/xml', 'xml', favoriteXml,
            deleteFavoriteSuccess, deleteFavoriteError);
  } else {
    getStatusNoConnected();
  }
}

function visitCardAdded() {
  $('html, body').animate({
    scrollTop: 0
  }, 'slow');
  $('#favoriteAdded').fadeIn(function () {
    $('#favoriteAdded').delay(1200).fadeOut();
  });
}

function addVisitCardSuccess() {
  visitCardAdded();
}

function addVisitCardError(xhr, status, error) {
  isSessionExpired(xhr.responseText);
}

function addVisitCard(favoriteXml, callBack) {
  if (connectedUser) {
    genericAjaxCall('POST', staticVars["urlBackEndCandidate"]
            + 'favorite/addFavorite', 'application/xml', 'xml', favoriteXml,
            function(xml){
                addVisitCardSuccess(xml);
                callBack();
            }, addVisitCardError);
  } else {
    getStatusNoConnected();
  }
}

function visitCardDeleted() {
  $('html, body').animate({
    scrollTop: 0
  }, 'slow');
  $('#favoriteDeleted').fadeIn(function () {
    $('#favoriteDeleted').delay(1200).fadeOut();
  });
}

function deleteVisitCardSuccess() {
  visitCardDeleted();
}

function deleteVisitCardError(xhr, status, error) {
  isSessionExpired(xhr.responseText);
}

function deleteVisitCard(favoriteXml, callBack) {
  if (connectedUser) {
    genericAjaxCall('POST', staticVars["urlBackEndCandidate"]
            + 'favorite/refuseVisitCard', 'application/xml', 'xml', favoriteXml,
            function(xml){
                deleteVisitCardSuccess(xml);
                callBack();
            }, deleteVisitCardError);
  } else {
    getStatusNoConnected();
  }
}

//*********************Favorites end*****************************

//*********************Chat*****************************
function initChat(chatType) {
  if (chatType == STAND_CHAT) {
    connectToChatStand();
  } else if (chatType == PUBLIC_CHAT) {
    connectToPublicChat();
  }
}


function connectToPublicChat() {
  var chatPublicComponentSelector = $('#chatPublicComponent');
  if (FlashDetect.installed) {
    var chatFlashContentId = 'chatFlashContent';
    chatPublicComponentSelector.empty();
    chatPublicComponentSelector.append('<div id="' + chatFlashContentId + '"></div> ');
    initChatPublic(chatFlashContentId);
    //genericAjaxCall('POST', staticVars["urlBackEndCandidate"]+'chat/infoChat', 'application/xml', 'xml','', getInfoChatPublicSuccess, getInfoChatPublicError);
    //$('#chatPublicComponent').load('modules/chat/chatFlex/ChatPublic.html');
  } else {
    $('#chatPublicComponent').load('modules/chat/chatHtml5/chathtml5Component.html', function () {
      initChatHtml5();
      $('.chatPublicLoading').css('display', 'none');
    });
  }
  analyticsView.viewPublicChat();
}

function getInfoChatPublicSuccess(result) {
  var chatFlashContentId = 'chatFlashContent';
  initChatPublic(chatFlashContentId, result);
}
function getInfoChatPublicError(xhr, status, error) {
  isSessionExpired(xhr.responseText);
}
function connectToChatStand() {
  /*if($.cookie('isFlash')=="true"){
   $('#listStandchatters').load('modules/chat/chatFlex/ChatStand.html');
   }else {*/
  $('#listStandchatters').load('modules/chat/chatHtml5/chatStandHtml5Component.html', function () {
    initChatStandHtml5();
    $('.chatStandLoading').css('display', 'none');
  });
  //}
}

function encode_utf8(s) {
  for (var c, i = -1, l = (s = s.split("")).length, o = String.fromCharCode; ++i < l;
          s[i] = (c = s[i].charCodeAt(0)) >= 127 ? o(0xc0 | (c >>> 6)) + o(0x80 | (c & 0x3f)) : s[i]
          )
    ;
  return s.join("");
}
function decode_utf8(s) {
  for (var a, b, i = -1, l = (s = s.split("")).length, o = String.fromCharCode, c = "charCodeAt"; ++i < l;
          ((a = s[i][c](0)) & 0x80) &&
          (s[i] = (a & 0xfc) == 0xc0 && ((b = s[i + 1][c](0)) & 0xc0) == 0x80 ?
                  o(((a & 0x03) << 6) + (b & 0x3f)) : o(128), s[++i] = "")
          )
    ;
  return s.join("");
}

function getFileName(url) {
  if (url)
    return url.split('/').pop();
  return "";
}
function getPurePath(url) {
  if (url)
    return url.substring((url.lastIndexOf('/') + 1), url.length);
  return "";
}
function factoryXMLFavory(idEntity, componentName) {
  var favoriteXml = "<favorite><idEntity>"
          + idEntity
          + "</idEntity><component><componentName>"
          + componentName
          + "</componentName></component></favorite>";
  return favoriteXml;
}

function factoryDeleteFavorite(that, idEntity, componentName) {
  var favoriteXml = factoryXMLFavory(idEntity, componentName);
  deleteFavorite(favoriteXml);
  $(that).closest('tr').remove();
}
function factoryVisitStand(that) {
  currentStandId = $(that).attr('id');
  $('#visitStandTrigger').trigger('click');
}
function playJwPlayer(videoId, videoUrl, w, h) {
  var player = jwplayer(videoId);
  if (compareToLowerString(videoUrl, '//youtube') != -1 || compareToLowerString(videoUrl, '//youtu.be') != -1 
    || compareToLowerString(videoUrl, '//www.youtube') != -1 || compareToLowerString(videoUrl, '//wwww.youtu.be') != -1) {
    
    if (compareToLowerString(videoUrl, '//youtu.be') != -1 || compareToLowerString(videoUrl, '//wwww.youtu.be') != -1) {
      videoUrl = videoUrl.replace('youtu.be/', 'youtube.com/embed/');
    }
    if (FlashDetect.installed) {
      videoUrl = videoUrl.replace('/v/', '/watch?v=');
      videoUrl = videoUrl.replace('/embed/', '/watch?v=');
      player.setup({
        id: "jw_" + videoId,
        file: videoUrl,
        width: "100%",
        height: "100%"
      });
    } else {
      videoUrl = videoUrl.replace('/v/', '/embed/');
      videoUrl = videoUrl.replace('/watch?v=', '/embed/');
      var video_container = $('<iframe src="'
              + videoUrl
              + '" style="width:100%; height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen></iframe>');
      $("#" + videoId).empty();
      $("#" + videoId).append(video_container);
    }
  } else if(compareToLowerString(videoUrl, '//www.dailymotion')!=-1){
          videoUrl=videoUrl.replace('/video/','/embed/video/');
          var video_container = $('<iframe src="'
            + videoUrl
            + '" style="width:100%; height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen></iframe>');
          $("#"+videoId).empty();
          $("#"+videoId).append(video_container);
    }else {
    //setTimeout(function(){
      player.setup({
        id: "jw_" + videoId,
        swf: 'jwplayer/player.swf',
        file: videoUrl,
        width: "100%",
        height: "100%",
      });
    //}, 1000);
    
  }
}
function XMLToString(oXML) {
  if (oXML != null) {
    //code for IE
    if (window.ActiveXObject) {
      var oString = oXML.xml;
      return oString;
    }
    // code for Chrome, Safari, Firefox, Opera, etc.
    else {
      return (new XMLSerializer()).serializeToString(oXML);
    }
  }
}
function getTranslatePath() {
  var href = window.location.href;
  if (href.indexOf('?') != -1)
    href = href.substring(0, href.lastIndexOf('?'));
  if (href.indexOf('#') != -1)
    href = href.substring(0, href.lastIndexOf('#'));
  href = href.substring(0, href.lastIndexOf('/'));
  return href + "/translate/" + getFileName(translateUrl);
}

function chatBlink() {
  //alert('chatBlink');
  //console.log('chatBlink');
  if(!$('.zone-pub .tchat').hasClass("open")) 
  	$(".icon-tchat").blink(1000);
}

function reconnectToChat() {
  //alert('chatBlink');
  //console.log('reconnectToChat');
  tryToReconnect++;
  /*chat_loaded = false;
   if (tryToReconnect < 2)
   $('#chatPublicButton').trigger('click');
   else*/
  window.location.reload();
}

function viewProfileStand(standId, standName) {
  //console.log('viewProfileStand');
  //$('#chatPublicComponent').trigger('viewProfileStand', standId);
  //$('.icon-tchat').trigger("click");
  //$('#signOut').trigger('click');
}

function doSettings() {
  //$('#userProfileLink').trigger("click");
  $('.myAccountLink').trigger("click");
  $('.icon-tchat').trigger("click");
  $('.zone-pub .tchat').removeClass("open");
}

function playVideoPopup(videoUrl, videoTitle) {
  $('#videoTitle').text(videoTitle);
  $('.popupVideo,.backgroundPopup').show();
  var video_container = null;
  var playerVideoPopupSelector = $('#playerVideoPopup');
  playerVideoPopupSelector.empty();
  if (compareToLowerString(videoUrl, "dailymotion") !== -1) {
    video_container = $('<iframe src="'
            + videoUrl
            + '" style="width:100%; height:100%;" frameborder="0"></iframe>');
    playerVideoPopupSelector.append(
            video_container);
  } else {
    video_container = $('<div id="playerVideo" class="player" style="width: 100%; height: 100%;"> </div>');
    playerVideoPopupSelector.append(
            video_container);
    playJwPlayer("playerVideo", videoUrl, "100%", "100%");
  }
}


function reconnectToChatStand() {
  //console.log('reconnectToChatStand');
  $('#chatterLink').trigger('click');
};

function closeChatStand() {
  //console.log('closeChatStand');
  $('#chatterLink').trigger('click');
};

function reduceStandChat() {
	$("#modal-stand-tchat .close").click();
};

function viewMyChats() {
  //console.log('viewMyChats');
  $('.userMyChatsLink').trigger("click");
  $('.icon-tchat').trigger("click");
  $('.zone-pub .tchat').removeClass("open");
}
//------------------------------------------------------------------------- métier à déplacer dans tools.js
function currentDate() {
  /*var today = new Date();
   var dd = today.getDate();
   var mm = today.getMonth() + 1; //January is 0!

   var yyyy = today.getFullYear();
   if (dd < 10)
   dd = '0' + dd;
   if (mm < 10)
   mm = '0' + mm;

   return dd + '/' + mm + '/' + yyyy;*/
  return moment().locale("fr").format('LL');
}

function textToHTML(text) {
  return ((text || "") + "")  // make sure it's a string;
          .replace(/&/g, "&amp;")
          .replace(/</g, "&lt;")
          .replace(/>/g, "&gt;")
          .replace(/\t/g, "    ")
          .replace(/ /g, "&#8203;&nbsp;&#8203;")
          .replace(/\r\n|\r|\n/g, "<br />");
}

function substringCustom(text, size) {
  if (text != null) {
    if (text.length < size)
      return text;
    else
      return text.substring(0, size) + "...";
  }
}

function getDrapeauUrl(langId) {
  return 'modules/chat/chatFlex/assets/images/drapeaux/' + langId + '.png';
}

function getFileExtension(url) {
  if (url)
    return url.split('.').pop();
  return "";
}

function getUploadFileName(timestamp, fileName) {
  var fileNameExtension = getFileExtension(fileName);
  //console.log("MD5:>" + timestamp + SEPARATEUPLOAD + fileName);
  return $.md5(timestamp + SEPARATEUPLOAD + fileName) + '.' + fileNameExtension;
}

function getUploadFileUrl(fileName) {
  return staticVars.urlServerStorage + fileName;
}
function getUrlPostUploadFile(timestamp, fileName) {
  //return staticVars.urlServerUploader+"?token="+timestamp+"&ext=."+getFileExtension(fileName);
  return staticVars.urlServerUploader + "?token=" + $.md5(timestamp + SEPARATEUPLOAD + fileName) + "&ext=." + getFileExtension(fileName);
}

function progressBarUploadFile(selectorDivProgress, data, maxProgress) {
  var progress = parseInt(data.loaded / data.total * 100, 10);
  if (progress <= maxProgress) {
    $(selectorDivProgress + ' .progress-bar').css('width', progress + '%');
    $(selectorDivProgress + ' .loadingText').text(CHARGEMENTLABEL + ' ' + progress + '%');
  }
}

function beforeUploadFile(selectorSaveBotton, selectorDivProgress, selectorToHide) {
  disabledButton(selectorSaveBotton);
  $(selectorToHide).hide();
  $(selectorDivProgress + ' .progress-bar').css('width', '0%');
  $(selectorDivProgress + ' .loadingText').text(CHARGEMENTLABEL + ' 0%');
  $(selectorDivProgress).show();
}
function afterUploadFile(selectorSaveBotton, selectorDivProgress, selectorToHide) {
  enabledButton(selectorSaveBotton);
  $(selectorToHide).hide();
  $(selectorDivProgress).show();
}
function isDisabled(selector) {
  if ($(selector).attr("disabled") == "disabled")
    return true;
  else
    return false;
}
function disabledButton(selector) {
  $(selector).attr("disabled", "disabled");
}
function enabledButton(selector) {
  $(selector).removeAttr("disabled");
}
//------------------------------------------------------------------------- métier à déplacer dans tools.js
function buildXML(rootElement, data) {
  var balise = "<" + rootElement + ">";
  var keys = Object.keys(data);
  $.each(keys, function () {
    balise = balise.concat('<' + this + '>' + data[this] + '</' + this + '>');
  });
  balise = balise.concat("</" + rootElement + ">");
  return balise;
}
function buildXMLList(rootElement, parentElement, datas) {
  var balise = "<" + rootElement + ">";
  $.each(datas, function () {
    var that = this;
    var keys = Object.keys(this);
    balise = balise.concat(buildXML(parentElement, that));
  });
  balise = balise.concat("</" + rootElement + ">");
  //console.log(balise);
  return balise;
}
function parseDateAgenda(date) {
  var yearUTC = date.substring(6, 10);
  var monthUTC = date.substring(3, 5);
  var dayUTC = date.substring(0, 2);
  var hoursUTC = date.substring(11, 13);
  var minutesUTC = date.substring(14, 16);
  var d = new Date();
  d.setUTCFullYear(yearUTC);
  d.setUTCMonth(monthUTC - 1);
  d.setUTCDate(dayUTC);
  d.setUTCHours(hoursUTC);
  d.setUTCMinutes(minutesUTC);
  return d;
}
function getYearMonthDayDateAgenda(d) {
  var year = d.getFullYear();
  var month = getStringElementDateNumber(d.getMonth() + 1);
  var day = getStringElementDateNumber(d.getDate());
  return [day, month, year].join('-');
}
function getStringElementDateNumber(nb) {
  if (nb < 10) {
    return nb = '0' + nb;
  }
  return nb;
}
function getTimeAgenda(d) {
  var hours = getStringElementDateNumber(d.getHours());
  var minutes = getStringElementDateNumber(d.getMinutes());
  return [hours, minutes].join(':');
}
function factoryTraductChatHtml5(tagModule) {
  $(".allRecruitersStandDisconnectedLabel").text(ALLRECRUITERSSTANDDISCONNECTEDLABEL);
  $(".okLabel").text(OKLABEL);
  $("#connectEnterprises" + tagModule + " span").text(ENTERPRISESCONNECTED + " (0)");
  $(".extendChatScreenLabel").text(EXTENDCHATSCREENLABEL);
  $(".reduceChatScreenLabel").text(REDUCECHATSCREENLABEL);
  $(".chatLabel").text(CHATLABEL);
  $(".sendLabel").text(SENDLABEL);
  $(".saveThisChatLabel").text(SAVETHISCHATLABEL);
  $(".stopSaveChatLabel").text(STOPSAVINGLABEL);
  $("#viewHistory" + tagModule).attr("title", MYCHATSLABEL);
  $("#chatReduceStandsButton" + tagModule + " span").attr('title', EXTENDCHATSCREENLABEL);
}
function extractDate(timestamp) {
  var date = new Date();
  date.setTime(timestamp);
  return getStringElementDateNumber(date.getHours()) + ":" + getStringElementDateNumber(date.getMinutes());
}

function timestampsToDate(timestamp) {
  var theDate = new Date();
  theDate.setTime(timestamp);
  //dateString = theDate.toGMTString();
  //return dateString.substring(5, 22);
  return theDate.toLocaleString();
}

function getLanguageName(languageId) {
  if (languageId != "" && languageId != null && listLangsAbrEnum[languageId] != null)
    return listLangsAbrEnum[languageId];
  else
    return "";
}
function removeCurrentSession() {
  genericAjaxCall('DELETE', staticVars["urlBackEndCandidate"] + 'stand/removeCurrentSession', 'application/xml', 'xml', '', '', '');
  return '';
}
$(window).bind('beforeunload', function () {
  //removeCurrentSession();
});
function sessionExtender() {
  //console.log('sessionExtender');
  genericAjaxCall('POST', staticVars["urlBackEndCandidate"] + 'stand/sessionExtender', 'application/xml', 'xml', '', '', '');
}
function removeCurrentStand() {
  getNotifsSuccess(publicationCash);
  $('#publicationsHidden').show();
  if ($('#content_stand_reception').is(":visible")) {
    //console.log('removeCurrentStand');
    genericAjaxCall('POST', staticVars["urlBackEndCandidate"] + 'stand/removeCurrentStand', 'application/xml', 'xml', '', '', '');
  }
}

function manageStatus(status, error) {
  //console.log("manageStatus : " + status);
  if (status < 200 || status > 299)
    catchTryAgainException();
  //status=parseInt(status);
}
function parseDateCareer(date) {
  var yearUTC = date.substring(6, 10);
  var monthUTC = date.substring(3, 5);
  var dayUTC = date.substring(0, 2);
  var hoursUTC = date.substring(11, 13);
  var minutesUTC = date.substring(14, 16);
  var d = new Date();
  d.setUTCFullYear(yearUTC);
  d.setUTCMonth(monthUTC - 1);
  d.setUTCDate(dayUTC);
  d.setUTCHours(hoursUTC);
  d.setUTCMinutes(minutesUTC);
  return d;
}
function factoryFavoriteExist(compId, compName) {
  var key = compId + SEPARATE_FAVORITE_KEY + compName;
  if (listUserFavorites[key] != null)
    return true;
  return false;
}
function replaceSpecialChars(value) {
  if (value)
  	return value.toString().replace('µ', '&micro');
  else
    return value;
}

function getStyleChatSWFPath() {
  return "modules/chat/chatFlex/assets/css/chat_style.swf";
}
function getStandSubstitutionLangId(standId, callBack) {
  genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'stand/substitutionLangId?standId=' + standId + "&idLang=" + getIdLangParam(),
          'application/xml', 'xml', '', function (xml) {
            var languageSubstitutionId = $(xml).find("languageId").text();
            //console.log("standId :::: " + standId + ", idLang : " + languageSubstitutionId);
            callBack(languageSubstitutionId);
          }, "");
}
;
function compareToLowerString(str1, str2) {
  if (str1 != null)
    str1 = str1.toLowerCase();
  else
    str1 = "";
  if (str2 != null)
    str2 = str2.toLowerCase();
  return str1.indexOf(str2);
}

function formattingLongWords(str, maxLength) {
  var result = null;
  if (str != null && result != '') {
    result = '';
    var words = str.split(' ');
    $.each(words, function () {
      if (this.length > maxLength)
        result = result.concat(this.substring(0, maxLength)).concat('... ');
      else
        result = result.concat(this).concat(' ');
    });
    result = result.substring(0, result.length - 1);
  }
  return result;
}
function cleanUpSpecialChars(str) {
	str = str.replace(/[ÀÁÂÃÄÅ]/g, "A");
	str = str.replace(/[àáâãäå]/g, "a");
	str = str.replace(/[ÈÉÊË]/g, "E");
	str = str.replace(/[éèëê]/g, "e");
	str = str.replace(/[ç]/g, "c");
	str = str.replace(/[ìíîï]/g, "i");
	str = str.replace(/[ñ]/g, "n");
	str = str.replace(/[òóôõö]/g, "o");
	str = str.replace(/[ùúûü]/g, "u");
	str = str.replace(/[ýÿ]/g, "y");
  //.... all the rest
  str = str.replace(/[^a-z0-9]/gi, ' ');
  str = str.replace(/\s{2,}/g, ' ');
  //while(str.indexOf('  ')!=-1) str.replace('  ',' ');
  return str; // final clean up
}
function getKeywordsFromCV(cvURL) {
  //console.log('cvURL: ' + cvURL);
  genericAjaxCall('POST', staticVars["urlBackEndCandidate"] + 'cvkeyword/addKeywordsToCandidate?cvURL=' + cvURL, 'application/xml', 'xml', '', postKeywordsToCandidate, '');
}
function postKeywordsToCandidate() {
  //console.log('Extracting keywords : Success');
}
function getKeywordsFromCVError(xml) {
  //console.log('An error while getting keywords from CV');
}
function customJqEasyCounter(selector, maxChars, maxCharsWarning, msgFontColor, msgCaracters) {
  $(selector).jqEasyCounter({
    'maxChars': maxChars,
    'maxCharsWarning': maxCharsWarning,
    'msgFontColor': msgFontColor,
    'msgCaracters': msgCaracters,
  });
}
;
function replaceAllHtmlCast(str) {
  return str.split("'").join("&apos;").split('"').join("&quot;");
}
function factoryshowMustBeConnectPopup() {
  //console.log('factoryshowMustBeConnectPopup');
  $('.mustBeConnected,.backgroundPopup').show();
}
function factoryGetCandidateCvs() {
  $('#docsCVsLink').trigger('click');
  if ($("#myCV .scroll-pane").length > 0) {
    $('#myCV .scroll-pane').jScrollPane();
  }
}
function changeDateToString(date) {
  var dayFrom = date.getDate();
  var monthFrom = date.getMonth() + 1;
  var yearFrom = date.getFullYear();

  if (dayFrom < 10)
    dayFrom = "0" + dayFrom;
  if (monthFrom < 10)
    monthFrom = "0" + monthFrom;

  var fullDate = dayFrom + "/" + monthFrom + "/" + yearFrom;

  return fullDate;
}
function getStatusNoConnected(modalSignIn) {
  $(".menu-container").animate({'left': '-400px'},350);
  $(".menu-container-cover").hide();
  $('.modal-loading').hide();
  connectedUser = false;
  $(".modal-body .temoignage .btn-primary,#loginButtonAction").click(function () {
    $(".menu-container").animate({'left': '-400px'},350);
    $(".menu-container-cover").hide();
  	$('#modal-forgot-password').modal('hide');
    $('#modal-forgot-password-Success').modal('hide');
    $("#modal-sign-in").modal();
  });
  $('#modal-forgot-password').modal('hide');
  $('#modal-forgot-password-Success').modal('hide');
  if(modalSignIn != false) 
  	$('#modal-sign-in').modal('show');
  else setTimeout(function(){
  	if(!connectedUser) $('#modal-sign-in').modal('show');
  }, 300000);
  $('#loged-candidat').addClass('hide');
  $('.home-buttons').removeClass('hide');
  $('#container header').removeClass('connecter');
  $('#container header').addClass('non-connecter');
  $('.main-menu-items-user,.main-menu-container-user,.main-menu-disconenct').addClass('hide');
  $('.main-menu #no-conneted-link').addClass('no-conneted');
  $('p.candidat-fullname').html('');
  $('a.candidat-fullname').html('<span class="caret"></span>');
  $('.candidat-avatar').attr('src', '');
}
function openDocument(document, documentContainer, documentTitleConatiner) {
  if (document != null) {
    documentContainer.empty();
    var document_container = "";
    if (!isIE) {
      document_container = $(
              '<iframe height="100%" width="100%" scrolling="no" allowfullscreen webkitallowfullscreen src="vplayer/mozilla-pdf.js/web/viewer.html?file='
              + document.getUrl()
              + '" type="text/html" class="ad-wrapper"></iframe>')
              .css('position', 'relative');
    } else {
      var filename = getFileName(document.getUrl());
      document_container = $(
              '<object id="mySWF" style="width:100%;height:100%;position:relative;" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000">'
              + '<param name="movie" value="' + staticVars.urlServerStorageSWF + filename + '.swf"/></object>');
    }

    documentContainer.prepend(
            document_container);
    documentTitleConatiner.text(
            substringCustom(formattingLongWords(document.getTitle(), 20), 60));
    analyticsView.viewStandZoneById(DOCUMENT + '/' + DOCUMENTID, document.getAllFilesId(), null);
  }
}

function openProductDocument(document, documentContainer, documentTitleConatiner) {
  if (document != null) {
    var document_container = "";
    if (!isIE) {
      document_container = $(
              '<iframe height="100%" width="100%" scrolling="no" allowfullscreen webkitallowfullscreen src="vplayer/mozilla-pdf.js/web/viewer.html?file='
              + document.getResourceUrl()
              + '" type="text/html" class="ad-wrapper"></iframe>')
              .css('position', 'relative');
    } else {
      var filename = getFileName(document.getResourceUrl());
      document_container = $(
              '<object id="mySWF" style="width:100%;height:100%;position:relative;" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000">'
              + '<param name="movie" value="' + staticVars.urlServerStorageSWF + filename + '.swf"/></object>');
    }

    documentContainer.prepend(
            document_container);
    documentTitleConatiner.text(
            substringCustom(formattingLongWords(document.getResourceName(), 20), 60));
    analyticsView.viewStandZoneById(DOCUMENT + '/' + DOCUMENTID, document.getResourceId(), null);
  }
}
function openPhoto(photo, photoContainer, photoTitleConatiner) {
	  if (photo != null) {
	    	photoContainer.html('<img alt="'+htmlEncode(photo.getTitle())+'" src="' + photo.getUrl() + '">');
	    	photoTitleConatiner.text(
	                substringCustom(formattingLongWords(photo.getTitle(), 20), 60));
	        analyticsView.viewStandZoneById(PHOTO+'/'+PHOTOID, photo.getAllFilesId(), null);
	  }
}

function goToStandById (standId) {
  $('#contentG').attr('class', '');
  $('#contentG').attr('class', 'info-stand stand' + standId);
  $(".main-menu .sub-main-menu, .second-menu li a").removeClass("active");
  if (standId == 7) {
    $('a.icon-format3').addClass('active');
  }
  $('#modal-forgot-password').modal('hide');
  $('#modal-forgot-password-Success').modal('hide');
  $('#modal-sign-in').modal('hide');
  $('#page-content').removeClass('no-padding');
  $('#vue-aerienne-info-stand').removeClass('hide');
  $('.genericContent').attr('style', 'display: none;');
  $('#videoHome').hide();
  $('#info-stand').removeClass('hide');

  $('.headerTop').hide();
  $('.mainContentAbsolutePage').addClass('staticPosition'); 
  $('body').addClass('bodyStyleCustom');
  $(".icon-tchat,.menu-btn-container .fa-search,.icon-reorder,.fa-bell ,#message-container .icon-envelope").addClass('greenColor');
  $('#content').show();
  $("#mainPageContent,.categList").show();
  $(".categList, .backToListStand").hide();

  $('#info-stand').attr('style', 'display: block;');
  $('.menu-stand a,.vue-aerienne-info-stand a').attr('standId', 'standId=' + standId);  
  $('#hall1MainContainer').attr('style','background: url(img/stand-'+standId+'.jpg);background-position: 0px -168px;background-size: 100%;');

  //$('#back-to-stand').closest('div').hide();
  initStandInfoController(standId);
}

function updateCurrentTime(selector) {
  $(selector).html(moment().format('LLLL'));
  setTimeout(updateCurrentTime, 60000, selector);
  return true;
}

function updateSessionTime(selector, hrs, mins, secs) {
  if (secs == 59) {
    mins++;
    secs = 0;
  } else {
    secs++;
  }

  if (mins == 59) {
  	hrs++;
    mins = 0;
    secs = 0;
  }

  $(selector).html(SESSIONLABEL+" : " + getStringElementDateNumber(hrs) + ":" + getStringElementDateNumber(mins)
  		+ ':' + getStringElementDateNumber(secs));
  setTimeout(updateSessionTime, 1000, selector, hrs, mins, secs);
}

function openVideo(video, videoContainer, videoTitleContainer) {
  if (video != null) {
    var isFavorite = factoryFavoriteExist(video.getAllFilesId(), FavoriteEnum.MEDIA);
    //var displayDeleteFavorite = "block";
    //var displayAddFavorite = "block";
    var videoContentHandler = null;
    var videoId = video.getAllFilesId();
    var videoIdName = null;
    if (isFavorite)
      displayAddFavorite = "none";
    else
      displayDeleteFavorite = "none";

    $(videoContainer).empty();


    videoTitleContainer.text(substringCustom(formattingLongWords(video.getTitle(), 20), 60));
//    var favoriteHtml = '<a href="#" class="btn btn-primary col-md-9 col-sm-9 deleteVideoRHFromFavoriteButton_' + videoId + '" style="display:' + displayDeleteFavorite + '" >'
//	    + '<i class="glyphicon glyphicon-star" aria-hidden="true">'
//	    + '</i>' + DELETEFROMFAVORITELABEL
//	    + '</a>'
//	    + '<a class="btn green col-md-9 col-sm-9 addVideoRHToFavoriteButton_' + videoId + '" style="display:' + displayAddFavorite + '">'
//	    + '<i class="glyphicon glyphicon-star" aria-hidden="true">'
//	    + '</i>' + ADDTOFAVORITELABEL
//	    + '</a>';
	  if (videoContainer == "#content_video_rh .ad-image-wrapper") {
	    videoIdName = "playerVideo";
	  } else {
	  	videoIdName = "playerVideoStand";
	  }
    videoContentHandler = $('<div id="'+videoIdName+'" class="ad-wrapper"></div>'
        + '<div class="captionCon" style="overflow-y: auto; overflow-x: hidden;"></div>');
    $(videoContainer).append(videoContentHandler);
    playJwPlayer(videoIdName, video.getUrl());
    videoContainer.find('.deleteVideoRHFromFavoriteButton_' + videoId).bind('click', function () {
      $('.deleteVideoRhFavoriteId_' + videoId).trigger('click');
    });

    videoContainer.find('.addVideoRHToFavoriteButton_' + videoId).bind('click', function () {
      $('.addVideoRhFavoriteId_' + videoId).trigger('click');
    });
    analyticsView.viewStandZoneById(VIDEO + '/' + VIDEOID, videoId, null);
  }
}

function openProductVideo(video, videoContainer, videoTitleContainer) {
  if (video != null) {
    $(videoContainer).empty();
    videoTitleContainer.text(substringCustom(formattingLongWords(video.getResourceName(), 20), 60));
    
    if (videoContainer == "#content_video_rh .ad-image-wrapper") {
      videoIdName = "playerVideo";
    } else {
      videoIdName = "playerVideoStand";
    }
    videoContentHandler = $('<div id="'+videoIdName+'" class="ad-wrapper"></div>'
        + '<div class="captionCon" style="overflow-y: auto; overflow-x: hidden;"></div>');
    $(videoContainer).append(videoContentHandler);
    playJwPlayer(videoIdName, video.getResourceUrl());
    analyticsView.viewStandZoneById(VIDEO + '/' + VIDEOID, video.getResourceId(), null);
  }
}

//------------- File Uploade CV-------------//
function  getDocumentUpload () {
  var timeUpload = [];
  return {
    beforeSend: function (jqXHR, settings) {
    },
    datatype: 'json',
    cache: false,
    add: function (e, data) {
      $('.modal-loading').show();
      var goUpload = true;
      var uploadFile = data.files[0];
      var fileName = uploadFile.name;
      timeUpload[fileName] = (new Date()).getTime();
      data.url = getUrlPostUploadFile(timeUpload, fileName);
      if (!regexDoc.test(fileName) && !regexImg.test(fileName)) {
        swal(DOWNLOADERRORLABEL, DOWNLOADEXTENSIONERRORLABEL, "error");
        $('.modal-loading').hide();
        goUpload = false;
      }

      if (goUpload == true) {
        if (uploadFile.size > MAXFILESIZE) {// 6mb
          swal(DOWNLOADERRORLABEL, DOWNLOADSIZEERRORLABEL, "error");
          $('.modal-loading').hide();
          goUpload = false;
        }
      }
      if (goUpload == true) {
        jqXHRResource = data.submit();
      }
    },
    progressall: function (e, data) {
    },
    done: function (e, data) {
      var file = data.files[0];
      var fileName = file.name;
      var hashFileName = getUploadFileName(timeUpload[fileName] , fileName);
      //console.log("Add file done, filename : " + fileName + ", hashFileName : " + hashFileName);
      var urlCache = getUploadFileUrl(hashFileName);
      $('#documents-product-list').append(urlCache+',');
      $('#documents-product-list-uploaded').append('<a href="'+urlCache+'" target="_blank">'+htmlEncode(fileName)+'</a><br>');
      $('.modal-loading').hide();
      swal(DOWNLOADLABEL, DOCUMENTSENTLABEL, "success");
    }
  };
}

function openPresse(presse, videoContainer, videoTitleContainer) {
  if (presse != null) {
    var presseContainer = "";
    var presseUrl = presse.getUrl();
    videoTitleContainer.text(substringCustom(formattingLongWords(presse.getTitle(), 20), 40));
    if (presseUrl != null && presseUrl.length > 0) {
      if (!isIE) {
      	presseContainer = $('<iframe height="100%" width="100%" scrolling="no" allowfullscreen webkitallowfullscreen src="vplayer/mozilla-pdf.js/web/viewer.html?file='
                + presseUrl
                + '" type="text/html" class="ad-wrapper" style="position: relative; height: 300px; width: 645px;"></iframe>'
                + '<div class="captionCon" style="overflow-y: auto; overflow-x: hidden;">'
                + '</div>')
                .css('position', 'relative');
      } else {
        var filename = getFileName(presseUrl);
        presseContainer = $('<object id="mySWF" style="width:100%;height:100%;position:relative;" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000">'
                + '<param name="movie" value="' + staticVars.urlServerStorageSWF + filename + '.swf"  style="position: relative; height: 300px; width: 645px;"/></object>'
                + '<div class="captionCon" style="overflow-y: auto; overflow-x: hidden;">'
                + '</div>');
      }
    	$(videoContainer).html(presseContainer);
    } else {

	   //var iframe = $('<iframe style="width:100%; height:100%;border: none;">');
      presseContainer = $('<div class="presse-description scroll-pane"><div class="presse-description-content"></div></div>');
      $(videoContainer).html(presseContainer);
    	/*presseContainer.find('.presse-description-content').html(iframe);
    	iframe = (iframe[0].contentWindow) ? iframe[0].contentWindow : (iframe[0].contentDocument.document) ? iframe[0].contentDocument.document : iframe[0].contentDocument;
    	iframe.document.open();
    	iframe.document.write(unescape(presse.getContent()));
    	iframe.document.close();*/
    	presseContainer.find('.presse-description-content').html(unescape(presse.getContent()).split("\n").join("<br/>"));
      if($(videoContainer).closest('#modal-document-lecteur-big').length == 0){
          $(videoContainer).find('.scroll-pane').jScrollPane();
      }
    }
    analyticsView.viewStandZoneById(ADVICE+'/'+ADVICEID, presse.getAdviceId(), null);
  }
}

//------Cookie------//
function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  var expires = "expires=" + d.toGMTString();
  document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ')
      c = c.substring(1);
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function reducePublicChat() {
  $('.tchat').removeClass("open");
}

function factoryActivateFaIcon(that){
  if ($(that).parent().hasClass('active')) {
    return false;
  }
  $(".menu-stand ul li a").parent().removeClass('active');
  $(that).parent().addClass('active');
}
function validateDateChamp(d) {
    if (Object.prototype.toString.call(d) === "[object Date]") {
        // it is a date
        if (isNaN(d.getTime())) {  // d.valueOf() could also work
            // date is not valid
            return false;
        }
        else {
            // date is valid
            return true;
        }
    }
    else {
        // not a date
        return false;
    }
}
function getAgendaUTCDateEvent(d) {
    var year = d.getUTCFullYear();
    var month = getStringElementDateNumber(d.getUTCMonth() + 1);
    var day = getStringElementDateNumber(d.getUTCDate());
    var hours = getStringElementDateNumber(d.getUTCHours());
    var minutes = getStringElementDateNumber(d.getUTCMinutes());
    var dateUTCString = [day, month, year].join('-');
    timeUTCString = [hours, minutes].join(':');
    dateUTCString = [dateUTCString, timeUTCString].join(' ');
    // console.log("**************dateUTCString :
    // "+dateUTCString+"****************");
    return dateUTCString;
}

function initStandInfoController(standId){
	if (infoStandController == null) {
    infoStandController = new $.InfoStandController(
    		new $.InfoStandModel(), new $.InfoStandView());
  } 
  infoStandController.initController(standId);
}
function addTableDatas (selector, datas){
	var oTable = $(selector).dataTable();
  oTable.fnClearTable();
  if(datas.length > 0) oTable.fnAddData(datas);
  oTable.fnDraw();
}
function stripScripts(s) {
  var div = document.createElement('div');
  div.innerHTML = s;
  var scripts = div.getElementsByTagName('script');
  var i = scripts.length;
  while (i--) {
    scripts[i].parentNode.removeChild(scripts[i]);
  }
  return div.innerHTML;
}

function reloadSponsorJcarousel(){
  $(".sliderG").html('<div class="sponsor">'
          +'<ul>'
              +'<li class="slide"><a href="http://www.compusFrance.org.ma/fr/" target="_blank" title=""><img src="img/your_logo_here.jpg" alt=""></a></li>'
              +'<li class="slide"><a href="http://www.compusFrance.org.ma/fr/" target="_blank" title=""><img src="img/your_logo_here.jpg" alt=""></a></li>'              
              +'<li class="slide"><a href="http://www.compusFrance.org.ma/fr/" target="_blanc" title=""><img src="img/your_logo_here.jpg" alt=""></a></li>'
              +'<li class="slide"><a href="http://www.compusFrance.org.ma/fr/" target="_blank" title=""><img src="img/your_logo_here.jpg" alt=""></a></li>'  
          +'</ul>'
      +'</div>');
  $('.sponsor').jcarousel({
      wrap: 'circular'
  })
          .jcarouselAutoscroll({
              target: '+=1',
              interval: 2000
          });
}

function connectToPrivateChat(userId){
	if(tChatFlashLoaded){
		var chatApp = document.getElementById("ChatApp");
		if (chatApp != undefined && chatApp != null) { 
			try {
				chatApp.connectToPrivateChat(userId);
		  } catch(err) {
		     console.log("There was an error on the flex callback.");
		     console.log(err);     
		  }
	  }
	}else{
		setTimeout(connectToPrivateChat, 3000, userId);
	}
}

function isTChatFlashLoaded(){
	tChatFlashLoaded = true;
}

function getWidth(){
  var pageWidth = $( window ).width();
  return pageWidth;
}

function getHtml(element) {
  if (element != null)
    return $("<div></div>").html(element).html();
}

function incrementIpNbrUsed(ip, topic, token){
  if(ip != null && ip != undefined){
    genericAjaxCall('POST', SERVER_BC_URL+'chat/incrementIpNbrUsed?ip='+ip+'&topic='+topic+'&token='+token, 'application/xml', 'xml', '', 
      function(xml){
          //console.log("success");
      }, function(xhr, status, error){
        isSessionExpired(xhr.responseText);
      });
  }
}
function decrementIpNbrUsed(ip, topic, token){
  if(ip != null && ip != undefined){
    genericAjaxCall('POST', SERVER_BC_URL+'chat/decrementIpNbrUsed?ip='+ip+'&topic='+topic+'&token='+token, 'application/xml', 'xml', '', 
        function(xml){
          //console.log("success");
        }, function(xhr, status, error){
          isSessionExpired(xhr.responseText);
        });
  }
}

function getAtmosphereIp(callBack){
  genericAjaxCall('GET', SERVER_BC_URL+'chat/get/ip', 'application/xml', 'xml', '', 
      function(xml){
        callBack($(xml).find("s").text());
      }, function(xhr, status, error){
        isSessionExpired(xhr.responseText);
      });
}

function getAMSIp(callBack){
  genericAjaxCall('GET', SERVER_BE_URL+'chat/ams/get/ip', 'application/xml', 'xml', '', 
      function(xml){
        //console.log("****** IP : "+$(xml).find("s").text()+" ******");
        callBack($(xml).find("s").text());
      }, function(xhr, status, error){
      });
}