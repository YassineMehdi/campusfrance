var listLangsEnum = []; //cet un arrays qui contient la liste des langs ID+Name ...
var listLangsAbrEnum = []; //cet un arrays qui contient la liste des abbréviations des langues...
var HOSTNAME_URL = window.location.hostname;
//var HOSTNAME_URL = "jobadvices.eventds.net";//
//var HTML_URL = 'http://'+HOSTNAME_URL+'/BackEndChat/';//172.31.47.158
var HTML_URL = 'http://:ip:/BackEndChat/'; //172.31.47.158
//var HTML_URL = 'http://172.31.47.158/BackEndChat/';
var SERVER_BE_URL = "http://" + HOSTNAME_URL + "/BackEndEnterprise/rest/";
var SERVER_BC_URL = "http://" + HOSTNAME_URL + "/BackEndCandidate/rest/";
var HTML_URL_FCM = "http://" + HOSTNAME_URL + "/mobile/";
var IMAGES_URL = "";
var PROFILE_URL = "http://" + HOSTNAME_URL + "/mobile/profileCandidate.html?candidateId=";
var FMS_URL = "rtmp://:ip:/"; //192.168.1.17,50.112.243.218
//var FMS_URL = "rtmp://52.49.92.74/";//192.168.1.17,50.112.243.218
var FMS_APPLICATION_CHAT = "chatfmscompusFrance";
var EXTENDERTIMESTAMP = 600000; //10min:600000
var SEPARATEPUBLICCHATSO = "_";
var ADD_MESSAGE = "Add_Message";
var UPDATE_MESSAGE = "Update_Message";
var REMOVE_MESSAGE = "Remove_Message";
var ADD_STAND = "Add_Stand";
var UPDATE_STAND = "Update_Stand";
var REMOVE_STAND = "Remove_Stand";
var PUBLIC_CHAT = "Public_Chat";
var STAND_CHAT = "Stand_Chat";
var chatflashvars = {};
var standflashvars = {};
var userType = "Candidate";
var candidateCVLogo = "http://" + HOSTNAME_URL + "/mobile/images/default_avatar.jpg";
var listStands = new Array();
var currentChatStand = 0;
var currentJobOffer = 0;
var userId = "";
var fullName = "";
var photoURL = "";
var profileUrl = "";
var chat_loaded = false;
var topicStand = "";
var translateUrl = "";
var tryToReconnect = 0;
var CSTAG = "_CS";
var CGTAG = "";
var currentChatStandLanguageId = null;
var MAXUPLOADCV = 9;
var candidateLogin = '';
var candidateLanguage = 'fr';
var currentStandId = 0;
var SEPARATESTANDS = ";";
var SEPARATESTANDANDLANGS = "_";
var SEPARATELANGSSTAND = ":";
var currentPage = "";
var candidatePhoto = "http://" + HOSTNAME_URL + "/visitor/images/default_avatar.jpg";
var urlMediaTemp = "http://" + HOSTNAME_URL + "/exhibitor/";
var candidateCvReferrer = window.document.referrer;
var standCache = "";
var viewStandList = false;
var showOnlySectors = false;
var showOnlyStandsConnected = false;
var allJobOffers = false;
var allDocuments = false;
var LEFTCARACTERES = "";
var publicationCash = "";
var tChatFlashLoaded = false;
var pushToken = new Date().getTime();

var staticVars = {
    urlBackEndCandidate: "http://" + HOSTNAME_URL + "/BackEndCandidate/rest/",
    urlBackEndEnterprise: "http://" + HOSTNAME_URL + "/BackEndEnterprise/rest/",
    urlvisitor: "http://" + HOSTNAME_URL + "/visitor/",
    urlexhibitor: "http://" + HOSTNAME_URL + "/exhibitor/",
    urlServerStorage: "https://s3-eu-west-1.amazonaws.com/africtalentsstorage/",
    urlServerStorageSWF: "https://s3-eu-west-1.amazonaws.com/africtalentsstorage/",
    urlServerUploader: 'http://52.213.163.11:81/amazon_uploader1/php_uploader/server/php/',
    urlServerPDF2Text: 'http://52.213.163.11:81/pdf2Text/',
    urlValidateCaptcha: "http://" + HOSTNAME_URL + ":81/recaptcha/captcha.php",
    //		urlServerUploader : 'http://backoffice.jobadvices.ch:81/amazon_uploader/php_uploader/server/php/'
    //		urlServerUploader : 'http://'+HOSTNAME_URL+':81/amazon_uploader/php_uploader/server/php/'
};

var fromFavorite = false;
var fromSearch = false;
var fromStand = false;
var chatTabs = null;
var sessionExtenderInterval = null;
var ua = navigator.userAgent.toLowerCase();
var check = function(r) {
    return r.test(ua);
};

var isIe_Ver = function() {
    var trident = !!navigator.userAgent.match(/Trident\/7.0/);
    var net = !!navigator.userAgent.match(/.NET4.0E/);
    var IE11 = trident && net;
    var IEold = (navigator.userAgent.match(/MSIE/i) ? true : false);
    if (IE11 || IEold) {
        return true;
    } else {
        return false;
    }
};
var isLinux = check(/linux/);
var isChrome = check(/chrome/);
var isIE = isIe_Ver();
var defaultBrowser = (isLinux) || (isIE);
var currentGroupStandId = 0;

// an object containing all stands ids
var StandIDS = {
    ATOM: 1,
    CORP: 2,
    ESHOP: 3,
    GWAVES: 4,
    SPARK: 5,
    TECH: 6,
    STANDINFO: 7,
};

var FavoriteEnum = {
    MEDIA: "AllFiles",
    JOBOFFER: "JobOffer",
    CONTACT: "Contact",
    ADVICE: "Advice",
    TESTIMONY: "Testimony",
    STAND: "Stand",
    USER: "User",
    JOBAPPLCATION: "JobApplication",
    PRODUCT: "Product",
    SOCIAL_NETWORK: "SocialNetwork",
    VISIT_CARD_CANDIDATE: "VisitCardCandidate",
    VISIT_CARD_ENTERPRISE_P: "VisitCardEnterpriseP",
};


var FavoriteIdsEnum = {
    VISIT_CARD_CANDIDATE_ID: "12",
    VISIT_CARD_ENTERPRISE_P_ID: "13",
};

var VisitCardStatusEnum = {
    INVITER: "INVITER",
    GUEST: "GUEST"
};


var sectorEnum = {
    1: "AerospaceAutomotive",
    2: "Handicrafts",
    3: "Construction",
    4: "SMEStartUp",
    5: "Chemistry",
    6: "EnergyWater",
    7: "GreenBusiness",
    8: "ITServices",
    9: "AgriculturalProductsAgribusiness",
    10: "FishIndustry",
    11: "TextileLeather",
    12: "Tourism",
};

var MediaEnum = {
    POSTER: 1,
    VIDEO: 2,
    PHOTO: 3,
    DOCUMENT: 4,
};

var PageEnum = {
    FORUM_REGISTRATION: "FORUM REGISTRATION",
    HOME_PAGE: "HOME PAGE",
    PROFILE_CANDIDATE: "PROFILE CANDIDATE"
};

var languagesIds = {
    DEUTSCH: 1,
    ENGLISH: 2,
    FRENCH: 3,
};

var GroupStandEnum = {
    BRONZE: 1,
    SILVER: 2,
    GOLD: 3,
    STAND_INFO: 4
};

var listUserFavorites = new Array(); // on array where we put the user favorites=======
var FORUM_TITLE = "EACCE Morocco: salon virtuel dédié à l'export";

var analyticsView = null;
var STANDCHATNOTDISPO = "";
var RECEPTION = 'Reception';
var MULTIMEDIA = 'Multimedia';
var PRESSE = 'Presse';
var STANDID = 'StandId';
var LANGUAGEID = "LanguageId";
var ZONE = 'Zone';
var PROFILE = 'Profile';
var TESTIMONIES = 'Testimonies';
var POSTERS = 'Posters';
var ADVICES = 'Advices';
var SENDMESSAGE = 'SendMessage';
var POSTULATE = 'Postulate';
var CONSTACTS = 'Contacts';
var WEBSITES = 'Websites';
var AGENDA = 'Agenda';
var JOBOFFERS = 'JobOffers';
var SURVEYS = 'Surveys';
var CHATTER = 'Chatter';
var POSTULATECV = 'PostulateCV';
var SOCIALNETWORK = 'SocialNetwork';
var QUESTION_INFORMATION = 'QuestionInformation';
var QUESTION_INFORMATIONS = 'QuestionInformations';
var VIDEOS = 'Videos';
var DOCUMENTS = 'Documents';
var PHOTOS = 'Photos';
var PRODUCTS = 'Products';
var VISITORS = 'Visitors';
var VIDEO = 'Video';
var DOCUMENT = 'Document';
var PHOTO = 'Photo';
var POSTER = 'Poster';
var SURVEY = 'Survey';
var JOBOFFER = 'JobOffer';
var ADVICE = 'Advice';
var PRODUCT = 'Product';
var VIDEOID = 'VideoId';
var DOCUMENTID = 'DocumentId';
var PHOTOID = 'PhotoId';
var POSTERID = 'PosterId';
var SURVEYID = 'SurveyId';
var JOBOFFERID = 'JobOfferId';
var ADVICEID = 'AdviceId';
var JOBOFFER_ID = 'JobOfferId';
var PRODUCT_ID = 'ProductId';
var VISITOR_ID = 'VisitorId';
var WEBSITE = 'Website';
var CAREER = 'Career';
var BLOG = 'Blog';
var FACEBOOK = 'Facebook';
var XING = 'Xing';
var TWITTER = 'Twitter';
var YOUTUBE = 'Youtube';
var LINKEDIN = 'LinkedIn';
var WEBSITEID = 'WebsiteId';
var PUBLICHCHAT = "PublicChat";
var PUBLIC = "public";
var PRIVATE = "private";
var FORUM = "Forum";
var CANDIDATE = "Candidate";
var dataTableLang = "French";
//var completeCV = false; // when we want to apply for job we test if the candidate has fill his CV EDS
var currentCandidateId = 0;

var agendaOfAll = true;

var videoLaunching = false;
var CHARGEMENT = 'Chargement';
var _AnalyticsCode = 'UA-128951072-1'; //UA-50762001-1
var TRACKEVENT = '_trackEvent';
var urlWorkers = 'analytics/js/worker_analytics.js';
var myWorker = "";
var workerSupport = window.Worker;
var contentSize = 0;
var rapportWidHeig = 65 / 170;
var goldExibitorsSize = 0;
var imgWidth = 0;
var imgHeight = 0;
var cacheAjax = true;
var isIE = false;
var playlist = {
    1: "OXTIk28rktw",
    2: "13yMNrV_0Ak",
    3: "MxRz2Hw4zAI",
    4: "G_bxRTitzig",
    5: "N3amy1QIglU"
};
var SEPARATEUPLOAD = "_EDSSEPEDS_";
var xmlLanguage = "";
var timeAddCV = null;
var timePictureUpload = null;
var timeCVUpload = null;
var MAXFILESIZE = 6291456; // 6mb //6291456
var listFavoriteJobOffers2 = new Array();
var listFavoriteDocuments2 = new Array();
var listFavoriteVideos2 = new Array();
var listFavoritePosters2 = new Array();
var listFavoritePhotos2 = new Array();
var listFavoriteContacts2 = new Array();
var listFavoriteTestimonies2 = new Array();
var listFavoriteAdvices2 = new Array();
var listDisplayedFavoriteOffers = null;
var listDisplayedFavoriteVideos = null;
var listDisplayedFavoriteDocuments = null;
var listDisplayedFavoritePosters = null;
var listDisplayedFavoriteAdvices = null;
var listDisplayedFavoritePhotos = null;
var listDisplayedFavoriteContacts = null;
var listDisplayedFavoriteTestimonies = null;
var conversationsDisplayed = [];
var reverse = 0;

var listStandsCache = null;

var userFavoritesController = null;
var userFavoritesModel = null;
var userFavoritesView = null;
var userCareerController = null;
var userMessagesModel = null;
var userMessagesView = null;
var userMessagesController = null;
var userMyChatsController = null;
var jobOfferController = null;
var MAX_EVENT_TITLE_SIZE = 12;
var SEPARATE_FAVORITE_KEY = "_";
var ALL = "all";
/*****************************traduction term***************************************/
var TESTIMONIESLABEL = "";
var loginNotValidLabel = "";
var LOGINNOTVALIDLABEL = "";
var ACTIVATACCOUNTLABEL = "";
var YESLABEL = "";
var EXHIBITIONHALLLABEL = "";
var ADDTOFAVORITELABEL = "";
var DELETEFROMFAVORITELABEL = "";
var CANTONLABEL = "";
var STUDYLEVELLABEL = "";
var FUNCTIONLABEL = "";
var WORKPERMITLABEL = "";
var EXPERIENCEYEARSLABEL = "";
var ACTIVITYSECTORSLABEL = "";
var VIEWLABEL = "";
var BEGINLABEL = "";
var ANSWEREDLABEL = "";
var ENTERPRISELABEL = "";
var JOBTITLELABEL = "";
var ACTIONSLABEL = "";
var VISITSTANDLABEL = "";
var CHARGEMENTLABEL = "";
var STANDLABEL = "";
var STANDSLABEL = "";
var CONNECTEDLABEL = "";
var TITLELABEL = "";
var DATELABEL = "";
var PERIODLABEL = "";
var DOCUMENTSLABEL = "";
var PUBLICCHATLABEL = "";
var VIDEOSLABEL = "";
var WORKTYPELABEL = "";
var TYPELABEL = "";
var APPLYLABEL = "";
var RETURNLABEL = "";
var USERNAMELABEL = "";
var EMAILLABEL = "";
var USERNAMEEMAILLABEL = "";
var PASSWORDLABEL = "";
var SEARCHLABEL = "";
var CONNECTEDSTANDSLABEL = "";
var SEARCHBYSECTORLABEL = "";
var CONNECTTOCHATLABEL = "";
var REDUCECHATLABEL = "";
var REGISTERLABEL = "";
var MYACCOUNTLABEL = "";
var MYPROFILELABEL = "";
var FAVORITESLABEL = "";
var MESSAGESLABEL = "";
var CAREERLABEL = "";
var MYCVLABEL = "";
var FROMLABEL = "";
var TOLABEL = "";
var OBJECTLABEL = "";
var REQUIREDLABEL = "";
var PHOTOLABEL = "";
var FIRSTANDLASTNAMELABEL = "";
var JOBTITLELABEL = "";
var DELETELABEL = "";
var WITHDRAWLABEL = "";
var DATEMUSTGREATERLABEL = "";
var OKNEXTLABEL = "";
var PLEASESELECTDATEBIRTHLABEL = "";
var UNSOLICITEDAPPLICATIONLABEL = "";
var CHOOSELABEL = "";
var LANGUESLABEL = "";
var NOLABEL = "";
var STANDNOTYETCONNECTEDLABEL = "";
var EMAILNOTVALIDLABEL = "";
var LOGINALREADYEXISTSLABEL = "";
var EMAILALREADYEXISTSLABEL = "";
var OpDE = "none";
var ALLRECRUITERSSTANDDISCONNECTEDLABEL = "";
var OKLABEL = "";
var ENTERPRISESCONNECTED = "";
var EXTENDCHATSCREENLABEL = "";
var REDUCECHATSCREENLABEL = "";
var SENDLABEL = "";
var CLOSELABEL = "";
var RECRUITERSNUMBERCHATLABEL = "";
var LANGUECHATLABEL = "";
var LANGAUGETAGLOCALELABEL = "";
var LANGUAGEDATEPICKERFORMATLABEL = "";
var ACCOUNT_NOT_YET_APPROVED_LABEL = "";
var OPENLABEL = "";
var PRIVATECHATLABEL = "";
var MYCHATSLABEL = "";
var CHATTYPELABEL = "";
var SAVETHISCHATLABEL = "";
var STOPSAVINGLABEL = "";
var PLEASETRYAGAINLABEL = "";
var ALLLABEL = "";
var STAND_CONNECTED_LABEL = "";
var STAND_DISCONNECTED_LABEL = "";
var CVNAMELABEL = "";
var PLEASEUPLOADCVLABEL = "";
var TODAYLABEL = "";
var DEPOTDOCUMENTSUCCESSLABEL = "";
var SENDERRORLABEL = "";
var CHOOSEDOCUMENTLABEL = "";
var DOWNLOADERRORLABEL = "";
var DOWNLOADEXTENSIONERRORLABEL = "";
var DOWNLOADSIZEERRORLABEL = "";
var DOWNLOADLABEL = "";
var DOWNLOADSUCCESSLABEL = "";
var NOPOSTER = "";
var NOCONTACT = "";
var DELETEFROMFAV = "";
var VIEWLABEL = "";
var CREATEACCOUNTSUCCESSLABEL = "";
var UPDATEACCOUNTSUCCESSLABEL = "";
var EACCETEAMLABEL = "";
var THANKSLABEL = "";
var VISITSTANDLABEL = "";
var SAVEMESSAGELABEL = "";
var ALREADYEXISTEMAILLABEL = "";
var SAVEERRORLABEL = "";
var DOWNEXTENSIONERRORLABEL = "";
var DOWNERRORLABEL = "";
var DOWNLOADLABEL = "";
var DOWNLOADSUCCESSLABEL = "";
var ALREADYREGISTREDLABEL = "";
var CANCELLABEL = "";
var PLEASWAITLABEL = "";
var INCORRECTPASSLABEL = "";
var NODOCUMENTSLABEL = "";
var LANGUAGELABEL = "";
var MESSAGESENTLABEL = "";
var NOPROFILSEARCHEDLABEL = "";
var NODEMANDELABEL = "";
var NORESPONSELABEL = "";
var UPDATESUCCESSLABEL = "";
var PICACCEPTEDEXTENSIONLABEL = "";
var PICSIZELABEL = "";
var NOPICTURELABEL = "";
var INFOREQUESTLABEL = "";
var NOPRODUCTLABEL = "";
var PARTICIPATELABEL = "";
var CHOOSEONERESPONSEATLEASTLABEL = "";
var NOSURVEYLABEL = "";
var SEEMORELABEL = "";
var SEELESSLABEL = "";
var NOTESTIMONYLABEL = "";
var NOVIDEOLABEL = "";
var SESSIONLABEL = "";
var ENTERPRISESLABEL = "";
var ACTIVITYSECTORLABEL = "";
var SELECTLABEL = "";
var FRENCHLABEL = "";
var ENGLISHLABEL = "";
var ARABICLABEL = "";
var RUSSIANLABEL = "";
var GERMANLABEL = "";
var SPANISHLABEL = "";
var REQUESTSUCCESSLABEL = "";
var DATASIZELABEL = "";
var DOCSIZELABEL = "";
var AVATARLABEL = "";
var FIRSTNAMELABEL = "";
var LASTNAMELABEL = "";
var EXCHANGE = "";
var INVITE_TO_TCHAT_LABEL = "";
var DESCRIPTIONLABEL = "";
var EMAILLOGINERRORLABEL = "";
var NEXTLABEL = "";
var BACKLABEL = "";
var SKIPLABEL = "";
var DONELABEL = "";
var ADDLABEL = "";
var SELECTED_LABEL = "";
var THANK_YOU_REMOVE_SPACES_LABEL = "";
var THANK_YOU_RESPECT_INDICATED_FORMAT_LABEL = "";
var HOME_PAGE_LABEL = "";
var LET_MESSAGE_LABEL = "";
var DEMAND_IN_PROGRESS_LABEL = "";
var ACCEPT_LABEL = "";
var REFUSE_LABEL = "";
var FAIR_SHOW_LABEL = "";
var SPONSORS_LABEL = "";

var listNotifCandidates = null;
var listNotifStand = null;

var conversationIndex = -1;
var notificationConversationIndex = -1;
var toCareerResponse = -1;
var toQuestionInfoResponse = false;
var formElements = [];
var nbreWSCallsInProgress = 0;
var nbreMenuOption2WSCallsInProgress = 0;
var nbreActivationWSCallsInProgress = 0;
var nbreForgetPasswordWSCallsInProgress = 0;
var nbreForumRegistrationWSCallsInProgress = 0;
var idCvToChange = 0;
var forumRegistrationController = null;
var homePageController = null;
var profileCandidateController = null;
var salleExpositionController = null;
var infoStandController = null;
var genericSearchController = null;
var documentsController = null;
var productsController = null;
var standDocumentController = null;
var videoRhController = null;
var testimonysController = null;
var affichesController = null;
var agendasController = null;
var multimediasController = null;
var myCvController = null;
var videoStandController = null;
var photoStandController = null;
var presseStandController = null;
var agendasFullController = null;
var productStandController = null;
var deposerDocumentStandController = null;
var myProfilController = null;
var sondagesStandController = null;
var myAccountController = null;
var visitCardListController = null;
var myCareerController = null;
var inboxController = null;
var notificationsController = null;
var contactStandController = null;
var chatController = null;
var sendMessageController = null;
var standTChatController = null;
var myQuestionInfoController = null;

var connectedUser = false;
var candidatGlobal = null;
var regexDoc = /\.(pdf|ppt|pptx|doc|docx)$/i;
var regexImg = /\.(jpg|jpeg|png|PNG|gif)$/i;
var regexEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;