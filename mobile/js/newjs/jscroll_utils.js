$(document).ready(function() {
	/**************************** Scroll *******************************/
	$('#modal-profile-entreprise').on('shown.bs.modal', function() {
		$(this).find('.scroll-pane').jScrollPane({ autoReinitialise: true });
	});
	$('#modal-temoignage').on('shown.bs.modal', function() {
		$(this).find('.scroll-pane').jScrollPane({ autoReinitialise: true });
	});
	$('#tchat-buttons').click(function() {
		$(this).find('.scroll-pane').jScrollPane({ autoReinitialise: true });
	});
	$('#modal-message').on('shown.bs.modal', function() {
		$(this).find('.scroll-pane').jScrollPane({ autoReinitialise: true });
	});
	$('#modal-sondage').on('shown.bs.modal', function() {
		$(this).find('.scroll-pane').jScrollPane({ autoReinitialise: true });
	});
	$('#modal-sondages-details').on('shown.bs.modal', function() {
		$(this).find('.scroll-pane').jScrollPane({ autoReinitialise: true });
	});
	$('#modal-offre-emploi').on('shown.bs.modal', function() {
		$(this).find('.scroll-pane').jScrollPane({ autoReinitialise: true });
	});
	$('#modal-offre-emploi-apercu').on('shown.bs.modal', function() {
		$(this).find('.scroll-pane').jScrollPane({ autoReinitialise: true });
        $('.modal-loading').hide();
	});
	$('#modal-profile-entreprise-agenda').on('shown.bs.modal', function() {
		$(this).find('.scroll-pane').jScrollPane({ autoReinitialise: true });
	});
	$('#modal-affiches').on('shown.bs.modal', function() {
		$(this).find('.scroll-pane').jScrollPane({ autoReinitialise: true });
	});
	$('#modal-contact').on('shown.bs.modal', function() {
		$(this).find('.scroll-pane').jScrollPane({ autoReinitialise: true });
	});
	$('#modal-document').on('shown.bs.modal', function() {
		$(this).find('.scroll-pane').jScrollPane({ autoReinitialise: true });
	});
	$('#modal-deposer-document').on('shown.bs.modal', function() {
		$(this).find('.scroll-pane').jScrollPane({ autoReinitialise: true });
	});
	$('#modal-presse').on('shown.bs.modal', function() {
		$(this).find('.scroll-pane').jScrollPane({ autoReinitialise: true });
	});
	$('#modal-video').on('shown.bs.modal', function() {
		$(this).find('.scroll-pane').jScrollPane({ autoReinitialise: true });
	});
	$('#modal-photo').on('shown.bs.modal', function() {
		$(this).find('.scroll-pane').jScrollPane({ autoReinitialise: true });
	});
	$('#modal-profile-recherche').on('shown.bs.modal', function() {
		$(this).find('.scroll-pane').jScrollPane({ autoReinitialise: true });
	});
	$('#modal-agenda').on('shown.bs.modal', function() {
		$(this).find('.scroll-pane').jScrollPane({ autoReinitialise: true });
	});
	$('#modal-contact').on('shown.bs.modal', function() {
		$(this).find('.scroll-pane').jScrollPane({ autoReinitialise: true });
	});
	$('#modal-deposer-document-offre').on('shown.bs.modal', function() {
		$(this).find('.scroll-pane').jScrollPane({ autoReinitialise: true });
	});
	$('#modal-profile-entreprise-agendaFull').on('shown.bs.modal', function() {
		$(this).find('.scroll-pane').jScrollPane({ autoReinitialise: true });
	});
	$('#modal-photo-reception').on('shown.bs.modal', function() {
		$(this).find('.scroll-pane').jScrollPane({ autoReinitialise: true });
	});
	$('#modal-video-reception').on('shown.bs.modal', function() {
		$(this).find('.scroll-pane').jScrollPane({ autoReinitialise: true });
	});
	$('#modal-presse-reception').on('shown.bs.modal', function() {
		$(this).find('.scroll-pane').jScrollPane({ autoReinitialise: true });
	});
	$('#modal-product-apercu').on('shown.bs.modal', function() {
		$(this).find('.scroll-pane').jScrollPane({ autoReinitialise: true });
	});
	
	/**************************** Scroll *******************************/
	$('#modal-temoignage').on('hidden.bs.modal', function () {
	  $(".menu-stand ul li a#temoignage").parent().removeClass('active');
	});
	$('#modal-profile-entreprise').on('hidden.bs.modal', function () {
	  $(".menu-stand ul li a#profil-entreprise").parent().removeClass('active');
	});
	$('#modal-message').on('hidden.bs.modal', function () {
	  $(".menu-stand ul li a#send-message-link").parent().removeClass('active');
	});
	$('#modal-sondage').on('hidden.bs.modal', function () {
	  $(".menu-stand ul li a#sondage-link").parent().removeClass('active');
	});
	$('#modal-offre-emploi').on('hidden.bs.modal', function () {
	  $(".menu-stand ul li a#offre-emploi").parent().removeClass('active');
	});
	$('#modal-profile-recherche').on('hidden.bs.modal', function () {
	  $(".menu-stand ul li a#profile-recherche").parent().removeClass('active');
	});
	$('#modal-agenda').on('hidden.bs.modal', function () {
	  $(".menu-stand ul li a#agenda-link").parent().removeClass('active');
	});
	$('#modal-affiches').on('hidden.bs.modal', function () {
	  $(".menu-stand ul li a#affiches").parent().removeClass('active');
	});
	$('#modal-contact').on('hidden.bs.modal', function () {
	  $(".menu-stand ul li a#contact-stand").parent().removeClass('active');
	});
	$('#modal-document').on('hidden.bs.modal', function () {
	  $(".menu-stand ul li a#document").parent().removeClass('active');
	});
	$('#modal-deposer-document').on('hidden.bs.modal', function () {
	  $(".menu-stand ul li a#deposer-document-link").parent().removeClass('active');
	});
	$('#modal-photo').on('hidden.bs.modal', function () {
	  $(".menu-stand ul li a#stand-photo-link").parent().removeClass('active');
	});
	$('#modal-video').on('hidden.bs.modal', function () {
	  $(".menu-stand ul li a#stand-video-link").parent().removeClass('active');
	});
	$('#modal-presse').on('hidden.bs.modal', function () {
	  $(".menu-stand ul li a#stand-presse-link").parent().removeClass('active');
	});
	$('#modal-stand-tchat').on('hidden.bs.modal', function () {
	  $(".menu-stand ul li a#tchat-buttons").parent().removeClass('active');
	});
	$('#modal-photo-reception').on('hidden.bs.modal', function () {
	  $(".menu-stand ul li a#stand-photo-recep-link").parent().removeClass('active');
	});
	$('#modal-video-reception').on('hidden.bs.modal', function () {
	  $(".menu-stand ul li a#stand-video-recep-link").parent().removeClass('active');
	});
	$('#modal-presse-reception').on('hidden.bs.modal', function () {
	  $(".menu-stand ul li a#stand-presse-recep-link").parent().removeClass('active');
	});
	$(".menu-stand ul li a").click(function () {
	  $(".menu-stand ul li a").parent().removeClass('active');
	  $(this).parent().addClass('active');
	  return false;
	});
});
