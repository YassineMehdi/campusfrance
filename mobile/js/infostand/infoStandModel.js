jQuery.extend({
  InfoStandModel: function () {
    var that = this;
    var listeners = new Array();
    this.initModel = function () {
    };
    //------------- Website -----------//
    this.websiteEntrepriseSuccess = function (xml) {
      that.notifyWebsiteEntrepriseSuccess(xml);
    };

    //------------- Entreprise -----------//
    this.profilEntrepriseSuccess = function (xml) {
      that.notifyProfilEntrepriseSuccess(xml);
    };

    this.profilEntrepriseError = function (xhr, status, error) {
      that.notifyProfilEntrepriseError(xhr);
    };
    this.profilEntreprise = function (standId) {
      genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'stand/standInfos?' + standId + '&idLang=' + getIdLangParam(),
              'application/xml', 'xml', '', that.profilEntrepriseSuccess,
              that.profilEntrepriseError);

      genericAjaxCall('GET', staticVars["urlBackEndCandidate"]+'stand/standWebSites?'+standId+'&idLang='+getIdLangParam(), 'application/xml', 'xml','', that.websiteEntrepriseSuccess,'');
      genericAjaxCall('POST', staticVars["urlBackEndCandidate"]+'stand/view?'+standId+'&idLang='+getIdLangParam(), 'application/xml', 'xml','', '','');
    };
    
	this.notifyWebsiteEntrepriseSuccess = function (Xml) {
      $.each(listeners, function (i) {
        listeners[i].websiteEntrepriseSuccess(Xml);
      });
    };
    this.notifyProfilEntrepriseSuccess = function (Xml) {
      $.each(listeners, function (i) {
        listeners[i].profilEntrepriseSuccess(Xml);
      });
    };
    this.notifyProfilEntrepriseError = function (xhr) {
      $.each(listeners, function (i) {
        listeners[i].profilEntrepriseError(xhr);
      });
    };

    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  InfoStandModelListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }
});

