jQuery.extend({
  InfoStandController: function (model, view) {
    var that = this;
    var infoStandModelListener = new $.InfoStandModelListener({
      profilEntrepriseSuccess: function (xml) {
        view.displayProfilEntreprise(xml);
      },
      profilEntrepriseError: function (xhr) {
        view.displayProfilEntrepriseError(xhr);
      },
      websiteEntrepriseSuccess: function (xml) {
        view.displayWebsiteEntreprise(xml);
      },
    });
    model.addListener(infoStandModelListener);

    // listen to the view
    var infoStandViewListener = new $.InfoStandViewListener({
      profilEntreprise: function (standId) {
        model.profilEntreprise(standId);
      }
    });
    view.addListener(infoStandViewListener);

    this.initController = function (standId) {
      view.initView(standId);
      model.initModel(standId);
    };
    that.initController(standId);
  }

});