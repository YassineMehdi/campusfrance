jQuery.extend({
  PresseStandController: function (model, view) {
    var that = this;
    /**
     * listen to the view
     */

    var vlist = $.ViewListener({
      launchPresseStand: function (standId) {
        model.getPresseStand(standId);
      },
    });

    view.addListener(vlist);

    /**
     * listen to the model
     */
    var mlist = $.ModelListener({
      loadPresseStand: function (presses) {
        view.displayPresseStand(presses);
      },
    });

    model.addListener(mlist);

    this.initController = function (standId) {
      view.initView(standId);
      model.initModel();
    };
  },
});