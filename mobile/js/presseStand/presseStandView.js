jQuery.extend({
  PresseStandManage: function (presse, view) {
    var presseId = presse.getAdviceId();

    var isFavorite = factoryFavoriteExist(presse.getAdviceId(), FavoriteEnum.ADVICE);
    var displayDeleteFavorite = "block";
    var displayAddFavorite = "block";
    if (isFavorite)
      displayAddFavorite = "none";
    else
      displayDeleteFavorite = "none";

    var dom = $('<li>'
            + '<div class="favoris">'
            + '<ul>'
            + '<li><a class="icon-etoile addPresseToFavorite addPresseRhFavoriteId_' + presseId + '" style="display:' + displayAddFavorite + '"></a><a class="icon-delete deletePresseFromFavorite deletePresseRhFavoriteId_' + presseId + '" style="display:' + displayDeleteFavorite + '"></a></li>'
            + '<li><a class="icon-galerie openCurrentPresse"></a></li>'
            + '</ul>'
            + '</div>'
            + '<div class="visuel">'
            + '<a class="openCurrentPresse">'
            + '<img src="img/visuel2.jpg" alt="">'
            + '</a>'
            + '</div>'
            + '<span>' + htmlEncode(substringCustom(formattingLongWords(presse.getTitle(), 20), 60)) + '</span>'
            + '</li>');

    this.refresh = function () {
      dom.find(".openCurrentPresse").unbind();
      dom.find(".openCurrentPresse").bind('click', function () {
        $(".agrandir-press").bind('click', function () {
          $('#modal-document-lecteur-big').modal();
          openPresse(presse, $("#modal-document-lecteur-big").find('.ad-image-wrapper'), $("#modal-document-lecteur-big").find('.modal-title'));
        });
      	view.openPresse(presse);
      });

      var addPresseFavoriteLink = dom.find(".addPresseToFavorite");
      var deletePresseFavoriteLink = dom.find(".deletePresseFromFavorite");

      addPresseFavoriteLink.click(function () {
        if (connectedUser) {
          var idEntity = presse.getAdviceId();
          var componentName = FavoriteEnum.ADVICE;
          var favoriteXml = factoryXMLFavory(idEntity, componentName);
          addFavorite(favoriteXml);
          $(this).hide();
          $(this).closest('div').find('.deletePresseFromFavorite').show();
          $('.addPresseRHToFavoriteButton_' + presseId).hide();
          $('.deletePresseRHFromFavoriteButton_' + presseId).show();
        } else {
          getStatusNoConnected();
        }
      });

      deletePresseFavoriteLink.click(function () {
        var idEntity = presse.getAdviceId();
        var componentName = FavoriteEnum.ADVICE;
        var favoriteXml = factoryXMLFavory(idEntity, componentName);
        deleteFavorite(favoriteXml);
        $(this).hide();
        $(this).closest('div').find('.addPresseToFavorite').show();
        $('.addPresseRHToFavoriteButton_' + presseId).show();
        $('.deletePresseRHFromFavoriteButton_' + presseId).hide();
      });
	    $('#modal-presse .scroll-pane').jScrollPane({ autoReinitialise: true });
	    $('.modal-loading').hide();
    };
    this.getDOM = function () { //retourne une ligne HTML <a .....
      return dom;
    };
    this.refresh();
  },
  PresseStandView: function () {
    var listeners = new Array();
    var that = this;
    var galleriesPresses = null;
    var infoStandPresseHandler = null;
    var galleryPresseHandler = null;
    var galleryTitleHandler = null;
    var pressesContentHandler = null;
	
    $('#modal-presse, #modal-presse-reception').on('hidden.bs.modal', function () {
    	$(this).find('.info-stand-presse').empty();
    });

    this.initView = function (standId) {
      var standId = $('#temoignage').attr('standId');
      $('#stand-presse-link').unbind('click').bind('click', function () {
        that.addModalAdviceListener('#modal-presse', standId);
      	infoStandPresseHandler =  $('#modal-presse .info-stand-presse');
      });
      $('#stand-presse-recep-link').unbind('click').bind('click', function () {
        that.addModalAdviceListener('#modal-presse-reception', standId);
      	infoStandPresseHandler =  $('#modal-presse-reception .info-stand-presse');
      });
    };
    this.addModalAdviceListener = function(selector, standId){
    	if(!($(selector).data('bs.modal') || {}).isShown) {
        factoryActivateFaIcon(this);
        $('.modal-loading').show();
        $('.modal').modal('hide');
        $(selector).modal('show');
        that.notifyPresseStand(standId);
      }
    };
    this.displayPresseStand = function (presses) {
    	galleriesPresses = presses;
      if (presses != null && Object.keys(presses).length > 0) {
      	infoStandPresseHandler.html('<div class="visuelG col-md-8 col-sm-8 no-padding">'
	            +'<div id="galleryPresse">'
		            +'<div class="ad-image-wrapper">'
		            +'</div>'
	            +'</div>'
            +'</div>'
            +'<div class="listing scroll-pane col-md-3 col-sm-3">'
	            +'<ul class="ul-presse">'
	            +'</ul>'
            +'</div>');
    	  galleryTitleHandler = $('.press-title-head');
    	  galleryPresseHandler = infoStandPresseHandler.find('.ad-image-wrapper');
    	  pressesContentHandler = infoStandPresseHandler.find('.ul-presse');
      	var presseStandManage = null;
      	for (var index in presses) {
      		presseStandManage = new $.PresseStandManage(presses[index], that);
      		pressesContentHandler.append(presseStandManage.getDOM());
      	}
      	that.openPresseByIndex(0);
      	infoStandPresseHandler.find('.scroll-pane').jScrollPane({ autoReinitialise: true });
      } else {
      	infoStandPresseHandler.html('<p class="empty">'+NODOCUMENTSLABEL+'</p>');
     	}
      $('.modal-loading').hide();
    };
    this.openPresseByIndex = function (index) {
      if(galleriesPresses != null){
        $(".agrandir-press").bind('click', function () {
          $('#modal-document-lecteur-big').modal();
          openPresse(galleriesPresses[index], $("#modal-document-lecteur-big").find('.ad-image-wrapper'), $("#modal-document-lecteur-big").find('.modal-title'));
        });
	      that.openPresse(galleriesPresses[index]);
      }
    };
    this.openPresse = function (presse) {
  	  if(presse != null){
  	  	openPresse(presse, galleryPresseHandler, galleryTitleHandler);
  	  }
    };
    this.notifyPresseStand = function (standId) {
      $.each(listeners, function (i) {
        listeners[i].launchPresseStand(standId);
      });
    };

    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  ViewListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }
});


