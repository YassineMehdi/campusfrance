jQuery.extend({
  MultimediasView: function () {
    var that = this;
    var listeners = new Array();
    this.initView = function () {
      var standId = $('#temoignage').attr('standId');
      var standIdVal = standId.replace('standId=', '');
      that.initDefaultZone(standIdVal);
      that.playerReceptionToMultimediaSelector = '#content_video_transition_info_stand_reception_to_multimedia';
      that.playerReceptionToPressSelector = '#content_video_transition_info_stand_reception_to_press';
      that.playerMultimediaToPressSelector = '#content_video_transition_info_stand_multimedia_to_press';
      that.playerPressToMultimediaSelector = '#content_video_transition_info_stand_press_to_multimedia';
      that.playerMultimediaToReceptionSelector = '#content_video_transition_info_stand_multimedia_to_reception';
      that.playerPressToReceptionSelector = '#content_video_transition_info_stand_press_to_reception';
      $('#go-to-press').unbind('click').bind('click', function () {
      	that.navigateToPresse(standIdVal, that.playerMultimediaToPress);
      });
      $('#stand-audio').unbind('click').bind('click', function () {
      	that.navigateToPresse(standIdVal, that.playerReceptionToPress);
      });
      $('.multimedia-to-reception').unbind('click').bind('click', function () {
        that.navigateToReception(standIdVal, that.playerMultimediaToReception);
      });
      $('.press-to-reception').unbind('click').bind('click', function () {
        that.navigateToReception(standIdVal, that.playerPressToReception);
      });
      $('#stand-video').unbind('click').bind('click', function () {
        that.navigateToMultimedia(standIdVal, that.playerReceptionToMultimedia);
      });
      $('#back-to-stand').unbind('click').bind('click', function () {
        var idHall = $.cookie('currentHall');
        if(idHall != null){
          $('.genericContent').attr('style', 'display: none;');
          $('#contentG').attr('class', '');
          $('#contentG').addClass('salle-exposition');
          $('#page-content').removeClass('no-padding');
          $('#home-page,#info-stand').addClass('hide');        
          $('#'+idHall).removeClass('hide');
          $('#'+idHall).show();
        }else{
          $('.icon-format2').click();
        }
      });
      $('#go-to-video').unbind('click').bind('click', function () {
        that.navigateToMultimedia(standIdVal, that.playerPressToMultimedia);
      });
      that.getMultimedias();
    };
    this.initDefaultZone = function (standId){
      $('.video-wrapper').addClass('hide');
      $('.multimedia-to-reception').attr('standId', standId);
      $('#vue-aerienne-info-stand, .video-stand-' + standId+',icone-recep-to-multi').removeClass('hide');
    };
    this.navigateToReception = function(standId, callBack){
    	$('.genericContent').hide();
      $('#vue-aerienne-info-stand').removeClass('hide');
      $('#contentG').removeClass('info-stand-multimedia').removeClass('info-stand-doc').addClass('info-stand');
      if (currentGroupStandId == GroupStandEnum.STAND_INFO) {
        $('#video_tran_content').removeClass('hide');
        callBack();
      } else {
        $('#info-stand, #websitesNetwork').show();
      }
    };
    this.navigateToMultimedia = function(standId, callBack){
    	$('.genericContent').hide();
      $('.icone-press, #vue-aerienne-info-stand').addClass('hide');
      $('.icone-multi').removeClass('hide');
      $('#contentG').removeClass('info-stand').removeClass('info-stand-doc').addClass('info-stand-multimedia');

      $('#info-stand-multimedia').removeClass();
      $('#info-stand-multimedia').addClass('genericContent multimedia goupe_'+currentGroupStandId);
      $('#info-stand-multimedia').addClass('standId_'+standId);

      if (currentGroupStandId == GroupStandEnum.STAND_INFO) {
        $('#video_tran_content').removeClass('hide');
        callBack();
      } else {
        $('#info-stand-multimedia, #websitesNetwork').show();
      }
    };
    this.navigateToPresse = function(standId, callBack){
    	$('.genericContent').hide();
    	$('.icone-multi,#vue-aerienne-info-stand').addClass('hide');
      $('.icone-press').removeClass('hide');
      $('#contentG').removeClass('info-stand').removeClass('info-stand-multimedia').addClass('info-stand-doc');

      $('#info-stand-multimedia').removeClass();
      $('#info-stand-multimedia').addClass('genericContent press goupe_'+currentGroupStandId);
      $('#info-stand-multimedia').addClass('standId_'+standId);

      if (currentGroupStandId == GroupStandEnum.STAND_INFO) {
        $('#video_tran_content').removeClass('hide');
        callBack();
      } else {
        $('#info-stand-multimedia, #websitesNetwork').show();
      }
    };
    this.playerMultimediaToReception = function () {
      flowAutoPlay('media/video/2016/infostand_africtalents_2016_multimedia_inverse', 'img/stand-7-affiche.png', function () {
      	return;
      }, that.playerMultimediaToReceptionSelector);
    };
    this.playerPressToReception = function () {
      flowAutoPlay('media/video/2016/infostand_africtalents_2016_press_inverse', 'img/stand-7-doc.png', function () {
      	return;
      }, that.playerPressToReceptionSelector);
    };
    this.playerPressToMultimedia = function () {
      flowAutoPlay('media/video/2016/infostand_africtalents_2016_press_multumedia', 'img/stand-7-doc.png', function () {
      	return;
      }, that.playerPressToMultimediaSelector);
    };
    this.playerMultimediaToPress = function () {
      flowAutoPlay('media/video/2016/infostand_africtalents_2016_multimedia_press', 'img/stand-7-affiche.png', function () {
      	return;
      }, that.playerMultimediaToPressSelector);
    };
    this.playerReceptionToMultimedia = function () {
      flowAutoPlay('media/video/2016/infostand_africtalents_2016_multimedia', 'img/stand-7.jpg', function () {
      	return;
      }, that.playerReceptionToMultimediaSelector);
    };
    this.playerReceptionToPress = function () {
      flowAutoPlay('media/video/2016/infostand_africtalents_2016_press', 'img/stand-7.jpg', function () {
      	return;
      }, that.playerReceptionToPressSelector);
    };
    //------- Multimedias --------------//
    this.getMultimedias = function () {
      var standId = $('#stand-video-link').attr('standId');
      if (videoStandController == null) {
        videoStandController = new $.VideoStandController(
        		new $.VideoStandModel(), new $.VideoStandView());
      }
      videoStandController.initController(standId);
      if (photoStandController == null) {
        photoStandController = new $.PhotoStandController(
        		new $.PhotoStandModel(), new $.PhotoStandView());
      }
      photoStandController.initController(standId);
      if (presseStandController == null) {
        presseStandController = new $.PresseStandController(
        		new $.PresseStandModel(), new $.PresseStandView());
      }
      presseStandController.initController(standId);
      that.notifyMultimedias(standId);
    };
    //----------- Multimedias --------//
    this.displayMultimedias = function (multimedias) {
      new $.MultimediaManage(multimedias);
    };
    this.displayMultimediasError = function (Xhr) {
      console.log(Xhr);
    };
    //----------- Notify Multimedias --------//
    this.notifyMultimedias = function (standId) {
      $.each(listeners, function (i) {
        listeners[i].multimedias(standId);
      });
    };
    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  MultimediaViewListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }
});

