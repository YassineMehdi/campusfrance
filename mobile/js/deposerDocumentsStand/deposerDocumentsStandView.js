jQuery.extend({
  LoadCvManage: function (listCandidateCVs) {
    var dom;
    var that = this;
    if (listCandidateCVs != null) {
      dom = $('<div class="col-md-4 col-sm-4 no-padding">'
              + '<input type="checkbox" class="filled-in checkcv" id="cv-' + listCandidateCVs.getIdPdfCv() + '" url="' + listCandidateCVs.getUrlCv() + '">'
              + '<label for="cv-' + listCandidateCVs.getIdPdfCv() + '" id="label-cv-' + listCandidateCVs.getIdPdfCv() + '">' + htmlEncode(listCandidateCVs.getCvName()) + '</label>'
              + '</div>');
    } else {
      dom = $('<p style="text-align: center;">Aucun Cv dans la base</p>');
    }

    $("#label-cv-" + listCandidateCVs.getIdPdfCv()).click(function () {
      $('#cv-' + listCandidateCVs.getIdPdfCv()).trigger('click');
    });
    this.getDOM = function () { //retourne une ligne HTML <a .....
      return dom;
    };
  },
  DeposerDocumentsStandView: function () {
    var listeners = new Array();
    var that = this;

    this.displaysendedDocumentsStand = function () {
      $('.modal-loading').hide();
      swal({
        title: DOCUMENT,
        text: DEPOTDOCUMENTSUCCESSLABEL,
        type: "success"},
      function () {
        $('#modal-deposer-document').modal("hide");
      });
    };
    this.displaycandidateCVs = function (listCandidateCVs) {
      $('#modal-deposer-document #cvs-hitory').html('');
      for (index in listCandidateCVs) {
        var cvs = new $.LoadCvManage(listCandidateCVs[index]);
        $('#modal-deposer-document #cvs-hitory').append(cvs.getDOM());
      }
      $('#modal-deposer-document .scroll-pane').jScrollPane({ autoReinitialise: true });
      $('.modal-loading').hide();
    };
    this.initView = function () {
      $('#deposer-document-link').unbind().bind('click', function () {
        if (connectedUser) {
            if(!($("#modal-deposer-document").data('bs.modal') || {}).isShown) {
	          factoryActivateFaIcon(this);
	          $('.modal').modal('hide');
	          that.notifyLoadDocsCVs();
	          $('#modal-deposer-document').modal('show');
	          $('#inputCv').fileupload(that.getCvUpload());
	          $('#submit-document').unbind().bind('click', function () {
	            if ($('.checkcv:checked').length > 0) {
	              that.notifySendDocsCVs($('.checkcv:checked').attr('url'), 
	            		  $('#modal-deposer-document textarea').val(), $('#deposer-document-link').attr('standId'));
	              $('.modal-loading').show();
	            } else {
	              swal(SENDERRORLABEL, CHOOSEDOCUMENTLABEL, "error");
	            }
	            return false;
	          });
            }
        } else {
          getStatusNoConnected();
        }
      });
    };
//------------- File Uploade CV-------------//
    this.getCvUpload = function () {
      var timeUpload = null;
      return {
        beforeSend: function (jqXHR, settings) {
        },
        datatype: 'json',
        cache: false,
        add: function (e, data) {
          $('.modal-loading').show();
          var goUpload = true;
          var uploadFile = data.files[0];
          var fileName = uploadFile.name;
          timeUpload = (new Date()).getTime();
          data.url = getUrlPostUploadFile(timeUpload, fileName);
          if (!regexDoc.test(fileName)) {
            swal(DOWNLOADERRORLABEL, DOWNLOADEXTENSIONERRORLABEL, "error");
            $('.modal-loading').hide();
            goUpload = false;
          }

          if (goUpload == true) {
            if (uploadFile.size > MAXFILESIZE) {// 6mb
              swal(DOWNLOADERRORLABEL, DOWNLOADSIZEERRORLABEL, "error");
              $('.modal-loading').hide();
              goUpload = false;
            }
          }
          if (goUpload == true) {
            jqXHRResource = data.submit();
          }
        },
        progressall: function (e, data) {
        },
        done: function (e, data) {
          var file = data.files[0];
          var fileName = file.name;
          var hashFileName = getUploadFileName(timeUpload, fileName);
          //console.log("Add file done, filename : " + fileName + ", hashFileName : " + hashFileName);
          var urlCache = getUploadFileUrl(hashFileName);
          addUploadCv(fileName, urlCache, function () {
            getUpLoadedCvs();
            factoryGetCandidateCvs();
          });
          getKeywordsFromCV(urlCache);
          $('#modal-deposer-document #cvs-hitory').html('');
          that.notifyLoadDocsCVs();
          $('.modal-loading').hide();
          swal(DOWNLOADLABEL, DOWNLOADSUCCESSLABEL, "success");
        }
      };
    };
    this.notifySendDocsCVs = function (url, messageToSend, standId) {
      $.each(listeners, function (i) {
        listeners[i].sendDeposerDocumentsStand(url, messageToSend, standId);
      });
    };
    this.notifyLoadDocsCVs = function () {
      $('.modal-loading').show();
      $.each(listeners, function (i) {
        listeners[i].notifyLoadDocsCVs();
      });
    };

    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  ViewListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }
});

