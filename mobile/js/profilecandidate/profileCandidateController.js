jQuery.extend({
  ProfileCandidateController: function (model, view) {
    var that = this;
    var profileCandidateModelListener = new $.ProfileCandidateModelListener({
      profileCandidate: function (profileCandidate) {
        view.displayprofileCandidate(profileCandidate);
      }
    });
    model.addListener(profileCandidateModelListener);

    // listen to the view
    var profileCandidateViewListener = new $.ProfileCandidateViewListener();
    view.addListener(profileCandidateViewListener);

    this.initController = function () {
      view.initView();
      model.initModel();
    };
    that.initController();
  }

});