jQuery.extend({
  ChatModel: function () {
    var listeners = new Array();
    var that = this;

    this.initModel = function () {
    };
    
    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  ModelListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }

});