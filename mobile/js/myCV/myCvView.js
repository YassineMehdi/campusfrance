jQuery.extend({

  VisitorCardManage: function (favorite, view) {
    var componentId = favorite.getComponent().getComponentId();
    var visitor = null;
    var visitorEmail = "";
    var visitorPhone = "";
    var visitorAdress = "";
    var visitorWebsite = "";
    var visitorJobTitle = "";
    var visitorEnterprise = "";
    var enterprise = "";
    var address = "";
    var webSite = "";
    var actions = "";
    var classUserType = "";
    
    if (componentId == FavoriteIdsEnum.VISIT_CARD_CANDIDATE_ID) {
      visitor = favorite.getCandidate();
      if(visitor.getCountry().getCountryName() != ""){
        address = visitor.getPlace() + ", " + visitor.getCountry().getCountryName() ;
      }
      webSite = visitor.getUserProfile().getWebSite();
      classUserType = "participant-visit-card";
      enterprise = visitor.getCurrentCompany();
    } else if (componentId == FavoriteIdsEnum.VISIT_CARD_ENTERPRISE_P_ID) {
      visitor = favorite.getEnterpriseP();
      if(visitor.getEnterprise().getAddress() != ""){
        address = visitor.getEnterprise().getAddress();
      }
      classUserType = "exibitor-visit-card";
      enterprise = visitor.getEnterprise().getSiegName();
      webSite = visitor.getEnterprise().getWebsite();
    } 
    if(VisitCardStatusEnum.INVITER == favorite.getStatus()){
      actions = $('<a href="#" class="btn pending-visit-card">' + DEMAND_IN_PROGRESS_LABEL + '</a>');
    }else if(VisitCardStatusEnum.GUEST == favorite.getStatus()){
      actions = $('<a href="#" class="btn refuse-visit-card">' + REFUSE_LABEL
          + '</a><a href="#" class="btn accept-visit-card">' + ACCEPT_LABEL + '</a>');
    }
    //var visitorId = visitor.getUserProfile().getUserProfileId();

    visitorJobTitle = visitor.getJobTitle();
    visitorEmail = $('<div class ="intro-offre-detail"><i class = "fa fa-envelope "></i>'+htmlEncode(visitor.getEmail())+'</div>');
    visitorPhone = $('<div class ="intro-offre-detail"><i class = "fa fa-phone"></i>'+htmlEncode(visitor.getCellPhone())+'</div>');
    visitorWebsite = $('<div class ="intro-offre-detail"><i class = "fa fa-chrome"></i>'+htmlEncode(webSite)+'</div>');
    visitorEnterprise = $('<div class ="intro-offre-detail"><i class = "fa fa-building-o"></i>'+htmlEncode(enterprise)+'</div>');
    visitorAdress = $('<div class ="intro-offre-detail"><i class = "fa fa-home"></i>'+htmlEncode(address)+'</div>');
    if(visitor.getEmail() == "") visitorEmail = $(visitorEmail).hide();
    if(visitor.getCellPhone() == "") visitorPhone = $(visitorPhone).hide();
    if(visitor.getUserProfile().getWebSite() == "") visitorWebsite = $(visitorWebsite).hide();
    if(enterprise == "") visitorEnterprise = $(visitorEnterprise).hide();
    if(address == "") visitorAdress = $(visitorAdress).hide();

    var dom = $('<div class="cardVisitContainer col-md-6 col-sm-12">'
                  +'<div class="viewVisitorScrollStyle viewVisitorGeneralInfo '+classUserType+'">'
                    + '<div class="visitorHeader row-fluid">'
                      + '<div class="col-md-9 col-sm-8 no-padding">'
                        + '<div class="visitorFullName">' + htmlEncode(visitor.getUserProfile().getFullName()) + '</div>'
                        + '<div class = "intro-offre">'+htmlEncode(visitorJobTitle)+'</div>'
                      + '</div>'
                      + '<div class="logo col-md-3 col-sm-4 no-padding">'
                        + '<img src="' + htmlEncode(visitor.getUserProfile().getAvatar()) + '" alt="">'
                      + '</div>'
                    + '</div>'
                    + '<div class="visitorDetails row-fluid">'
                      + getHtml(visitorPhone)
                      + getHtml(visitorEmail)
                      + getHtml(visitorEnterprise)
                      + getHtml(visitorWebsite)
                      + getHtml(visitorAdress)
                      + getHtml(actions)
                    + '</div>'
                + '</div>'
              + '</div>'
            );
    this.refresh = function () {
      $(dom).find(".accept-visit-card").unbind("click").bind("click", function(){
        var componentName = "";
        if (componentId == FavoriteIdsEnum.VISIT_CARD_CANDIDATE_ID) {
          componentName = FavoriteEnum.VISIT_CARD_CANDIDATE;
        }else if (componentId == FavoriteIdsEnum.VISIT_CARD_ENTERPRISE_P_ID) {
          componentName = FavoriteEnum.VISIT_CARD_ENTERPRISE_P;
        }
        var favoriteXml = factoryXMLFavory(visitor.getUserProfile().getUserProfileId(), componentName);
        addVisitCard(favoriteXml, function(){
          $("#myVisitCardFavLink").click();
        });
      });
      $(dom).find(".refuse-visit-card").unbind("click").bind("click", function(){
        componentName = FavoriteEnum.VISIT_CARD_CANDIDATE;
        var favoriteXml = factoryXMLFavory(visitor.getUserProfile().getUserProfileId(), FavoriteEnum.VISIT_CARD_CANDIDATE);
        deleteVisitCard(favoriteXml, function(){
          $("#myVisitCardFavLink").click();
        });
      });
    };
    this.getDOM = function () { //retourne une ligne HTML <a .....
      return dom;
    };
    this.refresh();
  },
  MyCvView: function () {
    var listeners = new Array();
    var that = this;
    this.displayCandidatePersonalInfos = function (candidate) {
      $('.categList').hide(); 
      $('#content').show();
      $('#myCV .avatarCvLogo').attr('src', candidate.getUserProfile().getAvatar());
      $('#myCV .candidateFullName').text(candidate.getUserProfile().getFullName());
      $('#myCV #enterpriseInput').val(candidate.getCurrentCompany());
      $('#myCV #functionInput').val(candidate.getJobTitle());
      $('#myCV .cvDescriptionInput').val(candidate.getDescription());
      $('.modal-loading').hide();
    };

    this.displayVisitCards = function (listFavorites) {
      $visitCardContainer.empty();
      if (listFavorites != null && Object.keys(listFavorites).length > 0) {
        for (var index in listFavorites) {
          var visitorsCard = new $.VisitorCardManage(listFavorites[index]);
          $visitCardContainer.append(visitorsCard.getDOM());
        }
      }else $visitCardContainer.html('<p class="empty noVisitCardLabel"></p>');
      traduct();
      $('.modal-loading').hide();
    };

    this.sendCvContentSuccess = function(){
      swal({
              title: "",
              text: UPDATESUCCESSLABEL,
              type: "success"},
          function () {
          });
      $('.modal-loading').hide();
    };
    this.beforeInitView = function () {
      $('#cvUploadBtn').unbind('click').bind('click', function () {
        $('#cvUploadNew').trigger('click');
      });
      $('.editCVCandidateLogo').on('click', function () {
        $('#cvAvatarLogoUpload').trigger('click');
      });
      //---------- refresh jscrollpane -----//
      $("#myCV .info-prof").unbind('click').bind('click', function () {
        $('#myCV .scroll-pane').jScrollPane({ autoReinitialise: true });
      });
      //---------- User EDS CV Main Menu -----//
      $('#edsCVLink').unbind('click').bind('click', function () {
        $('.modal-loading').show();
        $('.myCVContainers').hide();
        $('#cvContent').show();
        $("#myCV .glyphicon").removeClass('active');
        $(this).closest('li').addClass('active');
        $('#cvContent .scroll-pane').jScrollPane({ autoReinitialise: true });
        $('#cvTitleEdit').hide();
        $('.candidateCVTitle').show();
        that.notifyLoadCandidatePersonalInfo();
      });
      //---------- User EDS CV Main Menu -----//
      $('#visitCardListLink').unbind('click').bind('click', function () {
          window.history.pushState("test", "Title", "/mobile/new-home.html?idLang="+getIdLangParam());  
          $('.modal-loading').show();
          $('.myCVContainers').hide();
          $('#visitCardsList').show();
          $("#myCV .glyphicon").removeClass('active');
          $(this).closest('li').addClass('active');
          genericSearchController.getView().notifyLaunchSearch('visitCard', '');
          $('#visitCardsList .scroll-pane').jScrollPane({ autoReinitialise: true });

      });
      //---------- User Mon Dossier Main Menu -----//
      $('#myVisitCardFavLink').unbind('click').bind('click', function () {
        $('.modal-loading').show();
        $('.myCVContainers').hide();
        $('#cvContentDoc').show();
        $("#myCV .glyphicon").removeClass('active');
        $(this).closest('li').addClass('active');
        $('#cvContentDoc .scroll-pane').jScrollPane({ autoReinitialise: true });
        that.notifyLoadVisitCards();
      });
      $('#cvAvatarLogoUpload').fileupload({
        beforeSend: function (jqXHR, settings) {
          beforeUploadFile('#cvAvatarLogoUpload', '#progressAddCvLogo', '#candidatePictureDiv .filebutton,#loadPhotoError');
        },
        datatype: 'json',
        cache: false,
        add: function (e, data) {
          var goUpload = true;
          var uploadFile = data.files[0];
          var fileName = uploadFile.name;
          timePictureUpload = (new Date()).getTime();
          data.url = getUrlPostUploadFile(timePictureUpload, fileName);
          if (!(/\.(gif|jpg|jpeg|tiff|png)$/i).test(fileName)) {
            goUpload = false;
            swal({
              title: "",
              text: PICACCEPTEDEXTENSIONLABEL,
              type: "error"},
            function () {
            });
          }else{
            if (uploadFile.size > MAXFILESIZE) {// 6mb
              goUpload = false;
              swal({
              title: "",
                text: PICSIZELABEL,
                type: "error"},
            function () {
            });
            }
          }
          if (goUpload == true) {
            jqXHRCandidatePicture = data.submit();
          } else
            $("#loadPhotoError").show();
        },
        progressall: function (e, data) {
          progressBarUploadFile('#progressAddCvLogo', data, 100);
        },
        done: function (e, data) {
          var file = data.files[0];
          var fileName = file.name;
          var hashFileName = getUploadFileName(timePictureUpload, fileName);
          console.log("Add candidate photo done, filename :" + fileName + ", hashFileName : " + hashFileName);
          candidateCVLogo = getUploadFileUrl(hashFileName);
          $('.avatarCvLogo').attr('src', candidateCVLogo);
          $('.connexion .avatar').find('img').attr('src', candidateCVLogo);
          $('.main-menu .avatar').find('img').attr('src', candidateCVLogo);
          //$(".saveContentCvButton").trigger('click');
          afterUploadFile('#cvAvatarLogoUpload', '#candidatePictureDiv .filebutton', '#progressAddCvLogo,#loadPhotoError');
        }
      });
      $('#cvUploadNew').fileupload({
        beforeSend: function (jqXHR, settings) {
          beforeUploadFile('', '#progressAddCV', '#addCvDiv .filebutton,#loadCVError');
        },
        datatype: 'json',
        cache: false,
        add: function (e, data) {
          var goUpload = true;
          var uploadFile = data.files[0];
          var fileName = uploadFile.name;
          timeAddCV = (new Date()).getTime();
          data.url = getUrlPostUploadFile(timeAddCV, fileName);
          if (!(/\.(pdfx?|docx?|)$/i).test(fileName)) {
            goUpload = false;
            swal({
              title: "",
              text: DATASIZELABEL,
              type: "error"},
          function () {
          });
          }else{
            if (uploadFile.size > MAXFILESIZE) {// 6mb
              goUpload = false;
              swal({
                title: "",
                text: DOCSIZELABEL,
                type: "error"},
            function () {
            });
            }
          }
          if (goUpload == true) {
            data.submit();
          } 
        },
        progressall: function (e, data) {
          progressBarUploadFile('#progressAddCV', data, 100);
        },
        done: function (e, data) {
          var file = data.files[0];
          var fileName = file.name;
          var hashFileName = getUploadFileName(timeAddCV, fileName);
          console.log("Add candidate cv done, filename :" + fileName + ", hashFileName : " + hashFileName);
          var cvUrl = getUploadFileUrl(hashFileName);
          addUploadCv(fileName, cvUrl, function () {
            afterUploadFile('', '#addCvDiv .filebutton', '#progressAddCV,#loadCVError');
            that.notifyLoadDocsCVs();
            getKeywordsFromCV(cvUrl);
          });
        }
      });

      $(".saveContentCvButton").unbind('click').bind('click', function () {
        $('.modal-loading').show();
        var candidate = new $.Candidate();
        var userProfile = new $.UserProfile();
        userProfile.setAvatar($("#myCV .avatarCvLogo").attr('src'));
        candidate.setJobTitle($("#myCV #functionInput").val());
        candidate.setCurrentCompany($("#myCV #enterpriseInput").val());
        candidate.setDescription($("#myCV .cvDescriptionInput").val());
        candidate.setUserProfile(userProfile);
        that.notifyUpdateCandidate(candidate);
      });
    };
    this.removeCv = function(cvId){
      swal({   
        title: "Confirmation",   
        text: "Vous voulez vraiement surppimer votre cv",   
        type: "warning",   
        showCancelButton: true,     
        cancelButtonText: "Annuler",   
        confirmButtonText: "Supprimer",   
        closeOnConfirm: false 
        }, function(){   
          that.notifyDeleteCv(cvId);
        });
    };
    this.initView = function () {
      $('.modal-loading').show();
      $('.genericContent').hide();
      $("#myCV").show();
      $('#page-content').removeClass('no-padding');
      $('#edsCVLink').trigger('click');
      $('#progressAddCV, #progressAddCvLogo').hide();
      $visitCardContainer = $('.visitCardList');
      $('#myCV .scroll-pane').jScrollPane({ autoReinitialise: true });
    };
    that.beforeInitView();

    this.notifyLoadVisitCards = function () {
      $.each(listeners, function (i) {
        listeners[i].loadVisitCards();
      });
    };

    this.notifyLoadCandidatePersonalInfo = function () {
      $.each(listeners, function (i) {
        listeners[i].loadCandidatePersonalInfo();
      });
    };
    this.notifyLoadDocsCVs = function () {
      $.each(listeners, function (i) {
        listeners[i].loadDocsCVs();
      });
    };
    this.notifySendCvContent = function (cvContentXml) {
      $.each(listeners, function (i) {
        listeners[i].sendCvContent(cvContentXml);
      });
    };
    this.notifyLaunchSearch = function (optionVal, searchInput) {
      $.each(listeners, function (i) {
        listeners[i].launchSearch(optionVal, searchInput);
      });
    };
    this.notifyUpdateCandidate = function (candidate) {
      $.each(listeners, function (i) {
        listeners[i].updateCandidate(candidate);
      });
    };
    this.notifyDeleteCv = function(cvId){
      $.each(listeners, function (i) {
        listeners[i].deleteCv(cvId);
      });
    };
    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  ViewListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }
});



