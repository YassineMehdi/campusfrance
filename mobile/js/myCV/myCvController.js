jQuery.extend({
  MyCvController: function (model, view) {
    var that = this;
    /**
     * listen to the view
     */

    var vlist = $.ViewListener({
    	candidateCVsUploaded: function (listCandidateCVs) {
				view.displayCandidateCVs(listCandidateCVs);
			},
			loadCandidatePersonalInfos: function (candidate) {
				view.displayCandidatePersonalInfos(candidate);
			},
			loadCandidateContentCV: function (cvContent) {
				view.displayCandidateContentCV(cvContent);
			},
			sendCvContentSuccess: function(){
				view.sendCvContentSuccess();
			},
			deleteCvSuccess: function(){
				view.deleteCvSuccess();
			},
			loadListVisitCards: function (listFavorites) {
				view.displayVisitCards (listFavorites);
			}
    });

    view.addListener(vlist);

    /**
     * listen to the model
     */
    var mlist = $.ModelListener({
    	loadCandidatePersonalInfo: function () {
				model.getCandidatePersonalInfos();
				//model.getContentCv();
			},
			loadDocsCVs: function () {
				model.loadDocsCVs();
			},
			sendCvContent: function (cvContentXml) {
				model.sendCvContent(cvContentXml);
			},
			deleteCv: function(cvId){
				model.deleteCv(cvId);
			},
			updateCandidate: function(candidate){
				model.updateCandidate(candidate);
			},
			loadVisitCards: function () {
				model.getVisitCards();
			},
    });

    model.addListener(mlist);

    this.initController = function () {
  		view.initView();
  		model.initModel();
    };
  },
});


