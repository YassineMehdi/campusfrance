jQuery.extend({
  AffichesModel: function () {
    var that = this;
    var listeners = new Array();
    this.initModel = function () {
    };
//--------  Testimonies --------------//
    this.affichesSuccess = function (xml) {
      var affiches = [];
      $(xml).find("allfiles").each(function () {
        var affiche = new $.AllFiles($(this));
        affiches.push(affiche);
      });
      that.notifyAffichesSuccess(affiches);
    };

    this.affichesError = function (xhr, status, error) {
      that.notifyAffichesError(xhr);
    };
    this.affiches = function (standId) {
      genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'allfiles/standPosters?' + standId + '&idLang=' + getIdLangParam(),
              'application/xml', 'xml', '', that.affichesSuccess,
              that.affichesError);
    };
    this.notifyAffichesSuccess = function (affiches) {
      $.each(listeners, function (i) {
        listeners[i].affichesSuccess(affiches);
      });
    };
    this.notifyAffichesError = function (xhr) {
      $.each(listeners, function (i) {
        listeners[i].affichesError(xhr);
      });
    };
    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  AffichesModelListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }
});