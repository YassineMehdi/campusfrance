jQuery.extend({
  AfficheManage: function (affiche, view) {
    var isFavorite = factoryFavoriteExist(affiche.getAllFilesId(), FavoriteEnum.MEDIA);
    var displayDeleteFavorite = "block";
    var displayAddFavorite = "block";
    if (isFavorite)
      displayAddFavorite = "none";
    else
      displayDeleteFavorite = "none";
    var appendtext = '';
    appendtext += '<li>';
    appendtext += '<div class="favoris">';
    appendtext += '<ul>';
    appendtext += '<li><a href="#" class="icon-etoile addafficheToFavorite" style="display:' + displayAddFavorite + '"></a><a href="#" class="icon-delete deleteafficheFromFavorite" style="display:' + displayDeleteFavorite + '"></a></li>';
    appendtext += '<li><a href="#" class="icon-galerie show-image"></a></li>';
    appendtext += '</ul>';
    appendtext += '</div>';
    appendtext += '<div class="visuel">';
    appendtext += '<a href="#" class="show-image">';
    appendtext += '<img src="' + affiche.image + '" alt="" width="80" height="65">';
    appendtext += '<span>' + htmlEncode(affiche.title) + '</span>';
    appendtext += '</a>';
    appendtext += '</div>';
    appendtext += '</li>';
    var dom = $(appendtext);

    this.refresh = function () {
      var addafficheToFavorite = dom.find(".addafficheToFavorite");
      var deleteafficheFromFavorite = dom.find(".deleteafficheFromFavorite");

      dom.find(".show-image").click(function () {
        
    	  view.openPoster(affiche);
      });
      addafficheToFavorite.click(function () {
        if (connectedUser) {
          var idEntity = affiche.getAllFilesId();
          var componentName = FavoriteEnum.MEDIA;
          var favoriteXml = factoryXMLFavory(idEntity, componentName);
          addFavorite(favoriteXml);
          $(this).hide();
          $(this).closest('div').find('.deleteafficheFromFavorite').show();
        } else {
          getStatusNoConnected();
        }
      });

      deleteafficheFromFavorite.click(function () {
        var idEntity = affiche.getAllFilesId();
        var componentName = FavoriteEnum.MEDIA;
        var favoriteXml = factoryXMLFavory(idEntity, componentName);
        deleteFavorite(favoriteXml);
        $(this).hide();
        $(this).closest('div').find('.addafficheToFavorite').show();
      });
    };
    this.getDOM = function () { //retourne une ligne HTML <a .....
      return dom;
    };
    this.refresh();
  },
  AffichesView: function () {
    var that = this;
    var listeners = new Array();
    
    $('#modal-affiches').on('hidden.bs.modal', function () {
    	$(this).find('.info-stand-affiche').empty();
    });
    this.initView = function () {
      $('#affiches').unbind().bind('click', function () {
        if(!($("#modal-affiches").data('bs.modal') || {}).isShown) {
	      	factoryActivateFaIcon(this);
	        $('.modal-content .info-stand-affiche .listing').html('<ul class="content"></ul>');
	        $('.modal').modal('hide');
	        $('.modal-loading').show();
	        that.getAffiches();
	        $('#modal-affiches').modal('show');
        }
      });
    };
    //------- Affiches --------------//
    this.getAffiches = function () {
      var standId = $('#affiches').attr('standId');
      that.notifyAffiches(standId);
    };
    //----------- Affiches --------//
    this.displayAffiches = function (affiches) {
      if (affiches != null && affiches.length > 0) {
    	  $('#modal-affiches .info-stand-affiche').html('<div class="visuelG col-md-8 col-sm-8 no-padding">'
                                                    +'<h3></h3>'
                                                    +'<img src="img/visuelG.jpg" alt="" class="showin">'
                                                +'</div>'
                                                +'<div class="listing scroll-pane col-md-3 col-sm-3">'
                                                    +'<ul class="content">'
                                                    +'</ul>'
                                                +'</div>');
        for (var index in affiches) {
          var afficheManage = new $.AfficheManage(affiches[index], that);
          $('.modal-content .info-stand-affiche ul.content').append(afficheManage.getDOM());
        }
        that.openPoster(affiches[0]);
        $('#modal-affiches .scroll-pane').jScrollPane({ autoReinitialise: true });
      } else {
        $('#modal-affiches .info-stand-affiche').html('<p class="empty noPosterLabel">'+NOPOSTER+'</p>');
      }
      $('.modal-loading').hide();
    };
    this.displayAffichesError = function (Xhr) {
    	//console.log(Xhr);
    };
    this.openPoster = function (poster) {
      if (poster != null) {

        $('#galleryPoster .ad-image-wrapper').empty();

        $(".agrandir-affiche").bind('click', function () {
          $('#modal-document-lecteur-big').modal();
          $("#modal-document-lecteur-big").find('.modal-title').text(htmlEncode(poster.title));
          //openPoster(poster, $("#modal-document-lecteur-big").find('.ad-image-wrapper'), );
          var poster_container = "";
          var poster_url = poster.url;
          if (poster_url.toLowerCase().indexOf(".pdf") !== -1) {
            if (!isIE) {
              poster_container = $(
                      '<iframe height="100%" width="100%" scrolling="no" allowfullscreen webkitallowfullscreen src="vplayer/mozilla-pdf.js/web/viewer.html?file='
                      + poster_url
                      + '" type="text/html" class="ad-wrapper" style="position: relative; height: 100%; width: 100%;"></iframe>'
                      + '<div id="" class="captionCon" style="overflow-y: auto; overflow-x: hidden;">'
                      + '</div>')
                      .css('position', 'relative');
            } else {
              var filename = getFileName(poster_url);
              poster_container = $(
                      '<object id="mySWF" style="width:100%;height: 260px;position:relative;" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000">'
                      + '<param name="movie" value="' + staticVars.urlServerStorageSWF + filename + '.swf"  style="position: relative; height: 100%; width: 100%;"/></object>'
                      + '<div id="" class="captionCon" style="overflow-y: auto; overflow-x: hidden;">'
                      + '</div>');
            }
          } else {
            poster_container = $('<img src="' + poster_url + '" class="ad-wrapper2"/>');
          }
          $("#modal-document-lecteur-big").find('.ad-image-wrapper').html(poster_container);
          analyticsView.viewStandZoneById(POSTER+'/'+POSTERID, poster.getAllFilesId(), null);
        });

        $('#modal-affiches .affiche-title-head').text(htmlEncode(poster.title));
	      var poster_container = "";
	      var poster_url = poster.url;
	      if (poster_url.toLowerCase().indexOf(".pdf") !== -1) {
	        if (!isIE) {
	          poster_container = $(
	                  '<iframe height="100%" width="100%" scrolling="no" allowfullscreen webkitallowfullscreen src="vplayer/mozilla-pdf.js/web/viewer.html?file='
	                  + poster_url
	                  + '" type="text/html" class="ad-wrapper" style="position: relative; height: 100%; width: 100%;"></iframe>'
	                  + '<div id="" class="captionCon" style="overflow-y: auto; overflow-x: hidden;">'
	                  + '</div>')
	                  .css('position', 'relative');
	        } else {
	          var filename = getFileName(poster_url);
	          poster_container = $(
	                  '<object id="mySWF" style="width:100%;height: 260px;position:relative;" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000">'
	                  + '<param name="movie" value="' + staticVars.urlServerStorageSWF + filename + '.swf"  style="position: relative; height: 100%; width: 100%;"/></object>'
	                  + '<div id="" class="captionCon" style="overflow-y: auto; overflow-x: hidden;">'
	                  + '</div>');
	        }
	      } else {
	        poster_container = $('<img src="' + poster_url + '" class="ad-wrapper2"/>');
	      }
	      $('.info-stand-affiche .visuelG').html(poster_container);
	      analyticsView.viewStandZoneById(POSTER+'/'+POSTERID, poster.getAllFilesId(), null);
	    }
	  },
    //----------- Notify Affiches --------//
    this.notifyAffiches = function (standId) {
      $.each(listeners, function (i) {
        listeners[i].affiches(standId);
      });
    };
    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  AffichesViewListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }
});

