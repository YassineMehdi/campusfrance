jQuery.extend({
	
  CareerReponseCandidatureManage: function (message, documentsContainer, view) {
  	var dom = $('');
  	var jobTitle = 'Candidature Spontanée';
  	if(message.jobOfferTitle != ""){
  		jobTitle = message.jobOfferTitle;
  	}
  	
    this.refresh = function () {
    	dom = $('<li>'
		          +'<div class="date-event">'
		              +'<div class="logo-event"><img src="'+message.enterpriseLogo+'" alt=""></div>'
		              +'<span>'+htmlEncode(message.enterpriseName)+'<br>'+htmlEncode(jobTitle)+'</span>'
		              +'<span class="date">'+htmlEncode(message.candidatureDate)+'</span>'
		          +'</div>'
		          +'<div class="intro">'
		              +'<div class="intro-paragraphe">'
		              + htmlEncode(message.response)
		              +'</div>'
		          +'</div>'
		      +'</li>');
    },
    this.getDOM = function () { //retourne une ligne HTML <a .....
      return dom;
    },
    this.refresh();
  },
  
  CareerCandidatureManage: function (message, documentsContainer, view) {
    var dom = $('');
  	var jobTitle = 'Candidature Spontanée';
  	if(message.jobOfferTitle != ""){
  		jobTitle = message.jobOfferTitle;
  	}


    this.refresh = function () {
    	dom = $('<li>'
          +'<div class="logo-event"><img src="'+message.enterpriseLogo+'" alt=""></div>'
          +'<span>'+htmlEncode(message.enterpriseName)+'<br>'+htmlEncode(jobTitle)+'</span>'
          +'<span class="date-event">'+htmlEncode(message.candidatureDate)+'</span>'
      +'</li>');
    },
    this.getDOM = function () { //retourne une ligne HTML <a .....
      return dom;
    },
    this.refresh();
  },	
  MyCareerView: function () {
    var listeners = new Array();
    var that = this;
    var jobApplicationsDisplaySuccess = false;
    var unsolicitedJobApplicationsDisplaySuccess = false;

  	this.checkToDisplayEmpty = function(){
      if($candidatureListeContainer.html() == ""){
      	$candidatureListeContainer.html('<p class="empty">'+NODEMANDELABEL+'</p>');
    	}
      if($responseCandidatureListeContainer.html() == ""){
    		$responseCandidatureListeContainer.html('<p class="empty">'+NORESPONSELABEL+'</p>');
    	}
  	};
  	
    this.displayJobApplications = function(listJobApplications){
    	var candidatureSpontanee = null;
    	var responseCandidature = null;
  		for (index in listJobApplications) {
      	candidatureSpontanee = new $.CareerCandidatureManage(listJobApplications[index], $candidatureListeContainer, that);
        $candidatureListeContainer.append(candidatureSpontanee.getDOM());
        if(listJobApplications[index].response != ''){
        	responseCandidature = new $.CareerReponseCandidatureManage(listJobApplications[index], $responseCandidatureListeContainer, that);
	        $responseCandidatureListeContainer.append(responseCandidature.getDOM());
        }	        
  		}
  		if(unsolicitedJobApplicationsDisplaySuccess) that.checkToDisplayEmpty();
  		jobApplicationsDisplaySuccess = true;
    	$('.carriere-candidature .scroll-pane').jScrollPane({ autoReinitialise: true });
    	$('.modal-loading').hide();
    };
    
		this.displayUnsolicitedJobApplications = function(listUnsolicitedJobApplications){
			var candidatureSpontanee = null;
			var responseCandidature = null;
			for (index in listUnsolicitedJobApplications) {
	        	candidatureSpontanee = new $.CareerCandidatureManage(listUnsolicitedJobApplications[index], $candidatureListeContainer, that);
		        $candidatureListeContainer.append(candidatureSpontanee.getDOM());
		        if(listUnsolicitedJobApplications[index].response != ''){
		        	responseCandidature = new $.CareerReponseCandidatureManage(listUnsolicitedJobApplications[index], $responseCandidatureListeContainer, that);
			        $responseCandidatureListeContainer.append(responseCandidature.getDOM());
				}
	    }
			if(jobApplicationsDisplaySuccess) that.checkToDisplayEmpty();
			unsolicitedJobApplicationsDisplaySuccess = true;
		  $('.carriere-reponse-content .scroll-pane').jScrollPane({ autoReinitialise: true });
	  };

    this.initView = function () {
    	$('.modal-loading').show();
      $('.genericContent').hide();
      $('#candidateCarrer').show();
  		$('#page-content').removeClass('no-padding');
  		jobApplicationsDisplaySuccess = false;
  		unsolicitedJobApplicationsDisplaySuccess = false;
  		$candidatureListeContainer = $('.candidatureListe');
  		$responseCandidatureListeContainer = $('.responseCandidatureListe');
      $candidatureListeContainer.empty();
      $responseCandidatureListeContainer.empty();
      $('#candidaturesLink').trigger('click');
    };
    
    //---------- User Ma carriere Candidature Main Menu -----//
    $('#candidaturesLink').click(function () {
      $('.modal-loading').show();
      $('.candidateCarreerContainer').hide();
      $('#candidatureCareerContainer').show();
      $("#candidateCarrer .glyphicon").removeClass('active');
      $(this).closest('li').addClass('active');
      $('.carriere-candidature .scroll-pane').jScrollPane({ autoReinitialise: true });
      $('.modal-loading').hide();
    });

    //---------- User Ma carriere Reponse  Main Menu -----//
    $('#answersCandidaturesLink').click(function () {
      $('.modal-loading').show();
      $('.candidateCarreerContainer').hide();
      $('#candidateCareerResponseContainer').show();
      $("#candidateCarrer .glyphicon").removeClass('active');
      $(this).closest('li').addClass('active');
      $('.carriere-reponse-content .scroll-pane').jScrollPane({ autoReinitialise: true });
      $('.modal-loading').hide();
    });

    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  ViewListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }
});
