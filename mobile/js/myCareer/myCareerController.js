jQuery.extend({
  MyCareerController: function (model, view) {
    var that = this;
    /**
     * listen to the view
     */

    var vlist = $.ViewListener({
    });

    view.addListener(vlist);

    /**
     * listen to the model
     */
    var mlist = $.ModelListener({
      jobApplicationsLoaded : function(listJobApplications) {
	       view.displayJobApplications(listJobApplications);
			},
			unsolicitedJobApplicationsLoaded : function(listUnsolicitedJobApplications) {
		       view.displayUnsolicitedJobApplications(listUnsolicitedJobApplications);
			},
    });

    model.addListener(mlist);

    this.initController = function () {
      view.initView();
      model.initModel();
    };
    
  },
});