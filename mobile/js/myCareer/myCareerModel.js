jQuery.extend({
  MyCareerModel: function () {
    var listeners = new Array();
    var that = this;

    this.initModel = function () {
      this.getUserCandidatures();
      this.getUserUnsolicitedCandidatures();
    };
    
    this.getUserCandidaturesSuccess = function(xml){
    	var listJobApplications = new Array();
			$(xml).find('jobApplication').each(function(){
				var jobApplication = new $.JobApplication($(this));
				listJobApplications[jobApplication.jobApplicationId]=jobApplication;
			});
			that.notifyJobApplicationsLoaded(listJobApplications);
		};
		
    this.getUserCandidaturesError = function(xhr,status,error){
			isSessionExpired(xhr.responseText);
		};
	
		this.getUserCandidatures = function(){
			genericAjaxCall('GET', staticVars["urlBackEndCandidate"]+'jobApplication/candidateJobApplications', 'application/xml', 
					'xml','', that.getUserCandidaturesSuccess, that.getUserCandidaturesError);
		};
		
		this.getUserUnsolicitedCandidaturesSuccess = function(xml){
			var listUnsolicitedJobApplications = new Array();
			$(xml).find('unsolicitedJobApplication').each(function(){
				var unsolicitedJobApplication = new $.UnsolicitedJobApplication($(this));
				listUnsolicitedJobApplications[unsolicitedJobApplication.jobApplicationId]=unsolicitedJobApplication;
			});
			that.notifyUnsolicitedJobApplicationsLoaded(listUnsolicitedJobApplications);
		};
	
		this.getUserUnsolicitedCandidaturesError = function(xhr,status,error){
			isSessionExpired(xhr.responseText);
		};
	
		this.getUserUnsolicitedCandidatures = function(){
			genericAjaxCall('GET', staticVars["urlBackEndCandidate"]+'jobApplication/candidateUnsolicitedJobApplications', 'application/xml', 
					'xml','', that.getUserUnsolicitedCandidaturesSuccess, that.getUserUnsolicitedCandidaturesError);
		};
	
		this.notifyJobApplicationsLoaded = function(listJobApplications){
			$.each(listeners, function(i) {
				listeners[i].jobApplicationsLoaded(listJobApplications);
			});
		};
		
		this.notifyUnsolicitedJobApplicationsLoaded = function(listUnsolicitedJobApplications){
			$.each(listeners, function(i) {
				listeners[i].unsolicitedJobApplicationsLoaded(listUnsolicitedJobApplications);
			});
		};
	
	  this.addListener = function (list) {
	    listeners.push(list);
	  };
  },
  ModelListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }

});