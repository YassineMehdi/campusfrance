jQuery.extend({
  AgendasController: function (model, view) {
    var that = this;
    var agendaModelListener = new $.AgendaModelListener({
      agendasSuccess: function (events) {
        view.displayAgendas(events);
      },
      agendasError: function (xhr) {
        view.displayAgendasError(xhr);
      }
    });
    model.addListener(agendaModelListener);

    // listen to the view
    var agendaViewListener = new $.AgendaViewListener({
      agendas: function (standId) {
        model.agendas(standId);
      }
    });
    view.addListener(agendaViewListener);

    this.initController = function () {
      view.initView();
      model.initModel();
    };
    that.initController();
  }

});
