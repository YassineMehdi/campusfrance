jQuery.extend({
	DocumentManage: function (document, documentsContainer, view) {
    var isFavorite = factoryFavoriteExist(document.getAllFilesId(), FavoriteEnum.MEDIA);
    var displayDeleteFavorite = "block";
    var displayAddFavorite = "block";
    if (isFavorite)
      displayAddFavorite = "none";
    else
      displayDeleteFavorite = "none";

    var dom = $('<li>'
            + '<div class="infos">'
            + '<ul>'
            + '<li>'
            + '<div class="col-md-12 col-sm-12 no-padding">'
            + '<a class="document-name">'
            + htmlEncode(substringCustom(formattingLongWords(document.getTitle(), 20), 40))
            + '</a>'
            + '</div>'
            + '</li>'
            + '<li><a class="btn green col-md-12 addDocumentToFavorite" style="display:' + displayAddFavorite + '" ><i aria-hidden="true" class="glyphicon glyphicon-star"></i><span class="productActionLabel">'+ADDTOFAVORITELABEL+'<span></a><a class="btn btn-primary col-md-12 deleteDocumentFromFavorite" style="background-color: #b3081b !important;display:' + displayDeleteFavorite + '"><i aria-hidden="true" class="glyphicon glyphicon-star"></i><span class="productActionLabel">' + DELETEFROMFAVORITELABEL + '</span></a></li>'
            + '<li><a class="btn btn-primary col-md-12 blue openCurrentDocument "><i class="fa fa-info-circle" aria-hidden="true"></i><span class="productActionLabel viewLabel"></span></a></li>'
            + '</ul>'
            + '</div>'
            + '</li>');

    this.refresh = function () {
      dom.find(".openCurrentDocument").unbind();
      dom.find(".openCurrentDocument").bind('click', function () {
      	view.openDocument(document);
      });

      var addDocumentFavoriteLink = dom.find(".addDocumentToFavorite");
      var deleteDocumentFavoriteLink = dom.find(".deleteDocumentFromFavorite");

      addDocumentFavoriteLink.click(function () {
        if (connectedUser) {
          var idEntity = document.getAllFilesId();
          var componentName = FavoriteEnum.MEDIA;
          var favoriteXml = factoryXMLFavory(idEntity, componentName);
          addFavorite(favoriteXml);
          $(this).hide();
          $(this).closest('div').find('.deleteDocumentFromFavorite').show();
        } else {
          getStatusNoConnected();
        }
      });

      deleteDocumentFavoriteLink.click(function () {
        var idEntity = document.getAllFilesId();
        var componentName = FavoriteEnum.MEDIA;
        var favoriteXml = factoryXMLFavory(idEntity, componentName);
        deleteFavorite(favoriteXml);
        $(this).hide();
        $(this).closest('div').find('.addDocumentToFavorite').show();
      });
    };
    this.getDOM = function () { //retourne une ligne HTML <a .....
      return dom;
    };
    this.refresh();
  },
  DocumentView: function () {
    var listeners = new Array();
    var that = this;
    var galleriesDocuments = null;
    var infoDocumentHandler = null;
    var galleryDocumentHandler = null;
    var galleryTitleHandler = null;
    var documentsContentHandler = null;
    
    this.initView = function () {
	    $('.genericContent').hide();
      $('#modal-sign-in').modal('hide');
      $('#content_documents').show();
      $('#page-content').removeClass('no-padding');
      $('#content').show();   
      $('#content_documents').show();
      infoDocumentHandler = $('#content_documents .documents');
    };

    this.loadDocuments = function (documents) {
      galleriesDocuments = documents;
      if (documents != null && Object.keys(documents).length > 0) {
      	infoDocumentHandler.html('<div class="visuelG col-md-9 col-sm-9 no-padding">'
            +'<h3 class="documentTitle"></h3>'
	            +'<div id="galleryDocument">'
		            +'<div class="ad-image-wrapper">'
		            +'</div>'
	            +'</div>'
            +'</div>'
            +'<div class="listing scroll-pane col-md-4 col-sm-4">'
	            +'<ul class="ul-docs">'
	            +'</ul>'
            +'</div>');
      galleryDocumentHandler = infoDocumentHandler.find('.ad-image-wrapper');
      galleryTitleHandler = infoDocumentHandler.find(".documentTitle");
      documentsContentHandler = infoDocumentHandler.find('.ul-docs');
      var documentManage = null;
      for (var index in documents) {
        documentManage = new $.DocumentManage(documents[index], documentsContentHandler, that);
        documentsContentHandler.append(documentManage.getDOM());
      }
      that.openDocumentByIndex(0);
      infoDocumentHandler.find('.scroll-pane').jScrollPane({ autoReinitialise: true });
     } else {
  	   infoDocumentHandler.html('<p class="empty noDocumentsLabel"></p>');
     }
     traduct();
     $('.categList').hide();       
     $('.modal-loading').hide();
    };
    this.openDocumentByIndex = function (index) {
      if(galleriesDocuments != null){
	      that.openDocument(galleriesDocuments[index]);
      }
    };
    this.openDocument = function (document) {
  	  if(document != null){
  	    openDocument(document, galleryDocumentHandler, galleryTitleHandler);
  	  }
    };

    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  ViewListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }
});

