jQuery.extend({
  PhotoStandManage: function (photo, view) {
    var photoId = photo.getAllFilesId();
    var isFavorite = factoryFavoriteExist(photo.getAllFilesId(), FavoriteEnum.MEDIA);
    var displayDeleteFavorite = "block";
    var displayAddFavorite = "block";
    if (isFavorite)
      displayAddFavorite = "none";
    else
      displayDeleteFavorite = "none";

    var dom = $('<li>'
            + '<div class="favoris">'
            + '<ul>'
            + '<li><a class="icon-etoile addPhotoToFavorite addPhotoRhFavoriteId_' + photoId + '" style="display:' + displayAddFavorite + '"></a><a class="icon-delete deletePhotoFromFavorite deletePhotoRhFavoriteId_' + photoId + '" style="display:' + displayDeleteFavorite + '"></a></li>'
            + '<li><a class="icon-galerie openCurrentPhoto"></a></li>'
            + '</ul>'
            + '</div>'
            + '<div class="visuel">'
            + '<a class="openCurrentPhoto">'
            + '<img src="img/visuel2.jpg" alt="">'
            + '</a>'
            + '</div>'
            + '<span>' + htmlEncode(substringCustom(formattingLongWords(photo.getTitle(), 20), 60)) + '</span>'
            + '</li>');

    this.refresh = function () {
      dom.find(".openCurrentPhoto").unbind();
      dom.find(".openCurrentPhoto").bind('click', function () {
        $(".agrandir-photo").bind('click', function () {
          $('#modal-document-lecteur-big').modal();
          openPhoto(photo, $("#modal-document-lecteur-big").find('.ad-image-wrapper'), $("#modal-document-lecteur-big").find('.modal-title'));
        });
    	  view.openPhoto(photo);
      });

      var addPhotoFavoriteLink = dom.find(".addPhotoToFavorite");
      var deletePhotoFavoriteLink = dom.find(".deletePhotoFromFavorite");

      addPhotoFavoriteLink.click(function () {
        if (connectedUser) {
          var idEntity = photo.getAllFilesId();
          var componentName = FavoriteEnum.MEDIA;
          var favoriteXml = factoryXMLFavory(idEntity, componentName);
          addFavorite(favoriteXml);
          $(this).hide();
          $(this).closest('div').find('.deletePhotoFromFavorite').show();
          $('.addPhotoRHToFavoriteButton_' + photoId).hide();
          $('.deletePhotoRHFromFavoriteButton_' + photoId).show();
        } else {
          getStatusNoConnected();
        }
      });

      deletePhotoFavoriteLink.click(function () {
        var idEntity = photo.getAllFilesId();
        var componentName = FavoriteEnum.MEDIA;
        var favoriteXml = factoryXMLFavory(idEntity, componentName);
        deleteFavorite(favoriteXml);
        $(this).hide();
        $(this).closest('div').find('.addPhotoToFavorite').show();
        $('.addPhotoRHToFavoriteButton_' + photoId).show();
        $('.deletePhotoRHFromFavoriteButton_' + photoId).hide();
      });
    };
    this.getDOM = function () { //retourne une ligne HTML <a .....
      return dom;
    };
    this.refresh();
  },
  PhotoStandView: function () {
    var listeners = new Array();
    var that = this;
    var galleriesPhotos = null;
    var infoStandPhotoHandler = null;
    var galleryPhotoHandler = null;
    var galleryTitleHandler = null;
    var photosContentHandler = null;

    $('#modal-photo, #modal-photo-reception').on('hidden.bs.modal', function () {
    	$(this).find('.info-stand-photo').empty();
    });

    this.initView = function (standId) {
      var standId = $('#temoignage').attr('standId');
      $('#stand-photo-link').unbind('click').bind('click', function () {
        that.addModalPhotoListener("#modal-photo", standId);
        infoStandPhotoHandler =  $('#modal-photo .info-stand-photo');
      });
      $('#stand-photo-recep-link').unbind('click').bind('click', function () {
        that.addModalPhotoListener("#modal-photo-reception", standId);
        infoStandPhotoHandler =  $('#modal-photo-reception .info-stand-photo');
      });
    };
    this.addModalPhotoListener = function(selector, standId){
    	if(!($(selector).data('bs.modal') || {}).isShown) {
        factoryActivateFaIcon(this);
        $('.modal-loading').show();
        $('.modal').modal('hide');
        $(selector).modal('show');
        that.notifyPhotoStand(standId);
      }
    };
    this.displayPhotoStand = function (photos) {
      galleriesPhotos = photos;
      if (photos != null && Object.keys(photos).length > 0) {
      	infoStandPhotoHandler.html('<div class="col-md-8 col-sm-8 no-padding">'
            +'<h3 class="photoTitle"></h3>'
	            +'<div id="galleryPhoto">'
		            +'<div class="ad-image-wrapper">'
		            +'</div>'
	            +'</div>'
            +'</div>'
            +'<div class="listing scroll-pane col-md-3 col-sm-3">'
	            +'<ul class="ul-photo">'
	            +'</ul>'
            +'</div>');
      	galleryTitleHandler = $('.photo-title-head');
      	galleryPhotoHandler = infoStandPhotoHandler.find('.ad-image-wrapper');
      	photosContentHandler = infoStandPhotoHandler.find('.ul-photo');
      	var photoStandManage = null;
      	for (var index in photos) {
      		photoStandManage = new $.PhotoStandManage(photos[index], that);
      		photosContentHandler.append(photoStandManage.getDOM());
      	}
      	that.openPhotoByIndex(0);
      	infoStandPhotoHandler.find('.scroll-pane').jScrollPane({ autoReinitialise: true });
      } else {
      	infoStandPhotoHandler.html('<p class="empty">'+NOPICTURELABEL+'</p>');
      }
      $('.modal-loading').hide();
    };
    this.openPhotoByIndex = function (index) {
      if(galleriesPhotos != null){
        $(".agrandir-photo").bind('click', function () {
          $('#modal-document-lecteur-big').modal();
          openPhoto(galleriesPhotos[index], $("#modal-document-lecteur-big").find('.ad-image-wrapper'), $("#modal-document-lecteur-big").find('.modal-title'));
        });
	      that.openPhoto(galleriesPhotos[index]);
      }
    };
    this.openPhoto = function (photo) {
  	  if(photo != null){
  		openPhoto(photo, galleryPhotoHandler, galleryTitleHandler);
  	  }
    };
    this.notifyPhotoStand = function (standId) {
      $.each(listeners, function (i) {
        listeners[i].launchPhotoStand(standId);
      });
    };

    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  ViewListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }
});


