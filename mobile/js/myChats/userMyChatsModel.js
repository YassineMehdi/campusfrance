jQuery.extend({
	
	UserMyChatsModel : function() {
		var listeners = new Array();
		var that = this;
		this.addListener = function(list) {
			listeners.push(list);
		};
		
		this.initModel = function(){
			that.getHistoryPublicChats("01/01/2016", "31/11/2017");
			setTimeout(function(){
					that.getHistoryPrivateChats("01/01/2016", "31/11/2017");
				}, 1000);
		};
		this.getHistoryPublicChatsError = function(xhr,status,error){
			isSessionExpired(xhr.responseText);
		};
		this.getHistoryPublicChatsSuccess = function (xml){
			$(xml).find("chatPublicHistory").each(function () {
		    	var historicChat=new Object();
		    	historicChat.id=$(this).find('chatPublicHistoryId').text();
		    	historicChat.title=$(this).find('standDTO name').text()+" - "+$(this).find('eventTitle').text();
		    	historicChat.date=$(this).find('date').text();
		    	historicChat.path=$(this).find('path').text();
		    	historicChat.languageId=$(this).find('language > idlangage').text();
		    	historicChat.type=PUBLIC;
		    	historicChat.tagDate=$(this).find('tagDate').text();
		    	listHistoricChatCache.push(historicChat);
		    });
			that.notifyHistoryPublicChatsLoaded();
		};
		this.getHistoryPublicChats = function(dateBegin, dateEnd){
			genericAjaxCall('POST', staticVars["urlBackEndCandidate"]+"chatpublichistory/listchat?dateBegin="+dateBegin+'&dateEnd='+dateEnd, 'application/xml', 'xml','',that.getHistoryPublicChatsSuccess,that.getHistoryPublicChatsError);
		};
		this.getHistoryPrivateChatsError = function(xhr,status,error){
			isSessionExpired(xhr.responseText);
		};
		this.getHistoryPrivateChatsSuccess = function (xml){
		    $(xml).find("chatPrivateHistory").each(function () {
		    	var historicChat=new Object();
		    	historicChat.id=$(this).find('chatPrivateHistoryId').text();
		    	historicChat.title=$(this).find('userProfileDTO2').find('prenomUser').text()+" "+$(this).find('userProfileDTO2').find('nomUser').text();
		    	historicChat.date=$(this).find('date').text();
		    	historicChat.path=$(this).find('path').text();
		    	historicChat.tagDate=$(this).find('tagDate').text();
		    	historicChat.type=PRIVATE;
		    	listHistoricChatCache.push(historicChat);
		    });
			that.notifyHistoryPrivateChatsLoaded();
		};
		this.getHistoryPrivateChats = function(dateBegin, dateEnd){
			genericAjaxCall('POST', staticVars["urlBackEndCandidate"]+"chatprivatehistory/listchat?dateBegin="+dateBegin+'&dateEnd='+dateEnd, 'application/xml', 'xml','',that.getHistoryPrivateChatsSuccess,that.getHistoryPrivateChatsError);
		};
		this.notifyHistoryPublicChatsLoaded = function(){
			$.each(listeners, function(i) {
				listeners[i].historyPublicChatsLoaded();
			});
		};
		this.notifyHistoryPrivateChatsLoaded = function(){
			$.each(listeners, function(i) {
				listeners[i].historyPrivateChatsLoaded();
			});
		};

	},
	UserMyChatsModelListener : function(list) {
		if (!list)
			list = {};
		return $.extend(list);
	},

});
