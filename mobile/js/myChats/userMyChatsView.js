jQuery.extend({
  UserMyChatsView: function () {
    var listeners = new Array();
    var that = this;
    var dateBeginHistoryChatHandler = null;
    var dateEndHistoryChatHandler = null;
    //var languageHistoryChatHandler = null;
    var typeHistoryChatHandler = null;
    var searchButton = null;

    this.initHistory = function () {
      $('.modal-loading').show();
      //console.log("initHistory");
      $('.genericContent').hide();
      $('#myChats').show();
      $('#page-content').removeClass('no-padding');
      $('#myChats .glyphicon ').addClass('active');
    };

    this.beforeInitView = function() {
      dateBeginHistoryChatHandler = $('#dateBeginHistoryChat');
      dateEndHistoryChatHandler = $('#dateEndHistoryChat');
      typeHistoryChatHandler = $('#typeHistoryChat');
      searchButton = $('#historicTchatSearchButton');

      //dateBeginHistoryChatHandler.val("01 january, 2000");
      //dateEndHistoryChatHandler.val("02 january, 2000");
      typeHistoryChatHandler.change(that.displayHistoryChats);

      searchButton.bind("click", function () {
        //console.log('Inside Search Button');
        that.displayHistoryChats();
      });

      $('#myChats .scroll-pane').jScrollPane({ autoReinitialise: true });
    	var minDate = new Date(2016, 08, 01); 
      var maxDate = new Date(2017, 11, 01);
      $('#dateBeginHistoryChat').pickadate({
//        monthsFull: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
//        monthsShort: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
//        weekdaysFull: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
//        weekdaysShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
//        weekdaysLetter: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 1, // Creates a dropdown of 15 years to control year
//        today: "Aujourd'hui",
//        clear: 'Vider',
//        close: 'Fermer',
      });

      $('#dateEndHistoryChat').pickadate({
//        monthsFull: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
//        monthsShort: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
//        weekdaysFull: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
//        weekdaysShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
//        weekdaysLetter: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 1, // Creates a dropdown of 15 years to control year
//        today: "Aujourd'hui",
//        clear: 'Vider',
//        close: 'Fermer',
      });
      $("#dateBeginHistoryChat").pickadate('picker').set("select", minDate);
      $("#dateEndHistoryChat").pickadate('picker').set("select", maxDate);
      $('#typeHistoryChat').material_select();
    	that.initDatatableHistoryChatView();
    };
    this.initDatatableHistoryChatView = function() {
      $('#table-liste-hisoric-chat').dataTable({
        "aaData": [],
        "aoColumns": [
          {
            "sTitle": CHATTYPELABEL,"sWidth": "20%",
          },
          {
            "sTitle": TITLELABEL,"sWidth": "20%",
          },
          {
            "sTitle": DATELABEL,"sWidth": "20%",
          },
          {
            "sTitle": ACTIONSLABEL,"sWidth": "30%",
          }],
        "bAutoWidth": false,
        "bRetrieve": false,
        "bDestroy": true,
        "bFilter": false,
        "bLengthChange": false,
        "iDisplayLength": 10,
        "language": {
          "url": "dataTable/i18n/"+dataTableLang+".json"
        },
        "fnDrawCallback": function () {
          $('#table-liste-hisoric-chat .openFile').off();
          $('#table-liste-hisoric-chat .openFile').on("click", function () {
            var index = $(this).attr('id');
            var historicChat = listHistoricChatCache[index];
            /*$.cookie("path",historicChat.path);
             $.cookie("tagDate",new Date(historicChat.tagDate).getTime());*/
            var pathCrypt = $.base64.encode(historicChat.path);
            var tagDateCrypt = $.base64.encode(new Date(historicChat.tagDate).getTime());
            window.open(
                    'chathistory.html?path=' + pathCrypt + "&tagDate=" + tagDateCrypt,
                    '_blank');
          });
        }
      });
    }; 
    this.displayHistoryChats = function () {
      var dateFrom = new Date($("#dateBeginHistoryChat").pickadate('picker').get("select").obj);
      var dateTo = new Date($("#dateEndHistoryChat").pickadate('picker').get("select").obj);
      var typeHistoryChatValue = typeHistoryChatHandler.val();
      var historicChatDate = null;
      listDisplayedHistoricChat = [];
      
      $(listHistoricChatCache).each(
              function (index, historicChat) {
            	historicChatDate = new Date(historicChat.date);
                if (dateFrom <= historicChatDate && historicChatDate <= dateTo) {
                  if (typeHistoryChatValue == ALL || typeHistoryChatValue == historicChat.type) {
                    var typeLabel = "";
                    if (historicChat.type == PRIVATE)
                      typeLabel = PRIVATECHATLABEL;
                    else if (historicChat.type == PUBLIC)
                      typeLabel = PUBLICCHATLABEL;
                    var historicChatData = that.addHistoryChat(index, typeLabel, historicChat.title, historicChat.date, historicChat.languageId);
                    listDisplayedHistoricChat.push(historicChatData);
                  }
                }
              });
			that.addTableDatas('#table-liste-hisoric-chat', listDisplayedHistoricChat);
      $('.modal-loading').hide();
    };
    this.addHistoryChat = function (index, type, title, date, languageId) {
      date = new Date(date);
      date = date.toLocaleDateString(LANGAUGETAGLOCALELABEL.replace('_', '-'));
      var chatTypeComponent = "<span style='color:black;'>" + type + "</span>";
      var titleComponent = "<span style='color:black;'>" + formattingLongWords(title, 30) + "</span>";
      var dateComponent = "<span style='color:black;'>" + date + "</span>";
      var openFileComponent = "<a href='#' class='btn btn-success btn-middle openFile' src='#' id='" + index + "'> " + OPENLABEL + " </a>";
      return [
        chatTypeComponent,
        titleComponent,
        dateComponent,
        openFileComponent
      ];
    };
    this.historyPublicChatsLoaded = function () {
      that.displayHistoryChats();
    };
    this.historyPrivateChatsLoaded = function () {
      $('.categList').hide(); 
      $('#content').show();
      that.displayHistoryChats();
    };
    this.getHistoryPublicChats = function () {
      var fromDate = changeDateToString(new Date(dateBeginHistoryChatHandler.val()));
      var toDate = changeDateToString(new Date(dateEndHistoryChatHandler.val()));

      that.notifyGetHistoryPublicChats(fromDate, toDate);
    };
    this.getHistoryPrivateChats = function () {
      var fromDate = changeDateToString(new Date(dateBeginHistoryChatHandler.val()));
      var toDate = changeDateToString(new Date(dateEndHistoryChatHandler.val()));

      that.notifyGetHistoryPrivateChats(fromDate, toDate);
    };
    this.addTableDatas = function(selector, datas){
    	var oTable = $(selector).dataTable();
      oTable.fnClearTable();
      if(datas.length > 0) oTable.fnAddData(datas);
      oTable.fnDraw();
    };
  	that.beforeInitView();

    this.notifyGetHistoryPublicChats = function (dateBegin, dateEnd) {
      $.each(listeners, function (i) {
        listeners[i].getHistoryPublicChats(dateBegin, dateEnd);
      });
    };

    this.notifyGetHistoryPrivateChats = function (dateBegin, dateEnd) {
      $.each(listeners, function (i) {
        listeners[i].getHistoryPrivateChats(dateBegin, dateEnd);
      });
    };

    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  UserMyChatsViewListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  },
});


