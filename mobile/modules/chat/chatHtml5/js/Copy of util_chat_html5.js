/* 
 * 
 * CS:Chat stand 
 * CG:Chat général
 * 
 * */	
	var currentStand=null;
	var list_Stands_lang=null;
	var currentStand_CS=null;
	var listRegistersChat=null;
	//var checkboxChatHandler=null;
	var saveChatButtonHandler=null;
	var stopSaveChatButtonHandler=null;
	
	function traductChatHtml5G(){//G : Général
		factoryTraductChatHtml5(CGTAG);
	}
	function getInfoForChatSuccess(xml){
		var candidateId = $(xml).find('candidateId').text();
		if ($(xml).text() != "" && candidateId != 0) {
			userId=$(xml).find('candidateDTO > userProfileDTO > userProfileId:first').text();
			fullName=$(xml).find('candidateDTO > userProfileDTO:first').find('prenomUser').text()+' '+$(xml).find('userProfileDTO').find('nomUser').text();
			if($(xml).find('candidateDTO > userProfileDTO > logopath:first').text().indexOf('default_avatar') !== -1) photoURL = "http://"+HOSTNAME_URL+"/visitor/images/default_avatar.jpg";
			else photoURL = $(xml).find('candidateDTO > userProfileDTO > logopath:first').text();
			profileUrl= PROFILE_URL+$(xml).find('candidateId').text();
			$(xml).find("chatPrivateHistory").each(function() {
				var userFiendId=$(this).find('userProfileDTO1').find('userProfileId').text();
				if(userFiendId==userId) userFiendId=$(this).find('userProfileDTO2').find('userProfileId').text();
				addRegisterChat(userFiendId);
			});
			$(xml).find("chatPublicHistory").each(function() {
				var standId=$(this).find('standDTO').find('idstande').text();
				var languageId=$(this).find('language').find('idlangage').text();
				var eventId=$(this).find('eventDTO').find('idEvent').text();
				var topic=buildTopic(standId, languageId, eventId);
				addRegisterChat(topic);
			});
			getCacheStand();
			initStandAthmosphere();
			initChatComponent();
	  	}
	}
	
	function getInfoForChatError(xhr,status,error){
		isSessionExpired(xhr.responseText);
	}
	
	function initChatHtml5(){
		$('#replay_date').text(currentDate());
		traductChatHtml5G();
        listRegistersChat=new Array();
		genericAjaxCall('POST', staticVars["urlBackEndCandidate"]+'chat/infoChat', 'application/xml', 'xml','', getInfoForChatSuccess,getInfoForChatError);
	}
	
	function getCacheStand(){
		genericAjaxCall('GET', staticVars["urlBackEndEnterprise"]+'chat/stand/cache', 'application/json', 'json','', function(json) {
				  $.each(json, function(standId, stands){
					  list_Stands_lang=stands.list_Stands_lang;
					  $.each(list_Stands_lang, function(languageId, manageStandEvent){
						  $.each(manageStandEvent.list_Stands, function(chatId, stand){
							  manageStandConnection(stand);
						  });
					  });
				  });
			  },
			  function(){					  				        
				  console.log("error getCacheStand");
			  }
			);	
	}
	function manageStandConnection(stand){
		var list_stand_connect="list_stand_connect";
		var list_stand_connect_CS=list_stand_connect+CSTAG;
		var connectStand="connectStand"+CGTAG;
		var connectStand_CS=connectStand+CSTAG;
		var list_stand='list_stand'; 
		var list_stand_CS=list_stand+CSTAG;
		list_stand_connect+=CGTAG;
		connectStand+=CGTAG;
		list_stand+=CGTAG;
		factoryManageStandConnection(stand, list_stand_connect, connectStand, list_stand, CGTAG);
		if(isFromParentStand(stand)) factoryManageStandConnection(stand, list_stand_connect_CS, connectStand_CS, list_stand_CS, CSTAG);
	}
	function factoryManageStandConnection(stand, list_stand_connect_Id, connectStandTag, list_stand, tagModule){
		if (stand!=null && stand.topicStand != null) {
			//console.log("*********** topicStand : "+stand.topicStand+" ************** operation : "+stand.operation);
			if (stand.operation == ADD_STAND){
				if (document.getElementById(list_stand_connect_Id)!=null) addConnectStand("#"+list_stand_connect_Id, connectStandTag, stand.topicStand, stand.standName, stand.chatTitle, stand.photoURL, stand.numberRecruiter, stand.languageId, list_stand, tagModule);
			}else if (stand.operation == UPDATE_STAND){
				if (document.getElementById(list_stand_connect_Id)!=null) editConnectStand("#"+list_stand_connect_Id, connectStandTag, stand.topicStand, stand.standName, stand.chatTitle, stand.photoURL, stand.numberRecruiter, stand.languageId, list_stand, tagModule);
			}else if (stand.operation == REMOVE_STAND) delConnectStand(connectStandTag, stand.topicStand, tagModule);
		}
	}
	function isFromParentStand(stand){
		if(stand.standId==currentStandId && stand.languageId==currentChatStandLanguageId) return true;
		else return false;
	}
	function isFromParentStandFromTopic(topicStand){
		var topicArray=topicStand.split('_');
		if(topicArray[0]==currentStandId && topicArray[1]==currentChatStandLanguageId) return true;
		else return false;
	}	
	function initChatComponent() {
		var inputTextChatHandler = $('#inputTextChat'+CGTAG);
		var sendMessageStandButtonHandler= $('#sendMessageButton'+CGTAG);
		//var chatReduceStandsButtonHandler= $('#chatReduceStandsButton'+CGTAG);
		//var fullScreenChatSpanHandler= $('#fullScreenChatSpan'+CGTAG);
		//var exitFullScreenChatSpanHandler= $('#exitFullScreenChatSpan'+CGTAG);
		//checkboxChatHandler=$('#checkboxChat');
		saveChatButtonHandler=$('#saveChatButton'+CGTAG);
		stopSaveChatButtonHandler=$('#stopSaveChatButton'+CGTAG);
		sendMessageStandButtonHandler.bind("click", function() {
			if (inputTextChatHandler.val() != '' && currentStand!=null){
				sendMessage(currentStand, inputTextChatHandler.val());
				inputTextChatHandler.val('');	
			}
		});
	
		inputTextChatHandler.bind('keydown', function(e) {
			if (e.keyCode == 13 && e.ctrlKey) {
				if (inputTextChatHandler.val() != '' && currentStand!=null){
					sendMessage(currentStand, inputTextChatHandler.val());
					inputTextChatHandler.val('');	
				}
			}
		});
		/*
		chatReduceStandsButtonHandler.bind("click", function() {
			extendScreenChat('#chatListStands .standsConnect', '#chatListStands .separateStandsConnectChat', '#chatListStands .coprsChat', '#fullScreenChatSpan', '#exitFullScreenChatSpan');
		});
		
		fullScreenChatSpanHandler.bind("click", function() {
			extendScreenChat('#chatListStands .standsConnect', '#chatListStands .separateStandsConnectChat', '#chatListStands .coprsChat', '#fullScreenChatSpan', '#exitFullScreenChatSpan');
		});
		
		exitFullScreenChatSpanHandler.bind("click", function() {
			reduceScreenChat('#chatListStands .standsConnect', '#chatListStands .separateStandsConnectChat', '#chatListStands .coprsChat', '#fullScreenChatSpan', '#exitFullScreenChatSpan');
		});*/
		/*checkboxChatHandler.bind("change", function() {
            var check = $(this).is(":checked");
            if(currentStand!=null){
            	var currentStandArray=currentStand.split(SEPARATEPUBLICCHATSO);
	            standId = currentStandArray[0];
	            languageId = currentStandArray[1];
	            chatId = currentStandArray[2];
	            updateRegisterChatCall(check, standId, languageId, chatId);
            }else checkboxChatHandler.prop('checked', !check);
		});*/
		/*$('#viewHistory'+CGTAG).click(function(){
			$('#userMyChatsLink').trigger("click");
			$('#reducePublicChat').trigger("click");
		});
		saveChatButtonHandler.click(function(){
			factoryUpdateRegisterChatCall(currentStand, true);
		});
		stopSaveChatButtonHandler.click(function(){
			factoryUpdateRegisterChatCall(currentStand, false);
		});*/
	}
	function factoryUpdateRegisterChatCall(stand, register){
		if(stand!=null){
        	var standArray=stand.split(SEPARATEPUBLICCHATSO);
            standId = standArray[0];
            languageId = standArray[1];
            chatId = standArray[2];
            updateRegisterChatCall(register, standId, languageId, chatId);
		}
	}
	function updateRegisterChatCall(register, standId, languageId, chatId){
        genericAjaxCall('POST', staticVars["urlBackEndCandidate"]+"userprofile/registerpublicchat?register="+register+"&standId="+standId+"&languageId="+languageId+"&chatId="+chatId,
        		'plain/text', 'xml', '', function(response){
        	updateRegisterChat(register, buildTopic(standId, languageId, chatId));
    		updateCheckboxsChat(buildTopic(standId, languageId, chatId));
        }, function(xhr,textStatus,error){
        	if(xhr.status==404) {
        		updateRegisterChat(!register, buildTopic(standId, languageId, chatId));
        		updateCheckboxsChat(buildTopic(standId, languageId, chatId));
        		alert(PLEASETRYAGAINLABEL);
    	    }
        }, false);	
    }
	function extendScreenChat(standsConnectSelector, separateStandsConnectChatSelector, coprsChatSelector, fullScreenChatSpanSelector, exitFullScreenChatSpanSelector){
		$(standsConnectSelector).css({"width":"0%","display":"none"});
		$(separateStandsConnectChatSelector).css({"width":"0%","display":"none"});
		$(coprsChatSelector).css("width","100%");
		$(fullScreenChatSpanSelector).css("display","none");
		$(exitFullScreenChatSpanSelector).css("display","block");
	}
	
	function reduceScreenChat(standsConnectSelector, separateStandsConnectChatSelector, coprsChatSelector, fullScreenChatSpanSelector, exitFullScreenChatSpanSelector){
		$(standsConnectSelector).removeAttr( 'style' );
		$(separateStandsConnectChatSelector).removeAttr( 'style' );
		$(coprsChatSelector).removeAttr( 'style' );
		$(fullScreenChatSpanSelector).removeAttr( 'style' );
		$(exitFullScreenChatSpanSelector).css("display","none");
	}
	
	function getChatStandCache(topicStand, tagModule){
		if(listStands[topicStand]==null) {
			jQuery.ajax({
				  url: SERVER_BE_URL+'chat/chatmodule/'+topicStand+'/cache/chat',  
				  type: "GET",
				  contentType: "application/json",
				  dataType : 'json',
				  success: function(json){
					  	addMessagesToStand(json, topicStand);
					  	getMessagesByStand(topicStand, tagModule);
					},
				  error: function(){					  				        
					  //alert("error getCacheChat");
					}
				});	
		}else getMessagesByStand(topicStand, tagModule);
	}

	function addMessagesToStand(manageMessage, topicStand){
		if(manageMessage){
		  	listStands[topicStand]=new Array();
			$.each(manageMessage.listMessage, function(key, message){
					message.senderLogin=decodeURIComponent(message.senderLogin.trim());
					message.messageText=decodeURIComponent(message.messageText.trim());
				addMessageToCache(message, topicStand);
			});
		}
	}
	
	function getMessagesByStand(topicStand, tagModule){
		var messages_ui='messages_ui'+tagModule;
		if(document.getElementById(messages_ui)!=null){
			var contentTabsId='content_tabs'+tagModule;
			if(isIe_Ver()){//To change with another best way
				var height=$('#'+contentTabsId).height();
				$('#'+contentTabsId).height(height);
				$('#'+contentTabsId).css("margin-bottom","1px");
			}
			$('#'+messages_ui).empty();
			if(listStands[topicStand]!=null) {
				$.each(listStands[topicStand], function(key, message){
					var senderLogin=message.senderLogin.trim();
					var messageText=message.messageText.trim();
					try {
						senderLogin=decodeURIComponent(senderLogin);
				    }
				    catch(err) {
				        var text = "There was an error on this page.\n\n";
				        text += "Error description: " + err.message + "\n\n";
				        text += "senderLogin: " +senderLogin + "\n\n";
				        text += "Click OK to continue.\n\n";
				        //console.log(text);
				    }
				    try {
						messageText=decodeURIComponent(messageText);
				    }
				    catch(err) {
				        var text = "There was an error on this page.\n\n";
				        text += "Error description: " + err.message + "\n\n";
				        text += "messageText: " +messageText + "\n\n";
				        text += "Click OK to continue.\n\n";
				        //console.log(text);
				    }
					addMessageToStandChat(messages_ui, contentTabsId, message.messageId, senderLogin, messageText, message.photoURL, message.dateTime, message.operation);
				});
			}
		}
	}

	function receivedMessage(messageId, senderId, senderLogin, receiverId, messageText, photoURL, dateTime, operation) {
		factoryReceivedMessage(CGTAG, currentStand, messageId, senderId, senderLogin, receiverId, messageText, photoURL, dateTime, operation);
		factoryReceivedMessage(CSTAG, currentStand_CS, messageId, senderId, senderLogin, receiverId, messageText, photoURL, dateTime, operation);
		
	}
	function factoryReceivedMessage(tagModule, currentStand, messageId, senderId, senderLogin, receiverId, messageText, photoURL, dateTime, operation){
		if(currentStand==receiverId){
			var messages_ui="messages_ui"+tagModule;
			var content_tabs="content_tabs"+tagModule;
			try {
		    	senderLogin=decodeURIComponent(senderLogin.trim());
		    }
		    catch(err) {
		    }
		    try {
				messageText=decodeURIComponent(messageText.trim());
		    }
		    catch(err) {
		    }
			addMessageToStandChat(messages_ui, content_tabs, messageId, senderLogin, messageText, photoURL, dateTime, operation);
		}else {
			var list_stand='list_stand'+tagModule; 
			$('#'+list_stand+' #'+receiverId+tagModule).blink(1000);
		}
	}
	function addMessageToCache(message, topicStand) {
		if(listStands[topicStand]==null) listStands[topicStand]=new Array();
		messagesStand=listStands[topicStand];
		messagesStand.push(message);
	}
	function replaceAllProbCastHtmlTitle(str){
		return str.split('"').join("&quot;");
	}
	function addMessageToStandChat(idMessagesUi, contentTabsId, messageId, senderLogin, messageText, photoURL, dateTime, operation){
		if (document.getElementById(idMessagesUi) != null) {
			//console.log("addMessageToStandChat "+operation);
			if(operation==ADD_MESSAGE) {
				$('#'+idMessagesUi).append('<tr id="msg'+messageId+'">'+
									'<td class="extrDate" valign="top">'+
										'<div>'
											+'<span>'
													+ extractDate(dateTime) +
											'</span>'+
										'</div>'+
									'</td>'+
									'<td class="avatarMessage" valign="top">'+
										'<div>'+
											'<img alt="" src="'+photoURL+'" style="display:block"/>'+
										'</div>'+
									'</td>'+
									'<td style="width:1px"><span>&nbsp;</span></td>'+
									'<td style="width:1px"><span>&nbsp;</span></td>'+
									'<td class="senderTab" style="width:100%" valign="top">'+
										'<span class="senderFullName" title="'+replaceAllProbCastHtmlTitle(senderLogin)+'">'
											+ substringCustom(senderLogin, 7) +
										' : </span>'+
										'<span class="messageTextSpan">'
											+ textToHTML(messageText)+
										'</span>'+
									'</td>'+
								'</tr>');
				/*'<li><img src="img/avatar-tchat.jpg" alt="">'+
				'<p>'+
					'home obtenez alors un texte aléatoire que vous pourrez ensuite'+
					'utiliser librement dans vos maquettes. <span class="heure">12h:06</span>'+
				'</p></li>'+
			'<li><img src="img/avatar-tchat.jpg" alt="">'+
				'<p>'+
					'obtenez alors un texte aléatoire que vous pourrez ensuite'+
					'utiliser librement dans vos maquettes. <span class="heure">12h:06</span>'+
				'</p></li>'+
			'<li><img src="img/avatar-tchat.jpg" alt="">'+
				'<p>obtenez alors un texte aléatoire que vous pourrez ensuite'+
					'utiliser librement dans vos maquettes.</p></li>'+*/
			} else if(operation==UPDATE_MESSAGE) {
				$('#msg'+messageId+" .messageTextSpan").text(messageText);
			} else if(operation==REMOVE_MESSAGE) {
				$('#msg'+messageId).remove();
			}
			$('#'+contentTabsId).scrollTop($('#'+idMessagesUi).height());
		}
	}
		
	function sendMessage(topicStand, msgText) {
		var senderLogin = fullName;//
		senderLogin=encodeURIComponent(senderLogin);
		var senderId = userId;
		var receiverId = topicStand;
		var messageText = msgText.trim();
		//messageText=htmlEncode(messageText);
		messageText=encodeURIComponent(messageText);
		//alert(messageText);
		//messageText=htmlEncode(messageText);
		//alert(messageText);
		publishMessage(senderId, senderLogin, receiverId, '', messageText, photoURL, '', profileUrl);
	}
		
	function publishMessage(senderId, senderLogin, receiverId, receiverLogin, messageText, photoURL, dateTime, profileUrl){
		jQuery.ajax({
			  url: SERVER_BE_URL+'chat/chatmodule/'+receiverId,  
			  type: "POST",
			  contentType: "application/xml; charset=utf-8",
			  dataType : 'xml',
			  data : '<message><senderId>'+senderId+'</senderId><senderLogin>'+senderLogin+'</senderLogin><receiverId>'+receiverId+'</receiverId><receiverLogin>'+receiverLogin+'</receiverLogin><messageText>'+messageText+'</messageText><photoURL>'+photoURL+'</photoURL><dateTime>'+dateTime+'</dateTime><operation>'+ADD_MESSAGE+'</operation><profileUrl>'+profileUrl+'</profileUrl><languageId>'+getIdLangParam()+'</languageId></message>',
			  success: function(xml){
			  },
			  error: function(){					  				        
			  }
		});	 
	}

	function connectToStand(topicStand, standName, chatTitle, photoURL, languageId, list_stand, tagModule) {
		if (document.getElementById(topicStand+tagModule)==null){
			/*if(tagModule==CSTAG) currentStand_CS=topicStand;
			else if(tagModule==CGTAG) {
				currentStand=topicStand;
				updateCheckboxChat(currentStand);
			}*/
			addStand(topicStand, standName, chatTitle, photoURL, languageId, list_stand, tagModule);
			getChatStandCache(topicStand, tagModule);
			initChatStandAthmosphere(topicStand);
		}else {
			factorySelectStand(topicStand, list_stand, tagModule);
		}
	}
	function factorySelectStand(topicStand, list_stand, tagModule){
		if((currentStand!=topicStand && tagModule==CGTAG) || (currentStand_CS!=topicStand && tagModule==CSTAG)){
			var standTrId=topicStand + tagModule;
			$('#'+list_stand+' tr').removeClass('selectedStand');
			$('#'+list_stand+' #'+standTrId).addClass('selectedStand');
			if(tagModule==CGTAG){
				currentStand=topicStand;
				updateCheckboxsChat(currentStand);
			}else if(tagModule==CSTAG){
				currentStand_CS=topicStand;
			}
			getMessagesByStand(topicStand, tagModule);
		}
	}
	function addConnectStand(listStandSelector, tagTopicStandId, topicStand, standName, chatTitle, photoURL, numberRecruiter, languageId, list_stand, tagModule) {
		//chatTitle="Test chat ctotile sg  s";
		if (document.getElementById(tagTopicStandId+topicStand)==null) {
			/*$(listStandSelector+' tbody').append('<tr id="' +tagTopicStandId + topicStand + '">'+
						'<td style="width:1px;"><span>&nbsp;</span></td>'+
						'<td style="width:16%;">'+
							'<img class="avatarChat" src="'+photoURL+'" alt="" title="'+standName+'" />'+
						'</td>'+
						'<td style="width:1px"><span>&nbsp;</span></td>'+
						'<td style="width:59%">'+
							'<div class="standName">'+
								'<span title="'+standName+'">'+
									substringCustom(standName, 20) + 
								'</span>'+
							'</div>'+
							'<div class="titleChat">'+
								'<span title="'+chatTitle+'">'+
									substringCustom(chatTitle, 22) +
								'</span>'+
							'</div>'+
						'</td>'+
						'<td style="width:7%; vertical-align:middle;">'+
							'<span class="numberRecruiter" title="'+RECRUITERSNUMBERCHATLABEL+'">' + numberRecruiter
							+ '</span>'+
						'</td>'+
						'<td style="width:6%; vertical-align:middle;">'+
							'<img class="langueChatIcon" alt="'+LANGUECHATLABEL+'" title="'+LANGUECHATLABEL+'" src="'+getDrapeauUrl(languageId)+'"/>'+
						'</td>'+
						'<td style="width:8%; vertical-align:middle;">'+
							'<img class="publicChatIcon" alt="'+PUBLICCHATLABEL+'" title="'+PUBLICCHATLABEL+'" src="modules/chat/chatHtml5/images/public_chat.png"/>'+
						'</td>'+
						'<td style="width:1px;"><span>&nbsp;</span></td>'+
					'</tr>');*/
			$(listStandSelector).append('<li>'
											+'<img src="img/rekrute.jpg" alt="">'
											+'<a data-toggle="dropdown" class="dropdown-toggle glyphicon" href="#">Tchatter</a>'
											+'<ul class="dropdown-menu tchat-liste">'
												+'<li><a href="#">Comment Postuler</a></li>'
												+'<li class="active"><a href="#">Test titre</a></li>'
												+'<li><a href="#">Poser des questions </a></li>'
												+'<li><a href="#">Room 08</a></li>'
											+'</ul>'
										+'</li>');
			/*$(listStandSelector).on(
					'click',
					'#'+ tagTopicStandId + topicStand+ ' .publicChatIcon',
					function() {
						connectToStand(topicStand, standName, chatTitle, photoURL, languageId, 'list_stand'+CGTAG, CGTAG);
						if(isFromParentStandFromTopic(topicStand)) connectToStand(topicStand, standName, chatTitle, photoURL, languageId, 'list_stand'+CSTAG, CSTAG);
					});*/
			updateNumberEnterpriseConnect();
		}
	}
		
	function editConnectStand(listStandSelector, tagTopicStandId, topicStand, standName, chatTitle, photoURL, numberRecruiter, languageId, list_stand, tagModule) {
		if (document.getElementById(tagTopicStandId + topicStand) != null) {
			$('#'+tagTopicStandId + topicStand+' .numberRecruiter').html(numberRecruiter);
		}else{
			addConnectStand(listStandSelector, tagTopicStandId, topicStand, standName, chatTitle, photoURL, numberRecruiter, languageId, list_stand, tagModule);
		}
	}
	
	function delConnectStand(tagTopicStandId, topicStand, tagModule) {
		//console.log("delConnectStand : tagTopicStandId"+tagTopicStandId+"*****"+topicStand);
		var standConnectTrId=tagTopicStandId + topicStand;
		if (document.getElementById(standConnectTrId) != null) {
			$('#' + standConnectTrId).remove();
		}
		updateNumberEnterpriseConnect();
		var standTrId=topicStand + tagModule;
		//console.log("standTrId : "+standTrId);
		//console.log("currentStand : "+currentStand);
		//console.log("tagModule : "+tagModule);
		if (document.getElementById(standTrId)!= null) {
			//console.log(".popupAllRecruitersStandDisconnected"+tagModule);
			if ((tagModule==CGTAG && topicStand==currentStand) || (tagModule==CSTAG && topicStand==currentStand_CS)) {
				showPopupAllRecruitersStandDisconnected(topicStand, tagModule);
			}else {
				$('#' + standTrId+ ' div').bind(
						'click',
						function() {
							$('#' + standTrId+ ' div').unbind("click");
							showPopupAllRecruitersStandDisconnected(topicStand, tagModule);
						});
			}
		}
	}
	function showPopupAllRecruitersStandDisconnected(topicStand, tagModule){
		//console.log("showPopupAllRecruitersStandDisconnected : topicStand:"+topicStand+", tagModule:"+tagModule);
		$(".popupAllRecruitersStandDisconnected"+tagModule+", .backgroundPopupChatHtml5"+tagModule).show();
		//console.log(".backgroundPopupChatHtml5"+tagModule);
		$("#confirmAllRecruitersStandDisconnected"+tagModule).bind("click", function(){
			$(".popupAllRecruitersStandDisconnected"+tagModule).unbind("click");
			$(".popupAllRecruitersStandDisconnected"+tagModule+", .backgroundPopupChatHtml5"+tagModule).hide();
			delStand(topicStand, tagModule);
		});
	}
	function updateNumberEnterpriseConnect(){
		$('#connectEnterprises'+CGTAG).html('('+$('#list_stand_connect'+CGTAG+' > li').length+' '+ENTERPRISESCONNECTED+')');
		$('#connectEnterprises'+CSTAG).html('('+$('#list_stand_connect'+CSTAG+' > li').length+' '+ENTERPRISESCONNECTED+')');
	}
	function addStand(topicStand, standName, chatTitle, photoURL, languageId, list_stand, tagModule) {
		if(document.getElementById(list_stand)!=null){
			var standTrId=topicStand + tagModule;
			/*$('#'+list_stand+' tbody').append('<tr id="' + standTrId+'">'+
						'<td style="width:0.5%"><span>&nbsp;</span></td>'+
						'<td style="width:16%;">'+
							'<img class="avatarChatStand" src="'+photoURL+'" alt="" title="'+standName+'" />'+
						'</td>'+
						'<td style="width:0.5%"><span>&nbsp;</span></td>'+
						'<td style="width:52%">'+
							'<div class="standName" title="'+standName+'">'+
								'<span>'+
									substringCustom(standName, 14) + 
								'</span>'+
							'</div>'+
							'<div class="titleChat" title="'+chatTitle+'">'+
								'<span>'+
									substringCustom(chatTitle, 16) +
								'</span>'+
							'</div>'+
						'</td>'+
						'<td style="width:7%;vertical-align: middle;">'+
							'<img class="langueChatIcon" alt="'+LANGUECHATLABEL+'" title="'+LANGUECHATLABEL+'" src="'+getDrapeauUrl(languageId)+'"/>'+
						'</td>'+
						'<td style="width:6%;vertical-align: middle;">'+
							'<img class="closeStand" alt="'+CLOSELABEL+'" title="'+CLOSELABEL+'" src="modules/chat/chatHtml5/images/close.png"/>'+
						'</td>'+
						'<td style="width:1%"><span>&nbsp;</span></td>'+
					'</tr>');*/
			$('#'+list_stand+'.tab-content').append('<div id="home" class="tab-pane fade in active">'+
				'<div class="tchatmsg">'+
				'</div>'+
				'<form id="message" action="">'+
					'<div class="col-md-10 col-sm-10 input-field no-padding no-margin">'+
						'<textarea id="icon_prefix2" class="materialize-textarea validate"'+
							'maxlength=""></textarea>'+
					'</div>'+
					'<div'+
						'class="col-md-2 col-sm-2 input-field no-padding input-field submitform no-margin">'+
						'<button type="submit" class="btn">Envoyer</button>'+
					'</div>'+
				'</form>'+
			'</div>');
	
			$('#'+list_stand+' tr').removeClass('selectedStand');
			$('#'+list_stand+' #'+standTrId).addClass('selectedStand');
			if(tagModule==CSTAG) currentStand_CS=topicStand;
			else if(tagModule==CGTAG) {
				currentStand=topicStand;
			}
			updateCheckboxsChat(currentStand);
			$('#' + standTrId+ ' .closeStand').bind(
					'click', function() {
						delStand(topicStand, CSTAG);
						delStand(topicStand, CGTAG);
					});
			$('#' + standTrId+ ' div').bind(
					'click',
					function() {
						$('#'+standTrId).blink();
						factorySelectStand(topicStand, list_stand, tagModule);
					});
		}
	}
	
	function delStand(topicStand, tagModule) {
		var standTrId=topicStand + tagModule;
		var messages_ui='messages_ui'+tagModule;
		if (document.getElementById(standTrId)!= null) {
			$('#' + standTrId+ ' .closeStand').unbind('click');
			$('#' + standTrId+ ' .div').unbind('click');
			if($('#'+messages_ui)!=null) $('#'+messages_ui).empty();
			$('#' + standTrId).remove();
			if(listStands[topicStand]!=null) delete listStands[topicStand];
			listStands[topicStand]=null;
			if(topicStand==currentStand) currentStand=null;
			if(topicStand==currentStand_CS) currentStand_CS=null;
			updateCheckboxsChat(null);
			closeChatStandNetconnection(topicStand);
		}
	}
	function buildTopic(standId, langId, chatId){
		return standId+ SEPARATEPUBLICCHATSO + langId + SEPARATEPUBLICCHATSO +chatId;
	}

    function addRegisterChat(topic){
        listRegistersChat[topic]="";
    }
 
    function isRegisterChat(topic){
        if(listRegistersChat[topic]!=null) return true;
        return false;
    }
    function deleteRegisterChat(topic){
        delete listRegistersChat[topic];
    }
    function updateRegisterChat(register, topic){
        if(register) addRegisterChat(topic);
        else deleteRegisterChat(topic);
    }
    function updateCheckboxsChat(topic){
    	updateCheckboxChat(topic, CGTAG, currentStand);
    	updateCheckboxChat(topic, CSTAG, currentStand_CS);
    }
    function updateCheckboxChat(topic, tagModule, currentStand){
    	if(document.getElementById("saveChatButton"+tagModule)!=null) {
    		if(currentStand!=null){
	    		if(topic == currentStand){
	    			if(isRegisterChat(topic)) {
		    			$("#saveChatButton"+tagModule).hide();
		        		$("#stopSaveChatButton"+tagModule).show();
		    		}else {
		    			$("#saveChatButton"+tagModule).show();
		        		$("#stopSaveChatButton"+tagModule).hide();    			
		    		}
	    		}
    		}else {
    			$("#saveChatButton"+tagModule).hide();
        		$("#stopSaveChatButton"+tagModule).hide();
			}
    	}
    } 
	function getTimeChat(d){
		var hours = getStringElementDateNumber(d.getHours());
		var minutes = getStringElementDateNumber(d.getMinutes());
		return [hours,minutes].join('h');
	}

	function extractDate(fullDate) {
		if(fullDate!=null) if(fullDate.split(' ')[1]!=null)  {
			var date=parseDateChat(fullDate);
			return getTimeChat(date);
		}
		return "00h00";
	}
	function parseDateChat(fullDate){
		fullDate=fullDate.substring(1, fullDate.length-2);
		var fullDateArray=fullDate.split(' ');
		var date=fullDateArray[0];
		var time=fullDateArray[1];
		dateArray=date.split("/");
		timeArray=time.split("h");
		var yearUTC = dateArray[2];
		var monthUTC = dateArray[1];
		var dayUTC = dateArray[0];
		var hoursUTC = timeArray[0];
		var minutesUTC = timeArray[1];
		var d=new Date();
		d.setUTCFullYear(yearUTC);
		d.getUTCMonth(monthUTC-1);
		d.setUTCDate(dayUTC);
		d.setUTCHours(hoursUTC);
		d.setUTCMinutes(minutesUTC);
		return d;
	}
	
