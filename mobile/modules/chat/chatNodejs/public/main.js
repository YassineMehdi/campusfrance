var $window = $(window);
var blink_flag;
var usermsg = [];
/* var storage = window.localStorage.getItem("usermsg");
if (storage != null) {
    usermsg = storage;
    console.log("ok")
} */

var userSendMsg = {};

///console.log(window.localStorage.getItem("usermsg"))

function listenForClick() {
    $('.list li').on('click', function() {
        clearInterval(blink_flag);
        $('.list li').removeClass('blink');
        $('.chat .room').addClass('hide');
        $('.chat .emptychat').addClass('hide');
        var id = $(this).attr('id')
        $('#' + id + 'room').removeClass('hide');

    })
}


//var socket = io();
function connexion(data) {

    connected = true;
    var userV = {};
    userV.username = candidatGlobal.userProfile.firstName + " " + candidatGlobal.userProfile.secondName;
    userV.email = candidatGlobal.email;
    userV.avatar = candidatGlobal.userProfile.avatar;
    userSendMsg.user = userV;
    socket.emit('privatechatroom', { userV: userV });

}



function sendMessageNodeDrw(id, msg, date) {

    $('#' + id + 'room .chat-history ul').append('<li class="clearfix">' +
        '<div class="message-data align-right">' +
        '<span class="message-data-time">' + date + ' </span> &nbsp; &nbsp;' +
        '<span class="message-data-name">Moi</span> <i class="fa fa-circle me"></i>'

        +
        '</div>' +
        '<div class="message other-message float-right">' + msg + '</div>' +
        '</li>')
    $('#' + id + 'room .messageValue').val('');
}

function sendMessageNode() {
    console.log("sendffdd")
    $('.sendMsg').on('click', function() {
        var idroom = $(this).parent().parent().attr('id');
        console.log(idroom)
        var id = idroom.substring(0, idroom.length - 4);
        if ($('#' + idroom + ' .messageValue').val() != '') {
            var ladate = new Date();
            userSendMsg.msg = $('#' + idroom + ' .messageValue').val();
            userSendMsg.id = id;
            userSendMsg.date = ladate.getHours() + ":" + ladate.getMinutes();
            userSendMsg.receive = false;
            usermsg.push(userSendMsg);

            socket.emit('privatemsgToStand', { id: id, msg: $('#' + idroom + ' .messageValue').val() });

            console.log($('#' + id + ' .chat-history ul'))
            sendMessageNodeDrw(id, userSendMsg.msg, userSendMsg.date)
        }

    })

}












// Keyboard events

$window.keydown(function(event) {
    // Auto-focus the current input when a key is typed
    if (!(event.ctrlKey || event.metaKey || event.altKey)) {
        //$currentInput.focus();
    }
    // When the client hits ENTER on their keyboard
    if (event.which === 13) {
        if (username) {
            sendMessageNode();
            socket.emit('stop typing');
            typing = false;
        } else {
            setUsername();
        }
    }
});





//user Stand joined
socket.on('userStand', function(data) {
    addStandConnexion(data.userE)
    listenForClick();
    sendMessageNode()
    console.log(data)
})
socket.on('usersStandList', function(data) {
    console.log(data.users)
    $('.list').empty();

    setTimeout(function() {
        for (i = 0; i < data.users.length; i++) {
            console.log(data.users[i])
            addStandConnexion(data.users[i]);
            listenForClick();
            sendMessageNode()
        }

    }, 1000);
})

function addStandConnexion(user) {
    //if($('.list li').attr('id'))

    $('.list').append('<li id="' + user.id + '" class="clearfix">' +
        '<img src="' + user.avatar + '" alt="avatar" />' +
        '<div class="about">' +
        '<div class="name">' + user.username + '</div>' +
        '<div class="status">' +
        '<i class="fa fa-circle online"></i> online' +
        '</div>' +
        '</div>' +
        '</li>');
    $('.chat').append('<div id="' + user.id + 'room" class="room hide">' +
        '<div class="chat-header clearfix">' +
        '<img src="' + user.avatar + '" alt="avatar" />'

        +
        '<div class="chat-about">' +
        '<div class="chat-with">Chat avec ' + user.username + '</div>' +
        '<div class="chat-num-messages"></div>' +
        '</div>' +

        '</div>'

        +
        '<div class="chat-history">' +
        '<ul>' +
        '</ul>'

        +
        '</div>' +
        '<div class="chat-message clearfix">' +
        '<textarea name="message-to-send" class="messageValue" placeholder="Ecrire votre message" rows="3"></textarea>'

        +

        '<button class="sendMsg">Envoyer</button>'

        +
        '</div>' +
        '</div>')
    listenForClick();
    sendMessageNode();
}

function ReceiveMsgDraw(user) {
    $('#' + user.user.id + 'room .chat-history ul').append(' <li>' +
        '<div class="message-data">' +
        '<span class="message-data-name"><i class="fa fa-circle online"></i> ' + user.user.username + '</span>' +
        '<span class="message-data-time">' + user.date + '</span>' +
        '</div>' +
        '<div class="message my-message">' +
        user.msg +
        '</div>' +
        '</li>')
}
socket.on('new_msgV', function(data) {
        console.log(data)
        data.user.receive = true;
        usermsg.push(data.user);
        blink_flag = setInterval(function() {
            $('#' + data.user.user.id).toggleClass("blink");
        }, 1000);
        ReceiveMsgDraw(data.user)


    })
    // Whenever the server emits 'stand left', 
socket.on('standDisconnect', function(data) {
    console.log(data)
    $('#' + data.userId).remove();
    $('#' + data.userId + 'room').remove();

})

function drawMessageRoom() {
    //window.localStorage.setItem('usermsg', usermsg);
    console.log(usermsg);
    console.log(usermsg.length)
    for (i in usermsg) {
        console.log(i)
        console.log(usermsg[i]);
        if (usermsg[i].receive) {
            console.log(usermsg[i]);
            ReceiveMsgDraw(usermsg[i])
        } else {
            console.log(usermsg[i]);
            sendMessageNodeDrw(usermsg[i].id, usermsg[i].msg, usermsg[i].date)
        }

    }
}

socket.on('disconnect', function() {
    //log('you have been disconnected');
});

socket.on('reconnect', function() {
    //log('you have been reconnected');
    if (username) {
        socket.emit('add user', username);
    }
});

socket.on('reconnect_error', function() {
    //log('attempt to reconnect has failed');
});