/*jslint vars: true, plusplus: true, devel: true, nomen: true, indent: 4, maxerr: 50, browser: true */
/*global postMessage, addEventListener */

(function () {
    "use strict";
    
    addEventListener('message', function (event) {
        // doesn't matter what the message is, just toggle the worker
    	postMessage(event.data);
    });
    
    
}());