jQuery.extend({
  SalleExpositionModel: function () {
    var that = this;
    var listeners = new Array();
    this.initModel = function () {
    };
    this.standlisteError = function (xhr, status, error) {
      isSessionExpired(xhr.responseText);
    };

    this.standliste = function () {
      var listJobOffersCache = [];
      genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'stand?idLang=' + getIdLangParam(), 'application/xml', 'xml', '', function (xml) {
        listStandCache = new Array();
        $(xml).find("stand").each(function () {
          var stand = new $.Stand($(this));
          var standId = stand.getIdStande();
          if(!isInculdesStr(["76", "77", "78", "80", "83", "85", "86", "26", "3", "73", "65", "71", "69"], standId)){
            listStandCache[standId] = stand;
          }
        });
        that.notifyLoadStandliste(listStandCache);
      }, that.standlisteError);
    };
    this.notifyLoadStandliste = function (listStand) {
      $.each(listeners, function (i) {
        listeners[i].loadStandliste(listStand);
      });
    };
    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  SalleExpositionModelListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }
});