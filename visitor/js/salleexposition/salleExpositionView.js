jQuery.extend({
  SalleExpositionView: function () {
    var that = this;
    var listeners = new Array();
    this.playerSelector = '';
    this.initView = function () {
      that.playerSelector = '#content_video_transition_info_stand';
      that.Player();
      $('.icon-format2,.toHallButton').click(function () {
        window.history.pushState("test", "Title", "/visitor/new-home.html?idLang="+getIdLangParam());
        $('#contentG').attr('class', '');
        $('#contentG').addClass('principal-halls');
        $('#modal-forgot-password').modal('hide');
        $('#modal-forgot-password-Success').modal('hide');
        $('#modal-sign-in').modal('hide');
        $('#page-content').removeClass('no-padding');
        $('#home-page').addClass('hide');
        $('.genericContent').attr('style', 'display: none;');
        $('#videoHome').hide();
        $('#principal-halls .img-salle').show();
        $('.vue-aerienne').show();
        $('#principal-halls').find('.img-salle').attr('src','img/halls/hall_asmex_'+getIdLangParam()+'.jpg');
        $('#principal-halls').removeClass('hide');
        $('#principal-halls').attr('style', 'display: block;');

      });
      /*$('.icon-format2').click(function () {
        $('#contentG').attr('class', '');
        $('#contentG').addClass('salle-exposition');
        $('#modal-forgot-password').modal('hide');
        $('#modal-forgot-password-Success').modal('hide');
        $('#modal-sign-in').modal('hide');
        $('#page-content').removeClass('no-padding');
        $('#home-page').addClass('hide');
        $('.genericContent').attr('style', 'display: none;');
        $('#videoHome').hide();
        $('#salle-exposition').removeClass('hide');
        $('#salle-exposition').attr('style', 'display: block;');
      });
      $('.standBlock11 a').click(function () {
        var standId = $(this).attr('standId');
        $('#contentG').attr('class', '');
        $('#contentG').attr('class', 'info-stand stand' + standId);
        $(".main-menu .sub-main-menu, .second-menu li a").removeClass("active");
        if (standId == 7) {
          $('a.icon-format3').addClass('active');
        }
        $('#modal-forgot-password').modal('hide');
        $('#modal-forgot-password-Success').modal('hide');
        $('#modal-sign-in').modal('hide');
        $('#page-content').removeClass('no-padding');
        $('#vue-aerienne-info-stand').removeClass('hide');
        $('.genericContent').attr('style', 'display: none;');
        $('#videoHome').hide();
        $('#info-stand').removeClass('hide');
        $('#info-stand').attr('style', 'display: block;');
        $('.menu-stand a,.vue-aerienne-info-stand a').attr('standId', 'standId=' + standId);
        initStandInfoController(standId);
      });*/
      that.notifyStandListe();
    };
    this.Player = function () {
      $('.infoStandButton').click(function () {
        $('.genericContent').hide();
        flowAutoPlay('media/video/2016/exterieur_batiment_to_info_stand', 'img/loading.jpg', function () {
          $(".main-menu .sub-main-menu, .second-menu li a").removeClass("active");
          $('a.icon-format3').addClass('active');
          $('#modal-forgot-password').modal('hide');
          $('#modal-forgot-password-Success').modal('hide');
          $('#modal-sign-in').modal('hide');
          $('#videoHome').hide();
          $('#vue-aerienne').hide();
        }, that.playerSelector);
        $('.menu-stand a,.vue-aerienne-info-stand a').attr('standId', 'standId=7');
        initStandInfoController("7");
      });

      $('.infostandFromInterior').click(function () {
        $.cookie("currentHall", null);
        $('.genericContent').hide();
        flowAutoPlay('media/video/2016/info_stand', 'img/salle-exposition.jpg', function () {
          $(".main-menu .sub-main-menu, .second-menu li a").removeClass("active");
          $('a.icon-format3').addClass('active');
          $('#modal-forgot-password').modal('hide');
          $('#modal-forgot-password-Success').modal('hide');
          $('#modal-sign-in').modal('hide');
          $('#videoHome').hide();
          $('#vue-aerienne').hide();
        }, "#content_video_transition_exite_to_info");
        $('.menu-stand a,.vue-aerienne-info-stand a').attr('standId', 'standId=7');
        initStandInfoController("7");
      });
      $('#digifact-room-batiment').click(function () {
        $('.genericContent').hide();
        flowAutoPlay('media/video/2016/interieur_digievent_to_digi_fact', 'img/salle-exposition.jpg', function () {
          $(".main-menu .sub-main-menu, .second-menu li a").removeClass("active");
          $('a.icon-format3').addClass('active');
          $('#modal-forgot-password').modal('hide');
          $('#modal-forgot-password-Success').modal('hide');
          $('#modal-sign-in').modal('hide');
          $('#videoHome').hide();
          $('#vue-aerienne').hide();
        }, "#content_video_transition_to_digifact_batiment");
        $('.menu-stand a,.vue-aerienne-info-stand a').attr('standId', 'standId=7');        
      });
      $('#digievent-room-batiment').click(function () {
        $('.genericContent').hide();
        flowAutoPlay('media/video/2016/interieur_digievent_to_digi_event', 'img/salle-exposition.jpg', function () {
          $(".main-menu .sub-main-menu, .second-menu li a").removeClass("active");
          $('a.icon-format3').addClass('active');
          $('#modal-forgot-password').modal('hide');
          $('#modal-forgot-password-Success').modal('hide');
          $('#modal-sign-in').modal('hide');
          $('#videoHome').hide();
          $('#vue-aerienne').hide();
        }, "#content_video_transition_to_digievent_batiment");
        $('.menu-stand a,.vue-aerienne-info-stand a').attr('standId', 'standId=7');        
      });
    };
    this.displayLoadStand = function (listStands) {
      for (index in listStands) {
        var stand = listStands[index];
        var standId = stand.getIdStande();
        var standlogo = stand.getEnterprise().getLogo();
        var standname = stand.getName();
        var socialWallMessage = stand.getEnterprise().getSlogan();
        $('.standBlock' + standId + ' a').addClass('stands');
        $('.standBlock' + standId + ' a').attr('standId', standId);
        $('.standBlock' + standId + ' .info .standBlockLogo img').attr("alt", standname).attr("title", standname).attr("src", standlogo);
        $('.standBlock' + standId + ' .info .standBlockName').html(htmlEncode(standname));
        $('.standBlock' + standId + ' .info .standBlockDescription').html(htmlEncode(socialWallMessage));
        //console.log('img/stand.jpg');
        //console.log('img/stand-'+standId+'.jpg');
        //$(['img/stand-'+standId+'.jpg']).preloadImg();
        $('.standBlock' + standId + ' a').click(function () {
          var standId = $(this).attr('standId');
          $(['img/stand-'+standId+'.jpg']).preloadImg();
          $('#contentG').attr('class', '');
          $('#contentG').attr('class', 'info-stand stand' + standId);
          $(".main-menu .sub-main-menu, .second-menu li a").removeClass("active");
          if (standId == 7) {
            $('a.icon-format3').addClass('active');
          }
          $('#modal-forgot-password').modal('hide');
          $('#modal-forgot-password-Success').modal('hide');
          $('#modal-sign-in').modal('hide');
          $('#page-content').removeClass('no-padding');
          $('#vue-aerienne-info-stand').removeClass('hide');
          $('.genericContent').attr('style', 'display: none;');
          $('#videoHome').hide();
          $('#info-stand').removeClass('hide');
          $('#info-stand').attr('style', 'display: block;');
          $('.menu-stand a,.vue-aerienne-info-stand a').attr('standId', 'standId=' + standId);
          //$('#back-to-stand').closest('div').show();
          initStandInfoController(standId);
        });
      }
      $('.standBlock_empty a').unbind("click").bind("click", function () {
        window.open("contact.html?idLang=" + getIdLangParam(), '_blank');
      });
    };
    this.notifyStandListe = function () {
      $.each(listeners, function (i) {
        listeners[i].standliste();
      });
    };
    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  SalleExpositionViewListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }
});