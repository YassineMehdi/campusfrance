jQuery.extend({
  VideoStandModel: function () {
    var listeners = new Array();
    var that = this;

    this.initModel = function () {
    };

    //------------- get All Videos -----------//
    this.getVideoStandSuccess = function (xml) {
      var videos = [];
      videos = new Array();
      $(xml).find("allfiles").each(function () {
        videos.push(new $.AllFiles($(this)));
      });
      that.notifyLoadVideoStand(videos);
    };
    this.getVideoStandError = function (xhr, status, error) {
      isSessionExpired(xhr.responseText);
    };

    this.getVideoStand = function (standId) {
      genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'allfiles/standVideos?' + standId + '&idLang=' + getIdLangParam(), 'application/xml', 
      		'xml', '', that.getVideoStandSuccess, that.getVideoStandError);
    };

    this.notifyLoadVideoStand = function (videos) {
      $.each(listeners, function (i) {
        listeners[i].loadVideoStand(videos);
      });
    };

    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  ModelListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }

});