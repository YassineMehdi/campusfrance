jQuery.extend({
  VideoStandController: function (model, view) {
    var that = this;
    /**
     * listen to the view
     */

    var vlist = $.ViewListener({
      launchVideoStand: function (standId) {
        model.getVideoStand(standId);
      },
    });

    view.addListener(vlist);

    /**
     * listen to the model
     */
    var mlist = $.ModelListener({
      loadVideoStand: function (videos) {
        view.displayVideoStand(videos);
      },
    });

    model.addListener(mlist);

    this.initController = function (standId) {
      view.initView(standId);
      model.initModel();
    };
  },
});