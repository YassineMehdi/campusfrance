jQuery.extend({
    DocumentsManage: function(document, documentsContainer, view) {
        var isFavorite = factoryFavoriteExist(document.getAllFilesId(), FavoriteEnum.MEDIA);
        var displayDeleteFavorite = "block";
        var displayAddFavorite = "block";
        if (isFavorite)
            displayAddFavorite = "none";
        else
            displayDeleteFavorite = "none";

        var dom = $('<li>' +
            '<div class="infos">' +
            '<ul>' +
            '<li>' +
            '<div class="col-md-10 col-sm-10 no-padding">' +
            '<a class="document-name">' +
            htmlEncode(substringCustom(formattingLongWords(document.getTitle(), 20), 60)) +
            '</a>' +
            '</div>'
            /* + '<div class="  col-md-2 col-sm-2 no-padding-left">'
            + '<i class="icon-document"></i>'
            + '</div>' */
            +
            '</li>' +
            '<li><a class="btn green col-md-12 addDocumentToFavorite" style="margin-bottom: -3px;display:' + displayAddFavorite + '" ><i aria-hidden="true" class="glyphicon glyphicon-star"></i>' + ADDTOFAVORITELABEL + '</a><a class="btn btn-primary col-md-12 deleteDocumentFromFavorite" style="display:' + displayDeleteFavorite + '"><i aria-hidden="true" class="glyphicon glyphicon-star"></i>' + DELETEFROMFAVORITELABEL + '</a></li>' +
            '<li><a class="btn btn-primary col-md-12 blue openCurrentDocument">' + VIEWLABEL + '</a></li>' +
            '</ul>' +
            '</div>' +
            '</li>');

        this.refresh = function() {

            dom.find(".openCurrentDocument").unbind();
            dom.find(".openCurrentDocument").bind('click', function() {
                $(".agrandir-document-stand").bind('click', function() {
                    $('#modal-document-lecteur-big').modal();
                    openDocument(document, $("#modal-document-lecteur-big").find('.ad-image-wrapper'), $("#modal-document-lecteur-big").find('.modal-title'));
                });
                view.openDocument(document);
            });

            var addDocumentFavoriteLink = dom.find(".addDocumentToFavorite");
            var deleteDocumentFavoriteLink = dom.find(".deleteDocumentFromFavorite");

            addDocumentFavoriteLink.click(function() {
                if (connectedUser) {
                    var idEntity = document.getAllFilesId();
                    var componentName = FavoriteEnum.MEDIA;
                    var favoriteXml = factoryXMLFavory(idEntity, componentName);
                    addFavorite(favoriteXml);
                    $(this).hide();
                    $(this).closest('div').find('.deleteDocumentFromFavorite').show();
                } else {
                    getStatusNoConnected();
                }
            });

            deleteDocumentFavoriteLink.click(function() {
                var idEntity = document.getAllFilesId();
                var componentName = FavoriteEnum.MEDIA;
                var favoriteXml = factoryXMLFavory(idEntity, componentName);
                deleteFavorite(favoriteXml);
                $(this).hide();
                $(this).closest('div').find('.addDocumentToFavorite').show();
            });
        };
        this.getDOM = function() { //retourne une ligne HTML <a .....
            return dom;
        };
        this.refresh();
    },
    StandDocumentView: function() {
        var listeners = new Array();
        var that = this;
        var galleriesDocuments = null;
        var infoStandDocumentHandler = null;
        var galleryDocumentHandler = null;
        var galleryTitleHandler = null;
        var documentsContentHandler = null;

        $('#modal-document').on('hidden.bs.modal', function() {
            $(this).find('.info-stand-document').empty();
        });
        this.initView = function() {
            infoStandDocumentHandler = $('#modal-document .info-stand-document');
            $(".menu-stand .documentType").unbind('click').bind('click', function() {
                // $('#modal-document').modal('hide');
                id = $(this).attr('fileType');
                if (id == '8') {
                    console.log('doc8888')


                    $('#profil-entreprise').parent().css('display', 'none');
                    $('#sondage-link').parent().css('display', 'none');
                    $('.retourDoc').parent().css('display', 'inline');
                    /*  $('#doc1').css('display', 'none')
                     $('#doc2').css('display', 'none')
                     $('#doc3').css('display', 'none')
                     $('#doc4').css('display', 'none')
                     $('#doc5').css('display', 'none')
                     $('#doc6').css('display', 'none')
                     $('#doc15').css('display', 'none')
                     $('#doc18').css('display', 'none')

                     $('#doc7').css('display', 'inline')
                     $('#doc8').css('display', 'inline')
                     $('#doc9').css('display', 'inline')
                     $('#doc10').css('display', 'inline')
                     $('#doc11').css('display', 'inline')
                     $('#doc12').css('display', 'inline')
                     $('#doc13').css('display', 'inline')
                     $('#doc14').css('display', 'inline')
                     $('#doc16').css('display', 'inline')
                     $('#doc17').css('display', 'inline') */
                    $('.documentType').each(function() {

                        id = $(this).attr('fileType');

                        console.log(id)
                            //if (id == '6' || id == '7' || id == '9' || id == '23' || id == '20' || id == '10' || id == '11' || id == '8') {
                        if (isInculdesStr(["6", "7", "9", "20", "10", "23", "11", "8", "24"], id)) {
                            console.log(id)
                            $('#doc' + id).css('display', 'none')
                        } else if (isInculdesStr(["10", "11", "12", "13", "14", "15", "16", "18", "17", "19", "21", "22", "25", "26", "42", "43"], id)) {
                            $('#doc' + id).css('display', 'inline')
                        }
                        /* else {
                                                   console.log("else")
                                                   console.log(id)
                                                   $('#' + id).parent().css('display', 'inline')
                                               } */
                    })
                } else if (id == '23') {
                    $('#profil-entreprise').parent().css('display', 'none');
                    $('#sondage-link').parent().css('display', 'none');
                    $('.retourDoc').parent().css('display', 'inline');
                    $('.documentType').each(function() {

                        id = $(this).attr('fileType');

                        console.log(id)
                            //if (id == '6' || id == '7' || id == '9' || id == '23' || id == '20' || id == '10' || id == '11' || id == '8') {
                        if (isInculdesStr(["6", "7", "9", "20", "10", "11", "8", "24", "23", "42", "43"], id)) {
                            console.log(id)
                            $('#doc' + id).css('display', 'none')
                        } else if (isInculdesStr(["27", "28", "29", "30", "31", "32", "33", "34"], id)) {
                            $('#doc' + id).css('display', 'inline')
                        }
                        /* else {
                                                   console.log("else")
                                                   console.log(id)
                                                   $('#' + id).parent().css('display', 'inline')
                                               } */
                    })
                } else {
                    if (!($("#modal-document").data('bs.modal') || {}).isShown) {
                        factoryActivateFaIcon(this);
                        $('.modal').modal('hide');
                        $('.modal-loading').show();
                        console.log(this)
                        that.notifyGetDocumentsByStand($('.documentType').attr('standId'), $(this).attr('fileType'));
                        $('#modal-document').modal('show');
                    }
                }
            });

            /* $('#8').unbind('click').bind('click', function() {
                $('#profil-entreprise').parent().css('display', 'none');
                $('#sondage-link').parent().css('display', 'none');
                $('.retourDoc').css('display', 'inline');
                id = $(this).attr('fileType');
                if (id == '8') {
                    $('.documentType').each(function() {
                        id = $('this').attr('fileType');
                        if (id in (1, 2, 4, 23, 20, 10, 11, 8)) {
                            $('#' + id).css('display', 'none')
                        } else {
                            $('#' + id).css('display', 'inline')
                        }
                    })
                }
            }) */
        };
        this.loadStandDocuments = function(documents) {
            galleriesDocuments = documents;
            if (documents != null && Object.keys(documents).length > 0) {
                infoStandDocumentHandler.html('<div class="col-md-9 col-sm-8 no-padding">' +
                    '<h3 class="documentTitle"></h3>' +
                    '<div id="galleryDocument">' +
                    '<div class="ad-image-wrapper">' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="listing scroll-pane col-md-3 col-sm-3">' +
                    '<ul class="ul-docs">' +
                    '</ul>' +
                    '</div>');
                galleryDocumentHandler = infoStandDocumentHandler.find('.ad-image-wrapper');
                galleryTitleHandler = $('.document-title-head');
                documentsContentHandler = infoStandDocumentHandler.find('.ul-docs');
                var documentManage = null;
                for (var index in documents) {
                    documentManage = new $.DocumentsManage(documents[index], documentsContentHandler, that);
                    documentsContentHandler.append(documentManage.getDOM());
                }
                that.openDocumentByIndex(0);
                infoStandDocumentHandler.find('.scroll-pane').jScrollPane({ autoReinitialise: true });
            } else {
                infoStandDocumentHandler.html('<p class="empty">' + NODOCUMENTSLABEL + '</p>');
            }
            $('.modal-loading').hide();
        };

        this.openDocumentByIndex = function(index) {
            if (galleriesDocuments != null) {
                $(".agrandir-document-stand").bind('click', function() {
                    $('#modal-document-lecteur-big').modal();
                    openDocument(galleriesDocuments[index], $("#modal-document-lecteur-big").find('.ad-image-wrapper'), $("#modal-document-lecteur-big").find('.modal-title'));
                });
                that.openDocument(galleriesDocuments[index]);
            }
        };
        this.openDocument = function(document) {
            if (document != null) {
                openDocument(document, galleryDocumentHandler, galleryTitleHandler);
            }
        };

        this.notifyGetDocumentsByStand = function(standId, fileTypeId) {
            console.log(fileTypeId)
            $.each(listeners, function(i) {
                listeners[i].getDocumentsByStand(standId, fileTypeId);
            });
        };

        this.addListener = function(list) {
            listeners.push(list);
        };
    },
    ViewListener: function(list) {
        if (!list)
            list = {};
        return $.extend(list);
    }
});