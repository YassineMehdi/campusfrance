jQuery.extend({
	StandTChatController: function (model, view) {
    //var that = this;

    /**
     * listen to the view
     */

    var vlist = $.StandTChatViewListener({
    	getInfoForStandTChat : function (standFullId) {
        model.getInfoForStandTChat(standFullId);
      },
      getCacheStand : function(){
      	model.getCacheStand();
      },
    });

    view.addListener(vlist);

    /**
     * listen to the model
     */
    var mlist = $.StandTChatModelListener({
      startChat : function (standId, currentLangId, topic, tChatEvent) {
        view.startChat(standId, currentLangId, topic, tChatEvent);
      },
      standNotConnected : function(tChatEvent){
      	view.standNotConnected(tChatEvent);
      },
    });

    model.addListener(mlist);

    this.initController = function () {
      view.initView();
      model.initModel();
    };
  },
});