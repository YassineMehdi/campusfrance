/*
 * this file contains all objects used in the frontend candidate
 */

jQuery.extend({
  Candidate: function (data) {
    var that = this;
    that.candidateId = '';
    that.password = '';
    that.passwordClear = '';
    that.token = '';
    that.phoneNumber = '';
    that.currentCompany = '';
    that.jobTitle = '';
    that.email = '';
    that.se = '';
    that.userProfile = '';
    that.cvContent = '';
    that.studyLevel = '';
    that.functionCandidate = '';
    that.experienceYears = '';
    that.secteurActs = '';
    that.regions = '';
    that.pdfCvs = '';
    that.cellPhone = '';
    that.gender = '';
    that.birthDate = '';
    that.countryOrigin = '';
    that.areaExpertise = '';
    that.jobSought = '';
    that.formation = '';
    that.candidateLanguages = '';
    that.city = '';
		that.description = '';
		that.place = '';
		that.country = '';
		that.type = 'Candidate';
    that.connected = false;
    
    this.candidate = function (data) {
      if (data) {
        if ($(data).find("candidateId"))
          that.candidateId = $(data).find("candidateId").text();
        if ($(data).find("password"))
          that.password = $(data).find("password").text();
        if ($(data).find("passwordClear"))
          that.passwordClear = $(data).find("passwordClear").text();
        if ($(data).find("token"))
          that.token = $(data).find("token").text();
        if ($(data).find("phoneNumber"))
          that.phoneNumber = $(data).find("phoneNumber").text();
        if ($(data).find("currentCompany"))
          that.currentCompany = $(data).find("currentCompany").text();
        if ($(data).find("jobTitle"))
          that.jobTitle = $(data).find("jobTitle").text();
        if ($(data).find("email"))
          that.email = $(data).find("email").text();
        if ($(data).find("cellPhone"))
          that.cellPhone = $(data).find("cellPhone").text();
        if ($(data).find("gender"))
          that.gender = $(data).find("gender").text();
        if ($(data).find("birthDate")){
          	try {
	            that.birthDate = new Date($(data).find("birthDate").text()).toJSON();
						}
						catch(err) {
						    that.birthDate = '';
						}
        }
				if ($(data).find("description"))
					that.description = $(data).find("description").text();
        if ($(data).find("se"))
          that.se = $(data).find("se").text();
        if ($(data).find("userProfileDTO"))
          that.userProfile = new $.UserProfile($(data).find("userProfileDTO"));
        if ($(data).find("cvContentDTO"))
          that.cvContent = new $.CvContent($(data).find("cvContentDTO"));
        if ($(data).find("studyLevelDTO"))
          that.studyLevel = new $.StudyLevel($(data).find("studyLevelDTO"));
        if ($(data).find("functionCandidateDTO"))
          that.functionCandidate = new $.FunctionCandidate($(data).find("functionCandidateDTO"));
        if ($(data).find("experienceYearsDTO"))
          that.experienceYears = new $.ExperienceYears($(data).find("experienceYearsDTO"));
        that.secteurActs = new Array();
        $(data).find("secteurActDTO").each(function () {
          var secteurAct = new $.SecteurAct($(this));
          that.secteurActs.push(secteurAct);
        });
        that.candidateLanguages = new Array();
        $(data).find("candidateLanguages").each(function () {
          that.candidateLanguages.push(new $.CandidateLanguages($(this)));
        });
        that.regions = new Array();
        $(data).find("regionDTO").each(function () {
          var region = new $.Region($(this));
          that.regions.push(region);
        });
        that.pdfCvs = new Array();
        $(data).find("pdfCvDTO").each(function () {
          var pdfCv = new $.PdfCv($(this));
          that.pdfCvs.push(pdfCv);
        });
        if ($(data).find("countryOriginDTO"))
          that.countryOrigin = new $.Region($(data).find("countryOriginDTO"));
        if ($(data).find("areaExpertiseDTO"))
          that.areaExpertise = new $.AreaExpertise($(data).find("areaExpertiseDTO"));
        if ($(data).find("jobSoughtDTO"))
          that.jobSought = new $.JobSought($(data).find("jobSoughtDTO"));
        if ($(data).find("cityDTO"))
          that.city = new $.City($(data).find("cityDTO"));
				if ($(data).find("place"))
					that.place = $(data).find("place").text();
				if ($(data).find("countryDTO"))
					that.country = new $.Country($(data).find("countryDTO"));
      	}
    };
    this.getCandidateId = function () {
      return that.candidateId;
    };
    this.getPassword = function () {
      return that.password;
    };
    this.getPasswordClear = function () {
      return that.passwordClear;
    };
    this.getToken = function () {
      return that.token;
    };
    this.getPhoneNumber = function () {
      return that.phoneNumber;
    };
    this.getCurrentCompany = function () {
      return that.currentCompany;
    };
    this.getJobTitle = function () {
      return that.jobTitle;
    };
    this.getEmail = function () {
      return that.email;
    };
    this.getCellPhone = function () {
      return that.cellPhone;
    };
    this.getGender = function () {
      return that.gender;
    };
    this.getBirthDate = function () {
      return that.birthDate;
    };
    this.getAreaExpertise = function () {
      return that.areaExpertise;
    };
    this.getJobSought = function () {
      return that.jobSought;
    };
    this.getSe = function () {
      return that.se;
    };
    this.getUserProfile = function () {
      return that.userProfile;
    };
    this.getCvContent = function () {
      return that.cvContent;
    };
    this.getStudyLevel = function () {
      return that.studyLevel;
    };
    this.getFunctionCandidate = function () {
      return that.functionCandidate;
    };
    this.getExperienceYears = function () {
      return that.experienceYears;
    };
    this.getSecteurActs = function () {
      return that.secteurActs;
    };
    this.getRegions = function () {
      return that.regions;
    };
    this.getCountryOrigin = function () {
      return that.countryOrigin;
    };
    this.getFormation = function () {
      return that.formation;
    };
    this.getPdfCvs = function () {
      return that.pdfCvs;
    };
    this.getCandidateLanguages = function () {
      return that.candidateLanguages;
    };
    this.getCity = function() {
      return that.city;
    };
		this.getDescription = function() {
			return that.description;
		};
		this.getPlace = function() {
			return that.place;
		};
		this.getCountry= function() {
			return that.country;
		};
		this.isConnected= function() {
			return that.connected;
		};
    this.setCandidateId = function (candidateId) {
      that.candidateId = candidateId;
    };
    this.setPassword = function (password) {
      that.password = password;
    };
    this.setPasswordClear = function (passwordClear) {
      that.passwordClear = passwordClear;
    };
    this.setToken = function (token) {
      that.token = token;
    };
    this.setPhoneNumber = function (phoneNumber) {
      that.phoneNumber = phoneNumber;
    };
    this.setCurrentCompany = function (currentCompany) {
      that.currentCompany = currentCompany;
    };
    this.setJobTitle = function (jobTitle) {
      that.jobTitle = jobTitle;
    };
    this.setEmail = function (email) {
      that.email = email;
    };
    this.setCellPhone = function (cellPhone) {
      that.cellPhone = cellPhone;
    };
    this.setGender = function (gender) {
      that.gender = gender;
    };
    this.setBirthDate = function (birthDate) {
      that.birthDate = birthDate;
    };
    this.setAreaExpertise = function (areaExpertise) {
      that.areaExpertise = areaExpertise;
    };
    this.setJobSought = function (jobSought) {
      that.jobSought = jobSought;
    };
    this.setFormation = function (formation) {
      that.formation = formation;
    };
    this.setSe = function (se) {
      that.se = se;
    };
    this.setUserProfile = function (userProfile) {
      that.userProfile = userProfile;
    };
    this.setCvContent = function (cvContent) {
      that.cvContent = cvContent;
    };
    this.setStudyLevel = function (studyLevel) {
      that.studyLevel = studyLevel;
    };
    this.setFunctionCandidate = function (functionCandidate) {
      that.functionCandidate = functionCandidate;
    };
    this.setExperienceYears = function (experienceYears) {
      that.experienceYears = experienceYears;
    };
    this.setSecteurActs = function (secteurActs) {
      that.secteurActs = secteurActs;
    };
    this.setRegions = function (regions) {
      that.regions = regions;
    };
    this.setCountryOrigin = function (countryOrigin) {
      that.countryOrigin = countryOrigin;
    };
    this.setPdfCvs = function (pdfCvs) {
      that.pdfCvs = pdfCvs;
    };
    this.setCandidateLanguages = function (candidateLanguages) {
      that.candidateLanguages = candidateLanguages;
    };
    this.setCity = function(city) {
      that.city = city;
    };
		this.setDescription = function(description) {
			that.description = description;
		};
		this.setPlace = function(place) {
			that.place = place;
		};
		this.setCountry = function(country) {
			that.country = country;
		};
		this.setConnected = function(connected) {
			that.connected = connected;
		};
		this.getNameSectors = function() {
			var sectorsNames = new Array();
			var $sectors = $(data).find(
					"secteurActDTO");
			$sectors.each(function() {
				sectorsNames.push($(this).find('name').text());
			});
			return sectorsNames.join('- ');
		};
    this.update = function (newData) {
      that.candidate(newData);
    };
    this.xmlData = function () {
      var xml = '<candidateDTO>';
      xml += '<candidateId>' + that.candidateId + '</candidateId>';
      xml += '<password>' + htmlEncode(that.password) + '</password>';
      xml += '<passwordClear>' + htmlEncode(that.passwordClear) + '</passwordClear>';
      xml += '<token>' + htmlEncode(that.token) + '</token>';
      xml += '<phoneNumber>' + htmlEncode(that.phoneNumber) + '</phoneNumber>';
      xml += '<cellPhone>' + htmlEncode(that.cellPhone) + '</cellPhone>';
      xml += '<gender>' + htmlEncode(that.gender) + '</gender>';
      xml += '<currentCompany>' + htmlEncode(that.currentCompany) + '</currentCompany>';
			xml += '<jobTitle>' + htmlEncode(that.jobTitle)+ '</jobTitle>';
			xml += '<description>' + htmlEncode(that.description)+ '</description>';
      if (that.birthDate != undefined && that.birthDate != '')
        xml += '<birthDate>' + that.birthDate.toJSON() + '</birthDate>';
      else
        xml += '<birthDate></birthDate>';
      xml += '<email>' + htmlEncode(that.email) + '</email>';
      xml += '<se>' + that.se + '</se>';
      if (that.userProfile != "")
        xml += that.userProfile.xmlData();
      if (that.cvContent != "")
        xml += that.cvContent.xmlData();
      if (that.studyLevel != "")
        xml += that.studyLevel.xmlData();
      if (that.functionCandidate != "")
        xml += that.functionCandidate.xmlData();
      if (that.experienceYears != "")
        xml += that.experienceYears.xmlData();
      xml += '<secteurActDTOlist>';
      for (index in that.secteurActs) {
        var secteurAct = that.secteurActs[index];
        xml += secteurAct.xmlData();
      }
      xml += '</secteurActDTOlist>';
      xml += '<regionDTOlist>';
      for (index in that.regions) {
        var region = that.regions[index];
        xml += '<regionDTO>';
        xml += region.xmlData();
        xml += '</regionDTO>';
      }
      xml += '</regionDTOlist>';
      if (that.countryOrigin != "") {
        xml += '<countryOriginDTO>';
        xml += that.countryOrigin.xmlData();
        xml += '</countryOriginDTO>';
      }
      xml += '<pdfCvDTOList>';
      for (index in that.pdfCvs) {
        var pdfCv = that.pdfCvs[index];
        xml += pdfCv.xmlData();
      }
      xml += '</pdfCvDTOList>';
      xml += '<candidateLanguagesDTOList>';
      for (index in that.candidateLanguages) {
        var candidateLanguage = that.candidateLanguages[index];
        xml += candidateLanguage.xmlData();
      }
      xml += '</candidateLanguagesDTOList>';
      if (that.areaExpertise != "") {
        xml += that.areaExpertise.xmlData();
      }
      if (that.jobSought != "") {
        xml += that.jobSought.xmlData();
      }
      if (that.formation != "") {
        xml += that.formation.xmlData();
      }
      if (that.city != "") {
        xml += that.city.xmlData();
      }
			if (that.country != "") {
				xml += that.country.xmlData();
			}
			xml += '<place>' + htmlEncode(that.place)+ '</place>';
      xml += '</candidateDTO>';
      return xml;
    };
    this.candidate(data);
  }, 
  Formation: function (data) {
    var that = this;
    that.formationId = '';
    that.formationName = '';
    that.institutionName = '';
    that.institutionCountry = '';
    that.obtained = '';
    that.obtainedDate = '';
    this.formation = function (data) {
      if ($(data).find("formationId"))
        that.formationId = $(data).find("formationId").text();
      if ($(data).find("formationName"))
        that.formationName = $(data).find("formationName").text();
      if ($(data).find("institutionName"))
        that.institutionName = $(data).find("institutionName").text();
      if ($(data).find("institutionCountry"))
        that.institutionCountry = $(data).find("institutionCountry").text();
      if ($(data).find("obtained"))
        that.obtained = $(data).find("obtained").text();
      if ($(data).find("obtainedDate"))
        that.obtainedDate = new Date($(data).find("obtainedDate").text());
    };
    this.getFormationId = function () {
      return that.formationId;
    };
    this.getFormationName = function () {
      return that.formationName;
    };
    this.getInstitutionName = function () {
      return that.institutionName;
    };
    this.getInstitutionCountry = function () {
      return that.institutionCountry;
    };
    this.getObtained = function () {
      return that.obtained;
    };
    this.getObtainedDate = function () {
      return that.obtainedDate;
    };
    this.setFormationId = function (formationId) {
      that.formationId = formationId;
    };
    this.setFormationName = function (formationName) {
      that.formationName = formationName;
    };
    this.setInstitutionName = function (institutionName) {
      that.institutionName = institutionName;
    };
    this.setInstitutionCountry = function (institutionCountry) {
      that.institutionCountry = institutionCountry;
    };
    this.setObtained = function (obtained) {
      that.obtained = obtained;
    };
    this.setObtainedDate = function (obtainedDate) {
      that.obtainedDate = obtainedDate;
    };
    this.update = function (newData) {
      that.formation(newData);
    };
    this.xmlData = function () {
      var xml = '<formationDTO>';
      xml += '<formationId>' + that.formationId + '</formationId>';
      xml += '<formationName>' + htmlEncode(that.formationName) + '</formationName>';
      xml += '<institutionName>' + htmlEncode(that.institutionName) + '</institutionName>';
      xml += '<institutionCountry>' + htmlEncode(that.institutionCountry) + '</institutionCountry>';
      xml += '<obtained>' + that.obtained + '</obtained>';
      if (that.obtainedDate != undefined)
        xml += '<obtainedDate>' + that.obtainedDate.toJSON() + '</obtainedDate>';
      else
        xml += '<obtainedDate></obtainedDate>';
      xml += '</formationDTO>';
      return xml;
    };
    this.formation(data);
  },
  CandidateLanguages: function (data) {
    var that = this;
    that.languageUserProfile = '';
    that.languageLevel = '';

    this.candidateLanguages = function (data) {
      if ($(data).find("languageUserProfile"))
        that.languageUserProfile = new $.LanguageUserProfile($(data).find("languageUserProfile"));
      if ($(data).find("languageLevel"))
        that.languageLevel = new $.LanguageLevel($(data).find("languageLevel"));
    };
    this.getLanguageUserProfile = function () {
      return that.languageUserProfile;
    };
    this.getLanguageLevel = function () {
      return that.languageLevel;
    };
    this.setLanguageUserProfile = function (languageUserProfile) {
      that.languageUserProfile = languageUserProfile;
    }
    ;
    this.setLanguageLevel = function (languageLevel) {
      that.languageLevel = languageLevel;
    };
    this.update = function (newData) {
      that.candidateLanguages(newData);
    };
    this.xmlData = function () {
      var xml = '<candidateLanguages>';
      if (that.languageUserProfile != "")
        xml += that.languageUserProfile.xmlData();
      if (that.languageLevel != "")
        xml += that.languageLevel.xmlData();
      xml += '</candidateLanguages>';
      return xml;
    };
    this.candidateLanguages(data);
  },
  LanguageUserProfile: function (data) {
    var that = this;
    that.languageUserProfileId = '';
    that.languageUserProfileName = '';
    this.languageUserProfile = function (data) {
      if ($(data).find("languageUserProfileId"))
        that.languageUserProfileId = $(data).find("languageUserProfileId").text();
      if ($(data).find("languageUserProfileName"))
        that.languageUserProfileName = $(data).find("languageUserProfileName").text();
    };
    this.getLanguageUserProfileId = function () {
      return that.languageUserProfileId;
    };
    this.getLanguageUserProfileName = function () {
      return that.languageUserProfileName;
    };
    this.setLanguageUserProfileId = function (languageUserProfileId) {
      that.languageUserProfileId = languageUserProfileId;
    };
    this.setLanguageUserProfileName = function (languageUserProfileName) {
      that.languageUserProfileName = languageUserProfileName;
    };
    this.update = function (newData) {
      that.languageUserProfile(newData);
    };
    this.xmlData = function () {
      var xml = '<languageUserProfile>';
      xml += '<languageUserProfileId>' + that.languageUserProfileId + '</languageUserProfileId>';
      xml += '<languageUserProfileName>' + htmlEncode(that.languageUserProfileName) + '</languageUserProfileName>';
      xml += '</languageUserProfile>';
      return xml;
    };
    this.languageUserProfile(data);
  },
  LanguageLevel: function (data) {
    var that = this;
    that.languageLevelId = '';
    that.languageLevelName = '';
    this.languageLevel = function (data) {
      if ($(data).find("languageLevelId"))
        that.languageLevelId = $(data).find("languageLevelId").text();
      if ($(data).find("languageLevelName"))
        that.languageLevelName = $(data).find("languageLevelName").text();
    };
    this.getLanguageLevelId = function () {
      return that.languageLevelId;
    };
    this.getLanguageLevelName = function () {
      return that.languageLevelName;
    };
    this.setLanguageLevelId = function (languageLevelId) {
      that.languageLevelId = languageLevelId;
    };
    this.setLanguageLevelName = function (languageLevelName) {
      that.languageLevelName = languageLevelName;
    };
    this.update = function (newData) {
      that.languageLevel(newData);
    };
    this.xmlData = function () {
      var xml = '<languageLevel>';
      xml += '<languageLevelId>' + that.languageLevelId + '</languageLevelId>';
      xml += '<languageLevelName>' + htmlEncode(that.languageLevelName) + '</languageLevelName>';
      xml += '</languageLevel>';
      return xml;
    };
    this.languageLevel(data);
  },
  UserProfile: function (data) {
    var that = this;
    that.userProfileId = '';
    that.firstName = '';
    that.secondName = '';
    that.avatar = '';
    that.pseudoSkype = '';
    that.webSite = '';
    that.defaultLanguage = null;
    that.canChatOnPrivate = false;

    this.userProfile = function (data) {
      if ($(data).find("userProfileId"))
        that.userProfileId = $(data).find("userProfileId").text();
      if ($(data).find("firstName"))
        that.firstName = $(data).find("firstName").text();
      if ($(data).find("secondName"))
        that.secondName = $(data).find("secondName").text();
      if ($(data).find("avatar"))
        that.avatar = $(data).find("avatar").text();
      if ($(data).find("pseudoSkype"))
        that.pseudoSkype = $(data).find("pseudoSkype").text();
			if ($(data).find("webSite"))
				that.webSite = $(data).find("webSite").text();
			if ($(data).find("defaultLanguageDTO"))
				that.defaultLanguage = new $.Language($(data).find("defaultLanguageDTO"));
    };
    this.getUserProfileId = function () {
      return that.userProfileId;
    };
    this.getFirstName = function () {
      return that.firstName;
    };
    this.getSecondName = function () {
      return that.secondName;
    };
    this.getAvatar = function () {
      return that.avatar;
    };
    this.getPseudoSkype = function () {
      return that.pseudoSkype;
    };
		this.getWebSite = function() {
			return that.webSite;
		};
		this.getDefaultLanguage = function() {
			return that.defaultLanguage;
		};
    this.getCanChatOnPrivate= function() {
    	return that.canChatOnPrivate;
    };
    this.setUserProfileId = function (userProfileId) {
      that.userProfileId = userProfileId;
    };
    this.setFirstName = function (firstName) {
      that.firstName = firstName;
    };
    this.setSecondName = function (secondName) {
      that.secondName = secondName;
    };
    this.setAvatar = function (avatar) {
      that.avatar = avatar;
    };
    this.setPseudoSkype = function (pseudoSkype) {
      that.pseudoSkype = pseudoSkype;
    };
		this.setWebSite = function(webSite) {
			that.webSite = webSite;
		};
    this.getFullName = function () {
      return that.firstName + ' ' + that.secondName;
    };
		this.setDefaultLanguage = function(defaultLanguage) {
			that.defaultLanguage = defaultLanguage;
		};
    this.setCanChatOnPrivate = function (canChatOnPrivate) {
      that.canChatOnPrivate = canChatOnPrivate;
    };
    this.update = function (newData) {
      that.userProfile(newData);
    };
    this.xmlData = function () {
      var xml = '<userProfileDTO>';
      xml += '<userProfileId>' + that.userProfileId + '</userProfileId>';
      xml += '<firstName>' + htmlEncode(that.firstName) + '</firstName>';
      xml += '<secondName>' + htmlEncode(that.secondName) + '</secondName>';
      xml += '<avatar>' + htmlEncode(that.avatar) + '</avatar>';
      xml += '<pseudoSkype>' + htmlEncode(that.pseudoSkype) + '</pseudoSkype>';
			xml += '<webSite>' + htmlEncode(that.webSite) + '</webSite>';
			if (that.defaultLanguage != null) {
				xml += that.defaultLanguage.xmlData().replace(/languageDTO>/g, "defaultLanguageDTO>");
			}
      xml += '</userProfileDTO>';
      return xml;
    };

    this.userProfile(data);
  },
  AreaExpertise: function (data) {
    var that = this;
    that.areaExpertiseId = '';
    that.areaExpertiseName = '';
    this.areaExpertise = function (data) {
      if ($(data).find("areaExpertiseId"))
        that.areaExpertiseId = $(data).find("areaExpertiseId").text();
      if ($(data).find("areaExpertiseName"))
        that.areaExpertiseName = $(data).find("areaExpertiseName").text();
    };
    this.getAreaExpertiseId = function () {
      return that.areaExpertiseId;
    };
    this.getAreaExpertiseName = function () {
      return that.areaExpertiseName;
    };
    this.setAreaExpertiseId = function (areaExpertiseId) {
      that.areaExpertiseId = areaExpertiseId;
    };
    this.setAreaExpertiseName = function (areaExpertiseName) {
      that.areaExpertiseName = areaExpertiseName;
    };
    this.update = function (newData) {
      that.areaExpertise(newData);
    };
    this.xmlData = function () {
      var xml = '<areaExpertiseDTO>';
      xml += '<areaExpertiseId>' + that.areaExpertiseId + '</areaExpertiseId>';
      xml += '<areaExpertiseName>' + htmlEncode(that.areaExpertiseName) + '</areaExpertiseName>';
      xml += '</areaExpertiseDTO>';
      return xml;
    };
    this.areaExpertise(data);
  },
  StudyLevel: function (data) {
    var that = this;
    that.idStudyLevel = '';
    that.studyLevelName = '';

    this.studyLevel = function (data) {
      if (data) {
        if ($(data).find("idStudyLevel"))
          that.idStudyLevel = $(data).find("idStudyLevel").text();
        if ($(data).find("studyLevelName"))
          that.studyLevelName = $(data).find("studyLevelName").text();
      }
    };
    this.getIdStudyLevel = function () {
      return that.idStudyLevel;
    };
    this.getStudyLevelName = function () {
      return that.studyLevelName;
    };
    this.setIdStudyLevel = function (idStudyLevel) {
      that.idStudyLevel = idStudyLevel;
    };
    this.setStudyLevelName = function (studyLevelName) {
      that.studyLevelName = studyLevelName;
    };
    //Ajout d update
    this.update = function (newData) {
      that.studyLevel(newData);
    };
    this.xmlData = function () {
      var xml = '<studyLevelDTO>';
      xml += '<idStudyLevel>' + that.idStudyLevel + '</idStudyLevel>';
      xml += '<studyLevelName>' + htmlEncode(that.studyLevelName) + '</studyLevelName>';
      xml += '</studyLevelDTO>';
      return xml;
    };

    this.studyLevel(data);
  },
  FunctionCandidate: function (data) {
    var that = this;
    that.idFunction = '';
    that.functionName = '';

    this.functionCandidate = function (data) {
      if (data) {
        if ($(data).find("idFunction"))
          that.idFunction = $(data).find("idFunction").text();
        if ($(data).find("functionName"))
          that.functionName = $(data).find("functionName").text();
      }
    };
    this.getIdFunction = function () {
      return that.idFunction;
    };
    this.getFunctionName = function () {
      return that.functionName;
    };
    this.setIdFunction = function (idFunction) {
      that.idFunction = idFunction;
    };
    this.setFunctionName = function (functionName) {
      that.functionName = functionName;
    };
    this.update = function (newData) {
      that.functionCandidate(newData);
    };
    this.xmlData = function () {
      var xml = '<functionCandidateDTO>';
      xml += '<idFunction>' + that.idFunction + '</idFunction>';
      xml += '<functionName>' + htmlEncode(that.functionName) + '</functionName>';
      xml += '</functionCandidateDTO>';
      return xml;
    };
    this.functionCandidate(data);
  },
  ExperienceYears: function (data) {
    var that = this;
    that.idExperienceYears = '';
    that.experienceYearsName = '';

    this.experienceYears = function (data) {
      if ($(data).find("idExperienceYears"))
        that.idExperienceYears = $(data).find("idExperienceYears").text();
      if ($(data).find("experienceYearsName"))
        that.experienceYearsName = $(data).find("experienceYearsName").text();
    };
    this.getIdExperienceYears = function () {
      return that.idExperienceYears;
    };
    this.getExperienceYearsName = function () {
      return that.experienceYearsName;
    };
    this.setIdExperienceYears = function (idExperienceYears) {
      that.idExperienceYears = idExperienceYears;
    };
    this.setExperienceYearsName = function (experienceYearsName) {
      that.experienceYearsName = experienceYearsName;
    };
    this.update = function (newData) {
      that.experienceYears(newData);
    };

    this.xmlData = function () {
      var xml = '<experienceYearsDTO>';
      xml += '<idExperienceYears>' + that.idExperienceYears + '</idExperienceYears>';
      xml += '<experienceYearsName>' + htmlEncode(that.experienceYearsName) + '</experienceYearsName>';
      xml += '</experienceYearsDTO>';
      return xml;
    };

    this.experienceYears(data);
  },
  StandSector: function (data) {
    var that = this;
    that.sectorId = 0;
    that.sectorName = '';

    this.standSector = function (data) {
      if (data) {
        if ($(data).find("sectorId"))
          that.sectorId = $(data).find("sectorId").text();
        if ($(data).find("sectorName"))
          that.sectorName = $(data).find("sectorName").text();
      }
    };
    this.getSectorId = function () {
      return that.sectorId;
    };
    this.getSectorName = function () {
      return that.sectorName;
    };
    this.setSectorId = function (sectorId) {
      that.sectorId = sectorId;
    };
    this.setSectorName = function (sectorName) {
      that.sectorName = sectorName;
    };
    this.update = function (newData) {
      that.standSector(newData);
    };
    this.xmlData = function () {
      var xml = '<sectorDTO>';
      xml += '<sectorId>' + that.sectorId + '</sectorId>';
      xml += '<sectorName>' + htmlEncode(that.sectorName) + '</sectorName>';
      xml += '</sectorDTO>';
      return xml;
    };

    this.standSector(data);
  },
  SecteurAct: function (data) {
    var that = this;
    that.idsecteur = 0;
    that.name = '';

    this.secteurAct = function (data) {
      if (data) {
        if ($(data).find("idsecteur"))
          that.idsecteur = $(data).find("idsecteur").text();
        if ($(data).find("name"))
          that.name = $(data).find("name").text();
      }
    };
    this.getIdsecteur = function () {
      return that.idsecteur;
    };
    this.getName = function () {
      return that.name;
    };
    this.setIdsecteur = function (idsecteur) {
      that.idsecteur = idsecteur;
    };
    this.setName = function (name) {
      that.name = name;
    };
    this.update = function (newData) {
      that.secteurAct(newData);
    };
    this.xmlData = function () {
      var xml = '<secteurActDTO>';
      xml += '<idsecteur>' + that.idsecteur + '</idsecteur>';
      xml += '<name>' + htmlEncode(that.name) + '</name>';
      xml += '</secteurActDTO>';
      return xml;
    };

    this.secteurAct(data);
  },
  Region: function (data) {
    var that = this;
    that.idRegion = '';
    that.regionName = '';
    this.region = function (data) {
      if ($(data).find("idRegion"))
        that.idRegion = $(data).find("idRegion").text();
      if ($(data).find("regionName"))
        that.regionName = $(data).find("regionName").text();
    };
    this.getIdRegion = function () {
      return that.idRegion;
    };
    this.getRegionName = function () {
      return that.regionName;
    };
    this.setIdRegion = function (idRegion) {
      that.idRegion = idRegion;
    };
    this.setRegionName = function (regionName) {
      that.regionName = regionName;
    };
    this.update = function (newData) {
      that.region(newData);
    };

    this.xmlData = function () {
      var xml = '<idRegion>' + that.idRegion + '</idRegion>';
      xml += '<regionName>' + htmlEncode(that.regionName) + '</regionName>';
      return xml;
    };
    this.region(data);
  },
  PdfCv: function (data) {
    var that = this;
    that.idPdfCv = '';
    that.cvName = '';
    that.urlCv = '';

    this.pdfCv = function (data) {
      if ($(data).find("idPdfCv"))
        that.idPdfCv = $(data).find("idPdfCv").text();
      if ($(data).find("cvName"))
        that.cvName = $(data).find("cvName").text();
      if ($(data).find("urlCv"))
        that.urlCv = $(data).find("urlCv").text();
    };
    this.getIdPdfCv = function () {
      return that.idPdfCv;
    };
    this.getCvName = function () {
      return that.cvName;
    };
    this.getUrlCv = function () {
      return that.urlCv;
    };
    this.setIdPdfCv = function (idPdfCv) {
      that.idPdfCv = idPdfCv;
    };
    this.setCvName = function (cvName) {
      that.cvName = cvName;
    };
    this.setUrlCv = function (urlCv) {
      that.urlCv = urlCv;
    };
    this.update = function (newData) {
      that.pdfCv(newData);
    };

    this.xmlData = function () {
      var xml = '<pdfCvDTO>';
      xml += '<idPdfCv>' + that.idPdfCv + '</idPdfCv>';
      xml += '<urlCv>' + htmlEncode(that.urlCv) + '</urlCv>';
      xml += '<cvName>' + htmlEncode(that.cvName) + '</cvName>';
      xml += '</pdfCvDTO>';
      return xml;
    };
    this.pdfCv(data);
  },
  CvContent: function (data) {
    var that = this;
    that.idcvcontent = '';
    that.titleCV = '';
    that.description = '';
    that.tags = '';
    that.leisures = '';
    that.isVisibleEnterprises = '';
    that.isVisibleCandidates = '';
    that.projetCvs = '';
    that.experienceCvs = '';
    that.formationCvs = '';

    this.cvContent = function (data) {
      if (data) {
        if ($(data).find("idcvcontent"))
          that.idcvcontent = $(data).find("idcvcontent").text();
        if ($(data).find("titleCV"))
          that.titleCV = $(data).find("titleCV").text();
        if ($(data).find("description"))
          that.description = $(data).find("description").text();
        if ($(data).find("tags"))
          that.tags = $(data).find("tags").text();
        if ($(data).find("leisures"))
          that.leisures = $(data).find("leisures").text();
        if ($(data).find("isVisibleEnterprises"))
          that.isVisibleEnterprises = $(data).find("isVisibleEnterprises").text();
        if ($(data).find("isVisibleCandidates"))
          that.isVisibleCandidates = $(data).find("isVisibleCandidates").text();
        that.projetCvs = new Array();
        $(data).find("projetcv").each(function () {
          var projetCv = new $.ProjetCv($(this));
          that.projetCvs.push(projetCv);
        });
        that.experienceCvs = new Array();
        $(data).find("experiencecv").each(function () {
          var experienceCv = new $.ExperienceCv($(this));
          that.experienceCvs.push(experienceCv);
        });
        that.formationCvs = new Array();
        $(data).find("formationcv").each(function () {
          var formationCv = new $.FormationCv($(this));
          that.formationCvs.push(formationCv);
        });
      }
    };
    this.getIdcvcontent = function () {
      return that.idcvcontent;
    };
    this.getTitleCV = function () {
      return that.titleCV;
    };
    this.getDescription = function () {
      return that.description;
    };
    this.getTags = function () {
      return that.tags;
    };
    this.getLeisures = function () {
      return that.leisures;
    };
    this.getIsVisibleEnterprises = function () {
      return that.isVisibleEnterprises;
    };
    this.getIsVisibleCandidates = function () {
      return that.isVisibleCandidates;
    };
    this.getProjetCvs = function () {
      return that.projetCvs;
    };
    this.getExperienceCvs = function () {
      return that.experienceCvs;
    };
    this.getFormationCvs = function () {
      return that.formationCvs;
    };
    this.setIdcvcontent = function (idcvcontent) {
      that.idcvcontent = idcvcontent;
    };
    this.setTitleCV = function (titleCV) {
      that.titleCV = titleCV;
    };
    this.setDescription = function (description) {
      that.description = description;
    };
    this.setTags = function (tags) {
      that.tags = tags;
    };
    this.setLeisures = function (leisures) {
      that.leisures = leisures;
    };
    this.setIsVisibleEnterprises = function (isVisibleEnterprises) {
      that.isVisibleEnterprises = isVisibleEnterprises;
    };
    this.setIsVisibleCandidates = function (isVisibleCandidates) {
      that.isVisibleCandidates = isVisibleCandidates;
    };
    this.setProjetCvs = function (projetCvs) {
      that.projetCvs = projetCvs;
    };
    this.setExperienceCvs = function (experienceCvs) {
      that.experienceCvs = experienceCvs;
    };
    this.setFormationCvs = function (formationCvs) {
      that.formationCvs = formationCvs;
    };

    this.update = function (newData) {
      that.cvContent(newData);
    };

    this.xmlData = function () {
      var xml = '<cvcontent>';
      xml += '<idcvcontent>' + that.idcvcontent + '</idcvcontent>';
      xml += '<titleCV>' + that.CV + '</titleCV>';
      xml += '<description>' + that.description + '</description>';
      xml += '<tags>' + that.tags + '</tags>';
      xml += '<leisures>' + that.leisures + '</leisures>';
      xml += '<isVisibleEnterprises>' + that.isVisibleEnterprises + '</isVisibleEnterprises>';
      xml += '<isVisibleCandidates>' + that.isVisibleCandidates + '</isVisibleCandidates>';

      xml += '<projects>';
      for (index in that.projetCvs) {
        var projetCv = that.projetCvs[index];
        xml += projetCv.xmlData();
      }
      xml += '</projects>';

      xml += '<experiences>';
      for (index in that.experienceCvs) {
        var experienceCv = that.experienceCvs[index];
        xml += experienceCv.xmlData();
      }
      xml += '</experiences>';

      xml += '<formations>';
      for (index in that.formationCvs) {
        var formationCv = that.formationCvs[index];
        xml += formationCv.xmlData();
      }
      xml += '</formations>';

      xml += '</cvcontent>';
      return xml;
    };

    this.cvContent(data);
  },
  ProjetCv: function (data) {
    var that = this;
    that.idprojectcv = '';
    that.beginDate = '';
    that.endDate = '';
    that.name = '';
    that.description = '';
    that.url = '';

    this.projetCv = function (data) {
      if (data) {
        if ($(data).find("idprojectcv"))
          that.idprojectcv = $(data).find("idprojectcv").text();
        if ($(data).find("beginDate"))
          that.beginDate = $(data).find("beginDate").text();
        if ($(data).find("endDate"))
          that.endDate = $(data).find("endDate").text();
        if ($(data).find("name"))
          that.name = $(data).find("name").text();
        if ($(data).find("description"))
          that.description = $(data).find("description").text();
        if ($(data).find("url"))
          that.url = $(data).find("url").text();
      }
    };
    this.getIdprojectcv = function () {
      return that.idprojectcv;
    };
    this.getBeginDate = function () {
      return that.beginDate;
    };
    this.getEndDate = function () {
      return that.endDate;
    };
    this.getName = function () {
      return that.name;
    };
    this.getDescription = function () {
      return that.description;
    };
    this.getUrl = function () {
      return that.url;
    };
    this.setIdprojectcv = function (idprojectcv) {
      that.idprojectcv = idprojectcv;
    };
    this.setBeginDate = function (beginDate) {
      that.beginDate = beginDate;
    };
    this.setEndDate = function (endDate) {
      that.endDate = endDate;
    };
    this.setName = function (name) {
      that.name = name;
    };
    this.setDescription = function (description) {
      that.description = description;
    };
    this.setUrl = function (url) {
      that.url = url;
    };

    this.update = function (newData) {
      that.projetCv(newData);
    };

    this.xmlData = function () {
      var xml = '<projetcv>';
      xml += '<idprojectcv>' + that.idprojectcv + '</idprojectcv>';
      xml += '<beginDate>' + that.beginDate + '</beginDate>';
      xml += '<endDate>' + that.endDate + '</endDate>';
      xml += '<name>' + that.name + '</name>';
      xml += '<description>' + that.description + '</description>';
      xml += '<url>' + that.url + '</url>';
      xml += '</projetcv>';
      return xml;
    };

    this.projetCv(data);
  },
  ExperienceCv: function (data) {
    var that = this;
    that.idexperiencecv = '';
    that.beginDate = '';
    that.endDate = '';
    that.jobTitle = '';
    that.description = '';
    that.companyName = '';
    that.currentPosition = '';

    this.experienceCv = function (data) {
      if (data) {
        if ($(data).find("idexperiencecv"))
          that.idexperiencecv = $(data).find("idexperiencecv").text();
        if ($(data).find("beginDate"))
          that.beginDate = $(data).find("beginDate").text();
        if ($(data).find("endDate"))
          that.endDate = $(data).find("endDate").text();
        if ($(data).find("jobTitle"))
          that.jobTitle = $(data).find("jobTitle").text();
        if ($(data).find("description"))
          that.description = $(data).find("description").text();
        if ($(data).find("companyName"))
          that.companyName = $(data).find("companyName").text();
        if ($(data).find("currentPosition"))
          that.currentPosition = $(data).find("currentPosition").text();
      }
    };
    this.getIdexperiencecv = function () {
      return that.idexperiencecv;
    };
    this.getBeginDate = function () {
      return that.beginDate;
    };
    this.getEndDate = function () {
      return that.endDate;
    };
    this.getJobTitle = function () {
      return that.jobTitle;
    };
    this.getDescription = function () {
      return that.description;
    };
    this.getCompanyName = function () {
      return that.companyName;
    };
    this.getCurrentPosition = function () {
      return that.currentPosition;
    };
    this.setIdexperiencecv = function (idexperiencecv) {
      that.idexperiencecv = idexperiencecv;
    };
    this.setBeginDate = function (beginDate) {
      that.beginDate = beginDate;
    };
    this.setEndDate = function (endDate) {
      that.endDate = endDate;
    };
    this.setJobTitle = function (jobTitle) {
      that.jobTitle = jobTitle;
    };
    this.setDescription = function (description) {
      that.description = description;
    };
    this.setCompanyName = function (companyName) {
      that.companyName = companyName;
    };
    this.setCurrentPosition = function (currentPosition) {
      that.currentPosition = currentPosition;
    };

    this.update = function (newData) {
      that.experienceCv(newData);
    };

    this.xmlData = function () {
      var xml = '<experiencecv>';
      xml += '<idexperiencecv>' + that.idexperiencecv + '</idexperiencecv>';
      xml += '<beginDate>' + that.beginDate + '</beginDate>';
      xml += '<endDate>' + that.endDate + '</endDate>';
      xml += '<jobTitle>' + that.jobTitle + '</jobTitle>';
      xml += '<description>' + that.description + '</description>';
      xml += '<companyName>' + that.companyName + '</companyName>';
      xml += '<currentPosition>' + that.currentPosition + '</currentPosition>';
      xml += '</experiencecv>';
      return xml;
    };

    this.experienceCv(data);
  },
  FormationCv: function (data) {
    var that = this;
    that.idformationcv = '';
    that.beginDate = '';
    that.endDate = '';
    that.schoolName = '';
    that.description = '';
    that.speciality = '';
    that.degree = '';

    this.formationcv = function (data) {
      if (data) {
        if ($(data).find("idformationcv"))
          that.idformationcv = $(data).find("idformationcv").text();
        if ($(data).find("beginDate"))
          that.beginDate = $(data).find("beginDate").text();
        if ($(data).find("endDate"))
          that.endDate = $(data).find("endDate").text();
        if ($(data).find("schoolName"))
          that.schoolName = $(data).find("schoolName").text();
        if ($(data).find("description"))
          that.description = $(data).find("description").text();
        if ($(data).find("speciality"))
          that.speciality = $(data).find("speciality").text();
        if ($(data).find("degree"))
          that.degree = $(data).find("degree").text();
      }
    };
    this.getIdformationcv = function () {
      return that.idformationcv;
    };
    this.getBeginDate = function () {
      return that.beginDate;
    };
    this.getEndDate = function () {
      return that.endDate;
    };
    this.getSchoolName = function () {
      return that.schoolName;
    };
    this.getDescription = function () {
      return that.description;
    };
    this.getSpeciality = function () {
      return that.speciality;
    };
    this.getDegree = function () {
      return that.degree;
    };
    this.setIdformationcv = function (idformationcv) {
      that.idformationcv = idformationcv;
    };
    this.setBeginDate = function (beginDate) {
      that.beginDate = beginDate;
    };
    this.setEndDate = function (endDate) {
      that.endDate = endDate;
    };
    this.setSchoolName = function (schoolName) {
      that.schoolName = schoolName;
    };
    this.setDescription = function (description) {
      that.description = description;
    };
    this.setSpeciality = function (speciality) {
      that.speciality = speciality;
    };
    this.setDegree = function (degree) {
      that.degree = degree;
    };

    this.update = function (newData) {
      that.formationcv(newData);
    };

    this.xmlData = function () {
      var xml = '<formationcv>';
      xml += '<idformationcv>' + that.idformationcv + '</idformationcv>';
      xml += '<beginDate>' + that.beginDate + '</beginDate>';
      xml += '<endDate>' + that.endDate + '</endDate>';
      xml += '<schoolName>' + that.schoolName + '</schoolName>';
      xml += '<description>' + that.description + '</description>';
      xml += '<speciality>' + that.speciality + '</speciality>';
      xml += '<degree>' + that.degree + '</degree>';
      xml += '</formationcv>';
      return xml;
    };

    this.formationcv(data);
  },
  Stand: function (data) {
    var that = this;
    that.idStand = '';
    that.isInfoForum = '';
    that.name = '';
    that.profilesPlusRecherche = '';
    that.socialWallMessage = '';
    that.sustitutionLanguageId = 0;
    that.enterprise = '';
    that.groupStand = '';
    that.connectTChatLanguages = [];

    this.stand = function (data) {
      if (data) {
        if ($(data).find("idstande"))
          that.idstande = $(data).find("idstande").text();
        if ($(data).find("isinfoforum"))
          that.isInfoForum = $(data).find("isinfoforum").text();
        if ($(data).find("stand > name"))
          that.name = $(data).find("stand > name").text();
        if(that.name == "") that.name = $(data).find("standDTO > name").text();
        if ($(data).find("profilesplusrecherche"))
          that.profilesPlusRecherche = $(data).find("profilesplusrecherche").text();
        if ($(data).find("socialWallMessage"))
          that.socialWallMessage = $(data).find("socialWallMessage").text();
        if ($(data).find("sustitutionLanguageId"))
          that.sustitutionLanguageId = $(data).find("sustitutionLanguageId").text();
        if ($(data).find("enterpriseDTO"))
          that.enterprise = new $.Enterprise($(data).find("enterpriseDTO"));
        if ($(data).find("groupStandDTO"))
          that.groupStand = new $.GroupStand($(data).find("groupStandDTO"));
      }
    };
    this.getIdStande = function () {
      return that.idstande;
    };
    this.getIsInfoForum = function () {
      return that.isInfoForum;
    };
    this.getName = function () {
      return that.name;
    };
    this.getProfilesPlusRecherche = function () {
      return that.profilesPlusRecherche;
    };
    this.getSocialWallMessage = function () {
      return that.socialWallMessage;
    };
    this.getSustitutionLanguageId = function () {
      return that.sustitutionLanguageId;
    };
    this.getEnterprise = function () {
      return that.enterprise;
    };
    this.getGroupStand = function () {
      return that.groupStand;
    };
    this.getConnectTChatLanguages = function () {
      return that.connectTChatLanguages;
    };
    this.getNameSectors = function () {
      var sectorsNames = new Array();
      var $sectors = $(data).find("sectorsDTOList").find("sectorDTO");
      $sectors.each(function () {
        sectorsNames.push($(this).find('sectorName').text());
      });
			return sectorsNames.join('- ');
    };
    this.isConnected = function () {
      return this.connected;
    };

    this.setConnected = function (connected) {
      this.connected = connected;
    };
    this.setIdStande = function (idstande) {
      that.idstande = idstande;
    };
    this.setIsInfoForum = function (isInfoForum) {
      that.isInfoForum = isInfoForum;
    };
    this.setName = function (name) {
      that.name = name;
    };
    this.setProfilesPlusRecherche = function (profilesPlusRecherche) {
      that.profilesPlusRecherche = profilesPlusRecherche;
    };
    this.setSocialWallMessage = function (socialWallMessage) {
      that.socialWallMessage = socialWallMessage;
    };
    this.setSustitutionLanguageId = function (sustitutionLanguageId) {
      that.sustitutionLanguageId = sustitutionLanguageId;
    };
    this.setEnterprise = function (enterprise) {
      that.enterprise = enterprise;
    };
    this.setGroupStand = function (groupStand) {
      that.groupStand = groupStand;
    };
    this.setConnectTChatLanguages = function (connectTChatLanguages) {
      that.connectTChatLanguages = connectTChatLanguages;
    };

    //Ajout d update
    this.update = function (newData) {
      that.studyLevel(newData);
    };
    this.xmlData = function () {
      var xml = '<stand>';
      if (that.languageUserProfile != "")
        xml += that.enterprise.xmlData();
      xml += '<idstande>' + htmlEncode(that.idstande) + '</idstande>';
      xml += '<isinfoforum>' + htmlEncode(that.isInfoForum) + '</isinfoforum>';
      xml += '<name>' + htmlEncode(that.name) + '</name>';
      xml += '<profilesplusrecherche>' + htmlEncode(that.profilesPlusRecherche) + '</profilesplusrecherche>';
      xml += '<socialWallMessage>' + htmlEncode(that.socialWallMessage) + '</socialWallMessage>';
      xml += '<sustitutionLanguageId>' + htmlEncode(that.sustitutionLanguageId) + '</sustitutionLanguageId>';
      xml += '</stand>';
      return xml;
    };

    this.stand(data);
  },
	EnterpriseP: function (data) {
     var that = this;
     that.email = '';
     that.enterprise = '';
     that.jobTitle = '';
     that.userProfile = '';
     that.cellPhone = '';
     that.type = 'EnterpriseP';
     that.connected = false;

     this.enterpriseP = function (data) {
       if ($(data).find("email"))
         that.email = $(data).find("email").text();
       if ($(data).find("cellPhone"))
         that.cellPhone = $(data).find("cellPhone").text();
       if ($(data).find("enterpriseDTO"))
           that.enterprise = new $.Enterprise($(data).find("enterpriseDTO"));
       if ($(data).find("jobTitle"))
         that.jobTitle = $(data).find("jobTitle").text();
       if ($(data).find("userProfileDTO"))
           that.userProfile = new $.UserProfile($(data).find("userProfileDTO"));
     };

     this.getEmail = function () {
       return that.email;
     };
     this.getCellPhone = function () {
       return that.cellPhone;
     };
     this.getEnterprise = function () {
       return that.enterprise;
     };
     this.getJobTitle = function () {
       return that.jobTitle;
     };
     this.getUserProfile = function () {
       return that.userProfile;
     };
     this.isConnected= function() {
    	 return that.connected;
     };
     this.setEmail = function (email) {
       that.email = email;
     };
     this.setCellPhone = function (cellPhone) {
       that.cellPhone = cellPhone;
     };
     this.setEnterprise = function (enterprise) {
       that.enterprise = enterprise;
     };
     this.setJobTitle = function (jobTitle) {
       that.jobTitle = jobTitle;
     };
     this.setUserProfile = function (userProfile) {
       that.userProfile = userProfile;
     };
     this.setConnected = function (connected) {
       that.connected = connected;
     };
     this.setConnected = function (connected) {
       that.connected = connected;
     };
 		 this.getNameSectors = function() {
 			 var sectorsNames = new Array();
 			 var $sectors = $(data).find(
 					"secteurActDTO");
 			 $sectors.each(function() {
 				 sectorsNames.push($(this).find('name').text());
 			 });
 			 return sectorsNames.join('- ');
 		 };
     this.enterpriseP(data);
  },
  Enterprise: function (data) {
    var that = this;
    that.address = '';
    that.dateCreation = '';
    that.description = '';
    that.identrprise = '';
    that.lieu = '';
    that.logo = '';
    that.nbremploye = '';
    that.nom = '';
    that.siegName = '';
    that.slogan = '';
    that.website = '';    
    this.enterprise = function (data) {
      if ($(data).find("address"))
        that.address = $(data).find("address").text();
      if ($(data).find("datecreation"))
        that.dateCreation = $(data).find("datecreation").text();
      if ($(data).find("datecreation"))
        that.description = $(data).find("description").text();
      if ($(data).find("identrprise"))
        that.identrprise = $(data).find("identrprise").text();
      if ($(data).find("lieu"))
        that.lieu = $(data).find("lieu").text();
      if ($(data).find("logo"))
        that.logo = $(data).find("logo").text();
      if ($(data).find("nbremploye"))
        that.nbremploye = $(data).find("nbremploye").text();
      if ($(data).find("nom"))
        that.nom = $(data).find("nom").text();
      if ($(data).find("siegName"))
        that.siegName = $(data).find("siegName").text();
      if ($(data).find("slogan"))
        that.slogan = $(data).find("slogan").text();
      if ($(data).find("website"))
        that.website = $(data).find("website").text();
    };
    this.getAddress = function () {
      return that.address;
    };
    this.getDatecreation = function () {
      return that.dateCreation;
    };
    this.getDescription = function () {
      return that.description;
    };
    this.getIdentrprise = function () {
      return that.identrprise;
    };
    this.getLieu = function () {
      return that.lieu;
    };
    this.getLogo = function () {
      return that.logo;
    };
    this.getNbremploye = function () {
      return that.nbremploye;
    };
    this.getNom = function () {
      return that.nom;
    };
    this.getSiegName = function () {
      return that.siegName;
    };
    this.getSlogan = function () {
      return that.slogan;
    };
    this.getWebsite = function () {
      return that.website;
    };

    this.setAddress = function (address) {
      that.address = address;
    };
    this.setDatecreation = function (dateCreation) {
      that.dateCreation = dateCreation;
    };
    this.setDescription = function (description) {
      that.description = description;
    };
    this.setIdentrprise = function (identrprise) {
      that.identrprise = identrprise;
    };
    this.setLieu = function (lieu) {
      that.lieu = lieu;
    };
    this.setLogo = function (logo) {
      that.logo = logo;
    };
    this.setNbremploye = function (nbremploye) {
      that.nbremploye = nbremploye;
    };
    this.setNom = function (nom) {
      that.nom = nom;
    };
    this.setSiegName = function (siegName) {
      that.siegName = siegName;
    };
    this.setSlogan = function (slogan) {
      that.slogan = slogan;
    };
    this.setWebsite = function (website) {
      that.website = website;
    };
    this.update = function (newData) {
      that.enterprise(newData);
    };
    this.xmlData = function () {
      var xml = '<enterpriseDTO>';
      xml += '<address>' + htmlEncode(that.address) + '</address>';
      xml += '<datecreation>' + htmlEncode(that.dateCreation) + '</datecreation>';
      xml += '<description>' + htmlEncode(that.description) + '</description>';
      xml += '<identrprise>' + htmlEncode(that.identrprise) + '</identrprise>';
      xml += '<lieu>' + htmlEncode(that.lieu) + '</lieu>';
      xml += '<logo>' + htmlEncode(that.logo) + '</logo>';
      xml += '<nbremploye>' + htmlEncode(that.nbremploye) + '</nbremploye>';
      xml += '<nom>' + htmlEncode(that.nom) + '</nom>';
      xml += '<siegName>' + htmlEncode(that.siegName) + '</siegName>';
      xml += '<slogan>' + htmlEncode(that.slogan) + '</slogan>';
      xml += '</enterpriseDTO>';
      return xml;
    };
    this.enterprise(data);
  },
  Testimony: function (data) {
    var that = this;
    that.idTestimony = '';
    that.name = '';
    that.photo = '';
    that.standId = '';
    that.enterpriseName = '';
    that.jobTitle = '';
    that.testimonyContent = '';

    this.testimony = function (data) {
      if ($(data).find("idTestimony").text())
        that.idTestimony = $(data).find("idTestimony").text();
      if ($(data).find("name").text())
        that.name = $(data).find("name").text();
      if ($(data).find("photo").text())
        that.photo = $(data).find("photo").text();
      if ($(data).find("standId").text())
        that.standId = $(data).find("standId").text();
      if ($(data).find("enterpriseName").text())
        that.enterpriseName = $(data).find("enterpriseName").text();
      if ($(data).find("jobTitle").text())
        that.jobTitle = $(data).find("jobTitle").text();
      if ($(data).find("testimonyContent").text())
        that.testimonyContent = $(data).find("testimonyContent").text();
    };
    this.getName = function () {
      return that.name;
    };
    this.getPhoto = function () {
      return that.photo;
    };
    this.getJobTitle = function () {
      return that.jobTitle;
    };
    this.getTestimonyContent = function () {
      return that.testimonyContent;
    };
    this.getTestimonyId = function () {
      return that.idTestimony;
    };
    this.getStandId = function () {
      return that.standId;
    };
    this.getEnterpriseName = function () {
      return that.enterpriseName;
    };
    this.setName = function (name) {
      that.name = name;
    };
    this.setPhoto = function (photo) {
      that.photo;
    };
    this.setJobTitle = function (jobTitle) {
      that.jobTitle = jobTitle;
    };
    this.setTestimonyContent = function (testimonyContent) {
      that.testimonyContent = testimonyContent;
    };
    this.setTestimonyId = function (idTestimony) {
      that.idTestimony = idTestimony;
    };
    this.setStandId = function (standId) {
      that.standId = standId;
    };
    this.setEnterpriseName = function (enterpriseName) {
      that.enterpriseName = enterpriseName;
    };
    this.update = function (newData) {
      that.testimony(newData);
    };
    this.xmlData = function () {
      var xml = '<testimony>';
      xml += '<enterpriseName>' + htmlEncode(that.enterpriseName) + '</enterpriseName>';
      xml += '<idTestimony>' + htmlEncode(that.idTestimony) + '</idTestimony>';
      xml += '<jobTitle>' + htmlEncode(that.jobTitle) + '</jobTitle>';
      xml += '<name>' + htmlEncode(that.name) + '</name>';
      xml += '<photo>' + htmlEncode(that.photo) + '</photo>';
      xml += '<standId>' + htmlEncode(that.standId) + '</standId>';
      xml += '<testimonyContent>' + htmlEncode(that.testimonyContent) + '</testimonyContent>';
      xml += '</testimony>';
      return xml;
    };
    this.testimony(data);

  },
  JobSought: function (data) {
    var that = this;
    that.jobSoughtId = '';
    that.jobSoughtName = '';
    that.enable = '';

    this.jobSought = function (data) {
      if ($(data).find("jobSoughtId"))
        that.jobSoughtId = $(data).find("jobSoughtId").text();
      if ($(data).find("jobSoughtName"))
        that.jobSoughtName = $(data).find("jobSoughtName")
                .text();
      if ($(data).find("enable"))
        that.enable = $(data).find("enable").text();
    };

    this.getJobSoughtId = function () {
      return that.jobSoughtId;
    };
    this.getJobSoughtName = function () {
      return that.jobSoughtName;
    };
    this.getEnable = function () {
      return that.enable;
    };
    this.setJobSoughtId = function (jobSoughtId) {
      that.jobSoughtId = jobSoughtId;
    };
    this.setJobSoughtName = function (jobSoughtName) {
      that.jobSoughtName = jobSoughtName;
    };
    this.setEnable = function (enable) {
      that.enable = enable;
    };
    this.update = function (newData) {
      that.jobSought(newData);
    };

    this.xmlData = function () {
      var xml = '<jobSoughtDTO>';
      xml += '<jobSoughtId>' + that.jobSoughtId
              + '</jobSoughtId>';
      xml += '<jobSoughtName>' + htmlEncode(that.jobSoughtName)
              + '</jobSoughtName>';
      xml += '</jobSoughtDTO>';
      return xml;
    };

    this.jobSought(data);
  },
  JobOffer: function (data) {
    this.standId = $(data).find("standId").text();
    this.idJob = $(data).find("idmetier").text();
    this.jobTitle = $(data).find("title").text();
    this.jobReference = $(data).find("reference").text();
    this.enterpriseName = $(data).find("enterpriseName").text();
    this.enterpriseLogo = $(data).find("enterpriseLogo").text();
    this.description = $(data).children("description").text();
    this.tags = $(data).find("tags").text();
    this.internalApplication = $(data).find("internalApplication").text();
    this.jobUrl = $(data).find("url").text();
    this.functionCandidate = new $.FunctionCandidate($(data).find("functionCandidateDTO"));
    this.studyLevel = new $.StudyLevel($(data).find("studyLevelDTO"));
    this.experienceYears = new $.ExperienceYears($(data).find("experienceYearsDTO"));
    this.region = new $.Region($(data).find("regionDTO"));

    this.getStandId = function () {
      return this.standId;
    };

    this.getIdJob = function () {
      return this.idJob;
    };

    this.getJobTitle = function () {
      return this.jobTitle;
    };

    this.getJobReference = function () {
      return this.jobReference;
    };

    this.getNameSectors = function () {
      var sectorsNames = new Array();
      var $sectors = $(data).find("secteurActs").find("secteurActDTO");
      $sectors.each(function () {
        sectorsNames.push($(this).find('name').text());
      });
      return sectorsNames.join('- ');
    };

    this.getRegion = function () {
      return this.region;
    };

    this.getWorkType = function () {
      return $(data).find("workType").text();
    };
    this.getWorkPermitNames = function () {
      var workPermitNames = [];
      var $workPermits = $(data).find("workPermits").find("workPermit");
      $workPermits.each(function () {
        workPermitNames.push($(this).find('workPermitName').text());
      });
      return workPermitNames.toString().split(',').join(', ');
    };

    this.getEnterpriseName = function () {
      return this.enterpriseName;
    };

    this.getEnterpriseLogo = function () {
      return this.enterpriseLogo;
    };
    this.getJobDescription = function () {
      return this.description;
    };
    this.getJobTags = function () {
      return this.tags;
    };

    this.isInternalApplication = function () {
      return this.internalApplication;
    };

    this.getJobUrl = function () {
      return this.jobUrl;
    };

    this.getFunctionCandidate = function () {
      return this.functionCandidate;
    };

    this.getStudyLevel = function () {
      return this.studyLevel;
    };

    this.getExperienceYears = function () {
      return this.experienceYears;
    };

  },
  AllFiles: function (data) {
    this.allFilesId = $(data).find("idfile").text();
    this.title = $(data).find("title").text();
    this.enterpriseLogo = $(data).find("enterpriseDTO logo").text();
    this.enterpriseName = $(data).find("enterpriseDTO nom").text();
    this.url = $(data).find("url").text();
    this.standId = $(data).find("standDTO idstande").text();
    this.image = $(data).find("image").text();
    this.fileType = new $.FileType($(data).find("fileTypeDTO"));

    this.getAllFilesId = function () {
      return this.allFilesId;
    };

    this.getTitle = function () {
      return this.title;
    };

    this.getEnterpriseLogo = function () {
      return this.enterpriseLogo;
    };

    this.getEnterpriseName = function () {
      return this.enterpriseName;
    };

    this.getUrl = function () {
      return this.url;
    };

    this.getStandId = function () {
      return this.standId;
    };

    this.getFileType = function () {
      return this.fileType;
    };

  },
  FileType: function (data) {
    this.fileTypeId = parseInt($(data).find("idtypefile").text());
    this.fileTypeName = $(data).find("name").text();

    this.getFileTypeId = function () {
      return this.fileTypeId;
    };

    this.getFileTypeName = function () {
      return this.fileTypeName;
    };

  },
  Component: function (data) {
  	var that = this;
    that.componentId = "";
    that.componentName = "";

    that.component = function(data){
      if(data){
        if($(data).find("idComponent")) 
          that.componentId=$(data).find("idComponent").text();
        if($(data).find("componentName")) 
          that.componentName=$(data).find("componentName").text();
      }
    };
    
    this.getComponentId = function(){  
      return that.componentId;
    };
    
    this.getComponentName = function(){  
      return that.componentName;
    };
    
    this.setComponentId = function(componentId){
      that.componentId = componentId;
    };
    
    this.setComponentName = function(componentName){
      that.componentName=componentName;
    };
          
    this.update = function(newData){
      that.component(newData);
    };
    
    this.component(data);

  },
  Favorite: function (data) {
  	var that = this;
    that.favoriteId = "";
    that.entityId = "";
    that.status = "";
    that.component = "";
    that.jobOffer = "";
    that.contact = "";
    that.allfilles = "";
    that.testimony = "";
    that.advice = "";
    that.product = "";
    that.candidate = "";
    that.enterpriseP = "";

    that.favorite = function(data){
      if(data){
        if($(data).find("idFavorite")) 
          that.favoriteId=$(data).find("idFavorite").text();
        if($(data).find("idEntity")) 
          that.entityId=$(data).find("idEntity").text();
        if($(data).find("status")) 
          that.status=$(data).find("status").text();
        if($(data).find("component")) 
          that.component=new $.Component($(data).find("component"));
        if($(data).find("candidateDTO")) 
          that.candidate=new $.Candidate($(data).find("candidateDTO"));
        if($(data).find("enterprisePDTO")) 
          that.enterpriseP=new $.EnterpriseP($(data).find("enterprisePDTO"));
      }
    };
    
    this.getFavoriteId = function(){  
      return that.favoriteId;
    };
    
    this.getEntityId = function(){  
      return that.entityId;
    };
    
    this.getStatus = function(){ 
      return that.status;
    };
    
    this.getComponent = function(){ 
      return that.component;
    };
    
    this.getCandidate = function(){  
      return that.candidate;
    };
    
    this.getEnterpriseP = function(){  
      return that.enterpriseP;
    };
    
    this.setFavoriteId = function(favoriteId){
      that.favoriteId = favoriteId;
    };
    
    this.setEntityId = function(entityId){
      that.entityId=entityId;
    };
    
    this.setStatus = function(status){
      that.status=status;
    };
    
    this.setComponent = function(component){
      that.component=component;
    };
      
    this.setCandidate = function(candidate){
      that.candidate=candidate;
    };
    
    this.setEnterpriseP = function(enterpriseP){
      that.enterpriseP=enterpriseP;
    };
          
    this.update = function(newData){
      that.favorite(newData);
    };
    
    this.favorite(data);

  },
  
  FavoriteJobOffer: function (data) {
    this.idJob = $(data).find("idmetier").text();
    this.jobTitle = $(data).find("title").text();
    this.jobReference = $(data).find("reference").text();
    this.enterpriseName = $(data).find("enterpriseName").text();
    this.enterpriseLogo = $(data).find("enterpriseLogo").text();
    this.description = $(data).children("description").text();
    this.tags = $(data).find("tags").text();
    this.standId = $(data).find("standId").text();

    this.getIdJob = function () {
      return this.idJob;
    };

    this.getJobTitle = function () {
      return this.jobTitle;
    };

    this.getJobReference = function () {
      return this.jobReference;
    };

    this.getNameSectors = function () {
      var sectorsNames = new Array();
      var $sectors = $(data).find("secteurActs").find("secteuract");
      $sectors.each(function () {
        sectorsNames.push($(this).find('name').text());
      });
      return sectorsNames.join('- ');
    };

    this.getNameRegions = function () {
      var regionNames = [];
      var $sectors = $(data).find("regions").find("region");
      $sectors.each(function () {
        regionNames.push($(this).find('regionName').text());
      });
      return regionNames.toString();
    };

    this.getContractTypeName = function () {
      return $(data).find("contratType").find("name").text();
    };
    this.getStudyLevelName = function () {
      return $(data).find("studyLevel").find("studyLevelName").text();
    };

    this.getEnterpriseName = function () {
      return this.enterpriseName;
    };

    this.getEnterpriseLogo = function () {
      return this.enterpriseLogo;
    };
    this.getJobDescription = function () {
      return this.description;
    };
    this.getJobTags = function () {
      return this.tags;
    };
    this.getStandId = function () {
      return this.standId;
    };

  },
  Contact: function (data) {
    this.contactId = $(data).find("idcontact").text();
    this.contactPhoto = $(data).find("photo").text();
    this.contactEmail = $(data).find("email").text();
    this.contactName = $(data).find("nom").text();
    this.contactPhone = $(data).find("phone").text();
    this.jobTitle = $(data).find("jobTitle").text();
    this.enterpriseName = $(data).find("enterpriseName").text();

    this.getContactId = function () {
      return this.contactId;
    };
    this.getContactPhoto = function () {
      return this.contactPhoto;
    };
    this.getContactEmail = function () {
      return this.contactEmail;
    };
    this.getContactName = function () {
      return this.contactName;
    };
    this.getContactPhone = function () {
      return this.contactPhone;
    };

    this.getJobTitle = function () {
      return this.jobTitle;
    };
    this.getEnterpriseName = function () {
      return this.enterpriseName;
    };

  },
  FavoriteContact: function (data) {
    this.contactId = $(data).find("idcontact").text();
    this.contactPhoto = $(data).find("photo").text();
    this.contactEmail = $(data).find("email").text();
    this.contactName = $(data).find("nom").text();
    this.contactPhone = $(data).find("phone").text();
    this.jobTitle = $(data).find("jobTitle").text();
    this.enterpriseName = $(data).find("enterpriseName").text();
    this.standId = $(data).find("standId").text();

    this.getContactId = function () {
      return this.contactId;
    };
    this.getContactPhoto = function () {
      return this.contactPhoto;
    };
    this.getContactEmail = function () {
      return this.contactEmail;
    };
    this.getContactName = function () {
      return this.contactName;
    };
    this.getContactPhone = function () {
      return this.contactPhone;
    };

    this.getJobTitle = function () {
      return this.jobTitle;
    };
    this.getEnterpriseName = function () {
      return this.enterpriseName;
    };
    this.getStandId = function () {
      return this.standId;
    };
  },
  Advice: function (data) {
    this.idAdvice = $(data).find("idAdvice").text();
    this.title = $(data).find("title").text();
    this.standId = $(data).find("standId").text();
    this.enterpriseName = $(data).find("enterpriseName").text();
    this.enterpriseLogo = $(data).find("enterpriseLogo").text();
    this.url = $(data).find("url").text();
    this.content = $(data).find("content").text();

    this.getEnterpriseLogo = function () {
      return this.enterpriseLogo;
    };
    this.getAdviceId = function () {
      return this.idAdvice;
    };
    this.getTitle = function () {
      return this.title;
    };
    this.getStandId = function () {
      return this.standId;
    };
    this.getEnterpriseName = function () {
      return this.enterpriseName;
    };
    this.getUrl = function () {
      return this.url;
    };
    this.getContent = function () {
      return this.content;
    };

  },
  Survey: function (data) {
    this.surveyId = $(data).find("idsondage").text();
    this.surveyTitle = $(data).find("title").text();
    this.answered = $(data).find("answered").text();
    this.questions = new Array();
    var that = this;
    $(data).find('question').each(function () {
      var question = new $.Question($(this));
      that.questions.push(question);
    });

    this.getSurveyId = function () {
      return this.surveyId;
    };
    this.isAnswered = function () {
      return this.answered;
    };
    this.getSurveyTitle = function () {
      return this.surveyTitle;
    };
    this.getQuestions = function () {
      return this.questions;
    };
  },
  Question: function (data) {
    this.questionId = $(data).find("idQuestion").text();
    this.questionTitle = $(data).find("questionContent").text();
    this.responses = new Array();
    var that = this;
    $(data).find('response').each(function () {
      var response = new $.Response($(this));
      that.responses.push(response);
    });
    this.getQuestionId = function () {
      return this.questionId;
    };
    this.getQuestionTitle = function () {
      return this.questionTitle;
    };
    this.getResponses = function () {
      return this.responses;
    };
  },
  Form: function (data) {
    var that = this;
    that.formId = '';
    that.answered = '';
    that.formTitle = '';
    that.formBannerUrl = '';
    that.formDescription = '';
    that.formFields = '';
    that.surveyType = '';
    that.formType = '';

    this.form = function (data) {
      if (data) {
        if ($(data).find("formId"))
          that.formId = $(data).find("formId").text();
        if ($(data).find("answered"))
          that.answered = $(data).find("answered").text();
        if ($(data).find("formTitle"))
          that.formTitle = $(data).find("formTitle").text();
        if ($(data).find("formBannerUrl"))
          that.formBannerUrl = $(data).find("formBannerUrl").text();
        if ($(data).find("formDescription"))
          that.formDescription = $(data).find("formDescription").text();
        that.formFields = new Array();
        $(data).find("formFieldDTO").each(function () {
          var formField = new $.FormField($(this));
          that.formFields.push(formField);
        });
        if ($(data).find("surveyTypeDTO"))
          that.surveyType = new $.SurveyType($(data).find("surveyTypeDTO"));
        if ($(data).find("formTypeDTO"))
          that.formType = new $.FormType($(data).find("formTypeDTO"));
      }
    };
    this.getFormId = function () {
      return that.formId;
    },
    this.getFormTitle = function () {
      return that.formTitle;
    },
    this.getFormBannerUrl = function () {
      return that.formBannerUrl;
    },
    this.getFormDescription = function () {
      return that.formDescription;
    },
    this.getFormFields = function () {
      return that.formFields;
    },
    this.getSurveyType = function () {
      return that.surveyType;
    },
    this.getFormType = function () {
      return that.formType;
    },
    this.setFormId = function (formId) {
      that.formId = formId;
    },
    this.setFormTitle = function (formTitle) {
      that.formTitle = formTitle;
    },
    this.setFormBannerUrl = function (formBannerUrl) {
      that.formBannerUrl = formBannerUrl;
    },
    this.setFormDescription = function (formDescription) {
      that.formDescription = formDescription;
    },
    this.setFormFields = function (formFields) {
      that.formFields = formFields;
    },
    this.setSurveyType = function (surveyType) {
      that.surveyType = surveyType;
    },
    this.setFormType = function (formType) {
      that.formType = formType;
    },
    this.update = function (newData) {
      that.website(newData);
    };

    this.xmlData = function () {
      var xml = '<formDTO>';
      xml += '<formId>' + that.formId + '</formId>';
      xml += '<formTitle>' + htmlEncode(that.formTitle) + '</formTitle>';
      xml += '<formBannerUrl>' + htmlEncode(that.formBannerUrl) + '</formBannerUrl>';
      xml += '<formDescription>' + htmlEncode(that.formDescription) + '</formDescription>';
      xml += '<formFieldDTOList>';
      for (index in that.formFields) {
        var formField = that.formFields[index];
        xml += formField.xmlData();
      }
      xml += '</formFieldDTOList>';
      if (that.surveyType != "")
        xml += that.surveyType.xmlData();
      if (that.formType != "")
        xml += that.formType.xmlData();
      xml += '</formDTO>';
      return xml;
    };

    this.form(data);
  },
  FormField: function (data) {
    var that = this;
    that.formFieldId = '';
    that.formFieldLabel = '';
    that.required = '';
    that.generic = false;
    that.position = '';
    that.field = '';

    this.formField = function (data) {
      if ($(data).find("formFieldId"))
        that.formFieldId = $(data).find("formFieldId").text();
      if ($(data).find("formFieldLabel"))
        that.formFieldLabel = $(data).find("formFieldLabel").text();
      if ($(data).find("required"))
        that.required = $(data).find("required").text();
      if ($(data).find("generic") && $(data).find("generic:first").text() == "true")
        that.generic = true;
      if ($(data).find("position"))
        that.position = $(data).find("position").text();
      if ($(data).find("fieldDTO"))
        that.field = new $.Field($(data).find("fieldDTO"));
    };
    this.getFormFieldId = function () {
      return that.formFieldId;
    },
    this.getFormFieldLabel = function () {
      return that.formFieldLabel;
    },
    this.getRequired = function () {
      return that.required;
    },
    this.getGeneric = function () {
      return that.generic;
    },
    this.getPosition = function () {
      return that.position;
    },
    this.getField = function () {
      return that.field;
    },
    this.setFormFieldId = function (formFieldId) {
      that.formFieldId = formFieldId;
    },
    this.setFormFieldLabel = function (formFieldLabel) {
      that.formFieldLabel = formFieldLabel;
    },
    this.setRequired = function (required) {
      that.required = required;
    },
    this.setGeneric = function (generic) {
      that.generic = generic;
    },
    this.setPosition = function (position) {
      that.position = position;
    },
    this.setField = function (field) {
      that.field = field;
    },
    this.update = function (newData) {
      that.websiteType(newData);
    };

    this.xmlData = function () {
      var xml = '<formFieldDTO>';
      xml += '<formFieldId>' + that.formFieldId + '</formFieldId>';
      xml += '<formFieldLabel>' + htmlEncode(that.formFieldLabel) + '</formFieldLabel>';
      xml += '<required>' + that.required + '</required>';
      xml += '<generic>' + that.generic + '</generic>';
      xml += '<position>' + that.position + '</position>';
      xml += that.field.xmlData();
      xml += '</formFieldDTO>';
      return xml;
    };

    this.formField(data);
  },
  SurveyType: function (data) {
    var that = this;
    that.surveyTypeId = '';
    that.surveyTypeName = '';

    this.website = function (data) {
      if (data) {
        if ($(data).find("surveyTypeId"))
          that.surveyTypeId = $(data).find("surveyTypeId").text();
        if ($(data).find("surveyTypeName"))
          that.surveyTypeName = $(data).find("surveyTypeName").text();
      }
    };
    this.getSurveyTypeId = function () {
      return that.surveyTypeId;
    };
    this.getSurveyTypeName = function () {
      return that.surveyTypeName;
    };
    this.setSurveyTypeId = function (surveyTypeId) {
      that.surveyTypeId = surveyTypeId;
    };
    this.setSurveyTypeName = function (surveyTypeName) {
      that.surveyTypeName = surveyTypeName;
    };
    this.xmlData = function () {
      var xml = '<surveyTypeDTO>';
      xml += '<surveyTypeId>' + that.surveyTypeId + '</surveyTypeId>';
      xml += '<surveyTypeName>' + that.surveyTypeName + '</surveyTypeName>';
      xml += '</surveyTypeDTO>';
      return xml;
    };

    this.website(data);
  },
  FormType: function (data) {
    var that = this;
    that.formTypeId = '';
    that.formTypeName = '';

    this.website = function (data) {
      if (data) {
        if ($(data).find("formTypeId"))
          that.formTypeId = $(data).find("formTypeId").text();
        if ($(data).find("formTypeName"))
          that.formTypeName = $(data).find("formTypeName").text();
      }
    };
    this.getFormTypeId = function () {
      return that.formTypeId;
    };
    this.getFormTypeName = function () {
      return that.formTypeName;
    };
    this.setFormTypeId = function (formTypeId) {
      that.formTypeId = formTypeId;
    };
    this.setFormTypeName = function (formTypeName) {
      that.formTypeName = formTypeName;
    };
    this.xmlData = function () {
      var xml = '<formTypeDTO>';
      xml += '<formTypeId>' + that.formTypeId + '</formTypeId>';
      xml += '<formTypeName>' + that.formTypeName + '</formTypeName>';
      xml += '</formTypeDTO>';
      return xml;
    };

    this.website(data);
  },
  InputType: function (data) {
    var that = this;
    that.inputTypeId = '';
    that.inputTypeName = '';

    this.inputType = function (data) {
      if ($(data).find("inputTypeId"))
        that.inputTypeId = $(data).find("inputTypeId").text();
      if ($(data).find("inputTypeName"))
        that.inputTypeName = $(data).find("inputTypeName").text();
    };
    this.getInputTypeId = function () {
      return that.inputTypeId;
    };
    this.getInputTypeName = function () {
      return that.inputTypeName;
    };
    this.setInputTypeId = function (inputTypeId) {
      that.inputTypeId = inputTypeId;
    };
    this.setInputTypeName = function (inputTypeName) {
      that.inputTypeName = inputTypeName;
    };
    this.update = function (newData) {
      that.websiteType(newData);
    };

    this.xmlData = function () {
      var xml = '<inputTypeDTO>';
      xml += '<inputTypeId>' + that.inputTypeId + '</inputTypeId>';
      xml += '<inputTypeName>' + htmlEncode(that.inputTypeName) + '</inputTypeName>';
      xml += '</inputTypeDTO>';
      return xml;
    };

    this.inputType(data);
  },
  Attend: function (data) {
    var that = this;
    this.attendId = 0;
    this.attendsData = '';

    this.attend = function (data) {
      if (data) {
        if ($(data).find("attendId"))
          that.attendId = $(data).find("attendId").text();
        that.attendsData = new Array();
        $(data).find("attendDataDTO").each(function () {
          var attendData = new $.AttendData($(this));
          that.attendsData.push(attendData);
        });
      }
    };
    this.getAttendId = function () {
      return that.attendId;
    },
    this.getAttendsData = function () {
      return that.attendsData;
    },
    this.setAttendId = function (attendId) {
      that.attendId = attendId;
    },
    this.setAttendsData = function (attendsData) {
      that.attendsData = attendsData;
    },
    this.xmlData = function () {
      var xml = '<attendDTO>';
      xml += '<attendId>' + that.attendId + '</attendId>';
      xml += '<attendDataDTOList>';
      for (index in that.attendsData) {
        var attendData = that.attendsData[index];
        xml += attendData.xmlData();
      }
      xml += '</attendDataDTOList>';
      xml += '</attendDTO>';
      return xml;
    };

    this.attend(data);

  },
  AttendData: function (data) {
    var that = this;
    this.attendDataId = 0;
    this.form = 0;
    this.field = 0;
    this.value = '';

    this.attendData = function (data) {
      if (data) {
        if ($(data).find("attendDataId"))
          that.attendDataId = $(data).find("attendDataId").text();
        if ($(data).find("form"))
          that.form = new $.Form($(data).find("formDTO"));
        if ($(data).find("field"))
          that.field = new $.Field($(data).find("fieldDTO"));
        if ($(data).find("value"))
          that.value = $(data).find("value").text();
      }
    };
    this.getAttendDataId = function () {
      return that.attendDataId;
    },
    this.getForm = function () {
      return that.form;
    },
    this.getField = function () {
      return that.field;
    },
    this.getValue = function () {
      return that.value;
    },
    this.setAttendDataId = function (attendDataId) {
      that.attendDataId = attendDataId;
    },
    this.setForm = function (form) {
      that.form = form;
    },
    this.setField = function (field) {
      that.field = field;
    },
    this.setValue = function (value) {
      that.value = value;
    },
    this.update = function (newData) {
      that.manager(newData);
    };

    this.xmlData = function () {
      var xml = '<attendDataDTO>';
      xml += '<attendDataId>' + that.attendDataId + '</attendDataId>';
      xml += that.form.xmlData();
      xml += that.field.xmlData();
      xml += '<value>' + htmlEncode(that.value) + '</value>';
      xml += '</attendDataDTO>';
      return xml;
    };

    this.attendData(data);
  },
  Field: function (data) {
    var that = this;
    that.fieldId = '';
    that.fieldName = '';
    that.inputType = '';
    that.fieldValue = '';

    this.field = function (data) {
      if ($(data).find("fieldId"))
        that.fieldId = $(data).find("fieldId").text();
      if ($(data).find("fieldName"))
        that.fieldName = $(data).find("fieldName").text();
      if ($(data).find("fieldValue"))
        that.fieldName = $(data).find("fieldValue").text();
      if ($(data).find("inputTypeDTO"))
        that.inputType = new $.InputType($(data).find("inputTypeDTO"));
    };
    this.getFieldId = function () {
      return that.fieldId;
    },
    this.getFieldName = function () {
      return that.fieldName;
    },
    this.getInputType = function () {
      return that.inputType;
    },
    this.getFieldValue = function () {
      return that.fieldValue;
    },
    this.setFieldId = function (fieldId) {
      that.fieldId = fieldId;
    },
    this.setFieldName = function (fieldName) {
      that.fieldName = fieldName;
    },
    this.setInputType = function (inputType) {
      that.inputType = inputType;
    },
    this.setFieldValue = function (fieldValue) {
      that.fieldValue = fieldValue;
    },
    this.update = function (newData) {
      that.field(newData);
    };

    this.xmlData = function () {
      var xml = '<fieldDTO>';
      xml += '<fieldId>' + that.fieldId + '</fieldId>';
      xml += '<fieldName>' + htmlEncode(that.fieldName) + '</fieldName>';
      xml += '<fieldValue>' + that.fieldValue + '</fieldValue>';
      xml += that.inputType.xmlData();
      xml += '</fieldDTO>';
      return xml;
    };

    this.field(data);
  },
  JobApplication: function (data) {
    this.jobOfferId = $(data).find("jobOfferId").text();
    this.jobApplicationId = $(data).find("jobApplicationId").text();
    this.enterpriseName = $(data).find("enterpriseName").text();
    this.enterpriseLogo = $(data).find("enterpriseLogo").text();
    this.response = unescape($(data).find("response").text());
    this.jobOfferTitle = $(data).find("jobOfferTitle").text();
    this.standId = $(data).find("standId").text();
    this.candidatureDate = parseDateCareer($(data).find("candidatureDate").text());
    this.candidatureDate = this.candidatureDate.toLocaleDateString(LANGAUGETAGLOCALELABEL.replace('_', '-'));
  },
  UnsolicitedJobApplication: function (data) {
    this.jobApplicationId = $(data).find("jobApplicationId").text();
    this.enterpriseName = $(data).find("enterpriseName").text();
    this.enterpriseLogo = $(data).find("enterpriseLogo").text();
    this.response = unescape($(data).find("response").text());
    this.standId = $(data).find("standId").text();
    this.jobOfferTitle = "";
    this.candidatureDate = parseDateCareer($(data).find("candidatureDate").text());
    this.candidatureDate = this.candidatureDate.toLocaleDateString(LANGAUGETAGLOCALELABEL.replace('_', '-'));
  },
  Conversation: function (data) {
    var that = this;
    this.archived = $(data).find("archived").text();
    this.deleted = $(data).find("deleted").text();
    this.idConversation = $(data).find("idConversation").text();
    this.lastUpdate = $(data).find("lastUpdate").text();
    this.subject = $(data).find("subject").text();
    this.unread = $(data).find("unread").text();
    this.sender = new $.UserProfile($(data).find("sender").last());
    this.receiver = new $.UserProfile($(data).find("receiver")
            .last());
    this.enterpriseName = $($(data).find("stand")).find("name").text();
    this.candidateName = $(data).find("candidateName").text();
    this.messageFolder = new $.MessageFolder($(data).find(
            "messageFolder"));
    this.messages = [];
    $.each($(data).find("message"), function (index, value) {
      that.messages.push(new $.Message(value));
    });
  },
  Message: function (data) {
    var that = this;
    this.content = $(data).find("content").text();
    this.idmessage = $(data).find("idmessage").text();
    this.sender = new $.UserProfile($(data).find("sender"));
    this.receiver = new $.UserProfile($(data).find("receiver"));
    this.messageDate = $(data).find("messageDate").text();
    this.subject = $(data).find("subject").text();
    this.unread = $(data).find("unread").text();
    this.from = $(data).find("from").text();
    
    this.xmlData = function () {
      var xml = '<message>';
      xml += '<from>' + htmlEncode(that.from) + '</from>';
      xml += '<subject>' + htmlEncode(that.subject) + '</subject>';
      xml += '<content>' + htmlEncode(that.content) + '</content>';
      xml += '</message>';
      return xml;
    };
  },
  MessageFolder: function (data) {
    this.idMessageFolder = $(data).find("idMessageFolder").text();
    this.numberMessages = $(data).find("numberMessages").text();
  },

	EventAgenda: function (data) {
    var that = this;
    this.idEvent = 0;
    this.eventTitle = "";
    this.recurrent = "";
    this.eventType = 0;
    this.eventDescription = "";
    this.beginDate = null;
    this.endDate = null;
    this.enterpriseName = null;
    this.enterpriseLogo = null;
    this.stand = null;
    this.eventTypeId = null;
    
    
    this.eventAgenda = function (data) {
      if ($(data).find("idEvent"))
      	that.idEvent = $(data).find("idEvent").text();
      if ($(data).find("eventTitle"))
      	that.eventTitle = $(data).find("eventTitle").text();
      if ($(data).find("recurrent"))
      	that.recurrent = $(data).find("recurrent").text();
      if ($(data).find("idEventType"))
      	that.eventType = $(data).find("idEventType").val();
      if ($(data).find("eventDescription"))
      	that.eventDescription = $(data).find("eventDescription").text();
      if ($(data).find("beginDate"))
      	that.beginDate = $(data).find("beginDate").text();
      if ($(data).find("endDate"))
      	that.endDate = $(data).find("endDate").text();
      if ($(data).find("enterpriseName"))
      	that.enterpriseName = $(data).find("enterpriseName").text();
      if ($(data).find("enterpriseLogo"))
      	that.enterpriseLogo = $(data).find("enterpriseLogo").text();
      if ($(data).find("idEventType"))
      	that.eventTypeId = $(data).find("idEventType").text();
      if ($(data).find("standDTO"))
        that.stand = new $.Stand($(data).find("standDTO"));
    };
    
    this.getIdEvent = function () {
        return that.idEvent;
    };
    this.setIdEvent = function (idEvent) {
        that.idEvent = idEvent;
    };
    this.getEventTitle = function () {
        return that.eventTitle;
    };
    this.setEventTitle = function (eventTitle) {
        that.eventTitle = eventTitle;
    };
    this.getRecurrent = function () {
        return that.recurrent;
    };
    this.setRecurrent = function (recurrent) {
        that.recurrent = recurrent;
    };
    this.getEventType = function () {
        return that.eventType;
    };
    this.setEventType = function (eventType) {
        that.eventType = eventType;
    };
    this.getEventDescription = function () {
        return that.eventDescription;
    };
    this.getStand = function () {
	     return that.stand;
	};
    this.setEventDescription = function (eventDescription) {
        that.eventDescription = eventDescription;
    };
    this.getBeginDate = function () {
        return that.beginDate;
    };
    this.setBeginDate = function (beginDate) {
        that.beginDate = beginDate;
    };
    this.getEndDate = function () {
        return that.endDate;
    };
    this.setEndDate = function (endDate) {
        that.endDate = endDate;
    };
    this.getEnterpriseName = function () {
        return that.enterpriseName;
    };
    this.setEnterpriseName = function (enterpriseName) {
        that.enterpriseName = enterpriseName;
    };
    this.getEnterpriseLogo = function () {
        return that.enterpriseLogo;
    };
    this.setEnterpriseLogo = function (enterpriseLogo) {
        that.enterpriseLogo = enterpriseLogo;
    };
    this.setStand = function (stand) {
	    that.stand = stand;
	};
    this.getEventTypeId = function () {
    	return that.eventTypeId;
	};
	this.setEventTypeId = function (eventTypeId) {
		that.eventTypeId = eventTypeId;
	};
  this.xmlData = function () {
        var xml = "<event><idEvent>" + that.idEvent + "</idEvent><eventTitle>"
                + htmlEncode(that.eventTitle)
                + "</eventTitle><recurrent>"
                + "</recurrent><idEventType>"
                + that.eventType
                + "</idEventType><eventDescription>"
                + htmlEncode(that.eventDescription)
                + "</eventDescription><beginDate>"
                + getAgendaUTCDateEvent(that.beginDate)
                + "</beginDate><endDate>"
                + getAgendaUTCDateEvent(that.endDate)
                + "</endDate></event>";
        return xml;
    };
    
    this.eventAgenda(data);
  },
	
  TChatEvent: function (data) {
    var that = this;
    that.candidate = '';
    that.event = '';

    this.tchatEvent = function (data) {
      if ($(data).find("candidateDTO"))
        that.candidate = new $.Candidate($(data).find("candidateDTO"));
      if ($(data).find("eventDTO"))
        that.event = new $.EventAgenda($(data).find("eventDTO"));
    };
    this.getCandidate = function () {
      return that.candidate;
    },
    this.getEvent = function () {
      return that.event;
    },
    this.setCandidate = function (candidate) {
      that.candidate = candidate;
    },
    this.setEvent = function (event) {
      that.event = event;
    },
    this.update = function (newData) {
      that.chatEvent(newData);
    };

    this.xmlData = function () {
      var xml = '<chatEventDTO>';
      if(that.candidate != '') xml += that.candidate.xmlData();
      if(that.event != '') xml += that.event.xmlData();
      xml += '</chatEventDTO>';
      return xml;
    };

    this.tchatEvent(data);
  },

	
  GroupStand: function (data) {
    var that = this;
    that.groupStandId = 0;
    that.groupStandName = 0;

    this.groupStand = function (data) {
      if ($(data).find("idGroup"))
        that.groupStandId = $(data).find("idGroup").text();
      if ($(data).find("groupName"))
        that.groupStandName = $(data).find("groupName").text();
    };
    this.getGroupStandId = function () {
      return that.groupStandId;
    },
    this.getGroupStandName = function () {
      return that.groupStandName;
    },
    this.setGroupStandId = function (groupStandId) {
      that.groupStandId = groupStandId;
    },
    this.setGroupStandName = function (groupStandName) {
      that.groupStandName = groupStandName;
    },
    this.update = function (newData) {
      that.groupStand(newData);
    };

    this.xmlData = function () {
      var xml = '<groupStandDTO>';
      xml += '<idGroup>' + that.groupStandId + '</idGroup>';
      xml += '<groupName>' + htmlEncode(that.groupStandName) + '</groupName>';
      xml += '</groupStandDTO>';
      return xml;
    };

    this.groupStand(data);
  },

  City : function(data) {
    var that = this;
    that.cityId = '';
    that.cityName = '';
    that.state = '';
    this.city = function(data) {
      if ($(data).find("cityId"))
        that.cityId = $(data).find("cityId").text();
      if ($(data).find("cityName"))
        that.cityName = $(data).find("cityName").text();
      if ($(data).find("stateDTO"))
        that.state = new $.State($(data).find("stateDTO"));
    };
    this.getCityId = function() {
      return that.cityId;
    };
    this.getCityName = function() {
      return that.cityName;
    };
    this.getState = function() {
      return that.state;
    };
    this.setCityId = function(cityId) {
      that.cityId = cityId;
    };
    this.setCityName = function(cityName) {
      that.cityName = cityName;
    };
    this.setState = function(state) {
      that.state = state;
    };
    this.update = function(newData) {
      that.city(newData);
    };
    this.xmlData = function() {
      var xml = '<cityDTO>';
      xml += '<cityId>' + that.cityId + '</cityId>';
      xml += '<cityName>' + htmlEncode(that.cityName) + '</cityName>';
      if (that.state != "") {
        xml += that.state.xmlData();
      }
      xml += '</cityDTO>';
      return xml;
    };
    this.city(data);
  },

  Country : function(data) {
    var that = this;
    that.countryId = '';
    that.countryName = '';
    this.country = function(data) {
      if ($(data).find("countryId"))
        that.countryId = $(data).find("countryId").text();
      if ($(data).find("countryName"))
        that.countryName = $(data).find("countryName").text();
    };
    this.getCountryId = function() {
      return that.countryId;
    };
    this.getCountryName = function() {
      return that.countryName;
    };
    this.setCountryId = function(countryId) {
      that.countryId = countryId;
    };
    this.setCountryName = function(countryName) {
      that.countryName = countryName;
    };
    this.update = function(newData) {
      that.country(newData);
    };
    this.xmlData = function() {
      var xml = '<countryDTO>';
      xml += '<countryId>' + that.countryId + '</countryId>';
      xml += '<countryName>' + htmlEncode(that.countryName) + '</countryName>';
      xml += '</countryDTO>';
      return xml;
    };
    this.country(data);
  },

  State : function(data) {
    var that = this;
    that.stateId = '';
    that.stateName = '';
    that.country = '';
    this.state = function(data) {
      if ($(data).find("stateId"))
        that.stateId = $(data).find("stateId").text();
      if ($(data).find("stateName"))
        that.stateName = $(data).find("stateName").text();
      if ($(data).find("countryDTO"))
        that.country = new $.Country($(data).find("countryDTO"));
    };
    this.getStateId = function() {
      return that.stateId;
    };
    this.getStateName = function() {
      return that.stateName;
    };
    this.getCountry = function() {
      return that.country;
    };
    this.setStateId = function(stateId) {
      that.stateId = stateId;
    };
    this.setStateName = function(stateName) {
      that.stateName = stateName;
    };
    this.setCountry = function(country) {
      that.country = country;
    };
    this.update = function(newData) {
      that.state(newData);
    };
    this.xmlData = function() {
      var xml = '<stateDTO>';
      xml += '<stateId>' + that.stateId + '</stateId>';
      xml += '<stateName>' + htmlEncode(that.stateName) + '</stateName>';
      if (that.country != "") {
        xml += that.country.xmlData();
      }
      xml += '</stateDTO>';
      return xml;
    };
    this.state(data);
  },

  Product: function(data){
    var that = this;
    that.productId='';
    that.productTitle='';
    that.productDescription='';
    that.stand='';
    that.productImage='';
    that.resources='';
    
    that.product = function(data){
      if(data){
        if($(data).find("productId")) 
          that.productId = $(data).find("productId").text();
        if($(data).find("productTitle")) 
          that.productTitle = $(data).find("productTitle").text();
        if($(data).find("productDescription")) 
          that.productDescription = $(data).find("productDescription").text();
        if($(data).find("productImage")) 
          that.productImage = $(data).find("productImage").text();
        if ($(data).find("standDTO"))
        	that.stand = new $.Stand($(data).find("standDTO"));
        that.resources = new Array(); 
        $(data).find("resourceDTO").each(function(){  
          var resource = new $.Resource($(this));             
          that.resources.push(resource);  
        });
      }
    };
    
    this.getProductId= function(){  
      return that.productId;
    };
    
    this.getProductTitle = function(){  
      return that.productTitle;
    };
    
    this.getProductDescription = function(){  
      return that.productDescription;
    };
    
    this.getProductImage = function(){  
      return that.productImage;
    };

    this.getStand = function(){ 
      return that.stand;
    };
    
    this.getResources = function(){ 
      return that.resources;
    };
    
    this.setProductId = function(productId){
      that.productId = productId;
    };
    
    this.setProductTitle = function(productTitle){
      that.productTitle = productTitle;
    };
    
    this.setProductDescription = function(productDescription){
      that.productDescription = productDescription;
    };
     
    this.setProductImage = function(productImage){
      that.productImage = productImage;
    };

    this.setStand = function(stand){
      that.stand = stand;
    };
    
    this.setResources = function(resources){
      that.resources = resources;
    };
    
    this.update = function(newData){
      that.product(newData);
    };
    
    this.xmlData = function(){
      var xml='<productDTO>';
      xml+='<productId>'+that.productId+'</productId>';
      xml+='<productTitle>'+htmlEncode($.trim(that.productTitle))+'</productTitle>';
      xml+='<productDescription>'+htmlEncode($.trim(that.productDescription))+'</productDescription>';
      xml+='<productImage>'+htmlEncode($.trim(that.productImage))+'</productImage>';
      if(that.resources != ''){
        xml+='<resourceDTOList>';
        for (var index in that.resources) {
          var resource = that.resources[index];
          xml+=resource.xmlData();
        }
        xml+='</resourceDTOList>';
      }
      xml+='</productDTO>';
      return xml;
    };
    
    this.product(data);
  },  

  Resource: function(data){
		var that = this;
		that.resourceId='';
		that.resourceName='';
		that.creationDate='';
		that.resourceType='';
		that.resourceUrl='';
		this.resource = function(data){
			if(data){
				if($(data).find("resourceId")) that.resourceId=$(data).find("resourceId").text();
				if($(data).find("resourceName")) that.resourceName=$(data).find("resourceName").text();
				if($(data).find("dateCreation")) that.creationDate=that.dateFromString($(data).find("creationDate").text()).toString('dddd, MMMM ,yyyy'); 
				if($(data).find("resourceTypeDTO")) that.resourceType=new $.ResourceType($(data).find("resourceTypeDTO"));
				if($(data).find("resourceUrl")) that.resourceUrl=$(data).find("resourceUrl").text();
			}
		};
		
		this.dateFromString= function(s) {
			  var bits = s.split(/[-T:+]/g);
			  var d = new Date(bits[0], bits[1]-1, bits[2]);
			  d.setHours(bits[3], bits[4], bits[5]);
	
			  // Get supplied time zone offset in minutes
			  var offsetMinutes = bits[6] * 60 + Number(bits[7]);
			  var sign = /\d\d-\d\d:\d\d$/.test(s)? '-' : '+';
	
			  // Apply the sign
			  offsetMinutes = 0 + (sign == '-'? -1 * offsetMinutes : offsetMinutes);
	
			  // Apply offset and local timezone
			  d.setMinutes(d.getMinutes() - offsetMinutes - d.getTimezoneOffset());
	
			  // d is now a local time equivalent to the supplied time
			  return d;
		};
		
		this.getResourceId= function(){	
			return that.resourceId;
		};
		
		this.getResourceName = function(){	
			return that.resourceName;
		};
		
		this.getResourceUrl = function(){	
			return that.resourceUrl;
		};
		
		this.getCreationDate = function(){	
			return that.creationDate;
		};
	
		this.getResourceType = function(){	
			return that.resourceType;
		};
		
		this.setResourceId = function(resourceId){
			that.resourceId=resourceId;
		};
		
		this.setResourceName = function(resourceName){
			that.resourceName=resourceName;
		};
		
		this.setResourceUrl = function(resourceUrl){
			that.resourceUrl=resourceUrl;
		};
		
		this.setCreationDate = function(creationDate){
			that.creationDate=creationDate;
		};
		
		this.setResourceType = function(resourceType){
			that.resourceType=resourceType;
		};
		
		this.update = function(newData){
			that.resource(newData);
		};
		
		this.xmlData = function(){
			var xml='<resourceDTO>';
			xml+='<resourceId>'+that.resourceId+'</resourceId>';
			xml+='<resourceName>'+htmlEncode($.trim(that.resourceName))+'</resourceName>';
			xml+='<resourceUrl>'+htmlEncode($.trim(that.resourceUrl))+'</resourceUrl>';
			xml+=that.resourceType.xmlData();
			xml+='</resourceDTO>';
			return xml;
		};
		
		this.resource(data);
	},

	ResourceType: function(data){
		var that = this;
		that.resourceTypeId='';
		that.resourceTypeName='';
		that.resourceType = function(data){
			if(data){
				if($(data).find("resourceTypeId")) that.resourceTypeId=$(data).find("resourceTypeId").text();
				if($(data).find("resourceName")) that.resourceTypeName=$(data).find("resourceTypeName").text();
			}
		};
		
		this.getResourceTypeId= function(){	
			return that.resourceTypeId;
		};
		
		this.getResourceTypeName = function(){	
			return that.resourceTypeName;
		};
		
		this.setResourceTypeId = function(resourceTypeId){
			that.resourceTypeId=resourceTypeId;
		};
		
		this.setResourceTypeName = function(resourceTypeName){
			that.resourceTypeName=resourceTypeName;
		};
		
		this.update = function(newData){
			that.resourceType(newData);
		};
		
		this.xmlData = function(){
			var xml='<resourceTypeDTO>';
			xml+='<resourceTypeId>'+that.resourceTypeId+'</resourceTypeId>';
			xml+='<resourceTypeName>'+htmlEncode($.trim(that.resourceTypeName))+'</resourceTypeName>';
			xml+='</resourceTypeDTO>';
			return xml;
		};
		
		this.resourceType(data);
	},
	QuestionInfo: function(data){
		var that = this;
		that.questionInfoId = '';
		that.questionInfoTitle = '';
		that.question = '';
		that.response = '';
		that.questionInfoUrl = '';
		that.questionInfoDate = '';
		that.unread = false;
		that.candidate = null;
		that.product = null;
		
		this.questionInfo = function(data){
			if(data){
				if($(data).find("questionInfoId")) 
					that.questionInfoId = $(data).find("questionInfoId").text();
				if($(data).find("questionInfoTitle")) 
					that.questionInfoTitle = $(data).find("questionInfoTitle").text();
				if($(data).find("question")) 
					that.question = $(data).find("question").text();
				if($(data).find("response")) 
					that.response = $(data).find("response").text();
				if($(data).find("questionInfoUrl")) 
					that.questionInfoUrl = $(data).find("questionInfoUrl").text();
				if($(data).find("questionInfoDate")) 
					that.questionInfoDate=that.dateFromString($(data).find("questionInfoDate").text()); 
				if($(data).find("unread") && $(data).find("unread").text() == "true") 
					that.unread = true;
				if($(data).find("candidateDTO")) 
					that.candidate=new $.Candidate($(data).find("candidateDTO"));
				if($(data).find("productDTO")) 
					that.product=new $.Product($(data).find("productDTO"));
			}
		};
		
		this.dateFromString= function(s) {
			  var bits = s.split(/[-T:+]/g);
			  var d = new Date(bits[0], bits[1]-1, bits[2]);
			  d.setHours(bits[3], bits[4], bits[5]);

			  // Get supplied time zone offset in minutes
			  var offsetMinutes = bits[6] * 60 + Number(bits[7]);
			  var sign = /\d\d-\d\d:\d\d$/.test(s)? '-' : '+';

			  // Apply the sign
			  offsetMinutes = 0 + (sign == '-'? -1 * offsetMinutes : offsetMinutes);

			  // Apply offset and local timezone
			  d.setMinutes(d.getMinutes() - offsetMinutes - d.getTimezoneOffset());

			  // d is now a local time equivalent to the supplied time
			  return d;
		};
		
		this.getQuestionInfoId= function(){	
			return that.questionInfoId;
		};
		
		this.getQuestionInfoTitle= function(){	
			return that.questionInfoTitle;
		};
		
		this.getQuestion = function(){	
			return that.question;
		};
		
		this.getResponse = function(){	
			return that.response;
		};
		
		this.getQuestionInfoUrl = function(){	
			return that.questionInfoUrl;
		};
		
		this.getQuestionInfoDate = function(){	
			return that.questionInfoDate;
		};

		this.getUnread = function(){	
			return that.unread;
		};

		this.getCandidate = function(){	
			return that.candidate;
		};

		this.getProduct = function(){	
			return that.product;
		};
		
		this.setQuestionInfoId = function(questionInfoId){
			that.questionInfoId=questionInfoId;
		};
		
		this.setQuestionInfoTitle = function(questionInfoTitle){
			that.questionInfoTitle=questionInfoTitle;
		};
		
		this.setQuestion = function(question){
			that.question=question;
		};
		
		this.setResponse = function(response){
			that.response=response;
		};
		
		this.setQuestionInfoUrl = function(questionInfoUrl){
			that.questionInfoUrl=questionInfoUrl;
		};
		
		this.setQuestionInfoDate = function(questionInfoDate){
			that.questionInfoDate=questionInfoDate;
		};
		
		this.setUnread = function(unread){
			that.unread=unread;
		};
		
		this.setCandidate = function(candidate){
			that.candidate=candidate;
		};
		
		this.setProduct = function(product){
			that.product=product;
		};
		
		this.update = function(newData){
			that.questionInfo(newData);
		};
		
		this.xmlData = function(){
			var xml='<questionInfoDTO>';
			xml+='<questionInfoId>'+that.questionInfoId+'</questionInfoId>';
			xml+='<questionInfoTitle>'+htmlEncode($.trim(that.questionInfoTitle))+'</questionInfoTitle>';
			xml+='<question>'+htmlEncode($.trim(that.question))+'</question>';
			xml+='<response>'+htmlEncode($.trim(that.response))+'</response>';
			xml+='<questionInfoUrl>'+htmlEncode($.trim(that.questionInfoUrl))+'</questionInfoUrl>';
			xml+='<unread>'+htmlEncode($.trim(that.unread))+'</unread>';
			if(that.candidate != null) 
				xml+=that.candidate.xmlData();
			if(that.product != null) 
				xml+=that.product.xmlData();
			xml+='</questionInfoDTO>';
			return xml;
		};
		
		this.questionInfo(data);
	},
	
	Language: function(data){
		var that = this;
		that.languageId='';
		that.languageName='';
		
		that.language = function(data){
			if(data){
				if($(data).find("idlangage")) 
					that.languageId=$(data).find("idlangage").text();
				if($(data).find("name")) 
					that.languageName=$(data).find("name").text();
			}
		};
		
		this.getLanguageId= function(){	
			return that.languageId;
		};
		
		this.getLanguageName = function(){	
			return that.languageName;
		};
		
		this.setLanguageId = function(languageId){
			that.languageId=languageId;
		};
		
		this.setLanguageName = function(languageName){
			that.languageName=languageName;
		};
		
		this.update = function(newData){
			that.language(newData);
		};
		
		this.xmlData = function(){
			var xml='<languageDTO>';
			xml+='<idlangage>'+that.languageId+'</idlangage>';
			xml+='<name>'+htmlEncode($.trim(that.languageName))+'</name>';
			xml+='</languageDTO>';
			return xml;
		};
		
		this.language(data);
	},

  SEO: function(data){
      var that = this;
      that.seoId='';
      that.seoTitle='';
      that.seoMetadata='';
      that.seoKeywords='';
      that.seoFirstLink='';
      that.seoSecondeLink='';
      that.seoThirdLink='';
      
      that.seo = function(data){
        if(data){
          if($(data).find("seoId")) 
            that.seoId=$(data).find("seoId").text();
          if($(data).find("seoTitle")) 
            that.seoTitle=$(data).find("seoTitle").text();
          if($(data).find("seoMetadata")) 
            that.seoMetadata=$(data).find("seoMetadata").text();
          if($(data).find("seoKeywords")) 
            that.seoKeywords=$(data).find("seoKeywords").text();
          if($(data).find("seoFirstLink")) 
            that.seoFirstLink=$(data).find("seoFirstLink").text();
          if($(data).find("seoSecondeLink")) 
            that.seoSecondeLink=$(data).find("seoSecondeLink").text();
          if($(data).find("seoThirdLink")) 
            that.seoThirdLink=$(data).find("seoThirdLink").text();
        }
      };
      
      this.getSeoId= function(){  
        return that.seoId;
      };
      
      this.getSeoTitle = function(){  
        return that.seoTitle;
      };
      
      this.getSeoMetadata = function(){ 
        return that.seoMetadata;
      };
      
      this.getSeoKeywords = function(){ 
        return that.seoKeywords;
      };
      
      this.getSeoFirstLink = function(){  
        return that.seoFirstLink;
      };
      
      this.getSeoSecondeLink = function(){  
        return that.seoSecondeLink;
      };
      
      this.getSeoThirdLink = function(){  
        return that.seoThirdLink;
      };
      
      this.setSeoId = function(seoId){
        that.seoId=seoId;
      };
      
      this.setSeoTitle = function(seoTitle){
        that.seoTitle=seoTitle;
      };
      
      this.setSeoMetadata = function(seoMetadata){
        that.seoMetadata=seoMetadata;
      };
      
      this.setSeoKeywords = function(seoKeywords){
        that.seoKeywords=seoKeywords;
      };
      
      this.setSeoFirstLink = function(seoFirstLink){
        that.seoFirstLink=seoFirstLink;
      };
        
      this.setSeoSecondeLink = function(seoSecondeLink){
        that.seoSecondeLink=seoSecondeLink;
      };
        
      this.setSeoThirdLink = function(seoThirdLink){
        that.seoThirdLink=seoThirdLink;
      };
            
      this.update = function(newData){
        that.seo(newData);
      };
      
      this.xmlData = function(){
        var xml='<seoDTO>';
        xml+='<seoId>'+that.seoId+'</seoId>';
        xml+='<seoTitle>'+htmlEncode($.trim(that.seoTitle))+'</seoTitle>';
        xml+='<seoMetadata>'+htmlEncode($.trim(that.seoMetadata))+'</seoMetadata>';
        xml+='<seoKeywords>'+htmlEncode($.trim(that.seoKeywords))+'</seoKeywords>';
        xml+='<seoFirstLink>'+htmlEncode($.trim(that.seoFirstLink))+'</seoFirstLink>';
        xml+='<seoSecondeLink>'+htmlEncode($.trim(that.seoSecondeLink))+'</seoSecondeLink>';
        xml+='<setSeoThirdLink>'+htmlEncode($.trim(that.setSeoThirdLink))+'</setSeoThirdLink>';
        xml+='</seoDTO>';
        return xml;
      };
      
      this.seo(data);
    },
	
    Email: function(data){
      var that = this;
      that.emailId='';
      that.address='';
      that.fullName='';
      that.subject='';
      that.body='';
      
      that.email = function(data){
        if(data){
          if($(data).find("emailId")) 
            that.languageId=$(data).find("emailId").text();
          if($(data).find("address")) 
            that.address=$(data).find("address").text();
          if($(data).find("fullName")) 
            that.fullName=$(data).find("fullName").text();
          if($(data).find("subject")) 
            that.subject=$(data).find("subject").text();
          if($(data).find("body")) 
            that.body=$(data).find("body").text();
        }
      };
      
      this.getEmailId= function(){ 
        return that.emailId;
      };
      
      this.getAddress = function(){  
        return that.address;
      };
      
      this.getFullName = function(){  
        return that.fullName;
      };
      
      this.getSubject = function(){  
        return that.subject;
      };
      
      this.getBody = function(){  
        return that.body;
      };
            
      this.setEmailId = function(emailId){
        that.emailId=emailId;
      };
      
      this.setAddress = function(address){
        that.address=address;
      };
      
      this.setFullName = function(fullName){
        that.fullName=fullName;
      };
      
      this.setSubject = function(subject){
        that.subject=subject;
      };
      
      this.setBody = function(body){
        that.body=body;
      };
      
      this.update = function(newData){
        that.email(newData);
      };
      
      this.xmlData = function(){
        var xml='<emailDTO>';
        xml+='<emailId>'+that.emailId+'</emailId>';
        xml+='<address>'+htmlEncode($.trim(that.address))+'</address>';
        xml+='<fullName>'+htmlEncode($.trim(that.fullName))+'</fullName>';
        xml+='<subject>'+htmlEncode($.trim(that.subject))+'</subject>';
        xml+='<body>'+htmlEncode($.trim(that.body))+'</body>';
        xml+='</emailDTO>';
        return xml;
      };
      
      this.email(data);
    },

});

