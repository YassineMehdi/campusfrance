(function ($) {
    $(function () {
        var jcarousel = $('.jcarousel');
        jcarousel

                .jcarousel({
                    /*wrap: 'circular'*/
                });


        reloadSponsorJcarousel();

        $('#info-stand .menu-stand .jcarousel-control-prev')
                .jcarouselControl({
                    target: '-=1'
                });

        $('#info-stand .menu-stand .jcarousel-control-next')
                .jcarouselControl({
                    target: '+=1'
                });

        $('.jcarousel-pagination')
                .on('jcarouselpagination:active', 'a', function () {
                    $(this).addClass('active');
                })
                .on('jcarouselpagination:inactive', 'a', function () {
                    $(this).removeClass('active');
                })
                .on('click', function (e) {
                    e.preventDefault();
                })
                .jcarouselPagination({
                    perPage: 1,
                    item: function (page) {
                        return '<a href="#' + page + '">' + page + '</a>';
                    }
                });
    });
})(jQuery);

