var TTLLOADED = 3 * 60 * 1000; // 3 minutes
var MAX_TRY_TO_RELOAD = 3;
var isAllScriptsLoads = false;

function checkLoaded() {
    if (!isAllScriptsLoads)
        catchTryAgainException();
}

function catchTryAgainException() {
    var tryToReload = $.cookie("tryToReload");
    if (tryToReload == null)
        tryToReload = 0;
    tryToReload++;
    if (tryToReload < MAX_TRY_TO_RELOAD) {
        $.cookie("tryToReload", tryToReload);
        window.location.reload();
    } else {
        $.cookie("tryToReload", 0);
        $("#popupProblemChangeBrowser, .backgroundPopupProblemChangeBrowser").show();
        //window.open('', '_self', '');
        //window.close();
    }
}

function getParam(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    if (results == null)
        return "";
    else
        return results[1];
}

function isStandURL() {
    var standId = getParam("standId");
    if (standId != "") {
        // $('.modal-loading').show();
        setTimeout(function() {
            goToStandById(standId);
        }, 1000)
    }
}

function getIdLangParam() {
    var idLang = getParam("idLang");
    //var idLang=$.cookie("idLang");
    if (idLang == "" || idLang == null || (idLang != 3)) {
        var userLang = navigator.language || navigator.userLanguage;
        switch (userLang) {
            case 'fr':
                idLang = 3;
                break;
            default:
                idLang = 3;
                break;
        }
        //idLang = 3;

        if (currentPage == PageEnum.ACTIVATION)
            window.location.replace("?t=" + getParam("t") + "&idLang=" + idLang);
        else if (currentPage == PageEnum.PROFILE_CANDIDATE)
            window.location.replace("?candidateId=" + getParam("candidateId") + "&idLang=" + idLang);
        else
            window.location.replace("?idLang=" + idLang);
    }
    //console.log("idLang : "+idLang);
    return idLang;
}

function isIe_Ver() {
    var trident = !!navigator.userAgent.match(/Trident\/7.0/);
    var net = !!navigator.userAgent.match(/.NET4.0E/);
    var IE11 = trident && net;
    var IEold = (navigator.userAgent.match(/MSIE/i) ? true : false);
    if (IE11 || IEold) {
        return true;
    } else {
        return false;
    }
}
/**
 * @author lamlihe youssef
 * @param {type} value
 * @param {type} text
 * @returns {String}
 */
function generateOption(data, selector) {
    $(selector).empty();
    var selected = " selected";
    if (selector == "#secteur-actuelle" || $(selector).attr("multiple") != undefined) selected = "";
    $(selector).append('<option value="0" disabled="disabled"' + selected + ' >' + SELECTLABEL + '</option>');

    data.sort(function(a, b) {
        if (a.text > b.text)
            return 1;
        if (a.text < b.text)
            return -1;
        // a doit être égale à b
        return 0;
    });
    for (var i in data) {
        var item = data[i];
        var value = item.value;
        var text = item.text;
        if (item.enable != undefined) {
            if (item.enable == 'false') {
                $(selector).append('<option value="' + value + '" disabled="disabled">' + text + '</option>');
            } else {
                $(selector).append('<option value="' + value + '">' + text + '</option>');
            }
        } else {
            $(selector).append('<option value="' + value + '">' + text + '</option>');
        }
    }
}
/**
 * @author Saad Hjira
 * @param {type} value
 * @param {type} text
 * @returns {String}
 */
function generateOptionSelect(data, selector, selectValue) {
    $(selector).empty();
    var selected = " selected";
    if (selector == "#secteur-actuelle") selected = "";
    $(selector).append('<option value="" disabled="disabled"' + selected + '>' + selectValue + '</option>');
    $(selector).append('<option value="all">' + ALLLABEL + '</option>');

    data.sort(function(a, b) {
        if (a.text > b.text)
            return 1;
        if (a.text < b.text)
            return -1;
        // a doit être égale à b
        return 0;
    });
    for (var i in data) {
        var item = data[i];
        var value = item.value;
        var text = item.text;
        if (item.enable != undefined) {
            if (item.enable == 'false') {
                $(selector).append('<option value="' + value + '" disabled="disabled">' + text + '</option>');
            } else {
                $(selector).append('<option value="' + value + '">' + text + '</option>');
            }
        } else {
            $(selector).append('<option value="' + value + '">' + text + '</option>');
        }
    }
}
/**
 * @author lamlihe youssef
 * @param {type} value
 * @param {type} text
 * @returns {String}
 */
function stringToDate(str, sperator) {
    if (str != null) {
        var dateArray = str.split(sperator);
        var year = dateArray[2];
        var month = dateArray[1];
        var day = dateArray[0];
        if (sperator == null) sperator = "/";
        return new Date(year + sperator + month + sperator + day);
    }
    return null;
}
/**
 * @author lamlihe youssef
 * @param {type} value
 * @param {type} text
 * @returns {String}
 */
function isValidDate(dateStr, sperator) {

    // Checks for the following valid date formats:
    // MM/DD/YYYY
    // Also separates date into month, day, and year variables
    //  var datePat = /^(\d{2,2})(\/)(\d{2,2})\2(\d{4}|\d{4})$/;
    if (dateStr != null) {
        var datePat = /^\d{2}([.//])\d{2}\1\d{4}$/;

        var matchArray = dateStr.match(datePat); // is the format ok?
        if (matchArray == null) {
            return false;
        }

        var dateArray = dateStr.split(sperator);
        month = dateArray[1]; // parse date into variables
        day = dateArray[0];
        year = dateArray[2];
        if (year > 2016 || year < 1930) {
            return false;
        }
        if (month < 1 || month > 12) { // check month range
            return false;
        }
        if (day < 1 || day > 31) {
            return false;
        }
        if ((month == 4 || month == 6 || month == 9 || month == 11) && day == 31) {
            return false;
        }

        if (month == 2) { // check for february 29th
            var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
            if (day > 29 || (day == 29 && !isleap)) {
                return false;
            }
        }
        return true; // date is valid
    } else {
        return false;
    }
}

/**
 * @author Saad Hjira
 * @param {type} value
 * @param {type} text
 * @returns {boolean}
 */
function compareDate(dateStr1, dateStr2, sperator) {

    if (dateStr1 != null) {
        var datePat = /^\d{2}([.//])\d{2}\1\d{4}$/;

        var matchArray1 = dateStr1.match(datePat); // is the format ok?
        if (matchArray1 == null) {
            return false;
        }

        var matchArray2 = dateStr2.match(datePat); // is the format ok?
        if (matchArray2 == null) {
            return false;
        }

        var dateArray1 = dateStr1.split(sperator);
        month1 = dateArray1[1]; // parse date into variables
        day1 = dateArray1[0];
        year1 = dateArray1[2];

        var dateArray2 = dateStr2.split(sperator);
        month2 = dateArray2[1]; // parse date into variables
        day2 = dateArray2[0];
        year2 = dateArray2[2];

        var firstDate = year1 + '-' + month1 + '-' + day1;
        var secondDate = year2 + '-' + month2 + '-' + day2;

        if ((new Date(firstDate).getTime() > new Date(secondDate).getTime())) {
            return false;
        }

        return true; // date is valid
    } else {
        return false;
    }
};
/**
 * @author DAMI Azzeddine
 * @param {type} value
 * @param {type} text
 * @returns {String}
 */
function dateToString(date, sperator) {
    if (date != null) {
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var day = date.getDate();
        return getStringElementDateNumber(day) + sperator + getStringElementDateNumber(month) + sperator + year;
    }
    return null;
};

function getHLLanguageCode() {
    var idLang = getIdLangParam();
    switch (idLang) {
        case "1":
            return "ar";
        case "2":
            return "en";
        case "3":
            return "fr";
        case "4":
            return "ru";
        case "5":
            return "de";
        case "6":
            return "es";
        default:
            return "fr";
    }
}

function isInculdesStr(array, str) {
    if (array != null) {
        for (var i in array) {
            if (array[i] == str) return true;
        }
    }
    return false;
}