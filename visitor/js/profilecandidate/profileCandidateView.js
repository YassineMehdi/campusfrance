jQuery.extend({
  ProfileCandidateView: function () {
    var that = this;
    var listeners = new Array();
    this.scrollPanel = '';
    //----- init view ------------//
    this.initView = function () {
      this.scrollPanel = '.scroll-pane';
      that.ScrollPanel(this.scrollPanel);
    };
    this.ScrollPanel = function (scrollPanel) {
      $(scrollPanel).jScrollPane({
				autoReinitialise : true
			});
    };
    //-------- Load Session ------------//
    this.displayprofileCandidate = function (xml) {
      var candidate = new $.Candidate(xml);
      //console.log(candidate);
      $('#avatar').attr('src', candidate.userProfile.avatar);
      $('#profile-name').html(candidate.userProfile.firstName + ' ' + candidate.userProfile.secondName);
      $('#cv-title').html(candidate.cvContent.titleCV);
      $('#competences-info').html(candidate.cvContent.tags);
      that.AppendExperiences(candidate.cvContent.experienceCvs);
      that.AppendFormation(candidate.cvContent.formationCvs);
      that.AppendProjets(candidate.cvContent.projetCvs);
      that.AppendDocuments(candidate.pdfCvs);
      $('#myProfil .scroll-pane').jScrollPane({
				autoReinitialise : true
			});
    };
    this.AppendDocuments = function (documents) {
      //console.log(documents);
      for (var i in documents) {
        var item = documents[i];
        var cvName = item.cvName;
        var urlCv = item.urlCv;
        var tablecvName = cvName.split('.');
        var exte = 'icon-doc';
        if (tablecvName[1] == 'pdf') {
          exte = 'icon-pdf';
        }
        $('#liste-document').append('<li><a href="' + urlCv + '" title="' + tablecvName[0] + '" target="_blanck"><span class="' + exte + '"></span>' + tablecvName[0] + '</a></li>');
      }
    };
    this.AppendProjets = function (projets) {
      //console.log(projets);
      for (var i in projets) {
        var item = projets[i];
        var beginDate = item.beginDate;
        var endDate = item.endDate;
        var name = item.name;
        var url = item.url;
        var description = item.description;
        $('#projets-info').append('<li>' +
                '<span class="date">' + beginDate + '-' + endDate + '</span>' +
                '<span>' + name + ' / ' + url + '<br>' + description + ' </span>' +
                '</li>');
      }
    };
    this.AppendFormation = function (formation) {
      //console.log(formation);
      for (var i in formation) {
        var item = formation[i];
        var beginDate = item.beginDate;
        var endDate = item.endDate;
        var degree = item.degree;
        var speciality = item.speciality;
        var schoolName = item.schoolName;
        var description = item.description;
        $('#formation-info').append('<li>' +
                '<span class="date">' + beginDate + '-' + endDate + '</span>' +
                '<span>' + speciality + ' / ' + degree + ' /' + schoolName + '<br>' + description + ' </span>' +
                '</li>');
      }
    };
    this.AppendExperiences = function (experiences) {
      //console.log(experiences);
      for (var i in experiences) {
        var item = experiences[i];
        var beginDate = item.beginDate;
        var endDate = item.endDate;
        var companyName = item.companyName;
        var jobTitle = item.jobTitle;
        var description = item.description;
        $('#experiences-info').append('<li>' +
                '<span class="date">' + beginDate + '-' + endDate + '</span>' +
                '<span>' + companyName + ' / ' + jobTitle + ' <br>' + description + ' </span>' +
                '</li>');
      }
    };
    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  ProfileCandidateViewListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }
});

