jQuery.extend({
  ProfileCandidateModel: function () {
    var that = this;
    var listeners = new Array();
    this.initModel = function () {
      var candidateId = getParam("candidateId");
      that.candidateProfileCandidate(candidateId);
    };
//------------- Candidat Logout -----------//
    this.candidateProfileCandidateSuccess = function (xml) {
      that.notifyProfileCandidate(xml);
    };
    this.candidateProfileCandidateError = function (xhr, status, error) {
      isSessionExpired(xhr.responseText);
    };

    this.candidateProfileCandidate = function (candidateId) {
      genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'candidate/profile?candidateId=' + candidateId, 'application/xml', 'xml', '',
              that.candidateProfileCandidateSuccess, that.candidateProfileCandidateError);
    };
    //-------------------------------------//
    this.notifyProfileCandidate = function (profileCandidate) {
      $.each(listeners, function (i) {
        listeners[i].profileCandidate(profileCandidate);
      });
    };
    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  ProfileCandidateModelListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }
});