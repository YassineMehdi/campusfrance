jQuery.extend({
  MultimediasController: function (model, view) {
    var that = this;
    var multimediaModelListener = new $.MultimediaModelListener({
      multimediasSuccess: function (multimedias) {
        view.displayMultimedias(multimedias);
      },
      multimediasError: function (xhr) {
        view.displayMultimediasError(xhr);
      }
    });
    model.addListener(multimediaModelListener);

    // listen to the view
    var multimediaViewListener = new $.MultimediaViewListener({
    	multimedias: function (standId) {
    		model.multimedias(standId);
    	}
    });
    view.addListener(multimediaViewListener);

    this.initController = function () {
      view.initView();
      model.initModel();
    };
    that.initController();
  }

});
