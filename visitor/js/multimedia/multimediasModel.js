jQuery.extend({
  MultimediasModel: function () {
    var that = this;
    var listeners = new Array();
    this.initModel = function () {
    };
//--------  Multimedias --------------//
    this.multimediasSuccess = function (xml) {
      that.notifyMultimediasSuccess(xml);
    };

    this.multimediasError = function (xhr, status, error) {
      that.notifyMultimediasError(xhr);
    };
    this.multimedias = function (standId) {
      /*genericAjaxCall('POST', staticVars["urlBackEndCandidate"] + 'agenda/standAgenda?' + standId + '&idLang=' + getIdLangParam(),
              'application/xml', 'json', '', that.agendasSuccess,
              that.agendasError);*/
    };
    this.notifyMultimediasSuccess = function (multimedias) {
      $.each(listeners, function (i) {
        listeners[i].multimediasSuccess(multimedias);
      });
    };
    this.notifyMultimediasError = function (xhr) {
      $.each(listeners, function (i) {
        listeners[i].multimediasError(xhr);
      });
    };
    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  MultimediaModelListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }
});
