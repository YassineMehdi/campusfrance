jQuery.extend({
  AffichesController: function (model, view) {
    var that = this;
    var afficheModelListener = new $.AffichesModelListener({
      affichesSuccess: function (affiches) {
        view.displayAffiches(affiches);
      },
      affichesError: function (xhr) {
        view.displayAffichesError(xhr);
      }
    });
    model.addListener(afficheModelListener);

    // listen to the view
    var afficheViewListener = new $.AffichesViewListener({
      affiches: function (standId) {
        model.affiches(standId);
      }
    });
    view.addListener(afficheViewListener);

    this.initController = function () {
      view.initView();
      model.initModel();
    };
    that.initController();
  }

});