jQuery.extend({
  ContactStandModel: function () {
    var listeners = new Array();
    var that = this;

    this.initModel = function () {
      // this.getOffreEmplois();
    };
    //------------- get All OffreEmplois -----------//
    this.getContactStandError = function (xhr, status, error) {
      isSessionExpired(xhr.responseText);
    };

    this.getContactStand = function (standId) {
      listContactCache = [];
      genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'contact/standContacts?' + standId + '&idLang=' + getIdLangParam(), 'application/xml', 'xml', '', function (xml) {
        listContactCache = new Array();
        $(xml).find("contact").each(function () {
          var contact = new $.Contact($(this));
          listContactCache[contact.getContactId()] = contact;
        });
        that.notifyLoadContactStand(listContactCache);
      }, that.getContactStandError);
    };

    this.notifyLoadContactStand = function (listOffreEmplois) {
      $.each(listeners, function (i) {
        listeners[i].loadContactStand(listOffreEmplois);
      });
    };

    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  ModelListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }

});