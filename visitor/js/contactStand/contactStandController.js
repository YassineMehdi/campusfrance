jQuery.extend({
  ContactStandController: function (model, view) {
    var that = this;
    var listOffreEmploisInCache = [];
    /**
     * listen to the view
     */

    var vlist = $.ViewListener({
      launchContactStand: function (standId) {
        model.getContactStand(standId);
      }
    });

    view.addListener(vlist);

    /**
     * listen to the model
     */
    var mlist = $.ModelListener({
      loadContactStand: function (listOffreEmplois) {
        view.displayContactStand(listOffreEmplois);
      }
    });

    model.addListener(mlist);

    this.initController = function (standId) {
      view.initView(standId);
      model.initModel();
    };
    this.getView = function () {
      return view;
    };
    that.initController();
  }
});