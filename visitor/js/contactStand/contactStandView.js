jQuery.extend({
  ContactStandManage: function (contact) {
    var contactId = contact.getContactId();

    var isFavorite = factoryFavoriteExist(contactId, FavoriteEnum.CONTACT);
    var displayDeleteFavorite = "block";
    var displayAddFavorite = "block";
    if (isFavorite)
      displayAddFavorite = "none";
    else
      displayDeleteFavorite = "none";

    var dom = $('<div class="row no-margin info-p">'
		            + '<div class="col-md-6 col-sm-6 left">'
		            	+ '<img alt="" src="' + contact.getContactPhoto() + '" class="avatar">'
		            	+ '<h3>' + htmlEncode(contact.getContactName()) + '</h3>'
		            	+ '<ul>'
				            + '<li><i class="icon-entreprise"></i> <span class="enterpriseLabel">Entreprise</span></li>'
				            + '<li><i class="icon-fonction"></i> <span class="functionLabel">Fonction</span></li>'
				            + '<li><i class="icon-mail"></i> <span class="emailLabel">E-mail</span></li>'
				            + '<li><i class="icon-tel"></i> <span class="phoneNumberLabel">Téléphone</span></li>'
			            + '</ul>'
		            + '</div>'
		            + '<div class="col-md-6 col-sm-6 right">'
		            	+ '<a class="btn col-md-12 col-sm-12 active addOffreEmploiToFavorite .addOffreEmploiToFavoriteButton_' + contactId + '" style="display:' + displayAddFavorite + '"><i class="glyphicon glyphicon-star" aria-hidden="true"></i>'+ADDTOFAVORITELABEL+'</a><a class="btn col-md-12 col-sm-12 active deleteOffreEmploiFromFavorite  .deleteOffreEmploiToFavoriteButton_' + contactId + '" style="background-color: #b3081b;display:' + displayDeleteFavorite + '"><i class="glyphicon glyphicon-star" aria-hidden="true"></i>' + DELETEFROMFAVORITELABEL + '</a>'
		//            + '<a href="#" class="btn col-md-11 col-sm-11 active"><i class="glyphicon glyphicon-star" aria-hidden="true"></i> Favoris</a>'
		            + '<br/><ul>'
			            + '<li>' + htmlEncode(contact.getEnterpriseName()) + '</li>'
			            + '<li>' + htmlEncode(contact.getJobTitle()) + '</li>'
			            + '<li>' + htmlEncode(contact.getContactEmail()) + '</li>'
			            + '<li>' + htmlEncode(contact.getContactPhone()) + '</li>'
		            + '</ul>'
		            + '</div>'
            + '</div>');

    this.refresh = function () {
      var addContactFavoriteLink = dom.find(".addOffreEmploiToFavorite");
      var deleteContactFavoriteLink = dom.find(".deleteOffreEmploiFromFavorite");
      addContactFavoriteLink.click(function () {
        if (connectedUser) {
          var idEntity = contactId;
          var componentName = FavoriteEnum.CONTACT;
          var favoriteXml = factoryXMLFavory(idEntity, componentName);
          addFavorite(favoriteXml);
          $(this).hide();
          $(this).closest('div').find('.deleteOffreEmploiFromFavorite').show();
          $('.addOffreEmploiToFavoriteButton_' + contactId).hide();
          $('.deleteOffreEmploiFromFavoriteButton_' + contactId).show();
        } else {
          getStatusNoConnected();
        }
      });
      deleteContactFavoriteLink.click(function () {
        var idEntity = contactId;
        var componentName = FavoriteEnum.CONTACT;
        var favoriteXml = factoryXMLFavory(idEntity, componentName);
        deleteFavorite(favoriteXml);
        $(this).hide();
        $(this).closest('div').find('.addOffreEmploiToFavorite').show();
        $('.addOffreEmploiToFavoriteButton_' + contactId).show();
        $('.deleteOffreEmploiFromFavoriteButton_' + contactId).hide();
      });
    };
    this.getDOM = function () { //retourne une ligne HTML <a .....
      return dom;
    };
    this.refresh();
  },
  ContactStandView: function () {
    var listeners = new Array();
    var that = this;
    this.displayContactStand = function (contacts) {
      $('#modal-contact .info-stand-contact').empty();
      if (contacts!= null && contacts.length > 0) {
      	$('#modal-contact .info-stand-contact').append('<div class="scroll-pane"></div>');
      	var contactsHandler = $('.modal-content .info-stand-contact div');
        var contactStandManage = null;
        for (index in contacts) {
            contactStandManage = new $.ContactStandManage(contacts[index]);
            contactsHandler.append(contactStandManage.getDOM());
            traduct();
        }
        $('#modal-contact .scroll-pane').jScrollPane({ autoReinitialise: true });
      } else {
        $('#modal-contact .info-stand-contact').append('<p class="empty"> '+NOCONTACT+'</p>');
      }
      $('.modal-loading').hide();
    };
    this.initView = function () {
      $('#modal-contact .info-stand-contact').empty();
      var standId = $('#contact-stand').attr('standId');
      $('#contact-stand').unbind('click').bind('click', function () {
          if(!($("#modal-contact").data('bs.modal') || {}).isShown) {
  	        factoryActivateFaIcon(this);
  	        $('.modal').modal('hide');
  	        $('.modal-loading').show();
  	        that.notifyContactStand(standId);
  	        $('#modal-contact').modal('show');
          }
      });
    };
    this.notifyContactStand = function (standId) {
      $.each(listeners, function (i) {
        listeners[i].launchContactStand(standId);
      });
    };
    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  ViewListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }
});

