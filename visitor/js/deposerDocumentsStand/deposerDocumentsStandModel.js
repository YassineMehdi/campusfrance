jQuery.extend({
  DeposerDocumentsStandModel: function () {
    var listeners = new Array();
    var that = this;

    this.initModel = function () {
      // this.getPresses();
    };
    this.getCandidateCVsSuccess = function (xml) {
      listCandidateCVs = new Array();
      $(xml).find("pdfCvDTO").each(function () {
        var candidateCv = new $.PdfCv($(this));
        listCandidateCVs.push(candidateCv);
      });
      that.notifyCandidateCVsUploaded(listCandidateCVs);
    };

    this.getCandidateCVsError = function (xhr, status, error) {
      isSessionExpired(xhr.responseText);
    };

    this.notifyLoadDocsCVs = function () {
      genericAjaxCall('GET', staticVars["urlBackEndCandidate"]
              + 'pdfCv/uploadedCvs', 'application/xml', 'xml', '',
              that.getCandidateCVsSuccess, that.getCandidateCVsError);
    };
    this.sendDeposerDocumentsStandSuccess = function (xml) {
      that.notifySended();
    };

    this.sendDeposerDocumentsStandError = function (xhr, status, error) {
      isSessionExpired(xhr.responseText);
    };
    this.sendDeposerDocumentsStand = function (url, messageToSend, standId) {
      var xml = '<unSolicitedJobApplication>'
              + '<motivationLetter>' + htmlEncode(messageToSend) + '</motivationLetter>'
              + '<urlCv>' + htmlEncode(url) + '</urlCv>'
              + '</unSolicitedJobApplication>';
      genericAjaxCall('POST', staticVars["urlBackEndCandidate"]
              + 'jobApplication/addUnsolicitedJobApplication?' + standId + '&idLang=' + getIdLangParam(), 'application/xml', 'xml', xml,
              that.sendDeposerDocumentsStandSuccess, that.sendDeposerDocumentsStandError);
    };
    //------------- get All Presses -----------//


    this.notifySended = function () {
      $.each(listeners, function (i) {
        listeners[i].sendedDocumentsStand();
      });
    };
    this.notifyCandidateCVsUploaded = function (listCandidateCVs) {
      $.each(listeners, function (i) {
        listeners[i].candidateCVsUploaded(listCandidateCVs);
      });
    };
    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  ModelListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }

});
