jQuery.extend({
  DeposerDocumentsStandController: function (model, view) {
    var that = this;
    /**
     * listen to the view
     */

    var vlist = $.ViewListener({
      sendedDocumentsStand: function () {
        view.displaysendedDocumentsStand();
      },
      candidateCVsUploaded: function (listCandidateCVs) {
        view.displaycandidateCVs(listCandidateCVs);
      },
    });

    view.addListener(vlist);

    /**
     * listen to the model
     */
    var mlist = $.ModelListener({
      sendDeposerDocumentsStand: function (url, messageToSend, standId) {
        model.sendDeposerDocumentsStand(url, messageToSend, standId);
      },
      notifyLoadDocsCVs: function () {
        model.notifyLoadDocsCVs();
      },
    });

    model.addListener(mlist);

    this.initController = function (standId) {
      view.initView(standId);
      model.initModel();
    };
    that.initController();
  },
});