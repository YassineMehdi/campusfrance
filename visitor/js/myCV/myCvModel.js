jQuery.extend({
  MyCvModel: function () {
    var listeners = new Array();
    var that = this;

    this.initModel = function () {
      //this.getCandidatePersonalInfos();
    };

    //------------- get Candidate Personel Info -----------//
    this.candidatePersonalInfosSuccess = function (xml) {
			that.notifyLoadCandidatePersonalInfos(new $.Candidate(xml));
		};
	
		this.candidatePersonalInfosError = function (xhr, status, error) {
		  	isSessionExpired(xhr.responseText);
		};
		
		this.getCandidatePersonalInfos = function () {
		  	genericAjaxCall('GET', staticVars["urlBackEndCandidate"]
		          + 'candidate/candidateCv?idLang='+getIdLangParam(), 'application/xml', 'xml', '',
		          that.candidatePersonalInfosSuccess, that.candidatePersonalInfosError);
		};
		
		//------------- get Candidate Personel CV Info -----------//
		this.contentCVSuccess = function(xml) {
			that.notifyLoadCandidateContentCV(new $.CvContent(xml));
		};
		
		this.contentCVError = function(xhr, status, error) {
			isSessionExpired(xhr.responseText);
		};
	
		this.getContentCv = function() {	
			genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'cvcontent/candidateCv', 'application/xml', 'xml', '', 
					that.contentCVSuccess, that.contentCVError);
		};
		
		//------------- get Candidate Personel CV Info -----------//
		this.getVisitCardsSuccess = function(xml) {
//			var listCandidates = [];
			var listFavorites = [];
			var favorite = null;
			$(xml).find('favorite').each(function () {
					favorite = new $.Favorite(this);
					listFavorites.push(favorite);
    	});
			that.notifyLoadListVisitCards(listFavorites);
		};
		
		this.getVisitCardsError = function(xhr, status, error) {
			isSessionExpired(xhr.responseText);
		};
	
		this.getVisitCards = function() {	
			genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'favorite/userFavoritesVisitor?idLang=' + getIdLangParam(), 'application/xml', 'xml', '', 
					that.getVisitCardsSuccess, that.getVisitCardsError);
		};
		//------------- get Candidate CVs -----------//
		this.getCandidateCVsSuccess = function(xml) {
			listCandidateCVs = new Array();
	        $(xml).find("pdfCvDTO").each(function () {
	          var candidateCv = new $.PdfCv($(this));
	          listCandidateCVs.push(candidateCv);
	        });
	        //console.log(listCandidateCVs);
			that.notifyCandidateCVsUploaded(listCandidateCVs);
		};
		
		this.getCandidateCVsError = function(xhr, status, error) {
			isSessionExpired(xhr.responseText);
		};
		
		this.loadDocsCVs = function () {
		  	genericAjaxCall('GET', staticVars["urlBackEndCandidate"]
		          + 'pdfCv/uploadedCvs', 'application/xml', 'xml', '',
		          that.getCandidateCVsSuccess, that.getCandidateCVsError);
		};
		
	    //------------- Save cv content -----------//
		this.sendCVContentSuccess = function(xml) {
			that.notifySendCvContentSuccess();
		};
		
		this.sendCVContentError = function(xhr, status, error) {
			isSessionExpired(xhr.responseText);
		};
		
		this.sendCvContent = function (cvContentXml) {
	      	genericAjaxCall('POST', staticVars["urlBackEndCandidate"]
	          + 'cvcontent/addContent', 'application/xml', 'xml', cvContentXml,
	          that.sendCVContentSuccess, that.sendCVContentError);
		};

    //------------- Delete cv -----------//
		this.deleteCvSuccess = function(xml) {
			that.notifyDeleteCvSuccess();
			that.loadDocsCVs();
		};
		
		this.deleteCvError = function(xhr, status, error) {
			isSessionExpired(xhr.responseText);
		};
		
		this.deleteCv = function (cvId) {
			genericAjaxCall('POST', staticVars["urlBackEndCandidate"] + 'pdfCv/deleteCv?idCv=' + cvId, '', '', '', 
					that.deleteCvSuccess, that.deleteCvError);
		};

    //------------- Delete cv -----------//
		this.updateCandidateSuccess = function(xml) {
			that.notifySendCvContentSuccess();
		};
		
		this.updateCandidateError = function(xhr, status, error) {
			isSessionExpired(xhr.responseText);
		};
		
		this.updateCandidate = function (candidate) {
			genericAjaxCall('POST', staticVars["urlBackEndCandidate"] + 'candidate/updateCv?idLang='+getIdLangParam(), 
					'application/xml', 'xml', candidate.xmlData(), that.updateCandidateSuccess, that.updateCandidateError);
		};
		
	  this.notifyLoadCandidatePersonalInfos = function (candidate) {
	    $.each(listeners, function (i) {
	      listeners[i].loadCandidatePersonalInfos(candidate);
	    });
	  };
	    
 		this.notifyLoadListVisitCards = function (listFavorites) {
  		$.each(listeners, function (i) {
    			listeners[i].loadListVisitCards(listFavorites);
  		});
  	};
	  this.notifyLoadCandidateContentCV = function (cvContent) {
	    $.each(listeners, function (i) {
	      listeners[i].loadCandidateContentCV(cvContent);
	    });
	  };
	    
		this.notifyCandidateCVsUploaded = function (listCandidateCVs) {
	    $.each(listeners, function (i) {
	      listeners[i].candidateCVsUploaded(listCandidateCVs);
	    });
	  };
		this.notifySendCvContentSuccess = function () {
	    $.each(listeners, function (i) {
	      listeners[i].sendCvContentSuccess();
	    });
	  };
		this.notifyDeleteCvSuccess = function () {
	    $.each(listeners, function (i) {
	      listeners[i].deleteCvSuccess();
	    });
	  };
	  this.addListener = function (list) {
	    listeners.push(list);
	  };
    
  },
  ModelListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }

});


