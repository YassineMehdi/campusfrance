jQuery.extend({
  InboxView: function () {
    var listeners = new Array();
    var that = this;
    this.currentConversation = null;

    this.displayReceivedConversations = function (conversations){
    	conversationsDisplayed = conversations;
    	listDisplayedConversations = [];
    	for (index in conversations) {
				var conversation = conversations[index];
				var conversationSubject = conversation.subject;
				var conversationSender = conversation.enterpriseName;
				var conversationDate = conversation.lastUpdate;
				var listActions = "<a id='" + index + "' class='visiter-action showDetails'>"
	                + "<img src='img/icons/icon-visiter.png' alt='' title='"+VISITSTANDLABEL+"'/></a>";
				conversationSender = "<a href='javascript:void(0)' id='"+index+"'>"+htmlEncode(formattingLongWords(conversationSender, 30))+"</a>";
				conversationSubject = "<a href='javascript:void(0)' id='"+index+"'>"+htmlEncode(formattingLongWords(conversationSubject, 30))+"</a>";
				
				var data = [ conversationSender, conversationSubject, conversationDate,listActions ];
	
				listDisplayedConversations.push(data);
			}

			that.addTableDatas('#table-liste-message-recu', listDisplayedConversations);
    	$('#userInboxCantainer .scroll-pane').jScrollPane({ autoReinitialise: true });
			$('.modal-loading').hide();
    };
    
    this.notifyConversationDetails = function(conversationIndex){
		//console.log('messageSelectedId : '+conversationIndex );
		$('.inboxCantainer').hide();
		$('#detailsMessageCantainer').show();
		$('#messageDetails').empty();
		that.showConversation(conversationIndex);
	};
	
	this.showConversation = function(conversationIndex){
		var conversation = conversationsDisplayed[conversationIndex];
		that.currentConversation = conversation;
		var messagesList = conversation.messages;
		listDisplayedConversationMessages = [];
		for (index in messagesList) {
			var message = messagesList[index];
			var conversationSender = null;
			if (candidatGlobal.getUserProfile().getUserProfileId() == message.sender.getUserProfileId()){
				conversationSender = htmlEncode(message.sender.firstName+' '+message.sender.secondName)+"<br>"+htmlEncode(message.content).replace(/[\n]/gi, "<br/>" );
			}else{
				conversationSender = htmlEncode(conversation.enterpriseName)+"<br>"+htmlEncode(message.content).replace(/[\n]/gi, "<br/>" );
			}
			var conversationDate = message.messageDate;
			
			var data = [ conversationSender, conversationDate ];

			listDisplayedConversationMessages.push(data);
		}
		that.addTableDatas('#table-liste-details-message-recu', listDisplayedConversationMessages);
					
		$('#detailsMessageCantainer .scroll-pane').animate({scrollTop: 0}, 'slow');
		$('#sendMessagesLink, #backToInbow').show();
		$("#userInbox .glyphicon").removeClass('active');
    $('#sendMessagesLink').addClass('active');
    $('#detailsMessageCantainer .scroll-pane').jScrollPane({ autoReinitialise: true });
  	$("#sendResponseMessages").val("");
  	$('#messagesReveivedLink, .modal-loading').hide();
	};
  this.beforeInitView = function () {
  	that.initInboxDataTable();
		that.initDetailsConversationDataTable();
	  $('#messagesReveivedLink a, #backToInbow a').unbind('click').bind('click', function () {
	    that.initView();
	  });
	
		$('#sendResponseButton').unbind('click').bind('click',function() {
			$("#formSendReponse").validate({
	        errorPlacement: function errorPlacement(error, element) {
	          element.before(error);
	        },
	        rules: {
	          sendResponseMessages: {
	            required: true
	          },
	        }
	  	});
	  	var valide = $("#formSendReponse").valid();
	  	if (!valide) {
	        $('label.error').hide();
	        return false;
	  	}
	  	$('.modal-loading').show();
	 	
	   	var xmlReply = "";
			var content = $("#sendResponseMessages").val();
			var conversation=that.currentConversation;
			var subject = conversation.messages[0].subject;
			
			xmlReply = "<message><conversation>" + conversation.idConversation + "</conversation>"
						+"<subject>"+ htmlEncode(subject) + "</subject>"
						+"<content>"+ htmlEncode(content) + "</content></message>";
			that.notifyMessageReply(conversation.idConversation, xmlReply);
		});
  };
  
  this.initView = function () {
  	$('.genericContent, .inboxCantainer').hide();
  	$('#userInbox, #userInboxCantainer, #messagesReveivedLink').show();
  	$('#sendMessagesLink, #backToInbow').hide();
  	$('#page-content').removeClass('no-padding');
  	$("#userInbox .glyphicon").removeClass('active');
  	$('#messagesReveivedLink').addClass('active');
  };
  this.initInboxDataTable = function(){
  	$('#table-liste-message-recu')
		.dataTable(
				{
					"aaData" : [],
					"aoColumns" : [ {
						"sTitle" : FROMLABEL,
						"sWidth" : "30%",
					}, {
						"sTitle" : OBJECTLABEL,
						"sWidth" : "40%",
					}, {
						"sTitle" : DATELABEL,
						"sWidth" : "20%",
					},{
						"sTitle" : ACTIONSLABEL,
						"sWidth" : "10%",
					}],
					"bAutoWidth" : false,
					"bRetrieve" : false,
					"bDestroy" : true,
					"bFilter" : false,
					"bLengthChange" : false,
					"iDisplayLength" : 10,
					"language": {
			            "url": "dataTable/i18n/"+dataTableLang+".json"
		       		},
					"fnDrawCallback" : function() {
						$('.showDetails').off();
	                    $('.showDetails').on("click", function () {
 					 			$('.modal-loading').show();
	                    	var conversationIndex = $(this).attr('id');
		                    that.notifyConversationDetails(conversationIndex);
	                    } );
	                    $('#table-liste-message-recu a').off();
	                    $('#table-liste-message-recu a').on("click", function () {
 					 			$('.modal-loading').show();
	                    	var conversationIndex = $(this).attr('id');
		                    that.notifyConversationDetails(conversationIndex);
	                    } );
						$("thead").find('th:last')
								.removeClass('sorting');
					}
				});
	  };
    this.initDetailsConversationDataTable = function(){
    	$('#table-liste-details-message-recu')
				.dataTable(
					{
						"aaData" : [],
						"aoColumns" : [ {
							"sTitle" : MESSAGESLABEL,
							"sWidth" : "70%",
						},{
							"sTitle" : DATELABEL,
							"sWidth" : "30%",
						},],
					    "aaSorting": [[ 1, "desc" ]],
						"bAutoWidth" : false,
						"bRetrieve" : false,
						"bDestroy" : true,
						"bFilter" : false,
						"bLengthChange" : false,
						"iDisplayLength" : 10,
						"language": {
				            "url": "dataTable/i18n/"+dataTableLang+".json"
			       		},
						"fnDrawCallback" : function() {
							$("thead").find('th:last')
									.removeClass('sorting');
						}
					});
	  };
    this.addTableDatas = function(selector, datas){
    	var oTable = $(selector).dataTable();
      oTable.fnClearTable();
      if(datas.length > 0) oTable.fnAddData(datas);
      oTable.fnDraw();
    };
    this.messageReplySuccess = function(){
			swal({
	      title: "",
	      text: MESSAGESENTLABEL,
	      type: "success"},
		    function () {
		    });
			$('.userMessagesLink').trigger('click');
    };
	  that.beforeInitView();
    
    this.notifyMessageReply = function(standId, messageXml){
			$.each(listeners, function(i) {
				listeners[i].messageReply(standId, messageXml);
			});
		};
    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  
  ViewListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }
});

