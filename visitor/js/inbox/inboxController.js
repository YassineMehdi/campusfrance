jQuery.extend({
  InboxController: function (model, view) {
    var that = this;
    /**
     * listen to the view
     */

    var vlist = $.ViewListener({
    	messageReply : function(conversationId, messageXml) {
           model.messageReply(conversationId, messageXml);
    	},
    });

    view.addListener(vlist);

    /**
     * listen to the model
     */
    var mlist = $.ModelListener({
    	receivedConversationsLoaded : function(conversations) {
           view.displayReceivedConversations(conversations);
    	},
    	messageReplySuccess: function () {
        view.messageReplySuccess();
      },
    });

    model.addListener(mlist);

    this.initController = function () {
	  $('.modal-loading').show();
      view.initView();
      model.initModel();
    };
    
  },
});