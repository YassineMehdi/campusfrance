jQuery.extend({
  HomePageController: function (model, view) {
    var that = this;
    var homePageModelListener = new $.HomePageModelListener({
    	notifyLoadFunctionCandidates : function(functionCandidates) {
  			view.loadFunctionCandidates(functionCandidates);
  		},
  		
  		notifyLoadActivitySectors : function(activitySectors) {
  			view.loadActivitySectors(activitySectors);
  		},
  		
      notifyLoadStandSectors : function(standSectors) {
        view.loadStandSectors(standSectors);
      },

  		notifyLoadJobSought : function(jobSoughts) {
  			view.loadJobSought(jobSoughts);
  		},
  		
  		notifyLoadAnneesExperiences : function(anneesExperiences) {
  			view.loadAnneesExperiences(anneesExperiences);
  		},
  		
  		notifyLoadRegions : function(regions) {
  			view.loadRegions(regions);
  		},
  		
  		areaExpertise: function (areaexpertises) {
        view.loadAreaexpertise(areaexpertises);
    	},
	
    	loadSession: function (loadSession) {
      	view.displayloadSession(loadSession);
    	},
    	
    	candidatLoginSuccess: function (candidatXml) {
      	view.displaycandidatLoginSuccess(candidatXml);
    	},
    	
    	candidatLoginError: function (xhr) {
      	view.displaycandidatLoginError(xhr);
    	},
    	
    	candidateForgotPasswordSuccess: function (candidatXml) {
      	view.displaycandidateForgotPasswordSuccess(candidatXml);
    	},
    	
    	candidateForgotPasswordError: function (xhr) {
      	view.displaycandidateForgotPasswordError(xhr);
    	},
    	
    	candidatLogout: function (xhr) {
      	view.displaycandidatLogout(xhr);
    	},

      listenerOpeningHallSuccess : function (xhr) {
        view.listenerOpeningHallSuccess(xhr);
      },

      listenerOpeningHallError : function (xhr) {
        view.listenerOpeningHallError(xhr);
      },
    });
    model.addListener(homePageModelListener);

    // listen to the view
    var homePageViewListener = new $.HomePageViewListener({
      candidateLogin: function (login, password) {
        model.candidateLogin(login, password);
      },
      candidateForgotPassword: function (login) {
        model.candidateForgotPassword(login);
      },
      candidateLogout: function () {
        model.candidateLogout();
      },
      listenerOpeningHall : function(email){
        model.listenerOpeningHall(email);
      }
    });
    view.addListener(homePageViewListener);

    this.initController = function () {
      view.initView();
      model.initModel();
    };
    that.initController();
  }

});