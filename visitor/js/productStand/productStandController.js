jQuery.extend({
  ProductStandController: function(model, view) {
    var that = this;
    /**
     * listen to the view
     */

    var vlist = $.ViewListener({
      launchProductStand: function(standId) {
        model.getProductStand(standId);
      },
      notifyGetProductsList: function(currentProduct){
        model.getProductsList(currentProduct);
      },
    });

    view.addListener(vlist);

    /**
     * listen to the model
     */
    var mlist = $.ModelListener({
      loadProductStand: function(listProducts) {
        view.displayProductStand(listProducts);
      },
    });

    model.addListener(mlist);

    this.initController = function(standId) {
      view.initView(standId);
      model.initModel();
    };
    this.getView = function() {
      return view;
    };
  }
});
