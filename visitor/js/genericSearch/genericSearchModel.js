jQuery.extend({
  GenericSearchModel: function () {
    var listeners = new Array();
    var that = this;
    var listStandConnectedIds = new Array();
    
    this.initModel = function () {

    };
    // ------------- Visit Cards -----------//
    this.getVisitCardsSuccess = function (xml, keyWord) {
      var listVisitorsCache = new Array();
      $(xml).find("candidateDTO").each(function () {
        var candidate = new $.Candidate($(this));
        listVisitorsCache.push(candidate);
      });

      that.getVisitorsEnterpriseCards(listVisitorsCache, keyWord);
    };
    this.getVisitCardsError = function (xhr, status, error) {
      isSessionExpired(xhr.responseText);
    };

    this.getVisitCards = function (keyWord) {
      genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'candidate/get/visitors?idLang=' + getIdLangParam(), 'application/xml', 'xml', 
          '', function(xml){
          that.getVisitCardsSuccess(xml, keyWord);
        }, that.getVisitCardsError);
    };

    // ------------- Users List -----------//
    this.getVisitorsSuccess = function (xml, keyWord) {
    	var listVisitorsCache = new Array();
      $(xml).find("candidateDTO").each(function () {
        var candidate = new $.Candidate($(this));
        listVisitorsCache.push(candidate);
      });

      that.getVisitorsEnterprise(listVisitorsCache, keyWord);
    };
    this.getVisitorsError = function (xhr, status, error) {
      isSessionExpired(xhr.responseText);
    };

    this.getVisitors = function (keyWord) {
      genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'candidate/get/visitors?idLang=' + getIdLangParam(), 'application/xml', 'xml', 
          '', function(xml){
          that.getVisitorsSuccess(xml, keyWord);
        }, that.getVisitorsError);
    };
    // ------------- Enterprise List -----------//
    this.getVisitorsEnterpriseCardsSuccess = function (xml, listVisitorsCache, keyWord) {
      $(xml).find("enterprisePDTO").each(function () {
        var enterpriseP = new $.EnterpriseP($(this));
        listVisitorsCache.unshift(enterpriseP);
      });
      that.notifyLoadVisitorsCards(listVisitorsCache, keyWord);
    };
    this.getVisitorsEnterpriseCardsError = function (xhr, status, error) {
      isSessionExpired(xhr.responseText);
    };

    this.getVisitorsEnterpriseCards = function (listVisitorsCache, keyWord) {
      genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'enterprisep/get/visitors?idLang=' + getIdLangParam(), 'application/xml', 'xml', 
          '', function(xml){
          that.getVisitorsEnterpriseCardsSuccess(xml, listVisitorsCache, keyWord);
        }, that.getVisitorsEnterpriseCardsError);
    };

    // ------------- Users List -----------//
    this.getVisitorsEnterpriseSuccess = function (xml, listVisitorsCache, keyWord) {
      $(xml).find("enterprisePDTO").each(function () {
        var enterpriseP = new $.EnterpriseP($(this));
        listVisitorsCache.unshift(enterpriseP);
      });
      that.getConnectedVisitors(listVisitorsCache, keyWord);
    };
    this.getVisitorsEnterpriseError = function (xhr, status, error) {
      isSessionExpired(xhr.responseText);
    };

    this.getVisitorsEnterprise = function (listVisitorsCache, keyWord) {
      genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'enterprisep/get/visitors?idLang=' + getIdLangParam(), 'application/xml', 'xml', 
          '', function(xml){
          that.getVisitorsEnterpriseSuccess(xml, listVisitorsCache, keyWord);
        }, that.getVisitorsEnterpriseError);
    };

    // ------------- Users List -----------//
    this.getConnectedVisitorsSuccess = function (json, listVisitorsCache, keyWord) {
    	var connectedUsers = [];
    	$.each(json, function(key, connectedUser){
    		connectedUsers.push(connectedUser);
    	});
      that.notifyLoadVisitors(listVisitorsCache, connectedUsers, keyWord);
    };
    this.getConnectedVisitorsError = function (xhr, status, error) {
      isSessionExpired(xhr.responseText);
    };
    this.getConnectedVisitors= function (listVisitorsCache, keyWord) {
      genericAjaxCall('GET', staticVars["urlBackEndEnterprise"] + 'chat/get/connectedUsers?idLang=' + getIdLangParam(), 
      		'application/json', 'json', '', function(json){
          that.getConnectedVisitorsSuccess(json, listVisitorsCache, keyWord);
        }, that.getConnectedVisitorsError);
    };
    // ------------- Products List -----------//
    this.getProductsSuccess = function (xml, keyWord) {
    	listJobOffersCache = new Array();
      $(xml).find("productDTO").each(function () {
        var product = new $.Product($(this));
        var stand = listStandCache[product.getStand().getIdStande()];
        if(stand != null){
          listJobOffersCache.push(product);
        }
      });
      that.notifyLoadProducts(listJobOffersCache, keyWord);
    };
    this.getProductsError = function (xhr, status, error) {
      isSessionExpired(xhr.responseText);
    };

    this.getProducts = function (keyWord) {
      listJobOffersCache = [];
      genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'product/get/all?idLang=' + getIdLangParam(), 'application/xml', 'xml', 
      		'', function(xml){
	    		that.getProductsSuccess(xml, keyWord);
	    	}, that.getProductsError);
    };

// ------------- Send Question -----------//
    this.getProductionQuestionSuccess = function (xml, keyWord) {
      $('.modal-loading').hide();
      swal({
        title: "",
        text: REQUESTSUCCESSLABEL,
        type: "success"},
        function () {
          $('#modal-send-request-product').modal('hide');
        }
      );
    };
    this.getProductionQuestionError = function (xhr, status, error) {
      isSessionExpired(xhr.responseText);
    };

    this.sendProductQuestion = function (QuestionInfo) {
      genericAjaxCall('POST', staticVars["urlBackEndCandidate"] + 'questionInfo/question?idLang=' + getIdLangParam(), 'application/xml', 'xml', 
          QuestionInfo.xmlData(), that.getProductionQuestionSuccess, that.getProductionQuestionError);
    };
    // ------------- Product List -----------//
    this.getStandsSuccess = function (xml, keyWord, sectorText) {
    	listStandCache = new Array();
      $(xml).find("stand").each(function () {
        var stand = new $.Stand($(this));
        var standId = stand.getIdStande();
        if(!isInculdesStr(["76", "77", "78", "80", "83", "85", "86", "26", "3", "73", "65", "71", "69"], standId)){
          var languages = listStandConnectedIds[standId];
          stand.setConnectTChatLanguages(languages);
          if (languages != null) {
            stand.setConnected('yes');
          } else {
            stand.setConnected('no');
          }
          listStandCache.push(stand);
        }
      });
      that.notifyLoadStands(listStandCache, keyWord, sectorText);
    };
    this.getStandsError = function (xhr, status, error) {
      isSessionExpired(xhr.responseText);
    };

    this.getStands = function (keyWord, sectorText) {
      listJobOffersCache = [];
      genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'stand?idLang=' + getIdLangParam(), 'application/xml', 'xml', 
      		'', function(xml){
      		that.getStandsSuccess(xml, keyWord, sectorText);
      	}, that.getStandsError);
    };

    this.getStandsConnectedSuccess = function (xml) {
      listStandConnectedIds = new Array();
      var result = $(xml).find('restResponse').find('body').text();
      var listStandConnectedIdsArray = result.split(SEPARATESTANDS);
      for (j in listStandConnectedIdsArray) {   // remplacer par contains
        var stand = listStandConnectedIdsArray[j];
        var standArray = stand.split(SEPARATESTANDANDLANGS);
        var standId = standArray[0];
        var languageIds = standArray[1];
        if (languageIds != null && languageIds != "") {
          var languages = languageIds.split(SEPARATELANGSSTAND);
          listStandConnectedIds[standId] = languages;
        }
      }
    };

    this.getStandsConnected = function () {
      genericAjaxCall('GET', staticVars["urlBackEndEnterprise"] + 'stand/standsConnected', 'application/xml', 'xml', 
      		'', that.getStandsConnectedSuccess, '');
    };
    //----------------------------------------------//
    this.notifyLoadVisitors = function (listCandidate, connectedUsers, keyWord) {
      $.each(listeners, function (i) {
        listeners[i].loadCandidates(listCandidate, connectedUsers, keyWord);
      });
    };       
    this.notifyLoadVisitorsCards = function (listCandidate, keyWord) {
      $.each(listeners, function (i) {
        listeners[i].loadVisitorsCards(listCandidate, keyWord);
      });
    };      
    this.notifyLoadProducts = function (listProducts, keyWord) {
      $.each(listeners, function (i) {
        listeners[i].loadProducts(listProducts, keyWord);
      });
    };
    this.notifyLoadJobs = function (listJobOffers, keyWord) {
      $.each(listeners, function (i) {
        listeners[i].loadJobs(listJobOffers, keyWord);
      });
    };
    this.notifyLoadStands = function (listStand, keyWord, sectorText) {
      $.each(listeners, function (i) {
        listeners[i].loadStands(listStand, keyWord, sectorText);
      });
    };

    this.getNotifs = function (xhr, status, error) {
      $.each(listeners, function (i) {
        listeners[i].candidateLoginError(xhr, status, error);
      });
    };

    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  ModelListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }

});

