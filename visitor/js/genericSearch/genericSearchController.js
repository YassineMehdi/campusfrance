jQuery.extend({
  GenericSearchController: function (model, view) {
    var that = this;    
    /**
     * listen to the view
     */

    var vlist = $.ViewListener({
      notifySendProduction: function (QuestionInfo) {
        model.sendProductQuestion(QuestionInfo);
      },

      launchSearch: function (optionVal, searchInput, selectVal1) {
        $('.modal-loading').show();
        switch (optionVal) {
          case "stand":
            $('#stand-key').val(searchInput);
            model.getStandsConnected();
            model.getStands(searchInput, selectVal1);
            break;
          case "document":
            model.getDocuments(searchInput);
            break;
          case "video":
            model.getVideos(searchInput);
            break;
          case "webCast":
            model.getWebCasts(searchInput);
            break;
          case "publicChat":
            model.getPublicChats(searchInput);
            break;
          case "product":
            $('#product-key').val(searchInput);
            model.getProducts(searchInput);
            break;
          case "visitors":
            var oTable = $(".table-liste-generic-search-visitor").dataTable();
            oTable.fnClearTable();
            model.getVisitors(searchInput);
            break;
          case "visitCard":
            //var oTable = $(".table-liste-generic-search-visitCard").dataTable();
            //oTable.fnClearTable();
            model.getVisitCards(searchInput);
            break;
        }
      },
      standCriteriaSearch: function (standKey, activity, connected) {
        liststands = [];
        if (activity == ALLLABEL || activity == null) activity = "";
        if (connected == ALLLABEL || connected == null) connected = "";
        for (i in listStandsInCache) {
          var stand = listStandsInCache[i];
          var nameSectors = stand.getNameSectors();
          if ((compareToLowerString(stand.getName(), standKey) !== -1 || compareToLowerString(nameSectors, standKey) !== -1)
                  && (compareToLowerString(nameSectors, activity) !== -1)
                  && (compareToLowerString(stand.isConnected(), connected) !== -1)) {
            liststands.push(stand);
          }
        }

        view.displayStand(liststands, standKey);
      },
      productCriteriaSearch: function (productKey, enterpriseName, sectorName) {
        listProducts = [];
        for (i in listProductsInCache) {
          var product = listProductsInCache[i];
          var enterpriseNameProduct = product.getStand().getEnterprise().getNom();
          var stand = listStandCache[product.getStand().getIdStande()];
          if(stand != null){
            var nameSectors = stand.getNameSectors();
            if (((compareToLowerString(product.getProductTitle(), productKey) !== -1)
                    || (compareToLowerString(product.getProductDescription(), productKey) !== -1)
                    || (compareToLowerString(enterpriseNameProduct, productKey) !== -1 || compareToLowerString(nameSectors, productKey) !== -1))
              && (compareToLowerString(enterpriseNameProduct, enterpriseName) !== -1) && (compareToLowerString(nameSectors, sectorName) !== -1)){
              listProducts.push(product);
            }
          }
        }

        view.displayProducts(listProducts, productKey);
      },
      visitorCriteriaSearch: function (visitorKey, visitorSector) {
        $('#visitor-key').val(visitorKey);
        listVisitors = [];
        if (visitorSector == ALLLABEL || visitorSector == null || visitorSector == ACTIVITYSECTORLABEL) {
        	visitorSector = "";
        }
        for (i in listCandidatesInCache) {
          var visitor = listCandidatesInCache[i];
          if (((compareToLowerString(visitor.getUserProfile().getFirstName(), visitorKey) !== -1)
                  || (compareToLowerString(visitor.getUserProfile().getSecondName(), visitorKey) !== -1)) && (compareToLowerString(visitor.getNameSectors(), visitorSector) !== -1)) {
            listVisitors.push(visitor);
          }
        }
        view.displayVisitors(listVisitors, visitorKey);
      },
      visitorCardCriteriaSearch: function (visitorKey, visitorSector) {
        $('#visitor-card-key').val(visitorKey);
        listVisitors = [];
        if (visitorSector == ALLLABEL || visitorSector == null || visitorSector == ACTIVITYSECTORLABEL) {
          visitorSector = "";
        }
        for (i in listCandidatesInCache) {
          var visitor = listCandidatesInCache[i];
          if (((compareToLowerString(visitor.getUserProfile().getFirstName(), visitorKey) !== -1)
                  || (compareToLowerString(visitor.getUserProfile().getSecondName(), visitorKey) !== -1)) && (compareToLowerString(visitor.getNameSectors(), visitorSector) !== -1)) {
            listVisitors.push(visitor);
          }
        }
        view.displayVisitorsCards(listVisitors, visitorKey);
      },


    });

    view.addListener(vlist);

    /**
     * listen to the model
     */
    var mlist = $.ModelListener({
      loadProducts: function (listProductsCache, keyWord) {
        listProductsInCache = listJobOffersCache;
        $('#product-key').val(keyWord);
        var listProduct = [];
        for (i in listProductsInCache) {
          var product = listProductsInCache[i];
          if ((compareToLowerString(product.getProductTitle(), keyWord) !== -1)
                  || (compareToLowerString(product.getProductDescription(), keyWord) !== -1)) {
            listProduct.push(product);
          }
        }
        view.displayProducts(listProduct, keyWord);
      },
      loadVisitorsCards: function (listCandidatesCache, keyWord) {
        listCandidatesInCache = listCandidatesCache;
        var listVisitors = [];
        for (i in listCandidatesInCache) {
            var visitor = listCandidatesInCache[i];
            visitor.setConnected(true);
            if ((compareToLowerString(visitor.getUserProfile().getFirstName(), keyWord) !== -1)
                  || (compareToLowerString(visitor.getUserProfile().getSecondName(), keyWord) !== -1)) {
                listVisitors.push(visitor);
          }
        }
        view.displayVisitorsCards(listVisitors, keyWord);
      },     
      loadCandidates: function (listCandidatesCache, connectedUsers, keyWord) {
        listCandidatesInCache = listCandidatesCache;
        $('#visitors-key').val(keyWord);
        var connectedUser = null;
      	var connectedUserProfileIds = [];
      	var canChatonPrivateUserProfileIds = [];
      	for(var index in connectedUsers){
        	connectedUser = connectedUsers[index];
        	connectedUserProfileIds.push(connectedUser.userId);
        	if(connectedUser.canChatOnPrivate)
        		canChatonPrivateUserProfileIds.push(connectedUser.userId);
        }
        var listVisitors = [];
        for (i in listCandidatesInCache) {
          var visitor = listCandidatesInCache[i];
          if(connectedUserProfileIds.indexOf(visitor.getUserProfile().getUserProfileId()) != -1){
          	visitor.setConnected(true);
          	if(FlashDetect.installed && visitor.getUserProfile().getUserProfileId() != candidatGlobal.getUserProfile().getUserProfileId()
          			&& canChatonPrivateUserProfileIds.indexOf(visitor.getUserProfile().getUserProfileId()) != -1) 
          		visitor.getUserProfile().setCanChatOnPrivate(true);
          	else visitor.getUserProfile().setCanChatOnPrivate(false);
          }else {
        		visitor.setConnected(false);
          	visitor.getUserProfile().setCanChatOnPrivate(false);
        	}
          if ((compareToLowerString(visitor.getUserProfile().getFirstName(), keyWord) !== -1)
                  || (compareToLowerString(visitor.getUserProfile().getSecondName(), keyWord) !== -1)) {
            listVisitors.push(visitor);
          }
        }
        view.displayVisitors(listVisitors, keyWord);
      },
      loadStands: function (listStandsCache, keyWord, sectorText) {
        listStandsInCache = listStandsCache;
        if (sectorText == ALLLABEL || sectorText == null) {
        	sectorText = "";
        }
        var liststand = [];
        for (i in listStandsInCache) {
          var stand = listStandsInCache[i];
          var nameSectors = stand.getNameSectors();
          if (((compareToLowerString(stand.getName(), keyWord) !== -1)
                  || (compareToLowerString(stand.getSocialWallMessage(), keyWord) !== -1)
                  || (compareToLowerString(nameSectors, keyWord) !== -1)) && (compareToLowerString(nameSectors, sectorText) !== -1)) {
            liststand.push(stand);
          }
        }
        view.displayStand(liststand, keyWord, sectorText);
      },
    });

    model.addListener(mlist);

    this.initController = function () {
      view.initView();
    };

    this.searchStandByKey = function (standKey, secteur, cennected) {
      view.notifyStandCriteriaSearch(standKey,secteur,cennected);
    };

    this.searchProductByKey = function (productKey, enterpriseName, sectorName) {
      view.notifyProductCriteriaSearch(productKey, enterpriseName, sectorName);
    };

    this.getView = function() {
      return view;
    };
    
    that.initController();
    model.initModel();
  },
});


