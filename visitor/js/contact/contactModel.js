jQuery
.extend({
	Contact : function(data) {
		var that = this;
		that.firstName = '';
		that.secondName = '';
		that.currentCompany = '';
		that.jobTitle = '';
		that.cellPhone = '';
		that.email = '';
		that.groupStand = '';
		that.sector = '';
		this.contact = function(data) {
			if (data) {
				if ($(data).find("firstName"))
					that.firstName = $(data).find("firstName").text();
				if ($(data).find("secondName"))
					that.secondName = $(data).find("secondName").text();
				if ($(data).find("currentCompany"))
					that.currentCompany = $(data).find("currentCompany").text();
				if ($(data).find("jobTitle"))
					that.jobTitle = $(data).find("jobTitle").text();
				if ($(data).find("cellPhone"))
					that.cellPhone = $(data).find("cellPhone").text();
				if ($(data).find("email"))
					that.email = $(data).find("email").text();
				if ($(data).find("groupStand"))
					that.groupStand = $(data).find("groupStand").text();
				if ($(data).find("sector"))
					that.sector = $(data).find("sector").text();
			}
		};
		this.getFirstName = function() {
			return that.firstName;
		};
		this.getSecondName = function() {
			return that.secondName;
		};
		this.getCurrentCompany = function() {
			return that.currentCompany;
		};
		this.getJobTitle = function() {
			return that.jobTitle;
		};
		this.getCellPhone = function() {
			return that.cellPhone;
		};
		this.getEmail = function() {
			return that.email;
		};
		this.getGroupStand = function() {
			return that.groupStand;
		};
		this.getSector = function() {
			return that.sector;
		};
		this.setFirstName = function(firstName) {
			that.firstName = firstName;
		};
		this.setSecondName = function(secondName) {
			that.secondName = secondName;
		};
		this.setCurrentCompany = function(currentCompany) {
			that.currentCompany = currentCompany;
		};
		this.setJobTitle = function(jobTitle) {
			that.jobTitle = jobTitle;
		};
		this.setCellPhone = function(cellPhone) {
			that.cellPhone = cellPhone;
		};
		this.setEmail = function(email) {
			that.email = email;
		};
		this.setGroupStand = function(groupStand) {
			that.groupStand = groupStand;
		};
		this.setSector = function(sector) {
			that.sector = sector;
		};
		this.update = function(newData) {
			that.contact(newData);
		};
		this.xmlData = function() {
			var xml = '<contactDTO>';
			xml += '<firstName>' + htmlEncode(that.firstName) + '</firstName>';
			xml += '<secondName>' + htmlEncode(that.secondName) + '</secondName>';
			xml += '<currentCompany>' + htmlEncode(that.currentCompany)
					+ '</currentCompany>';
			xml += '<jobTitle>' + htmlEncode(that.jobTitle)+ '</jobTitle>';
			xml += '<cellPhone>' + htmlEncode(that.cellPhone) + '</cellPhone>';
			xml += '<email>' + htmlEncode(that.email) + '</email>';
			xml += '<groupStand>' + htmlEncode(that.groupStand)
					+ '</groupStand>';
			xml += '<sector>' + htmlEncode(that.sector) + '</sector>';
			xml += '</contactDTO>';
			return xml;
		};
		this.contact(data);
	},
	StandSector: function (data) {
		var that = this;
		that.sectorId = 0;
		that.sectorName = '';

		this.standSector = function (data) {
			if (data) {
				if ($(data).find("sectorId"))
					that.sectorId = $(data).find("sectorId").text();
				if ($(data).find("sectorName"))
					that.sectorName = $(data).find("sectorName").text();
			}
		};
		this.getSectorId = function () {
			return that.sectorId;
		};
		this.getSectorName = function () {
			return that.sectorName;
		};
		this.setSectorId = function (sectorId) {
			that.sectorId = sectorId;
		};
		this.setSectorName = function (sectorName) {
			that.sectorName = sectorName;
		};
		this.update = function (newData) {
			that.standSector(newData);
		};
		this.xmlData = function () {
			var xml = '<sectorDTO>';
			xml += '<sectorId>' + that.sectorId + '</sectorId>';
			xml += '<sectorName>' + htmlEncode(that.sectorName) + '</sectorName>';
			xml += '</sectorDTO>';
			return xml;
		};

		this.standSector(data);
	},
	ContactModel : function() {
		var that = this;
		var listeners = new Array();
		this.initModel = function() {
			this.fillSectors();
		};
		// ------------- Activity sector ----------- //
		this.fillSectorsSuccess = function(xml) {
			var sectors = [];
			$(xml).find('sectorDTO').each(function() {
				var sector = new $.StandSector($(this));
				sectors.push(sector);
			});
			that.notifyLoadSectors(sectors);
		};
		this.fillSectorsError = function(xhr, status, error) {
			isSessionExpired(xhr.responseText);
		};
		// Activation page scripts
		this.fillSectors = function() {
			genericAjaxCall('GET', staticVars["urlBackEndCandidate"]
				+ 'sector?idLang=' + getIdLangParam(), 'application/xml',
				'xml', '', that.fillSectorsSuccess,
				that.fillSectorsError);
		};
		this.notifyLoadSectors = function(sectors) {
			$.each(listeners, function(i) {
				listeners[i].loadSectors(sectors);
			});
		};
		// --------- END ---------------------//
		// ------------- Register Contact -----------//
		this.registerContactSuccess = function(xml) {
			that.notifyLoadSuccess();
		};
		this.registerContactError = function(xhr, status, error) {
			isSessionExpired(xhr.responseText);
			that.notifyLoadError(xhr);
		};
		// Function Contacts page scripts
		this.registerContact = function(contact) {
			genericAjaxCall('POST', staticVars["urlBackEndCandidate"]
				+ 'message/reservestand?idLang=' + getIdLangParam(),
				'application/xml', 'xml', contact.xmlData(),
				that.registerContactSuccess, that.registerContactError);
		};
		// ------------- Captcha -----------//
		this.verifyCaptchaRegister =  function(contact, response) {
			$.ajax({
				type: "POST",
				url: staticVars.urlValidateCaptcha,
				dataType: "text",
				data : {'g-recaptcha-response':response},
				contentType : "application/x-www-form-urlencoded; charset=UTF-8",
				success: function(text){
					if(text.trim() == 'true'){
						that.registerContact(contact);
					}else{
						$("body").removeClass("loading");
					}
				},
				error: function(){
					$("body").removeClass("loading");
				},
			});
		};
		// --------- END ---------------------//
		this.notifyLoadSuccess = function() {
			$.each(listeners, function(i) {
				listeners[i].contactSuccess();
			});
		};
		this.notifyLoadError = function(xhr) {
			$.each(listeners, function(i) {
				listeners[i].contactError(xhr);
			});
		};

		// --------- END ---------------------//

		this.addListener = function(list) {
			listeners.push(list);
		};
	},
	ContactModelListener : function(list) {
		if (!list)
			list = {};
		return $.extend(list);
	}
});


