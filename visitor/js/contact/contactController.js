jQuery.extend({
	ContactController : function(model, view) {
		var that = this;
		var contactModelListener = new $.ContactModelListener({
			contactSuccess : function() {
				view.displayContactSuccess();
			},
			contactError : function(xhr) {
				view.displayContactError(xhr);
			},
			loadSectors : function(sectors) {
				view.displaySectors(sectors);
			},
		});
		model.addListener(contactModelListener);

		// listen to the view
		var contactViewListener = new $.ContactViewListener({
			registerContact : function(contact) {
				model.registerContact(contact);
			},
			verifyCaptchaRegister : function(contact, response) {
				model.verifyCaptchaRegister(contact, response);
			},
		});
		view.addListener(contactViewListener);

		this.initController = function() {
			view.initView();
			model.initModel();
		};
		that.initController();
	}

});


