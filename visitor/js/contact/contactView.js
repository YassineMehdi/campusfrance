jQuery
.extend({
	ContactView : function() {
		var that = this;
		var listeners = new Array();
		this.validator = '';
		this.languageSelector = '';
		this.submitSelector = '';
		this.emailHandler = null;
		this.groupStandSelector = null;
		this.hallsSelector = null;
		// ----- init view ------------//
		this.initView = function() {
			switch (getIdLangParam()) {
				case '3':
				$("#langDrop").append('<img src="flags/4x3/fr.svg" alt=""><span class="">'+FRENCHLABEL+'</span><span class="caret"></span>');
				break;
				default:
				$("#langDrop").append('<img src="flags/4x3/gb.svg" alt=""><span class="">'+ENGLISHLABEL+'</span><span class="caret"></span>');
				break;
			}
			this.submitSelector = '#envoyer';
			this.emailHandler = $('#email');
			this.groupStandSelector = '#groupStand';
			this.hallsSelector = "#halls";
			that.actionSubmitHandler(this.submitSelector);
			
			$('#homePage').on('click', function () {
				location.replace("http://" + HOSTNAME_URL + "/visitor/new-home.html?idLang="+getIdLangParam());
			});
			$('.inscription').show();
			if (defaultBrowser)
				$("select:not([multiple='multiple'])").addClass("browser-default");
			else
				$('select').attr('style', 'display:none');
			that.Select(that.groupStandSelector);
		};
		this.MultiSelect = function(Selector) {
			$(Selector).multiselect({
				noneSelectedText : SELECTLABEL,
				selectedList : 3,
				header : false,
				selectedText : '# ' + SELECTED_LABEL,
			});
		};
		this.Select = function(SelectSelector) {
			$(SelectSelector).material_select();
			// that.scrollbarMousedown(SelectSelector);
		};
		this.actionSubmitHandler = function(submitSelector) {
			$(submitSelector).on('click', function() {
				that.validateRegisterForm();
				$('select').attr('style', 'display:block');
				$("#g-recaptcha-response").show();
				var $valid = $("#inscription").valid();
				if (!$valid) {
					if (defaultBrowser)
						$("select[multiple='multiple']").attr('style', 'display:none');
					else
						$('select').attr('style', 'display:none');
					$("#g-recaptcha-response").hide();
					return false;
				}
				$("#g-recaptcha-response").hide();
				$('select').attr('style', 'display:none');
				$("body").addClass("loading");
				var contact = new $.Contact();
				/************************** END ********************/
				// ---- Set FirstName
				contact.setFirstName($('#firstName').val());
				// ---- Set SecondName
				contact.setSecondName($('#secondName').val());
				// ---- Set Current Company
				contact.setCurrentCompany($('#currentCompany').val());
				// ---- Set JobTitle
				contact.setJobTitle($('#jobTitle').val().trim());
				// ---- Set Phone Number
				contact.setCellPhone($('#phonePortable').val());
				// ---- Set Email
				contact.setEmail($('#email').val().trim());
				// ---- Set Web site
				contact.setGroupStand($("#groupStand option:selected").text());
				// ---- Set Web site
				contact.setSector($("#halls option:selected").text());
				/** *********** END ****************************** */
				that.notifyVerifyCaptchaRegister(contact, $("#g-recaptcha-response").val());
			});
		};
		this.validateRegisterForm = function() {
			$.validator.addMethod("noSpace", function(value, element) {
				return value.indexOf(" ") < 0 && value != "";
			}, THANK_YOU_REMOVE_SPACES_LABEL);
			$.validator.addMethod("datevalid", function(value, element) {
				if (value.length > 0) {
					return isValidDate(value, '/');
				} else {
					return true;
				}
			}, THANK_YOU_RESPECT_INDICATED_FORMAT_LABEL);
			$.validator
			.addMethod(
				"phoneNumbre",
				function(phone_number, element) {
					phone_number = phone_number.replace(/\s+/g, "");
					return this.optional(element)
					|| phone_number.length > 9
					&& (phone_number
						.match(/\(?\+([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/) || phone_number
						.match(/\(?(00[0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/));
				}, THANK_YOU_RESPECT_INDICATED_FORMAT_LABEL);
			$("#inscription").validate({
				errorPlacement : function errorPlacement(error, element) {
					element.before(error);
				},
				rules : {
					firstName : {
						required : true
					},
					secondName : {
						required : true
					},
					email : {
						required : true,
						email : true,
						minlength : 3,
						noSpace : true
					},
					phonePortable : {
						required: true,
						minlength: 10
					},
					currentCompany : {
						required : true
					},
					jobTitle : {
						required : true
					},
					groupStand : {
						required : true
					},
					halls : {
						required : true
					},
					"g-recaptcha-response" : {
						required: true
					},
				}
			});
		};
		/* steps form */
		// -------- Activity Sectors ------------//
		this.displaySectors = function(sectors) {
			var data = [];
			for ( var i in sectors) {
				var sector = sectors[i];
				data.push({
					value : replaceSpecialChars(sector.getSectorId()),
					text : replaceSpecialChars(sector.getSectorName())
				});
			}
	    	generateOption(data, that.hallsSelector);
	    	that.Select(that.hallsSelector);
		};
		// -------- Display Contact Success Save ------------//
		this.displayContactSuccess = function() {
			$("body").removeClass("loading");
			$("#inscription").remove();
			$('.bannier')
			.after(
				'<div class="inscription"><h2>'
				+ YOUR_REQUEST_HAS_BEEN_FORWARDED_LABEL
				+'<br/>'+EACCETEAMLABEL+'</h2><br/><br/><br/><br/></div>');
			setTimeout(function() {
				window.location.replace("new-home.html?idLang="+getIdLangParam());
			}, 5000);
		};
		// -------- Display Contact Error Save ------------//
		this.displayContactError = function(xhr) {
			$("body").removeClass("loading");
			if (xhr.status === 409) {
				swal(SAVEMESSAGELABEL, ALREADYEXISTEMAILLABEL, "error");
			} else {
				swal(SAVEMESSAGELABEL,
					SAVEERRORLABEL, "error");
			}
			// $('.previous a').trigger('click');
		};
		// -------- Check Email Error ------------//
		this.notifyRegisterContact = function(contact) {
			$.each(listeners, function(i) {
				listeners[i].registerContact(contact);
			});
		};
		this.notifyVerifyCaptchaRegister = function(contact, response){
			$.each(listeners, function(i){
				listeners[i].verifyCaptchaRegister(contact, response);
			});
		};
		this.addListener = function(list) {
			listeners.push(list);
		};
	},
	ContactViewListener : function(list) {
		if (!list)
			list = {};
		return $.extend(list);
	}
});



