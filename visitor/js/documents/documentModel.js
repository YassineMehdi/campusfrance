jQuery.extend({
  DocumentModel: function () {
    var listeners = new Array();
    var that = this;

    this.initModel = function () {
    	that.getDocuments();
    };

    //------------- get All Documents -----------//
    this.getDocumentsSuccess = function (xml) {
      var documents = [];
      $(xml).find("allfiles").each(function () {
          var document = new $.AllFiles($(this));
          var stand = listStandCache[document.getStandId()];
          if(stand != null){
      	     documents.push(document);
          }
      });
      that.notifyLoadDocuments(documents);
    };

    this.getDocumentsError = function (xhr, status, error) {
      isSessionExpired(xhr.responseText);
    };

    this.getDocuments = function () {
      $('.modal-loading').show();
      genericAjaxCall('GET', staticVars["urlBackEndCandidate"]+ 'allfiles/documents?idLang=' + getIdLangParam(),
              'application/xml', 'xml', '', that.getDocumentsSuccess, that.getDocumentsError);
    };

    this.notifyLoadDocuments = function (documents) {
      $.each(listeners, function (i) {
        listeners[i].loadDocuments(documents);
      });
    };

    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  ModelListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }

});

