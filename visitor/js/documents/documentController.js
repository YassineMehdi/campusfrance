jQuery.extend({
  DocumentController: function (model, view) {
    var that = this;
    /**
     * listen to the view
     */

    var vlist = $.ViewListener({
    });

    view.addListener(vlist);

    /**
     * listen to the model
     */
    var mlist = $.ModelListener({
      loadDocuments: function (documents) {
        view.loadDocuments(documents);
      },
    });

    model.addListener(mlist);

    this.initController = function () {
      view.initView();
      model.initModel();
    };
  },
});

