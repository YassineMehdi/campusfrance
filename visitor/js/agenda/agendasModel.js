jQuery.extend({
  AgendasModel: function () {
    var that = this;
    var listeners = new Array();
    this.initModel = function () {
    };
//--------  Agendas --------------//
    this.agendasSuccess = function (xml) {
      var events = [];
    	$(xml).find('event').each(function() {
    		events.push(new $.EventAgenda($(this)));
			});
      that.notifyAgendasSuccess(events);
    };

    this.agendasError = function (xhr, status, error) {
      that.notifyAgendasError(xhr);
    };
    this.agendas = function (standId) {
    	genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'agenda/standAgenda?' + standId + '&idLang=' + getIdLangParam(),
              'application/xml', 'xml', '', that.agendasSuccess,
              that.agendasError);
    };
    this.notifyAgendasSuccess = function (events) {
      $.each(listeners, function (i) {
        listeners[i].agendasSuccess(events);
      });
    };
    this.notifyAgendasError = function (xhr) {
      $.each(listeners, function (i) {
        listeners[i].agendasError(xhr);
      });
    };
    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  AgendaModelListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }
});
