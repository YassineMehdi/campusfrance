jQuery.extend({
  ProductApercuManageFav: function (product) {
    var productId = product.getProductId();
    var isFavorite = factoryFavoriteExist(productId, FavoriteEnum.PRODUCT);
    var displayDeleteFavorite = "block";
    var displayAddFavorite = "block";
    if (isFavorite)
      displayAddFavorite = "none";
    else
      displayDeleteFavorite = "none";
    var productImage = product.getProductImage();
    if(productImage == "" || productImage == "#" || productImage == undefined) 
    	productImage = product.getStand().getEnterprise().getLogo();
    	
    var dom = $('<div class="cel-info">'
            + '<div class="logo col-md-12 col-sm-12 no-padding">'
            + '<img src="' + productImage + '" alt="">'
            + '</div>'
            + '<h3>' + product.getProductTitle() + '</h3>'
            + '<br>'
            + '<p class = "intro-offre"  id = "productDescription">' + unescape(product.getProductDescription()) + '</p>'
            + '</div>'
            + '<div class="cel-btn ">'
            + '<br>'
            + '<a class="btn col-md-11 col-sm-11 active addProductToFavorite addProductToFavoriteButton_' + productId + '" style="display:' + displayAddFavorite + '"><i class="glyphicon glyphicon-star" aria-hidden="true"></i>'+ADDTOFAVORITELABEL+'</a><a class="btn col-md-11 col-sm-11 active deleteProductFromFavorite deleteProductFromFavoriteButton_' + productId + '" style="background-color: #b3081b;display:' + displayDeleteFavorite + '"><i class="glyphicon glyphicon-star" aria-hidden="true"></i>' + DELETEFROMFAVORITELABEL + '</a>'
            + '<a href="#" class="btn col-md-11 col-sm-11 green postuler-job" id="postuler_' + productId + '"> Postuler</a>'
            + '</div>');
    this.refresh = function () {
      var addProductFavoriteLink = dom.find(".addProductToFavorite");
      var deleteProductFavoriteLink = dom.find(".deleteProductFromFavorite");
      addProductFavoriteLink.click(function () {
        if (connectedUser) {
          var componentName = FavoriteEnum.PRODUCT;
          var favoriteXml = factoryXMLFavory(product.getProductId(), componentName);
          addFavorite(favoriteXml);

          $('.addProductToFavoriteButton_' + productId).hide();
          $('.deleteProductFromFavoriteButton_' + productId).show();
        } else {
          getStatusNoConnected();
        }
      });
      deleteProductFavoriteLink.click(function () {
        var componentName = FavoriteEnum.PRODUCT;
        var favoriteXml = factoryXMLFavory(productId, componentName);
        deleteFavorite(favoriteXml);
        $('.addProductToFavoriteButton_' + productId).show();
        $('.deleteProductFromFavoriteButton_' + productId).hide();
      });
      analyticsView.viewStandZoneById(PRODUCT+'/'+PRODUCT_ID, productId);
    };
    this.getDOM = function () { //retourne une ligne HTML <a .....
      return dom;
    };
    this.refresh();
  },
  FavoriesView: function () {
    var listeners = new Array();
    var that = this;
    var jobOffersFavoritesCache = [];
    var contactsFavoritesCache = [];
    var testimoniesFavoritesCache = [];
    var advicesFavoritesCache = [];
    var mediasFavoritesCache = [];
    var productsFavoritesCache = [];

    $('#modal-document-lecteur').on('hidden.bs.modal', function () {
    	$(this).find('.document-lecteur').empty();
    });

    this.favoritesLoaded = function (jobOffersFavorites, contactsFavorites, testimoniesFavorites, 
				advicesFavorites, mediasFavorites, productsFavorites) {
      //that.addJobOffersFavoriteDatas(jobOffersFavorites);
      that.addContactsFavoriteDatas(contactsFavorites);
      that.addTestimoniesFavoriteDatas(testimoniesFavorites);
      that.addAdvicesFavoriteDatas(advicesFavorites);
      that.addMediasFavoriteDatas(mediasFavorites);
      that.addProductsFavoriteDatas(productsFavorites);

      traduct();
      $('#mediaFavLink').trigger('click');
    };
    this.initView = function () {
			$('.modal-loading').show();
      $('.genericContent').hide();
      $('#favories').show();
      $('#page-content').removeClass('no-padding');

      var listProductsCache = [];

      //that.initJobOffersFavoriteTable();
      that.initContactsFavoriteTable();
      that.initTestimoniesFavoriteTable();
      that.initAdvicesFavoriteTable();
      that.initMediasFavoriteTable();
      that.initProductsFavoriteTable();
    };
    this.addFavoriteDatas = function(selector, favoritesData){
    	var oTable = $(selector).dataTable();
      oTable.fnClearTable();
      if(favoritesData.length > 0) oTable.fnAddData(favoritesData);
      oTable.fnDraw();
    };
    this.initJobOffersFavoriteTable = function(){
    };
    this.addJobOffersFavoriteDatas = function(favorites){
    	jobOffersFavoritesCache = favorites;
    	var favoritesData = [];
      $(favorites).each(function (index, jobOffer) {
        var jobOfferTitle = jobOffer.getJobTitle();
        var enterpriseLogo = jobOffer.getEnterpriseLogo();
        var standId = jobOffer.getStandId();
        var enterpriseInfo = '<div class="logo-company-wrapper"> <a href="#" class="logo-company"> <img class="" src="' + enterpriseLogo + '" alt="" /> </a> <span class="name-company" >' + htmlEncode(jobOffer.getEnterpriseName()) + '</span> </div>';
        var listActions = "<a id='" + standId + "' class='visiter-action stands viewFavoriteStandJobOffer' standId='" + standId + "' title='"+VISITSTANDLABEL+"'>"
                + "<img src='img/icons/icon-visiter.png' alt='' /></a>";

        listActions = listActions
                + "<a id='" + index + "' class='visiter-action deleteFavoriteJobOfferButton' title='"+DELETEFROMFAV+"'>"
                + "<img src='img/icons/icon-delete.png' alt='' /></a>"
                + "<a id='" + index + "' class='visiter-action viewFavoriteJobOffer' title='"+VIEWLABEL+"'>"
                + "<img src='img/icons/icon-apercu.png' alt='' /></a>";

        var data = [enterpriseInfo, formattingLongWords(jobOfferTitle, 30), listActions];
        favoritesData.push(data);
      });
      that.addFavoriteDatas(".listFavoriteJobOffers", favoritesData);
    };
    this.initContactsFavoriteTable = function(){
      $('.listFavoriteContacts').dataTable({
        "aaData": [],
        "aoColumns": [{
            "sTitle": PHOTOLABEL
          }, {
            "sTitle": ENTERPRISELABEL
          }, {
            "sTitle": FIRSTANDLASTNAMELABEL
          }, {
            "sTitle": EMAILLABEL
          }, {
            "sTitle": JOBTITLELABEL
          }, {
            "sTitle": ACTIONSLABEL
          }],
        "bAutoWidth": false,
        "bRetrieve": false,
        "bDestroy": true,
        "bFilter": false,
        "bLengthChange": false,
        "iDisplayLength": 10,
        "language": {
          "url": "dataTable/i18n/"+dataTableLang+".json"
        },
        "fnDrawCallback": function () {
          $('.viewFavoriteStandContact').off();
          $('.viewFavoriteStandContact')
                  .on("click",
                          function () {
                    				that.visitStand($(this).attr('standId'));
                          });
          $('.deleteFavoriteContactButton').off();
          $('.deleteFavoriteContactButton')
                  .on('click',
                          function () {
                            var index = $(this).attr('id');
                            var contact = contactsFavoritesCache[index];
                            var contactId = contact.getContactId();
                            factoryDeleteFavorite(this, contactId, FavoriteEnum.CONTACT);
                          });
          $("thead").find('th:last')
                  .removeClass('sorting');
        }
      });
    };
    this.addContactsFavoriteDatas = function(favorites){
    	contactsFavoritesCache = favorites;
    	var favoritesData = [];
      $(favorites).each(function (index, contact) {
        var contactPhoto = contact.getContactPhoto();
        var contactName = contact.getContactName();
        var contactEmail = contact.getContactEmail();
        var enterpriseName = contact.getEnterpriseName();
        var jobTitle = contact.getJobTitle();
        var standId = contact.getStandId();
        var contactPhotoImg = '<div class="logo-company-wrapper"> <a href="#" class="logo-company"> <img class="" src="' + contactPhoto + '" alt="" /> </a> </div>';
        var listActions = "<a id='" + standId + "' class='visiter-action stands viewFavoriteStandContact'  standId='" + standId + "' title='"+VISITSTANDLABEL+"'>"
                + "<img src='img/icons/icon-visiter.png' alt='' /></a>"
                + "<a id='" + index + "' href='#' class='visiter-action deleteFavoriteContactButton' title='"+DELETEFROMFAV+"'>"
                + "<img src='img/icons/icon-delete.png' alt='' /></a>";

        var data = [contactPhotoImg, formattingLongWords(enterpriseName, 30), formattingLongWords(contactName, 30), contactEmail, formattingLongWords(jobTitle, 30), listActions];
        favoritesData.push(data);
      });
      that.addFavoriteDatas(".listFavoriteContacts", favoritesData);
    };
    this.initTestimoniesFavoriteTable = function(){
    	$('.listFavoriteTestimonies').dataTable({
        "aaData": [],
        "aoColumns": [{
            "sTitle": PHOTOLABEL
          }, {
            "sTitle": ENTERPRISELABEL
          }, {
            "sTitle": FIRSTANDLASTNAMELABEL
          }, {
            "sTitle": JOBTITLELABEL
          }, {
            "sTitle": ACTIONSLABEL
          }],
        "bAutoWidth": false,
        "bRetrieve": false,
        "bDestroy": true,
        "bFilter": false,
        "bLengthChange": false,
        "iDisplayLength": 10,
        "language": {
          "url": "dataTable/i18n/"+dataTableLang+".json"
        },
        "fnDrawCallback": function () {
          $('.viewFavoriteStandTestimony').off();
          $('.viewFavoriteStandTestimony')
                  .on("click",
                          function () {
                            that.visitStand($(this).attr('standId'));
                          });
          $('.deleteFavoriteTestimonyButton').off();
          $('.deleteFavoriteTestimonyButton')
                  .on('click',
                          function () {
                            var index = $(this).attr('id');
                            var testimony = testimoniesFavoritesCache[index];
                            factoryDeleteFavorite(this, testimony.getTestimonyId(), FavoriteEnum.TESTIMONY);
                          });
          $("thead").find('th:last')
                  .removeClass('sorting');
        }
      });
    };
    this.addTestimoniesFavoriteDatas = function(favorites){
    	testimoniesFavoritesCache = favorites;
    	var favoritesData = [];
      $(favorites).each(function (index, testimony) {
        var testimonyName = testimony.getName();
        var testimonyJobTitle = testimony.getJobTitle();
        var testimonyPhoto = testimony.getPhoto();
        var enterpriseName = testimony.getEnterpriseName();
        var standId = testimony.getStandId();
        var enterpriseInfo = '<div class="logo-company-wrapper"> <a href="#" class="logo-company"> <img class="" src="' + testimonyPhoto + '" alt="" /> </a> <span class="name-company" >' + enterpriseName + '</span> </div>';
        var listActions = "<a id='" + standId + "' class='visiter-action stands viewFavoriteStandTestimony'  standId='" + standId + "' title='"+VISITSTANDLABEL+"'>"
                + "<img src='img/icons/icon-visiter.png' alt='' /></a>"
        				+ "<a id='" + index + "' class='visiter-action deleteFavoriteTestimonyButton' title='"+DELETEFROMFAV+"'>"
                + "<img src='img/icons/icon-delete.png' alt='' /></a>";
        var data = [enterpriseInfo, formattingLongWords(enterpriseName, 30), formattingLongWords(testimonyName, 30), 
                    formattingLongWords(testimonyJobTitle, 30), listActions];
        favoritesData.push(data);
      });
      that.addFavoriteDatas(".listFavoriteTestimonies", favoritesData);
    };
    this.initAdvicesFavoriteTable = function(){
    	$('.listFavoriteAdvices').dataTable({
        "aaData": [],
        "aoColumns": [{
            "sTitle": ENTERPRISELABEL
          }, {
            "sTitle": TITLELABEL
          }, {
            "sTitle": ACTIONSLABEL
          }],
        "bAutoWidth": false,
        "bRetrieve": false,
        "bDestroy": true,
        "bFilter": false,
        "bLengthChange": false,
        "iDisplayLength": 10,
        "language": {
          "url": "dataTable/i18n/"+dataTableLang+".json"
        },
        "fnDrawCallback": function () {
          $('.viewFavoriteStandAdvice').off();
          $('.viewFavoriteStandAdvice')
                  .on("click",
                          function () {
                    				that.visitStand($(this).attr('standId'));
                          });
          $('.deleteFavoriteAdviceButton').off();
          $('.deleteFavoriteAdviceButton')
                  .on('click',
                          function () {
                            var index = $(this).attr('id');
                            var advice = advicesFavoritesCache[index];
                            factoryDeleteFavorite(this, advice.getAdviceId(), FavoriteEnum.ADVICE);
                          });
          $("thead").find('th:last')
                  .removeClass('sorting');
        }
      });
    };
    this.addAdvicesFavoriteDatas = function(favorites){
    	advicesFavoritesCache = favorites;
    	var favoritesData = [];
      $(favorites).each(function (index, advice) {
        var adviceTitle = advice.getTitle();
        var enterpriseLogo = advice.getEnterpriseLogo();
        var enterpriseName = advice.getEnterpriseName();
        var standId = advice.getStandId();
        var enterpriseInfo = '<div class="logo-company-wrapper"> <a href="#" class="logo-company"> <img class="" src="' + enterpriseLogo + '" alt="" /> </a> <span class="name-company" >' + enterpriseName + '</span> </div>';
        var listActions = "<a id='" + standId + "' class='visiter-action stands viewFavoriteStandAdvice' standId='" + standId + "' title='"+VISITSTANDLABEL+"'>"
                + "<img src='img/icons/icon-visiter.png' alt='' /></a>"
        				+ "<a id='" + index + "' class='visiter-action deleteFavoriteAdviceButton' title='"+DELETEFROMFAV+"'>"
                + "<img src='img/icons/icon-delete.png' alt='' /></a>";

        var data = [enterpriseInfo, formattingLongWords(adviceTitle, 30), listActions];
        favoritesData.push(data);
      });
      that.addFavoriteDatas(".listFavoriteAdvices", favoritesData);
    };    

    this.initProductsFavoriteTable = function(){
      $('.listFavoriteProducts').dataTable({
        "aaData": [],
        "aoColumns": [{
          "sTitle": ENTERPRISELABEL,
          "sWidth": "15%",
        }, {
          "sTitle": TITLELABEL,
          "sWidth": "20%",
        }, {
          "sTitle": DESCRIPTIONLABEL,
          "sWidth": "50%",
        }, {
          "sTitle": ACTIONSLABEL,
          "sWidth": "15%",
        }],
        "bAutoWidth": false,
        "bRetrieve": false,
        "bDestroy": true,
        "bFilter": false,
        "bLengthChange": false,
        "iDisplayLength": 50,
        "language": {
          "url": "dataTable/i18n/"+dataTableLang+".json"
        },
        "fnDrawCallback": function () {
          $('.viewFavoriteStandProduct').off();
          $('.viewFavoriteStandProduct').on("click",function () {
            that.visitStand($(this).attr('standId'));
          });

          $('.deleteFavoriteProductButton').off();
          $('.deleteFavoriteProductButton').on('click',function () {
            var index = $(this).attr('id');
            var product = productsFavoritesCache[index];
            factoryDeleteFavorite(this, product.getProductId(), FavoriteEnum.PRODUCT);
          });

          $('.viewProduct').off();
          $('.viewProduct').on('click',function () {
            $('.modal-loading').show();
            var index = $(this).attr('id');
            var product = productsFavoritesCache[index];
            var productApercuManage = new $.ProductApercuManage(product, that);
            $('#modal-product-apercu #info-produit-apercu').html('<div id="apercu" class="scroll-pane"></div>')
              .find('#apercu').html(productApercuManage.getDOM());
            $('#info-stand').removeClass('hide');
            $('#info-stand').show();
            $('#contentG').attr('class', 'info-stand');
            $('#info-stand .menu-stand,#info-stand #vue-aerienne-info-stand').hide();
            $('#modal-product-apercu').modal('show');
            $('#info-produit .scroll-pane').jScrollPane({ autoReinitialise: true });
            $('.modal-loading').hide();
          });

          $("thead").find('th:last')
                  .removeClass('sorting');
        }
      });
    };

    this.addProductsFavoriteDatas = function(favorites){
      productsFavoritesCache = favorites;
      var favoritesData = [];
      $(favorites).each(function (index, product) {
        var productTitle = formattingLongWords(product.getProductTitle(), 30);
        var productDescription = substringCustom(formattingLongWords(product.getProductDescription(), 40),100);
        var productId = product.getProductId();
        var enterpriseInfo = '<div class="logo-company-wrapper"> <a href="#" class="logo-company"> <img class="" src="' + product.getStand().getEnterprise().getLogo() + '" alt="" /> </a> <span class="name-company" >' + product.getStand().getEnterprise().getNom() + '</span> </div>';
        var listActions = "<a id='" + product.getStand().idstande + "' class='visiter-action stands viewFavoriteStandProduct'  standId='" + product.getStand().idstande + "' title='"+VISITSTANDLABEL+"'>"
                + "<img src='img/icons/icon-visiter.png' alt='' /></a>"
                +"<a id='" + index + "' class='visiter-action deleteFavoriteProductButton' title='"+DELETEFROMFAV+"'>"
                + "<img src='img/icons/icon-delete.png' alt='' /></a>"
                + "<a id='" + index + "' class='visiter-action viewProduct' title='"+VIEWLABEL+"'>"
                + "<img src='img/icons/icon-apercu.png' alt='' /></a>";

        var data = [enterpriseInfo, formattingLongWords(productTitle, 30), formattingLongWords(productDescription, 30), listActions];
        favoritesData.push(data);
      });
      that.addFavoriteDatas(".listFavoriteProducts", favoritesData);
    };    

    this.initMediasFavoriteTable = function(){
      $('.listFavoriteMedia').dataTable({
        "aaData": [],
        "aoColumns": [{
            "sTitle": ENTERPRISELABEL,
            "sWidth": "20%",
          }, {
            "sTitle": TITLELABEL,
            "sWidth": "40%",
          }, {
            "sTitle": TYPELABEL,
            "sWidth": "20%",
          }, {
            "sTitle": ACTIONSLABEL,
            "sWidth": "20%",
          }],
        "bAutoWidth": false,
        "bRetrieve": false,
        "bDestroy": true,
        "bFilter": false,
        "bLengthChange": false,
        "iDisplayLength": 50,
        "language": {
          "url": "dataTable/i18n/"+dataTableLang+".json"
        },
        "fnDrawCallback": function () {
          $('.viewFavoriteStandMedia').off();
          $('.viewFavoriteStandMedia')
                  .on("click",
                          function () {
                            that.visitStand($(this).attr('standId'));
                          });
          $('.deleteFavoriteMediaButton').off();
          $('.deleteFavoriteMediaButton')
                  .on('click',
                          function () {
                            var index = $(this).attr('id');
                            var media = mediasFavoritesCache[index];
                            factoryDeleteFavorite(this, media.allFilesId, FavoriteEnum.MEDIA);
                          });
          $('.viewMediaFavorites').off();
          $('.viewMediaFavorites')
                  .on('click',
                          function () {
                            var index = $(this).attr('id');
                            that.openMedia(mediasFavoritesCache[index], $("#modal-document-lecteur .document-lecteur"), 
                                $("#modal-document-lecteur .document-lecteur-title"));
                            $('.modal').hide();
                            $("#modal-document-lecteur").modal();
                          });
          $("thead").find('th:last')
                  .removeClass('sorting');
        }
      });
    };

    this.addMediasFavoriteDatas = function(favorites){
    	mediasFavoritesCache = favorites;
    	var favoritesData = [];
      $(favorites).each(function (index, media) {
        var mediaTitle = media.title;
        var enterpriseLogo = media.enterpriseLogo;
        var standId = media.standId;
        var fileType = media.getFileType().getFileTypeName();
        var enterpriseInfo = '<div class="logo-company-wrapper"> <a href="#" class="logo-company"> <img class="" src="' + enterpriseLogo + '" alt="" /> </a> <span class="name-company" >' + htmlEncode(media.getEnterpriseName()) + '</span> </div>';
        var listActions = "<a id='" + standId + "' class='visiter-action stands viewFavoriteStandMedia'  standId='" + standId + "' title='"+VISITSTANDLABEL+"'>"
                + "<img src='img/icons/icon-visiter.png' alt='' /></a>"
        				+ "<a id='" + index + "' href='#' class='visiter-action deleteFavoriteMediaButton' title='"+DELETEFROMFAV+"'>"
                + "<img src='img/icons/icon-delete.png' alt='' /></a>"
                + "<a id='" + index + "' class='visiter-action viewMediaFavorites' title='"+VIEWLABEL+"'>"
                + "<img src='img/icons/icon-apercu.png' alt='' /></a>";

        var data = [enterpriseInfo, formattingLongWords(mediaTitle, 30), fileType, listActions];
        favoritesData.push(data);
      });
      that.addFavoriteDatas(".listFavoriteMedia", favoritesData);
    };
    this.visitStand = function(standId){
      $('#contentG').attr('class', '');
      $('#contentG').attr('class', 'info-stand stand' + standId);
      $(".main-menu .sub-main-menu, .second-menu li a").removeClass("active");
      if (standId == 7) {
        $('a.icon-format3').addClass('active');
      }
      $('#modal-forgot-password').modal('hide');
      $('#modal-forgot-password-Success').modal('hide');
      $('#modal-sign-in').modal('hide');
      $('#page-content').removeClass('no-padding');
      $('#vue-aerienne-info-stand').removeClass('hide');
      $('.genericContent').attr('style', 'display: none;');
      $('#videoHome').hide();
      $('#info-stand').removeClass('hide');
      $('#info-stand').attr('style', 'display: block;');
      $('.menu-stand a,.vue-aerienne-info-stand a').attr('standId', 'standId=' + standId);
      initStandInfoController(standId);
    };
    this.openMedia = function(media, containerHandler, titleHandler){
      //console.log(media);
      if (media != null) {
	      var mediaUrl = media.url;
    		titleHandler.text(formattingLongWords(media.getTitle(), 30));
      	if(media.getFileType().getFileTypeId() != 2){
      		containerHandler.empty();
  	      var mediaContainer = "";
  	      if (mediaUrl.toLowerCase().indexOf(".pdf") !== -1) {
  	        if (!isIE) {
  	        	mediaContainer = $(
  	                  '<iframe height="100%" width="100%" scrolling="no" allowfullscreen webkitallowfullscreen src="vplayer/mozilla-pdf.js/web/viewer.html?file='
  	                  + mediaUrl
  	                  + '" type="text/html" class="ad-wrapper" style="position: relative; height: 100%; width: 100%;"></iframe>'
  	                  + '<div id="" class="captionCon" style="overflow-y: auto; overflow-x: hidden;">'
  	                  + '</div>')
  	                  .css('position', 'relative');
  	        } else {
  	          var filename = getFileName(mediaUrl);
  	          mediaContainer = $(
  	                  '<object id="mySWF" style="width:100%;height: 100%;position:relative;" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000">'
  	                  + '<param name="movie" value="' + staticVars.urlServerStorageSWF + filename + '.swf"  style="position: relative; height: 100%; width: 100%;"/></object>'
  	                  + '<div id="" class="captionCon" style="overflow-y: auto; overflow-x: hidden;">'
  	
  	                  + '</div>');
  	        }
  	      } else {
  	      	mediaContainer = $('<img src="' + mediaUrl + '" class="ad-wrapper2"/>');
  	      }
  	      containerHandler.html(mediaContainer);
  	    }else {
  	    	var videoIdName = "playerVideoFavoris";
  	    	mediaContainer = $('<div id="'+videoIdName+'" class="ad-wrapper"></div>'
  	          + '<div class="captionCon" style="overflow-y: auto; overflow-x: hidden;"></div>');
  	      containerHandler.html(mediaContainer);
  	      playJwPlayer(videoIdName, mediaUrl);
  	    }
      }
    };

    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  ViewListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }
});

