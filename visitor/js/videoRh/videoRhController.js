jQuery.extend({
  VideoRhController: function (model, view) {
    var that = this;
    var listVideosInCache = [];
    /**
     * listen to the view
     */

    var vlist = $.ViewListener({
      launchSearchVideoRh: function (optionVal, searchInput) {
        $('.modal-loading').show();
        model.getVideosRh(searchInput);
      },
      videoRhCriteriaSearch: function (keyWord, enterpriseName) {
        listVideosRh = [];
        //console.log(listVideosInCache);
        //console.log(keyWord);
        //console.log(enterpriseName);

        for (i in listVideosInCache) {
          var videoRH = listVideosInCache[i];
          if (((compareToLowerString(videoRH.getTitle(), keyWord) !== -1)
                  || (compareToLowerString(videoRH.getEnterpriseName(), keyWord) !== -1))
                  && (compareToLowerString(videoRH.getEnterpriseName(), enterpriseName) !== -1)) {
            listVideosRh[videoRH.getAllFilesId()] = videoRH;
          }
        }
        //console.log(listVideosRh);
        view.displayVideos(listVideosRh, keyWord);
      },
    });

    view.addListener(vlist);

    /**
     * listen to the model
     */
    var mlist = $.ModelListener({
      loadVideosRh: function (listVideos, keyWord) {
        listVideosInCache = listVideos;
        var videoList = [];
        for (i in listVideos) {
          var video = listVideos[i];
          if ((compareToLowerString(video.getTitle(), keyWord) !== -1)
                  || (compareToLowerString(video.getEnterpriseName(), keyWord) !== -1)) {
            videoList.push(video);
          }
        }
        view.displayVideos(videoList);
      },
    });

    model.addListener(mlist);

    this.initController = function () {
      view.initView();
      //model.initModel();
    };
  },
});