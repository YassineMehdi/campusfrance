jQuery.extend({
  VideoRhModel: function () {
    var listeners = new Array();
    var that = this;

    this.initModel = function () {
      // this.getVideos();
    };

    //------------- get All Videos -----------//
    this.getVideosRhError = function (xhr, status, error) {
      isSessionExpired(xhr.responseText);
    };

    this.getVideosRh = function (keyWord) {
      listVideosCache = [];
      genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'allfiles/videosRh?idLang=' + getIdLangParam(), 'application/xml', 'xml', '', function (xml) {
        listVideosCache = new Array();
        $(xml).find("allfiles").each(function () {
          var video = new $.AllFiles($(this));
          listVideosCache[video.getAllFilesId()] = video;
        });
        that.notifyLoadVideosRh(listVideosCache, keyWord);
      }, that.getVideosRhError);
    };

    this.notifyLoadVideosRh = function (listVideos, keyWord) {
      $.each(listeners, function (i) {
        listeners[i].loadVideosRh(listVideos, keyWord);
      });
    };

    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  ModelListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }

});