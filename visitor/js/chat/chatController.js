jQuery.extend({
  ChatController: function (model, view) {
    var that = this;
    /**
     * listen to the view
     */

    var vlist = $.ViewListener({
    });

    view.addListener(vlist);

    /**
     * listen to the model
     */
    var mlist = $.ModelListener({
    });

    model.addListener(mlist);

    this.initController = function () {
      view.initView();
      model.initModel();
    };

  },
});