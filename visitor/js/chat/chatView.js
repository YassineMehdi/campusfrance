jQuery.extend({
    ChatView: function() {
        var listeners = new Array();
        var that = this;
        this.firstTime = true;
        this.initView = function() {
            $(document).click(function(event) {
                var targetHandler = $(event.target);
                var parents = targetHandler.parents();
                var targetClass = targetHandler.attr("class");
                if (parents.index($('#ChatApp')) == -1 && parents.index($('#publicTchat')) == -1 && parents.index($('#tchatG')) == -1 && (targetClass == null || targetClass != undefined && targetClass.indexOf('closeStand') == -1)) {
                    $('.zone-pub .tchat').removeClass("open");
                }
            });
            $('.tchatD').empty();
            if ($('#tchatG').length > 0) {
                $('.icon-tchat').unbind("click").bind("click", function() {
                    $(".icon-tchat").blink();
                    if (connectedUser) {
                        $('.zone-pub .tchat').toggleClass('open');
                        if ($("#tchatG .scroll-pane").length > 0) {
                            $(' #tchatG .scroll-pane').jScrollPane({ autoReinitialise: true });
                        }
                        //if (that.firstTime) {
                        //that.launchChatNodeJs();
                        /* 	if(FlashDetect.installed){
		            		$('#tchatG .connecter').remove();
                            getAMSIp(function(ip){
                                 if(ip != undefined && ip != "")
                                     that.initChatPublic("publicTchat", "", ip);
                                 else that.launchChatHtml5();
                            });*/
                        // } else {
                        //that.launchChatHtml5();

                        that.launchChatNodeJs();
                        //}
                        that.firstTime = false;
                        analyticsView.viewPublicChat();
                        $('#logout').click(function() {
                            $('.zone-pub .tchat').removeClass("open");
                        });

                    } else {
                        $('#modal-forgot-password').modal('hide');
                        $('#modal-forgot-password-Success').modal('hide');
                        $("#modal-sign-in").modal();
                    }
                    return false;
                });
            }
        };
        this.launchChatNodeJs = function() {
            console.log("nodejssssss")

            connexion()

            $('.tchatD').load('modules/chat/chatNodejs/public/index.html', function() {
                $('tchatG').removeClass('tchatD');
                $('tchatG').addClass('toggleTchat');
                $('.clearfix').css('display', 'block');
                $('.toggleTchat').on('click', function() {
                    $(this).find('.container').toggleClass('hide');
                })
                $('.closeChat').on('click', function() {
                    $('.clearfix').css('display', 'none')
                })
                setTimeout(function() {
                    drawMessageRoom();
                }, 2000);

                // initChatHtml5();
                //initAthmosphereListener();
                //$('.chatPublicLoading').css('display', 'none');
                /*  $.getScript("modules/chat/chatNodejs/public/socket.io.js");
                 $.getScript("modules/chat/chatNodejs/public/socket.js");*/
                //$.getScript("modules/chat/chatNodejs/public/main.js");
                chatTabs = $('#tabs1').scrollTabs();
                $('#tchatG .scroll-pane').jScrollPane({ autoReinitialise: true });
            });
        };
        this.launchChatHtml5 = function() {
            $('.tchatD').load('modules/chat/chatHtml5/chathtml5Component.html', function() {
                initChatHtml5();
                initAthmosphereListener();
                //$('.chatPublicLoading').css('display', 'none');
                chatTabs = $('#tabs1').scrollTabs();
                $('#tchatG .scroll-pane').jScrollPane({ autoReinitialise: true });
            });
        };
        /******************* Initialise and launch Public Chat************************/
        this.initChatPublic = function(chatFlashContentId, chatInfo, ip) { //
            chatflashvars.serverUrl = SERVER_BC_URL;
            chatflashvars.fmsUrl = FMS_URL.replace(":ip:", ip) + FMS_APPLICATION_CHAT;
            chatflashvars.profileUrl = PROFILE_URL;
            chatflashvars.imagesUrl = IMAGES_URL;
            chatflashvars.state = 'forum';
            //chatflashvars.translateUrl = translateUrl.replace('./','http://'+HOSTNAME_URL+'/visitor/');
            chatflashvars.translateUrl = getTranslatePath();
            chatflashvars.languageId = getIdLangParam();
            chatflashvars.userType = userType;
            //chatflashvars.chatInfo=XMLToString(chatInfo);
            chatflashvars.extenderTimestamp = EXTENDERTIMESTAMP;
            chatflashvars.urlStyleSWF = getStyleChatSWFPath();

            // For version detection, set to min. required Flash Player version, or 0 (or 0.0.0), for no version detection.
            var swfVersionStr = "11.1.0";
            // To use express install, set to playerProductInstall.swf, otherwise the empty string.
            var xiSwfUrlStr = "";
            /*var flashvars={};
             flashvars.id = 1;
             flashvars.type = "Recruiter";*/
            var params = {};
            params.quality = "high";
            params.bgcolor = "#ffffff";
            params.allowscriptaccess = "always";
            params.allowfullscreen = "true";
            var attributes = {};
            attributes.id = "ChatApp";
            attributes.name = "ChatApp";
            attributes.align = "middle";
            var date = new Date();
            var time = date.getTime();
            swfobject.embedSWF(
                "modules/chat/chatFlex/ChatApp.swf?version=" + time, chatFlashContentId,
                "100%", "100%",
                swfVersionStr, xiSwfUrlStr,
                chatflashvars, params, attributes,
                function() {
                    /*setTimeout(function () {
                      $('.chatPublicLoading').css('display', 'none');
                    }, 3000);*/
                });
            // JavaScript enabled so display the flashContent div in case it is not replaced with a swf object.
            swfobject.createCSS("#" + chatFlashContentId, "display:block;text-align:left;");
        };
        /******************* Initialise and launch Public Chat************************/

        this.addListener = function(list) {
            listeners.push(list);
        };
    },
    ViewListener: function(list) {
        if (!list)
            list = {};
        return $.extend(list);
    }
});