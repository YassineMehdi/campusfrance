jQuery.extend({
  SendMessageModel : function() {
    var listeners = new Array();
    var that = this;
    this.initModel = function() {
    };

    this.sendMessageSuccess = function(xml) {
      swal({
        title : "",
        text : "Votre message a été envoyé.",
        type : "success"
      }, function() {
      });
      $('#standSendMessage').val('');
      $('#standSendDescription').val('');
      $('#modal-message .close').trigger('click');
      $('.modal-loading').hide();
    };
    this.sendMessageError = function(xhr, status, error) {
      isSessionExpired(xhr.responseText);
      $('.modal-loading').hide();
    };

    this.sendMessage = function(standId, message) {
      genericAjaxCall('POST', staticVars["urlBackEndCandidate"]
          + 'message/sendMessage?' + standId, 'application/xml', 'xml',
          message.xmlData(), that.sendMessageSuccess, that.sendMessageError);
    };

    this.sendVisitedMessage = function(standId, message) {
      genericAjaxCall('POST', staticVars["urlBackEndCandidate"]
          + 'message/sendVisitedMessage?' + standId, 'application/xml', 'xml',
          message.xmlData(), that.sendMessageSuccess, that.sendMessageError);
    };

    this.addListener = function(list) {
      listeners.push(list);
    };
  },
  ModelListener : function(list) {
    if (!list)
      list = {};
    return $.extend(list);
  }

});