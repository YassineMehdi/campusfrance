jQuery.extend({

  SendMessageView: function () {
    var listeners = new Array();
    var that = this;
    
    this.initView = function () {
      $('#modal-message #standSendMessage, #modal-message #standSendDescription, #modal-message #senderEmail').val('');
      $('#sendMessageBtn').unbind("click").bind('click',function(){
        var standId = $('#sondage-link').attr('standId');
      	$("#send-message").validate({
          errorPlacement: function errorPlacement(error, element) {
            element.before(error);
          },
          rules: {
            standSendMessage: {
              required: true
            },
            standSendDescription: {
              required: true
            },
            senderEmailName: {
              required: true
            },
          }
        });
        
        var valide = $("#send-message").valid();
        if (!valide) {
          $('label.error').hide();
          return false;
        }

        $('.modal-loading').show();
  			var message = new $.Message();
  			message.from = $('#modal-message #senderEmail').val();
  			message.subject = $('#modal-message #standSendMessage').val();
  			message.content = $('#modal-message #standSendDescription').val();
      	if (connectedUser) {
    			$('.modal-loading').show();
	        that.notifySendMessage(standId, message);
        } else {
    			$('.modal-loading').show();
    			message.from = $('#modal-message #senderEmail').val();
	        that.notifySendVisitedMessage(standId, message);
        }
      });
      $('#send-message-link').unbind('click').bind('click',function(){
          //if (connectedUser) {
  	        //$('.modal-loading').show();
          	if(!($("#modal-message").data('bs.modal') || {}).isShown) {
		          factoryActivateFaIcon(this);
		          $('.modal').modal('hide');
		          $('#modal-message').modal('show');
            }
          /*} else {
            getStatusNoConnected();
          }*/
          if (connectedUser) {
          	$('#modal-message #senderEmail').val("******@mail.com");
          	$("#modal-message .send-message-from").hide();
          	$("#modal-message .modal-body").removeClass("disconnected-status");
          } else {
            $('#modal-message #standSendMessage, #modal-message #standSendDescription').val('');
          	$('#modal-message #senderEmail').val("");
          	$("#modal-message .send-message-from").show();
          	$("#modal-message .modal-body").addClass("disconnected-status");
          }
        });
    };
    
    this.notifySendMessage = function(standId, message) {
			$.each(listeners, function(i) {
				listeners[i].sendMessage(standId, message);
			});
		};
		this.notifySendVisitedMessage = function(standId, message){
			$.each(listeners, function(i) {
				listeners[i].sendVisitedMessage(standId, message);
			});
		};
    // this.notifyAddReponse = function (attend,formId) {
      // $.each(listeners, function (i) {
        // listeners[i].addReponse(attend,formId);
      // });
    // };
    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  ViewListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }
});

