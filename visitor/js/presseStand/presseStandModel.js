jQuery.extend({
  PresseStandModel: function () {
    var listeners = new Array();
    var that = this;

    this.initModel = function () {
    };

    //------------- get All Presses -----------//
    this.getPresseStandSuccess = function (xml) {
    	var presses = new Array();
      $(xml).find("advice").each(function () {
        presses.push(new $.Advice($(this)));
      });
      that.notifyLoadPresseStand(presses);
    };
    this.getPresseStandError = function (xhr, status, error) {
      isSessionExpired(xhr.responseText);
    };
    this.getPresseStand = function (standId) {
      genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'advice/standAdvices?' + standId + '&idLang=' + getIdLangParam(), 'application/xml', 
      		'xml', '', that.getPresseStandSuccess, that.getPresseStandError);
    };
    this.notifyLoadPresseStand = function (listPresses) {
      $.each(listeners, function (i) {
        listeners[i].loadPresseStand(listPresses);
      });
    };
    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  ModelListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }

});