jQuery.extend({
  PhotoStandModel: function () {
    var listeners = new Array();
    var that = this;

    this.initModel = function () {
    };

    //------------- get All Photos -----------//
    this.getPhotoStandSuccess = function (xml) {
        var photos = [];
        $(xml).find("allfiles").each(function () {
        	photos.push(new $.AllFiles($(this)));
        });
        that.notifyLoadPhotoStand(photos);
    };
    this.getPhotoStandError = function (xhr, status, error) {
      isSessionExpired(xhr.responseText);
    };

    this.getPhotoStand = function (standId) {
      genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'allfiles/standPhotos?' + standId + '&idLang=' + getIdLangParam(), 
    		  'application/xml', 'xml', '', that.getPhotoStandSuccess, that.getPhotoStandError);
    };

    this.notifyLoadPhotoStand = function (listPhotos) {
      $.each(listeners, function (i) {
        listeners[i].loadPhotoStand(listPhotos);
      });
    };

    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  ModelListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }

});