jQuery.extend({
  PhotoStandController: function (model, view) {
    var that = this;

    /**
     * listen to the view
     */

    var vlist = $.ViewListener({
      launchPhotoStand: function (standId) {
        model.getPhotoStand(standId);
      },
    });

    view.addListener(vlist);

    /**
     * listen to the model
     */
    var mlist = $.ModelListener({
      loadPhotoStand: function (photos) {
        view.displayPhotoStand(photos);
      },
    });

    model.addListener(mlist);

    this.initController = function (standId) {
      view.initView(standId);
      model.initModel();
    };
  },
});