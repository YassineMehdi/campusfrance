jQuery.extend({

	UserMyChatsController : function(model, view) {
		var that=this;
		//listen to the model
		var userMyChatsModelListener = new $.UserMyChatsModelListener({
			historyPublicChatsLoaded : function() {
               view.historyPublicChatsLoaded();
			},
			historyPrivateChatsLoaded : function() {
               view.historyPrivateChatsLoaded();
			},
		});
		model.addListener(userMyChatsModelListener);
		
		// listen to the view
		var userMyChatsViewListener = new $.UserMyChatsViewListener({
			getHistoryPublicChats : function(dateBegin, dateEnd) {
			   model.getHistoryPublicChats(dateBegin, dateEnd);
			},
			getHistoryPrivateChats : function(dateBegin, dateEnd) {
	           model.getHistoryPrivateChats(dateBegin, dateEnd);
			},
		});
		view.addListener(userMyChatsViewListener);

		this.initController=function (){
			listHistoricChatCache = new Array();
			view.initHistory();
			model.initModel();
		};
	},

});
