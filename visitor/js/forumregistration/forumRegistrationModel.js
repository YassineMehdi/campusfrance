jQuery
		.extend({
			Candidate : function(data) {
				var that = this;
				that.candidateId = '';
				that.password = '';
				that.passwordClear = '';
				that.token = '';
				that.phoneNumber = '';
				that.currentCompany = '';
				that.jobTitle = '';
				that.email = '';
				that.se = '';
				that.userProfile = '';
				that.studyLevel = '';
				that.functionCandidate = '';
				that.experienceYears = '';
				that.secteurActs = '';
				that.regions = '';
				that.pdfCvs = '';
				that.cellPhone = '';
				that.gender = '';
				that.birthDate = '';
				that.countryOrigin = '';
				that.areaExpertise = '';
				that.jobSought = '';
				that.formation = '';
				that.candidateLanguages = '';
				that.city = '';
				that.description = '';
				that.place = '';
				that.country = '';
				this.candidate = function(data) {
					if (data) {
						if ($(data).find("candidateId"))
							that.candidateId = $(data).find("candidateId").text();
						if ($(data).find("password"))
							that.password = $(data).find("password").text();
						if ($(data).find("passwordClear"))
							that.passwordClear = $(data).find("passwordClear").text();
						if ($(data).find("token"))
							that.token = $(data).find("token").text();
						if ($(data).find("phoneNumber"))
							that.phoneNumber = $(data).find("phoneNumber").text();
						if ($(data).find("currentCompany"))
							that.currentCompany = $(data).find("currentCompany").text();
						if ($(data).find("jobTitle"))
							that.jobTitle = $(data).find("jobTitle").text();
						if ($(data).find("email"))
							that.email = $(data).find("email").text();
						if ($(data).find("cellPhone"))
							that.cellPhone = $(data).find("cellPhone").text();
						if ($(data).find("gender"))
							that.gender = $(data).find("gender").text();
		        		if ($(data).find("birthDate")){
	          				try {
		            			that.birthDate = new Date($(data).find("birthDate").text()).toJSON();
							}
							catch(err) {
							    that.birthDate = '';
							}
				        }
						if ($(data).find("se"))
							that.se = $(data).find("se").text();
						if ($(data).find("description"))
							that.description = $(data).find("description").text();
						if ($(data).find("userProfileDTO"))
							that.userProfile = new $.UserProfile($(data).find(
									"userProfileDTO"));
						if ($(data).find("studyLevelDTO"))
							that.studyLevel = new $.StudyLevel($(data).find("studyLevelDTO"));
						if ($(data).find("functionCandidateDTO"))
							that.functionCandidate = new $.FunctionCandidate($(data).find(
									"functionCandidateDTO"));
						if ($(data).find("experienceYearsDTO"))
							that.experienceYears = new $.ExperienceYears($(data).find(
									"experienceYearsDTO"));
						that.secteurActs = new Array();
						$(data).find("secteurActDTO").each(function() {
							var secteurAct = new $.SecteurAct($(this));
							that.secteurActs.push(secteurAct);
						});
						that.candidateLanguages = new Array();
						$(data).find("candidateLanguages").each(function() {
							that.candidateLanguages.push(new $.CandidateLanguages($(this)));
						});
						that.regions = new Array();
						$(data).find("regionDTO").each(function() {
							var region = new $.Region($(this));
							that.regions.push(region);
						});
						if ($(data).find("countryOriginDTO"))
							that.countryOrigin = new $.Region($(data)
									.find("countryOriginDTO"));
						if ($(data).find("areaExpertiseDTO"))
							that.areaExpertise = new $.AreaExpertise($(data).find(
									"areaExpertiseDTO"));
						if ($(data).find("jobSoughtDTO"))
							that.jobSought = new $.JobSought($(data).find("jobSoughtDTO"));
						if ($(data).find("formationDTO"))
							that.formation = new $.Formation($(data).find("formationDTO"));
						if ($(data).find("cityDTO"))
							that.city = new $.City($(data).find("cityDTO"));
						that.pdfCvs = new Array();
						$(data).find("pdfCvDTO").each(function() {
							var pdfCv = new $.PdfCv($(this));
							that.pdfCvs.push(pdfCv);
						});
						if ($(data).find("place"))
							that.place = $(data).find("place").text();
						if ($(data).find("countryDTO"))
							that.country = new $.Country($(data).find("countryDTO"));
					}
				};
				this.getCandidateId = function() {
					return that.candidateId;
				};
				this.getPassword = function() {
					return that.password;
				};
				this.getPasswordClear = function() {
					return that.passwordClear;
				};
				this.getToken = function() {
					return that.token;
				};
				this.getPhoneNumber = function() {
					return that.phoneNumber;
				};
				this.getCurrentCompany = function() {
					return that.currentCompany;
				};
				this.getJobTitle = function() {
					return that.jobTitle;
				};
				this.getEmail = function() {
					return that.email;
				};
				this.getCellPhone = function() {
					return that.cellPhone;
				};
				this.getGender = function() {
					return that.gender;
				};
				this.getBirthDate = function() {
					return that.birthDate;
				};
				this.getAreaExpertise = function() {
					return that.areaExpertise;
				};
				this.getJobSought = function() {
					return that.jobSought;
				};
				this.getSe = function() {
					return that.se;
				};
				this.getUserProfile = function() {
					return that.userProfile;
				};
				this.getStudyLevel = function() {
					return that.studyLevel;
				};
				this.getFunctionCandidate = function() {
					return that.functionCandidate;
				};
				this.getExperienceYears = function() {
					return that.experienceYears;
				};
				this.getSecteurActs = function() {
					return that.secteurActs;
				};
				this.getRegions = function() {
					return that.regions;
				};
				this.getCountryOrigin = function() {
					return that.countryOrigin;
				};
				this.getFormation = function() {
					return that.formation;
				};
				this.getPdfCvs = function() {
					return that.pdfCvs;
				};
				this.getCandidateLanguages = function() {
					return that.candidateLanguages;
				};
				this.getCity = function() {
					return that.city;
				};
				this.getDescription = function() {
					return that.description;
				};
				this.getPlace = function() {
					return that.place;
				};
				this.getCountry= function() {
					return that.country;
				};
				this.setCandidateId = function(candidateId) {
					that.candidateId = candidateId;
				};
				this.setPassword = function(password) {
					that.password = password;
				};
				this.setPasswordClear = function(passwordClear) {
					that.passwordClear = passwordClear;
				};
				this.setToken = function(token) {
					that.token = token;
				};
				this.setPhoneNumber = function(phoneNumber) {
					that.phoneNumber = phoneNumber;
				};
				this.setCurrentCompany = function(currentCompany) {
					that.currentCompany = currentCompany;
				};
				this.setJobTitle = function(jobTitle) {
					that.jobTitle = jobTitle;
				};
				this.setEmail = function(email) {
					that.email = email;
				};
				this.setCellPhone = function(cellPhone) {
					that.cellPhone = cellPhone;
				};
				this.setGender = function(gender) {
					that.gender = gender;
				};
				this.setBirthDate = function(birthDate) {
					that.birthDate = birthDate;
				};
				this.setAreaExpertise = function(areaExpertise) {
					that.areaExpertise = areaExpertise;
				};
				this.setJobSought = function(jobSought) {
					that.jobSought = jobSought;
				};
				this.setFormation = function(formation) {
					that.formation = formation;
				};
				this.setSe = function(se) {
					that.se = se;
				};
				this.setUserProfile = function(userProfile) {
					that.userProfile = userProfile;
				};
				this.setStudyLevel = function(studyLevel) {
					that.studyLevel = studyLevel;
				};
				this.setFunctionCandidate = function(functionCandidate) {
					that.functionCandidate = functionCandidate;
				};
				this.setExperienceYears = function(experienceYears) {
					that.experienceYears = experienceYears;
				};
				this.setSecteurActs = function(secteurActs) {
					that.secteurActs = secteurActs;
				};
				this.setRegions = function(regions) {
					that.regions = regions;
				};
				this.setCountryOrigin = function(countryOrigin) {
					that.countryOrigin = countryOrigin;
				};
				this.setPdfCvs = function(pdfCvs) {
					that.pdfCvs = pdfCvs;
				};
				this.setCandidateLanguages = function(candidateLanguages) {
					that.candidateLanguages = candidateLanguages;
				};
				this.setCity = function(city) {
					that.city = city;
				};
				this.setDescription = function(description) {
					that.description = description;
				};
				this.setPlace = function(place) {
					that.place = place;
				};
				this.setCountry = function(country) {
					that.country = country;
				};
				this.update = function(newData) {
					that.candidate(newData);
				};
				this.xmlData = function() {
					var xml = '<candidateDTO>';
					xml += '<candidateId>' + that.candidateId + '</candidateId>';
					xml += '<password>' + htmlEncode(that.password) + '</password>';
					xml += '<passwordClear>' + htmlEncode(that.passwordClear)
							+ '</passwordClear>';
					xml += '<token>' + htmlEncode(that.token) + '</token>';
					xml += '<phoneNumber>' + htmlEncode(that.phoneNumber)
							+ '</phoneNumber>';
					xml += '<cellPhone>' + htmlEncode(that.cellPhone) + '</cellPhone>';
					xml += '<gender>' + htmlEncode(that.gender) + '</gender>';
					xml += '<currentCompany>' + htmlEncode(that.currentCompany)
							+ '</currentCompany>';
					xml += '<jobTitle>' + htmlEncode(that.jobTitle)+ '</jobTitle>';
					xml += '<description>' + htmlEncode(that.description)+ '</description>';
					if (that.birthDate != undefined && that.birthDate != '')
						xml += '<birthDate>' + that.birthDate.toJSON() + '</birthDate>';
					else
						xml += '<birthDate></birthDate>';
					xml += '<email>' + htmlEncode(that.email) + '</email>';
					xml += '<se>' + that.se + '</se>';
					if (that.userProfile != "")
						xml += that.userProfile.xmlData();
					if (that.studyLevel != "")
						xml += that.studyLevel.xmlData();
					if (that.functionCandidate != "")
						xml += that.functionCandidate.xmlData();
					if (that.experienceYears != "")
						xml += that.experienceYears.xmlData();
					xml += '<secteurActDTOlist>';
					for (index in that.secteurActs) {
						var secteurAct = that.secteurActs[index];
						xml += secteurAct.xmlData();
					}
					xml += '</secteurActDTOlist>';
					xml += '<regionDTOlist>';
					for (index in that.regions) {
						var region = that.regions[index];
						xml += '<regionDTO>';
						xml += region.xmlData();
						xml += '</regionDTO>';
					}
					xml += '</regionDTOlist>';
					if (that.countryOrigin != "") {
						xml += '<countryOriginDTO>';
						xml += that.countryOrigin.xmlData();
						xml += '</countryOriginDTO>';
					}
					xml += '<pdfCvDTOList>';
					for (index in that.pdfCvs) {
						var pdfCv = that.pdfCvs[index];
						xml += pdfCv.xmlData();
					}
					xml += '</pdfCvDTOList>';
					xml += '<candidateLanguagesDTOList>';
					for (index in that.candidateLanguages) {
						var candidateLanguage = that.candidateLanguages[index];
						xml += candidateLanguage.xmlData();
					}
					xml += '</candidateLanguagesDTOList>';
					if (that.areaExpertise != "") {
						xml += that.areaExpertise.xmlData();
					}
					if (that.jobSought != "") {
						xml += that.jobSought.xmlData();
					}
					if (that.formation != "") {
						xml += that.formation.xmlData();
					}
					if (that.city != "") {
						xml += that.city.xmlData();
					}
					if (that.country != "") {
						xml += that.country.xmlData();
					}
					xml += '<place>' + htmlEncode(that.place)+ '</place>';
					xml += '</candidateDTO>';
					return xml;
				};
				this.candidate(data);
			},
			UserProfile : function(data) {
				var that = this;
				that.userProfileId = '';
				that.firstName = '';
				that.secondName = '';
				that.avatar = '';
				that.pseudoSkype = '';
				that.webSite = '';
				that.defaultLanguage = null;
				
				this.userProfile = function(data) {
					if ($(data).find("userProfileId"))
						that.userProfileId = $(data).find("userProfileId").text();
					if ($(data).find("firstName"))
						that.firstName = $(data).find("firstName").text();
					if ($(data).find("secondName"))
						that.secondName = $(data).find("secondName").text();
					if ($(data).find("avatar"))
						that.avatar = $(data).find("avatar").text();
					if ($(data).find("pseudoSkype"))
						that.pseudoSkype = $(data).find("pseudoSkype").text();
					if ($(data).find("webSite"))
						that.webSite = $(data).find("webSite").text();
					if ($(data).find("defaultLanguageDTO"))
						that.defaultLanguage = new $.Language($(data).find("defaultLanguageDTO"));
				};
				this.getUserProfileId = function() {
					return that.userProfileId;
				};
				this.getFirstName = function() {
					return that.firstName;
				};
				this.getSecondName = function() {
					return that.secondName;
				};
				this.getAvatar = function() {
					return that.avatar;
				};
				this.getPseudoSkype = function() {
					return that.pseudoSkype;
				};
				this.getDefaultLanguage = function() {
					return that.defaultLanguage;
				};
				this.getWebSite = function() {
					return that.webSite;
				};
				this.setUserProfileId = function(userProfileId) {
					that.userProfileId = userProfileId;
				};
				this.setFirstName = function(firstName) {
					that.firstName = firstName;
				};
				this.setSecondName = function(secondName) {
					that.secondName = secondName;
				};
				this.setAvatar = function(avatar) {
					that.avatar = avatar;
				};
				this.setPseudoSkype = function(pseudoSkype) {
					that.pseudoSkype = pseudoSkype;
				};
				this.setWebSite = function(webSite) {
					that.webSite = webSite;
				};
				this.setDefaultLanguage = function(defaultLanguage) {
					that.defaultLanguage = defaultLanguage;
				};
				this.update = function(newData) {
					that.userProfile(newData);
				};
				this.xmlData = function() {
					var xml = '<userProfileDTO>';
					xml += '<userProfileId>' + that.userProfileId + '</userProfileId>';
					xml += '<firstName>' + htmlEncode(that.firstName) + '</firstName>';
					xml += '<secondName>' + htmlEncode(that.secondName) + '</secondName>';
					xml += '<avatar>' + htmlEncode(that.avatar) + '</avatar>';
					xml += '<webSite>' + htmlEncode(that.webSite) + '</webSite>';
					xml += '<pseudoSkype>' + htmlEncode(that.pseudoSkype)
							+ '</pseudoSkype>';
					if (that.defaultLanguage != null) {
						xml += that.defaultLanguage.xmlData().replace(/languageDTO>/g, "defaultLanguageDTO>");
					}
					xml += '</userProfileDTO>';
					return xml;
				};
				this.userProfile(data);
			},
			StudyLevel : function(data) {
				var that = this;
				that.idStudyLevel = '';
				that.studyLevelName = '';
				this.studyLevel = function(data) {
					if (data) {
						if ($(data).find("idStudyLevel"))
							that.idStudyLevel = $(data).find("idStudyLevel").text();
						if ($(data).find("studyLevelName"))
							that.studyLevelName = $(data).find("studyLevelName").text();
					}
				};
				this.getIdStudyLevel = function() {
					return that.idStudyLevel;
				};
				this.getStudyLevelName = function() {
					return that.studyLevelName;
				};
				this.setIdStudyLevel = function(idStudyLevel) {
					that.idStudyLevel = idStudyLevel;
				};
				this.setStudyLevelName = function(studyLevelName) {
					that.studyLevelName = studyLevelName;
				};
				// Ajout d update
				this.update = function(newData) {
					that.studyLevel(newData);
				};
				this.xmlData = function() {
					var xml = '<studyLevelDTO>';
					xml += '<idStudyLevel>' + that.idStudyLevel + '</idStudyLevel>';
					xml += '<studyLevelName>' + htmlEncode(that.studyLevelName)
							+ '</studyLevelName>';
					xml += '</studyLevelDTO>';
					return xml;
				};
				this.studyLevel(data);
			},
			FunctionCandidate : function(data) {
				var that = this;
				that.idFunction = '';
				that.functionName = '';
				this.functionCandidate = function(data) {
					if (data) {
						if ($(data).find("idFunction"))
							that.idFunction = $(data).find("idFunction").text();
						if ($(data).find("functionName"))
							that.functionName = $(data).find("functionName").text();
					}
				};
				this.getIdFunction = function() {
					return that.idFunction;
				};
				this.getFunctionName = function() {
					return that.functionName;
				};
				this.setIdFunction = function(idFunction) {
					that.idFunction = idFunction;
				};
				this.setFunctionName = function(functionName) {
					that.functionName = functionName;
				};
				this.update = function(newData) {
					that.functionCandidate(newData);
				};
				this.xmlData = function() {
					var xml = '<functionCandidateDTO>';
					xml += '<idFunction>' + that.idFunction + '</idFunction>';
					xml += '<functionName>' + htmlEncode(that.functionName)
							+ '</functionName>';
					xml += '</functionCandidateDTO>';
					return xml;
				};
				this.functionCandidate(data);
			},
			ExperienceYears : function(data) {
				var that = this;
				that.idExperienceYears = '';
				that.experienceYearsName = '';
				this.experienceYears = function(data) {
					if ($(data).find("idExperienceYears"))
						that.idExperienceYears = $(data).find("idExperienceYears").text();
					if ($(data).find("experienceYearsName"))
						that.experienceYearsName = $(data).find("experienceYearsName")
								.text();
				};
				this.getIdExperienceYears = function() {
					return that.idExperienceYears;
				};
				this.getExperienceYearsName = function() {
					return that.experienceYearsName;
				};
				this.setIdExperienceYears = function(idExperienceYears) {
					that.idExperienceYears = idExperienceYears;
				};
				this.setExperienceYearsName = function(experienceYearsName) {
					that.experienceYearsName = experienceYearsName;
				};
				this.update = function(newData) {
					that.experienceYears(newData);
				};
				this.xmlData = function() {
					var xml = '<experienceYearsDTO>';
					xml += '<idExperienceYears>' + that.idExperienceYears
							+ '</idExperienceYears>';
					xml += '<experienceYearsName>' + htmlEncode(that.experienceYearsName)
							+ '</experienceYearsName>';
					xml += '</experienceYearsDTO>';
					return xml;
				};
				this.experienceYears(data);
			},
			SecteurAct : function(data) {
				var that = this;
				that.idsecteur = 0;
				that.name = '';
				this.secteurAct = function(data) {
					if (data) {
						if ($(data).find("idsecteur"))
							that.idsecteur = $(data).find("idsecteur").text();
						if ($(data).find("name"))
							that.name = $(data).find("name").text();
					}
				};
				this.getIdsecteur = function() {
					return that.idsecteur;
				};
				this.getName = function() {
					return that.name;
				};
				this.setIdsecteur = function(idsecteur) {
					that.idsecteur = idsecteur;
				};
				this.setName = function(name) {
					that.name = name;
				};
				this.update = function(newData) {
					that.secteurAct(newData);
				};
				this.xmlData = function() {
					var xml = '<secteurActDTO>';
					xml += '<idsecteur>' + that.idsecteur + '</idsecteur>';
					xml += '<name>' + htmlEncode(that.name) + '</name>';
					xml += '</secteurActDTO>';
					return xml;
				};
				this.secteurAct(data);
			},
			Region : function(data) {
				var that = this;
				that.idRegion = '';
				that.regionName = '';
				this.region = function(data) {
					if ($(data).find("idRegion"))
						that.idRegion = $(data).find("idRegion").text();
					if ($(data).find("regionName"))
						that.regionName = $(data).find("regionName").text();
				};
				this.getIdRegion = function() {
					return that.idRegion;
				};
				this.getRegionName = function() {
					return that.regionName;
				};
				this.setIdRegion = function(idRegion) {
					that.idRegion = idRegion;
				};
				this.setRegionName = function(regionName) {
					that.regionName = regionName;
				};
				this.update = function(newData) {
					that.region(newData);
				};
				this.xmlData = function() {
					var xml = '<idRegion>' + that.idRegion + '</idRegion>';
					xml += '<regionName>' + htmlEncode(that.regionName) + '</regionName>';
					return xml;
				};
				this.region(data);
			},
			PdfCv : function(data) {
				var that = this;
				that.idPdfCv = '';
				that.cvName = '';
				that.urlCv = '';
				this.pdfCv = function(data) {
					if ($(data).find("idPdfCv"))
						that.idPdfCv = $(data).find("idPdfCv").text();
					if ($(data).find("cvName"))
						that.cvName = $(data).find("cvName").text();
					if ($(data).find("urlCv"))
						that.urlCv = $(data).find("urlCv").text();
				};
				this.getIdPdfCv = function() {
					return that.idPdfCv;
				};
				this.getCvName = function() {
					return that.cvName;
				};
				this.getUrlCv = function() {
					return that.urlCv;
				};
				this.setIdPdfCv = function(idPdfCv) {
					that.idPdfCv = idPdfCv;
				};
				this.setCvName = function(cvName) {
					that.cvName = cvName;
				};
				this.setUrlCv = function(urlCv) {
					that.urlCv = urlCv;
				};
				this.update = function(newData) {
					that.pdfCv(newData);
				};
				this.xmlData = function() {
					var xml = '<pdfCvDTO>';
					xml += '<idPdfCv>' + that.idPdfCv + '</idPdfCv>';
					xml += '<urlCv>' + htmlEncode(that.urlCv) + '</urlCv>';
					xml += '<cvName>' + htmlEncode(that.cvName) + '</cvName>';
					xml += '</pdfCvDTO>';
					return xml;
				};
				this.pdfCv(data);
			},
			AreaExpertise : function(data) {
				var that = this;
				that.areaExpertiseId = '';
				that.areaExpertiseName = '';
				this.areaExpertise = function(data) {
					if ($(data).find("areaExpertiseId"))
						that.areaExpertiseId = $(data).find("areaExpertiseId").text();
					if ($(data).find("areaExpertiseName"))
						that.areaExpertiseName = $(data).find("areaExpertiseName").text();
				};
				this.getAreaExpertiseId = function() {
					return that.areaExpertiseId;
				};
				this.getAreaExpertiseName = function() {
					return that.areaExpertiseName;
				};
				this.setAreaExpertiseId = function(areaExpertiseId) {
					that.areaExpertiseId = areaExpertiseId;
				};
				this.setAreaExpertiseName = function(areaExpertiseName) {
					that.areaExpertiseName = areaExpertiseName;
				};
				this.update = function(newData) {
					that.areaExpertise(newData);
				};
				this.xmlData = function() {
					var xml = '<areaExpertiseDTO>';
					xml += '<areaExpertiseId>' + that.areaExpertiseId
							+ '</areaExpertiseId>';
					xml += '<areaExpertiseName>' + htmlEncode(that.areaExpertiseName)
							+ '</areaExpertiseName>';
					xml += '</areaExpertiseDTO>';
					return xml;
				};
				this.areaExpertise(data);
			},
			JobSought : function(data) {
				var that = this;
				that.jobSoughtId = '';
				that.jobSoughtName = '';
				that.enable = '';
				this.jobSought = function(data) {
					if ($(data).find("jobSoughtId"))
						that.jobSoughtId = $(data).find("jobSoughtId").text();
					if ($(data).find("jobSoughtName"))
						that.jobSoughtName = $(data).find("jobSoughtName").text();
					if ($(data).find("enable"))
						that.enable = $(data).find("enable").text();
				};
				this.getJobSoughtId = function() {
					return that.jobSoughtId;
				};
				this.getJobSoughtName = function() {
					return that.jobSoughtName;
				};
				this.getEnable = function() {
					return that.enable;
				};
				this.setJobSoughtId = function(jobSoughtId) {
					that.jobSoughtId = jobSoughtId;
				};
				this.setJobSoughtName = function(jobSoughtName) {
					that.jobSoughtName = jobSoughtName;
				};
				this.setEnable = function(enable) {
					that.enable = enable;
				};
				this.update = function(newData) {
					that.jobSought(newData);
				};
				this.xmlData = function() {
					var xml = '<jobSoughtDTO>';
					xml += '<jobSoughtId>' + that.jobSoughtId + '</jobSoughtId>';
					xml += '<jobSoughtName>' + htmlEncode(that.jobSoughtName)
							+ '</jobSoughtName>';
					xml += '</jobSoughtDTO>';
					return xml;
				};
				this.jobSought(data);
			},
			City : function(data) {
				var that = this;
				that.cityId = '';
				that.cityName = '';
				that.state = '';
				this.city = function(data) {
					if ($(data).find("cityId"))
						that.cityId = $(data).find("cityId").text();
					if ($(data).find("cityName"))
						that.cityName = $(data).find("cityName").text();
					if ($(data).find("stateDTO"))
						that.state = new $.State($(data).find("stateDTO"));
				};
				this.getCityId = function() {
					return that.cityId;
				};
				this.getCityName = function() {
					return that.cityName;
				};
				this.getState = function() {
					return that.state;
				};
				this.setCityId = function(cityId) {
					that.cityId = cityId;
				};
				this.setCityName = function(cityName) {
					that.cityName = cityName;
				};
				this.setState = function(state) {
					that.state = state;
				};
				this.update = function(newData) {
					that.city(newData);
				};
				this.xmlData = function() {
					var xml = '<cityDTO>';
					xml += '<cityId>' + that.cityId + '</cityId>';
					xml += '<cityName>' + htmlEncode(that.cityName) + '</cityName>';
					if (that.state != "") {
						xml += that.state.xmlData();
					}
					xml += '</cityDTO>';
					return xml;
				};
				this.city(data);
			},
			Country : function(data) {
				var that = this;
				that.countryId = '';
				that.countryName = '';
				this.country = function(data) {
					if ($(data).find("countryId"))
						that.countryId = $(data).find("countryId").text();
					if ($(data).find("countryName"))
						that.countryName = $(data).find("countryName").text();
				};
				this.getCountryId = function() {
					return that.countryId;
				};
				this.getCountryName = function() {
					return that.countryName;
				};
				this.setCountryId = function(countryId) {
					that.countryId = countryId;
				};
				this.setCountryName = function(countryName) {
					that.countryName = countryName;
				};
				this.update = function(newData) {
					that.country(newData);
				};
				this.xmlData = function() {
					var xml = '<countryDTO>';
					xml += '<countryId>' + that.countryId + '</countryId>';
					xml += '<countryName>' + htmlEncode(that.countryName) + '</countryName>';
					xml += '</countryDTO>';
					return xml;
				};
				this.country(data);
			},
			State : function(data) {
				var that = this;
				that.stateId = '';
				that.stateName = '';
				that.country = '';
				this.state = function(data) {
					if ($(data).find("stateId"))
						that.stateId = $(data).find("stateId").text();
					if ($(data).find("stateName"))
						that.stateName = $(data).find("stateName").text();
					if ($(data).find("countryDTO"))
						that.country = new $.Country($(data).find("countryDTO"));
				};
				this.getStateId = function() {
					return that.stateId;
				};
				this.getStateName = function() {
					return that.stateName;
				};
				this.getCountry = function() {
					return that.country;
				};
				this.setStateId = function(stateId) {
					that.stateId = stateId;
				};
				this.setStateName = function(stateName) {
					that.stateName = stateName;
				};
				this.setCountry = function(country) {
					that.country = country;
				};
				this.update = function(newData) {
					that.state(newData);
				};
				this.xmlData = function() {
					var xml = '<stateDTO>';
					xml += '<stateId>' + that.stateId + '</stateId>';
					xml += '<stateName>' + htmlEncode(that.stateName) + '</stateName>';
					if (that.country != "") {
						xml += that.country.xmlData();
					}
					xml += '</stateDTO>';
					return xml;
				};
				this.state(data);
			},
			Formation : function(data) {
				var that = this;
				that.formationId = '';
				that.formationName = '';
				that.institutionName = '';
				that.institutionCountry = '';
				that.obtained = '';
				that.obtainedDate = '';
				this.formation = function(data) {
					if ($(data).find("formationId"))
						that.formationId = $(data).find("formationId").text();
					if ($(data).find("formationName"))
						that.formationName = $(data).find("formationName").text();
					if ($(data).find("institutionName"))
						that.institutionName = $(data).find("institutionName").text();
					if ($(data).find("institutionCountry"))
						that.institutionCountry = $(data).find("institutionCountry").text();
					if ($(data).find("obtained"))
						that.obtained = $(data).find("obtained").text();
					if ($(data).find("obtainedDate"))
						that.obtainedDate = new Date($(data).find("obtainedDate").text());
				};
				this.getFormationId = function() {
					return that.formationId;
				};
				this.getFormationName = function() {
					return that.formationName;
				};
				this.getInstitutionName = function() {
					return that.institutionName;
				};
				this.getInstitutionCountry = function() {
					return that.institutionCountry;
				};
				this.getObtained = function() {
					return that.obtained;
				};
				this.getObtainedDate = function() {
					return that.obtainedDate;
				};
				this.setFormationId = function(formationId) {
					that.formationId = formationId;
				};
				this.setFormationName = function(formationName) {
					that.formationName = formationName;
				};
				this.setInstitutionName = function(institutionName) {
					that.institutionName = institutionName;
				};
				this.setInstitutionCountry = function(institutionCountry) {
					that.institutionCountry = institutionCountry;
				};
				this.setObtained = function(obtained) {
					that.obtained = obtained;
				};
				this.setObtainedDate = function(obtainedDate) {
					that.obtainedDate = obtainedDate;
				};
				this.update = function(newData) {
					that.formation(newData);
				};
				this.xmlData = function() {
					var xml = '<formationDTO>';
					xml += '<formationId>' + that.formationId + '</formationId>';
					xml += '<formationName>' + htmlEncode(that.formationName)
							+ '</formationName>';
					xml += '<institutionName>' + htmlEncode(that.institutionName)
							+ '</institutionName>';
					xml += '<institutionCountry>' + htmlEncode(that.institutionCountry)
							+ '</institutionCountry>';
					xml += '<obtained>' + that.obtained + '</obtained>';
					if (that.obtainedDate != undefined)
						xml += '<obtainedDate>' + that.obtainedDate.toJSON()
								+ '</obtainedDate>';
					else
						xml += '<obtainedDate></obtainedDate>';
					xml += '</formationDTO>';
					return xml;
				};
				this.formation(data);
			},
			CandidateLanguages : function(data) {
				var that = this;
				that.languageUserProfile = '';
				that.languageLevel = '';

				this.candidateLanguages = function(data) {
					if ($(data).find("languageUserProfile"))
						that.languageUserProfile = new $.LanguageUserProfile($(data).find(
								"languageUserProfile"));
					if ($(data).find("languageLevel"))
						that.languageLevel = new $.LanguageLevel($(data).find(
								"languageLevel"));
				};
				this.getLanguageUserProfile = function() {
					return that.languageUserProfile;
				};
				this.getLanguageLevel = function() {
					return that.languageLevel;
				};
				this.setLanguageUserProfile = function(languageUserProfile) {
					that.languageUserProfile = languageUserProfile;
				};
				this.setLanguageLevel = function(languageLevel) {
					that.languageLevel = languageLevel;
				};
				this.update = function(newData) {
					that.candidateLanguages(newData);
				};
				this.xmlData = function() {
					var xml = '<candidateLanguages>';
					if (that.languageUserProfile != "")
						xml += that.languageUserProfile.xmlData();
					if (that.languageLevel != "")
						xml += that.languageLevel.xmlData();
					xml += '</candidateLanguages>';
					return xml;
				};
				this.candidateLanguages(data);
			},
			LanguageUserProfile : function(data) {
				var that = this;
				that.languageUserProfileId = '';
				that.languageUserProfileName = '';
				this.languageUserProfile = function(data) {
					if ($(data).find("languageUserProfileId"))
						that.languageUserProfileId = $(data).find("languageUserProfileId")
								.text();
					if ($(data).find("languageUserProfileName"))
						that.languageUserProfileName = $(data).find(
								"languageUserProfileName").text();
				};
				this.getLanguageUserProfileId = function() {
					return that.languageUserProfileId;
				};
				this.getLanguageUserProfileName = function() {
					return that.languageUserProfileName;
				};
				this.setLanguageUserProfileId = function(languageUserProfileId) {
					that.languageUserProfileId = languageUserProfileId;
				};
				this.setLanguageUserProfileName = function(languageUserProfileName) {
					that.languageUserProfileName = languageUserProfileName;
				};
				this.update = function(newData) {
					that.languageUserProfile(newData);
				};
				this.xmlData = function() {
					var xml = '<languageUserProfile>';
					xml += '<languageUserProfileId>' + that.languageUserProfileId
							+ '</languageUserProfileId>';
					xml += '<languageUserProfileName>'
							+ htmlEncode(that.languageUserProfileName)
							+ '</languageUserProfileName>';
					xml += '</languageUserProfile>';
					return xml;
				};
				this.languageUserProfile(data);
			},
			LanguageLevel : function(data) {
				var that = this;
				that.languageLevelId = '';
				that.languageLevelName = '';
				this.languageLevel = function(data) {
					if ($(data).find("languageLevelId"))
						that.languageLevelId = $(data).find("languageLevelId").text();
					if ($(data).find("languageLevelName"))
						that.languageLevelName = $(data).find("languageLevelName").text();
				};
				this.getLanguageLevelId = function() {
					return that.languageLevelId;
				};
				this.getLanguageLevelName = function() {
					return that.languageLevelName;
				};
				this.setLanguageLevelId = function(languageLevelId) {
					that.languageLevelId = languageLevelId;
				};
				this.setLanguageLevelName = function(languageLevelName) {
					that.languageLevelName = languageLevelName;
				};
				this.update = function(newData) {
					that.languageLevel(newData);
				};
				this.xmlData = function() {
					var xml = '<languageLevel>';
					xml += '<languageLevelId>' + that.languageLevelId
							+ '</languageLevelId>';
					xml += '<languageLevelName>' + htmlEncode(that.languageLevelName)
							+ '</languageLevelName>';
					xml += '</languageLevel>';
					return xml;
				};
				this.languageLevel(data);
			},
			Language: function(data){
				var that = this;
				that.languageId='';
				that.languageName='';
				
				that.language = function(data){
					if(data){
						if($(data).find("idlangage")) 
							that.languageId=$(data).find("idlangage").text();
						if($(data).find("name")) 
							that.languageName=$(data).find("name").text();
					}
				};
				
				this.getLanguageId= function(){	
					return that.languageId;
				};
				
				this.getLanguageName = function(){	
					return that.languageName;
				};
				
				this.setLanguageId = function(languageId){
					that.languageId=languageId;
				};
				
				this.setLanguageName = function(languageName){
					that.languageName=languageName;
				};
				
				this.update = function(newData){
					that.language(newData);
				};
				
				this.xmlData = function(){
					var xml='<languageDTO>';
					xml+='<idlangage>'+that.languageId+'</idlangage>';
					xml+='<name>'+htmlEncode($.trim(that.languageName))+'</name>';
					xml+='</languageDTO>';
					return xml;
				};
				
				this.language(data);
			},
			ForumRegistrationModel : function() {
				var that = this;
				var listeners = new Array();
				this.initModel = function() {
					this.fillActivitySectors();
					this.fillCountries();
					this.fillLanguages();
		      this.fillFunctionCandidates();
				};
				// ------------- Activity sector ----------- //
				this.fillActivitySectorsSuccess = function(xml) {
					var activitySectors = [];
					$(xml).find('secteurActDTO').each(function() {
						var secteurAct = new $.SecteurAct($(this));
						activitySectors.push(secteurAct);
					});
					that.notifyLoadActivitySectors(activitySectors);
				};
				this.fillActivitySectorsError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};
				// Activation page scripts
				this.fillActivitySectors = function() {
					genericAjaxCall('GET', staticVars["urlBackEndCandidate"]
							+ 'secteuract?idLang=' + getIdLangParam(), 'application/xml',
							'xml', '', that.fillActivitySectorsSuccess,
							that.fillActivitySectorsError);
				};
				this.notifyLoadActivitySectors = function(activitySectors) {
					$.each(listeners, function(i) {
						listeners[i].loadActivitySectors(activitySectors);
					});
				};
				// --------- END ---------------------//
			  //------------- Function Candidates -----------//
		    this.fillFunctionCandidatesSuccess = function (xml) {
		      var functionCandidates = [];
		      $(xml).find('functionCandidateDTO').each(function () {
		        var functionCandidate = new $.FunctionCandidate($(this));
		        functionCandidates.push(functionCandidate);
		      });
		      that.notifyLoadFunctionCandidates(functionCandidates);
		    };
		    this.fillFunctionCandidatesError = function (xhr, status, error) {
		      isSessionExpired(xhr.responseText);
		    };
			  // --------- END ---------------------//
			  // Function Candidates page scripts
		    this.fillFunctionCandidates = function () {
		      genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'functionCandidate?idLang=' + getIdLangParam(),
		              'application/xml', 'xml', '', that.fillFunctionCandidatesSuccess,
		              that.fillFunctionCandidatesError);
		    };
				// ------------- Country -----------//
				this.fillCountriesSuccess = function(xml) {
					var countries = [];
					$(xml).find('countryDTO').each(function() {
						var country = new $.Country($(this));
						countries.push(country);
					});
					that.notifyLoadCountries(countries);
				};
				this.fillCountriesError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};
				// Country page scripts
				this.fillCountries = function() {
					genericAjaxCall('GET', staticVars["urlBackEndCandidate"]
							+ 'country?idLang=' + getIdLangParam(), 'application/xml', 'xml',
							'', that.fillCountriesSuccess, that.fillCountriesError);
				};
				this.notifyLoadCountries = function(countries) {
					$.each(listeners, function(i) {
						listeners[i].loadCountries(countries);
					});
				};
				// --------- END ---------------------//
				// ------------- State -----------//
				this.fillStatesSuccess = function(xml) {
					var states = [];
					$(xml).find('stateDTO').each(function() {
						var state = new $.State($(this));
						states.push(state);
					});
					that.notifyStates(states);
				};
				this.fillStatesError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};
				// State page scripts
				this.fillStates = function(countryId) {
					genericAjaxCall('GET', staticVars["urlBackEndCandidate"]
							+ 'state/by/country?idLang=' + getIdLangParam() + "&countryId=" + countryId, 'application/xml', 'xml',
							'', that.fillStatesSuccess, that.fillStatesError);
				};
				this.notifyStates = function(states) {
					$.each(listeners, function(i) {
						listeners[i].loadStates(states);
					});
				};
				// --------- END ---------------------//
				// ------------- City -----------//
				this.fillCitiesSuccess = function(xml) {
					var cities = [];
					$(xml).find('cityDTO').each(function() {
						var city = new $.City($(this));
						cities.push(city);
					});
					that.notifyCities(cities);
				};
				this.fillCitiesError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};
				// City page scripts
				this.fillCities = function(stateId) {
					genericAjaxCall('GET', staticVars["urlBackEndCandidate"]
							+ 'city/by/state?idLang=' + getIdLangParam() + "&stateId=" + stateId, 'application/xml', 'xml',
							'', that.fillCitiesSuccess, that.fillCitiesError);
				};
				this.notifyCities = function(cities) {
					$.each(listeners, function(i) {
						listeners[i].loadCities(cities);
					});
				};
				// --------- END ---------------------//
				// ------------- Language -----------//
				this.fillLanguagesSuccess = function(xml) {
					var languages = [];
					$(xml).find('languaget').each(function() {
						var language = new $.Language($(this));
						languages.push(language);
					});
					that.notifyLoadLanguages(languages);
				};
				this.fillLanguagesError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
				};
				// Language page scripts
				this.fillLanguages = function() {
					genericAjaxCall('GET', staticVars["urlBackEndCandidate"]
							+ 'languaget?idLang=' + getIdLangParam(), 'application/xml', 'xml',
							'', that.fillLanguagesSuccess, that.fillLanguagesError);
				};
				this.notifyLoadLanguages = function(languages) {
					$.each(listeners, function(i) {
						listeners[i].loadLanguages(languages);
					});
				};
				// --------- END ---------------------//
				// ------------- Register Candidate -----------//
				this.registerCandidateSuccess = function(xml) {
					var candidate = new $.Candidate(xml);
					for ( var index in candidate.getPdfCvs()) {
						var pdfCv = candidate.getPdfCvs()[index];
						that.extracteCV(candidate.getToken(), pdfCv.getUrlCv());
					}
					that.notifyLoadSuccess();
				};
				this.registerCandidateError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
					that.notifyLoadError(xhr);
				};
				// Function Candidates page scripts
				this.registerCandidate = function(candidate) {
					genericAjaxCall('POST', staticVars["urlBackEndCandidate"]
							+ 'candidate/register?idLang=' + getIdLangParam(),
							'application/xml', 'xml', candidate.xmlData(),
							that.registerCandidateSuccess, that.registerCandidateError);
				};
				this.extracteCV = function(token, urlCv) {
					genericAjaxCall('POST', staticVars["urlBackEndCandidate"]
							+ 'cvkeyword/addKeywords?token=' + token + '&urlCv=' + urlCv,
							'application/xml', 'xml', "", "", "");
				};
				// ------------- Check Email -----------//
				this.checkEmailSuccess = function(xml) {
					that.notifyCheckEmailSuccess($(xml).find('b').text());
				};
				this.checkEmailError = function(xhr, status, error) {
					that.notifyCheckEmailError(xhr);
				};
				this.checkEmail = function(email) {
					genericAjaxCall('POST', staticVars["urlBackEndCandidate"]
							+ 'candidate/check/email?email=' + email, 'application/xml',
							'xml', '', that.checkEmailSuccess, that.checkEmailError);
				};
				// --------- END ---------------------//
				// ------------- Check Email -----------//
				this.getCandidateInfoSuccess = function(xml) {
					that.notifyGetCandidateInfoSuccess(new $.Candidate(xml));
				};
				this.getCandidateInfoError = function(xhr, status, error) {
					isSessionExpired(xhr.responseText);
					that.notifyLoadCandidateInfoError(xhr);
				};
				this.getCandidateInfo = function(email, pwd) {
					genericAjaxCall('GET', staticVars["urlBackEndCandidate"]
							+ 'candidate/register?email=' + email + '&pwd=' + $.sha1(pwd),
							'application/xml', 'xml', '', that.getCandidateInfoSuccess,
							that.getCandidateInfoError);
				};
				// --------- END ---------------------//

				// ------------- Captcha -----------//
				this.verifyCaptchaRegister =  function(candidate, response) {
					$.ajax({
						type: "POST",
						url: staticVars.urlValidateCaptcha,
						dataType: "text",
						data : {'g-recaptcha-response':response},
						contentType : "application/x-www-form-urlencoded; charset=UTF-8",
							success: function(text){
								if(text.trim() == 'true'){
									that.registerCandidate(candidate);
								}else{
									$("body").removeClass("loading");
								}
							},
							error: function(){
								$("body").removeClass("loading");
							},
						});
				};
				// --------- END ---------------------//
				this.notifyLoadFunctionCandidates = function(functionCandidates) {
					$.each(listeners, function(i) {
						listeners[i].functionCandidates(functionCandidates);
					});
				};
				this.notifyLoadSuccess = function() {
					$.each(listeners, function(i) {
						listeners[i].candidateSuccess();
					});
				};
				this.notifyLoadError = function(xhr) {
					$.each(listeners, function(i) {
						listeners[i].candidateError(xhr);
					});
				};
				this.notifyCheckEmailSuccess = function(exist) {
					$.each(listeners, function(i) {
						listeners[i].checkEmailSuccess(exist);
					});
				};
				this.notifyGetCandidateInfoSuccess = function(candidate) {
					$.each(listeners, function(i) {
						listeners[i].getCandidateInfoSuccess(candidate);
					});
				};
				this.notifyLoadCandidateInfoError = function(xhr) {
					$.each(listeners, function(i) {
						listeners[i].loadCandidateInfoError(xhr);
					});
				};
				this.notifyCheckEmailError = function(xhr) {
					$.each(listeners, function(i) {
						listeners[i].checkEmailError(xhr);
					});
				};

				// --------- END ---------------------//

				this.addListener = function(list) {
					listeners.push(list);
				};
			},
			ForumRegistrationModelListener : function(list) {
				if (!list)
					list = {};
				return $.extend(list);
			}
		});


