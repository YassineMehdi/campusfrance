jQuery.extend({
	ForumRegistrationController : function(model, view) {
		var that = this;
		var forumRegistrationModelListener = new $.ForumRegistrationModelListener({
			loadActivitySectors : function(activitySectors) {
				view.displayActivitySectors(activitySectors);
			},
			candidateSuccess : function() {
				view.displayCandidateSuccess();
			},
			candidateError : function(xhr) {
				view.displayCandidateError(xhr);
			},
			checkEmailSuccess : function(exist) {
				view.displayCheckEmailSuccess(exist);
			},
			getCandidateInfoSuccess : function(candidate) {
				view.displayCandidateInfoSuccess(candidate);
			},
			loadCandidateInfoError : function(xhr) {
				view.displayCandidateInfoError(xhr);
			},
			checkEmailError : function(xhr) {
				view.checkEmailError(xhr);
			},
			loadCountries : function(countries) {
				view.displayCountries(countries);
			},
			loadStates : function(states) {
				view.displayStates(states);
			},
			loadCities : function(cities) {
				view.displayCities(cities);
			},
			loadLanguages : function(languages) {
				view.displayLanguages(languages);
			},
      functionCandidates: function (functionCandidates) {
        view.displayFunctionCandidates(functionCandidates);
      },
		});
		model.addListener(forumRegistrationModelListener);

		// listen to the view
		var forumRegistrationViewListener = new $.ForumRegistrationViewListener({
			registerCandidate : function(candidate) {
				model.registerCandidate(candidate);
			},
			checkEmail : function(email) {
				model.checkEmail(email);
			},
			getCandidateInfo : function(email, pwd) {
				model.getCandidateInfo(email, pwd);
			},
			getStatesByCountryId : function(countryId){
				model.fillStates(countryId);
			},
			getCitiesByStateId : function(stateId){
				model.fillCities(stateId);
			},
			verifyCaptchaRegister : function(candidate, response) {
				model.verifyCaptchaRegister(candidate, response);
			},
		});
		view.addListener(forumRegistrationViewListener);

		this.initController = function() {
			view.initView();
			model.initModel();
		};
		that.initController();
	}

});


