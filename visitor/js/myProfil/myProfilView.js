jQuery.extend({
    MyProfilView: function () {
        var listeners = new Array();
        var that = this;
        this.secteurActuel = '';
        this.country = '';
        this.ctate = '';
        this.city = '';
        this.stateId = null;
        this.cityId = null;

        this.displayCountries = function(countries) {
          var data = [];
          for ( var i in countries) {
            var country = countries[i];
            data.push({
              value : replaceSpecialChars(country.getCountryId()),
              text : replaceSpecialChars(country.getCountryName())
            });
          }
          generateOption(data, that.Country);
          that.Select(that.Country);
        };
        this.displayStates = function (states) {
          var data = [];
          for (var i in states) {
            var state = states[i];
            data.push({
              value: replaceSpecialChars(state.getStateId()),
              text: replaceSpecialChars(state.getStateName()),
            });
          }
          generateOption(data, that.State);
          that.Select(that.State);
          if(that.stateId != undefined) 
            $(that.State).val(that.stateId).material_select('update');
        };
        //-------- Display Cities ------------//
        this.displayCities = function (cities) {
          var data = [];
          for (var i in cities) {
            var city = cities[i];
            data.push({
              value: replaceSpecialChars(city.getCityId()),
              text: replaceSpecialChars(city.getCityName()),
            });
          }
          generateOption(data, that.City);
          that.Select(that.City);
          if(that.cityId != undefined) 
            $(that.City).val(that.cityId).material_select('update');
        };

        this.loadProfileActivitySectors = function (activitySectors) {
            var data = [];
            for ( var i in activitySectors) {
              var activitySector = activitySectors[i];
              var secteurId = activitySector.idsecteur;
              var secteurName = activitySector.name;
              data.push({
                value : replaceSpecialChars(secteurId),
                text : replaceSpecialChars(secteurName)
              });
            }
            generateOption(data, that.SecteurActuel);
            that.MultiSelect(that.SecteurActuel);
            $('.ui-multiselect-checkboxes li span').click(function() {
              if ($(this).hasClass('active')) {
                $(this).removeClass('active');
              } else {
                $(this).addClass('active');
              }
            });
        };
        
        this.loadProfileFunctionCandidates = function (functionCandidates) {
          var data = [];
          $(that.ProfilSelect).empty();

          for (index in functionCandidates) {
              var functionCandidate = functionCandidates[index];
              var functionCandidateId = functionCandidate.getIdFunction();
              var functionCandidateName = functionCandidate.getFunctionName();
              data.push({
                  value: replaceSpecialChars(functionCandidateId),
                  text: replaceSpecialChars(functionCandidateName)
              });
          }
          generateOption(data, $(that.ProfilSelect));
          that.Select($(that.ProfilSelect));
        };
        this.MultiSelect = function(Selector) {
          $(Selector).multiselect({
            noneSelectedText : SELECTLABEL,
            selectedList : 3,
            header : false,
            selectedText : '# ' + SELECTED_LABEL,
          });
        };
        this.Select = function(SelectSelector) {
          $(SelectSelector).material_select();
          // that.scrollbarMousedown(SelectSelector);
        };
        //-------- Display Candidate Info Success ------------//
        this.displayCandidateInfoSuccess = function(candidate){
          var secteurActs = candidate.getSecteurActs();
          if(secteurActs != null){
            var secteurActIds = [];
            for(var i=0; i<secteurActs.length ; i++){
              var secteurAct = secteurActs[i];
              secteurActIds.push(secteurAct.getIdsecteur());
            }
            $(that.SecteurActuel).val(secteurActIds).multiselect("refresh");
          }
          /*if(candidate.getCity() != null) {
            if(candidate.getCity().getState() != null) {
              that.stateId = candidate.getCity().getState().getStateId();
              if(candidate.getCity().getState().getCountry() != null) {
                that.cityId = candidate.getCity().getCityId();
                $(that.Country).val(candidate.getCity().getState().getCountry().getCountryId()).material_select('update');
              }
            }
          }*/
					// ---- Get Place
					$('#place').val(candidate.getPlace());
					if(candidate.getCountry() != null && candidate.getCountry().getCountryId() != '') {
						$(that.Country).val(candidate.getCountry().getCountryId()).material_select('update');
					}
					var functionCandidate = candidate.getFunctionCandidate();
	        if(functionCandidate != null){
	        	$(that.ProfilSelect).val(functionCandidate.getIdFunction()).material_select('update');
	        }
          //that.notifyStatesByCountryId($(that.Country).val());
          //that.notifyCitiesByStateId(that.stateId);
    	    $(".tab-content .input-field > span.caret").remove();
          $('.modal-loading').hide();
          /************* END *******************************/
        };
        //-------- Display Candidate Success Save ------------//
        this.displayCandidateSuccess = function () {
            swal({
                title: "",
                text: UPDATESUCCESSLABEL,
                type: "success"},
            function () {
            });
            $('.modal-loading').hide();
        };
        //-------- Display States ------------//
        
        this.validateProfilForm = function () {
        	$("#monProfil").validate({
            errorPlacement: function errorPlacement(error, element) {
              element.before(error);
            },
            rules: {
              activity_sector: {
                required: true
              },
              country : {
                required : true
              },
              state : {
                required : true
              },
              city : {
                required : true
              },
              place : {
              	required: true
              },
            }
          });
        };
        this.beforeInitView = function(){
          var dom = $('#monProfil');
          /*$country = dom.find("#country");
          $state = dom.find("#state");
          $city = dom.find("#city");
          $activitySector = dom.find("#activity-sector-select");*/

          this.SecteurActuel = '#activity-sector-select';
          this.City = '#city';
          this.Country = '#country';
          this.State = '#state';
          this.ProfilSelect = '#profile-select';
          
          that.validateProfilForm();


          generateOption([], that.Country);
          that.Select([].Country);
          /*generateOption([], that.City);
          that.Select(that.City);
          generateOption([], that.State);
          that.Select(that.State);
          $(that.Country).unbind('change').bind('change', function(e) {
            that.stateId = null;
            that.cityId = null;
            that.notifyStatesByCountryId($(this).val());
          });
          $(that.State).unbind('change').bind('change', function(e) {
            that.cityId = null;
            that.notifyCitiesByStateId($(this).val());
          });*/
          dom.find("#envoyer").unbind("click").bind("click", function () {
            //console.log('inside envoyer');
            that.validateProfilForm();
          	$('#monProfil select').attr('style', 'display:block');
						var $valid = $("#monProfil").valid();
						if (!$valid) {
							if (defaultBrowser)
								$("#monProfil select[multiple='multiple']").attr('style', 'display:none');
							else
								$('#monProfil select').attr('style', 'display:none');
							$("#g-recaptcha-response").hide();
							return false;
						}
            $('.modal-loading').show();
            var candidate = new $.Candidate();
            var functionCandidate = new $.FunctionCandidate();
            if ($(that.SecteurActuel).val()) {
              var secteurActs = [];
              var secteurAct = null;
              var selectSecteur = $(that.SecteurActuel).val();
              for (index in selectSecteur) {
                secteurAct = new $.SecteurAct();
                secteurAct.setIdsecteur(selectSecteur[index]);
                secteurActs.push(secteurAct);
              }
              // ---- Set Secteur Acts to candidate
              candidate.setSecteurActs(secteurActs);
            }
            /************** Function Candidate *********************/
            //---- Set Id Function
            functionCandidate.setIdFunction($(that.ProfilSelect).val());
            //---- Set FunctionCandidate to candidate
            candidate.setFunctionCandidate(functionCandidate);
            /************* END *******************************/
            /*var city = new $.City();
            city.setCityId($('#city').val());
            // ---- Set City to candidate
            candidate.setCity(city);*/
            //console.log(candidate);
						// ---- Set Place
						candidate.setPlace($('#place').val().trim());
            /** *********** Country ******************** */
						var country = new $.Country();
						country.setCountryId($('#country').val());
						// ---- Set Country to candidate
						candidate.setCountry(country);
						/** *********** END ****************************** */
            that.notifyRegisterCandidate(candidate);

          	if(defaultBrowser) $("#monProfil select[multiple='multiple']").attr('style', 'display:none');
          	else $('#monProfil select').attr('style', 'display:none');
	        });
        };
      	this.getCandidateLanguageByIds = function(languageUserProfileId, languageLevelId){
          languageLevel = new $.LanguageLevel;
          languageUserProfile = new $.LanguageUserProfile;
          candidateLanguage = new $.CandidateLanguages;
          languageLevel.setLanguageLevelId(languageLevelId);
          languageUserProfile.setLanguageUserProfileId(languageUserProfileId);
          candidateLanguage.setLanguageUserProfile(languageUserProfile);
          candidateLanguage.setLanguageLevel(languageLevel);
          return candidateLanguage;
      	};
        this.initView = function () {
          	$('.modal-loading').show();
            $('.genericContent').hide();
            $('#myProfil').show();
            $('#page-content').removeClass('no-padding');
            $('#myProfil .scroll-pane').jScrollPane({ autoReinitialise: true });
						if (defaultBrowser)
							$("#monProfil select:not([multiple='multiple'])").addClass("browser-default");
						else
							$('#monProfil select').attr('style', 'display:none');
        };
        that.beforeInitView();

        this.notifyRegisterCandidate = function (candidate) {
            $.each(listeners, function (i) {
                listeners[i].registerCandidate(candidate);
            });
        };

        this.notifyStatesByCountryId = function(countryId) {
          $.each(listeners, function(i) {
            listeners[i].getStatesByCountryId(countryId);
          });
        };      

        this.notifyCitiesByStateId = function(stateId) {
          $.each(listeners, function(i) {
            listeners[i].getCitiesByStateId(stateId);
          });
        };

        this.notifyViewInit = function (regions) {
            $.each(listeners, function (i) {
                listeners[i].notifyViewInit(regions);
            });
        };

        this.addListener = function (list) {
            listeners.push(list);
        };
    },
    ViewListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }
});

