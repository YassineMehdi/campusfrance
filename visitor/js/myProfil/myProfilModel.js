jQuery.extend({
  MyProfilModel: function () {
    var listeners = new Array();
    var that = this;

    this.initModel = function () {
      this.fillActivitySectors();
      this.fillCountries();
      this.fillFunctionCandidates();
      setTimeout(function () {
	      that.getCandidateAccount();
      }, 1500);
    };

	//------------- Register Candidate -----------//
    this.registerCandidateSuccess = function (xml) {
      that.notifyLoadSuccess();
    };
    this.registerCandidateError = function (xhr, status, error) {
      isSessionExpired(xhr.responseText);
    };
	// Function Candidates page scripts
    this.registerCandidate = function (candidate) {
      genericAjaxCall('POST', staticVars["urlBackEndCandidate"] + 'candidate/updateAccount',
              'application/xml', 'xml', candidate.xmlData(), that.registerCandidateSuccess,
              that.registerCandidateError);
    };
    //------------- get Candidate Account -----------//
    this.getCandidateAccountSuccess = function (xml) {
      that.notifyGetCandidateInfoSuccess(new $.Candidate(xml));
    };
    this.getCandidateAccountError = function (xhr, status, error) {
      isSessionExpired(xhr.responseText);
    };

    this.getCandidateAccount = function () {
      genericAjaxCall('GET', staticVars["urlBackEndCandidate"]
              + 'candidate/candidateAccount?idLang=' + getIdLangParam(),
              'application/xml', 'xml', '', that.getCandidateAccountSuccess,
              that.getCandidateAccountError);
    };

    //------------- Activity Sector -----------//
    this.fillActivitySectorsSuccess = function (xml) {
      var activitySectors = [];
      $(xml).find('secteurActDTO').each(function () {
        var secteurAct = new $.SecteurAct($(this));
        activitySectors.push(secteurAct);
      });
      that.notifyLoadActivitySectors(activitySectors);
    };

    this.fillActivitySectorsError = function (xhr, status, error) {
      isSessionExpired(xhr.responseText);
    };

    this.fillActivitySectors = function () {
      genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'secteuract?idLang=' + getIdLangParam(), 'application/xml', 'xml', '', that.fillActivitySectorsSuccess, that.fillActivitySectorsError);
    };

    //------------- Function Candidates -----------//
    this.fillFunctionCandidatesSuccess = function (xml) {
      var functionCandidates = [];
      $(xml).find('functionCandidateDTO').each(function () {
        var functionCandidate = new $.FunctionCandidate($(this));
        functionCandidates.push(functionCandidate);
      });
      that.notifyLoadFunctionCandidates(functionCandidates);
    };

    this.fillFunctionCandidatesError = function (xhr, status, error) {
      isSessionExpired(xhr.responseText);
    };

    this.fillFunctionCandidates = function () {
      genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'functionCandidate?idLang=' + getIdLangParam(), 'application/xml', 'xml', '', that.fillFunctionCandidatesSuccess, that.fillFunctionCandidatesError);
    };
    
    this.fillCountriesSuccess = function(xml) {
      var countries = [];
      $(xml).find('countryDTO').each(function() {
        var country = new $.Country($(this));
        countries.push(country);
      });
      that.notifyLoadCountries(countries);
    };
    this.fillCountriesError = function(xhr, status, error) {
      isSessionExpired(xhr.responseText);
    };
    // Country page scripts
    this.fillCountries = function() {
      genericAjaxCall('GET', staticVars["urlBackEndCandidate"]
          + 'country?idLang=' + getIdLangParam(), 'application/xml', 'xml',
          '', that.fillCountriesSuccess, that.fillCountriesError);
    };
    // ------------- State -----------//
    this.fillStatesSuccess = function(xml) {
      var states = [];
      $(xml).find('stateDTO').each(function() {
        var state = new $.State($(this));
        states.push(state);
      });
      that.notifyStates(states);
    };
    this.fillStatesError = function(xhr, status, error) {
      isSessionExpired(xhr.responseText);
    };
    // State page scripts
    this.fillStates = function(countryId) {
      genericAjaxCall('GET', staticVars["urlBackEndCandidate"]
          + 'state/by/country?idLang=' + getIdLangParam() + "&countryId=" + countryId, 'application/xml', 'xml',
          '', that.fillStatesSuccess, that.fillStatesError);
    };
    this.notifyStates = function(states) {
      $.each(listeners, function(i) {
        listeners[i].loadStates(states);
      });
    };
    // --------- END ---------------------//
    // ------------- City -----------//
    this.fillCitiesSuccess = function(xml) {
      var cities = [];
      $(xml).find('cityDTO').each(function() {
        var city = new $.City($(this));
        cities.push(city);
      });
      that.notifyCities(cities);
    };
    this.fillCitiesError = function(xhr, status, error) {
      isSessionExpired(xhr.responseText);
    };
    // City page scripts
    this.fillCities = function(stateId) {
      genericAjaxCall('GET', staticVars["urlBackEndCandidate"]
          + 'city/by/state?idLang=' + getIdLangParam() + "&stateId=" + stateId, 'application/xml', 'xml',
          '', that.fillCitiesSuccess, that.fillCitiesError);
    };
    this.notifyCities = function(cities) {
      $.each(listeners, function(i) {
        listeners[i].loadCities(cities);
      });
    };
    this.notifyLoadCountries = function(countries) {
      $.each(listeners, function(i) {
        listeners[i].loadCountries(countries);
      });
    };

    this.notifyLoadActivitySectors = function (activitySectors) {
      $.each(listeners, function (i) {
        listeners[i].profileActivitySectors(activitySectors);
      });
    };
    this.notifyLoadFunctionCandidates = function (functionCandidates) {
      $.each(listeners, function (i) {
        listeners[i].profileFunctionCandidates(functionCandidates);
      });
    };
    this.notifyLoadSuccess = function () {
      $.each(listeners, function (i) {
        listeners[i].candidateSuccess();
      });
    };
    this.notifyGetCandidateInfoSuccess = function (candidate) {
      $.each(listeners, function (i) {
        listeners[i].getCandidateInfoSuccess(candidate);
      });
    };
    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  ModelListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }

});
