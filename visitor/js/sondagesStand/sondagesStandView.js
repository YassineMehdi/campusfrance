jQuery.extend({
  SelectRowManage: function (formField) {
    var that = this;
    //console.log("inside SelectRowManage");

    var dom = $('<div class="row no-margin response-container">'
            + '<p class="labelField"></p>'
            + '<div class="controls hide" id=""></div>'
            + '<div id="selectContainer" class="surveyResponseDetails">'
            + '</div>'
            + '</div>');

    this.refresh = function () {
      dom.find('.labelField').attr("id", "formFieldID_" + formField.getFormFieldId());
      dom.find('.labelField').text(formField.getFormFieldLabel());
      dom.find('.controls').attr("id", "fieldID_" + formField.getField().getFieldId());
      var selectValues = formField.getField().getFieldName().split(",");

      for (var index in selectValues) {
        if (selectValues[index] != "") {
          var selectHtml = '<div class="row no-margin">'
                  + '<input type="checkbox" class="selectBoxChoices" id="Reponse_' + replaceAllHtmlCast(selectValues[index]) + '_' + formField.getFormFieldId() + '" class="filled-in" />'
                  + '<label for="Reponse_' + replaceAllHtmlCast(selectValues[index]) + '_' + formField.getFormFieldId()+ '">' + htmlEncode(selectValues[index]) + '</label>'
                  + '</div>';
          dom.find('#selectContainer').append(selectHtml);
        }
      }
    };
    this.getDOM = function () {
      return dom;
    };
    this.refresh();
  },
  RadioRowManage: function (formField) {
    var that = this;
    //console.log("inside RadioRowManage");

    var dom = $('<div class="row no-margin response-container">'
            + '<p class="labelField">Surfez sur la nouvelle tendance technologique des forums virtuels, visitez notre Demo !</p>'
            + '<div class="controls hide" id=""></div>'
            + '<div id="radioContainer" class="surveyResponseDetails">'
            + '</div>'
            + '</div>');

    this.refresh = function () {
      dom.find('.labelField').attr("id", "formFieldID_" + formField.getFormFieldId());
      dom.find('.labelField').text(formField.getFormFieldLabel());
      dom.find('.controls').attr("id", "fieldID_" + formField.getField().getFieldId());
      var radioValues = formField.getField().getFieldName().split(",");

      for (var index in radioValues) {
        if (radioValues[index] != "") {
          var radioHtml = '<div class="row no-margin">'
                  + '<input class="with-gap radioBoxChoices" name="' + formField.getFormFieldId() + '" type="radio" value="true" id="optionsRadios_' + replaceAllHtmlCast(radioValues[index]) + '_' + formField.getFormFieldId() + '"/>'
                  + '<label for="optionsRadios_' + replaceAllHtmlCast(radioValues[index]) + '_' + formField.getFormFieldId() + '">' + htmlEncode(radioValues[index]) + '</label>'
                  + '</div>';
          dom.find('#radioContainer').append(radioHtml);
        }
      }
    };
    this.getDOM = function () {
      return dom;
    };
    this.refresh();
  },
  InputRowManage: function (formField) {
    var that = this;
    //console.log("inside InputRowManage");

    var dom = $('<div class="row no-margin response-container">'
            + '<p class="labelField"></p>'
            + '<div class="controls hide" id=""></div>'
            + '<div id="inputTextContainer" class="surveyResponseDetails">'
            + '<div class="row no-margin">'
            + '<textarea class="materialize-textarea"></textarea>'
            + '</div>'
            + '</div>'
            + '</div>');

    this.refresh = function () {
      dom.find('.labelField').attr("id", "formFieldID_" + formField.getFormFieldId());
      dom.find('.labelField').text(formField.getFormFieldLabel());
      dom.find('.controls').attr("id", "fieldID_" + formField.getField().getFieldId());
      dom.find('textarea').attr("id", "inputID_" + formField.getFormFieldId());
    };
    this.getDOM = function () {
      return dom;
    };
    this.refresh();
  },
  RegisterRowManage: function (formField) {
    var that = this;
    that.form = null;
    that.genericRow = null;
    that.dom = $('');
    //console.log("inside RegisterRowManage");
    this.refresh = function () {
      switch (formField.getField().getInputType().getInputTypeId()) {
        case "2" :
          that.genericRow = new $.InputRowManage(formField);
          break;
        case "3" :
          that.genericRow = new $.SelectRowManage(formField);
          break;
        case "4" :
          that.genericRow = new $.RadioRowManage(formField);
          break;
      }
      if (that.genericRow != null) {
        dom = that.genericRow.getDOM();
      }
    };
    this.getDOM = function () { //retourne une ligne HTML <a .....
      return dom;
    };
    this.refresh();
  },
  SondagesStandManage: function (sondages, view) {
    var sondageDetails = $('#sondage');
    var answerButton = null;
    sondageDetails.empty();

    if (sondages.answered == "true") {
      answerButton = '<a href="#" class="btn sup">'+ANSWEREDLABEL+'</a>';
    } else {
      answerButton = '<a data-toggle="modal" data-target="#modal-sondages-details" class="btn getModal participer-link" style="padding: 0px; ">'+PARTICIPATELABEL+'</a>';
    }

    var dom = $('<li class="sondage_' + sondages.getFormId() + '">'
            + '<span class="col-md-10 col-sm-10 no-padding">' + htmlEncode(sondages.getFormTitle()) + '</span>'
            + '<span class="button col-md-2 col-sm-2 no-padding">'
            + answerButton
            + ' </span>'
            + '</li>');

    this.refresh = function () {
        dom.find(".participer-link").click(function () {
		    analyticsView.viewStandZoneById(SURVEY+'/'+SURVEYID, sondages.getFormId(), null);
	        $('.modal-loading').show();
	        var sondageFields = sondages.getFormFields();
	        $('#sondageDetailsContent').empty().append('<div id="sondage" class="scroll-pane form"></div>');
	        for (var index in sondageFields) {
	          var registerRowManage = new $.RegisterRowManage(sondageFields[index]);
	          $('#sondage').append(registerRowManage.getDOM());
	        }
	
	        $('#sondage').append('<div id="idForm" style="display:none;">' + sondages.getFormId() + '</div>'
	                + '<div>'
	                + '<button class="btn send-responce_' + sondages.getFormId() + '" style="padding: 0px;">'+SENDLABEL+'</button>'
	                + '</div>');
	
	        $('.send-responce_' + sondages.getFormId()).click(function () {
		      $('.modal-loading').show();
	          if ($(".errorValidateField") != undefined) {
	            $(".errorValidateField").remove();
	          }
	          var isValid = true;
	          var attendsDataList = new Array();
	
	          $('#sondage').find('.response-container').each(function () {
	            var attendData = new $.AttendData();
	            var field = new $.Field();
	            var fieldId = 0;
	            fieldId = $(this).find('.controls').attr('id').split('_')[1];
	
	            field.setFieldId(fieldId);
	            attendData.setField(field);
	
	            var form = new $.Form();
	            form.setFormId($('#idForm').text());
	            attendData.setForm(form);
	
	            var responseType = $($(this).find('.surveyResponseDetails')).attr('id');
	
	            switch (responseType) {
	              case "selectContainer":
	                var isSelectValid = true;
	                var value = '';
	                $(this).find('.selectBoxChoices').each(function () {
	                  if ($(this).is(':checked')) {
	                    var textLabel = $(this).closest('div').find('label').text();
	                    value = value + "," + textLabel;
	                  }
	                });
	
	                if (value == "") {
	                  $(this).find('#selectContainer').after('<label class="error errorValidateField" style="display: block;">'+CHOOSEONERESPONSEATLEASTLABEL+'</label>');
	                  isSelectValid = false;
	                }
	
	                if (isSelectValid == true) {
	                  attendData.setValue(value);
	                } else
	                  isValid = false;
	                break;
	
	              case "radioContainer":
	                var isRadioValid = true;
	                var value = '';
	                $(this).find('.radioBoxChoices').each(function () {
	                  if ($(this).is(':checked')) {
	                    var textLabel = $(this).closest('div').find('label').text();
	                    value = textLabel;
	                  }
	                });
	
	                if (value == "") {
	                  $(this).find('#radioContainer').after('<label class="error errorValidateField" style="display: block;">'+CHOOSEONERESPONSEATLEASTLABEL+'</label>');
	                  isRadioValid = false;
	                }
	
	                if (isRadioValid == true) {
	                  attendData.setValue(value);
	                } else
	                  isValid = false;
	
	                break;
	
	              case "inputTextContainer":
	                var value = $($(this).find('.surveyResponseDetails')).find('textarea').val();
	                var isInputValid = true;
	
	                if (value == "") {
	                  $($(this).find('.surveyResponseDetails')).find('textarea').after('<label class="error errorValidateField" style="display: block;">'+REQUIREDLABEL+'</label>');
	                  isInputValid = false;
	                }
	
	                if (isInputValid == true) {
	                  attendData.setValue(value);
	                } else
	                  isValid = false;
	                break;
	
	            }
	
	            attendsDataList.push(attendData);
	          });
	
	          if (isValid == true) {
	            view.notifyAddReponse(attendsDataList, sondages.getFormId());
	          }else {
							$("#sondage .jspPane").css("top", "0px");  	        
							$('.modal-loading').hide();
					  }
	        });
	        $('#modal-sondages-details .scroll-pane').jScrollPane({ autoReinitialise: true });
	        $('.modal-loading').hide();
	      });

    };
    this.getDOM = function () { //retourne une ligne HTML <a .....
      return dom;
    };
    this.refresh();
  },
  SondagesStandView: function () {
    var listeners = new Array();
    var that = this;
    var surveyContent = $('.survey-body');
    surveyContent.empty();

    this.addResponseComplete = function (sondageFormId) {
      swal({
        title: "",
        text: UPDATESUCCESSLABEL,
        type: "success"},
      function () {
      });
      $('.sondage_' + sondageFormId + ' a').trigger('click');
      $('.sondage_' + sondageFormId).find("a").closest('span').empty();
      $($('.sondage_' + sondageFormId).find('span')[1]).append('<a class="btn sup">'+ANSWEREDLABEL+'</a>');
    };

    this.displaySondagesStand = function (listSondages) {
      surveyContent.empty();
      if (listSondages != null && listSondages.length != 0) {
        var sondages = [];
        surveyContent.append('<ul class="scroll-pane"></ul>');
        var surveyContentUl = surveyContent.find('ul');
        for (index in listSondages) {
          if (listSondages[index] != undefined) {
            sondages.push(listSondages[index]);
            var sondagesStandManage = new $.SondagesStandManage(listSondages[index], that);
            var docDom = sondagesStandManage.getDOM();
            surveyContentUl.append(docDom);
          }
        }
      } else {
    	surveyContent.append('<p class="empty">'+NOSURVEYLABEL+'</p>');
      }

      $('#modal-sondage .scroll-pane').jScrollPane({ autoReinitialise: true });
      $('.modal-loading').hide();
    };

    this.initView = function () {
      var standId = $('#sondage-link').attr('standId');
      $('#sondage-link').unbind('click').bind('click', function () {
        if ($(this).parent().hasClass('active')) {
          $(".menu-stand ul li a").parent().removeClass('active');
          $(this).parent().addClass('active');
          return false;
        }
        $(".menu-stand ul li a").parent().removeClass('active');
        $(this).parent().addClass('active');
        $('.modal').modal('hide');
        $('.survey-body').empty();
        if (connectedUser) {
          if ($('#modal-sondage.in').size() == 0) {
            $('.modal-loading').show();
            that.notifySondagesStand(standId);
            $('#modal-sondage').modal('show');
          } else {
            $('.close').click();
          }
        } else {
          getStatusNoConnected();
        }
      });
    };
    this.notifySondagesStand = function (standId) {
      $.each(listeners, function (i) {
        listeners[i].launchSondagesStand(standId);
      });
    };
    this.notifyAddReponse = function (attend, formId) {
      $.each(listeners, function (i) {
        listeners[i].addReponse(attend, formId);
      });
    };
    this.addListener = function (list) {
      listeners.push(list);
    }
    ;
  },
  ViewListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }
});

