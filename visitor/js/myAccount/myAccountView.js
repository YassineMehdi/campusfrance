jQuery.extend({
  MyAccountView: function () {
    var listeners = new Array();
    var that = this;
    
    this.loadCandidateAccount = function (candidate){
    	//console.log(candidate);
    	var userProfile = candidate.getUserProfile();
    	$loginAccount.val(candidate.getEmail());
    	$firstNameAccount.val(userProfile.getFirstName());
    	$lastNameAccount.val(userProfile.getSecondName());
    	$webSite.val(userProfile.getWebSite());
    	$phoneportableAccount.val(candidate.getCellPhone());
    	$currentCompanyAccount.val(candidate.getCurrentCompany());
    	$('.modal-loading').hide();
    };

    this.initView = function () {
    	$('.modal-loading').show();
    	$('.genericContent').hide();
      $('#profileAccount').show();
  		$('#page-content').removeClass('no-padding');
  		$loginAccount = $('#loginAccount');
  		$firstNameAccount = $('#prenomAccount');
  		$lastNameAccount = $('#nomAccount');
  		$phoneportableAccount = $('#phoneportable');
  		$currentCompanyAccount = $('#currentCompany');
  		$passwordAccount = $('#passwordAccount');
  		$confirmPasswordAccount = $('#confirmPasswordAccount');
  		$webSite = $('#website');
	    
  		$('#Reponse2').unbind('change').bind('change', function () {
        if ($('#Reponse2').is(':checked') == true) {
            $('#passwordAccount').prop('disabled', false);
            $('#confirmPasswordAccount').prop('disabled', false);
        } else {
            $('#passwordAccount').prop('disabled', true);
            $('#confirmPasswordAccount').prop('disabled', true);
        }
	    });
	    
	    $("#envoyerAccount").unbind('click').bind('click', function () {
	      if (!$("#myAccountForm").valid()) {
	            return false;
	     	}
	     	
	    	var candidate  = new $.Candidate();
	    	var userProfile  = new $.UserProfile();
	    	candidate.setCurrentCompany($currentCompanyAccount.val());
	      candidate.setCellPhone($phoneportableAccount.val());
	     	userProfile.setFirstName($firstNameAccount.val());
	     	userProfile.setSecondName($lastNameAccount.val());
				// ---- Set Web site
				userProfile.setWebSite($webSite.val());
				if($passwordAccount.val().trim()!=''){
					candidate.setPassword($.sha1($passwordAccount.val()));
				      candidate.setPasswordClear($passwordAccount.val());
				}
				candidate.setUserProfile(userProfile);
				$('.modal-loading').show();
				that.notifyUpdateAccount(candidate);
	    });
	    
	    $('#profileAccount .scroll-pane').jScrollPane({ autoReinitialise: true });
    };
    this.myAccountFormValidate = function(){
      $.validator.addMethod("phoneNumbre", function (phone_number, element) {
        phone_number = phone_number.replace(/\s+/g, "");
        return this.optional(element) || phone_number.length > 9 &&
                (phone_number.match(/\(?\+([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/) || phone_number.match(/\(?(00[0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/));
      }, "Merci de respecter le format indiqué");
    	$("#myAccountForm").validate({
            errorPlacement: function errorPlacement(error, element) {
              element.before(error);
            },
            rules: {
              loginAccount: {
                required: true,
                email: true,
              },
              prenomAccount: {
                required: true
              },
              nomAccount: {
                required: true
              },
              phoneportable: {
                required: true,
                minlength: 10
              },
              passwordAccount: {
                required: true
              },
              confirmPasswordAccount: {
                required: true,
                equalTo: "#passwordAccount"
              },
              currentCompany : {
            	  required: true
              }
              
            }
    	});
    };
	that.myAccountFormValidate();
	this.updateAccountSuccess = function(){
		swal({
            title: "",
            text: UPDATESUCCESSLABEL,
            type: "success"},
        function () {
        });
		$('.modal-loading').hide();
    };
    this.notifyUpdateAccount = function (candidate) {
      $.each(listeners, function (i) {
        listeners[i].updateAccount(candidate);
      });
    };

    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  ViewListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }
});

