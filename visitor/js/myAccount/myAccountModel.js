jQuery.extend({
  MyAccountModel: function () {
    var listeners = new Array();
    var that = this;

    this.initModel = function () {
      this.getCandidatePersonalInfos();
    };

    //------------- update Candidate Personel Info -----------//	
		this.updateAccountSuccess = function () {
			that.notifyUpdateAccountSuccess();
		};
		
		this.getAccountError = function (xhr, status, error) {
		  	isSessionExpired(xhr.responseText);
		};
		
		this.updateAccount = function (candidate){
			genericAjaxCall('POST', staticVars["urlBackEndCandidate"]
			+ 'candidate/updateInfos', 'Application/xml', 'xml', candidate.xmlData(), that.updateAccountSuccess,
			that.getAccountError);
		};
	
    //------------- get Candidate Personel Info -----------//
    this.candidatePersonalInfosSuccess = function (xml) {
			that.notifyLoadCandidatePersonalInfos(new $.Candidate(xml));
		};

		this.candidatePersonalInfosError = function (xhr, status, error) {
		  	isSessionExpired(xhr.responseText);
		};
	
		this.getCandidatePersonalInfos = function () {
		  	genericAjaxCall('GET', staticVars["urlBackEndCandidate"]
		          + 'candidate/personalInfos', 'application/xml', 'xml', '',
		          that.candidatePersonalInfosSuccess, that.candidatePersonalInfosError);
		};

    this.notifyLoadCandidatePersonalInfos = function (candidate) {
      $.each(listeners, function (i) {
        listeners[i].loadCandidateAccount(candidate);
      });
    };
    this.notifyUpdateAccountSuccess = function (){
    	$.each(listeners, function (i) {
        listeners[i].updateAccountSuccess();
      });
    };

    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  ModelListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }

});