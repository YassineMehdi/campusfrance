jQuery.extend({
  MyAccountController: function (model, view) {
    var that = this;
    /**
     * listen to the view
     */

    var vlist = $.ViewListener({
    	updateAccount: function (candidate) {
        model.updateAccount(candidate);
      },
    });

    view.addListener(vlist);

    /**
     * listen to the model
     */
    var mlist = $.ModelListener({
      loadCandidateAccount: function (candidate) {
        view.loadCandidateAccount(candidate);
      },
      updateAccountSuccess: function(){
      	view.updateAccountSuccess();
      },
    });

    model.addListener(mlist);

    this.initController = function () {
	    view.initView();
      model.initModel();
    };
    
  },
});