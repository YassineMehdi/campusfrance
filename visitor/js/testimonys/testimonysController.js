jQuery.extend({
  TestimonysController: function (model, view) {
    var that = this;
    var testimonyModelListener = new $.TestimonyModelListener({
      testimoniesSuccess: function (testimonys) {
        view.displayTestimonies(testimonys);
      },
      testimoniesError: function (xhr) {
        view.displayTestimoniesError(xhr);
      }
    });
    model.addListener(testimonyModelListener);

    // listen to the view
    var testimonyViewListener = new $.TestimonyViewListener({
      testimonies: function (standId) {
        model.testimonies(standId);
      }
    });
    view.addListener(testimonyViewListener);

    this.initController = function () {
      view.initView();
      model.initModel();
    };
    that.initController();
  }

});