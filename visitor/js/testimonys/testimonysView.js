jQuery.extend({
  TestimonyManage: function (testimony) {
    var isFavorite = factoryFavoriteExist(testimony.idTestimony, FavoriteEnum.TESTIMONY);
    var displayDeleteFavorite = "block";
    var displayAddFavorite = "block";
    if (isFavorite)
      displayAddFavorite = "none";
    else
      displayDeleteFavorite = "none";

    var appendtext = '';
    appendtext += '<li>';
    appendtext += '<div class = "avatarP col-md-4 col-sm-4">';
    appendtext += '<img alt = "" src = "' + testimony.photo + '">';
    appendtext += '<p>' + htmlEncode(testimony.name) + '<span class = "fonction">' + htmlEncode(testimony.getJobTitle()) + '</span></p>';
    appendtext += '</div>';
    appendtext += '<div class = "discription col-md-8 col-sm-8 no-padding">';
    appendtext += '<a class = "btn btn-primary col-md-10 active addtestimonyToFavorite" href = "#" style="display : ' + displayAddFavorite + '"> <i aria-hidden = "true" class = "glyphicon glyphicon-star"> </i>'+ADDTOFAVORITELABEL+'</a>';
    appendtext += '<a class = "btn btn-primary col-md-10 deletetestimonyFromFavorite" href = "#" style="display : ' + displayDeleteFavorite + '"> <i aria-hidden = "true" class = "glyphicon glyphicon-star"> </i> '+DELETEFROMFAVORITELABEL+'</a>';

    appendtext += '<div class="content-testimony more-content-testimony">' + htmlEncode(unescape(testimony.testimonyContent)) + '</div><a class = "more" href = "#"> '+SEEMORELABEL+' </a>';
    appendtext += ' </div>';
    appendtext += '</li>';
    var dom = $(appendtext);

    this.refresh = function () {

      var addtestimonyToFavorite = dom.find(".addtestimonyToFavorite");
      var deletetestimonyFromFavorite = dom.find(".deletetestimonyFromFavorite");

      addtestimonyToFavorite.click(function () {
        if (connectedUser) {
          var idEntity = testimony.idTestimony;
          var componentName = FavoriteEnum.TESTIMONY;
          var favoriteXml = factoryXMLFavory(idEntity, componentName);
          addFavorite(favoriteXml);
          $(this).hide();
          $(this).closest('div').find('.deletetestimonyFromFavorite').show();
        } else {
          getStatusNoConnected();
        }
      });

      deletetestimonyFromFavorite.click(function () {
        var idEntity = testimony.idTestimony;
        var componentName = FavoriteEnum.TESTIMONY;
        var favoriteXml = factoryXMLFavory(idEntity, componentName);
        deleteFavorite(favoriteXml);
        $(this).hide();
        $(this).closest('div').find('.addtestimonyToFavorite').show();
      });

      dom.find('.more').on('click', function () {
	    	$(this).toggleClass('less');
	        if ($(this).hasClass('less')) {
	            $(this).html(SEELESSLABEL);
	            dom.find('.content-testimony').removeClass("more-content-testimony");
	        } else {
	            $(this).html(SEEMORELABEL);
	            dom.find('.content-testimony').addClass("more-content-testimony");
	        }
	        $('#modal-temoignage .scroll-pane').jScrollPane({ autoReinitialise: true });
      });
    };
    this.getDOM = function () { //retourne une ligne HTML <a .....
      return dom;
    };
    this.refresh();
  },
  TestimonysView: function () {
    var that = this;
    var listeners = new Array();
    this.initView = function () {
      that.getTestimonies();
    };
    //------- Testimonies --------------//
    this.getTestimonies = function () {
      var standId = $('#temoignage').attr('standId');
      that.notifyTestimonies(standId);
    };
    //----------- Testimonies --------//
    this.displayTestimonies = function (testimonys) {
      $('.modal-content .temoignage').empty();
      if (testimonys != null && testimonys.length > 0) {
    	$('.modal-content .temoignage').append('<div class="scroll-pane">'
								    			+'<ul></ul>'
								    			+'</div>');
    	var temoignagesHandler = $('.modal-content .temoignage ul');
    	var testimonyManage = null;
        for (index in testimonys) {
          testimonyManage = new $.TestimonyManage(testimonys[index]);
          temoignagesHandler.append(testimonyManage.getDOM());
        }
        $('#modal-temoignage .scroll-pane').jScrollPane({ autoReinitialise: true });
      } else {
        $('.modal-content .temoignage').append('<p class="empty">'+NOTESTIMONYLABEL+'</p>');
      }
    };
    this.displayTestimoniesError = function (Xhr) {
      console.log(Xhr);
    };
    //----------- Notify Testimonies --------//
    this.notifyTestimonies = function (standId) {
      $.each(listeners, function (i) {
        listeners[i].testimonies(standId);
      });
    };
    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  TestimonyViewListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }
});
