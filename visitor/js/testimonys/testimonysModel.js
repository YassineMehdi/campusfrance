jQuery.extend({
  TestimonysModel: function () {
    var that = this;
    var listeners = new Array();
    this.initModel = function () {
    };
//--------  Testimonies --------------//
    this.testimoniesSuccess = function (xml) {
      var testimonys = [];
      $(xml).find("testimony").each(function () {
        var testimony = new $.Testimony($(this));
        testimonys.push(testimony);
      });
      that.notifyTestimoniesSuccess(testimonys);
    };

    this.testimoniesError = function (xhr, status, error) {
      that.notifyTestimoniesError(xhr);
    };
    this.testimonies = function (standId) {
      genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'testimony/standTestimonies?' + standId + '&idLang=' + getIdLangParam(),
              'application/xml', 'xml', '', that.testimoniesSuccess,
              that.testimoniesError);
    };
    this.notifyTestimoniesSuccess = function (testimonys) {
      $.each(listeners, function (i) {
        listeners[i].testimoniesSuccess(testimonys);
      });
    };
    this.notifyTestimoniesError = function (xhr) {
      $.each(listeners, function (i) {
        listeners[i].testimoniesError(xhr);
      });
    };
    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  TestimonyModelListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }
});