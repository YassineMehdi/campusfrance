jQuery.extend({
    InfoStandView: function() {
        var that = this;
        var listeners = new Array();
        this.initView = function(standId) {
            $('.modal').modal('hide');
            $(".menu-stand ul li a").parent().removeClass('active');
            $('#info-stand .menu-stand,#info-stand #vue-aerienne-info-stand').show();
            that.GetProfilEntreprise();
            playVideoBackGroundStand(standId);
            $('#websitesNetwork').hide();
            //--------- testimonys -----------------//
            if (testimonysController == null) {
                testimonysModel = new $.TestimonysModel();
                testimonysView = new $.TestimonysView();
                testimonysController = new $.TestimonysController(
                    testimonysModel, testimonysView);

            } else
                testimonysController.initController();
            //--------- Send Message -----------------//
            if (sendMessageController == null) {
                sendMessageController = new $.SendMessageController(
                    new $.SendMessageModel(), new $.SendMessageView());
            }
            sendMessageController.initController();
            //--------- affiches ------------------//
            if (affichesController == null) {
                affichesModel = new $.AffichesModel();
                affichesView = new $.AffichesView();
                affichesController = new $.AffichesController(
                    affichesModel, affichesView);

            } else
                affichesController.initController();
            //------------- Documents ---------------//
            if (standDocumentController == null) {
                standDocumentController = new $.StandDocumentController(
                    new $.StandDocumentModel(), new $.StandDocumentView());
            }
            standDocumentController.initController();
            //------------- Agenda ---------------//
            if (agendasController == null) {
                agendasController = new $.AgendasController(
                    new $.AgendasModel(), new $.AgendasView());
            }
            //------------- Multimedias ---------------//
            if (multimediasController == null) {
                multimediasController = new $.MultimediasController(
                    new $.MultimediasModel(), new $.MultimediasView());
            } else {
                multimediasController.initController();
            }
            //------------- Product Stand ---------------//
            if (productStandController == null) {
                productStandController = new $.ProductStandController(
                    new $.ProductStandModel(), new $.ProductStandView());
            }
            productStandController.initController();
            //------------- deposer document Stand ---------------//
            if (deposerDocumentStandController == null) {
                deposerDocumentStandController = new $.DeposerDocumentsStandController(
                    new $.DeposerDocumentsStandModel(), new $.DeposerDocumentsStandView());
            } else {
                deposerDocumentStandController.initController();
            }
            //------------- sondage Stand ---------------//
            if (sondagesStandController == null) {
                sondagesStandController = new $.SondagesStandController(
                    new $.SondagesStandModel(), new $.SondagesStandView());
            } else {
                sondagesStandController.initController();
            }
            //------------- contact Stand ---------------//
            if (contactStandController == null) {
                contactStandController = new $.ContactStandController(
                    new $.ContactStandModel(), new $.ContactStandView());
            } else {
                contactStandController.initController();
            }
            //------------- contact Stand ---------------//
            if (standTChatController == null) {
                standTChatController = new $.StandTChatController(
                    new $.StandTChatModel(), new $.StandTChatView());
            }
            standTChatController.initController();
            /*$('.icon-tchat,#tchat-buttons').unbind("click").bind("click", function () {
            	
            });*/
        };
        //------- Profil Entreprise -------//
        this.GetProfilEntreprise = function() {
            var standId = $('#profil-entreprise').attr('standId');
            window.history.pushState("test", "Title", "/visitor/new-home.html?idLang=" + getIdLangParam() + "&" + standId);
            getSeoByStandId(getIdLangParam(), standId);
            that.notifyProfilEntreprise(standId);
            //return false;
        };
        //-----------   Profil Website  --------//
        this.displayWebsiteEntreprise = function(xml) {
            $('#websitesNetwork ul').empty();
            $(xml).find("website").each(function() {
                var type = $(this).find('type').text();
                var url = $(this).find('url').text();
                var webSiteId = $(this).find('webSiteId').text();
                switch (type) {
                    case 'facebook':
                        $('#websitesNetwork ul').append('<li><a href="' + url + '" title="Facbook" target="_blank" ><i class="icon-2x icon-facebook"></i></a></li>');
                        $('.icon-facebook').unbind('click').bind('click', function() {
                            analyticsView.viewWebSitesById(FACEBOOK, webSiteId, analyticsView.getCurrentStandId());
                        });
                        break;
                    case 'twitter':
                        $('#websitesNetwork ul').append('<li><a href="' + url + '" title="Twitter" target="_blank"><i class="icon-2x icon-twitter"></i></a></li>');
                        $('.icon-twitter').unbind('click').bind('click', function() {
                            analyticsView.viewWebSitesById(TWITTER, webSiteId, analyticsView.getCurrentStandId());
                        });
                        break;
                    case 'youtube':
                        $('#websitesNetwork ul').append('<li><a href="' + url + '" title="Youtube" target="_blank"><i class="icon-2x icon-youtube-play"></i></a></li>');
                        $('.icon-youtube-play').unbind('click').bind('click', function() {
                            analyticsView.viewWebSitesById(YOUTUBE, webSiteId, analyticsView.getCurrentStandId());
                        });
                        break;
                    case 'linkedin':
                        $('#websitesNetwork ul').append('<li><a href="' + url + '" title="Linkedin" target="_blank"><i class="icon-2x icon-linkedin"></i></a></li>');
                        $('.icon-linkedin').unbind('click').bind('click', function() {
                            analyticsView.viewWebSitesById(WEBSITE, webSiteId, analyticsView.getCurrentStandId());
                        });
                        break;
                    case 'xing':
                        $('#websitesNetwork ul').append('<li><a href="' + url + '" title="Xing" target="_blank"><i class="icon-2x icon-xing"></i></a></li>');
                        $('.icon-xing').unbind('click').bind('click', function() {
                            analyticsView.viewWebSitesById(XING, webSiteId, analyticsView.getCurrentStandId());
                        });
                        break;
                    case 'bolg':
                        $('#websitesNetwork ul').append('<li><a href="' + url + '" title="Blog" target="_blank"><i class="icon-2x fa-rss"></i></a></li>');
                        $('.icon-rss').unbind('click').bind('click', function() {
                            analyticsView.viewWebSitesById(BLOG, webSiteId, analyticsView.getCurrentStandId());
                        });
                        break;
                    case 'career':
                        $('#websitesNetwork ul').append('<li><a href="' + url + '" title="Site Cariere" target="_blank"><i class="icon-2x fa-clipboard"></i></a></li>');
                        $('.icon-clipboard').unbind('click').bind('click', function() {
                            analyticsView.viewWebSitesById(CAREER, webSiteId, analyticsView.getCurrentStandId());
                        });
                        break;
                    case 'ourWebsite':
                        $('#websitesNetwork ul').append('<li><a href="' + url + '" title="Site Web" target="_blank"><i class="icon-2x fa-globe"></i></a></li>');
                        $('.icon-globe').unbind('click').bind('click', function() {
                            analyticsView.viewWebSitesById(WEBSITE, webSiteId, analyticsView.getCurrentStandId());
                        });
                        break;
                }
            });
            if (!$('#content_video_transition_info_stand').is(':visible'))
                $('#websitesNetwork').show();
        };
        //----------- Profil Entreprise --------//
        this.displayProfilEntreprise = function(Xml) {
            var stand = new $.Stand(Xml);
            if (stand.getGroupStand())
                currentGroupStandId = stand.getGroupStand().getGroupStandId();
            else currentGroupStandId = 4;
            $('#vue-aerienne-info-stand').removeClass();
            $('#vue-aerienne-info-stand').addClass('goupe_' + currentGroupStandId);
            $('#vue-aerienne-info-stand').addClass('standId_' + stand.idstande);
            if (currentGroupStandId == GroupStandEnum.GOLD) {
                $(".menu-stand .icone-multi-recep, .menu-stand .icone-press-recep, .jcarousel-control-navigate").addClass("hide");
                $('#vue-aerienne-info-stand, .icon-recep-to-press').show();
                $('.icone-multi-to-press, .icone-recep-to-press').show();
                $('.icon-recep-to-multi, .icon-recep-to-press').show();
                $(".menu-stand .jcarousel ul").css("left", "0px");
            } else if (currentGroupStandId == GroupStandEnum.SILVER) {
                $(".menu-stand .icone-multi-recep").addClass("hide");
                $(".menu-stand .icone-press-recep, .jcarousel-control-navigate").removeClass("hide");
                $('.icone-multi-to-press, .icone-recep-to-press').hide();
                $('.icon-recep-to-press').hide();
                $('#vue-aerienne-info-stand').show();
                $('.icon-recep-to-multi').show();
                $(".menu-stand .jcarousel ul").css("left", "0px");
            } else if (currentGroupStandId == GroupStandEnum.BRONZE) {
                $(".menu-stand .icone-multi-recep, .menu-stand .icone-press-recep, .jcarousel-control-navigate").removeClass("hide");
                $('#vue-aerienne-info-stand').show();
                $('.icon-recep-to-multi, .icon-recep-to-press').hide();
            } else if (currentGroupStandId == GroupStandEnum.STAND_INFO) {
                $(".menu-stand .icone-multi-recep, .menu-stand .icone-press-recep ").addClass("hide");
                $(".menu-stand .icone-multi-recep, .menu-stand .icone-press-recep ").addClass("hide");
                $('#vue-aerienne-info-stand, .icon-recep-to-press').show();
                $('.icone-multi-to-press, .icone-recep-to-press').show();
                $('.icon-recep-to-multi, .icon-recep-to-press').show();
                $(".menu-stand .jcarousel ul").css("left", "0px");
                //$('#back-to-stand').closest('div').hide();
            }

            $('#modal-profile-entreprise .entreprise-descriptif').addClass("more-content-profile-entreprise");
            $('.afficher-plus-link').removeClass("moins");
            $('.afficher-plus-link').unbind('click').bind('click', function() {
                $(this).toggleClass('moins');
                if ($(this).hasClass('moins')) {
                    $(this).html(SEELESSLABEL);
                    $('#modal-profile-entreprise .entreprise-descriptif').removeClass("more-content-profile-entreprise");
                } else {
                    $(this).html(SEEMORELABEL);
                    $('#modal-profile-entreprise .entreprise-descriptif').addClass("more-content-profile-entreprise");
                    $('#modal-profile-entreprise .scroll-pane .jspPane').css("top", "0px");
                }
                $('#modal-profile-entreprise .scroll-pane').jScrollPane({ autoReinitialise: true });
            });
            $('.entreprise-name,#entreprise-name').html(stand.enterprise.nom);
            $('.entreprise-accroche').html(stand.enterprise.slogan);
            $('.entreprise-descriptif').html(unescape(stand.enterprise.description).replace(/[\n]/gi, "<br/>"));
            $('#entreprise-image').attr('src', stand.enterprise.logo);
            if (stand.enterprise.address != "" && stand.enterprise.address != undefined) {
                $('#entreprise-adresse').html(stand.enterprise.address);
                $(".entreprise-address-block").show();
            } else {
                $(".entreprise-address-block").hide();
            }
            if (stand.enterprise.lieu != "" && stand.enterprise.lieu != undefined) {
                $('#entreprise-lieu').html(stand.enterprise.lieu);
                $(".entreprise-place-block").show();
            } else {
                $(".entreprise-place-block").hide();
            }
            if (stand.enterprise.dateCreation != "" && stand.enterprise.dateCreation != undefined) {
                $('#entreprise-annee').html(stand.enterprise.dateCreation);
                $(".entreprise-creation-year-block").show();
            } else {
                $(".entreprise-creation-year-block").hide();
            }
            if (stand.enterprise.nbremploye != "5000" && stand.enterprise.nbremploye != "500" && stand.enterprise.nbremploye != "" && stand.enterprise.nbremploye != undefined) {
                $('#entreprise-nbemployes').html(stand.enterprise.nbremploye);
                $(".entreprise-employees-number-block").show();
            } else {
                $(".entreprise-employees-number-block").hide();
                $('#entreprise-nbemployes').html("-");
            }
            if (stand.getProfilesPlusRecherche() != undefined && stand.getProfilesPlusRecherche() != '') {
                $('#modal-profile-recherche .profile-details').html('<div class="scroll-pane"><div class="col-md-12">' +
                    htmlEncode(unescape(stand.getProfilesPlusRecherche())).replace(/[\n]/gi, "<br/>") +
                    '</div></div>');
            } else {
                $('#modal-profile-recherche .profile-details').html('<p class="empty">' + NOPROFILSEARCHEDLABEL + '</p>');
            }
            $('.slogo .stand-name').html(substringCustom(stand.enterprise.nom, 15)).attr("title", stand.enterprise.nom);
            if (stand.enterprise.slogan != undefined && stand.enterprise.slogan != "")
                $('.slogo .stand-message').html('" ' + substringCustom(stand.enterprise.slogan, 50) + ' "').attr("title", stand.enterprise.slogan);
            else $('.slogo .stand-message').html("").attr("title", "");
            //$('.sliderG').hide();
            //$('.slogo').show();
            $('#standReceiverName').val(stand.enterprise.nom);
            if (analyticsView == null)
                analyticsView = new $.AnalyticsView();
            analyticsView.enterToStand(Xml);
        };
        this.displayProfilEntrepriseError = function(Xhr) {
            //console.log(Xhr);
        };
        //----------- Notify Profil Entreprise --------//
        this.notifyProfilEntreprise = function(standId) {
            $.each(listeners, function(i) {
                listeners[i].profilEntreprise(standId);
            });
        };
        this.addListener = function(list) {
            listeners.push(list);
        };
    },
    InfoStandViewListener: function(list) {
        if (!list)
            list = {};
        return $.extend(list);
    }
});