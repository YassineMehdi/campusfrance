jQuery.extend({
  AgendaFullManage: function (events, view) {
  	var dom = $('');
  	this.refresh = function () {
	    var eventsData = view.convertAgendaEventsData(events);	  
      $('.calandar-wrapper #calendarFull').fullCalendar('removeEvents');
      $('.calandar-wrapper #calendarFull').fullCalendar('addEventSource', eventsData);
	    $('.modal-loading').hide(); 
    };
    this.getDOM = function () { 
      return dom;
    };
    this.refresh();
  },
  AgendaFullsView: function () {
    var that = this;
    var listeners = new Array();
    var lang = 'fr';
    switch(getIdLangParam()) {
		    case "1":
		  	  lang = 'ar-ma';
		  	  break;
		    case "2":
		  	  lang = 'en';
		  	  break;
		    case "3":
		  	  lang = 'fr';
		  	  break;
		    case "4":
		  	  lang = 'ru';
		  	  break;
		    case "5":
		  	  lang = 'de';
		  	  break;
		    case "6":
		  	  lang = 'es';
		  	  break;
		}
    this.initView = function () {
    	$('.calandar-wrapper #calendarFull').fullCalendar({
        lang: lang,
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,agendaDay',
          lang: lang,
        },
        minTime: "08:00:00",
        maxTime: "21:00:00",
        height: 'auto',
        defaultDate: new Date(),
        editable: true,
        eventLimit: false,
        eventClick: function (event, jsEvent, view) {
          that.showEventDetails(event);
        },
        eventAfterAllRender: function(view) {
          $('.calandar-wrapper #calendarFull .scroll-pane').jScrollPane({ autoReinitialise: true });
        }, 
      });
      $('.sliderG').show();
      $('.slogo').hide();
      reloadSponsorJcarousel();
    };

    this.showEventDetails = function (event) { 
	    $('#full-agenda #modalFullTitle').html(event.title);
	    $('#full-agenda #modalFullEnterpriseName').html(event.enterpriseName);
	    $('#full-agenda #modalFullEnterpriseLogo').attr('src', event.enterpriselogo);
	    $('#full-agenda #modalFullBody').html(event.description);
	    $('#full-agenda #modalFullDate').html(that.stringDetailsDate(event.start)+ ' ' + that.stringDetailsDate(event.end));
	    $('#full-agenda #modalFullTime').html(event.time);
	    $('#modal-profile-entreprise-agendaFull').modal('show');
	    $('#modal-profile-entreprise-agendaFull .scroll-pane').jScrollPane({ autoReinitialise: true });
    };
    this.stringDetailsDate = function(d){
    	if(d != null && d != ""){
    		var date = new Date(d);
    		return [getStringElementDateNumber(date.getDate()), getStringElementDateNumber(date.getMonth() + 1), 
    		        getStringElementDateNumber(date.getFullYear())].join('-');
    	}
    	return "";
    };

    //04-04-2016 14:05
    this.parseDateAgenda = function(yearUTC, monthUTC, dayUTC, hoursUTC, minutesUTC){
    	var d=new Date();
    	d.setUTCFullYear(yearUTC);
    	d.setUTCMonth(monthUTC-1);
    	d.setUTCDate(dayUTC);
    	d.setUTCHours(hoursUTC);
    	d.setUTCMinutes(minutesUTC);
    	return d;
    };
    this.getYearMonthDayDateAgenda = function (d) {
      var year = getStringElementDateNumber(d.getFullYear());
      var month = getStringElementDateNumber(d.getMonth() + 1);
      var day = getStringElementDateNumber(d.getDate());
      return [year, month, day].join('-');
    };
    this.getTimeAgenda = function(d){
    	var hours = getStringElementDateNumber(d.getHours());
    	var minutes = getStringElementDateNumber(d.getMinutes());
    	return [hours,minutes].join(':');
    };
    this.stringToDate = function(sd) {
	      if (sd != null && sd != "" && sd != undefined) {
	          var arrayDate = sd.split(" ");
	          var beginDateArray = arrayDate[0].split('-');
	          var time = arrayDate[1];
	          var timeArray = time.split(':');
	          var date = that.parseDateAgenda(beginDateArray[2], beginDateArray[1], beginDateArray[0],
	          		timeArray[0], timeArray[1]);
	        	var bDateString = that.getYearMonthDayDateAgenda(date);
	        	var bTimeString = that.getTimeAgenda(date);
	          return bDateString + " " + bTimeString;
	      }
	      return null;
	  };
    this.convertAgendaEventsData = function(events) {
      var newEvents = new Array();
    	for (index in events) {
	      var event = events[index];
        var newEventJson = new Object();
        var beginDate = that.stringToDate(event.getBeginDate());
        var endDate = that.stringToDate(event.getEndDate());
        newEventJson.id = event.getIdEvent();
        newEventJson.title = event.getEventTitle().replace('&eacute;', 'é');
        newEventJson.enterpriseName = event.getEnterpriseName();
        newEventJson.enterpriselogo = event.getEnterpriseLogo();
        newEventJson.description = event.getEventDescription();
        newEventJson.start = beginDate;
        newEventJson.end = endDate;
        newEventJson.time = beginDate.split(" ")[1] + ' ' + endDate.split(" ")[1];
        newEventJson.allDay = "";
        newEventJson.eventTypeId = event.getEventTypeId();
        if(newEventJson.eventTypeId == 1) {
        	newEventJson.backgroundColor = "#00a100";
        	newEventJson.title = CHATLABEL + " - " + newEventJson.title;
        }else if(newEventJson.eventTypeId == 2) {
        	newEventJson.backgroundColor = "#8bc34a";
        	newEventJson.title = FAIR_SHOW_LABEL + " - " + newEventJson.title;
        }
        newEvents.push(newEventJson);
	    }
      return newEvents;
    };
    //----------- AgendaFulls --------//
    this.displayAgendaFulls = function (events) {
      new $.AgendaFullManage(events, that);
    };
    this.displayAgendaFullsError = function (Xhr) {
      console.log(Xhr);
    };
    //----------- Notify AgendaFulls --------//
    this.notifyAgendaFulls = function () {
      $.each(listeners, function (i) {
        listeners[i].agendaFulls();
      });
    };
    this.addListener = function (list) {
      listeners.push(list);
    };
  },
  AgendaFullViewListener: function (list) {
    if (!list)
      list = {};
    return $.extend(list);
  }
});

