$(function() {


    // Initialize variables
    var $window = $(window);
    var $usernameInput = $('.user_email'); // Input for username
    var $messages = $('.messages'); // Messages area
    var $inputMessage = $('#message-to-send'); // Input message input box

    var $loginPage = $('.login.page'); // The login page
    var $chatPage = $('.chat.page'); // The chatroom page
    var $userSender = $('.listUser');
    // Prompt for setting a username
    var username;
    var connected = false;
    var typing = false;
    var lastTypingTime;
    var $currentInput = $usernameInput.focus();


    function listenForClick() {
        $('.list li').on('click', function() {
            $('.chat .room').addClass('hide');
            var id = $(this).attr('id')
            $('#' + id + 'room').removeClass('hide');

        })
    }
    //var socket = io();
    $('.privateroom').on('click', function() {
        connected = true;
        var userE = {};
        userE.avatar = "avatar";
        userE.username = "infostand";
        userE.login = "infoStand_4";
        socket.emit('connexionStand', { userE: userE });
    })

    function sendMessage() {
        $('.sendMsg').on('click', function() {

            if ($('.messageValue').val() != '') {

                var idroom = $(this).parent().parent().attr('id');
                var id = idroom.substring(0, idroom.length - 4);
                console.log($('.messageValue').val())
                socket.emit('privatemsgToVisitor', { id: id, msg: $('.messageValue').val() });

                console.log($('#' + id + ' .chat-history ul'))
                $('#' + id + 'room .chat-history ul').append('<li class="clearfix">' +
                    '<div class="message-data align-right">' +
                    '<span class="message-data-time">10:10 AM, Today</span> &nbsp; &nbsp;' +
                    '<span class="message-data-name">' + $('#' + id + ' .name').val() + '</span> <i class="fa fa-circle me"></i>'

                    +
                    '</div>' +
                    '<div class="message other-message float-right">' + $('.messageValue').val() + '</div>' +
                    '</li>')


                $('.messageValue').val('');
            }

        })
    }
    socket.on('users', function(data) {
            console.log(data.users)
            for (i = 0; i < data.users.length; i++) {
                console.log(data.users[i])
                addUserConnexion(data.users[i]);
            }
        })
        /*socket.on('usersVisitor', function(data) {
            console.log(data);
            addUserConnexion(data.user);
        })*/
    socket.on('userVisitor', function(data) {
        console.log(data);
        addUserConnexion(data.userV);
        listenForClick();
        sendMessage();
    })

    function addUserConnexion(user) {
        console.log(user)
        $('.list').append('<li id="' + user.id + '" class="clearfix">' +
            '<img src="' + user.avatar + '" alt="avatar" />' +
            '<div class="about">' +
            '<div class="name">' + user.username + '</div>' +
            '<div class="status">' +
            '<i class="fa fa-circle online"></i> online' +
            '</div>' +
            '</div>' +
            '</li>');
        $('.chat').append('<div id="' + user.id + 'room" class="room hide">' +
            '<div class="chat-header clearfix">' +
            '<img src="' + user.avatar + '" alt="avatar" />'

            +
            '<div class="chat-about">' +
            '<div class="chat-with">Chat avec ' + user.username + '</div>' +
            '<div class="chat-num-messages"></div>' +
            '</div>' +
            '<i class="fa fa-star"></i>' +
            '</div>'

            +
            '<div class="chat-history">' +
            '<ul>' +
            '</ul>'

            +
            '</div>' +
            '<div class="chat-message clearfix">' +
            '<textarea name="message-to-send"  class="messageValue" placeholder="Type your message" rows="3"></textarea>'

            +
            '<i class="fa fa-file-o"></i> &nbsp;&nbsp;&nbsp;' +
            '<i class="fa fa-file-image-o"></i>'

            +
            '<button class="sendMsg">Send</button>'

            +
            '</div>' +
            '</div>')
    }
    socket.on('new_msg', function(data) {
            console.log(data)

            $('#' + data.userV.user.id + 'room .chat-history ul').append(' <li>' +
                '<div class="message-data">' +
                '<span class="message-data-name"><i class="fa fa-circle online"></i> ' + data.userV.user.username + '</span>' +
                '<span class="message-data-time">' + data.userV.date + '</span>' +
                '</div>' +
                '<div class="message my-message">' +
                data.userV.msg +
                '</div>' +
                '</li>');




        })
        // Whenever the server emits 'stand left', 
    socket.on('userDisconnect', function(data) {
        console.log(data)
        $('#' + data.userId.id).remove();
        $('#' + data.userId.id + 'room').remove();

    })

    /* function addParticipantsMessage(data) {
        var message = '';
        if (data.numUsers === 1) {
            message += "there's 1 participant";
        } else {
            message += "there are " + data.numUsers + " participants";
        }
        log(message);
    } */

    /* // Sets the client's username
     function setUsername() {
         username = cleanInput($usernameInput.val().trim());

         // If the username is valid
         if (username) {
             $loginPage.fadeOut();
             $chatPage.show();
             $loginPage.off('click');
             $currentInput = $inputMessage.focus();

             // Tell the server your username
             socket.emit('add user', username);
         }
     }*/

    // Sends a chat message
    /* function sendMessage() {
        var message = $inputMessage.val();
        // Prevent markup from being injected into the message
        message = cleanInput(message);
        // if there is a non-empty message and a socket connection
        if (message && connected) {
            $inputMessage.val('');
            addChatMessage({
                username: username,
                message: message
            });
            // tell server to execute 'new message' and send along one parameter
            socket.emit('new message', message);
        }
    } */

    /*  // Log a message
     function log(message, options) {
         var $el = $('<li>').addClass('log').text(message);
         addMessageElement($el, options);
     }

     // Adds the visual chat message to the message list
     function addChatMessage(data, options) {
         // Don't fade the message in if there is an 'X was typing'
         var $typingMessages = getTypingMessages(data);
         // Log a message
         function log(message, options) {
             var $el = $('<li>').addClass('log').text(message);
             addMessageElement($el, options);
         }

         // Adds the visual chat message to the message list
         function addChatMessage(data, options) {
             // Don't fade the message in if there is an 'X was typing'
             var $typingMessages = getTypingMessages(data);
             // Log a message
             function log(message, options) {
                 var $el = $('<li>').addClass('log').text(message);
                 addMessageElement($el, options);
             } */

    /*  // Adds the visual chat message to the message list
    function addChatMessage(data, options) {
        // Don't fade the message in if there is an 'X was typing'
        var $typingMessages = getTypingMessages(data);
 */
    /*   // Log a message
    function log(message, options) {
        var $el = $('<li>').addClass('log').text(message);
        addMessageElement($el, options);
    }

    // Adds the visual chat message to the message list
    function addChatMessage(data, options) {
        // Don't fade the message in if there is an 'X was typing'
        var $typingMessages = getTypingMessages(data);
        options = options || {};
        if ($typingMessages.length !== 0) {
            options.fade = false;
            $typingMessages.remove();
        }

        var $usernameDiv = $('<span class="username"/>')
            .text(data.username)
            .css('color', getUsernameColor(data.username));
        var $messageBodyDiv = $('<span class="messageBody">')
            .text(data.message);

        var typingClass = data.typing ? 'typing' : '';
        var $messageDiv = $('<li class="message"/>')
            .data('username', data.username)
            .addClass(typingClass)
            .append($usernameDiv, $messageBodyDiv);

        addMessageElement($messageDiv, options);
    }

    // Adds the visual chat typing message
    function addChatTyping(data) {
        data.typing = true;
        data.message = 'is typing';
        addChatMessage(data);
    }

    // Removes the visual chat typing message
    function removeChatTyping(data) {
        getTypingMessages(data).fadeOut(function() {
            $(this).remove();
        });
    }
 */
    // Adds a message element to the messages and scrolls to the bottom
    // el - The element to add as a message
    // options.fade - If the element should fade-in (default = true)
    // options.prepend - If the element should prepend
    //   all other messages (default = false)
    /*     function addMessageElement(el, options) {
            var $el = $(el);

            // Setup default options
            if (!options) {
                options = {};
            }
            if (typeof options.fade === 'undefined') {
                options.fade = true;
            }
            if (typeof options.prepend === 'undefined') {
                options.prepend = false;
            }

            // Apply options
            if (options.fade) {
                $el.hide().fadeIn(FADE_TIME);
                $el.hide().fadeIn(FADE_TIME);
                $el.hide().fadeIn(FADE_TIME);
            }
            if (options.prepend) {
                $messages.prepend($el);
            } else {
                $messages.append($el);
            }
            $messages[0].scrollTop = $messages[0].scrollHeight;
        }

        // Prevents input from having injected markup
        function cleanInput(input) {
            return $('<div/>').text(input).html();
        }

        // Updates the typing event
        function updateTyping() {
            if (connected) {
                if (!typing) {
                    typing = true;
                    socket.emit('typing');
                }
                lastTypingTime = (new Date()).getTime();

                setTimeout(function() {
                    var typingTimer = (new Date()).getTime();
                    var timeDiff = typingTimer - lastTypingTime;
                    if (timeDiff >= TYPING_TIMER_LENGTH && typing) {
                        socket.emit('stop typing');
                        typing = false;
                    }
                }, TYPING_TIMER_LENGTH);
            }
        }

        // Gets the 'X is typing' messages of a user
        function getTypingMessages(data) {

            return $('.typing.message').filter(function(i) {
                return $(this).data('username') === data.username;
            });
        }

        // Gets the color of a username through our hash function
        function getUsernameColor(username) {
            // Compute hash code
            var hash = 7;
            for (var i = 0; i < username.length; i++) {
                hash = username.charCodeAt(i) + (hash << 5) - hash;
            }
            // Calculate color
            var index = Math.abs(hash % COLORS.length);
            return COLORS[index];
        }

     */ // Keyboard events

    /*   $window.keydown(function(event) {
        // Auto-focus the current input when a key is typed
        if (!(event.ctrlKey || event.metaKey || event.altKey)) {
            $currentInput.focus();
        }
        // When the client hits ENTER on their keyboard
        if (event.which === 13) {
            if (username) {
                sendMessage();
                socket.emit('stop typing');
                typing = false;
            } else {
                setUsername();
            }
        }
    });
 */
    /* $inputMessage.on('input', function() {
        updateTyping();
    }); */

    // Click events

    // Focus input when clicking anywhere on login page


    // Focus input when clicking on the message input's border


    // Socket events
    // User connecter
    socket.on('users', function(data) {
            if (data != null) {
                users = data;
                console.log(users);
            }
        })
        // Whenever the server emits 'login', log the login message

    /* // Whenever the server emits 'new message', update the chat body
    socket.on('new_msg', function(data) {
        console.log(data)
        addChatMessage(data);
    }); */

    /*  // Whenever the server emits 'user joined', log it in the chat body
     socket.on('user joined', function(data) {
         log(data.username + ' joined');
         addParticipantsMessage(data);
     }); */

    /* // Whenever the server emits 'user left', log it in the chat body
    socket.on('user left', function(data) {
        log(data.username + ' left');
        addParticipantsMessage(data);
        removeChatTyping(data);
    });

    // Whenever the server emits 'typing', show the typing message
    socket.on('typing', function(data) {
        console.log('typinnggg');
        addChatTyping(data);
    });

    // Whenever the server emits 'stop typing', kill the typing message
    socket.on('stop typing', function(data) {
        removeChatTyping(data);
    });
 */
    socket.on('disconnect', function() {
        log('you have been disconnected');
    });

    socket.on('reconnect', function() {
        log('you have been reconnected');
        if (username) {
            socket.emit('add user', username);
        }
    });

    socket.on('reconnect_error', function() {
        log('attempt to reconnect has failed');
    });

});