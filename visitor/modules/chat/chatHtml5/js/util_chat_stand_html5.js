/* 
 * 
 * CS:Chat stand 
 * CG:Chat général
 * 
 * */
var currentStandName = null;
//var checkboxChat_CSHandler = null;
var saveChatButton_CSHandler=null;
var stopSaveChatButton_CSHandler=null;
function traductChatHtml5Stand(){
	factoryTraductChatHtml5(CSTAG);
}
function startChat(idStand,topicStand, infoChatStand) {
	if(FlashDetect.installed){
		var targedDiv = $('#listStandchatters');
		targedDiv.empty();
		targedDiv.append('<div id="chatStandFlashContent"/>');   
		getAMSIp(function(ip){
	    	initSwfChatStand(idStand, topicStand,'chatStandFlashContent', infoChatStand, ip); //methode qui charge le swf CHAT Stand et passes changedParams.
	    });
	}else {
		$('#chatListStands_CS').show();
		$('#otherChooseStandLangAlert').remove();
		currentChatStand = topicStand;
		initChatStandComponent();
		getCacheStand();
		/*if (listStands[topicStand] == null)
			getChatCacheForStand(topicStand);
		else {
			getMessagesForStand(topicStand);
		}*/
	}
}

function buildTopicError(event) {
	//$('#content_tabs_stand').append('<br/><br/><center><b>Aucun stand n\'est disponible</b></center>');
	standNetConnectAlert(currentStandName);
}
function buildTopicSuccess(xml2, xmlParentCB, idLangToConnect) {
	var standLangs=$(xml2).find('body').text();
	if(standLangs!=""){
		var langs = standLangs.split(SEPARATESTANDANDLANGS)[1];
		if(langs!="" && langs!=null){
			var listStands="";
			var standId=$(xmlParentCB).find('standDTO:first').find('idstande').text();
			var eventId=$(xmlParentCB).find('eventDTO:first').find('idEvent').text();
			var j=0;
			var langsArray=langs.split(SEPARATELANGSSTAND);
			for(j in langsArray){   
				var langId=langsArray[j];
				//console.log(langId+" : "+getIdLangParam());
				if(langId==idLangToConnect){
					var topic = standId+ SEPARATEPUBLICCHATSO + langId + SEPARATEPUBLICCHATSO +eventId;
					//console.log(" topic : "+topic);
					currentChatStandLanguageId=langId;
					startChat(standId, topic, xmlParentCB);
					return;
				}
				var nameLang = listLangsEnum[langId];
				var drapeauUrl=getDrapeauUrl(langId);
				listStands=listStands+"<div class='startChatChoiceLang_btn'><span class='"+langId+"'>"+nameLang+"</span> : <img class='drapeauStand' alt='"+LANGUECHATLABEL+"' title='"+LANGUECHATLABEL+"' src='"+drapeauUrl+"'/></a></div><br/>";		
			}	
			standNetConnectChooseOtherLangAlert(currentStandName, listStands);
			$('.startChatChoiceLang_btn').click(function () {
				var langSelected=$(this).find('span').attr('class');
				var topic = standId + SEPARATEPUBLICCHATSO + langSelected + SEPARATEPUBLICCHATSO +eventId;
				currentChatStandLanguageId=langSelected;
				$('#chatInfos').empty();
				startChat(standId, topic, xmlParentCB);					
			});	
			//startChat(topics);
		}else standNetConnectAlert(currentStandName);
	}else standNetConnectAlert(currentStandName);
}
function getInfoForChatStandSuccess(xml) {
	
	if ($(xml).text() != "" && $(xml).find('idcondidat').text() != 0
			&& $(xml).find('idstande').text() != 0) {
		
		var langsubstitution=null;
		 $(xml).find('langsubstitution > idlangage').each(
	                function()
	                {
	                    $name = $(this).text();
	                    langsubstitution=$name;
	                }
	             );

		userId = $(xml).find('userProfileDTO').find('iduser').text();
		fullName = $(xml).find('userProfileDTO').find('prenomUser').text()
				+ ' ' + $(xml).find('userProfileDTO').find('nomUser').text();
		if ($(xml).find('userProfileDTO').find('logopath').text().indexOf(
				'default_avatar') !== -1)
			photoURL = "http://"+HOSTNAME_URL+"/visitor/images/default_avatar.jpg";
		else
			photoURL = $(xml).find('userProfileDTO').find('logopath').text();
		profileUrl = PROFILE_URL + $(xml).find('idcondidat').text();
		//console.log("*******************langsubstitution:"+langsubstitution);
		currentStandName = $(xml).find('standDTO').find('name').text();
		var idLang=null;
		if(langsubstitution == null){
			idLang=getIdLangParam();
		}else{	
			idLang=langsubstitution;	
		}
		genericAjaxCall('GET', staticVars["urlBackEndEnterprise"]
		+ 'languaget/topicBuilder/?idStand=' + currentStandId+'&idLang='+idLang,
		'application/xml', 'xml', '', function(xml2){
			buildTopicSuccess(xml2, xml, idLang);			
		},buildTopicError);
		//alert( "===>>>"+SEPARATEPUBLICCHATSO + langsubstitution + SEPARATEPUBLICCHATSO );
	}
}

function getInfoForChatStandError(xhr, status, error) {
	isSessionExpired(xhr.responseText);
}

function initChatStandHtml5() {
	$('#replay_date'+CSTAG).text('(' + currentDate() + ')');
	traductChatHtml5Stand();
	genericAjaxCall('POST', staticVars["urlBackEndCandidate"]
			+ 'chat/infoChatStand?standId=' + currentStandId+'&langID='+getIdLangParam(), //ici il faut changer idLang par idSub ...
			'application/xml', 'xml', '', getInfoForChatStandSuccess,
			getInfoForChatStandError);
}

function initChatStandComponent() {
	var inputTextChat_CSHandler= $('#inputTextChat'+CSTAG);
	var sendMessageStandButton_CSHandler= $('#sendMessageButton'+CSTAG);
	var chatReduceStandsButton_CSHandler= $('#chatReduceStandsButton'+CSTAG);
	var fullScreenChatSpan_CSHandler= $('#fullScreenChatSpan'+CSTAG);
	var exitFullScreenChatSpan_CSHandler= $('#exitFullScreenChatSpan'+CSTAG);
	//checkboxChat_CSHandler= $('#checkboxChat'+CSTAG);
	saveChatButton_CSHandler=$('#saveChatButton'+CSTAG);
	stopSaveChatButton_CSHandler=$('#stopSaveChatButton'+CSTAG);
	sendMessageStandButton_CSHandler.bind("click", function() {
		if (inputTextChat_CSHandler.val() != '' && currentStand_CS != null) {
			sendMessage(currentStand_CS, inputTextChat_CSHandler.val());
			inputTextChat_CSHandler.val('');
		}
	});

	inputTextChat_CSHandler.bind('keydown', function(e) {
		if (e.keyCode == 13 && e.ctrlKey) {
			if (inputTextChat_CSHandler.val() != '' && currentStand_CS != null) {
				sendMessage(currentStand_CS, inputTextChat_CSHandler.val());
				inputTextChat_CSHandler.val('');
			}
		}
	});
	chatReduceStandsButton_CSHandler.bind("click", function() {
		extendScreenChat('#chatListStands_CS .standsConnect', '#chatListStands_CS .separateStandsConnectChat', '#chatListStands_CS .coprsChat', '#fullScreenChatSpan_CS', '#exitFullScreenChatSpan_CS');
	});
	
	fullScreenChatSpan_CSHandler.bind("click", function() {
		extendScreenChat('#chatListStands_CS .standsConnect', '#chatListStands_CS .separateStandsConnectChat', '#chatListStands_CS .coprsChat', '#fullScreenChatSpan_CS', '#exitFullScreenChatSpan_CS');
	});
	
	exitFullScreenChatSpan_CSHandler.bind("click", function() {
		//console.log("exitFullScreenChatSpan_CSHandler");
		reduceScreenChat('#chatListStands_CS .standsConnect', '#chatListStands_CS .separateStandsConnectChat', '#chatListStands_CS .coprsChat', '#fullScreenChatSpan_CS', '#exitFullScreenChatSpan_CS');
	});
	/*checkboxChat_CSHandler.bind("change", function() {
        var check = $(this).is(":checked");
        if(currentStand_CS!=null){
        	var currentStandArray=currentStand_CS.split(SEPARATEPUBLICCHATSO);
            standId = currentStandArray[0];
            languageId = currentStandArray[1];
            chatId = currentStandArray[2];
            updateRegisterChatCall(check, standId, languageId, chatId);
        }else checkboxChat_CSHandler.prop('checked', !check);
	});*/
	$('#viewHistory'+CSTAG).click(function(){
		$('#userMyChatsLink').trigger("click");
		$('#reducePublicChat').trigger("click");
	});
	saveChatButton_CSHandler.click(function(){
		factoryUpdateRegisterChatCall(currentStand_CS, true);
	});
	stopSaveChatButton_CSHandler.click(function(){
		factoryUpdateRegisterChatCall(currentStand_CS, false);
	});
}
function getChatCacheForStandSuccess(json){
	addMessagesToStand(json, topicStand);
	getMessagesForStand(topicStand);
}
function getChatCacheForStandError(){
	console.log("error getChatCacheForStandError");
}
function getChatCacheForStand(topicStand) {
	//console.log("getChatCacheForStand(" + topicStand + ")");
	genericAjaxCall('GET', staticVars["urlBackEndEnterprise"]
		+'chat/chatmodule/' + topicStand + '/cache/chat',
	'application/json', 'json', '', getChatCacheForStandSuccess,
	getChatCacheForStandError);
}

function getMessagesForStand(topicStand) {
	if ($('#messages_ui_stand') != null)
		$('#messages_ui_stand').empty();
	if (listStands[topicStand] != null) {
		$.each(listStands[topicStand], function(key, message) {
			addMessageToStandChat('messages_ui_stand', message.messageId,
					message.senderLogin, message.messageText, message.photoURL,
					message.dateTime, message.operation);
		});
	}
}

function standNetConnectAlert(standName){
	
	
	//$('#chatListStands_CS, #otherChooseStandLangAlert').remove();
	$("#send-message-link").trigger('click');
	//$('#standNotConnectAlert').show();
	$('#modal-message .modal-title')
			.text(standName+ " " +STANDNOTYETCONNECTEDLABEL);
	//$('#closeChatStandSpan').text(CLOSELABEL);
	
	
}

function standNetConnectChooseOtherLangAlert(standName, listStands){
	$('#standNotConnectAlert').remove();
	$('#chatListStands_CS').hide();
	$('#otherChooseStandLangAlert').show();
	var chatNotDispoPhrase = STANDCHATNOTDISPO.replace(":STAND_NAME", standName);
	//chatNotDispoPhrase="<div id='chatInfos'><br/><br/><center><br/>"+ chatNotDispoPhrase+"<br/><br/>"+ tmpImgs+"</div>";	
	$('#alertChatStandOtherLangSpan')
			.text(chatNotDispoPhrase);
	$('#listStandsChatStandDiv').append(listStands);
}

$('#closeChatStandSpan').click(function() {
	$('.contentOpacity').hide();
	$('.content_infos').hide();
	$('#content_infos_reception').toggle("slide", {
		direction : "right"
	}, 300);
});
