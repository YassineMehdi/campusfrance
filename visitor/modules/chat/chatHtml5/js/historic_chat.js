function erreur(xml){
	console.log('Error :'+$(xml).text());
}
function addMessageToStandChat(idMessagesUi, senderLogin, messageText, dateTime){
	//console.log("messageText : "+messageText);
	try {
    	senderLogin=decodeURIComponent(senderLogin.trim());
    }
    catch(err) {
    	console.log("catch decodeURIComponent senderLogin");
    }
	try {
		messageText=decodeURIComponent(messageText.trim());
    }
    catch(err) {
    	console.log("catch decodeURIComponent messageText");
    }
    /*try {
		senderLogin=decode_utf8(senderLogin);
	}catch(err) {
    }
	try {
		messageText=decode_utf8(messageText);
	}catch(err) {
    }*/
	if (document.getElementById(idMessagesUi) != null) {
		$('#'+idMessagesUi).append('<tr>'+
							'<td class="extrDate" valign="top">'+
								'<div>'
									+ extractDate(dateTime) +
								'</div>'+
							'</td>'+
							'<td class="senderTab" style="width:100%" valign="top">'+
								'<span class="senderFullName">'
									+ htmlEncode(retrieveAllHtmlCast(senderLogin)) +
								' : </span>'+
								'<span>'
									+ htmlEncode(retrieveAllHtmlCast(messageText.trim())) +
								'</span>'+
							'</td>'+
						'</tr>');
	}	
}
function replaceAllHtmlCast(str){
	return str.split("'").join("&apos;").split('"').join("&quot;");
}
function retrieveAllHtmlCast(str){
	return str.split("&apos;").join("'").split('&quot;').join('"');
}
function parseXml(text) {
	text="<body>"+text+"</body>";
	var xml=jQuery.parseXML(text);
    $(xml).find("data").each(function () {
    	addMessageToStandChat("messages_history_ui", $(this).attr('senderLogin'), $(this).attr('messageText'), $(this).attr('date'));
    });
}

function readFile(){
	var path = $.base64.decode(getParam("path"));
	//console.log("link : "+link);
	$.ajax({
		type: "GET",
		url: path,
		dataType: "text",
		contentType : "text/plain; charset=UTF-8",
		cache: cacheAjax,
		success: parseXml,
		error: erreur,
		statusCode : {
			401: exceptionUnAutorized
		}
	});
}