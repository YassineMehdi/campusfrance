/* 
 * 
 * CS:Chat stand 
 * CG:Chat général
 * 
 * */
var countMessages = 0;
var subSocketArray = new Array();
var subSocket = null;
var timeBeforeSendConnectInfo = 5000;
var tryToReconnectToChat = 0;
var MAXTRYTORECONNECTTOCHAT = 30;
var CONNECTION_TOPIC = "connection";

function initStandAthmosphere(ip) {
    console.log(ip);
    console.log("----------------------")
    console.log(subSocket)
    if (subSocket == null) {
        var socket = $.atmosphere;
        var request = {
            url: HTML_URL.replace(":ip:", ip) + "chat/" + CONNECTION_TOPIC,
            contentType: "application/json",
            logLevel: 'debug',
            transport: 'websocket',
            fallbackTransport: 'long-polling'
        };

        request.onOpen = function(response) {
            //var message = jQuery.parseJSON(response);
            //console.log("onOpen : "+JSON.stringify(response));
            /**************************On a ralenti l'execution de push afin que suspend soit executé avant.**************************/
            tryToReconnectToChat--;
            incrementIpNbrUsed(ip, CONNECTION_TOPIC, pushToken);
            setTimeout(sendConnectionInfoCandidate, timeBeforeSendConnectInfo);

            //setInterval(sendConnectionInfoCandidate, 3000);
            //setInterval(sendDisconnectionInfoCandidate, 3987);
            //subSocket.push(jQuery.stringifyJSON({ topicStand: '', userId: userId, fullName: fullName, userType:userType, photoURL: photoURL , profileUrl : profileUrl, typeConnection:'user', isConnect:'true'}));
        };
        request.onReconnect = function(request, response) {
            console.log("Reconnecting");
        };
        request.onMessage = function(response) {
            var messageData = response.responseBody;
            try {
                var stand = jQuery.parseJSON(messageData);
            } catch (e) {
                console.log('This doesn\'t look like a valid JSON: ', messageData.data);
                return;
            }
            if (stand != null) {
                stand.chatTitle = decode_utf8(stand.chatTitle);
                stand.standName = decode_utf8(stand.standName);
                manageStandConnection(stand);
                chatBlink();
            }
        };

        request.onClose = function(response) {
            //console.log('onClose : Sorry, but there\'s some problem with your socket or the server is down state : '+response.state);
        };

        request.onError = function(response) {
            console.log('onError : Sorry, but there\'s some problem with your socket or the server is down state : ' + response.state);
            var state = response.state;
            if (state == "error" && subSocket != null) {
                console.log("****** Recconect to BUS ******");
                decrementIpNbrUsed(ip, CONNECTION_TOPIC, pushToken);
                tryToReconnectToChat++;
                if (tryToReconnectToChat < MAXTRYTORECONNECTTOCHAT) {
                    setTimeout(function() {
                        subSocket = null,
                            initStandAthmosphere(ip);
                    }, 30000);
                }
            }

        };

        subSocket = socket.subscribe(request);
    }

};

function sendConnectionInfoCandidate() {
    //console.log('***********************');
    if (subSocket != null) {
        subSocket.push(jQuery.stringifyJSON({ topicStand: '', userId: userId, fullName: fullName, userType: userType, photoURL: photoURL, profileUrl: profileUrl, typeConnection: 'user', isConnect: 'true', languageId: getIdLangParam() }));
        //console.log("******sendConnectionInfoCandidate ok ******");
    }
    //console.log('***********************');
}

function sendDisconnectionInfoCandidate() {
    //console.log('***********************');
    if (subSocket != null) {
        subSocket.push(jQuery.stringifyJSON({ topicStand: '', userId: userId, typeConnection: 'user', isConnect: 'false' }));
        //console.log("******sendDisconnectionInfoCandidate ok ******");
    }
    //console.log('***********************');
}

function sendConnectionToStandInfoCandidate(topicS) {
    //console.log('***********************');
    var subSock = subSocketArray[topicS];
    if (subSock != null) {
        subSock.push(jQuery.stringifyJSON({ topicStand: topicS, userId: userId, fullName: fullName, photoURL: photoURL, profileUrl: profileUrl, typeConnection: 'stand', isConnect: 'true' }));
        //console.log("******sendConnectionToStandInfoCandidate ok topicStand:"+topicS+"******");
        //console.log("onOpen : "+JSON.stringify(request));
    }
    //console.log('***********************');
}

function sendDisconnectionFromStandInfoCandidate(topicS) {
    //console.log('***********************');
    var subSock = subSocketArray[topicS];
    if (subSock != null) {
        subSock.push(jQuery.stringifyJSON({ topicStand: topicS, userId: userId, typeConnection: 'stand', isConnect: 'false' }));
        //console.log("******sendDisconnectionFromStandInfoCandidate ok topicStand:"+topicS+"******");
    }
    //console.log('***********************');
}

function initChatStandAthmosphere(topicS, ip) {
    //console.log('***********************');
    //console.log('Topic Traying:'+topicS);
    //console.log('***********************');
    if (subSocketArray[topicS] == null) {
        var socket = $.atmosphere;
        var request = {
            url: HTML_URL.replace(":ip:", ip) + "chat/" + topicS,
            contentType: "application/json",
            logLevel: 'debug',
            transport: 'websocket',
            fallbackTransport: 'long-polling'
        };
        request.onOpen = function(response) {
            //console.log('***********************:'+topicS);
            //console.log('Topic Opened:'+topicS);
            //console.log('***********************:'+topicS);	
            //var message = jQuery.parseJSON(response);
            tryToReconnectToChat--;
            incrementIpNbrUsed(ip, topicS, pushToken);
            setTimeout(sendConnectionToStandInfoCandidate, timeBeforeSendConnectInfo, topicS);

            //setInterval(sendConnectionToStandInfoCandidate, 5100, topicS,request);
            //setInterval(sendDisconnectionFromStandInfoCandidate, 3131, topicS);
            //subSocketArray[topicS].push(jQuery.stringifyJSON({ topicStand: topicS, userId:userId, fullName: fullName, photoURL : photoURL, profileUrl : profileUrl, typeConnection:'stand', isConnect:'true'}));
        };

        request.onReconnect = function(request, response) {
            console.log("Reconnecting, topicS : " + topicS);
        };
        request.onMessage = function(response) {
            var messageData = response.responseBody;
            try {
                var message = jQuery.parseJSON(messageData);
            } catch (e) {
                console.log('This doesn\'t look like a valid JSON: ', messageData.data);
                return;
            }
            if (message.receiverId == topicS) {
                receivedMessage(message.messageId, message.senderId, message.senderLogin, message.receiverId, message.messageText,
                    message.photoURL, message.dateTime, message.operation);
                //if (document.getElementById('messages_ui_stand')!=null) $('#messages_ui_stand').trigger('receiveMessage', message);
                //message.senderLogin=encodeURIComponent(message.senderLogin);
                //message.messageText=encodeURIComponent(message.messageText);
                addMessageToCache(message, topicS);
                chatBlink();
            }
        };

        request.onClose = function(response) {
            //console.log('onClose : Sorry, but there\'s some problem with your socket or the server is down, state : '+response.state);
        };

        request.onError = function(response) {
            console.log('onError : Sorry, but there\'s some problem with your socket or the server is down, state : ' + response.state);
            console.log("onError : topicS : " + topicS);
            var state = response.state;
            var subSock = subSocketArray[topicS];
            if (state == "error" && subSock != null) {
                console.log("****** Reconnecting to BUS ******");
                decrementIpNbrUsed(ip, topicS, pushToken);
                tryToReconnectToChat++;
                if (tryToReconnectToChat < MAXTRYTORECONNECTTOCHAT) {
                    setTimeout(function() {
                        delete subSocketArray[topicS];
                        initChatStandAthmosphere(topicS, ip);
                    }, 30000);
                }
            }
        };

        closeChatStandNetconnection(topicS);
        subSocketArray[topicS] = socket.subscribe(request);
    }

};

function closeChatGeneralNetconnection() {
    //todo
    if (subSocket != null) {
        if (subSock.request != null && subSock.request.url != null) {
            var ip = subSocket.request.url.split("/")[2];
            decrementIpNbrUsed(ip, CONNECTION_TOPIC, pushToken);
        }
        sendDisconnectionInfoCandidate();
        subSocket.close();
        subSocket = null;
        //console.log("******closeChatGeneralNetconnection ok ******");
    }
}

function closeChatStandNetconnection(topicS) {
    //todo
    var subSock = subSocketArray[topicS];
    if (subSock != null) {
        //console.log('***************closeChatStandNetconnection topicStand : '+topicS+"*******************");
        if (subSock.request != null && subSock.request.url != null) {
            var ip = subSock.request.url.split("/")[2];
            decrementIpNbrUsed(ip, topicS, pushToken);
        }
        sendDisconnectionFromStandInfoCandidate(topicS);
        subSock.close();
        delete subSocketArray[topicS];
    }
}
/*****************Avant de quitter le forum, il faut cloturer tous les websockets et notifier le backend***************/
function beforeExitForumChatNotify() {
    //console.log('beforeExitForumChatNotify');
    for (var topicS in subSocketArray) {
        closeChatStandNetconnection(topicS);
    }

    //console.log('userId : '+userId);
    closeChatGeneralNetconnection();
    $('#chatPublicComponent').empty();
    $("#contentChatPublic").attr('class', 'nonvisibleChat');
    $('#listStandchatters').empty();
}

function extendSessionSuccess(xml) {
    $('#chatPublicComponent').trigger('sendConnectInfo');
    setTimeout(extendSession, 60000 * 5); //planificateur sur 5 minute
};

function extendSessionError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
};

function extendSession() {
    genericAjaxCall('POST', staticVars["urlBackEndCandidate"] + 'chat/sessionExtender', 'application/xml', 'xml', '', extendSessionSuccess, extendSessionError, false);
};

function initAthmosphereListener() {
    setTimeout(extendSession, 60000 * 5); //planificateur sur 5 minute
}
$('#chatPublicComponent').bind('sendConnectInfo', function() {
    var i = 1;
    sendConnectionInfoCandidate();
    for (var topicS in subSocketArray) {
        setTimeout(sendConnectionToStandInfoCandidate, i * 500, topicS);
        i++;
    }
});
$(window).bind('beforeunload', function() {
    beforeExitForumChatNotify();
    return "";
});
$(document).ready(function() {
    $('#logout').click(function() {
        beforeExitForumChatNotify();
    });
});