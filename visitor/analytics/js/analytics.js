var _gaq = _gaq || [];
jQuery
		.extend({
			AnalyticsView : function() {
			var that=this;
	        if (workerSupport) {
	        	console.log("Worker is support");
	        	myWorker = new Worker(urlWorkers); 
						myWorker.addEventListener("message", function (oEvent) {
							var data=oEvent.data;
							//console.log("Worker (param1 : "+data.param1+", param2 :"+data.param2+", param3 :"+data.param3+")");
							sendToAnalytics(data.param1, data.param2, data.param3);
						}, false);
	        }else console.log("Worker isn't support");
			
			this.initAnalytics = function(){
				/*****************************Specific analytics*******************************/
				//console.log('initAnalytics');
				_gaq.push(['_setAccount', _AnalyticsCode]);
				(function () {
					var ga = document.createElement('script');
					ga.type = 'text/javascript';
					ga.async = true;
					ga.src = 'https://ssl.google-analytics.com/ga.js';
					var s = document.getElementsByTagName('script')[0];
					s.parentNode.insertBefore(ga, s);
				})();
	
				(function (i, s, o, g, r, a, m) {
				    i['GoogleAnalyticsObject'] = r;
				    i[r] = i[r] || function () {
				        (i[r].q = i[r].q || []).push(arguments);
				    }, i[r].l = 1 * new Date();
				    a = s.createElement(o),
				    m = s.getElementsByTagName(o)[0];
				    a.async = 1;
				    a.src = g;
				    m.parentNode.insertBefore(a, m);
				})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'gaEDSMaroc');

				gaEDSMaroc('create', _AnalyticsCode, HOSTNAME_URL);
				gaEDSMaroc('send', 'pageview');
			};
			that.initAnalytics();
			
				/***************************** Variables Utilisée Métiers par analyrics*******************************/
				var currentStandInfo = null;
				var candidateInfos = null;
				
				/*****************************Utils*******************************/
				function stringDate(date) {
				  var year = date.getUTCFullYear();
				  var month = date.getUTCMonth() + 1;
				  if (month < 10) month = '0' + month;
				  var day = date.getUTCDate();
				  if (day < 10) day = '0' + day;
				  var hours = date.getUTCHours();
				  if (hours < 10) hours = '0' + hours;
				  var minutes = date.getUTCMinutes();
				  if (minutes < 10) minutes = '0' + minutes;
				  var seconds = date.getUTCSeconds();
				  if (seconds < 10) seconds = '0' + seconds;
				  return [year, month, day].join('-') + ' ' +[hours, minutes, seconds].join(':');
				}
				
				//Calculate the difference of two dates in total min
				function diffDays(date1, date2){
					// We use the getTime() method and get the unixtime (in milliseconds, but we want seconds, therefore we divide it through 1000)
					date1_unixtime = parseInt(date1.getTime() / 1000);
					date2_unixtime = parseInt(date2.getTime() / 1000);
				
					// This is the calculated difference in seconds
					var timeDifference = date2_unixtime - date1_unixtime;
				
					// in Minutes
					var timeDifferenceInMins = timeDifference / 60;
				
					return timeDifferenceInMins;
				}

				function decodeHtml(str){
					return $(document.createElement('div')).html(str).text();
				}
				function sendToAnalytics(param1, param2, param3){
					//console.log("Begin sendToAnalytics (param1 : "+param1+", param2 :"+param2+", param3 :"+param3+")");
					_gaq.push([TRACKEVENT, param1, param2, param3]);
					//console.log("End sendToAnalytics ");
				}
				function postMessageToAnalytics(param1, param2, param3){
					if (workerSupport) {
						var data=new Object();
						data.param1= param1;
						data.param2= param2;
						data.param3= param3;
						myWorker.postMessage(data);
					}else sendToAnalytics(param1, param2, param3);
				}
				/*****************************Business Function*******************************/

				this.candidateIsConnected = function (candidate){
					candidateInfos=candidate;
					if(candidateInfos!=null){
						//console.log('candidateIsConnected, email: ' + candidateInfos.getEmail());
						postMessageToAnalytics(FORUM+'/'+LANGUAGEID+':'+getIdLangParam()+'/'+PROFILE+':'+CANDIDATE, candidateInfos.getEmail(), stringDate(new Date()));
					}
				};
				
				this.enterToStand = function (standXml){
					if(currentStandInfo == null && candidateInfos != null) {
						currentStandInfo=new Object();
						currentStandInfo.currentDate=new Date();
						currentStandInfo.standId=$(standXml).find('idstande').text();
						currentStandInfo.languageId=$(standXml).find('sustitutionLanguageId').text();
						//console.log("currentStandInfo.standId : "+currentStandInfo.standId);
						//console.log("currentStandInfo.languageId : "+currentStandInfo.languageId);
						that.enterToReception();
					}
				};
				
				this.exitFromStand = function (){
					if(currentStandInfo != null && candidateInfos != null){
						//console.log('exitFromStand, login: ' + candidateInfos.getEmail()+' , StandId : '+currentStandInfo.standId);
						var timeSpent=diffDays(currentStandInfo.currentDate, new Date());
						postMessageToAnalytics(STANDID+':'+currentStandInfo.standId+'/'+LANGUAGEID+':'+currentStandInfo.languageId+'/Profile:Candidate', candidateInfos.getEmail(), timeSpent+'|'+stringDate(currentStandInfo.currentDate));
						that.exitLocation();
						currentStandInfo=null;
					}
			    };
				
				this.enterToLocation = function (locationName){
					if(currentStandInfo != null) {
						//console.log('enterToLocation, locationName: ' + locationName+' , StandId : '+currentStandInfo.standId);
						var location=new Object();
						location.name=locationName;
						location.enterDate=new Date();
						currentStandInfo.location=location;
					}
				};
				
				this.exitLocation = function (){
					if(candidateInfos != null && currentStandInfo != null && currentStandInfo.location != null) {
						var locat=currentStandInfo.location;
						var timeSpent=diffDays(locat.enterDate, new Date());
						//console.log('exitLocation, locationName: ' + locat.name+' , StandId : '+currentStandInfo.standId);
						postMessageToAnalytics(STANDID+':'+currentStandInfo.standId+'/'+LANGUAGEID+':'+currentStandInfo.languageId+'/'+ZONE+':'+locat.name, candidateInfos.getEmail(), timeSpent+'|'+stringDate(locat.enterDate)); 
						currentStandInfo.location=null;
					}
				};
				
				this.enterToReception = function (){
					that.enterToLocation(RECEPTION);
				};
				
				this.exitReception = function (){
					that.exitLocation();
				};
				
				this.enterToMultimedia = function (){
					that.enterToLocation(MULTIMEDIA);
				};
				
				this.exitMultimedia = function (){
					that.exitLocation();
				};
				this.enterToPresse = function (){
					that.enterToLocation(PRESSE);
				};
				this.exitPresse = function (){
					that.exitLocation();
				};
				
				/*
				 * Reception
				 * */
				
				this.viewStandZone = function (zone){
					//console.log('viewStandZone');
					if(currentStandInfo!=null && candidateInfos!=null) {
						//console.log('StandId :  ' + currentStandInfo.standId+", zone : "+zone);
						postMessageToAnalytics(STANDID+':'+currentStandInfo.standId+'/'+LANGUAGEID+':'+currentStandInfo.languageId+'/'+ZONE+':'+zone, candidateInfos.getEmail(), stringDate(new Date()));
					}
				};

				/*
				 * Multimedia
				 * */
				this.viewStandZoneById = function (path, zoneElementId, standId){
					if(candidateInfos!=null) {
						if(currentStandInfo!=null) {
							standId=currentStandInfo.standId;
							languageId=currentStandInfo.languageId;
							that.viewStandZoneByIds(path, zoneElementId, currentStandInfo.standId, currentStandInfo.languageId);
						}else if(standId!=null){
							getStandSubstitutionLangId(standId, function(languageId){
								that.viewStandZoneByIds(path, zoneElementId, standId, languageId);
							}); 
						}
					}
				};
				this.viewStandZoneByIds = function (path, zoneElementId, standId, languageId){
					if(candidateInfos!=null) {
						postMessageToAnalytics(STANDID+':'+standId+'/'+LANGUAGEID+':'+languageId+'/'+MULTIMEDIA+':'+path+':'+zoneElementId, candidateInfos.getEmail(), stringDate(new Date()));
					}
				};

				/********
				 * 
				 * Chat 
				 * 
				 ********/
				this.viewPublicChat = function (){
					if(candidateInfos!=null) {
						//console.log('viewPublicChat , login :  ' + candidateInfos.getEmail());
						postMessageToAnalytics(PUBLICHCHAT, candidateInfos.getEmail(), stringDate(new Date()));
					}
				};
				this.viewWebSitesById = function (type, elementId, standId){
					if(candidateInfos!=null) {
						if(currentStandInfo!=null) {
							standId=currentStandInfo.standId;
							languageId=currentStandInfo.languageId;
							postMessageToAnalytics(STANDID+':'+standId+'/'+LANGUAGEID+':'+currentStandInfo.languageId+'/'+WEBSITE+':'+type+'/'+WEBSITEID+':'+elementId, candidateInfos.getEmail(), stringDate(new Date()));
						}else if(standId!=null){
							getStandSubstitutionLangId(standId, function(languageId){
								postMessageToAnalytics(STANDID+':'+standId+'/'+LANGUAGEID+':'+languageId+'/'+WEBSITE+':'+type+'/'+WEBSITEID+':'+elementId, candidateInfos.getEmail(), stringDate(new Date()));
							}); 
						}
					}
				};
				this.beforeExitForumAnalyticsNotify = function(){
					if(currentStandInfo!=null) {
						that.exitFromStand();
						currentStandInfo=null;
					}
					if(candidateInfos!=null) {
						candidateInfos=null;
					}
				};
				this.getCurrentStandId = function(){
					if(currentStandInfo!=null) {
						return currentStandInfo.standId;
					}else return 0;
				};
				//window.onload = function() {
					
				/************************************* Event Listener google analytics ****************************************/
					
					$('#logout').click(function() {
						that.beforeExitForumAnalyticsNotify();
					});
					
					/********
					 * 
					 * Stand 
					 * 
					 ********/
					 
					 $('#stand-video').click(
							function() {
								if(currentStandInfo != null) {
								  that.exitReception();
								  that.enterToMultimedia();
								}
					});
					
					$('#stand-audio').click(
							function() {
								if(currentStandInfo != null) {
							    that.exitReception();
									that.enterToPresse();
								}
					});

					$('.multimedia-to-reception').click(
							function() {
								if(currentStandInfo != null) {
									that.exitMultimedia();
									that.enterToReception();
								}
					});

					$('#go-to-press').click(
							function() {
								if(currentStandInfo != null) {
									that.exitMultimedia();
									that.enterToPresse();
								}
					});

					$('.press-to-reception').click(
							function() {
								if(currentStandInfo != null) {
									that.exitPresse();
									that.enterToReception();
								}
					});

					$('#go-to-video').click(
							function() {
								if(currentStandInfo != null) {
									that.exitPresse();
							    that.enterToMultimedia();
								}
					});

					$('#modal-profile-entreprise').on('shown.bs.modal', function () {
								analyticsView.viewStandZone(PROFILE);
					});
					$('#modal-temoignage').on('shown.bs.modal', function () {
								analyticsView.viewStandZone(TESTIMONIES);
					});
					$('#tchat-buttons').click(
							function() {
								analyticsView.viewStandZone(CHATTER);
					});
					$('#modal-message').on('shown.bs.modal', function () {
								analyticsView.viewStandZone(SENDMESSAGE);
					});
					$('#modal-sondage').on('shown.bs.modal', function () {
								analyticsView.viewStandZone(SURVEYS);
					});
					$('#modal-offre-emploi').on('shown.bs.modal', function () {
								analyticsView.viewStandZone(JOBOFFERS);
					});
					$('#modal-profile-entreprise-agenda').on('shown.bs.modal', function () {
								analyticsView.viewStandZone(AGENDA);
					});
					$('#modal-affiches').on('shown.bs.modal', function () {
								analyticsView.viewStandZone(POSTERS);
					});
					$('#modal-contact').on('shown.bs.modal', function () {
								analyticsView.viewStandZone(CONSTACTS);
					});
					$('#modal-document').on('shown.bs.modal', function () {
								analyticsView.viewStandZone(DOCUMENTS);
					});
					$('#modal-deposer-document').on('shown.bs.modal', function () {
								analyticsView.viewStandZone(POSTULATE);
					});
					$('#modal-presse').on('shown.bs.modal', function () {
								analyticsView.viewStandZone(ADVICES);
					});
					$('#modal-video').on('shown.bs.modal', function () {
								analyticsView.viewStandZone(VIDEOS);
					});
					$('#modal-photo').on('shown.bs.modal', function () {
								analyticsView.viewStandZone(PHOTOS);
					});
					$('#modal-profile-recherche').on('shown.bs.modal', function () {
								//analyticsView.viewStandZone(PHOTOS);
					});
					$('#modal-send-request-product').on('shown.bs.modal', function () {
				        analyticsView.viewStandZone(QUESTION_INFORMATIONS);
					});

					$(window).bind('beforeunload', function(){
						that.beforeExitForumAnalyticsNotify();
						//if(!FlashDetect.installed) $('#signOut').trigger('exitforum');
						return '';
					});
				//};
			}
		});

