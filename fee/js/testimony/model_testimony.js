jQuery.extend({
    TestimonyModel: function () {
        var that = this;
        var listeners = new Array();
        this.initModel = function () {
            that.getStandTestimonies();
        };
        // lister Poster
        this.getStandTestimoniesSuccess = function (xml) {
            var listTestimonies = new Array();
            $(xml).find("testimony").each(function () {
                var testimony = new $.Testimony($(this));
                listTestimonies[testimony.getTestimonyId()] = testimony;
            });
            that.notifyLoadTestimonies(listTestimonies);
        };

        this.getStandTestimoniesError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.getStandTestimonies = function () {
            genericAjaxCall('GET', staticVars.urlBackEnd + 'testimony/standTestimonies?idLang=' + getIdLangParam(), 'application/xml', 'xml', '',
                    that.getStandTestimoniesSuccess, that.getStandTestimoniesError);
        };
        // Add Testimony
        this.addTestimonySuccess = function (xml) {
            that.notifyAddTestimonySuccess();
            that.getStandTestimonies();
        };

        this.addTestimonyError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.addTestimony = function (testimony) {
            genericAjaxCall('POST', staticVars.urlBackEnd + 'testimony/addTestimony?idLang=' + getIdLangParam(), 'application/xml', 'xml', testimony.xmlData(),
                    that.addTestimonySuccess, that.addTestimonyError);
        };
        // Update Testimony
        this.updateTestimonySuccess = function (xml) {
            that.notifyUpdateTestimonySuccess();
            that.getStandTestimonies();
        };

        this.updateTestimonyError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.updateTestimony = function (testimony) {
            genericAjaxCall('POST', staticVars.urlBackEnd + 'testimony/updateTestimony?idLang=' + getIdLangParam(), 'application/xml', 'xml', testimony.xmlData(),
                    that.updateTestimonySuccess, that.updateTestimonyError);
        };
        // Delete Testimony
        this.deleteTestimonySuccess = function () {
            that.notifyDeleteTestimonySuccess();
            that.getStandTestimonies();
        };

        this.deleteTestimonyError = function (xhr, status, error) {
            //isSessionExpired(xhr.responseText);
        };

        this.deleteTestimony = function (testimonyId) {
            genericAjaxCall('POST', staticVars.urlBackEnd + 'testimony/deleteTestimony', 'application/xml', 'xml', testimonyId,
                    that.deleteTestimonySuccess, that.deleteTestimonyError);
        };

        this.addListener = function (list) {
            listeners.push(list);
        };
        this.notifyLoadTestimonies = function (listTestimonies) {
            $.each(listeners, function (i) {
                listeners[i].loadTestimonies(listTestimonies);
            });
        };
        this.notifyAddTestimonySuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].addTestimonySuccess();
            });
        };
        this.notifyDeleteTestimonySuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].deleteTestimonySuccess();
            });
        };
        this.notifyUpdateTestimonySuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].updateTestimonySuccess();
            });
        };

    },
    TestimonyModelListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }
});
