jQuery.extend({
    ConseilController: function (model, view) {
        var that = this;
        /**
         * listen to the view
         */
        var vlist = $.ConseilViewListener({
            addAdvice: function (advice) {
                model.addAdvice(advice);
            },
            updateAdvice: function (advice) {
                model.modelUpdateAdvice(advice);
            },
            deleteAdvice: function (adviceId) {
                model.modelDeleteAdvice(adviceId);
            }
        });

        view.addListener(vlist);

        /**
         * listen to the model
         */
        var mlist = $.ConseilModelListener({
            loadAdvices: function (listAdvices) {
                view.displayAdvices(listAdvices);
            },
            addAdviceSuccess: function (advice) {
                view.addAdviceSuccess(advice);
            },
            updateAdviceSuccess: function (advice) {
                view.viewUpdateAdviceSuccess(advice);
            },
            updateAdviceError: function (advice) {
                view.viewUpdateAdviceError(advice);
            },
            deleteAdviceSuccess: function (advice) {
                view.viewDeleteAdviceSuccess(advice);
            },
            deleteAdviceError: function (advice) {
                view.viewDeleteAdviceError(advice);
            },
            viewAdvice: function (advice) {
                view.viewAdvice(advice);
            }
        });

        model.addListener(mlist);
        
        this.getAnalyticsAdvices = function(callBack, path) {
            model.getAnalyticsAdvices(callBack, path);
        };

        this.initController = function () {
            view.initView();
            model.initModel();
        };
    }
});
