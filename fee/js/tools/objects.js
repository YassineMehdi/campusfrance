/*
 * this file contains all objects used in the frontend enterprise
 */
jQuery.extend({
    File: function (data) {
        var that = this;
        this.idFile = $(data).find("idfile").text();
        this.fileTitle = $(data).find("title").text();
        this.fileDescription = $(data).find("description").text();
        this.fileUrl = $(data).find("url").text();
        this.fileImage = $(data).find("image").text();
        this.fileName = htmlDecode($(data).find("fileName").text());
        this.fileType = new $.FileType($(data).find("fileTypeDTO"));

        this.getIdFile = function () {
            return this.idFile;
        };

        this.getFileTitle = function () {
            return this.fileTitle;
        };

        this.getFileDescription = function () {
            return this.fileDescription;
        };

        this.getFileUrl = function () {
            return this.fileUrl;
        };

        this.getFileImage = function () {
            return this.fileImage;
        };

        this.getFileName = function () {
            return this.fileName;
        };
        this.getFileType = function () {
            return this.fileType;
        };

        this.setIdFile = function (idFile) {
            that.idFile = idFile;
        };

        this.setFileTitle = function (fileTitle) {
            that.fileTitle = fileTitle;
        };

        this.setFileDescription = function (fileDescription) {
            that.fileDescription = fileDescription;
        };

        this.setFileUrl = function (fileUrl) {
            that.fileUrl = fileUrl;
        };


        this.setFileImage = function (fileImage) {
            that.fileImage = fileImage;
        };

        this.setFileName = function (fileName) {
            that.fileName = fileName;
        };

        this.setFileType = function (fileType) {
            that.fileType = fileType;
        };

        this.xmlData = function () {
            var xml = '<allfiles>';
            xml += '<idfile>' + that.idFile + '</idfile>';
            xml += '<title>' + htmlEncode(that.fileTitle) + '</title>';
            xml += '<description>' + htmlEncode(that.fileDescription) + '</description>';
            xml += '<url>' + that.fileUrl + '</url>';
            xml += '<image>' + that.fileImage + '</image>';
            xml += '<fileName>' + htmlEncode(that.fileName) + '</fileName>';
            xml += that.fileType.xmlData();
            xml += '</allfiles>';
            return xml;
        };
    },
    FileType: function (data) {
        var that = this;
        this.fileTypeId = $(data).find("idtypefile").text();
        this.getFileType = function () {
            return that.fileTypeId;
        };
        this.setFileTypeId = function (fileTypeId) {
            that.fileTypeId = fileTypeId;
        };
        this.xmlData = function () {
            var xml = '<fileTypeDTO>';
            xml += '<idtypefile>' + that.fileTypeId + '</idtypefile>';
            xml += '</fileTypeDTO>';
            return xml;
        };
    },

    Photo : function(data) {
        var that = this;
        this.idPhoto = $(data).find("idfile").text();
        this.photoTitle = $(data).find("title").text();
        this.photoUrl = $(data).find("url").text();
        this.photoDescription = $(data).find("description").text();
        this.fileName = $(data).find("fileName").text();
        this.fileType = new $.FileType($(data).find("fileTypeDTO"));
        this.getIdPhoto = function() {
            return that.idPhoto;
        };

        this.getPhotoTitle = function() {
            return that.photoTitle;
        };

        this.getPhotoDescription = function() {
            return that.photoDescription;
        };

        this.getPhotoUrl = function() {
            return that.photoUrl;
        };

        this.getFileType = function(fileType) {
            return fileType;
        };

        this.setPhotoId = function(idPhoto) {
            that.idPhoto = idPhoto;
        };

        this.setPhotoTitle = function(photoTitle) {
            that.photoTitle = photoTitle;
        };

        this.setPhotoDescription = function(photoDescription) {
            that.photoDescription = photoDescription;
        };

        this.setPhotoUrl = function(photoUrl) {
            that.photoUrl = photoUrl;
        };

        this.setFileType = function(fileType) {
            that.fileType = fileType;
        };

        this.xmlData = function() {
            var xml = '<allfiles>';
            xml += '<idfile>' + htmlEncode(that.idPhoto) + '</idfile>';
            xml += '<title>' + htmlEncode(that.photoTitle) + '</title>';
            xml += '<url>' + htmlEncode(that.photoUrl) + '</url>';
            xml += '<description>' + htmlEncode(that.photoDescription)
                    + '</description>';
            xml += '<fileName>' + htmlEncode(that.fileName)
                    + '</fileName>';
            xml += that.fileType.xmlData();
            xml += '</allfiles>';
            return xml;
        };
    },

    Video : function(data) {
        var that = this;
        this.idVideo = $(data).find("idfile").text();
        this.videoTitle = $(data).find("title").text();
        this.videoDescription = $(data).find("description").text();
        this.videoUrl = $(data).find("url").text();
        this.fileName = $(data).find("fileName").text();
        this.fileType = new $.FileType($(data).find("fileTypeDTO"));

        this.getIdVideo = function() {
            return this.idVideo;
        };

        this.getVideoTitle = function() {
            return this.videoTitle;
        };

        this.getVideoDescription = function() {
            return this.videoDescription;
        };

        this.getVideoUrl = function() {
            return this.videoUrl;
        };

        this.getFileType = function() {
            return this.fileType;
        };

        this.setIdVideo = function(idVideo) {
            that.idVideo = idVideo;
        };

        this.setVideoTitle = function(videoTitle) {
            that.videoTitle = videoTitle;
        };

        this.setVideoDescription = function(videoDescription) {
            that.videoDescription = videoDescription;
        };

        this.setVideoUrl = function(videoUrl) {
            that.videoUrl = videoUrl;
        };

        this.setFileType = function(fileType) {
            that.fileType = fileType;
        };
        this.xmlData = function() {
            var xml = '<allfiles>';
            xml += '<idfile>' + htmlEncode(that.idVideo) + '</idfile>';
            xml += '<title>' + htmlEncode(that.videoTitle) + '</title>';
            xml += '<description>' + htmlEncode(that.videoDescription)
                    + '</description>';
            xml += '<url>' + htmlEncode(that.videoUrl) + '</url>';
            xml += '<fileName>' + htmlEncode(that.fileName)
                    + '</fileName>';
            xml += that.fileType.xmlData();
            xml += '</allfiles>';
            return xml;
        };
    },

    Document : function(data) {
        var that = this;
        this.idDocument = $(data).find("idfile").text();
        this.documentTitle = $(data).find("title").text();
        this.documentDescription = $(data).find("description").text();
        this.documentUrl = $(data).find("url").text();
        this.fileName = $(data).find("fileName").text();
        this.fileType = new $.FileType($(data).find("fileTypeDTO"));

        this.getIdDocument = function() {
            return this.idDocument;
        };

        this.getDocumentTitle = function() {
            return this.documentTitle;
        };

        this.getDocumentDescription = function() {
            return this.documentDescription;
        };

        this.getDocumentUrl = function() {
            return this.documentUrl;
        };

        this.setDocumentId = function(idDocument) {
            that.idDocument = idDocument;
        };

        this.setDocumentTitle = function(documentTitle) {
            that.documentTitle = documentTitle;
        };

        this.setDocumentDescription = function(documentDescription) {
            that.documentDescription = documentDescription;
        };

        this.setDocumentUrl = function(documentUrl) {
            that.documentUrl = documentUrl;
        };

        this.setFileType = function(fileType) {
            that.fileType = fileType;
        };

        this.xmlData = function() {
            var xml = '<allfiles>';
            xml += '<idfile>' + htmlEncode(that.idDocument) + '</idfile>';
            xml += '<title>' + htmlEncode(that.documentTitle) + '</title>';
            xml += '<description>' + htmlEncode(that.documentDescription)
                    + '</description>';
            xml += '<url>' + htmlEncode(that.documentUrl) + '</url>';
            xml += '<fileName>' + htmlEncode(that.fileName)
                    + '</fileName>';
            xml += that.fileType.xmlData();
            xml += '</allfiles>';
            return xml;
        };
    },

    Testimony: function (data) {
        var that = this;
        this.idTestimony = $(data).find("idTestinomy").text();
        this.name = unescape($(data).find("name").text());
        this.photo = $(data).find("photo").text();
        this.jobTitle = unescape($(data).find("jobTitle").text());
        this.testimonyContent = unescape($(data).find("testimonyContent").text());

        this.getName = function () {
            return this.name;
        };
        this.getPhoto = function () {
            return this.photo;
        };
        this.getJobTitle = function () {
            return this.jobTitle;
        };
        this.getTestimonyContent = function () {
            return this.testimonyContent;
        };
        this.getTestimonyId = function () {
            return this.idTestimony;
        };

        this.setName = function (name) {
            that.name = name;
        };
        this.setPhoto = function (photo) {
            that.photo = photo;
        };
        this.setJobTitle = function (jobTitle) {
            that.jobTitle = jobTitle;
        };
        this.setTestimonyContent = function (testimonyContent) {
            that.testimonyContent = testimonyContent;
        };
        this.setTestimonyId = function (idTestimony) {
            that.idTestimony = idTestimony;
        };
        this.xmlData = function () {
            var xml = '<testimony>';
            xml += '<idTestinomy>' + that.idTestimony + '</idTestinomy>';
            xml += '<photo>' + that.photo + '</photo>';
            xml += '<name>' + htmlEncode(that.name) + '</name>';
            xml += '<jobTitle>' + htmlEncode(that.jobTitle) + '</jobTitle>';
            xml += '<testimonyContent>' + htmlEncode(that.testimonyContent) + '</testimonyContent>';
            xml += '</testimony>';
            return xml;
        };

    },
    StudyLevel: function (data) {
        var that = this;
        that.idStudyLevel = '';
        that.studyLevelName = '';

        this.studyLevel = function (data) {
            if (data) {
                if ($(data).find("idStudyLevel"))
                    that.idStudyLevel = $(data).find("idStudyLevel")
                            .text();
                if ($(data).find("studyLevelName"))
                    that.studyLevelName = $(data)
                            .find("studyLevelName").text();
            }
        };
        this.getIdStudyLevel = function () {
            return that.idStudyLevel;
        };
        this.getStudyLevelName = function () {
            return that.studyLevelName;
        };
        this.setIdStudyLevel = function (idStudyLevel) {
            that.idStudyLevel = idStudyLevel;
        };
        this.setStudyLevelName = function (studyLevelName) {
            that.studyLevelName = studyLevelName;
        };
        // Ajout d update
        this.update = function (newData) {
            that.studyLevel(newData);
        };
        this.xmlData = function () {
            var xml = '<studyLevelDTO>';
            xml += '<idStudyLevel>' + that.idStudyLevel
                    + '</idStudyLevel>';
            xml += '<studyLevelName>' + htmlEncode(that.studyLevelName)
                    + '</studyLevelName>';
            xml += '</studyLevelDTO>';
            return xml;
        };

        this.studyLevel(data);
    },
    FunctionCandidate: function (data) {
        var that = this;
        that.idFunction = '';
        that.functionName = '';

        this.functionCandidate = function (data) {
            if (data) {
                if ($(data).find("idFunction"))
                    that.idFunction = $(data).find("idFunction").text();
                if ($(data).find("functionName"))
                    that.functionName = $(data).find("functionName")
                            .text();
            }
        };
        this.getIdFunction = function () {
            return that.idFunction;
        };
        this.getFunctionName = function () {
            return that.functionName;
        };
        this.setIdFunction = function (idFunction) {
            that.idFunction = idFunction;
        };
        this.setFunctionName = function (functionName) {
            that.functionName = functionName;
        };
        this.update = function (newData) {
            that.functionCandidate(newData);
        };
        this.xmlData = function () {
            var xml = '<functionCandidateDTO>';
            xml += '<idFunction>' + that.idFunction + '</idFunction>';
            xml += '<functionName>' + htmlEncode(that.functionName)
                    + '</functionName>';
            xml += '</functionCandidateDTO>';
            return xml;
        };
        this.functionCandidate(data);
    },
    ExperienceYears: function (data) {
        var that = this;
        that.idExperienceYears = '';
        that.experienceYearsName = '';

        this.experienceYears = function (data) {
            if ($(data).find("idExperienceYears"))
                that.idExperienceYears = $(data).find(
                        "idExperienceYears").text();
            if ($(data).find("experienceYearsName"))
                that.experienceYearsName = $(data).find(
                        "experienceYearsName").text();
        };
        this.getIdExperienceYears = function () {
            return that.idExperienceYears;
        };
        this.getExperienceYearsName = function () {
            return that.experienceYearsName;
        };
        this.setIdExperienceYears = function (idExperienceYears) {
            that.idExperienceYears = idExperienceYears;
        };
        this.setExperienceYearsName = function (experienceYearsName) {
            that.experienceYearsName = experienceYearsName;
        };
        this.update = function (newData) {
            that.experienceYears(newData);
        };

        this.xmlData = function () {
            var xml = '<experienceYearsDTO>';
            xml += '<idExperienceYears>' + that.idExperienceYears
                    + '</idExperienceYears>';
            xml += '<experienceYearsName>'
                    + htmlEncode(that.experienceYearsName)
                    + '</experienceYearsName>';
            xml += '</experienceYearsDTO>';
            return xml;
        };

        this.experienceYears(data);
    },
    JobOffer: function (data) {
        var that = this;
        that.jobId = 0;
        that.jobTitle = "";
        that.jobReference = "";
        that.jobDescription = "";
        that.jobSkills = "";
        that.internalApplication = "";
        that.jobUrl = "";
        that.studyLevel = null;
        that.functionCandidate = null;
        that.experienceYears = null;
        that.alreadyTraduct = null;

        this.jobOffer = function (data) {
            if (data) {
                if ($(data).find("idmetier"))
                    that.jobId = $(data).find("idmetier").text();
                if ($(data).find("title"))
                    that.jobTitle = $(data).find("title").text();
                if ($(data).find("reference"))
                    that.jobReference = $(data).find("reference")
                            .text();
                if ($(data).find("description"))
                    that.jobDescription = unescape($(data).find(
                            "description").text());
                if ($(data).find("tags"))
                    that.jobSkills = $(data).find("tags").text();
                if ($(data).find("url"))
                    that.jobUrl = $(data).find("url").text();
                if ($(data).find("alreadyTraduct"))
                    that.alreadyTraduct = $(data)
                            .find("alreadyTraduct").text();
                if ($(data).find("studyLevelDTO"))
                    that.studyLevel = new $.StudyLevel($(data).find(
                            "studyLevelDTO"));
                if ($(data).find("functionCandidateDTO"))
                    that.functionCandidate = new $.FunctionCandidate($(
                            data).find("functionCandidateDTO"));
                if ($(data).find("experienceYearsDTO"))
                    that.experienceYears = new $.ExperienceYears(
                            $(data).find("experienceYearsDTO"));
            }
        };

        this.getJobId = function () {
            return this.jobId;
        };

        this.isAlreadyTraduct = function () {
            return this.alreadyTraduct;
        };

        this.getJobTitle = function () {
            return this.jobTitle;
        };

        this.getJobReference = function () {
            return this.jobReference;
        };

        this.getJobDescription = function () {
            return this.jobDescription;
        };

        this.getJobSkills = function () {
            return this.jobSkills;
        };

        this.isInternalApplication = function () {
            return this.internalApplication;
        };

        this.getJobUrl = function () {
            return this.jobUrl;
        };

        this.getSectors = function () {
            var listSectors = [];
            var $sectors = $(data).find("secteurActs").find(
                    "secteurActDTO");
            $sectors.each(function () {
                listSectors.push(new $.SecteurAct($(this)));
            });
            return listSectors;
        };

        this.getNameSectors = function () {
            var sectorsNames = [];
            var $sectors = $(data).find("secteurActs").find(
                    "secteurActDTO");
            $sectors.each(function () {
                sectorsNames.push($(this).find('name').text());
            });
            return sectorsNames.toString().split(',').join(', ');
        };

        this.getLanguagesId = function () {
            var languagesId = [];
            var $languages = $(data).find("languagesTranslation").find(
                    "languaget");
            $languages.each(function () {
                languagesId[$(this).find('idlangage').text()] = $(this)
                        .find('name').text();
            });
            return languagesId;
        };
        this.getNameLanguages = function () {
            var languagesNames = [];
            var $languages = $(data).find("languagesTranslation").find(
                    "languaget");
            $languages.each(function () {
                languagesNames.push(getLanguageName($(this).find(
                        'idlangage').text()));
            });
            languagesNames.sort();
            return languagesNames.toString().split(',').join(', ');
        };
        this.getLanguagesNumber = function () {
            return $(data).find("languagesTranslation").find(
                    "languaget").size();
        };
        this.getRegions = function () {
            var listRegions = [];
            var $regions = $(data).find("regions").find("regionDTO");
            $regions.each(function () {
                listRegions.push(new $.Region($(this)));
            });
            return listRegions;
        };

        this.getRegionNames = function () {
            var regionNames = [];
            var $regions = $(data).find("regions").find("regionDTO");
            $regions.each(function () {
                regionNames.push($(this).find('regionName').text());
            });
            return regionNames.toString().split(',').join(', ');
        };

        this.getStudyLevel = function () {
            return this.studyLevel;
        };

        this.getExperienceYears = function () {
            return that.experienceYears;
        };
        this.getFunctionCandidate = function () {
            return that.functionCandidate;
        };

        this.getStudyLevelName = function () {
            return $(data).find("studyLevelDTO").find("studyLevelName")
                    .text();
        };

        this.getExperienceYearsName = function () {
            return $(data).find("experienceYearsDTO").find(
                    "experienceYearsName").text();
        };
        this.getFunctionCandidateName = function () {
            return $(data).find("functionCandidateDTO").find(
                    "functionName").text();
        };

        this.jobOffer(data);

    },
    JobApplication: function (data) {
        var that = this;
        this.userId = $(data).find("userId").text();
        this.candidateFirstName = $(data).find("candidateFirstName").text();
        this.candidateLastName = $(data).find("candidateLastName").text();
        this.jobOfferTitle = $(data).find("jobOfferTitle").text();
        this.motivationLetter = unescape($(data).find("motivationLetter").text());
        this.urlCv = unescape($(data).find("urlCV").text());
        this.idJobApplication = $(data).find("idJobApplication").text();
        this.candidateId = $(data).find("candidateId").text();
        this.response = unescape($(data).find("response").text());
        this.candidatureDate = $(data).find("candidatureDate").text();
        this.jobOfferId = $(data).find("jobOfferId").text();
        if ($(data).find("unread").text() == "true")
            this.unread = true;
        else
            this.unread = false;

        this.getCandidateFirstName = function () {
            return this.candidateFirstName;
        };
        this.setCandidateFirstName = function (candidateFirstName) {
            that.candidateFirstName = candidateFirstName;
        };

        this.getCandidateLastName = function () {
            return this.candidateLastName;
        };
        this.setCandidateLastName = function (candidateLastName) {
            that.candidateLastName = candidateLastName;
        };

        this.getJobOfferReference = function () {
            return this.jobOfferReference;
        };
        this.setJobOfferReference = function (jobOfferReference) {
            that.jobOfferReference = jobOfferReference;
        };

        this.getMotivationLetter = function () {
            return this.motivationLetter;
        };
        this.setMotivationLetter = function (motivationLetter) {
            that.motivationLetter = motivationLetter;
        };

        this.getUserId = function () {
            return this.userId;
        };
        this.setUserId = function (userId) {
            that.userId = userId;
        };

        this.getUrlCv = function () {
            return this.urlCv;
        };
        this.setUrlCv = function (urlCv) {
            that.userId = urlCv;
        };

        this.getJobApplicationId = function () {
            return this.idJobApplication;
        };
        this.setJobApplicationId = function (idJobApplication) {
            that.idJobApplication = idJobApplication;
        };

        this.getCandidateId = function () {
            return this.candidateId;
        };
        this.setCandidateId = function (candidateId) {
            that.candidateId = candidateId;
        };

        this.getJobOfferTitle = function () {
            return this.jobOfferTitle;
        };
        this.setJobOfferTitle = function (jobOfferTitle) {
            that.jobOfferTitle = jobOfferTitle;
        };

        this.getFullName = function () {
            return that.candidateFirstName + " "
                    + that.candidateLastName;
        };

        this.getUnread = function () {
            return this.unread;
        };
        this.setUnread = function (unread) {
            this.unread = unread;
        };

        this.getResponse = function () {
            return this.response;
        };
        this.setResponse = function (response) {
            this.response = response;
        };

        this.xmlData = function () {
            var xml = '<jobApplication>';
            xml += '<idJobApplication>' + that.idJobApplication + '</idJobApplication>';
            xml += '<response>' + that.response + '</response>';
            xml += '</jobApplication>';
            return xml;
        };
    },
    UnsolicitedJobApplication: function (data) {
        var that = this;
        this.userId = $(data).find("candidateDTO").find('userProfileDTO').find('userProfileId').text();
        this.candidateId = $(data).find("candidateDTO").find('candidateId').text();
        this.candidateFirstName = $(data).find("candidateDTO").find('userProfileDTO').find('firstName').text();
        this.candidateLastName = $(data).find("candidateDTO").find('userProfileDTO').find('secondName').text();
        this.motivationLetter = unescape($(data).find("motivationLetter").text());
        this.urlCv = unescape($(data).find("urlCv").text());
        this.idJobApplication = $(data).find("idJobApplication").text();
        this.response = unescape($(data).find("response").text());
        if ($(data).find("unread").text() == "true")
            this.unread = true;
        else
            this.unread = false;
        this.getCandidateFirstName = function () {
            return this.candidateFirstName;
        };
        this.setCandidateFirstName = function (candidateFirstName) {
            this.candidateFirstName = candidateFirstName;
        };

        this.getCandidateLastName = function () {
            return this.candidateLastName;
        };
        this.setCandidateLastName = function (candidateLastName) {
            this.candidateLastName = candidateLastName;
        };

        this.getMotivationLetter = function () {
            return this.motivationLetter;
        };
        this.setMotivationLetter = function (motivationLetter) {
            this.motivationLetter = motivationLetter;
        };

        this.getUserId = function () {
            return this.userId;
        };
        this.setUserId = function (userId) {
            this.userId = userId;
        };

        this.getUrlCv = function () {
            return this.urlCv;
        };
        this.setUrlCv = function (urlCv) {
            this.urlCv = urlCv;
        };

        this.getJobApplicationId = function () {
            return this.idJobApplication;
        };
        this.setJobApplicationId = function (idJobApplication) {
            this.idJobApplication = idJobApplication;
        };

        this.getCandidateId = function () {
            return this.candidateId;
        };
        this.setCandidateId = function (candidateId) {
            this.candidateId = candidateId;
        };

        this.getResponse = function () {
            return this.response;
        };
        this.setResponse = function (response) {
            this.response = response;
        };

        this.getFullName = function () {
            return that.candidateFirstName + " "
                    + that.candidateLastName;
        };
        this.getUnread = function () {
            return this.unread;
        };
        this.setUnread = function (unread) {
            this.unread = unread;
        };

        this.xmlData = function () {
            var xml = '<unSolicitedJobApplication>';
            xml += '<idJobApplication>' + that.idJobApplication + '</idJobApplication>';
            xml += '<response>' + that.response + '</response>';
            xml += '</unSolicitedJobApplication>';
            return xml;
        };
    },

    AreaExpertise: function (data) {
        var that = this;
        that.areaExpertiseId = '';
        that.areaExpertiseName = '';
        this.areaExpertise = function (data) {
            if ($(data).find("areaExpertiseId"))
                that.areaExpertiseId = $(data).find("areaExpertiseId").text();
            if ($(data).find("areaExpertiseName"))
                that.areaExpertiseName = $(data).find("areaExpertiseName").text();
        };
        this.getAreaExpertiseId = function () {
            return that.areaExpertiseId;
        };
        this.getAreaExpertiseName = function () {
            return that.areaExpertiseName;
        };
        this.setAreaExpertiseId = function (areaExpertiseId) {
            that.areaExpertiseId = areaExpertiseId;
        };
        this.setAreaExpertiseName = function (areaExpertiseName) {
            that.areaExpertiseName = areaExpertiseName;
        };
        this.update = function (newData) {
          thishat.areaExpertise(newData);
        };
        this.xmlData = function () {
            var xml = '<areaExpertiseDTO>';
            xml += '<areaExpertiseId>' + that.areaExpertiseId + '</areaExpertiseId>';
            xml += '<areaExpertiseName>' + htmlEncode(that.areaExpertiseName) + '</areaExpertiseName>';
            xml += '</areaExpertiseDTO>';
             return xml;
        };
        this.areaExpertise(data);
    },

    SecteurAct : function(data) {
        var that = this;
        that.idsecteur = 0;
        that.name = '';

        this.secteurAct = function(data) {
            if (data) {
                if ($(data).find("idsecteur"))
                    that.idsecteur = $(data).find("idsecteur").text();
                if ($(data).find("name"))
                    that.name = $(data).find("name").text();
            }
        };
        this.getIdsecteur = function() {
            return that.idsecteur;
        };
        this.getName = function() {
            return that.name;
        };
        this.setIdsecteur = function(idsecteur) {
            that.idsecteur = idsecteur;
        };
        this.setName = function(name) {
            that.name = name;
        };
        this.update = function(newData) {
            that.secteurAct(newData);
        };
        this.xmlData = function() {
            var xml = '<secteurActDTO>';
            xml += '<idsecteur>' + that.idsecteur + '</idsecteur>';
            xml += '<name>' + htmlEncode(that.name) + '</name>';
            xml += '</secteurActDTO>';
            return xml;
        };

        this.secteurAct(data);
    },
    JobSought: function (data) {
        var that = this;
        that.jobSoughtId = '';
        that.jobSoughtName = '';
        that.enable = '';

        this.jobSought = function (data) {
            if ($(data).find("jobSoughtId"))
                that.jobSoughtId = $(data).find("jobSoughtId").text();
            if ($(data).find("jobSoughtName"))
                that.jobSoughtName = $(data).find("jobSoughtName")
                .text();
            if ($(data).find("enable"))
                that.enable = $(data).find("enable").text();
        };

        this.getJobSoughtId = function () {
            return that.jobSoughtId;
        };
        this.getJobSoughtName = function () {
            return that.jobSoughtName;
        };
        this.getEnable = function () {
            return that.enable;
        };
        this.setJobSoughtId = function (jobSoughtId) {
            that.jobSoughtId = jobSoughtId;
        };
        this.setJobSoughtName = function (jobSoughtName) {
            that.jobSoughtName = jobSoughtName;
        };
        this.setEnable = function (enable) {
            that.enable = enable;
        };
        this.update = function (newData) {
            that.jobSought(newData);
        };

        this.xmlData = function () {
            var xml = '<jobSoughtDTO>';
            xml += '<jobSoughtId>' + that.jobSoughtId
                + '</jobSoughtId>';
            xml += '<jobSoughtName>' + htmlEncode(that.jobSoughtName)
                + '</jobSoughtName>';
            xml += '</jobSoughtDTO>';
            return xml;
        };

        this.jobSought(data);
    },
    Region : function(data) {
        var that = this;
        that.idRegion = '';
        that.regionName = '';

        this.region = function(data) {
            if ($(data).find("idRegion"))
                that.idRegion = $(data).find("idRegion").text();
            if ($(data).find("regionName"))
                that.regionName = $(data).find("regionName").text();
        };
        this.getIdRegion = function() {
            return that.idRegion;
        };
        this.getRegionName = function() {
            return that.regionName;
        };
        this.setIdRegion = function(idRegion) {
            that.idRegion = idRegion;
        };
        this.setRegionName = function(regionName) {
            that.regionName = regionName;
        };
        this.update = function(newData) {
            that.region(newData);
        };

        this.xmlData = function() {
            var xml = '<regionDTO>';
            xml += '<idRegion>' + that.idRegion + '</idRegion>';
            xml += '<regionName>' + htmlEncode(that.regionName)
                    + '</regionName>';
            xml += '</regionDTO>';
            return xml;
        };
        this.region(data);
    },
    CandidateState : function(data) {
        var that = this;
        that.candidateStateId = '';
        that.candidateStateName = '';
        that.candidateStateDescription = '';
        
        this.candidateState = function(data) {
            if ($(data).find("candidateStateId"))
                that.candidateStateId = $(data).find("candidateStateId").text();
            if ($(data).find("candidateStateName"))
                that.candidateStateName = $(data).find("candidateStateName").text();
            if ($(data).find("candidateStateDescription"))
                that.candidateStateDescription = $(data).find("candidateStateDescription").text();
        };
        this.getCandidateStateId = function() {
            return that.candidateStateId;
        };
        this.getCandidateStateName = function() {
            return that.candidateStateName;
        };
        this.getCandidateStateDescription = function() {
            return that.candidateStateDescription;
        };
        this.setCandidateStateId = function(candidateStateId) {
            that.candidateStateId = candidateStateId;
        };
        this.setCandidateStateName = function(candidateStateName) {
            that.candidateStateName = candidateStateName;
        };
        this.setCandidateStateDescription = function(candidateStateDescription) {
            that.candidateStateDescription = candidateStateDescription;
        };
        this.update = function(newData) {
            that.candidateState(newData);
        };
        this.xmlData = function() {
            var xml = '<candidateStateDTO>';
            xml += '<candidateStateId>' + that.candidateStateId + '</candidateStateId>';
            xml += '<candidateStateName>' + htmlEncode(that.candidateStateName) + '</candidateStateName>';
            xml += '<candidateStateDescription>' + htmlEncode(that.candidateStateDescription) + '</candidateStateDescription>';
            xml += '</candidateStateDTO>';
            return xml;
        };
        this.candidateState(data);
    },
    StandRight : function(data) {
        this.rightId = $(data).find("idRightGroup").text();
        this.functionalityName = $(data).find("functionalityDTO")
                .find('name').text();
        this.rightCount = $(data).find("rightCount").text();

        this.getRightId = function() {
            return this.rightId;
        };

        this.getFunctionalityName = function() {
            return this.functionalityName;
        };

        this.getRightCount = function() {
            return this.rightCount;
        };

    },

    RightGroup : function(data) {
        var that = this;
        this.rightGroupId = null;
        this.functionality = null;
        this.rightCount = null;

        this.rightGroup = function(){
            if (data) {
                if ($(data).find("idRightGroup"))
                    that.rightGroupId = $(data).find("idRightGroup").text();
                if ($(data).find("functionalityDTO"))
                    that.functionality = new $.Functionality($(data).find("functionalityDTO"));
                if ($(data).find("rightCount"))
                    that.rightCount = $(data).find("rightCount").text();
            }
        };
        
        this.getRightGroupId = function() {
            return that.rightGroupId;
        };
        this.setRightGroupId = function(rightGroupId) {
            that.rightGroupId = rightGroupId;
        };

        this.getFunctionality = function() {
            return that.functionality;
        };
        this.setFunctionality = function(functionality) {
            that.functionality = functionality;
        };

        this.getRightCount = function() {
            return that.rightCount;
        };
        this.setRightCount = function(rightCount) {
            that.rightCount = rightCount;
        };
        
        this.rightGroup(data);
    },

    Functionality : function(data) {
        var that = this;
        this.functionalityId = null;
        this.functionalityName = null;
        this.functionalityDescription = null;

        this.functionality = function(){
            if (data) {
                if ($(data).find("idfunc"))
                    that.functionalityId = $(data).find("idfunc").text();
                if ($(data).find("name"))
                    that.functionalityName = $(data).find("name").text();
                if ($(data).find("descript"))
                    that.functionalityDescription = $(data).find("descript").text();
            }
        };
        
        this.getFunctionalityId = function() {
            return that.functionalityId;
        };
        this.setFunctionalityId = function(functionalityId) {
            that.functionalityId = functionalityId;
        };

        this.getFunctionalityName = function() {
            return that.functionalityName;
        };
        this.setFunctionalityName = function(functionalityName) {
            that.functionalityName = functionalityName;
        };

        this.getFunctionalityDescription = function() {
            return that.functionalityDescription;
        };
        this.setFunctionalityDescription = function(functionalityDescription) {
            that.functionalityDescription = functionalityDescription;
        };
        
        this.functionality(data);
    },

    Message: function(data){
        var that = this;
        that.idmessage = 0;
        that.messageDate = '';
        that.content = '';
        that.unread = false;
        that.sender = '';
        that.receiver = '';
        that.subject = '';
        
        this.message = function(data){
            if($(data).find("idmessage")) that.idmessage=$(data).find("idmessage").text();
            if($(data).find("messageDate")) that.messageDate=$(data).find("messageDate").text();
            if($(data).find("content")) that.content=$(data).find("content").text();
            if($(data).find("unread") && $(data).find("unread").text() == "true") that.unread=true;
            if($(data).find("subject")) that.subject=$(data).find("subject").text();
            if($(data).find("sender")) that.sender=new $.UserProfile($(data).find("sender"));
            if($(data).find("receiver")) that.receiver=new $.UserProfile($(data).find("receiver"));
        },
        
        this.getIdmessage = function(){ 
            return that.idmessage;
        },
        
        this.getMessageDate = function(){   
            return that.messageDate;
        },
        
        this.getContent = function(){       
            return that.content;
        },
        
        this.getUnread = function(){        
            return that.unread;
        },

        this.getSubject = function(){       
            return that.subject;
        },
        
        this.getSender = function(){        
            return that.sender;
        },
        
        this.getReceiver = function(){      
            return that.receiver;
        },
        
        this.setIdmessage= function(idmessage){ 
            that.idmessage=idmessage;
        },
        
        this.setMessageDate = function(messageDate){    
            that.messageDate=messageDate;
        },
        
        this.setContent = function(content){        
            that.content=content;
        },
        
        this.setUnread = function(unread){      
            that.unread=unread;
        },
        
        this.setSubject = function(subject){        
            that.subject=subject;
        },
        
        this.setSender = function(sender){      
            that.sender=sender;
        },

        this.setReceiver = function(sender){        
            that.receiver=sender;
        },
        
        this.xmlData = function(){
            var xml='<message>';
            xml+='<idmessage>'+that.idmessage+'</idmessage>';
            xml+='<messageDate>'+htmlEncode(that.messageDate)+'</messageDate>';
            xml+='<content>'+htmlEncode(that.content)+'</content>';
            xml+='<unread>'+htmlEncode(that.unread)+'</unread>';
            xml+='<subject>'+htmlEncode(that.subject)+'</subject>';
            if(that.sender != "") {
                xml+=that.sender.xmlData().replace('<userProfileDTO>','<sender>').replace('</userProfileDTO>','</sender>');
            }
            if(that.receiver != "") {
                xml+=that.receiver.xmlData().replace('<userProfileDTO>','<receiver>').replace('</userProfileDTO>','</receiver>');
            }
            xml+='</message>';
            return xml;
        };
        
        this.message(data);
    },

    Email : function(data) {
        var that = this;
        that.address = '';
        that.subject = '';
        that.body = '';
        
        this.email = function(data) {
            if ($(data).find("address"))
                that.address = $(data).find("address").text();
            if ($(data).find("subject"))
                that.subject = $(data).find("subject").text();
            if ($(data).find("body"))
                that.body = $(data).find("body").text();
        };
        this.getAddress = function() {
            return that.address;
        };
        this.getSubject = function() {
            return that.subject;
        };
        this.getBody = function() {
            return that.body;
        };
        this.setAddress = function(address) {
            that.address = address;
        };
        this.setSubject = function(subject) {
            that.subject = subject;
        };
        this.setBody = function(body) {
            that.body = body;
        };
        this.update = function(newData) {
            that.email(newData);
        };
        this.xmlData = function() {
            var xml = '<emailDTO>';
            xml += '<address>' + htmlEncode(that.address) + '</address>';
            xml += '<subject>' + htmlEncode(that.subject) + '</subject>';
            xml += '<body>' + htmlEncode(that.body) + '</body>';
            xml += '</emailDTO>';
            return xml;
        };
        this.email(data);
    },
    
    Candidate : function(data) {
        var that = this;
        that.candidateId = '';
        that.password = '';
        that.passwordClear = '';
        that.token = '';
        that.phoneNumber = '';
        that.jobTitle = '';
        that.email = '';
        that.se = '';
        that.userProfile = '';
        that.functionCandidate = '';
        that.experienceYears = '';
        that.secteurActs = '';
        that.regions = '';
        that.pdfCvs = '';
        that.candidateState = null;
        that.cvContent = new $.CVContent($(data).find("cvContentDTO"));
        that.cvKeywords = "";
        that.countryOrigin = '';
        that.areaExpertise = '';
        that.jobSought = '';
        that.cellPhone = '';
        that.gender = '';

        this.candidate = function(data) {
            if (data) {
                if ($(data).find("candidateId"))
                    that.candidateId = $(data).find("candidateId")
                            .text();
                if ($(data).find("password"))
                    that.password = $(data).find("password").text();
                if ($(data).find("passwordClear"))
                    that.passwordClear = $(data).find("passwordClear")
                            .text();
                if ($(data).find("token"))
                    that.token = $(data).find("token").text();
                if ($(data).find("phoneNumber"))
                    that.phoneNumber = $(data).find("phoneNumber")
                            .text();
                if ($(data).find("jobTitle"))
                    that.jobTitle = $(data).find("jobTitle").text();
                if ($(data).find("email"))
                    that.email = $(data).find("email").text();
                if ($(data).find("se"))
                    that.se = $(data).find("se").text();
                if ($(data).find("userProfileDTO"))
                    that.userProfile = new $.UserProfile($(data).find(
                            "userProfileDTO"));
                if ($(data).find("functionCandidateDTO"))
                    that.functionCandidate = new $.FunctionCandidate($(
                            data).find("functionCandidateDTO"));
                if ($(data).find("experienceYearsDTO"))
                    that.experienceYears = new $.ExperienceYears(
                            $(data).find("experienceYearsDTO"));
                if ($(data).find("candidateStateDTO"))
                    that.candidateState = new $.CandidateState($(data).find(
                            "candidateStateDTO"));
                that.secteurActs = new Array();
                $(data).find("secteurActDTO").each(function() {
                    var secteurAct = new $.SecteurAct($(this));
                    that.secteurActs.push(secteurAct);
                });
                that.regions = new Array();
                $(data).find("regionDTO").each(function() {
                    var region = new $.Region($(this));
                    that.regions.push(region);
                });
                that.pdfCvs = new Array();
                $(data).find("pdfCvDTO").each(function() {
                    var pdfCv = new $.PdfCv($(this));
                    that.pdfCvs.push(pdfCv);
                });

                $(data).find("cvkeywordsDTO").each(function() {
                    if (that.cvKeywords.length != 0)
                        that.cvKeywords = that.cvKeywords.concat(', ');
                    that.cvKeywords = that.cvKeywords.concat($(this).find(
                            'value').text()
                            + "(" + $(this).find('repetition').text() + ")");
                });
                if ($(data).find("countryOriginDTO"))
                    that.countryOrigin = new $.Region($(data).find("countryOriginDTO"));
                if ($(data).find("areaExpertiseDTO"))
                    that.areaExpertise = new $.AreaExpertise($(data).find("areaExpertiseDTO"));
                if ($(data).find("jobSoughtDTO"))
                    that.jobSought = new $.JobSought($(data).find("jobSoughtDTO"));
                if ($(data).find("cellPhone"))
                    that.cellPhone = $(data).find("cellPhone").text();
                if ($(data).find("gender"))
                    that.gender = $(data).find("gender").text();
            }
        };
        this.getCandidateId = function() {
            return that.candidateId;
        };
        this.getPassword = function() {
            return that.password;
        };
        this.getPasswordClear = function() {
            return that.passwordClear;
        };
        this.getToken = function() {
            return that.token;
        };
        this.getPhoneNumber = function() {
            return that.phoneNumber;
        };
        this.getJobTitle = function() {
            return that.jobTitle;
        };
        this.getEmail = function() {
            return that.email;
        };
        this.getSe = function() {
            return that.se;
        };
        this.getUserProfile = function() {
            return that.userProfile;
        };
        this.getFunctionCandidate = function() {
            return that.functionCandidate;
        };
        this.getExperienceYears = function() {
            return that.experienceYears;
        };
        this.getSecteurActs = function() {
            return that.secteurActs;
        };
        this.getRegions = function() {
            return that.regions;
        };
        this.getPdfCvs = function() {
            return that.pdfCvs;
        };
        this.getCVKeywords = function() {
            return that.cvKeywords;
        };
        this.getCandidateState = function() {
            return that.candidateState;
        };
        this.getAreaExpertise = function () {
          return that.areaExpertise;
        };
        this.getJobSought = function () {
          return that.jobSought;
        };
        this.getCellPhone = function () {
          return that.cellPhone;
        };
        this.getCountryOrigin = function () {
          return that.countryOrigin;
        };
        this.getGender = function () {
          return that.gender;
        };
        this.setCandidateId = function(candidateId) {
            that.candidateId = candidateId;
        };
        this.setPassword = function(password) {
            that.password = password;
        };
        this.setPasswordClear = function(passwordClear) {
            that.passwordClear = passwordClear;
        };
        this.setToken = function(token) {
            that.token = token;
        };
        this.setPhoneNumber = function(phoneNumber) {
            that.phoneNumber = phoneNumber;
        };
        this.setJobTitle = function(jobTitle) {
            that.jobTitle = jobTitle;
        };
        this.setEmail = function(email) {
            that.email = email;
        };
        this.setSe = function(se) {
            that.se = se;
        };
        this.setUserProfile = function(userProfile) {
            that.userProfile = userProfile;
        };
        this.setFunctionCandidate = function(functionCandidate) {
            that.functionCandidate = functionCandidate;
        };
        this.setExperienceYears = function(experienceYears) {
            that.experienceYears = experienceYears;
        };
        this.setSecteurActs = function(secteurActs) {
            that.secteurActs = secteurActs;
        };
        this.setRegions = function(regions) {
            that.regions = regions;
        };
        this.setPdfCvs = function(pdfCvs) {
            that.pdfCvs = pdfCvs;
        };
        this.setCandidateState = function(candidateState) {
            that.candidateState = candidateState;
        };
        this.setAreaExpertise = function (areaExpertise) {
          that.areaExpertise = areaExpertise;
        };
        this.setJobSought = function (jobSought) {
          that.jobSought = jobSought;
        };
        this.setCellPhone = function (cellPhone) {
          that.cellPhone = cellPhone;
        };
        this.setCountryOrigin = function (countryOrigin) {
          that.countryOrigin = countryOrigin;
        };
        this.setGender = function (gender) {
          that.gender = gender;
        };

        this.getFullName = function() {
            if(that.userProfile != null && that.userProfile != ""){
                return that.userProfile.getFirstName() + " "
                        + that.userProfile.getSecondName();
            }else return "";
        };
        
        this.getNameSectors = function() {
            var sectorsNames = new Array();
            var $sectors = $(data).find(
                    "secteurActDTO");
            $sectors.each(function() {
                sectorsNames.push($(this).find('name').text());
            });
            return sectorsNames.toString().split(',').join(', ');
        };
        this.getSectorIds = function() {
            var sectorActsIds = new Array();
            var sectorActs = that.secteurActs;
            if(sectorActs != null){
                for(var i=0; i < sectorActs.length; i++){
                    sectorActsIds.push(sectorActs[i].getIdsecteur());
                }
            } 
            return sectorActsIds;
        };
        this.getRegionNames = function() {
            var regionNames = new Array();
            var $regions = $(data).find("regionDTO");
            $regions.each(function() {
                regionNames.push($(this).find('regionName').text());
            });
            return regionNames.toString().split(',').join(', ');
        };
        this.getRegionIds = function() {
            var regionsIds = new Array();
            var regions = that.regions;
            if(regions != null){
                for(var i=0; i < regions.length; i++){
                    regionsIds.push(regions[i].getIdRegion());
                }
            } 
            return regionsIds;
        };
        this.getPdfCVsUrl = function(){
            var pdfCvsUrl = new Array();
            var pdfCvs = that.pdfCvs;
            if(pdfCvs != null){
                for(var i=0; i < pdfCvs.length; i++){
                    pdfCvsUrl.push(pdfCvs[i].getUrlCv());
                }
            } 
            return pdfCvsUrl.toString().split(',').join(', ');
        };
        this.searchInExperience = function(str) {
            return that.cvContent.searchInExperience(str);
        };
        this.searchInProject = function(str) {
            return that.cvContent.searchInProject(str);
        };
        this.searchInFormation = function(str) {
            return that.cvContent.searchInFormation(str);
        };
        this.searchInLanguage = function(str) {
            return that.cvContent.searchInLanguage(str);
        };
        this.searchWithKeyWord = function(keyWord) {
            if (compareToLowerString(that.userProfile.firstName, keyWord) !== -1
                    || compareToLowerString(that.userProfile.secondName, keyWord) !== -1
                    || compareToLowerString(that.email, keyWord) !== -1
                    || that.cvContent.searchWithKeyWord(keyWord))
                return true;
            else
                return false;
        };
        this.searchInActivitySectors = function(activitySectorId) {
            if (activitySectorId == 0 || that.getSectorIds().indexOf(activitySectorId) !== -1)
                return true;
            else
                return false;
        };
        this.searchInFunctionCandidate = function(functionCandidateId) {
            if (functionCandidateId == 0 || (that.functionCandidate && 
                    that.functionCandidate.getIdFunction() == functionCandidateId)){
                return true;
            }else
                return false;
        };
        this.searchInCountryResidence = function(countryResidenceId) {
            if (countryResidenceId == 0 || that.getRegionIds().indexOf(countryResidenceId) !== -1)
                return true;
            else
                return false;
        };
        this.searchInCountry = function(countryId) {
            if (countryId == 0 || (that.country && 
                    that.country.getCountryId() == countryId))
                return true;
            else
                return false;
        };
        this.searchInCountryOrigin = function(countryOriginId) {
            if (countryOriginId == 0 || (that.countryOrigin && 
                    that.countryOrigin.getIdRegion() == countryOriginId))
                return true;
            else
                return false;
        };
        this.searchInExperienceYears = function(experienceYearsId) {
            if (experienceYearsId == 0 || (that.experienceYears && 
                    that.experienceYears.getIdExperienceYears() == experienceYearsId))
                return true;
            else
                return false;
        };
        this.searchInAreaExpertise = function(areaExpertiseId) {
            if (areaExpertiseId == 0 || (that.areaExpertise && 
                    that.areaExpertise.getAreaExpertiseId() == areaExpertiseId))
                return true;
            else
                return false;
        };
        this.searchInJobSought = function(jobSoughtId) {
            if (jobSoughtId == 0 || (that.jobSought && 
                    that.jobSought.getJobSoughtId() == jobSoughtId))
                return true;
            else
                return false;
        };
        this.searchInCandidateState = function(candidateStateId) {
            if (candidateStateId == 0 || (that.candidateState && 
                    that.candidateState.getCandidateStateId() == candidateStateId))
                return true;
            else
                return false;
        };
        this.update = function(newData) {
            that.candidate(newData);
        };

        this.xmlData = function() {
            var xml = '<candidateDTO>';
            xml += '<candidateId>' + that.candidateId
                    + '</candidateId>';
            xml += '<password>' + htmlEncode(that.password)
                    + '</password>';
            xml += '<passwordClear>' + htmlEncode(that.passwordClear)
                    + '</passwordClear>';
            xml += '<token>' + htmlEncode(that.token) + '</token>';
            xml += '<phoneNumber>' + htmlEncode(that.phoneNumber)
                    + '</phoneNumber>';
            xml += '<jobTitle>' + htmlEncode(that.jobTitle)
                    + '</jobTitle>';
            xml += '<email>' + htmlEncode(that.email) + '</email>';
            xml += '<se>' + that.se + '</se>';
            if (that.userProfile != "")
                xml += that.userProfile.xmlData();
            if (that.functionCandidate != "")
                xml += that.functionCandidate.xmlData();
            if (that.experienceYears != "")
                xml += that.experienceYears.xmlData();
            if (that.candidateState != "")
                xml += that.candidateState.xmlData();

            xml += '<secteurActDTOlist>';
            for (index in that.secteurActs) {
                var secteurAct = that.secteurActs[index];
                xml += secteurAct.xmlData();
            }
            xml += '</secteurActDTOlist>';

            xml += '<regionDTOlist>';
            for (index in that.regions) {
                var region = that.regions[index];
                xml += region.xmlData();
            }
            xml += '</regionDTOlist>';
            xml += '<pdfCvDTOList>';
            for (index in that.pdfCvs) {
                var pdfCv = that.pdfCvs[index];
                xml += pdfCv.xmlData();
            }
            xml += '</pdfCvDTOList>';
            xml += '</candidateDTO>';
            return xml;
        };

        this.candidate(data);
    },
    CVContent : function(data) {
        var that = this;
        this.cvContentId = $(data).find("idcvcontent").text();
        this.cvContentTitle = $(data).find("titleCV").text();
        this.cvContentDescription = $(data).find("description").text();
        this.cvContentTags = $(data).find("tags").text();
        this.cvContentLeisures = $(data).find("leisures").text();
        this.cvContentLogo = $(data).find("logo").text();
        this.cvContentExperiences = [];
        $.each($(data).find("experiencecv"), function(index,
                experienceCV) {
            that.cvContentExperiences.push(new $.ExperienceCV(
                    experienceCV));
        });
        this.cvContentProjects = [];
        $.each($(data).find("projetcv"), function(index, projectCV) {
            that.cvContentProjects.push(new $.ProjectCV(projectCV));
        });
        this.cvContentFormations = [];
        $.each($(data).find("formationcv"),
                function(index, formationCV) {
                    that.cvContentFormations.push(new $.FormationCV(
                            formationCV));
                });
        this.cvContentLanguages = [];
        $.each($(data).find("cvLanguage"), function(index, languageCV) {
            that.cvContentLanguages.push(new $.LanguageCV(languageCV));
        });
        this.cvContentExperiencesData = $(data).find("experiences");
        this.cvContentProjectsData = $(data).find("projects");
        this.cvContentFormationsData = $(data).find("formations");
        this.cvContentLanguagesData = $(data).find("cvLanguages");
        this.searchInExperience = function(str) {
            if (compareToLowerString(that.cvContentExperiencesData
                    .text(), str) != -1)
                return true;
            else
                return false;
        };
        this.searchInProject = function(str) {
            if (compareToLowerString(that.cvContentProjectsData.text(),
                    str) != -1)
                return true;
            else
                return false;
        };
        this.searchInFormation = function(str) {
            if (compareToLowerString(that.cvContentFormationsData
                    .text(), str) != -1)
                return true;
            else
                return false;
        };
        this.searchInLanguage = function(str) {
            // if(that.cvContentLanguagesData.text()!="")
            // console.log("cvContentLanguagesData :
            // "+that.cvContentLanguagesData.text());
            if (compareToLowerString(
                    that.cvContentLanguagesData.text(), str) != -1)
                return true;
            else
                return false;
        };
        this.searchWithKeyWord = function(keyWord) {
            if (compareToLowerString(that.cvContentTitle, keyWord) !== -1
                    || compareToLowerString(that.cvContentTags, keyWord) !== -1
                    || compareToLowerString(that.cvContentFormationsData.text(), keyWord) !== -1
                    || compareToLowerString(that.cvContentExperiencesData.text(), keyWord) !== -1
                    || compareToLowerString(that.cvContentProjectsData.text(), keyWord) !== -1)
                return true;
            else
                return false;
        };
    },
    MessageFolderList:  function(data){
        var that = this;
        that.messageFolders='';
        
        this.messageFolderList = function(data){
            if(data){
                that.messageFolders = new Array();  
                $(data).find("messageFolder").each(function(){  
                    var messageFolder = new $.MessageFolder($(this));                           
                    that.messageFolders.push(messageFolder);    
                });
            }
        },
        
        this.getMessageFolders= function(){ 
            return that.messageFolders;
        },
        
        this.setMessageFolders = function(messageFolders){
            that.messageFolders = messageFolders;
        };
        
        this.xmlData = function(){
            var xml ='<messageFolderDTOlist>';
            for (index in that.messageFolders) {
                var messageFolder = that.messageFolders[index];
                xml+=messageFolder.xmlData();
            }
            xml+='</messageFolderDTOlist>';
            return xml;
        };
        
        this.messageFolderList(data);
    }, 

    MessageFolder: function(data){
        var that = this;
        that.idMessageFolder='';
        that.folderName='';
        that.numberMessages='';
        
        this.messageFolder = function(data){
            if($(data).find("idMessageFolder")) that.idMessageFolder=$(data).find("idMessageFolder").text();
            if($(data).find("folderName")) that.folderName=$(data).find("folderName").text();
            if($(data).find("numberMessages")) that.numberMessages=$(data).find("numberMessages").text();
        },

        this.getIdMessageFolder= function(){    
            return that.idMessageFolder;
        },

        this.getFolderName= function(){ 
            return that.folderName;
        },
        
        this.getNumberMessages= function(){ 
            return that.numberMessages;
        },
        
        this.setIdMessageFolder = function(idMessageFolder){
            that.idMessageFolder = idMessageFolder;
        };
        
        this.setFolderName = function(folderName){
            that.folderName = folderName;
        };
        
        this.setNumberMessages = function(numberMessages){
            that.numberMessages = numberMessages;
        };
        
        this.xmlData = function(){
            var xml='<messageFolder>';
            xml+='<idMessageFolder>' + that.idMessageFolder+'</idMessageFolder>';
            xml+='<folderName>' + htmlEncode(that.folderName) + '</folderName>';
            xml+='<numberMessages>' + that.numberMessages + '</numberMessages>';
            xml+='</messageFolder>';
            return xml;
        };
        
        this.messageFolder(data);
    },

    ConversationList:  function(data){
        var that = this;
        that.conversations = new Array();
        
        this.conversationList = function(data){
            if(data){
                that.conversations = new Array();   
                $(data).find("conversations").each(function(){  
                    var conversation = new $.Conversations($(this));                            
                    that.conversations.push(conversation);  
                });
            }
        },
        
        this.getConversations = function(){ 
            return that.conversations;
        },
        
        this.setConversations = function(conversations){
            that.conversations = conversations;
        };
        
        this.xmlData = function(){
            var xml ='<conversationDTOlist>';
            for (index in that.conversations) {
                var conversation = that.conversations[index];
                xml+=conversation.xmlData();
            }
            xml+='</conversationDTOlist>';
            return xml;
        };
        
        this.conversationList(data);
    },

    Conversations: function(data){
        var that = this;
        that.idConversation='';
        that.lastUpdate='';
        that.subject='';
        that.archived='';
        that.candidate='';
        that.messageFolder='';
        that.messages='';
        that.stand='';
        
        
        this.conversations = function(data){
            if($(data).find("idConversation")) that.idConversation=$(data).find("idConversation").text();
            if($(data).find("lastUpdate")) that.lastUpdate=$(data).find("lastUpdate").text();
            if($(data).find("subject")) that.subject=$(data).find("subject").text();
            if($(data).find("archived")) that.archived=$(data).find("archived").text();
            if($(data).find("candidate")) that.candidate=new $.Candidate($(data).find("candidate"));
            if($(data).find("messageFolder")) that.messageFolder=new $.MessageFolder($(data).find("messageFolder"));
            if($(data).find("messages")) that.messages=new $.MessageList($(data).find("messages"));
            if($(data).find("stand")) that.stand=new $.Stand($(data).find("stand"));
        },

        this.getIdConversation= function(){ 
            return that.idConversation;
        },

        this.getLastUpdate= function(){ 
            return that.lastUpdate;
        },

        this.getSubject= function(){    
            return that.subject;
        },

        this.getArchived= function(){   
            return that.archived;
        },

        this.getCandidate= function(){  
            return that.candidate;
        },

        this.getMessageFolder= function(){  
            return that.messageFolder;
        },

        this.getMessages= function(){   
            return that.messages;
        },

        this.getStand= function(){  
            return that.stand;
        },
        
        this.setIdConversation = function(idConversation){
            that.idConversation = idConversation;
        };
        
        this.setLastUpdate = function(lastUpdate){
            that.lastUpdate = lastUpdate;
        };
        
        this.setSubject = function(subject){
            that.subject = subject;
        };
        
        this.setArchived = function(archived){
            that.archived = archived;
        };
        
        this.setCandidate = function(candidate){
            that.candidate = candidate;
        };
        
        this.setMessageFolder = function(messageFolder){
            that.messageFolder = messageFolder;
        };
        
        this.setMessages = function(messages){
            that.messages = messages;
        };
        
        this.setStand = function(stand){
            that.stand = stand;
        };
        
        this.xmlData = function(){
            var xml='<conversations>';
            xml+='<idConversation>'+that.idConversation+'</idConversation>';
            xml+='<lastUpdate>'+htmlEncode(that.lastUpdate)+'</lastUpdate>';
            xml+='<subject>'+htmlEncode(that.subject)+'</subject>';
            xml+='<archived>'+htmlEncode(that.archived)+'</archived>';
            if(that.candidate != "") xml+=that.candidate.xmlData();
            if(that.messageFolder != "") xml+=that.messageFolder.xmlData();
            that.messages = new Array();    
            $(data).find("messages").each(function(){   
                var message = new $.Message($(this));                           
                that.messages.push(message);    
            });
            if(that.stand != "") xml+=that.stand.xmlData();
            xml+='</conversations>';
            return xml;
        };
        
        this.conversations(data);
    },
    
    MessageList:  function(data){
        var that = this;
        that.messages='';
        
        this.messageList = function(data){
            if(data){
                that.messages = new Array();    
                $(data).find("message").each(function(){    
                    var message = new $.Message($(this));                           
                    that.messages.push(message);    
                });
            }
        },
        
        this.getMessages= function(){   
            return that.messages;
        },
        
        this.setMessages = function(messages){
            that.messages = messages;
        };
        
        this.xmlData = function(){
            var xml ='<messages>';
            for (index in that.messages) {
                var message = that.messages[index];
                xml+=message.xmlData();
            }
            xml+='</messages>';
            return xml;
        };
        
        this.messageList(data);
    },
    
    Message: function(data){
        var that = this;
        that.idmessage = 0;
        that.messageDate = '';
        that.content = '';
        that.unread = false;
        that.sender = '';
        that.receiver = '';
        that.subject = '';
        
        this.message = function(data){
            if($(data).find("idmessage")) that.idmessage=$(data).find("idmessage").text();
            if($(data).find("messageDate")) that.messageDate=$(data).find("messageDate").text();
            if($(data).find("content")) that.content=$(data).find("content").text();
            if($(data).find("unread") && $(data).find("unread").text() == "true") that.unread=true;
            if($(data).find("subject")) that.subject=$(data).find("subject").text();
            if($(data).find("sender")) that.sender=new $.UserProfile($(data).find("sender"));
            if($(data).find("receiver")) that.receiver=new $.UserProfile($(data).find("receiver"));
        },
        
        this.getIdmessage = function(){ 
            return that.idmessage;
        },
        
        this.getMessageDate = function(){   
            return that.messageDate;
        },
        
        this.getContent = function(){       
            return that.content;
        },
        
        this.getUnread = function(){        
            return that.unread;
        },

        this.getSubject = function(){       
            return that.subject;
        },
        
        this.getSender = function(){        
            return that.sender;
        },
        
        this.getReceiver = function(){      
            return that.receiver;
        },
        
        this.setIdmessage= function(idmessage){ 
            that.idmessage=idmessage;
        },
        
        this.setMessageDate = function(messageDate){    
            that.messageDate=messageDate;
        },
        
        this.setContent = function(content){        
            that.content=content;
        },
        
        this.setUnread = function(unread){      
            that.unread=unread;
        },
        
        this.setSubject = function(subject){        
            that.subject=subject;
        },
        
        this.setSender = function(sender){      
            that.sender=sender;
        },

        this.setReceiver = function(sender){        
            that.receiver=sender;
        },
        
        this.xmlData = function(){
            var xml='<message>';
            xml+='<idmessage>'+that.idmessage+'</idmessage>';
            xml+='<messageDate>'+htmlEncode(that.messageDate)+'</messageDate>';
            xml+='<content>'+htmlEncode(that.content)+'</content>';
            xml+='<unread>'+htmlEncode(that.unread)+'</unread>';
            xml+='<subject>'+htmlEncode(that.subject)+'</subject>';
            if(that.sender != "") {
                xml+=that.sender.xmlData().replace('<userProfileDTO>','<sender>').replace('</userProfileDTO>','</sender>');
            }
            if(that.receiver != "") {
                xml+=that.receiver.xmlData().replace('<userProfileDTO>','<receiver>').replace('</userProfileDTO>','</receiver>');
            }
            xml+='</message>';
            return xml;
        };
        
        this.message(data);
    },
    Country : function(data) {
        var that = this;
        that.countryId = '';
        that.countryName = '';
        this.country = function(data) {
          if ($(data).find("countryId"))
            that.countryId = $(data).find("countryId").text();
          if ($(data).find("countryName"))
            that.countryName = $(data).find("countryName").text();
        };
        this.getCountryId = function() {
          return that.countryId;
        };
        this.getCountryName = function() {
          return that.countryName;
        };
        this.setCountryId = function(countryId) {
          that.countryId = countryId;
        };
        this.setCountryName = function(countryName) {
          that.countryName = countryName;
        };
        this.update = function(newData) {
          that.country(newData);
        };
        this.xmlData = function() {
          var xml = '<countryDTO>';
          xml += '<countryId>' + that.countryId + '</countryId>';
          xml += '<countryName>' + htmlEncode(that.countryName) + '</countryName>';
          xml += '</countryDTO>';
          return xml;
        };
        this.country(data);
    },
    
    UserProfile : function(data) {
        var that = this;
        that.userProfileId = '';
        that.firstName = '';
        that.secondName = '';
        that.avatar = '';
        that.pseudoSkype = '';

        this.userProfile = function(data) {
            if ($(data).find("userProfileId"))
                that.userProfileId = $(data).find("userProfileId")
                        .text();
            if ($(data).find("firstName"))
                that.firstName = $(data).find("firstName").text();
            if ($(data).find("secondName"))
                that.secondName = $(data).find("secondName").text();
            if ($(data).find("avatar"))
                that.avatar = $(data).find("avatar").text();
            if ($(data).find("pseudoSkype"))
                that.pseudoSkype = $(data).find("pseudoSkype").text();
        };
        this.getUserProfileId = function() {
            return that.userProfileId;
        };
        this.getFirstName = function() {
            return that.firstName;
        };
        this.getSecondName = function() {
            return that.secondName;
        };
        this.getAvatar = function() {
            return that.avatar;
        };
        this.getPseudoSkype = function() {
            return that.pseudoSkype;
        };
        this.setUserProfileId = function(userProfileId) {
            that.userProfileId = userProfileId;
        };
        this.setFirstName = function(firstName) {
            that.firstName = firstName;
        };
        this.setSecondName = function(secondName) {
            that.secondName = secondName;
        };
        this.setAvatar = function(avatar) {
            that.avatar = avatar;
        };
        this.setPseudoSkype = function(pseudoSkype) {
            that.pseudoSkype = pseudoSkype;
        };
        this.setCVKeywords = function(cvKeywords) {
            that.cvKeywords = cvKeywords;
        };
        this.update = function(newData) {
            that.userProfile(newData);
        };
        this.xmlData = function() {
            var xml = '<userProfileDTO>';
            xml += '<userProfileId>' + that.userProfileId
                    + '</userProfileId>';
            xml += '<firstName>' + htmlEncode(that.firstName)
                    + '</firstName>';
            xml += '<secondName>' + htmlEncode(that.secondName)
                    + '</secondName>';
            xml += '<avatar>' + htmlEncode(that.avatar) + '</avatar>';
            xml += '<pseudoSkype>' + htmlEncode(that.pseudoSkype)
                    + '</pseudoSkype>';
            xml += '</userProfileDTO>';
            return xml;
        };

        this.userProfile(data);
    },

    Favorite : function(data) {
        this.favoriteId = $(data).find("idFavorite").text();
        this.componenetId = $(data).find("idEntity").text();
        this.componentName = $(data).find("component").find(
                "componentName").text();

        this.getFavoriteId = function() {
            return this.favoriteId;
        };
        this.getComponentId = function() {
            return this.componenetId;
        };
        this.getComponentName = function() {
            return this.componentName;
        };

        this.equals = function(idComp, compName) {
            if ((this.componenetId == idComp)
                    && (this.componentName == compName)) {
                return true;
            } else {
                return false;
            }
        };

    },

    PdfCv : function(data) {
        var that = this;
        that.idPdfCv = '';
        that.cvName = '';
        that.urlCv = '';
        this.pdfCv = function(data) {
            if ($(data).find("idPdfCv"))
                that.idPdfCv = $(data).find("idPdfCv").text();
            if ($(data).find("cvName"))
                that.cvName = $(data).find("cvName").text();
            if ($(data).find("urlCv"))
                that.urlCv = $(data).find("urlCv").text();
        };
        this.getIdPdfCv = function() {
            return that.idPdfCv;
        };
        this.getCvName = function() {
            return that.cvName;
        };
        this.getUrlCv = function() {
            return that.urlCv;
        };
        this.setIdPdfCv = function(idPdfCv) {
            that.idPdfCv = idPdfCv;
        };
        this.setCvName = function(cvName) {
            that.cvName = cvName;
        };
        this.setUrlCv = function(urlCv) {
            that.urlCv = urlCv;
        };
        this.update = function(newData) {
            that.pdfCv(newData);
        };
        this.xmlData = function() {
            var xml = '<pdfCvDTO>';
            xml += '<idPdfCv>' + that.idPdfCv + '</idPdfCv>';
            xml += '<urlCv>' + htmlEncode(that.urlCv) + '</urlCv>';
            xml += '<cvName>' + htmlEncode(that.cvName) + '</cvName>';
            xml += '</pdfCvDTO>';
            return xml;
        };
        this.pdfCv(data);
    },

    Product: function(data){
        var that = this;
        that.productId='';
        that.productTitle='';
        that.productDescription='';
        that.productImage='';
        that.resources=[];
        that.alreadyTraduct = false;
        that.languages=[];
        
        that.product = function(data){
            if(data){
                if($(data).find("productId")) 
                    that.productId = $(data).find("productId").text();
                if($(data).find("productTitle")) 
                    that.productTitle = $(data).find("productTitle").text();
                if($(data).find("productDescription")) 
                    that.productDescription = $(data).find("productDescription").text();
                if($(data).find("productImage")) 
                    that.productImage = $(data).find("productImage").text();
                that.resources = new Array();   
                $(data).find("resourceDTO").each(function(){    
                    var resource = new $.Resource($(this));                         
                    that.resources.push(resource);  
                });
                if($(data).find("alreadyTraduct") && $(data).find("alreadyTraduct").text() == "true") 
                    that.alreadyTraduct = true;
                else that.alreadyTraduct = false;
                that.languages = new Array();   
                $(data).find("languagesTranslation languaget").each(function(){ 
                    var language = new $.Language($(this));                         
                    that.languages[language.getLanguageId()]=language;  
                });
            }
        };
        
        this.getProductId= function(){  
            return that.productId;
        };
        
        this.getProductTitle = function(){  
            return that.productTitle;
        };
        
        this.getProductDescription = function(){    
            return that.productDescription;
        };
        
        this.getProductImage = function(){  
            return that.productImage;
        };
        
        this.getResources = function(){ 
            return that.resources;
        };
        
        this.getAlreadyTraduct = function(){    
            return that.alreadyTraduct;
        };
        
        this.getLanguages = function(){ 
            return that.languages;
        };
        
        this.setProductId = function(productId){
            that.productId = productId;
        };
        
        this.setProductTitle = function(productTitle){
            that.productTitle = productTitle;
        };
        
        this.setProductDescription = function(productDescription){
            that.productDescription = productDescription;
        };
        
        this.setProductImage = function(productImage){
            that.productImage = productImage;
        };
        
        this.setResources = function(resources){
            that.resources = resources;
        };
        
        this.setAlreadyTraduct = function(alreadyTraduct){
            that.alreadyTraduct = alreadyTraduct;
        };
        
        this.setLanguages = function(languages){
            that.languages = languages;
        };
        
        this.getNameLanguages = function() {
            var languagesNames = new Array();
            var $languages = $(data).find("languagesTranslation languaget");
            $languages.each(function() {
                languagesNames.push(getLanguageName($(this).find('idlangage').text()));
            });
            languagesNames.sort();
            return languagesNames.toString().split(',').join(', ');
        };
        
        this.update = function(newData){
            that.product(newData);
        };
        
        this.xmlData = function(){
            var xml='<productDTO>';
            xml+='<productId>'+that.productId+'</productId>';
            xml+='<productTitle>'+htmlEncode($.trim(that.productTitle))+'</productTitle>';
            xml+='<productDescription>'+htmlEncode($.trim(that.productDescription))+'</productDescription>';
            xml+='<productImage>'+htmlEncode($.trim(that.productImage))+'</productImage>';
            if(that.resources != ''){
                xml+='<resourceDTOList>';
                for (var index in that.resources) {
                    var resource = that.resources[index];
                    xml+=resource.xmlData();
                }
                xml+='</resourceDTOList>';
            }
            xml+='</productDTO>';
            return xml;
        };
        
        this.product(data);
    },

    QuestionInfo: function(data){
        var that = this;
        that.questionInfoId = '';
        that.questionInfoTitle = '';
        that.question = '';
        that.response = '';
        that.questionInfoUrl = '';
        that.questionInfoDate = '';
        that.unread = false;
        that.candidate = null;
        that.product = null;
        
        this.questionInfo = function(data){
            if(data){
                if($(data).find("questionInfoId")) 
                    that.questionInfoId = $(data).find("questionInfoId").text();
                if($(data).find("questionInfoTitle")) 
                    that.questionInfoTitle = $(data).find("questionInfoTitle").text();
                if($(data).find("question")) 
                    that.question = $(data).find("question").text();
                if($(data).find("response")) 
                    that.response = $(data).find("response").text();
                if($(data).find("questionInfoUrl")) 
                    that.questionInfoUrl = $(data).find("questionInfoUrl").text();
                if($(data).find("questionInfoDate")) 
                    that.questionInfoDate=that.dateFromString($(data).find("questionInfoDate").text()); 
                if($(data).find("unread") && $(data).find("unread").text() == "true") 
                    that.unread = true;
                if($(data).find("candidateDTO")) 
                    that.candidate=new $.Candidate($(data).find("candidateDTO"));
                if($(data).find("productDTO")) 
                    that.product=new $.Product($(data).find("productDTO"));
            }
        };
        
        this.dateFromString= function(s) {
              var bits = s.split(/[-T:+]/g);
              var d = new Date(bits[0], bits[1]-1, bits[2]);
              d.setHours(bits[3], bits[4], bits[5]);

              // Get supplied time zone offset in minutes
              var offsetMinutes = bits[6] * 60 + Number(bits[7]);
              var sign = /\d\d-\d\d:\d\d$/.test(s)? '-' : '+';

              // Apply the sign
              offsetMinutes = 0 + (sign == '-'? -1 * offsetMinutes : offsetMinutes);

              // Apply offset and local timezone
              d.setMinutes(d.getMinutes() - offsetMinutes - d.getTimezoneOffset());

              // d is now a local time equivalent to the supplied time
              return d;
        };
        
        this.getQuestionInfoId= function(){ 
            return that.questionInfoId;
        };
        
        this.getQuestionInfoTitle= function(){  
            return that.questionInfoTitle;
        };
        
        this.getQuestion = function(){  
            return that.question;
        };
        
        this.getResponse = function(){  
            return that.response;
        };
        
        this.getQuestionInfoUrl = function(){   
            return that.questionInfoUrl;
        };
        
        this.getQuestionInfoDate = function(){  
            return that.questionInfoDate;
        };

        this.getUnread = function(){    
            return that.unread;
        };

        this.getCandidate = function(){ 
            return that.candidate;
        };

        this.getProduct = function(){   
            return that.product;
        };
        
        this.setQuestionInfoId = function(questionInfoId){
            that.questionInfoId=questionInfoId;
        };
        
        this.setQuestionInfoTitle = function(questionInfoTitle){
            that.questionInfoTitle=questionInfoTitle;
        };
        
        this.setQuestion = function(question){
            that.question=question;
        };
        
        this.setResponse = function(response){
            that.response=response;
        };
        
        this.setQuestionInfoUrl = function(questionInfoUrl){
            that.questionInfoUrl=questionInfoUrl;
        };
        
        this.setQuestionInfoDate = function(questionInfoDate){
            that.questionInfoDate=questionInfoDate;
        };
        
        this.setUnread = function(unread){
            that.unread=unread;
        };
        
        this.setCandidate = function(candidate){
            that.candidate=candidate;
        };
        
        this.setProduct = function(product){
            that.product=product;
        };
        
        this.update = function(newData){
            that.questionInfo(newData);
        };
        
        this.xmlData = function(){
            var xml='<questionInfoDTO>';
            xml+='<questionInfoId>'+that.questionInfoId+'</questionInfoId>';
            xml+='<questionInfoTitle>'+htmlEncode($.trim(that.questionInfoTitle))+'</questionInfoTitle>';
            xml+='<question>'+htmlEncode($.trim(that.question))+'</question>';
            xml+='<response>'+htmlEncode($.trim(that.response))+'</response>';
            xml+='<questionInfoUrl>'+htmlEncode($.trim(that.questionInfoUrl))+'</questionInfoUrl>';
            xml+='<unread>'+htmlEncode($.trim(that.unread))+'</unread>';
            if(that.candidate != null) 
                xml+=that.candidate.xmlData();
            if(that.product != null) 
                xml+=that.product.xmlData();
            xml+='</questionInfoDTO>';
            return xml;
        };
        
        this.questionInfo(data);
    },
    
    Resource: function(data){
        var that = this;
        that.resourceId='';
        that.resourceName='';
        that.creationDate='';
        that.resourceType='';
        that.resourceUrl='';
        this.resource = function(data){
            if(data){
                if($(data).find("resourceId")) that.resourceId=$(data).find("resourceId").text();
                if($(data).find("resourceName")) that.resourceName=$(data).find("resourceName").text();
                if($(data).find("dateCreation")) that.creationDate=that.dateFromString($(data).find("creationDate").text()).toString('dddd, MMMM ,yyyy'); 
                if($(data).find("resourceTypeDTO")) that.resourceType=new $.ResourceType($(data).find("resourceTypeDTO"));
                if($(data).find("resourceUrl")) that.resourceUrl=$(data).find("resourceUrl").text();
            }
        };
        
        this.dateFromString= function(s) {
              var bits = s.split(/[-T:+]/g);
              var d = new Date(bits[0], bits[1]-1, bits[2]);
              d.setHours(bits[3], bits[4], bits[5]);

              // Get supplied time zone offset in minutes
              var offsetMinutes = bits[6] * 60 + Number(bits[7]);
              var sign = /\d\d-\d\d:\d\d$/.test(s)? '-' : '+';

              // Apply the sign
              offsetMinutes = 0 + (sign == '-'? -1 * offsetMinutes : offsetMinutes);

              // Apply offset and local timezone
              d.setMinutes(d.getMinutes() - offsetMinutes - d.getTimezoneOffset());

              // d is now a local time equivalent to the supplied time
              return d;
        };
        
        this.getResourceId= function(){ 
            return that.resourceId;
        };
        
        this.getResourceName = function(){  
            return that.resourceName;
        };
        
        this.getResourceUrl = function(){   
            return that.resourceUrl;
        };
        
        this.getCreationDate = function(){  
            return that.creationDate;
        };

        this.getResourceType = function(){  
            return that.resourceType;
        };
        
        this.setResourceId = function(resourceId){
            that.resourceId=resourceId;
        };
        
        this.setResourceName = function(resourceName){
            that.resourceName=resourceName;
        };
        
        this.setResourceUrl = function(resourceUrl){
            that.resourceUrl=resourceUrl;
        };
        
        this.setCreationDate = function(creationDate){
            that.creationDate=creationDate;
        };
        
        this.setResourceType = function(resourceType){
            that.resourceType=resourceType;
        };
        
        this.update = function(newData){
            that.resource(newData);
        };
        
        this.xmlData = function(){
            var xml='<resourceDTO>';
            xml+='<resourceId>'+that.resourceId+'</resourceId>';
            xml+='<resourceName>'+htmlEncode($.trim(that.resourceName))+'</resourceName>';
            xml+='<resourceUrl>'+htmlEncode($.trim(that.resourceUrl))+'</resourceUrl>';
            xml+=that.resourceType.xmlData();
            xml+='</resourceDTO>';
            return xml;
        };
        
        this.resource(data);
    },
    
    ResourceType: function(data){
        var that = this;
        that.resourceTypeId='';
        that.resourceTypeName='';
        that.resourceType = function(data){
            if(data){
                if($(data).find("resourceTypeId")) that.resourceTypeId=$(data).find("resourceTypeId").text();
                if($(data).find("resourceName")) that.resourceTypeName=$(data).find("resourceTypeName").text();
            }
        };
        
        this.getResourceTypeId= function(){ 
            return that.resourceTypeId;
        };
        
        this.getResourceTypeName = function(){  
            return that.resourceTypeName;
        };
        
        this.setResourceTypeId = function(resourceTypeId){
            that.resourceTypeId=resourceTypeId;
        };
        
        this.setResourceTypeName = function(resourceTypeName){
            that.resourceTypeName=resourceTypeName;
        };
        
        this.update = function(newData){
            that.resourceType(newData);
        };
        
        this.xmlData = function(){
            var xml='<resourceTypeDTO>';
            xml+='<resourceTypeId>'+that.resourceTypeId+'</resourceTypeId>';
            xml+='<resourceTypeName>'+htmlEncode($.trim(that.resourceTypeName))+'</resourceTypeName>';
            xml+='</resourceTypeDTO>';
            return xml;
        };
        
        this.resourceType(data);
    },
    
    EnterpriseP: function(data){
        var that = this;
        that.enterpriseId=0;
        that.login='';
        that.password='';
        that.jobTitle='';
        that.token='';
        that.email='';
        that.isAdmin=false;
        that.userProfile=null;
        that.description='';
        that.cellPhone = '';
        that.enterprise = null;
        that.type = 'EnterpriseP';
        
        that.enterpriseP = function(data){
            if(data){
                if($(data).find("identrprise").first()) {
                    if(data[0] != undefined) that.enterpriseId=$(data[0]).find("> identrprise:first").text();
                    else that.enterpriseId=$(data).find("identrprise:first-child").text();
                }
                if($(data).find("login")) 
                    that.login=$(data).find("login").text();
                if($(data).find("password")) 
                    that.password=$(data).find("password").text();
                if($(data).find("jobTitle")) 
                    that.jobTitle=$(data).find("jobTitle").text();
                if($(data).find("token")) 
                    that.token=$(data).find("token").text();
                if($(data).find("email")) 
                    that.email=$(data).find("email").text();
                if($(data).find("isAdmin") && $(data).find('isAdmin').text() == "true") 
                    that.isAdmin=true;
                else that.isAdmin=false;
                if($(data).find("userProfileDTO")) 
                    that.userProfile=new $.UserProfile($(data).find("userProfileDTO"));
                if($(data).find("description")) 
                    that.description=$(data).find("description").text();
                if ($(data).find("cellPhone"))
                  that.cellPhone = $(data).find("cellPhone").text();
                if ($(data).find("enterpriseDTO"))
                    that.enterprise = new $.Enterprise($(data).find("enterpriseDTO"));
            }
        };
        
        this.getEnterprisePId= function(){  
            return that.enterpriseId;
        };
        
        this.getLogin = function(){ 
            return that.login;
        };
        
        this.getPassword = function(){  
            return that.password;
        };
        
        this.getJobTitle = function(){  
            return that.jobTitle;
        };
        
        this.getToken = function(){ 
            return that.token;
        };
        
        this.getEmail = function(){ 
            return that.email;
        };
        
        this.getIsAdmin = function(){   
            return that.isAdmin;
        };
        
        this.getUserProfile = function(){   
            return that.userProfile;
        };
        
        this.getDescription = function(){   
            return that.description;
        };

        this.getCellPhone = function () {
          return that.cellPhone;
        };
        
        this.getEnterprise = function () {
            return that.enterprise;
        };
        
        this.setEnterprisePId = function(enterpriseId){
            that.enterpriseId=enterpriseId;
        };
        
        this.setLogin = function(login){
            that.login=login;
        };
        
        this.setPassword = function(password){
            that.password=password;
        };
        
        this.setJobTitle = function(jobTitle){
            that.jobTitle=jobTitle;
        };
        
        this.setToken = function(token){
            that.token=token;
        };
        
        this.setEmail = function(email){
            that.email=email;
        };
        
        this.setIsAdmin = function(isAdmin){
            that.isAdmin=isAdmin;
        };
        
        this.setUserProfile = function(userProfile){
            that.userProfile=userProfile;
        };
        
        this.setDescription = function(description){
            that.description=description;
        };

        this.setCellPhone = function (cellPhone) {
          that.cellPhone = cellPhone;
        };
        
        this.setEnterprise = function (enterprise) {
            that.enterprise = enterprise;
        };
        
        this.getFullName = function() {
            if(that.userProfile != null && that.userProfile != ""){
                return that.userProfile.getFirstName() + " "
                        + that.userProfile.getSecondName();
            }else return "";
        };
        
        this.update = function(newData){
            that.enterpriseP(newData);
        };
        
        this.xmlData = function(){
            var xml='<enterprisep>';
            xml+='<identrprise>'+that.enterpriseId+'</identrprise>';
            xml+='<login>'+htmlEncode(that.login)+'</login>';
            xml+='<password>'+htmlEncode($.trim(that.password))+'</password>';
            xml+='<jobTitle>'+htmlEncode($.trim(that.jobTitle))+'</jobTitle>';
            xml+='<token>'+htmlEncode($.trim(that.token))+'</token>';
            xml+='<email>'+htmlEncode($.trim(that.email))+'</email>';
            xml+='<cellPhone>'+htmlEncode($.trim(that.cellPhone))+'</cellPhone>';
            xml+='<isAdmin>'+htmlEncode($.trim(that.isAdmin))+'</isAdmin>';
            if(that.userProfile != null) 
                xml+=that.userProfile.xmlData();
            xml+='<description>'+htmlEncode($.trim(that.description))+'</description>';
            xml+='</enterprisep>';
            return xml;
        };
        
        this.enterpriseP(data);
    },
    Language: function(data){
        var that = this;
        that.languageId='';
        that.languageName='';
        
        that.language = function(data){
            if(data){
                if($(data).find("idlangage")) 
                    that.languageId=$(data).find("idlangage").text();
                if($(data).find("name")) 
                    that.languageName=$(data).find("name").text();
            }
        };
        
        this.getLanguageId= function(){ 
            return that.languageId;
        };
        
        this.getLanguageName = function(){  
            return that.languageName;
        };
        
        this.setLanguageId = function(languageId){
            that.languageId=languageId;
        };
        
        this.setLanguageName = function(languageName){
            that.languageName=languageName;
        };
        
        this.update = function(newData){
            that.language(newData);
        };
        
        this.xmlData = function(){
            var xml='<languageDTO>';
            xml+='<idlangage>'+that.languageId+'</idlangage>';
            xml+='<name>'+htmlEncode($.trim(that.languageName))+'</name>';
            xml+='</languageDTO>';
            return xml;
        };
        
        this.language(data);
    },
    
    SEO: function(data){
        var that = this;
        that.seoId='';
        that.seoTitle='';
        that.seoMetadata='';
        that.seoKeywords='';
        that.seoFirstLink='';
        that.seoSecondeLink='';
        that.seoThirdLink='';
        
        that.seo = function(data){
            if(data){
                if($(data).find("seoId")) 
                    that.seoId=$(data).find("seoId").text();
                if($(data).find("seoTitle")) 
                    that.seoTitle=$(data).find("seoTitle").text();
                if($(data).find("seoMetadata")) 
                    that.seoMetadata=$(data).find("seoMetadata").text();
                if($(data).find("seoKeywords")) 
                    that.seoKeywords=$(data).find("seoKeywords").text();
                if($(data).find("seoFirstLink")) 
                    that.seoFirstLink=$(data).find("seoFirstLink").text();
                if($(data).find("seoSecondeLink")) 
                    that.seoSecondeLink=$(data).find("seoSecondeLink").text();
                if($(data).find("seoThirdLink")) 
                    that.seoThirdLink=$(data).find("seoThirdLink").text();
            }
        };
        
        this.getSeoId= function(){  
            return that.seoId;
        };
        
        this.getSeoTitle = function(){  
            return that.seoTitle;
        };
        
        this.getSeoMetadata = function(){   
            return that.seoMetadata;
        };
        
        this.getSeoKeywords = function(){   
            return that.seoKeywords;
        };
        
        this.getSeoFirstLink = function(){  
            return that.seoFirstLink;
        };
        
        this.getSeoSecondeLink = function(){    
            return that.seoSecondeLink;
        };
        
        this.getSeoThirdLink = function(){  
            return that.seoThirdLink;
        };
        
        this.setSeoId = function(seoId){
            that.seoId=seoId;
        };
        
        this.setSeoTitle = function(seoTitle){
            that.seoTitle=seoTitle;
        };
        
        this.setSeoMetadata = function(seoMetadata){
            that.seoMetadata=seoMetadata;
        };
        
        this.setSeoKeywords = function(seoKeywords){
            that.seoKeywords=seoKeywords;
        };
        
        this.setSeoFirstLink = function(seoFirstLink){
            that.seoFirstLink=seoFirstLink;
        };
            
        this.setSeoSecondeLink = function(seoSecondeLink){
            that.seoSecondeLink=seoSecondeLink;
        };
            
        this.setSeoThirdLink = function(seoThirdLink){
            that.seoThirdLink=seoThirdLink;
        };
                    
        this.update = function(newData){
            that.seo(newData);
        };
        
        this.xmlData = function(){
            var xml='<seoDTO>';
            xml+='<seoId>'+that.seoId+'</seoId>';
            xml+='<seoTitle>'+htmlEncode($.trim(that.seoTitle))+'</seoTitle>';
            xml+='<seoMetadata>'+htmlEncode($.trim(that.seoMetadata))+'</seoMetadata>';
            xml+='<seoKeywords>'+htmlEncode($.trim(that.seoKeywords))+'</seoKeywords>';
            xml+='<seoFirstLink>'+htmlEncode($.trim(that.seoFirstLink))+'</seoFirstLink>';
            xml+='<seoSecondeLink>'+htmlEncode($.trim(that.seoSecondeLink))+'</seoSecondeLink>';
            xml+='<seoThirdLink>'+htmlEncode($.trim(that.seoThirdLink))+'</seoThirdLink>';
            xml+='</seoDTO>';
            return xml;
        };
        
        this.seo(data);
    },

    ExperienceCV : function(data) {

    },
    ProjectCV : function(data) {

    },
    FormationCV : function(data) {

    },
    LanguageCV : function(data) {

    },


});
