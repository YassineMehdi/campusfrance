var browserLang = window.navigator.language;
function getParam(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    if (results == null)
        return "";
    else
        return results[1];
}
function getIdLangParam() {
    var idLang=getParam("idLang");
    if(idLang == "" || idLang == null || ([1, 2, 3, 4, 5, 6].indexOf(idLang) != -1)) {
        switch (browserLang) {
            case 'fr':
                idLang=3;
                break;
            case 'en':
                idLang=2;
                break;
            default:
                idLang=3;
                break;
        }
        window.location.replace("?idLang="+idLang);
    }
    return idLang;
}

function getLocalePDFJS() {
    var idLang = getParam("idLang");
    var locale = null;
    switch (idLang) {
        case 1:
            locale = 'ar';
            break;
        case 2:
            locale = 'en';
            break;
        case 3:
            locale = 'fr';
            break;
        case 4:
            locale = 'ru';
            break;
        case 5:
            locale = 'de';
            break;
        default:
            locale = 'es';
            break;
    }
    return locale;
}