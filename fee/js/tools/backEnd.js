var xmlLanguage = "";
// genericAjaxCall is used to centralize all ajax calls
function genericAjaxCall(method, url, contentType, dataType, data,
        successCallBack, errorCallBack, loading) {
    /*nbreWSCallsInProgress++;
     if (loading == null || loading == true)
     $('#loadingEds').show();*/
    $.ajax({
        type: method,
        url: url,
        contentType: contentType,
        dataType: dataType,
        data: data,
        cache: cacheAjax,
        success: successCallBack,
        error: errorCallBack,
        statusCode: {
//            401: exceptionUnAutorized,
//            403: exceptionFrobidden
        }
    }).complete(function () {
        /*nbreWSCallsInProgress--;
         if (nbreWSCallsInProgress < 1) {
         $('#loadingEds').hide();
         nbreWSCallsInProgress = 0;
         }*/
    });
}

function exceptionUnAutorized() {
    var cookies = document.cookie.split(";");
    for (var i = 0; i < cookies.length; i++) {
        var equals = cookies[i].indexOf("=");
        var name = equals > -1 ? cookies[i].substr(0, equals) : cookies[i];
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
    window.location.replace("login.html?idLang=" + getIdLangParam());
}

function exceptionFrobidden() {
    hidePopups();
    $(".backgroundPopup, .popupCantDoOperation").fadeIn();
    emptyEventFields();
}

function genericError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}

function htmlEncode(value) {
    if (value) {
        return $('<div />').text(value).html();
    }
    return value;
}
function htmlDecode(value) {
    if (value) {
        return $('<div />').text(value).text();
    }
    return value;
}
function nbreConnectedSuccess(xml) {
    var nbreConnected = $(xml).find('nbreConnected').text();
    $('.forumVisitorNbreVal').html(nbreConnected);
}
function nbreConnectedOnStandsSuccess(xml) {
    var nbreConnected = $(xml).find('nbreConnected').text();
    $('.standsVisitorNbreVal').html(nbreConnected);
}
function nbreConnectedByStandSuccess(xml) {
    var nbreConnected = $(xml).find('nbreConnected').text();
    $('.standVisitorNbreVal').html(nbreConnected);
}
function updateVisitorsInfo() {
    genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'stand/nbreConnected', 'application/xml', 'xml', '', nbreConnectedSuccess, '', false);
// genericAjaxCall('GET', staticVars["urlBackEndCandidate"] +
// 'stand/nbreConnectedOnStands', 'application/xml', 'xml', '',
// nbreConnectedOnStandsSuccess, '');
    if ($.cookie('standId') != null && $.cookie('standId') != "")
        genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'stand/nbreConnectedByStand?standId=' + $.cookie('standId'), 'application/xml', 'xml', '', nbreConnectedByStandSuccess, '', false);
}
function sessionExtenderError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}
function sessionExtenderSuccess() {
    updateVisitorsInfo();
}
function sessionExtender() {
    //console.log('sessionExtender');
    genericAjaxCall('GET', staticVars["urlBackEnd"] + 'chat/sessionExtender', 'application/xml', 'xml', '', updateVisitorsInfo, sessionExtenderError, false);
}
function showLanguagePopup() {
    $('#popupLanguageChange').modal();
}

// test if session expired
function isSessionExpired(response) {
    if (compareToLowerString(response, "nosession") !== -1) {
        window.location.replace("login.html?idLang=" + getIdLangParam());
    }
}
// test if email already exist
function emailAlreadyExit(response) {
    if (compareToLowerString(response, "EMAIL_UNIQUE") !== -1) {
        //console.log("emailExist");
        $('#adrAlrExistLabel').show();
    }
}
function getCountriesSuccess(xml) {
    listCountriesCache = [];
    $(xml).find("countryDTO").each(
                    function() {
                        var country=new $.Country($(this));
                        listCountriesCache[country.getCountryId()] = country;
                    });
    listCountriesCache.sort(function(a, b) {
        if(a.getCountryName() > b.getCountryName()) return 1;
        else if(a.getCountryName() < b.getCountryName()) return -1;
        else return 0;
    });
}
function getCountriesError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}
function getCountries() {
    if(listRegionsCache == null){
        var langId = getIdLangParam();
        if(langId != 2) 
            langId = 3; 
        genericAjaxCall('GET', staticVars.urlBackEnd + 'country?idLang='+langId, 'application/xml',
                'xml', '', getCountriesSuccess, getCountriesError);
    }
}
function wait() {
    $.blockUI({
        css: {
            border: 'none',
            backgroundColor: '',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: 0.5
        },
        message: '',
        overlayCSS: {
            backgroundColor: ''
        }
    });
}

function goToTop() {
    $('html, body').animate({
        scrollTop: 0
    }, 'slow');
}

function showUpdateSuccess() {
    $('html, body').animate({
        scrollTop: 0
    }, 'slow');
    $('#loadingEds').hide();

    $('#dataSent').fadeIn(function () {
        $('#dataSent').delay(3000).fadeOut();
    });

}

function showPopupMessageSent() {
    hidePopups();
    $('#messageSent').fadeIn(function () {
        $('#messageSent').delay(1200).fadeOut();
    });
}

function infoMessage(idComponent) {
    $('html, body').animate({
        scrollTop: 0
    }, 'slow');
    $(idComponent).fadeIn(function () {
        $(idComponent).delay(3000).fadeOut();
    });
}

function showPopupMessage() {
    $('.backgroundPopup').show();
    $('.popupMessage').show();
}
function removeSpaces(string) {
    return string.split(' ').join('');
}

function hidePopups() {
    $('.backgroundPopup,.eds-dialog-container').fadeOut();
}

// backend page scripts

// login page scripts

var idLang = 1;

function parseXmlTranslate(xml) {
    xmlLanguage = xml;
    // $(xml).find("component").each(function() {
    // var itemId = $(this).attr('id');
    // var itemValue = $(this).attr('value');
    // $("#" + itemId).text(itemValue);
    // $("." + itemId).text(itemValue);
    // });
    // $('#loadingEds').hide();
    traductAllLabel();
    traduct();
}

function traduct() {
    $(xmlLanguage).find("component").each(function () {
        var itemId = $(this).attr('id');
        var itemValue = $(this).attr('value');
        $("#" + itemId).text(itemValue);
        $("." + itemId).text(itemValue);
    });
    $('#loadingEds').hide();
}

function traductLabel(label) {
    return $(xmlLanguage).find("[id=" + label + "]").attr('value');
}

function traductAllLabel() {
    VIEWLABEL = traductLabel("viewLabel");
    EDITLABEL = traductLabel("editLabel");
    DELETELABEL = traductLabel("deleteLabel");
    SAVELABEL = traductLabel("saveLabel");
    SKYPEINVITATIONLABEL = traductLabel("skypeInvitationLabel");
    APPROVELABEL = traductLabel("approveLabel");
    DISAPPROVELABEL = traductLabel("desapproveLabel");
    CANCELLABEL = traductLabel("cancelLabel");
    CONNECTTOCHATLABEL = traductLabel("connectToChatLabel");
    DISCONNECTFROMCHATLABEL = traductLabel("toDisconnectFromChatLabel");
    PHOTOLABEL = traductLabel("photoLabel");
    FIRSTANDLASTNAMELABEL = traductLabel("firstAndLastNameLabel");
    JOBTITLELABEL = traductLabel("jobTitleLabel");
    TESTIMONYLABEL = traductLabel("testimonyLabel");
    ACTIONSLABEL = traductLabel("actionsLabel");
    CHARGEMENTLABEL = traductLabel("loadingLabel");
    CHARGEMENT = CHARGEMENTLABEL;
    RESPONSELABEL = traductLabel("responseLabel");
    QUESTIONLABEL = traductLabel("questionLabel");
    ADDLABEL = traductLabel("addLabel");
    SAVECHANGESLABEL = traductLabel("saveChangesLabel");
    LEFTCARACTERES = traductLabel("leftCarachters");
    REMOVELABEL = traductLabel("removeLabel");
    REFERENCELABEL = traductLabel("referenceLabel");
    TITLELABEL = traductLabel("titleLabel");
    DESCRIPTIONLABEL = traductLabel("descriptionLabel");
    SKILLSLABEL = traductLabel("skillsLabel");
    REQUIREDLABEL = traductLabel("requiredLabel");
    DATEINVALIDLABEL = traductLabel("invalidDateLabel");
    LOWERDATELABEL = traductLabel("lowerDateLabel");
    OKLABEL = traductLabel("okLabel");
    CONTENTLABEL = traductLabel("contentLabel");
    PUBLICCHATLABEL = traductLabel("publicChatLabel");
    PRIVATECHATLABEL = traductLabel("privateChatLabel");
    LANGAUGETAGLOCALELABEL = traductLabel("languageTagLocaleLabel");
    LANGUAGECURRENTLABEL = traductLabel("languageCurrentLabel"); 
    LANGUAGEDATEPICKERFORMATLABEL = traductLabel("languageDatePickerFormatLabel");
    OPENLABEL = traductLabel("openLabel");
    SEEPROFILELABEL = traductLabel("seeProfileLabel");
    SEECVLABEL = traductLabel("seeCVLabel");
    SEELETTERLABEL = traductLabel("seeLetterLabel");
    NEWLABEL = traductLabel("newLabel");
    ATLEASTONELABEL = traductLabel("atLeastOneLabel");

    AGELABEL = traductLabel("ageLabel");
    TOTALLABEL = traductLabel("totalLabel");
    CANTONLABEL = traductLabel("cantonLabel");
    STUDYLEVELLABEL = traductLabel("studyLevelLabel");
    FUNCTIONLABEL = traductLabel("functionLabel");
    EXPERIENCEYEARSLABEL = traductLabel("experienceYearsLabel");
    ACTIVITYSECTORLABEL = traductLabel("activitySectorLabel");
    USERLABEL = traductLabel("userLabel");
    VISITSTOTALLABEL = traductLabel("visitsTotalLabel");
    TYPE_LABEL = traductLabel("typeLabel");
    FIRSTVISITLABEL = traductLabel("firstVisitLabel");
    LASTVISITLABEL = traductLabel("lastVisitLabel");
    AVGVISITSLABEL = traductLabel("avgVisitsLabel");
    VISITSLABEL = traductLabel("visitsLabel");
    LANGUAGELABEL = traductLabel("languageLabel");
    LANGUAGESLABEL = traductLabel("languagesLabel");
    COUNTRYTERRITORYLABEL = traductLabel("countryTerritoryLabel");

    CITYLABEL = traductLabel("cityLabel");
    CITIESLABEL = traductLabel("citiesLabel");
    ATTACH_FILES = traductLabel("attachFilesLabel");
    BROWSERLABEL = traductLabel("browserLabel");
    BROWSERSLABEL = traductLabel("browsersLabel");
    OPERATINGSYSTEMSLABEL = traductLabel("operatingSystemsLabel");
    OPERATINGSYSTEMLABEL = traductLabel("operatingSystemLabel");
    DAYLABEL = traductLabel("dayLabel");
    HOURLABEL = traductLabel("hourLabel");
    BOUNCESLABEL = traductLabel("bouncesLabel");
    NEWVISITSLABEL = traductLabel("newVisitsLabel");
    AVGTIMEONSITELABEL = traductLabel("avgTimeOnSiteLabel");
    TRAFFICSOURCESLABEL = traductLabel("trafficSourcesLabel");
    PHOTOSLABEL = traductLabel("photosLabel");
    ENTERPRISELABEL = traductLabel("enterpriseLabel");
    NUMBERVIEWSLABEL = traductLabel("numberViewsLabel");
    DOCUMENTSLABEL = traductLabel("documentsLabel");
    VIDEOSLABEL = traductLabel("videosLabel");
    POSTERSLABEL = traductLabel("postersLabel");
    ADVICESLABEL = traductLabel("advicesLabel");
    JOBOFFERSLABEL = traductLabel("jobOffersLabel");
    SURVEYLABEL = traductLabel("surveyLabel");
    EVENTLABEL = traductLabel("eventLabel");
    ERRORACCOUNTLABEL = traductLabel("errorAccountLabel");
    ERRORCONNECTIONLABEL = traductLabel("errorConnectionLabel");
    NEWMESSAGELABEL = traductLabel("newMessageLabel");
   
    DATELABEL = traductLabel("dateLabel");
    AREALABEL = traductLabel("areaLabel");
    AREAVIEWSLABEL = traductLabel("areaViewsLabel");
    UNIQUEAREAVIEWSLABEL = traductLabel("uniqueAreaViewsLabel");
    VISITORSLABEL = traductLabel("visitorsLabel");
    VISITORTYPELABEL = traductLabel("visitorTypeLabel");
    AVGTIMEAREAVIEWSLABEL = traductLabel("avgTimeAreaViewsLabel");
    COMPANY_PROFILE_LABEL = traductLabel("companyProfileLabel");

    DEMOGRAPHICSDATALABEL = traductLabel("demographicsDataLabel");
    OVERVIEWLABEL = traductLabel("overviewLabel");
    PROFILLABEL = traductLabel("profilLabel");
    VISITSBYFUNCTIONLABEL = traductLabel("visitsByFunctionLabel");
    NEWVISITORSVSRETURNINGVISITORSLABEL = traductLabel("newVisitorsVSReturningVisitorsLabel");
    BOOTHVISITORSLABEL = traductLabel("boothVisitorsLabel");
    WEBSITESLABEL = traductLabel("webSitesLabel");
    TESTIMONIESLABEL = traductLabel("testimoniesLabel");
    APPLYLABEL = traductLabel("applyLabel");
    SUBMITCVLABEL = traductLabel("submitCvLabel");
    SENDMESSAGELABEL = traductLabel("sendMessageLabel");
    CVSUBMITTEDLABEL = traductLabel("CVSubmittedLabel");
    DATEOFAPPLICATIONLABEL = traductLabel("dateOfApplicationLabel");
    SENDLABEL = traductLabel("sendLabel");
    FIRSTNAMELABEL = traductLabel("firstNameLabel");
    LASTNAMELABEL = traductLabel("lastNameLabel");
    ADDTOFAVORITELABEL = traductLabel("addToFavoriteLabel");
    DELETEFROMFAVORITELABEL = traductLabel("deleteFromFavoriteLabel");
    VIEWPROFILELABEL = traductLabel("viewProfileLabel");
    YOUDONTHAVERIGHTTODOMODIF = traductLabel("dontHaveRightTODoThisModifLabel");
    ACCEPT_PRODUCT_RESOURCE = traductLabel("acceptProductResourceLabel");
    CHOOSELABEL = traductLabel("chooseLabel");
    VIEWCVLABEL = traductLabel("viewCVLabel");
    VIEWLETTERLABEL = traductLabel("viewLetterLabel");
    TRADUCTLABEL = traductLabel("traductLabel");
    TRADUCTIONLABEL = traductLabel("translationLabel");
    INBOXLABEL = traductLabel("inboxLabel");
    RETURNLABEL = traductLabel("returnLabel");
    URLLABEL = traductLabel("urlLabel");
    SURVEYSLABEL = traductLabel("surveysLabel");
    CVSSUBMITEDNBRLABEL = traductLabel("cvsSubmitedNbrLabel");
    QUESTIONSLABEL = traductLabel("questionsLabel");
    ALLLABEL = traductLabel("allLabel");
    REPLACELANGUAGELABEL = traductLabel("replaceLanguageLabel");
    LANGUAGELEVELLABEL = traductLabel("languageLevelLabel");
    NUMBEROFCHOICES = traductLabel("numberOfChoicesLabel");
    SEARCHTHECVLABEL = traductLabel("searchtheCVLabel");
    UNSOLICITEDAPPLICATIONLABEL = traductLabel("unsolicitedApplicationLabel");
    NEWLABEL = traductLabel("newLabel");
    UPDATEDLABEL = traductLabel("updatedLabel");
    NOTUPDATEDLABEL = traductLabel("notUpdatedLabel");
    STATELABEL = traductLabel("stateLabel");
    ARCHIVETOOLTIPLABEL = traductLabel("archiveTooltipLabel");
    MOVETOOLTIPLABEL = traductLabel("moveTooltipLabel");
    DELETETOOLTIPLABEL = traductLabel("deleteTooltipLabel");
    SHAREFRIENDLABEL = traductLabel("shareWithFriendLabel");
    LOGO_LABEL = traductLabel("logoLabel");
    MODIFY_PRODUCT = traductLabel("modifyProduct");
    TRANSLATE_PRODUCT = traductLabel("translateProduct");
    DOCUMENT_LABEL = traductLabel("documentLabel");
    VIDEO_LABEL = traductLabel("videoLabel");
    IMAGE_LABEL = traductLabel("imageLabel");
    VIEW_LINKS_LABEL = traductLabel("viewLinksLabel");
    EXCHANGE_VISIT_CARD_LABEL = traductLabel("exchangeVisitCardLabel");
    DATE_FORMAT = traductLabel("dateFormatLabel");
    affectTraduct();
}
function affectTraduct() {
  $('.archiveTooltip').attr('title', ARCHIVETOOLTIPLABEL);
  $('.moveTooltip').attr('title', MOVETOOLTIPLABEL);
  $('.deleteTooltip').attr('title', DELETETOOLTIPLABEL);
}

function translateError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}

function translate(pathFile) {
    translateUrl = pathFile;
    genericAjaxCall('GET', pathFile, 'application/xml', 'xml', '',
            function (xml) {
                parseXmlTranslate(xml);
            }, translateError);
}

function logout() {
    $.cookie("cusr", null);
    $.cookie("cpw", null);

    genericAjaxCall('POST', staticVars["urlBackEnd"]
            + 'enterprisep/logout',
            'application/xml', 'xml', '', function (xml) {
                window.location.href = "login.html?idLang=" + getIdLangParam();
            }, '');

}

function deleteMediaError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}

function deleteMedia(fileId) {
    genericAjaxCall('GET', staticVars.urlBackEnd + 'allfiles/deleteMedia/'
            + fileId, 'application/xml', 'xml', '', '', deleteMediaError);
}

function getLanguagePath(idLang) {
    // $.cookie("idLang", idLang);
    genericAjaxCall(
            'GET',
            staticVars.urlBackEnd + 'languaget/translate/?idLang=' + idLang,
            'application/xml',
            'xml',
            '',
            function (xml) {
                translate($(xml).find('path').text());
                if (currentPage == PageEnum.CHAT) {
                    initChatPage();
                } else {
                    /*if (document.getElementById("enterpriseProfileForm") !== null) {
                        //getStandInfos();
                    } else if (document.getElementById("profileSearchedForm") !== null) {
                        //getProfileSearched();
                    }
                    checkLoginWSCallsInProgress();*/
                }
            }, '');
}

function login() {
    var loginUser = $('#recruiterLoginForm input[id="loginInput"]').val(), passwordUser = $(
            '#recruiterLoginForm input[id="passwordInput"]').val();
    if ((loginUser != "") && (passwordUser != "")) {
        var passwordCrypted = $.sha1(passwordUser), enterprisePxml = "<enterprisep><login>"
                + htmlEncode(loginUser)
                + "</login><password>"
                + passwordCrypted + "</password></enterprisep>";
        genericAjaxCall('POST', staticVars.urlBackEnd + 'enterprisep/login',
                'application/xml', 'xml', enterprisePxml, function (xml) {
                    var status = $(xml).find('status').text();
                    if (status === "success") {
                        $.cookie("enterpriseID", $(xml).find('enterpriseID')
                                .text());
                        $.cookie("groupId", $(xml).find('groupId').text());
                        $.cookie("enterprisepId", $(xml).find('enterprisepId')
                                .text());
                        standId = $(xml).find('standId').text();
                        $.cookie("standId", standId);
                        var token = $(xml).find('token').text();
                        if (token == null || token == "null")
                            token = "";
                        $.cookie("token", token);
                        //console.log($(xml).find('isAdmin'));
                        if ($(xml).find('isAdmin').text() == "true")
                            window.location.replace("backend.html?idLang=" + getIdLangParam());
                        else
                            window.location.replace("chats.html?idLang=" + getIdLangParam());
                    } else {
                        $('#errorLogin').show();
                    }
                }, function () {
            $('#errorLogin').show();
        });
    } else {
        $('#errorLogin').show();
    }
}

function loginFromCookie() {
    var loginUser = $.cookie("cusr");
    var passwordCrypted = $.cookie("cpw");
    var enterprisePxml = "<enterprisep><login>" + htmlEncode(loginUser)
            + "</login><password>" + passwordCrypted
            + "</password></enterprisep>";
    genericAjaxCall('POST', staticVars.urlBackEnd + 'enterprisep/login',
            'application/xml', 'xml', enterprisePxml, function (xml) {
                var status = $(xml).find('status').text();
                if (status == "success") {
                    $.cookie("enterpriseID", $(xml).find('enterpriseID')
                            .text());
                    $.cookie("enterprisepId", $(xml).find('enterprisepId')
                            .text());
                    $.cookie("standId", $(xml).find('standId').text());
                    $.cookie("token", $(xml).find('token').text());
                    standId = $(xml).find('standId').text();
                    window.location.href = "backend.html?idLang=" + getIdLangParam();
                } else {
                    window.location.href = "login.html?idLang=" + getIdLangParam();
                }
            }, '');

}

/**
 *
 * Chat page scripts
 *
 * **/

function isSettingsFilled() {
    if ($('#recruiterFirstName').val() == ""
            || $('#recruiterLastName').val() == ""
            || $('#recruiterJobTitle').val() == ""
            || $('#recruiterPseudoSkype').val() == ""
            || $('#recruiterEmail').val() == "") {
        $('#chatSettings li').removeClass('active');
        $('#settingsLi').addClass('active');

        $('.chatTabs').css('visibility', 'hidden');
        $('.chatTabs').css('height', '0px');
        $('.chatTabs').css('display', 'none');
        $('#settingsTab').css('visibility', 'visible');
        $('#settingsTab').css('height', '100%');
        $('#settingsTab').css('display', 'block');
        return false;
    } else {
        $('#chatSettings li').removeClass('active');
        $('#chatLi').addClass('active');
        $('.chatTabs').css('visibility', 'hidden');
        $('.chatTabs').css('height', '0px');
        $('.chatTabs').css('display', 'none');
        $('#chatTab').css('visibility', 'visible');
        $('#chatTab').css('height', '100%');
        $('#chatTab').css('display', 'block');
        return true;
    }
}

function getRecruiterLanguagesError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}

function getRecruiterLanguages() {
    genericAjaxCall('GET', staticVars.urlBackEnd
            + 'enterprisep/spokenLanguages', 'application/xml', 'xml', '',
            function (xml) {
                parseRecruiterLanguagesXml(xml);
            }, getRecruiterLanguagesError);

}

function parseRecruiterLanguagesXml(xml) {
    $(xml).find('speakedLanguage').each(
            function () {
                var idLang = $(this).find('idlangage').text();
                var langName = $(this).find('name').text();
                var interestdata = {
                    idLanguage: replaceSpecialChars(idLang),
                    languageName: replaceSpecialChars(langName)
                };
                $("#tempLanguagesRecruiter").tmpl(interestdata).appendTo(
                        "#listLanguages");
                if ($(this).find('speaked').text() == "true") {
                    $('#' + idLang).attr('checked', 'checked');
                }
            });
}

function getRecruiterInfosChatError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}

function getRecruiterInfosChat() {
    genericAjaxCall('GET', staticVars.urlBackEnd + 'enterprisep/getInfos',
            'application/xml', 'xml', '', parseRecruiterInfosXml, getRecruiterInfosChatError);
}

function parseRecruiterInfosXml(xml) {
    getRecruiterInfo = xml;
    if ($(xml).find('isAdmin').text() == "false")
        window.location.replace("chats.html?idLang=" + getIdLangParam());
    $('#recruiterFirstName').val(
            $(xml).find('userProfileDTO').find('firstName').text());
    $('#recruiterLastName').val(
            $(xml).find('userProfileDTO').find('secondName').text());
    $('#recruiterJobTitle').val($(xml).find('jobTitle').text());
    $('#recruiterEmail').val($(xml).find('email').text());
    $('#recruiterPseudoSkype').val($(xml).find('pseudoSkype').text());
    if ($(xml).find('userProfileDTO').find('avatar').text() != "") {
        enterprisepPhoto = $(xml).find('userProfileDTO').find('avatar')
                .text();
    }
    var enterpriseP = new $.EnterpriseP(xml);
    $('.recruiterPicture').attr('src', enterprisepPhoto);
    $('#recruiter_full_name').text($(xml).find('firstName').text()+' '+$(xml).find('secondName').text());
    /*currentEnterpriseP = enterpriseP;
    var userProfile = enterpriseP.getUserProfile();
    if(!enterpriseP.getIsAdmin()) 
        window.location.replace("chats.html?idLang="+getIdLangParam());
    $('#recruiterFirstName').val(userProfile.getFirstName());
    $('#recruiterLastName').val(userProfile.getSecondName());
    $('#recruiterJobTitle').val(enterpriseP.getJobTitle());
    if (userProfile.getAvatar() != "") {
        enterprisepPhoto = userProfile.getAvatar();
    }
    $('#recruiterPicture').attr('src', enterprisepPhoto);*/
    //isSettingsFilled();
    //hideTabChat();
}

function sendRecruiterInfosChatError(xhr, status, error) {
    emailAlreadyExit(xhr.responseText);
    isSessionExpired(xhr.responseText);
}

function sendRecruiterInfosChat() {
    var recruiterXml = "<enterprisep><jobTitle>" + htmlEncode($('#recruiterJobTitle').val())
            + "</jobTitle><email>" + htmlEncode($('#recruiterEmail').val())
            + "</email><userProfileDTO><firstName>" + htmlEncode($('#recruiterFirstName').val())
            + "</firstName><secondName>" + htmlEncode($('#recruiterLastName').val()) + "</secondName><pseudoSkype>"
            + htmlEncode($('#recruiterPseudoSkype').val()) + "</pseudoSkype><avatar>" + enterprisepPhoto
            + "</avatar></userProfileDTO></enterprisep>";
    genericAjaxCall('POST', staticVars.urlBackEnd + 'enterprisep/update/chat',
            'application/xml', 'xml', recruiterXml, function (xml) {
                showUpdateSuccess();
                reconnectToChat();
            }, sendRecruiterInfosChatError);
}

function validateAccountForm() {
    $("#accountForm").validate({
        rules: {
            recruiterEmail: {
                required: true,
                email: true
            },
        }
    });
}

/**
 *
 * Inbox page scripts
 *
 **/

var unreadMessagesNumber = 0;
var tabMessagesId = [];
var idFolder = 1;
var idFolderToDelete;
var receiverLogin;

function updateSelectedTab(that) {
    $(".btn_indox").removeClass("selected");
    $(that).addClass("selected");
}

function getLanguageName(languageId){
    if(languageId!="" && languageId!=null && listLangsAbrEnum[languageId] != null) return listLangsAbrEnum[languageId];
    else return "";
}

function fillReceivedConversations() {
    $("#returnDiv").hide();
    $("#listMessages").show();
    $("#listConversationDetails").hide();
    $("#replyDiv").hide();
    $("#listMessages").empty();
    getReceivedConversations(idFolder);
}

function parseReceivedConversationXml(xml) {
    var testEmpty = $(xml).find('message').children().length;
    if (testEmpty == 0) {
        $('#noMessage').show();
        $('#noMessageSent').hide();
        $('#messageOptions').css('visibility', 'hidden');
    } else {
        $('#noMessageSent').hide();
        $('#noMessage').hide();
        $('#messageOptions').css('visibility', 'visible');
        listConversationsCache = new Array();
        unreadMessagesNumber = 0;
        $(xml).find('conversations').each(function () {
            listConversationsCache.push(new $.Conversation($(this)));
        });

        if (notificationConversationIndex != -1 && conversationIndex[notificationConversationIndex])
            conversationIndex = notificationConversationIndex;
        notificationConversationIndex = -1;

        $.each(listConversationsCache, function (index, value) {
            //console.log('id: '+value.idConversation);
            var backgroundColor;
            var bold;
            var colorLink;
            var readClass;
            if (value.unread.indexOf("true") != -1) {
                unreadMessagesNumber++;
                bold = "bold";
                colorLink = "black";
                backgroundColor = "#ffffff";
                readClass = "unreadCheckbox";
            } else {
                bold = "normal";
                colorLink = "gray";
                backgroundColor = "#f0f0f0";
                readClass = "readCheckbox";
            }

            var interestdata = {
                senderTemp: replaceSpecialChars(value.candidateName),
                subjectTemp: replaceSpecialChars(value.object),
                dateTemp: replaceSpecialChars(value.lastUpdate),
                colorStatus: bold,
                colorLink: colorLink,
                backgroundColor: backgroundColor,
                reverseWay: true,
                readClass: readClass,
                indexConversation: replaceSpecialChars(index),
            };
            $("#tempMessage").tmpl(interestdata).appendTo("#listMessages");

            if (conversationIndex != -1) {
                //console.log('CLICK CONV');
                displayConversationDetails(conversationIndex, true);
                conversationIndex = -1;
            }
        });
        if (unreadMessagesNumber == 0)
            $('.messagesReceived').text(INBOXLABEL);
        else
            $('.messagesReceived').text(INBOXLABEL + ' (' + unreadMessagesNumber + ')');

        $(".checkConversation").click(function () {
            tabMessagesId = [];
            $('.checkConversation').each(function () {
                if ($(this).is(':checked'))
                    tabMessagesId.push($(this).attr('id'));
            });
            if (tabMessagesId.length != 0) {
                $("#actionDiv").show();
            } else {
                tabMessagesId = [];
                $("#actionDiv").hide();
            }
        });
    }
}

function getConversationsError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}

function getReceivedConversations(messageFolderId) {
    if (messageFolderId == null)
        messageFolderId = 1;
    genericAjaxCall('GET', staticVars.urlBackEnd + 'conversation/stand/received?messageFolderId=' + messageFolderId,
            'application/xml', 'xml', '', parseReceivedConversationXml, getConversationsError);
}

function fillSentConversations() {
    $("#listMessages").show();
    $("#listConversationDetails").hide();
    $("#replyDiv").hide();
    $("#listMessages").empty();
    getSentConversations();
}

function parseSentConversationXml(xml) {
    var testEmpty = $(xml).find('message').children().length;
    if (testEmpty == 0) {
        $('#noMessageSent').show();
        $('#noMessage').hide();
        $('#messageOptions').css('visibility', 'hidden');
    } else {
        $('#noMessageSent').hide();
        $('#noMessage').hide();
        $('#messageOptions').css('visibility', 'visible');
        conversations = new Array();
        $(xml).find('conversations').each(function () {
            conversations.push(new $.Conversation($(this)));
        });

        $.each(conversations, function (index, value) {
            var bold = "normal";
            var colorLink = "gray";
            var backgroundColor = "#f0f0f0";
            ;
            var interestdata = {
                senderTemp: replaceSpecialChars(value.receiver.prenom) + ' ' + replaceSpecialChars(value.receiver.nom),
                subjectTemp: replaceSpecialChars(value.object),
                dateTemp: replaceSpecialChars(value.lastUpdate),
                colorStatus: bold,
                colorLink: colorLink,
                backgroundColor: backgroundColor,
                reverseWay: false,
                readClass: "readCheckbox",
                indexConversation: replaceSpecialChars(index),
            };
            $("#tempMessage").tmpl(interestdata).appendTo("#listMessages");
        });

        $(".checkConversation").click(function () {
            tabMessagesId = [];
            $('.checkConversation').each(function () {
                if ($(this).is(':checked'))
                    tabMessagesId.push($(this).attr('id'));
            });
            if (tabMessagesId.length != 0) {
                $("#actionDiv").show();
            } else {
                tabMessagesId = [];
                $("#actionDiv").hide();
            }
        });
    }
}

function getSentConversations() {
    genericAjaxCall('GET', staticVars.urlBackEnd + 'conversation/stand/sent',
            'application/xml', 'xml', '', parseSentConversationXml, getConversationsError);
}

function fillArchivedConversations() {
    $("#listMessages").show();
    $("#listConversationDetails").hide();
    $("#replyDiv").hide();
    $("#listMessages").empty();
    getArchivedConversation();
}

function parseArchivedConversationXml(xml) {
    var testEmpty = $(xml).find('conversations').length;
    if (testEmpty == 0) {
        $('#noMessage').show();
        $('#noMessageSent').hide();
        $('#messageOptions').css('visibility', 'hidden');
    } else {
        $('#noMessageSent').hide();
        $('#noMessage').hide();
        $('#messageOptions').css('visibility', 'visible');
        listConversationsCache = new Array();
        $(xml).find('conversations').each(function () {
            listConversationsCache.push(new $.Conversation($(this)));
        });
        $.each(listConversationsCache, function (index, value) {
            var bold = "normal";
            var colorLink = "gray";
            var backgroundColor = "#f0f0f0";
            ;
            var interestdata = {
                senderTemp: replaceSpecialChars(value.receiver.prenom) + ' ' + replaceSpecialChars(value.receiver.nom),
                subjectTemp: replaceSpecialChars(value.object),
                dateTemp: replaceSpecialChars(value.lastUpdate),
                colorStatus: bold,
                colorLink: colorLink,
                backgroundColor: backgroundColor,
                reverseWay: false,
                readClass: "readCheckbox",
                indexConversation: replaceSpecialChars(index),
            };
            $("#tempMessage").tmpl(interestdata).appendTo("#listMessages");
        });

        $(".checkConversation").click(function () {
            tabMessagesId = [];
            $('.checkConversation').each(function () {
                if ($(this).is(':checked'))
                    tabMessagesId.push($(this).attr('id'));
            });
            if (tabMessagesId.length != 0) {
                $("#actionDiv").show();
            } else {
                tabMessagesId = [];
                $("#actionDiv").hide();
            }
        });
    }
}

function getArchivedConversations() {
    genericAjaxCall('GET', staticVars.urlBackEnd + 'conversation/stand/archived',
            'application/xml', 'xml', '', parseArchivedConversationXml, getConversationsError);
}

function genericAjaxCallMarkAsRead(idConversation) {
    genericAjaxCall('POST', staticVars.urlBackEnd + 'conversation/markAsRead?idConversation='
            + idConversation, 'application/xml', 'xml', '', '', '');
}

function displayConversationDetails(indexConversation, reverseWay) {
    if ($('#divMessage' + indexConversation).find('input').hasClass('unreadCheckbox')) {
        unreadMessagesNumber--;
        if (unreadMessagesNumber == 0)
            $('.messagesReceived').text(INBOXLABEL);
        else
            $('.messagesReceived').text(INBOXLABEL + ' (' + unreadMessagesNumber + ')');
    }

    indexCurrentConversation = indexConversation;
    $('#listConversationDetails').empty();

    var conversation = conversations[indexConversation];
    genericAjaxCallMarkAsRead(conversation.idConversation);

    if (reverseWay) {
        sender = conversation.receiver;
        receiver = conversation.sender;
    } else {
        sender = conversation.sender;
        receiver = conversation.receiver;
    }

    $.each(conversation.messages, function (index, value) {
        var interestdata = {
            indexConversation: replaceSpecialChars(indexConversation),
            loginSenderTemp: replaceSpecialChars(value.sender.prenom) + " " + replaceSpecialChars(value.sender.nom),
            contentTemp: replaceSpecialChars(value.content),
            dateTemp: replaceSpecialChars(value.messageDate),
            // idConversation : $(this).find('idMessage').text(),
        };
        $("#tempMessageDetails").tmpl(interestdata).appendTo("#listConversationDetails");
    });
    $('#listMessages').hide();
    $('#listConversationDetails').show();
    $('#replyDiv').show();
    $("#returnDiv").show();
}

function sendReplyError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}

function sendReply() {
    var xmlReply = "<message><conversation>" + indexCurrentConversation
            + "</conversation><sender><userProfileId>" + sender.userProfileId + "</userProfileId></sender><receiver><userProfileId>" + receiver.userProfileId + "</userProfileId></receiver><content>"
            + htmlEncode($("#replyContent").val())
            + "</content></message>";
    genericAjaxCall('POST', staticVars.urlBackEnd + 'conversation/reply',
            'application/xml', 'xml', xmlReply, function (xml) {
                var interestdata = {
                    loginSenderTemp: replaceSpecialChars($(xml).find('enterpriseName').text()),
                    contentTemp: replaceSpecialChars($("#replyContent").val()),
                    dateTemp: new Date().toLocaleTimeString(),
                    // idConversation : $(this).find('idMessage').text(),
                };
                $("#tempMessageDetails").tmpl(interestdata).appendTo(
                        "#listConversationDetails");
                $("#replyContent").val("");
            }, sendReplyError);
}

function saveFolderError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}

function saveFolder() {
    var xmlMessageFolder = "<messageFolder><folderName>" + htmlEncode($('#folderName').val()) + "</folderName></messageFolder>";

    genericAjaxCall('POST', staticVars.urlBackEnd + 'messageFolder/createFolder', 'application/xml', 'xml', xmlMessageFolder,
            function (xml) {
                var folderId = $(xml).find('idMessageFolder').text();
                var folderName = $("#folderName").val();

                $("<li style='margin-left:5%;'><a id="
                        + folderId
                        + " href='javascript:void(0)' class='btn_indox'>"
                        + htmlEncode(formattingLongWords(folderName, 15))
                        + " <span class="
                        + folderId
                        + "></span><strong style='z-index:200;float:right;color:white;font-size:14px;width:20px;' class='test'>x</strong></a></li>")
                        .insertAfter('.initialInbox');
                $('#' + folderId + ' .test').bind('click', function () {
                    idFolderToDelete = folderId;
                    $('.folderNameDelete').text(folderName);
                    $('.folderDeletePopup').show();
                    $('.backgroundPopup').show();
                });

                $('#' + folderId).bind('click', function () {
                    idFolder = folderId;
                    updateSelectedTab($(this));
                    fillReceivedConversations();
                });
                $('#changeFolderSelect').append("<option value=" + folderId + ">" + htmlEncode(folderName) + "</option>");
                $('.backgroundPopup').hide();
                $('.popupFolder').hide();
                $('#folderName').val("");
            }, saveFolderError);

}

function parseXmlMessageFolders(xml) {
    $(xml).find('messageFolder').each(function () {
        var folderId = $(this).find('idMessageFolder').text();
        var folderName = $(this).find('folderName').text();
        var numberMessages = $(this).find('numberMessages').text();
        $("<li style='margin-left:5%;'><a id="
                + folderId
                + " href='javascript:void(0)' class='btn_indox'>"
                + htmlEncode(formattingLongWords(folderName, 15))
                + " <span class="
                + folderId
                + "></span><strong style='z-index:200;float:right;color:white;font-size:14px;width:20px;' class='test'>x</strong></a></li>")
                .insertAfter('.initialInbox');
        $('#' + folderId + ' .test').bind('click', function () {
            idFolderToDelete = folderId;
            $('.folderNameDelete').text(folderName);
            $('.folderDeletePopup').show();
            $('.backgroundPopup').show();
        });
        if (numberMessages > 0) {
            $('#' + folderId).children('span').text("(" + numberMessages + ")");
            $('.' + folderName).css('font-weight', 'bold');
        } else {
            $('#' + folderId).children('span').text("");
            $('#' + idFolder).parent().css('font-weight', 'normal');
        }
        $('#' + folderId).bind('click', function () {
            idFolder = folderId;
            updateSelectedTab($(this));
            fillReceivedConversations();
        });
        $('#changeFolderSelect').append(
                "<option value=" + folderId + ">" + htmlEncode(folderName)
                + "</option>");
    });
}

function fillMessageFoldersError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}

function fillMessageFolders() {
    genericAjaxCall('GET', staticVars.urlBackEnd + 'messageFolder/folders',
            'application/xml', 'xml', '', function (xml) {
                parseXmlMessageFolders(xml);
            }, fillMessageFoldersError);
}

function changeFolder() {
    $.each(tabMessagesId, function (index, value) {
        var conversation = conversations[value];
        genericAjaxCall('POST', staticVars.urlBackEnd + 'conversation/changeFolder?idMessageFolder='
                + $("#changeFolderSelect").val() + '&idConversation='
                + conversation.idConversation, 'application/xml', 'xml', '', '', '');
    });
    $("input[type='checkbox']").prop("checked", false);
    tabMessagesId = [];
    $("#actionDiv").hide();
    $('.eds-dialog-container').hide();
    $('.backgroundPopup').hide();
    $('.inbox').trigger('click');
}

function archiveMessages() {
    //console.log('tabMessagesId: '+tabMessagesId);
    $.each(tabMessagesId, function (index, value) {
        var conversation = conversations[value];
        genericAjaxCall('POST', staticVars.urlBackEnd
                + 'conversation/archiveConversation?idConversation=' + conversation.idConversation,
                'application/xml', 'xml', '', '', '');
    });
    tabMessagesId = [];
    $("#actionDiv").hide();
    $('.inbox').trigger('click');
}

function deleteFolderError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}

function deleteFolder(idFolderDeleted) {
    $('#' + idFolderDeleted).parent('li').remove();
    $('.backgroundPopup').hide();
    $('.eds-dialog-container').hide();

    genericAjaxCall('POST', staticVars.urlBackEnd + 'messageFolder/deleteFolder?idFolder='
            + idFolderDeleted, 'application/xml', 'xml', '', function () {
                $.cookie("isInbox", "true");
                window.location.reload();
            }, deleteFolderError);
}

function markAsReadError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}

function markAsRead() {
    //console.log('tabMessagesId: '+tabMessagesId);
    $.each(tabMessagesId, function (index, value) {
        var conversation = conversations[value];
        genericAjaxCallMarkAsRead(conversation.idConversation);
    });
    $("input[type='checkbox']").prop("checked", false);
    tabMessagesId = [];
    $("#actionDiv").hide();
    $('.inbox').trigger('click');
}

function deleteConversations(listConversationId) {
    $.each(listConversationId, function (index, conversationId) {
        $("#divMessage" + value).remove();
        genericAjaxCall('POST', staticVars.urlBackEnd + 'conversation/stand/delete/byId?conversationId=' + conversationId,
                'application/xml', 'xml', '', '', '');
    });
    $('.inbox').trigger('click');
}

function validateFolderCreationForm() {
    $("#folderCreationForm").validate({});
}

/**
 *
 *  Agenda page script
 *
 **/
var idEventToChange;
var isUpdate = false;
var recurrentValue = "everyDay";
function displayEventOptions(idEvent) {
    idEventToChange = idEvent;
    $('.popupEventOptions').show();
    $('.backgroundPopup').show();
}

function parseXmlEventTypes(xml) {
    $(xml).find("eventType").each(
            function () {
                $('#eventType').append(
                        "<option value=" + $(this).find('idEeventType').text()
                        + ">" + $(this).find('eventTypeName').text()
                        + "</option>");
            });
}

function fillEventTypesError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}

function fillEventTypes() {

    genericAjaxCall('GET', staticVars.urlBackEnd + 'agenda/eventTypes',
            'application/xml', 'xml', '', function (xml) {
                parseXmlEventTypes(xml);
            }, fillEventTypesError);

}

function validateEventAgendaForm() {
    $("#eventAgendaForm").validate({
        rules: {
            endTime: {
                validTime: true
            },
        }
    });
}

function getEditedEventError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}

function getEditedEvent(idEvent) {

    genericAjaxCall('GET', staticVars.urlBackEnd
            + 'agenda/getEventById?idEvent=' + idEvent, 'application/xml',
            'xml', '', function (xml) {
                parseXmlEvent(xml);
            }, getEditedEventError);

}
function parseDateAgenda(date) {
    var yearUTC = date.substring(6, 10);
    var monthUTC = date.substring(3, 5);
    var dayUTC = date.substring(0, 2);
    var hoursUTC = date.substring(11, 13);
    var minutesUTC = date.substring(14, 16);
    var d = new Date();
    d.setUTCFullYear(yearUTC);
    d.setUTCMonth(monthUTC - 1);
    d.setUTCDate(dayUTC);
    d.setUTCHours(hoursUTC);
    d.setUTCMinutes(minutesUTC);
    return d;
}
function getYearMonthDayDateAgenda(d) {
    var year = d.getFullYear();
    var month = getStringElementDateNumber(d.getMonth() + 1);
    var day = getStringElementDateNumber(d.getDate());
    return [day, month, year].join('-');
}
function getTimeAgenda(d) {
    var hours = getStringElementDateNumber(d.getHours());
    var minutes = getStringElementDateNumber(d.getMinutes());
    return [hours, minutes].join(':');
}
function parseXmlEvent(xml) {
    var eventTypeId = $(xml).find('idEventType').text();
    var eventTitle = $(xml).find('eventTitle').text();
    var eventDescription = $(xml).find('eventDescription').text();
    var beginDate = $(xml).find('beginDate').text();
    var endDate = $(xml).find('endDate').text();
    $('#eventType option').filter(function () {
        return $(this).val() == eventTypeId;
    }).attr('selected', true);
    $('#eventTitle').val(eventTitle);
    $('#eventDescription').val(eventDescription);
    var beginD = parseDateAgenda(beginDate);
    var endD = parseDateAgenda(endDate);
    var bDateString = getYearMonthDayDateAgenda(beginD);
    var eDateString = getYearMonthDayDateAgenda(endD);
    var bTimeString = getTimeAgenda(beginD);
    var eTimeString = getTimeAgenda(endD);
    $('#dateEvent').val(bDateString);
    $('#dateEndEvent').val(eDateString);
    $('#beginTime').val(bTimeString);
    $('#endTime').val(eTimeString);
    $('.popupEvent,.backgroundPopup').show();
}

function deleteEventSuccess(xml) {
    $.cookie("isAgenda", "true");
    window.location.reload();
}
function deleteEventError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}

function deleteEvent(idEvent) {
    genericAjaxCall('POST', staticVars.urlBackEnd + 'agenda/delete?idEvent='
            + idEvent + '&forumId=' + forumId, 'application/xml', 'xml', '', deleteEventSuccess, deleteEventError);
}

function deleteContactError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}

function deleteContact(idContact) {
    $('#contact' + idContact).remove();
    genericAjaxCall('POST', staticVars.urlBackEnd + 'contact/delete?idContact='
            + idContact, 'application/xml', 'xml', '', function () {
                $('.popupDeleteContact,.backgroundPopup').hide();
            }, deleteContactError);
}

function emptyEventFields() {
    $('#eventAgendaForm input,#eventAgendaForm textarea').val("");
}

function sendEventInfoError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}
function getStringElementDateNumber(nb) {
    if (nb < 10) {
        return nb = '0' + nb;
    }
    return nb;
}
function getAgendaUTCDate(date, time) {
    var d = new Date(date);
    var timeArray = time.split(':');
    d.setHours(timeArray[0]);
    d.setMinutes(timeArray[1]);

    var year = d.getUTCFullYear();
    var month = getStringElementDateNumber(d.getUTCMonth() + 1);
    var day = getStringElementDateNumber(d.getUTCDate());
    var hours = getStringElementDateNumber(d.getUTCHours());
    var minutes = getStringElementDateNumber(d.getUTCMinutes());
    var dateUTCString = [day, month, year].join('-');
    timeUTCString = [hours, minutes].join(':');
    dateUTCString = [dateUTCString, timeUTCString].join(' ');
    // console.log("**************dateUTCString :
    // "+dateUTCString+"****************");
    return dateUTCString;
}
function agendaDateToString(stringDate) {
    return [[stringDate.get, stringDate.substring(4, 6),
            stringDate.substring(6, 8)].join('-'), stringDate.substring(9, 14)].join(' ');
}
function sendEventInfo() {
    var dateEvent = $('#dateEvent').datepicker('getDate');
    var beginTime = $('#beginTime').val();

    var dateEndEvent = $('#dateEndEvent').datepicker('getDate');
    var endTime = $('#endTime').val();

    var beginDate = getAgendaUTCDate(dateEvent, beginTime);
    var endDate = getAgendaUTCDate(dateEndEvent, endTime);


    var eventXml = "<event><eventTitle>"
            + htmlEncode($('#eventTitle').val()).trim()
            + "</eventTitle><recurrent>" + recurrentValue
            + "</recurrent><idEventType>" + htmlEncode($('#eventType').val())
            + "</idEventType><eventDescription>"
            + htmlEncode($('#eventDescription').val())
            + "</eventDescription><beginDate>" + beginDate + "</beginDate><endDate>"
            + endDate
            + "</endDate></event>";
    genericAjaxCall('POST', staticVars.urlBackEnd + 'agenda/add?idLang=' + getIdLangParam(),
            'application/xml', 'xml', eventXml, function (xml) {
                $('.backgroundPopup').hide();
                $('.popupEvent').hide();
                emptyEventFields();
                $("#isRecurrent").attr('checked', false);
                $("#radioDiv").hide();
                $.cookie("isAgenda", "true");
                window.location.reload();
            }, sendEventInfoError);
}

function updateEventError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}

function updateEvent(idEvent) {
    var dateEvent = $('#dateEvent').datepicker('getDate');
    var beginTime = $('#beginTime').val();

    var dateEndEvent = $('#dateEndEvent').datepicker('getDate');
    var endTime = $('#endTime').val();

    var beginDate = getAgendaUTCDate(dateEvent, beginTime);
    var endDate = getAgendaUTCDate(dateEndEvent, endTime);


    var eventXml = "<event><idEvent>" + idEvent + "</idEvent><eventTitle>"
            + htmlEncode($('#eventTitle').val().trim())
            + "</eventTitle><recurrent>" + recurrentValue
            + "</recurrent><idEventType>" + $('#eventType').val()
            + "</idEventType><eventDescription>"
            + htmlEncode($('#eventDescription').val())
            + "</eventDescription><beginDate>" + beginDate + "</beginDate><endDate>"
            + endDate
            + "</endDate></event>";
    // console.log(eventXml);
    genericAjaxCall('POST', staticVars.urlBackEnd + 'agenda/update?forumId=' + forumId,
            'application/xml', 'xml', eventXml, function (xml) {
                $('.backgroundPopup').hide();
                $('.popupEvent').hide();
                emptyEventFields();
                $("#isRecurrent").attr('checked', false);
                $("#radioDiv").hide();
                $.cookie("isAgenda", "true");
                window.location.reload();
                // $("#gridcontainer").reload();
            }, updateEventError);
}

var tabStandModelImages = [];
var tabReceptionistImages = [];

function fillReceptionistModelsError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}

function fillReceptionistModels() {

    genericAjaxCall('GET', staticVars.urlBackEnd
            + 'receptionDesign/receptionistModel', 'application/xml', 'xml',
            '', function (xml) {
                parseXmlReceptionistModel(xml);
            }, fillReceptionistModelsError);

}

function parseXmlReceptionistModel(xml) {
    $(xml).find("receptionistModel").each(
            function () {
                tabReceptionistImages[$(this).find('idreceptionistModel')
                        .text()] = $(this).find('receptionistImage').text();
                $('#receptionistModelSelect').append(
                        "<option value="
                        + $(this).find('idreceptionistModel').text()
                        + ">" + $(this).find('receptionistName').text()
                        + "</option>");
            });
}

function fillStandModelsError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}

function fillStandModels() {

    genericAjaxCall('GET',
            staticVars.urlBackEnd + 'receptionDesign/standModel',
            'application/xml', 'xml', '', function (xml) {
                parseXmlStandModel(xml);
            }, fillStandModelsError);

}

function parseXmlStandModel(xml) {
    $(xml).find("standModel").each(
            function () {
                tabStandModelImages[$(this).find('idstandModel').text()] = $(
                        this).find('imageModel').text();
                $('#standModelSelect').append(
                        "<option value=" + $(this).find('idstandModel').text()
                        + ">" + $(this).find('nameModel').text()
                        + "</option>");
            });

}

function getStandReceptionDesignError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}

function getStandReceptionDesign() {
    genericAjaxCall('GET', staticVars.urlBackEnd
            + 'receptionDesign/standReception', 'application/json', 'json', '',
            function (data) {
                parseXmlStandReceptionDesign(data);
            }, getStandReceptionDesignError);
}

function parseXmlStandReceptionDesign(data) {
    $('#standModelSelect option').filter(function () {
        return $(this).val() == data.idStandModel;
    }).attr('selected', true);
    $('#receptionistModelSelect option').filter(function () {
        return $(this).val() == data.idReceptionistModel;
    }).attr('selected', true);

    $("#standImage").attr('src', data.standModelImage);
    $("#recImage").attr('src', data.receptionistModelImage);
}

function getStandReceptionDesignError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}

function sendReceptionDesign() {
    genericAjaxCall('POST', staticVars.urlBackEnd
            + 'receptionDesign/changeDesign?idStandModel='
            + $('#standModelSelect').val() + '&idReceptionistModel='
            + $('#receptionistModelSelect').val(), 'application/xml', 'xml',
            '', function () {
                showUpdateSuccess();
            }, getStandReceptionDesignError);
}

// help page scripts

function flowPlayer(videoUrl, backgroundImageUrl) {
    $f("player", "flowplayer/flowplayer-3.2.15.swf", {
        clip: {
            url: videoUrl, // 'videos/wildlife.wmv', // put video url here
            autoPlay: false
        },
        plugins: {
            audio: {
                url: 'flowplayer/flowplayer.audio.swf'
            }
        },
        canvas: {
            // configure background properties
            background: '#000000 url(' + backgroundImageUrl
                    + ') no-repeat 30 10', // '#000000 url(images/rec1.jpg)
            // no-repeat 30 10',

            // remove default canvas gradient
            backgroundGradient: 'none',
        }
    });
}

function viewUserProfile(idCandidate) {
    //console.log("viewUserProfile");
    window.open(staticVars["urlProfile"] + idCandidate + "&idLang=" + getIdLangParam(), '_blank');
}

function viewUserProfileReaded(idCandidate, jobAppId, cookie) {
    window.open(staticVars["urlProfile"] + idCandidate + "&idLang=" + getIdLangParam(), '_blank');
    var cookieName = "";
    if (cookie == 2) {
        $('#notReaded_' + jobAppId).hide();
        cookieName = "readed-candidature-spontanee";
    } else {
        $('#notReded_' + jobAppId).hide();
        cookieName = "readed-candidature";
    }
    var readedjobApplication = $.cookie(cookieName);
    if (readedjobApplication != null) {
        readedjobApplication = readedjobApplication + ',' + jobAppId;
    } else {
        readedjobApplication = jobAppId;
    }
    $.cookie(cookieName, readedjobApplication);
}

function viewCVReaded(jobAppId, cookie) {
    var cookieName = "";
    if (cookie == 2) {
        $('#notReaded_' + jobAppId).hide();
        cookieName = "readed-candidature-spontanee";
    } else {
        $('#notReded_' + jobAppId).hide();
        cookieName = "readed-candidature";
    }
    var readedjobApplication = $.cookie(cookieName);
    if (readedjobApplication != null) {
        readedjobApplication = readedjobApplication + ',' + jobAppId;
    } else {
        readedjobApplication = jobAppId;
    }
    $.cookie(cookieName, readedjobApplication);
}

// Rights Scripts

function StandRight(data) {
    this.rightId = $(data).find("idRightGroup").text();
    this.functionnalityName = $(data).find("functionalityDTO").find('name')
            .text();
    this.rightCount = $(data).find("rightCount").text();

    this.getRightId = function() {
        return this.rightId;
    };

    this.getFunctionnalityName = function() {
        return this.functionnalityName;
    };

    this.getRightCount = function() {
        return this.rightCount;
    };

}

function getStandRightsError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}

function getStandRights(callBack) {
    //console.log("***********getStandRights*******");
    if(listStandRights==null){ 
        genericAjaxCall('GET', staticVars.urlBackEnd + 'stand/standRights',
                'application/xml', 'xml', '', function(xml) {
                    listStandRights=[];
                    $(xml).find("rightGroup").each(function() {
                        var standRight = new $.RightGroup($(this));
                        listStandRights[standRight.getFunctionality().getFunctionalityId()] = standRight;
                    });
                    callBack();
                }, getStandRightsError);
    }else callBack();

}

// jobOffer page scripts
function fillActivitySectors() {
    var activitySectorSelectHandler = $('#addPhotoModal #activitySectorSelect');
    activitySectorSelectHandler.empty();
    //activitySectorSelectHandler.append($('<option/>').val(0).html(CHOOSELABEL));
    for (index in listActivitySectorsCache) {
        var secteurAct = listActivitySectorsCache[index];
        activitySectorSelectHandler.append($('<option/>').val(secteurAct.getIdsecteur()).html(secteurAct.getName()));
    }
}

function getActivitySectorsSuccess(xml) {
    listActivitySectorsCache = [];
    $(xml).find("secteurActDTO").each(
            function () {
                var secteurAct = new $.SecteurAct($(this));
                listActivitySectorsCache[secteurAct.getIdsecteur()] = secteurAct;
            });
}

function getActivitySectorsError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}
function getActivitySectors() {
    if (listActivitySectorsCache == null) {
        genericAjaxCall('GET', staticVars.urlBackEnd + 'secteuract?idLang=' + getIdLangParam(),
                'application/xml', 'xml', '', getActivitySectorsSuccess, getActivitySectorsError);
    }
}
function fillRegions() {
    var cantonSelectHandler = $('#addPhotoModal #cantonSelect');
    cantonSelectHandler.empty();
    //cantonSelectHandler.append($('<option/>').val(0).html(CHOOSELABEL));
    for (index in listRegionsCache) {
        var region = listRegionsCache[index];
        cantonSelectHandler.append($('<option/>').val(region.getIdRegion()).html(region.getRegionName()));
    }
}
function getRegionSuccess(xml) {
    listRegionsCache = [];
    $(xml).find("regionDTO").each(
            function () {
                var region = new $.Region($(this));
                listRegionsCache[region.getIdRegion()] = region;
            });
}
function getRegionsError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}
function getRegions() {
    if (listRegionsCache == null) {
        genericAjaxCall('GET', staticVars.urlBackEnd + 'region?idLang=' + getIdLangParam(), 'application/xml',
                'xml', '', getRegionSuccess, getRegionsError);
    }
}

function fillStudyLevels() {
    var studyLevelSelectHandler = $('#addPhotoModal #studyLevelSelect');
    studyLevelSelectHandler.empty();
    //studyLevelSelectHandler.append($('<option/>').val(0).html(CHOOSELABEL));
    for (index in listStudyLevelsCache) {
        var studyLevel = listStudyLevelsCache[index];
        studyLevelSelectHandler.append($('<option/>').val(studyLevel.getIdStudyLevel()).html(studyLevel.getStudyLevelName()));
    }
}
function getStudyLevelsSuccess(xml) {
    listStudyLevelsCache = [];
    $(xml).find("studyLevelDTO").each(
            function () {
                var studyLevel = new $.StudyLevel($(this));
                listStudyLevelsCache[studyLevel.getIdStudyLevel()] = studyLevel;
            });
}
function getStudyLevelsError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}

function getStudyLevels() {
    if (listStudyLevelsCache == null) {
        genericAjaxCall('GET', staticVars.urlBackEnd + 'studyLevel?idLang=' + getIdLangParam(),
                'application/xml', 'xml', '', getStudyLevelsSuccess, getStudyLevelsError);
    }

}
function fillFunctionsCandidate() {
    var functionSelectSelectHandler = $('#addPhotoModal #functionSelect');
    functionSelectSelectHandler.empty();
    //functionSelectSelectHandler.append($('<option/>').val(0).html(CHOOSELABEL));
    for (index in listFunctionsCandidateCache) {
        var functionCandidate = listFunctionsCandidateCache[index];
        functionSelectSelectHandler.append($('<option/>').val(functionCandidate.getIdFunction()).html(functionCandidate.getFunctionName()));
    }
}
function getFunctionsCandidateSuccess(xml) {
    listFunctionsCandidateCache = [];
    $(xml).find("functionCandidateDTO").each(
            function () {
                var functionCandidate = new $.FunctionCandidate($(this));
                listFunctionsCandidateCache[functionCandidate.getIdFunction()] = functionCandidate;
            });
}
function getFunctionsCandidateError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}

function getFunctionsCandidate() {
    if (listFunctionsCandidateCache == null) {
        genericAjaxCall('GET', staticVars.urlBackEnd + 'functionCandidate?idLang=' + getIdLangParam(),
                'application/xml', 'xml', '', getFunctionsCandidateSuccess, getFunctionsCandidateError);
    }
}
function fillExperienceYears() {
    var experienceYearsSelectSelectHandler = $('#addPhotoModal #experienceYearsSelect');
    experienceYearsSelectSelectHandler.empty();
    //experienceYearsSelectSelectHandler.append($('<option/>').val(0).html(CHOOSELABEL));
    for (index in listExperienceYearsCache) {
        var experienceYears = listExperienceYearsCache[index];
        experienceYearsSelectSelectHandler.append($('<option/>').val(experienceYears.getIdExperienceYears()).html(experienceYears.getExperienceYearsName()));
    }
}
function getExperienceYearsSuccess(xml) {
    listExperienceYearsCache = [];
    $(xml).find("experienceYearsDTO").each(
            function () {
                var experienceYears = new $.ExperienceYears($(this));
                listExperienceYearsCache[experienceYears.getIdExperienceYears()] = experienceYears;
            });
}
function getExperienceYearsError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}

function getExperienceYears() {
    if (listExperienceYearsCache == null) {
        genericAjaxCall('GET', staticVars.urlBackEnd + 'experienceYears?idLang=' + getIdLangParam(),
                'application/xml', 'xml', '', getExperienceYearsSuccess, getExperienceYearsError);
    }

}

function getAreaExpertisesSuccess(xml) {
    listAreaExpertisesCache = [];
    $(xml).find("areaExpertiseDTO").each(
                    function() {
                        var areaExpertise=new $.AreaExpertise($(this));
                        listAreaExpertisesCache[areaExpertise.getAreaExpertiseId()] = areaExpertise;
                    });
}
function getAreaExpertisesError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}

function getAreaExpertises() {
    if(listAreaExpertisesCache == null){
        genericAjaxCall('GET', staticVars.urlBackEnd + 'areaExpertise?idLang='+getIdLangParam(),
                'application/xml', 'xml', '', getAreaExpertisesSuccess, getAreaExpertisesError);
    }
}

function getJobsSoughtSuccess(xml) {
    listJobsSoughtCache = [];
    $(xml).find("jobSoughtDTO").each(
                    function() {
                        var jobSought=new $.JobSought($(this));
                        listJobsSoughtCache[jobSought.getJobSoughtId()] = jobSought;
                    });
}
function getJobsSoughtError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}

function getJobsSought() {
    if(listJobsSoughtCache == null){
        genericAjaxCall('GET', staticVars.urlBackEnd + 'jobSought?idLang='+getIdLangParam(),
                'application/xml', 'xml', '', getJobsSoughtSuccess, getJobsSoughtError);
    
    }
}

function getCandidateStatesSuccess(xml) {
    listCandidateStatesCache = [];
    $(xml).find("candidateStateDTO").each(
                    function() {
                        var candidateState = new $.CandidateState($(this));
                        listCandidateStatesCache[candidateState.getCandidateStateId()] = candidateState;
                    });
}
function getCandidateStatesError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}

function getCandidateStates() {
    if(listCandidateStatesCache == null){
        genericAjaxCall('GET', staticVars.urlBackEnd + 'candidateState?idLang='+getIdLangParam(),
                'application/xml', 'xml', '', getCandidateStatesSuccess, getCandidateStatesError);
    }
}
function getSurveysByPathError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}

function getSurveysByPath(callBack, path) {
    genericAjaxCall('GET', staticVars.urlBackEnd + path, 'application/xml',
            'xml', '', function (xml) {
                listSurveysCache = new Array();
                $(xml).find("formDTO")
                        .each(
                                function () {
                                    listSurveysCache[$(this).find("formId")
                                            .text()] = $(this);
                                });
                callBack();
            }, getSurveysByPathError);

}
function getStandWebsiteByPathError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}

function getStandWebsiteByPath(callBack, path) {
    //console.log('getStandWebsiteByPath : '+path);
    genericAjaxCall('GET', staticVars.urlBackEnd + path, 'application/xml',
            'xml', '', function (xml) {
                listWebsitesCache = new Array();
                $(xml).find("website")
                        .each(
                                function () {
                                    listWebsitesCache[$(this).find("idwebsite").text()] = new $.Website($(this));
                                });
                callBack();
            }, getStandWebsiteByPathError);

}

function getSurveysError(xhr, status, error) {
    isSessionExpired(xhr.responseText);
}

function getSurveys() {
    genericAjaxCall('GET', staticVars.urlBackEnd + 'sondage/standSurveys?idLang=' + getIdLangParam(),
            'application/xml', 'xml', '', function (xml) {
                parseAllSurveysXml(xml);
            }, getSurveysError);

}

function parseAllSurveysXml(xml) {
    $(".listSurveys").empty();
    $(xml).find("sondage").each(
            function () {
                var idSurvey = $(this).find("idsondage").text();
                var surveyTitle = $(this).find("title").text();
                var questionNumber = 1;
                var surveyData = {
                    idSurvey: replaceSpecialChars(idSurvey),
                    surveyTitle: replaceSpecialChars(surveyTitle),
                    surveyTitle_formatted: replaceSpecialChars(formattingLongWords(surveyTitle, 50)),
                    addLabel: ADDLABEL,
                    editLabel: EDITLABEL,
                    deleteLabel: DELETELABEL,
                    questionLabel: QUESTIONLABEL,
                    okLabel: OKLABEL,
                    cancelLabel: CANCELLABEL,
                };
                $("#tempSurvey").tmpl(surveyData).appendTo(".listSurveys");
                $("#removeSurvey" + idSurvey).bind('click', function () {
                    idSurveyToRemove = idSurvey;
                    $('.popupDeleteSurvey').show();
                    $('.backgroundPopup').show();
                });
                $('#surveyTitleInput' + idSurvey).bind(
                        'blur',
                        function () {
                            var valSurvey = $('#surveyTitleInput' + idSurvey).val();
                            if (valSurvey == null || valSurvey == "" || valSurvey == " ") {

                            } else {
                                updateSurvey(idSurvey, valSurvey);
                                //valSurvey = formattingLongWords(valSurvey, 50);
                                $('#surveyTitleLabel' + idSurvey).text(formattingLongWords(valSurvey, 50));
                                $('#surveyTitleLabel' + idSurvey).show();
                                $('#surveyTitleInput' + idSurvey).hide();
                                $("#editDeleteLi" + idSurvey).show();
                                $("#restoreLi" + idSurvey).hide();
                            }
                        });
                $('.saveDiv' + idSurvey).hide();
                $('.addQuestionLink' + idSurvey).bind('click',
                        function () {
                            displayAddQuestion(idSurvey);
                        });

                $(this).find("questionDTOList")
                        .find('question')
                        .each(
                                function () {
                                    var idQuestion = $(this).find(
                                            'idQuestion').text();
                                    var questionContent = $(this).find(
                                            'questionContent').text();
                                    var responseNumber = 1;
                                    var questionData = {
                                        idQuestion: replaceSpecialChars(idQuestion),
                                        questionContent: replaceSpecialChars(questionContent),
                                        questionContent_formatted: replaceSpecialChars(formattingLongWords(questionContent, 50)),
                                        questionNumber: replaceSpecialChars(questionNumber),
                                        addLabel: ADDLABEL,
                                        editLabel: EDITLABEL,
                                        deleteLabel: DELETELABEL,
                                        okLabel: OKLABEL,
                                        cancelLabel: CANCELLABEL,
                                        questionLabel: QUESTIONLABEL,
                                        responseLabel: RESPONSELABEL
                                    };
                                    questionNumber++;
                                    $("#tempQuestionDisplay").tmpl(
                                            questionData)
                                            .appendTo(
                                                    ".listQuestions"
                                                    + idSurvey);
                                    $('#questionContentInput' + idQuestion)
                                            .bind(
                                                    'blur',
                                                    function () {
                                                        var valQuestion = $('#questionContentInput' + idQuestion).val();
                                                        if (valQuestion == null || valQuestion == "" || valQuestion == " ") {

                                                        } else {
                                                            updateSurveyUpdateQuestion(idQuestion, valQuestion);
                                                            $('#questionContentLabel' + idQuestion).text(formattingLongWords(valQuestion, 50));
                                                            $('#questionContentLabel' + idQuestion).show();
                                                            $('#questionContentInput' + idQuestion).hide();
                                                            $("#editDeleteQuestionLi" + idQuestion).show();
                                                            $("#restoreQuestionLi" + idQuestion).hide();
                                                        }
                                                    });
                                    $('.displayAddResponse' + idQuestion)
                                            .bind('click',
                                                    function () {
                                                        displayAddResponse(idQuestion);
                                                    });
                                    $(this).find('responseDTOList')
                                            .find('response')
                                            .each(
                                                    function () {
                                                        var idResponse = $(this).find('idResponse').text();
                                                        var responseContent = $(this).find('responseContent').text();
                                                        var responseData = {
                                                            idResponse: replaceSpecialChars($(this).find('idResponse').text()),
                                                            responseContent: replaceSpecialChars(responseContent),
                                                            responseContent_formatted: replaceSpecialChars(formattingLongWords(responseContent, 40)),
                                                            responseNumber: replaceSpecialChars(responseNumber),
                                                            editLabel: EDITLABEL,
                                                            deleteLabel: DELETELABEL,
                                                            okLabel: OKLABEL,
                                                            responseLabel: RESPONSELABEL
                                                        };
                                                        responseNumber++;
                                                        $("#tempResponseDisplay").tmpl(responseData).appendTo("#listResponses" + idQuestion);
                                                        $('#responseContentInput' + idResponse)
                                                                .bind('blur',
                                                                        function () {
                                                                            var valResponse = $('#responseContentInput' + idResponse).val();
                                                                            if (valResponse != null && valResponse != "" && valResponse != " ") {
                                                                                updateSurveyUpdateResponse(idResponse, valResponse);
                                                                                $('#responseContentLabel' + idResponse).text(formattingLongWords(valResponse, 40));
                                                                                $('#responseContentLabel' + idResponse).show();
                                                                                $('#responseContentInput' + idResponse).hide();
                                                                                $("#editDeleteResponseLi" + idResponse).show();
                                                                                $("#restoreResponseLi" + idResponse).hide();
                                                                            }
                                                                        });
                                                    });
                                });
                var surveySelectorId = "survey" + idSurvey;
                customJqEasyCounter("#" + surveySelectorId + " .surveyInput", 150, 150, '#0796BE', LEFTCARACTERES);
                customJqEasyCounter("#" + surveySelectorId + " .questionInput, #" + surveySelectorId + " .responseInput", 255, 255, '#0796BE', LEFTCARACTERES);
                $("#" + surveySelectorId + " div.jqEasyCounterMsg").css('width', '55%');
            });
    $('.inputTitle').hide();
    $('.restore').hide();
    $('.listQuestions').hide();
    $(".listSurveys").show();
    $('#loadingEds').hide();
}

function playJwPlayer(idVideo, videoUrl, w, h) {
    if (compareToLowerString(videoUrl, '//www.youtube') != -1) {
        videoUrl = videoUrl.replace('/v/', '/watch?v=');
        jwplayer(idVideo).setup({
            id: idVideo,
            file: videoUrl,
            width: w,
            height: h
        });
    } else {
        jwplayer(idVideo).setup({
            id: idVideo,
            swf: 'plugins/jwplayer/player.swf',
            file: videoUrl,
            width: w,
            height: h
        });
    }
}

function validateJobOfferForm() {
    var validateMessage = "<div>" + REQUIREDLABEL + "</div>";
    $.validator.addMethod("validSelect1", function (value, element) {
        var selectVal = $.trim($("#listActivitySectors").html());
        if (selectVal == '')
            return false;
        else
            return true;
    }, validateMessage);
    $.validator.addMethod("validSelect2", function (value, element) {
        var selectVal = $.trim($("#listCantons").html());
        if (selectVal == '')
            return false;
        else
            return true;
    }, validateMessage);
    $.validator.addMethod("validSelect4", function (value, element) {
        var selectVal = $("#studyLevelSelect").val();
        if (selectVal == 0)
            return false;
        else
            return true;
    }, validateMessage);
    $.validator.addMethod("validSelect5", function (value, element) {
        var selectVal = $("#functionSelect").val();
        if (selectVal == 0)
            return false;
        else
            return true;
    }, validateMessage);

    $.validator.addMethod("validSelect8", function (value, element) {
        var selectVal = $("#experienceYearsSelect").val();
        if (selectVal == 0)
            return false;
        else
            return true;
    }, validateMessage);
    $("#jobCreationForm").validate({
        rules: {
            activitySectorSelect: {
                validSelect1: true
            },
            cantonSelect: {
                validSelect2: true
            },
            studyLevelSelect: {
                validSelect4: true
            },
            functionSelect: {
                validSelect5: true
            },
            experienceYearsSelect: {
                validSelect8: true
            },
        }
    });
}

/*
 * contact scripts
 */

function parseContactXml(xml) {
    $("#listContacts").empty();
    $(xml).find('contact').each(function () {
        var idContact = $(this).find('idcontact').text();
        var photoContact = $(this).find('photo').text();
        var nameContact = $(this).find('nom').text();
        var functionContact = $(this).find('jobTitle').text();
        var emailContact = $(this).find('email').text();
        var phoneContact = $(this).find('phone').text();
        var data = {
            idContact: replaceSpecialChars(idContact),
            photoContact: replaceSpecialChars(photoContact),
            nameContact: replaceSpecialChars(nameContact),
            emailContact: replaceSpecialChars(emailContact),
            phoneContact: replaceSpecialChars(phoneContact),
            functionContact: replaceSpecialChars(functionContact)
        };
        $('#contactTemplate').tmpl(data).appendTo('#listContacts');
        // $('#contact' + idContact).bind('mouseenter', function() {
        // $(this).find('.contactOptions').show();
        // });
        // $('#contact' + idContact).bind('mouseleave', function() {
        // $(this).find('.contactOptions').hide();
        // });
        $('.deleteContact' + idContact).bind('click', function () {
            idContactToChange = idContact;
            $('.popupDeleteContact,.backgroundPopup').show();
        });
        $('#editContact' + idContact).bind('click', function () {
            idContactToChange = idContact;
            isContactUpdate = true;
            $('#contactPhoto').attr('src', photoContact);
            $('#contactName').val(nameContact);
            $('#emailContact').val(emailContact);
            $('#contactTel').val(phoneContact);
            $('#contactJobTitle').val(functionContact);
            $('.popupContact,.backgroundPopup,#contactDefaultImage').show();
            $('#progressContactPhoto,#loadContactError').hide();
            enabledButton('#addContactButton');
        });
    });
}

function getContacts() {
    genericAjaxCall('GET', staticVars["urlBackEnd"] + 'contact?idLang=' + getIdLangParam(),
            'application/xml', 'xml', '', function (xml) {
                parseContactXml(xml);
            }, '');
}

function addContactSuccess(xml) {
    hidePopups();
    enabledButton('#addContactButton');
    getContacts();
}

function addContact() {
    var contactXml = "<contact><photo>" + contactPhoto + "</photo><email>"
            + htmlEncode($('#emailContact').val()) + "</email><nom>"
            + htmlEncode($('#contactName').val()) + "</nom><phone>"
            + htmlEncode($('#contactTel').val()) + "</phone><jobTitle>"
            + htmlEncode($('#contactJobTitle').val()) + "</jobTitle></contact>";
    genericAjaxCall('POST', staticVars["urlBackEnd"] + 'contact/add?idLang=' + getIdLangParam(),
            'application/xml', 'xml', contactXml, addContactSuccess,
            genericError);
}

function updateContactSuccess() {
    hidePopups();
    enabledButton('#addContactButton');
    getContacts();
}

function updateContact() {
    var contactXml = "<contact><idcontact>" + idContactToChange
            + "</idcontact><photo>" + contactPhoto + "</photo><email>"
            + htmlEncode($('#emailContact').val()) + "</email><nom>"
            + htmlEncode($('#contactName').val()) + "</nom><phone>"
            + htmlEncode($('#contactTel').val()) + "</phone><jobTitle>"
            + htmlEncode($('#contactJobTitle').val()) + "</jobTitle></contact>";
    genericAjaxCall('POST', staticVars["urlBackEnd"] + 'contact/update',
            'application/xml', 'xml', contactXml, updateContactSuccess,
            genericError);
}

function getInternetExplorerVersion()// 6.0 | 7.0 | 8.0
// -1 : Other Navigator
{
    var rv = -1; // Return value assumes failure.
    if (navigator.appName == 'Microsoft Internet Explorer') {
        var ua = navigator.userAgent;
        var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
        if (re.exec(ua) != null)
            rv = parseFloat(RegExp.$1);
    }
    return rv;
}
function hideComponent(comp) {
    comp.css('visibility', 'hidden');
}
function showComponent(comp) {
    comp.css('visibility', 'visible');
}
function capitaliseFirstLetter(string)
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}
function getExtension(filename) {
    if (filename != null)
        return filename.split('.').pop().toLowerCase();
    else
        return filename;
}
function disabledButton(selector) {
    $(selector).attr("disabled", "disabled");
}
function enabledButton(selector) {
    $(selector).removeAttr("disabled");
}

function playVideoPopup(videoUrl, videoTitle) {
    var formatVideoTitle = formattingLongWords(videoTitle, 70);
    var customVideoTitle = substringCustom(formatVideoTitle, 70);
    $('#videoTitlePopup').text(customVideoTitle);
    $('#videoTitlePopup').attr('title', videoTitle);
    var video_container = null;
    var playerVideoPopupSelector = $('.contentViewVideo');
    playerVideoPopupSelector.empty();
    if (compareToLowerString(videoUrl, "dailymotion") !== -1) {
        video_container = $('<iframe id="playerVideo" src="'
                + videoUrl
                + '" style="width:100%; height:100%;" frameborder="0"></iframe>');
        playerVideoPopupSelector.append(
                video_container);
    } else {
        video_container = $('<div id="playerVideo" class="player" style="width: 100%; height: 100%;"> </div>');
        playerVideoPopupSelector.append(
                video_container);
        playJwPlayer("playerVideo", videoUrl, "100%", "100%");
    }
}
function substringCustom(text, size)
{
    if (text.length < size)
        return text;
    else
        return text.substring(0, size) + "...";
}
function getFileExtension(url) {
    if (url)
        return url.split('.').pop();
    return "";
}

function getUploadFileName(timestamp, fileName) {
    var fileNameExtension = getFileExtension(fileName);
    return $.md5(timestamp + SEPARATEUPLOAD + fileName) + '.' + fileNameExtension;
}

function getUploadFileUrl(fileName) {
    return staticVars.urlServerStorage + fileName;
}
function getUrlPostUploadFile(timestamp, fileName) {
    return staticVars.urlServerUploader + "?token=" + $.md5(timestamp + SEPARATEUPLOAD + fileName) + "&ext=." + getFileExtension(fileName);
}

function progressBarUploadFile(selectorDivProgress, data, maxProgress) {
    var progress = parseInt(data.loaded / data.total * 100, 10);
    if (progress <= maxProgress) {
        $(selectorDivProgress + ' .progress-bar').css('width', progress + '%');
        $(selectorDivProgress + ' .loadingText').text(CHARGEMENTLABEL + ' ' + progress + '%');
    }
}

function beforeUploadFile(selectorSaveBotton, selectorDivProgress, selectorToHide) {
    disabledButton(selectorSaveBotton);
    $(selectorToHide).hide();
    $(selectorDivProgress + ' .progress-bar').css('width', '0%');
    $(selectorDivProgress + ' .loadingText').text(CHARGEMENTLABEL + ' 0%');
    $(selectorDivProgress).show();
}
function afterUploadFile(selectorSaveBotton, selectorDivProgress, selectorToHide) {
    enabledButton(selectorSaveBotton);
    $(selectorToHide).hide();
    $(selectorDivProgress).show();
}
function isDisabled(selector) {
    if ($(selector).attr("disabled") == "disabled")
        return true;
    else
        return false;
}
function buildXML(rootElement, data) {
    var balise = "<" + rootElement + ">";
    var keys = Object.keys(data);
    $.each(keys, function () {
        balise = balise.concat('<' + this + '>' + data[this] + '</' + this + '>');
    });
    balise = balise.concat("</" + rootElement + ">");
    return balise;
}
function buildXMLList(rootElement, parentElement, datas) {
    var balise = "<" + rootElement + ">";
    $.each(datas, function () {
        var that = this;
        var keys = Object.keys(this);
        balise = balise.concat(buildXML(parentElement, that));
    });
    balise = balise.concat("</" + rootElement + ">");
    //console.log(balise);
    return balise;
}
function encode_utf8(s) {
    for (var c, i = -1, l = (s = s.split("")).length, o = String.fromCharCode; ++i < l;
            s[i] = (c = s[i].charCodeAt(0)) >= 127 ? o(0xc0 | (c >>> 6)) + o(0x80 | (c & 0x3f)) : s[i]
            )
        ;
    return s.join("");
}
function decode_utf8(s) {
    for (var a, b, i = -1, l = (s = s.split("")).length, o = String.fromCharCode, c = "charCodeAt"; ++i < l;
            ((a = s[i][c](0)) & 0x80) &&
            (s[i] = (a & 0xfc) == 0xc0 && ((b = s[i + 1][c](0)) & 0xc0) == 0x80 ?
                    o(((a & 0x03) << 6) + (b & 0x3f)) : o(128), s[++i] = "")
            )
        ;
    return s.join("");
}
function extractDate(timestamp) {
    var date = new Date(timestamp * 1000);
    return getStringElementDateNumber(date.getHours()) + ":" + getStringElementDateNumber(date.getMinutes());
}

function timestampsToDate(timestamp) {
    var theDate = new Date();
    theDate.setTime(timestamp);
    dateString = theDate.toGMTString();
    return dateString.substring(5, 22);
}

function timestampsToDateFormated(timestamp) {
    moment.locale('fr');
    var theDate = new Date(parseInt(timestamp));
    return moment(theDate).calendar();   
}

getUserFavoritesSuccess = function (xml) {
    listUserFavorites = [];
    $(xml).find("favorite").each(
            function () {
                var favorite = new $.Favorite(
                        $(this));
                listUserFavorites.push(favorite);
            });
};

getUserFavoritesError = function (xhr, status, error) {
    isSessionExpired(xhr.responseText);
};

getUserFavorites = function () {
    genericAjaxCall('GET', staticVars["urlBackEnd"] + 'favorite/userFavorites?idLang=' + getIdLangParam(), 'application/xml', 'xml', '', getUserFavoritesSuccess, getUserFavoritesError);
};

function factoryXMLFavory(idEntity, componentName) {
    return "<favorite><idEntity>"
            + idEntity
            + "</idEntity><component><componentName>"
            + componentName
            + "</componentName></component></favorite>";
}

function addFavoriteSuccess() {
    getUserFavorites();
    favoriteAdded();
}

function addFavorite(favoriteXml) {
    genericAjaxCall('POST', staticVars["urlBackEnd"] + 'favorite/addFavorite', 'application/xml', 'xml', favoriteXml, addFavoriteSuccess, '');
}

function favoriteAdded() {
    $('html, body').animate({
        scrollTop: 0
    }, 'slow');
    $('#favoriteAdded').fadeIn(function () {
        $('#favoriteAdded').delay(3000).fadeOut();
    });
}

function deleteFavoriteSuccess() {
    getUserFavorites();
    favoriteDeleted();
}

function deleteFavorite(favoriteXml) {
    genericAjaxCall('POST', staticVars["urlBackEnd"] + 'favorite/deleteFavorite', 'application/xml', 'xml', favoriteXml, deleteFavoriteSuccess, '');
}

function favoriteDeleted() {
    $('html, body').animate({
        scrollTop: 0
    }, 'slow');
    $('#favoriteDeleted').fadeIn(function () {
        $('#favoriteDeleted').delay(1200).fadeOut();
    });
}

function messageSent() {
    $('.contentOpacity').hide();
    $('#content_infos_reception').toggle();
    $('html, body').animate({
        scrollTop: 0
    }, 'slow');
    $('#messageSent').fadeIn(function () {
        $('#messageSent').delay(3000).fadeOut();
    });
    $('#messageForm #messageContent,#messageForm #messageSubject').val("");
}
function getTranslatePath() {
    var href = window.location.href;
    if (href.indexOf('?') != -1)
        href = href.substring(0, href.lastIndexOf('?'));
    if (href.indexOf('#') != -1)
        href = href.substring(0, href.lastIndexOf('#'));
    href = href.substring(0, href.lastIndexOf('/'));
    return href + "/translate/" + getFileName(translateUrl);
}
function getHistoryChatPath() {
    var href = window.location.href;
    if (href.indexOf('?') != -1)
        href = href.substring(0, href.lastIndexOf('?'));
    if (href.indexOf('#') != -1)
        href = href.substring(0, href.lastIndexOf('#'));
    href = href.substring(0, href.lastIndexOf('/'));
    return href + "/chathistory.html?path=";
}
function getFileName(url) {
    if (url)
        return url.split('/').pop();
    return "";
}
function replaceSpecialChars(value) {
    if (value)
        return value.toString().replace('µ', '&micro');
    else
        return value;
}

function compareToLowerString(str1, str2) {
    if (str1 != null)
        str1 = str1.toLowerCase();
    else
        str1 = "";
    if (str2 != null)
        str2 = str2.toLowerCase();
    return str1.indexOf(str2);
}
function getAllLangagesDBinitSuccess(languagesXml) {
    $("#languageSelect").empty();
    $(languagesXml).find('languaget').each(
            function () {
                var languageId=$(this).find('idlangage').text();
                 var languageName=$(this).find('name').text();
                 var languageAbr=$(this).find('abbreviation').text();
                 $("#languageSelect").append($('<option/>').val(languageId).html(languageName));
                 listLangsEnum[languageId] = languageName;
                 listLangsAbrEnum[languageId] = languageAbr;
            });
    $('.languageSelect').change(function () {
        if (currentPage == PageEnum.BACKEND)
            showLanguagePopup();
        else if (currentPage == PageEnum.LOGIN)
            window.location.replace("?idLang=" + $("#languageSelect").val());
    });
    $('#languageSelect option').filter(function () {
        return $(this).val() == getIdLangParam();
    }).attr('selected', true);

    if (currentPage == PageEnum.BACKEND)
        //$('#languageSelect').selectpicker('reloadLi');
    checkLoginWSCallsInProgress();
}
;
function getAllLangagesDBinit() {
    genericAjaxCall('GET', staticVars["urlBackEnd"] + 'languaget/listLanguages?idLang=' + getIdLangParam(), 'application/xml', 'xml', '', getAllLangagesDBinitSuccess);
}
;
function checkLoginWSCallsInProgress() {
    nbreLoginWSCallsInProgress--;
    //console.log('nbreLoginWSCallsInProgress: '+nbreLoginWSCallsInProgress);
    if (nbreLoginWSCallsInProgress < 1) {
        nbreLoginWSCallsInProgress = 0;
        $('#loadingEdsLogin').hide();
    }
}

function replaceAllHtmlCast(str){
    if(str != null && str != undefined) 
        return str.split("'").join("&apos;").split('"').join("&quot;");
    else return "";
}

function formattingLongWords(str, maxLength) {
    var result = null;
    if (str != null && result != '') {
        result = '';
        var words = str.split(' ');
        $.each(words, function () {
            if (this.length > maxLength)
                result = result.concat(this.substring(0, maxLength)).concat('... ');
            else
                result = result.concat(this).concat(' ');
        });
        result = result.substring(0, result.length - 1);
    }
    return result;
}

/*function customJqEasyCounter(selector, maxChars, maxCharsWarning, msgFontColor, msgCaracters) {
    $(selector).jqEasyCounter({
        'maxChars': maxChars,
        'maxCharsWarning': maxCharsWarning,
        'msgFontColor': msgFontColor,
        'msgCaracters': msgCaracters,
    });
}
;*/

function factoryChangeChatTab(selectorLi, selectorTab) {
    $('#chatSettings li').removeClass('active');
    $(selectorLi).addClass('active');
    $('.chatTabs').css('visibility', 'hidden').css('height', '0px').css('display', 'none');
    $(selectorTab).css('visibility', 'visible').css('height', '100%').css('display', 'block');
}

function connectToChatFlash(chatId) {
    isConnectToChat = true;
    $('#chatPublicLoading').css('display', 'block');
    $('#chatComponent').empty();
    $('#chatComponent').append('<div id="chatFlashContent"></div>'); 
    getAMSIp(function(ip){
        initChatPublic("chatFlashContent", chatId, ip);
    });
    $('#connectToChatButton').text(DISCONNECTFROMCHATLABEL);
  $('#connectToChatButton').addClass("disconnected");
}

function chatBlink() {
    //console.log('chatBlink');
};

function doSettings() {
    //console.log('doSettings');
    $('#chatSettings a.settings').trigger("click");
};

function viewMyChats() {
    //console.log('viewMyChats');
    $('#chatSettings a.historicChat').trigger("click");
}
function reconnectToChat() {
    if (currentPage == PageEnum.CHAT)
        chatController.reconnectToChat();
    else {
        if (isConnectToChat) {
            tryToReconnect++;
            if (tryToReconnect < 2) {
                isConnectToChat = false;
                $('#connectToChatButton').text(CONNECTTOCHATLABEL);
                $('#connectToChatButton').removeClass("disconnected");
                getCurrentsChat();
            } else
                window.location.reload();
        }
    }
}

/******************* Initialise and launch Public Chat************************/
function initChatPublic(chatFlashContentId, chatId, ip) {
    //console.log('initChatPublic');
    flashvars.serverUrl = staticVars.urlBackEnd;
    flashvars.fmsUrl = FMS_URL.replace(":ip:", ip) + FMS_APPLICATION_CHAT;
    flashvars.historyUrl = HISTORY_URL;
    flashvars.imagesUrl = IMAGES_URL;
    flashvars.state = 'forum';
    flashvars.translateUrl = getTranslatePath();
    flashvars.historyChatUrl = getHistoryChatPath();
    flashvars.standLanguageId = getIdLangParam();
    flashvars.languageId = getIdLangParam();
    flashvars.customColor1 = customColor1;
    flashvars.userType = userType;
    flashvars.chatId = chatId;
    flashvars.extenderTimestamp = EXTENDERTIMESTAMP;
    flashvars.urlStyleSWF = getStyleChatSWFPath();
    //flashvars.TIME_CHECK_HISTORY=TIME_CHECK_HISTORY;
    // For version detection, set to min. required Flash Player version, or 0 (or
    // 0.0.0), for no version detection.
    var swfVersionStr = "11.1.0";
    // To use express install, set to playerProductInstall.swf, otherwise the empty
    // string.
    var xiSwfUrlStr = "";
    /*
     * var flashvars={}; flashvars.id = 1; flashvars.type = "Recruiter";
     */
    // alert('tst');
    var params = {};
    params.quality = "high";
    params.bgcolor = "#ffffff";
    params.allowscriptaccess = "always";
    params.allowfullscreen = "true";
    var attributes = {};
    attributes.id = "ChatApp";
    attributes.name = "ChatApp";
    attributes.align = "middle";
    var date = new Date();
    var time = date.getTime();
    swfobject.embedSWF("chat/ChatApp.swf?version=" + time, chatFlashContentId, "100%", "100%",
            swfVersionStr, xiSwfUrlStr,
            // flashvars, params, attributes);
            flashvars, params, attributes, function () {
                setTimeout(function () {
                    $('#chatPublicLoading').css('display', 'none');
                }, 3000);
            });
    // JavaScript enabled so display the flashContent div in case it is not replaced
    // with a swf object.
    swfobject.createCSS("#" + chatFlashContentId, "display:block;text-align:left;");
}
function getStyleChatSWFPath() {
    return "chat/assets/css/chat_style.swf";
};

function factoryExportVisitors(candidates){
  $('#archiveExportFileName').val("");
  $('#downloadArchiveFile').unbind("click").bind("click", function(e){
        $('#loadingEds').css('display', 'block');
    var candidateColumnsIds = [];
        $("#candidateSortableColumns input[type=checkbox]:checked").each(function(){
            candidateColumnsIds.push(parseInt($(this).val()));
        });
    var table = createExportDataTable(candidates, candidateColumnsIds);
    $('.archiveDataTableContainer').empty();
    $('.archiveDataTableContainer').append(table);
    var separator = $("#fileSeparator").val();
    var fileName = $('#archiveExportFileName').val();
    if(fileName == "") fileName = DOWNLOADING_LABEL;
    $('#archiveExportFileName').val("");
    var result = null;
    if(separator == "excel") {
        fileName = fileName.replace(".xlsx", "").replace(".xls", "");
        $(this).attr('download', fileName + ".xls");
        result = ExcellentExport.excel(this, "archiveDataTableExport", separator);
    }else {
        fileName = fileName.replace(".csv", "");
        $(this).attr('download', fileName + ".csv");
        result = ExcellentExport.csv(this, "archiveDataTableExport", separator);
    }
        $('#loadingEds').hide();
    return result;
  });
}

function createExportDataTable(candidates, candidateColumnsIds){
    var tableHeader = "";
  var tableBody = "";
  var candidate = null;
  var userProfile = null;
    tableHeader += "<tr>";
    for(var i=0; i<candidateColumnsIds.length; i++){
        switch(candidateColumnsIds[i]){
            case candidateColumnsEnum.LAST_NAME : 
                tableHeader += "<th>" + LASTNAMELABEL + "</th>";
                break;
            case candidateColumnsEnum.FIRST_NAME : 
                tableHeader += "<th>" + FIRSTNAMELABEL + "</th>";
                break;
            case candidateColumnsEnum.EMAIL : 
                tableHeader += "<th>" + EMAIL_LABEL + "</th>";
                break;
            case candidateColumnsEnum.GENDER : 
                tableHeader += "<th>" + GENDER_LABEL + "</th>";
                break;
            case candidateColumnsEnum.CELL_PHONE : 
                tableHeader += "<th>" + CELL_PHONE_LABEL + "</th>";
                break;
            case candidateColumnsEnum.FIXE_PHONE : 
                tableHeader += "<th>" + FIXE_PHONE_LABEL + "</th>";
                break;
            case candidateColumnsEnum.PROFIL : 
                tableHeader += "<th>" + COMPANY_PROFILE_LABEL + "</th>";
                break;
            case candidateColumnsEnum.ACTIVITY_SECTOR : 
                tableHeader += "<th>" + ACTIVITYSECTORLABEL + "</th>";
                break;
            case candidateColumnsEnum.EXPERIENCE_YEARS : 
                tableHeader += "<th>" + EXPERIENCEYEARSLABEL + "</th>";
                break;
            case candidateColumnsEnum.COUNTRY_RESIDENCE : 
                tableHeader += "<th>" + COUNTRY_RESIDENCE_LABEL + "</th>";
                break;
            case candidateColumnsEnum.COUNTRY_ORIGIN : 
                tableHeader += "<th>" + COUNTRY_ORIGIN_LABEL + "</th>";
                break;
            case candidateColumnsEnum.AREA_EXPERTISE : 
                tableHeader += "<th>" + AREA_EXPERTISE_LABEL + "</th>";
                break;
            case candidateColumnsEnum.JOB_SOUGHT : 
                tableHeader += "<th>" + JOB_SOUGHT_LABEL + "</th>";
                break;
            case candidateColumnsEnum.CVS : 
                tableHeader += "<th>" + CVS_LABEL + "</th>";
                break;
            case candidateColumnsEnum.COUNTRY : 
                tableHeader += "<th>" + COUNTRY_LABEL + "</th>";
                break;
            case candidateColumnsEnum.ENTERPRISE : 
                tableHeader += "<th>" + ENTERPRISELABEL + "</th>";
                break;
            case candidateColumnsEnum.FUNCTION : 
                tableHeader += "<th>" + FUNCTIONLABEL + "</th>";
                break;
            case candidateColumnsEnum.WEB_SITE : 
                tableHeader += "<th>" + WEB_SITE_LABEL + "</th>";
                break;
            case candidateColumnsEnum.CITY : 
                tableHeader += "<th>" + CITY_LABEL + "</th>";
                break;
        }
    }
    tableHeader += "</tr>";
    candidates.sort(function(a, b){
        if(a.getUserProfile().getSecondName() > b.getUserProfile().getSecondName()) return 1;
        else if(a.getUserProfile().getSecondName() < b.getUserProfile().getSecondName()) return -1;
        else return 0;
        });
    for(var index in candidates){
    candidate = candidates[index];
        var candidateState = candidate.getCandidateState();
        if (candidateState.getCandidateStateId() != CandidateStateEnum.DISAPPROVED) {
        userProfile = candidate.getUserProfile();
        tableBody += "<tr>";
        for(var i=0; i<candidateColumnsIds.length; i++){
            switch(candidateColumnsIds[i]){
                case candidateColumnsEnum.LAST_NAME : 
                tableBody += "<td>" + htmlEncode(userProfile.getSecondName()) + "</td>";
                    break;
                case candidateColumnsEnum.FIRST_NAME : 
                tableBody += "<td>" + htmlEncode(userProfile.getFirstName()) + "</td>";
                    break;
                case candidateColumnsEnum.EMAIL : 
                tableBody += "<td>" + htmlEncode(candidate.getEmail()) + "</td>";
                    break;
                case candidateColumnsEnum.GENDER : 
                tableBody += "<td>" + htmlEncode(candidate.getGender()) + "</td>";
                    break;
                case candidateColumnsEnum.CELL_PHONE : 
                tableBody += "<td>" + htmlEncode(candidate.getCellPhone()) + "</td>";
                    break;
                case candidateColumnsEnum.FIXE_PHONE : 
                tableBody += "<td>" + htmlEncode(candidate.getPhoneNumber()) + "</td>";
                    break;
                case candidateColumnsEnum.PROFIL : 
                tableBody += "<td>" + htmlEncode(candidate.getFunctionCandidate().getFunctionName()) + "</td>";
                    break;
                case candidateColumnsEnum.ACTIVITY_SECTOR : 
                tableBody += "<td>" + htmlEncode(candidate.getNameSectors()) + "</td>";
                    break;
                case candidateColumnsEnum.EXPERIENCE_YEARS : 
                tableBody += "<td>" + htmlEncode(candidate.getExperienceYears().getExperienceYearsName()) + "</td>";
                    break;
                case candidateColumnsEnum.COUNTRY_RESIDENCE : 
                tableBody += "<td>" + htmlEncode(candidate.getRegionNames()) + "</td>";
                    break;
                case candidateColumnsEnum.COUNTRY_ORIGIN : 
                tableBody += "<td>" + htmlEncode(candidate.getCountryOrigin().getRegionName()) + "</td>";
                    break;
                case candidateColumnsEnum.AREA_EXPERTISE : 
                tableBody += "<td>" + htmlEncode(candidate.getAreaExpertise().getAreaExpertiseName()) + "</td>";
                    break;
                case candidateColumnsEnum.JOB_SOUGHT : 
                tableBody += "<td>" + htmlEncode(candidate.getJobSought().getJobSoughtName()) + "</td>";
                    break;
                case candidateColumnsEnum.CVS : 
                    var pdfCvs = candidate.getPdfCvs();
                    var pdfCvsUrl = "";
                    if(pdfCvs != null){
                        var pdfCvUrl = null;
                        for(var j=0; j < pdfCvs.length; j++){
                            pdfCvUrl = pdfCvs[j].getUrlCv();
                            pdfCvUrl = htmlEncode(pdfCvUrl);
                            pdfCvsUrl += "<a href='"+pdfCvUrl+"'>"+pdfCvUrl+"</a><br/>\n";
                        }
                    } 
                    tableBody += "<td>" + pdfCvsUrl + "</td>";
                    break;
                case candidateColumnsEnum.COUNTRY : 
                    tableBody += "<td>" + htmlEncode(candidate.getCountry().getCountryName()) + "</td>";
                    break;
                case candidateColumnsEnum.ENTERPRISE : 
                    tableBody += "<td>" + htmlEncode(candidate.getCurrentCompany()) + "</td>";
                    break;
                case candidateColumnsEnum.FUNCTION : 
                    tableBody += "<td>" + htmlEncode(candidate.getJobTitle()) + "</td>";
                    break;
                case candidateColumnsEnum.WEB_SITE : 
                    tableBody += "<td>" + htmlEncode(userProfile.getWebSite()) + "</td>";
                    break;
                case candidateColumnsEnum.CITY : 
                    tableBody += "<td>" + htmlEncode(candidate.getPlace()) + "</td>";
                    break;
            }
        }
        tableBody += "</tr>";
        firstTime = false;
    }
  }
  return "<table id='archiveDataTableExport' style='display:none;'>"
                        +"<tbody>"
                            + tableHeader
                    + tableBody
                +"</tbody>"
            +"</table>";
}

/* *****************************************************************************
 * FEE
 ****************************************************************************** */

function validateDateChamp(d) {
    if (Object.prototype.toString.call(d) === "[object Date]") {
        // it is a date
        if (isNaN(d.getTime())) {  // d.valueOf() could also work
            // date is not valid
            return false;
        }
        else {
            // date is valid
            return true;
        }
    }
    else {
        // not a date
        return false;
    }
}
;

function stringToDate(sd) {
    if (sd != null && sd != "") {
        var arrayDate = sd.split(" ");
        var d = new Date(arrayDate[0]);
        var time = arrayDate[1];
        if (time != null && time != "") {
            var timeArray = time.split(':');
            d.setHours(timeArray[0]);
            d.setMinutes(timeArray[1]);
        }
        return d;
    }
    return null;
}
;
function getAgendaUTCDateEvent(d) {
    var year = d.getUTCFullYear();
    var month = getStringElementDateNumber(d.getUTCMonth() + 1);
    var day = getStringElementDateNumber(d.getUTCDate());
    var hours = getStringElementDateNumber(d.getUTCHours());
    var minutes = getStringElementDateNumber(d.getUTCMinutes());
    var dateUTCString = [day, month, year].join('-');
    timeUTCString = [hours, minutes].join(':');
    dateUTCString = [dateUTCString, timeUTCString].join(' ');
    // console.log("**************dateUTCString :
    // "+dateUTCString+"****************");
    return dateUTCString;
}

function convertAgendaEventsData(jsonEvents) {
    var newEvents = new Array();
    for (var i = 0; i < jsonEvents.length; i++) {
        var event = jsonEvents[i];
//console.log(event);
        var newEventJson = new Object();
        var beginDateArray = event.beginDate.split('-');
        var beginDate = [beginDateArray['2'], beginDateArray['1'], beginDateArray['0']].join('-') + " " + event.beginTime;
        var beginDate = stringToDate(beginDate);
        //console.log(event);
        var endDateArray = event.endDate.split('-');
        var endDate = [endDateArray['2'], endDateArray['1'], endDateArray['0']].join('-') + " " + event.endTime;
        var endDate = stringToDate(endDate);
        newEventJson.id = event.id;
        newEventJson.title = event.ack;
        newEventJson.description = event.description;
        newEventJson.start = new Date(beginDate);
        newEventJson.end = new Date(endDate);
        newEventJson.allDay = "";
        //console.log(newEventJson);
        newEvents.push(newEventJson);
    }
    return newEvents;
}
function bindCheckMaxLength(handler, maxLength) {
    handler.unbind("keydown").bind("keydown", function (e) {
        lengthCharacters(handler, maxLength);
    });
}
function lengthCharacters(handler, maxLength) {
    if (handler.parent().find(".errorValidateField") != undefined) {
        handler.parent().find(".errorValidateField").remove();
    }
    if (handler.val().trim().length >= maxLength) {
        handler.after('<label class="error errorValidateField">Merci de ne pas depassé ' + maxLength + ' caractéres. </label>');
    }
}
;
//Custom checkboxes
function factorycheckbox() {
    $('input:checkbox').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    });
}
;
//Custom select
function addOption(selector, value, text) {
    $(selector).append('<option value=' + value + '>' + text + '</option>');
}
;
//Custom data call
function addTableDatas(selector, datas) {
    var oTable = $(selector).dataTable();
    oTable.fnClearTable();
    if (datas.length > 0) {
        oTable.fnAddData(datas);
    }
    oTable.fnDraw();
}
;

function getHtml(element) {
    if (element != null)
        return $("<div></div>").html(element).html();
};

function getAMSIp(callBack){
    genericAjaxCall('GET', staticVars.urlBackEnd+'chat/ams/get/ip', 'application/xml', 'xml', '', 
            function(xml){
                //console.log("****** IP : "+$(xml).find("s").text()+" ******");
                callBack($(xml).find("s").text());
            }, function(xhr, status, error){
            });
};


