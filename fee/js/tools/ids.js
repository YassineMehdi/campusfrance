var HOSTNAME_URL = window.location.hostname;
var staticVars = {
    urlBackEnd: "http://" + HOSTNAME_URL + "/BackEndEnterprise/rest/",
    urlBackEndCandidate: "http://" + HOSTNAME_URL + "/BackEndCandidate/rest/",
    urlvisitor: "http://" + HOSTNAME_URL + "/visitor/",
    urlexhibitor: "http://" + HOSTNAME_URL + "/exhibitor/",
    urlBackEndChat: "http://" + HOSTNAME_URL + "/BackEndChat/",
    urlServerUploader: "http://52.213.163.11:81/amazon_uploader1/php_uploader/server/php/", //"http://"+HOSTNAME_URL+":81/amazon_uploader/prx.php"
    urlServerStorage: "https://s3-eu-west-1.amazonaws.com/campusfrancestorage/",
    urlServerUpload: "http://52.213.163.11:81/amazon_uploader1/prx.php", //"http://"+HOSTNAME_URL+":81/amazon_uploader/prx.php"
    defaultImage: "http://" + HOSTNAME_URL + "/exhibitor/images/default_avatar.jpg",
    urlProfile: "http://" + HOSTNAME_URL + "/visitor/profileCandidate.html?candidateId="
};
var UPDATE_CANDIDATES_CACHE_TIME_STAMP = 3600000; //60min:3600000
var listLangsEnum = []; //cet un arrays qui contient la liste des langs ID+Name ...
var listLangsAbrEnum = []; //cet un arrays qui contient la liste des abbréviations des langues...
var localePDFJS = getLocalePDFJS();
var standId = '';
var FILE_EXTENSION_REGEX = /\.(pdf|ppt|pptx|doc|docx|xls|xlsx|avi|flv|mp4|MP4|wmv|WMV|webm|jpg|jpeg|png|PNG|gif|svg)$/i;
var DOC_EXTENSION_REGEX = /\.(pdf|ppt|pptx|doc|docx|xls|xlsx)$/i;
var VIDEO_EXTENSION_REGEX = /\.(avi|flv|mp4|MP4|wmv|WMV|webm)$/i;
var IMAGE_EXTENSION_REGEX = /\.(jpg|jpeg|png|PNG|gif|svg)$/i;
var MAX_FILE_SIZE = 6 * 1024 * 1024;
var MAX_VIDEO_SIZE = 50 * 1024 * 1024;

var cacheAjax = true;
var tchatController = null;
var tchatfeeController = null;
var listVideosCache = null;
var listPhotosCache = null;
var listDocumentsCache = null;
var listPostersCache = null;
var listAdvicesCache = null;
var listJobOffersCache = null;
var listSurveysCache = null;
var listStandsCache = null;
var listWebsitesCache = null;
var listHistoricChatCache = null;
var listActivitySectorsCache = null;
var listRegionsCache = null;
var listStudyLevelsCache = null;
var listFunctionsCandidateCache = null;
var listExperienceYearsCache = null;
var listAreaExpertisesCache = null;
var listJobsSoughtCache = null;
var listCandidateStatesCache = null;
var listJobApplicationsCache = null;
var listUnsolicitedJobApplicationsCache = null;
var listLanguageUserProfilesCache = null;
var listLanguageLevelsCache = null;
var listCountriesCache = null;
var notificationController = null;
var calendarController = null;
var alertController = null;
var conseilController = null;
var accountController = null;
var standController = null;
var profilEntrepriseController = null;
var profileSearchedController = null;
var contactController = null;
var postersController = null;
var videosController = null;
var videosRhController = null;
var documentController = null;
var photosController = null;
var documentsController = null;
var testimonyController = null;
var surveyController = null;
var visitorsController = null;
var candidatureController = null;
var inboxController = null;
var jobOfferController = null;
var languagesController = null;
var seoController = null;
var productController = null;
var questionInfoController = null;
var SEPARATEUPLOAD = "_EDSSEPEDS_";
var MAXFILESIZE = 6291456; // 6mb //6291456
var timeUploadAdvice = null;
var timeUploadLogo = null;
var timeUploadPoster = null;
var timeUploadVideo = null;
var timeUploadDocument = null;
var getRecruiterInfo = null;
var isConnectToChat = false;
var dashboardLoaded = 0;
var analyticsLoaded = 0;
var listUserFavorites = new Array();
var flashvars = {};

/******************Chat flex begin ******************/

var FMS_URL = "rtmp://:ip:/"; //50.112.243.218, 127.0.0.1, 52.49.92.74
//var FMS_URL = "rtmp://52.49.92.74/";
var FMS_APPLICATION_CHAT = "chatfmsdev";
var HISTORY_URL = "http://" + HOSTNAME_URL + ":81/HistoricChatDev/"; //192.168.1.17
var IMAGES_URL = "";
//var TIME_CHECK_HISTORY = 5*1000;//5min : 5*60*1000 A modifier dans déploiement
var flashvars = {};
var userType = "Recruiter";
var isConnectToChat = false;
var tryToReconnect = 0;
var customColor1 = "0x69085A";
var EXTENDERTIMESTAMP = 10 * 60 * 1000; //10min:600000 A modifier dans déploiement
var PUBLIC = "public";
var PRIVATE = "private";
var isDatepickerLoad = false;
var LANGUAGES_NUMBER = 2;
var MAX_LENGTH_CHARACTER = 28;

/******************Chat flex end ******************/

var fileTypeEnum = {
    POSTER: 1,
    VIDEO: 2,
    PHOTO: 3,
    DOCUMENT: 4,
    VIDEORH: 5
};

var resourceTypeEnum = {
    DOCUMENT: 1,
    VIDEO: 2,
    IMAGE: 3
};

var SurveyTypeEnum = {
    PRIVATE: '1',
    PUBLIC: '2',
};

var InputTypes = ['TEXT_AREA', 'SELECT', 'RADIO'];

var InputTypeEnum = {
    INPUT_TEXT: '1',
    TEXT_AREA: '2',
    SELECT: '3',
    RADIO: '4',
    FILE: '5'
};

var FormTypeEnum = {
    REGISTER: '1',
    SURVEY: '2',
};

var PageEnum = {
    BACKEND: "Backend",
    LOGIN: "login",
    CHAT: "chat"
};

var rightGroupEnum = {
    ADVICES: 2,
    JOB_OFFERS: 5,
    POSTERS: 6,
    VIDEOS: 7,
    FORUM_PUBLICATIONS: 8,
    STAND_PUBLICATIONS: 9,
    DOCUMENTS: 10,
    PHOTOS: 11,
    TESTIMONIES: 12,
    SURVEYS: 13,
    INBOX: 14,
    VIDEOS_RH: 15,
    FORUM_DASHBOARD: 16,
    STAND_DASHBOARD: 17,
    SOLICITED_JOB_APPLICATION: 18,
    UN_SOLICITED_JOB_APPLICATION: 19,
    REGISTER_VISITORS: 20,
    FORUM_VISITORS: 21,
    STAND_VISITORS: 22,
    FAVORITS_VISITORS: 23,
    PRODUCTS: 24,
    QUESTION_INFO: 25,
    VISIT_CARDS_VISITORS: 26,
    EXHIBITORS: 27
};
var PageEnum = {
    BACKEND: "Backend",
    LOGIN: "login",
    CHAT: "chat"
};

var CandidateStateEnum = {
    REGISTER: '1',
    APPROVED: '2',
    DISAPPROVED: '3',
    UPDATED: '4',
    OLD: '5',
};

var FavoriteEnum = {
    MEDIA: "AllFiles",
    JOBOFFER: "JobOffer",
    CONTACT: "Contact",
    ADVICE: "Advice",
    TESTIMONY: "Testimony",
    STAND: "Stand",
    USER: "User"
};

var ALL = "all";

var candidateColumnsEnum = {
    LAST_NAME: 1,
    FIRST_NAME: 2,
    EMAIL: 3,
    GENDER: 4,
    CELL_PHONE: 5,
    FIXE_PHONE: 6,
    PROFIL: 7,
    ACTIVITY_SECTOR: 8,
    EXPERIENCE_YEARS: 9,
    COUNTRY_RESIDENCE: 10,
    COUNTRY_OIGIN: 11,
    AREA_EXPERTISE: 12,
    JOB_SOUGHT: 13,
    CVS: 14
};
var listStandRights = null;
/*******************Agenda begin********************/
var forumId = 1;
/*******************traduction terms****************/
var currentEnterpriseP = null;
var VIEWLABEL = "";
var EDITLABEL = "";
var DELETELABEL = "";
var CONNECTTOCHATLABEL = "";
var SKYPEINVITATIONLABEL = "";
var APPROVELABEL = "";
var DISAPPROVELABEL = "";
var DISCONNECTFROMCHATLABEL = "";
var SAVELABEL = "";
var CANCELLABEL = "";
var PHOTOLABEL = "Photo";
var FIRSTANDLASTNAMELABEL = "Prénom et Nom";
var JOBTITLELABEL = "Intitulé du poste";
var TESTIMONYLABEL = "Témoignage";
var ACTIONSLABEL = "Actions";
var RESPONSELABEL = "";
var QUESTIONLABEL = "";
var CHARGEMENTLABEL = "";
var ADDLABEL = "";
var SAVECHANGESLABEL = "";
var LEFTCARACTERES = "";
var REMOVELABEL = "";
var REFERENCELABEL = "";
var TITLELABEL = "Titre";
var DESCRIPTIONLABEL = "";
var SKILLSLABEL = "";
var REQUIREDLABEL = "";
var DATEINVALIDLABEL = "";
var OKLABEL = "";
var FILESIZENOTEXCEEDLABEL = "";
var CONTENTLABEL = "Contenu";
var PUBLICCHATLABEL = "";
var PRIVATECHATLABEL = "";
var LANGAUGETAGLOCALELABEL = "";
var LANGUAGECURRENTLABEL = "";
var LANGUAGEDATEPICKERFORMATLABEL = "";
var COMPANY_PROFILE_LABEL = "";
var OPENLABEL = "";
var AGELABEL = "";
var TOTALLABEL = "";
var CANTONLABEL = "";
var STUDYLEVELLABEL = "";
var FUNCTIONLABEL = "";
var EXPERIENCEYEARSLABEL = "";
var ACTIVITYSECTORLABEL = "";
var USERLABEL = "";
var VISITSTOTALLABEL = "";
var FIRSTVISITLABEL = "";
var LASTVISITLABEL = "";
var AVGVISITSLABEL = "";
var VISITSLABEL = "";
var LANGUAGELABEL = "";
var LANGUAGESLABEL = "";
var COUNTRYTERRITORYLABEL = "";
var ERRORCONNECTIONLABEL = "";
var ERRORACCOUNTLABEL = "";
var CITYLABEL = "";
var CITIESLABEL = "";
var BROWSERLABEL = "";
var BROWSERSLABEL = "";
var OPERATINGSYSTEMSLABEL = "";
var OPERATINGSYSTEMLABEL = "";
var DAYLABEL = "";
var HOURLABEL = "";
var BOUNCESLABEL = "";
var NEWVISITSLABEL = "";
var AVGTIMEONSITELABEL = "";
var PHOTOSLABEL = "";
var ENTERPRISELABEL = "";
var NUMBERVIEWSLABEL = "";
var DOCUMENTSLABEL = "";
var VIDEOSLABEL = "";
var POSTERSLABEL = "";
var ADVICESLABEL = "";
var ACCEPT_PRODUCT_RESOURCE = "";
var JOBOFFERSLABEL = "";
var SURVEYLABEL = "";
var EXCHANGE_VISIT_CARD_LABEL = "";
var DATELABEL = "";
var AREALABEL = "";
var AREAVIEWSLABEL = "";
var UNIQUEAREAVIEWSLABEL = "";
var SEEPROFILELABEL = "";
var SEECVLABEL = "";
var SEELETTERLABEL = "";
var NEWLABEL = "";
var ATLEASTONELABEL = "";
var VISITORSLABEL = "";
var VISITORTYPELABEL = "";
var AVGTIMEAREAVIEWSLABEL = "";
var TRAFFICSOURCESLABEL = "";
var DEMOGRAPHICSDATALABEL = "";
var OVERVIEWLABEL = "";
var PROFILELABEL = "";
var VISITSBYFUNCTIONLABEL = "";
var NEWVISITORSVSRETURNINGVISITORSLABEL = "";
var BOOTHVISITORSLABEL = "";
var WEBSITESLABEL = "";
var TESTIMONIESLABEL = "";
var APPLYLABEL = "";
var SUBMITCVLABEL = "";
var SENDMESSAGELABEL = "";
var CVSUBMITTEDLABEL = "";
var DATEOFAPPLICATIONLABEL = "";
var SENDLABEL = "";
var YOUDONTHAVERIGHTTODOMODIF = "";
var FIRSTNAMELABEL = "";
var LASTNAMELABEL = "";
var ADDTOFAVORITELABEL = "";
var DELETEFROMFAVORITELABEL = "";
var VIEWPROFILELABEL = "";
var CHOOSELABEL = "";
var VIEWCVLABEL = "";
var VIEWLETTERLABEL = "";
var TRADUCTLABEL = "";
var TRADUCTIONLABEL = "";
var ACTIVITYSECTORSLABEL = "";
var RETURNLABEL = "";
var URLLABEL = "";
var SURVEYSLABEL = "";
var CVSSUBMITEDNBRLABEL = "";
var QUESTIONSLABEL = "";
var ALLLABEL = "";
var REPLACELANGUAGELABEL = "";
var LANGUAGELEVELLABEL = "";
var NUMBEROFCHOICES = "";
var UNSOLICITEDAPPLICATIONLABEL = "";
var NEWLABEL = "";
var NEWMESSAGELABEL = "";
var UPDATEDLABEL = "";
var STATELABEL = "";
var NOTUPDATEDLABEL = "";
var EMAIL_LABEL = "";
var GENDER_LABEL = "";
var CELL_PHONE_LABEL = "";
var FIXE_PHONE_LABEL = "";
var PROFILLABEL = "";
var COUNTRY_RESIDENCE_LABEL = "";
var COUNTRY_OIGIN_LABEL = "";
var AREA_EXPERTISE_LABEL = "";
var JOB_SOUGHT_LABEL = "";
var GENDER_LABEL = "";
var CVS_LABEL = "";
var DOWNLOADING_LABEL = "";
var SHARE_WITH_FRIEND_LABEL = "";
var CV_LABEL = "";
var PROFIL_LABEL = "";
var LETTER_LABEL = "";
var SHARE_WITH_FRIEND_LABEL = "";
var ARCHIVETOOLTIPLABEL = "";
var MOVETOOLTIPLABEL = "";
var DELETETOOLTIPLABEL = "";
var LOGO_LABEL = "";
var TYPE_LABEL = "";
var ATTACH_FILES = "";
var LANGUAGENAME = ":LANGUAGENAME";
var VIEW_LINKS_LABEL = "";
var DATE_FORMAT = "";
/******************end traduction term*****************/
var nbreLoginWSCallsInProgress = 0;