jQuery.extend({
    CandidatureModel: function () {
        var that = this;
        var listeners = new Array();
        this.initModel = function () {
            that.getUnsolicitedJobApplications();
            that.getJobApplications();
            that.getStandJobOffers();
        };

        this.getStandJobOffersSuccess = function (xml) {
            var jobOffers = [];
            $(xml).find("joboffer").each(function () {
                var jobOffer = new $.JobOffer($(this));
                jobOffers[jobOffer.getJobId()] = jobOffer;
            });
            that.notifyLoadJobOffers(jobOffers);
        };

        this.getStandJobOffersError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.getStandJobOffers = function () {
            genericAjaxCall('GET', staticVars.urlBackEnd
                    + 'jobOffer/standJobOffers?idLang=' + getIdLangParam(), 'application/xml', 'xml', '',
                    that.getStandJobOffersSuccess, that.getStandJobOffersError);
        };
        /**
         *  Get job application
         *                                      */
        this.getJobApplicationsSuccess = function (xml) {
            var listJobApplicationsCache = new Array();
            $(xml).find("jobApplication").each(function () {
                var jobApplication = new $.JobApplication($(this));
                listJobApplicationsCache[jobApplication.getJobApplicationId()] = jobApplication;
            });
            that.notifyLoadJobApplication(listJobApplicationsCache);
        };

        this.getJobApplicationsError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.getJobApplications = function () {
            genericAjaxCall('GET', staticVars.urlBackEnd
                    + 'jobApplication/jobApplications',
                    'application/xml', 'xml', '',
                    that.getJobApplicationsSuccess, '');
        };

        /**
         *  Get unsolicited job application
         *                                      */
        this.getUnsolicitedJobApplicationsSuccess = function (xml) {
            listUnsolicitedJobApplicationsCache = new Array();
            $(xml).find("unsolicitedJobApplication").each(
                    function () {
                        var unsolicitedJobApplication = new $.UnsolicitedJobApplication($(this));
                        listUnsolicitedJobApplicationsCache[unsolicitedJobApplication.getJobApplicationId()] = unsolicitedJobApplication;
                    });
            that.notifyLoadUnsolicitedJobApplication(listUnsolicitedJobApplicationsCache);
        };

        this.getUnsolicitedJobApplicationsError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.getUnsolicitedJobApplications = function () {
            genericAjaxCall('GET', staticVars.urlBackEnd
                    + 'jobApplication/unSolicitedJobApplications',
                    'application/xml', 'xml', '',
                    that.getUnsolicitedJobApplicationsSuccess, that.getUnsolicitedJobApplicationsError);
        };

        /**
         *  update job application
         *                                      */

        this.sendMessageSuccess = function () {
            that.notifySendMessageUnsolicitedJob();
        };

        this.sendMessageError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.updateJobApplication = function (jobApplication) {
            genericAjaxCall('POST', staticVars.urlBackEnd
                    + 'jobApplication/updateJobApplication?idLang=' + getIdLangParam(), 'application/xml', 'xml',
                    jobApplication.xmlData(), that.sendMessageSuccess, that.sendMessageError);
        };

        /**
         *  update unsolicited job application
         *
         *                                                                            */
        this.sendMessageUnsolicitedJobSuccess = function () {
            that.notifySendMessageUnsolicitedJob();
        };

        this.sendMessageUnsolicitedJobError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.updateUnsolicitedJobApplication = function (unsolicitedJobApplication) {
            genericAjaxCall('POST', staticVars.urlBackEnd
                    + 'jobApplication/updateUnsolicitedJobApplication', 'application/xml', 'xml',
                    unsolicitedJobApplication.xmlData(), that.sendMessageUnsolicitedJobSuccess, that.sendMessageUnsolicitedJobError);
        };

        /**
         *  update unsolicited job application
         *                                      */
        this.readUnsolicitedJobApplicationSuccess = function (xml) {
        };

        this.readUnsolicitedJobApplicationError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.readUnsolicitedJobApplication = function (unsolicitedJobApplicationId) {
            genericAjaxCall('POST', staticVars.urlBackEnd + 'jobApplication/readUnsolicitedJobApplication?unsolicitedJobApplicationId=' + unsolicitedJobApplicationId,
                    'application/xml', 'xml', '', that.readUnsolicitedJobApplicationSuccess, that.readUnsolicitedJobApplicationError);
        };

        /**
         *  update job application
         *                                      */
        this.readJobApplicationSuccess = function (xml) {
        };

        this.readJobApplicationError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.readJobApplication = function (jobApplicationId) {
            genericAjaxCall('POST', staticVars.urlBackEnd + 'jobApplication/readJobApplication?jobApplicationId=' + jobApplicationId,
                    'application/xml', 'xml', '', that.readJobApplicationSuccess, that.readJobApplicationError);
        };

        this.addListener = function (list) {
            listeners.push(list);
        };

        this.notifyLoadJobOffers = function (jobOffers) {
            $.each(listeners, function (i) {
                listeners[i].loadCareerJobOffers(jobOffers);
            });
        };
        this.notifyLoadJobApplication = function (listJobApplicationsCache) {
            $.each(listeners, function (i) {
                listeners[i].loadJobApplication(listJobApplicationsCache);
            });
        };

        this.notifyLoadUnsolicitedJobApplication = function (listUnsolicitedJobApplicationsCache) {
            $.each(listeners, function (i) {
                listeners[i].loadUnsolicitedJobApplication(listUnsolicitedJobApplicationsCache);
            });
        };

        this.notifySendMessageUnsolicitedJob = function () {
            $.each(listeners, function (i) {
                listeners[i].sendMessageUnsolicitedJobSuccess();
            });
        };

    }
    ,
    CandidatureModelListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }
});
