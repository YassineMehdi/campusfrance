jQuery.extend({
    CandidatureController: function (model, view) {
        var that = this;
        /**
         * listen to the view
         */
        var vlist = $.CandidatureViewListener({
            updateJobApplication: function (messageXml) {
                model.updateJobApplication(messageXml);
            },
            updateUnsolicitedJobApplication: function (messageXml) {
                model.updateUnsolicitedJobApplication(messageXml);
            },
            readJobApplication : function(jobApplicationId) {
                model.readJobApplication(jobApplicationId);
            },
            readUnsolicitedJobApplication : function(unsolicitedJobApplicationId) {
                model.readUnsolicitedJobApplication(unsolicitedJobApplicationId);
            },
        });
        view.addListener(vlist);

        /**
         * listen to the model
         */
        var mlist = $.CandidatureModelListener({
            loadCareerJobOffers: function (jobOffers) {
                view.displayJobOffers(jobOffers);
            },
            loadJobApplication: function (list) {
                view.displayJobApplications(list);
            },
            loadUnsolicitedJobApplication: function (list) {
                view.displayUnsolicitedJobApplications(list);
            },
            sendMessageUnsolicitedJobSuccess: function () {
                view.sendMessageUnsolicitedJobSuccess();
            }
        });

        model.addListener(mlist);

        this.initController = function () {
            view.initView();
            model.initModel();
        };
    }
});
