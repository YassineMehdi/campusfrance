jQuery.extend({
    DocumentsController: function (model, view) {
        var that = this;
        /**
         * listen to the view
         */
        var vlist = $.DocumentsViewListener({
            addDocument: function (document) {
                model.addDocument(document);
            },
            deleteDocument: function (documentId) {
                model.deleteDocument(documentId);
            },
            updateDocument: function (document) {
                model.updateDocument(document);
            }
        });

        view.addListener(vlist);

        /**
         * listen to the model
         */
        var mlist = $.DocumentsModelListener({
            loadDocuments: function (listDocuments) {
                view.displayDocuments(listDocuments);
            },
            addDocumentSuccess: function () {
                view.addDocumentSuccess();
            },
            deleteDocumentSuccess: function () {
                view.deleteDocumentSuccess();
            },
            updateDocumentSuccess: function () {
                view.updateDocumentSuccess();
            }
        });

        model.addListener(mlist);

        this.getAnalyticsDocuments = function(callBack, path) {
            model.getAnalyticsDocuments(callBack, path);
        };
        this.getDocumentById = function(documentId) {
            return model.getDocumentById(documentId);
        };
        this.initController = function () {
            view.initView();
            model.initModel();
        };
    }
});
