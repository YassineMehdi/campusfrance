jQuery.extend({
    JobOfferController: function (model, view) {
        var that = this;
        /**
         * listen to the view
         */
        var vlist = $.JobOfferViewListener({
            addJobOffer: function () {
            	model.addJobOffer();
            },
            updateJob: function (jobOfferXml) {
            	model.updateJoboffer(jobOfferXml);
            },
            saveJob : function(jobOfferXml) {
                model.addJoboffer(jobOfferXml);
            },
            deleteJobOffer : function(jobOfferId) {
                model.deleteJobOffer(jobOfferId);
            },
        });
        view.addListener(vlist);

        /**
         * listen to the model
         */
        var mlist = $.JobOfferModelListener({
            loadJobOffers: function (jobOffers) {
            	view.displayJobOffers(jobOffers);
            },
            addJobOfferSuccess: function () {
            	view.addJobOfferSuccess();
            },
            updateJobOfferSuccess: function () {
            	view.updateJobOfferSuccess();
            },
            deleteJobOfferSuccess: function () {
            	view.deleteJobOfferSuccess();
            },
            jobOfferSaved : function(jobOffer) {
                view.refresh();
            },
            jobOfferDeleted : function() {
                view.jobOfferDeleted();
            },
        });

        model.addListener(mlist);

        this.getAnalyticsJobOffers = function(callBack, path) {
            model.getAnalyticsJobOffers(callBack, path);
        };
        
        this.getJobOfferById = function(jobOfferId) {
            return model.getJobOfferById(jobOfferId);
        };

        this.initController = function () {
            view.initView();
            model.initModel();
        };
    }
});

