jQuery.extend({
    LanguagesView: function () {
        var that = this;
        var listeners = new Array();
        var sustitutionsTBodyHandler = null;
        var substitutionArray = new Array();
        var sustitutionsTBodyHandler = null;
        var saveSubstitutionButtonHandler = null;

        this.initView = function () {
            traduct();
            that.sustitutionsTBodyHandler = $('#sustitutionsTBody');
            that.saveSubstitutionButtonHandler = $('#saveSubstitutionButton');
            that.getFilledLanguages();
            that.saveSubstitutionButtonHandler.unbind("click").bind("click", function (e) {
                var languagesSubstitutionXml = "";
                for ( var languageId in listLangsEnum) {
                    languagesSubstitutionXml = languagesSubstitutionXml
                            + that.factoryLanguageSubstitutionXML(languageId);
                }

                languagesSubstitutionXml = "<languagesSubstitution>"
                        + languagesSubstitutionXml + "</languagesSubstitution>";
                that.notifyUpdateSubstitutions(languagesSubstitutionXml);
            });
        };
        this.getSubstitutionsSuccess = function (xml) {
            $(xml).find("languageSubstitution").each(
                function() {
                    var languageId = $(this).find('idlangage').text();
                    var idLanguageSubstitution = $(this).find('idLanguageSubstitution').text();
                    that.factoryUpdateSustitutionLang(languageId,idLanguageSubstitution);
                    substitutionArray[languageId] = $(this)
                            .find('idLanguageSubstitution').text();
                    that.updateViewSubs();
                });
            $('#loadingEds').hide();
        };

        this.factoryUpdateSustitutionLang = function(languageId,idLanguageSubstitution) {
            var substituteLanguageSelectHandler = $('#substitute' + languageId
                    + 'Select');
            var substituteLanguageHandler = $('#substitute' + languageId);
            substituteLanguageSelectHandler.val(idLanguageSubstitution);
            substituteLanguageSelectHandler.prop('disabled', false);
            substituteLanguageHandler.prop('checked', 'checked');
        };
        this.factoryLanguageSubstitutionXML = function (languageId) {
            var substituteSelectId = "substitute" + languageId + "Select";
            var substituteSelectHandler = $("#" + substituteSelectId);
            var substituteCheckBoxHandler = $("#substitute" + languageId);
            if (document.getElementById(substituteSelectId)
                    && substituteCheckBoxHandler.is(':checked')
                    && substituteSelectHandler.has('option').length > 0)
                return "<languageSubstitution><idlangage>" + languageId
                        + "</idlangage><idLanguageSubstitution>"
                        + substituteSelectHandler.val()
                        + "</idLanguageSubstitution></languageSubstitution>";
            else
                return ""
        };
        this.getFilledLanguages = function() {
            for ( var languageId in listLangsEnum) {
                that.sustitutionsTBodyHandler.append(that.buildSubstitutionXML(languageId,
                        listLangsEnum[languageId]));
                that.factorySubstituteCheckBoxClick(languageId);
                that.factorySubstituteSelectChange(languageId);
            }
            for ( var languageId in listLangsEnum) {
                for ( var languageId2 in listLangsEnum) {
                    if (languageId != languageId2) {
                        $('#substitute' + languageId + 'Select').prepend(
                                "<option value=" + languageId2
                                            + ">"
                                        + listLangsEnum[languageId2] + "</option>");
                    }
                }
                that.hideSubsitutionTrByLangId(languageId);
            }
            that.notifyGetSubstitutions();
        };

        this.factorySubstituteCheckBoxClick = function(languageId) {
            var substituteSelectId = "substitute" + languageId + "Select";
            if (document.getElementById(substituteSelectId) != null) {
                var substituteSelectSelector = "#" + substituteSelectId;
                var substituteSelectHandler = $(substituteSelectSelector);
                var substituteCheckBoxSelector = "#substitute" + languageId;
                var substituteCheckBoxHandler = $(substituteCheckBoxSelector);
                enabledButton(substituteCheckBoxSelector);
                substituteCheckBoxHandler.click(function() {
                    if (!isDisabled(substituteCheckBoxSelector)) {
                        if (substituteCheckBoxHandler.is(':checked')) {
                            that.ckeckUpdateRightSubstitution(languageId, substituteSelectHandler
                                    .val(), function(xml) {
                                substituteSelectHandler.prop('disabled', false);
                                //substituteSelectHandler.data("previous-value", substituteSelectHandler.val());
                                //updateViewSub(substituteSelectHandler.data("previous-value"), substituteSelectHandler.val());
                                substitutionArray[languageId] = substituteSelectHandler.val();
                                that.updateViewSubs();
                            }, function(xhr, status, error) {
                                //substituteCheckBoxHandler.click();
                                infoMessage('#langError');
                                substituteCheckBoxHandler.prop("checked", false);
                            });
                        } else {
                            that.ckeckUpdateRightSubstitution(languageId, 0, function(xml) {
                                substituteSelectHandler.prop('disabled', true);
                                //updateViewSub(substituteSelectHandler.val(), 0);
                                delete substitutionArray[languageId];
                                that.updateViewSubs();
                            }, function(xhr, status, error) {
                                //substituteCheckBoxHandler.click();
                                infoMessage('#langError');
                                substituteCheckBoxHandler.prop("checked", true);
                                isSessionExpired(xhr.responseText);
                            });
                        }
                        disabledButton(substituteCheckBoxSelector);
                        setTimeout(function() {
                            enabledButton(substituteCheckBoxSelector);
                        }, 3000);
                    } else {
                        substituteCheckBoxHandler.prop("checked", !substituteCheckBoxHandler
                                .is(':checked'));
                    }
                });
            }
        };
        this.ckeckUpdateRightSubstitution = function(languageId, idLanguageSubstitution,
            callBackSuccess, callBackError) {
            var languageSubstitutionXml = "<languageSubstitution><idlangage>"
                    + languageId + "</idlangage><idLanguageSubstitution>"
                    + idLanguageSubstitution
                    + "</idLanguageSubstitution></languageSubstitution>";
            //console.log(languageSubstitutionXml);
            genericAjaxCall('POST', staticVars.urlBackEnd
                    + 'languaget/checkUpdateSubstitution', 'application/xml', 'xml',
                    languageSubstitutionXml, callBackSuccess, callBackError);
        };
        this.factorySubstituteSelectChange = function(languageId) {
            if (document.getElementById("substitute" + languageId + "Select") != null) {
                var substituteSelectHandler = $("#substitute" + languageId + "Select");
                var substituteCheckBoxHandler = $("#substitute" + languageId);
                substituteSelectHandler.change(function() {
                    //var previousValue = $(this).data("previous-value");
                    if (substituteCheckBoxHandler.is(':checked')) {
                        //console.log("languageId : "+languageId+', SubsLanguageId : '+substituteSelectHandler.val()+', before SubsLanguageId : '+substituteSelectHandler.data("previous-value"));
                        //updateViewSub(substituteSelectHandler.data("previous-value"), substituteSelectHandler.val());
                        substitutionArray[languageId] = substituteSelectHandler.val();
                        that.updateViewSubs();
                    }
                    //substituteSelectHandler.data("previous-value", substituteSelectHandler.val());
                });
            }
        };

        this.buildSubstitutionXML = function (languageId, languageName) {
            var container = $('<tr id="substituteTr'+languageId+'">'
                    + '<td style="font-size: 13px;">'
                    + languageName
                    + '</td>'
                    + '<td><input type="checkbox" class="substitute'+languageId+'" id="substitute'+languageId+'" />'
                    + '<label class="substitute'+languageId+' substituteLabel" for="substitute'+languageId+'" style="display: inline;position: relative;bottom:1px;"></label></td>'
                    + '<td style="text-align:left; ">'
                    + '<span style="font-size: 12px;width: 25%;" class="replace'
                    + languageId
                    + 'Label">'
                    + REPLACELANGUAGELABEL.replace(LANGUAGENAME, languageName)
                    + '</span>'
                    + '</td>'
                    + '<td style="text-align:left; ">'
                    + '<select disabled="disabled" class="substitueSelect  form-control" id="substitute'+languageId+'Select">'
                    + '</select>' + '</td>' + '</tr>');
            return container;
        };
        this.idLangSubstInArray = function(idLanguageSubstitution) {
            return jQuery.inArray(idLanguageSubstitution, substitutionArray);
        };
        this.hideSubsitutionTrByLangId = function(idlanguage) {
            if ($('#substitute' + idlanguage + 'Select').find("option").text() == "")
                $('#substituteTr' + idlanguage).hide();
        };
        this.updateViewSubs = function() {
            for ( var languageId in listLangsEnum) {
                that.updateViewSub(languageId);
            }
            for ( var languageId in listLangsEnum) {
                that.hideSubsitutionTrByLangId(languageId);
            }
        };
        this.updateViewSub = function(idLanguageSubstitution) {
            //console.log("updateViewSub, idLanguageSubstitution : "+idLanguageSubstitution+", idLangSubstInArray : "+idLangSubstInArray(idLanguageSubstitution));
            var substituteTrHandler = $("#substituteTr" + idLanguageSubstitution);
            if (that.idLangSubstInArray(idLanguageSubstitution) != -1)
                substituteTrHandler.hide();
            else
                substituteTrHandler.show();
            for ( var languageId in listLangsEnum) {
                that.removeOptionByTagName(idLanguageSubstitution, languageId);
            }
        };
        this.removeOptionByTagName = function(idLanguageSubstitution, languageId) {
            if (languageId != idLanguageSubstitution) {
                var substituteOptionSelector = "#substitute" + languageId+ "Select option[value='" + idLanguageSubstitution + "']";
                if (substitutionArray[idLanguageSubstitution] != null) {
                    //console.log("removeOptionByTagName, idLanguageSubstitution : "+idLanguageSubstitution+", languageId : "+languageId+", languageTagName:"+languageTagName);
                    $(substituteOptionSelector).hide();
                    var substituteSelectHandler = $("#substitute" + languageId + "Select");
                    if (substituteSelectHandler.val() == idLanguageSubstitution) {
                        substituteSelectHandler.find("option").each(function() {
                            if (this.value != idLanguageSubstitution) {
                                $(this).attr('selected', 'selected')
                            }
                        });
                    }
                } else
                    $(substituteOptionSelector).show();
            }
        };
        this.addListener = function (list) {
            listeners.push(list);
        };
        this.notifyGetSubstitutions = function() {
            $.each(listeners, function (i) {
                listeners[i].getSubstitutions();
            });
        };
        this.notifyUpdateSubstitutions = function(languagesSubstitutionXml) {
            $.each(listeners, function (i) {
                listeners[i].updateSubstitutions(languagesSubstitutionXml);
            });
        };
    },
    AccountViewListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }



});
