jQuery.extend({
    LanguagesController: function (model, view) {
        var that = this;
        /**
         * listen to the view
         */
        var vlist = $.AccountViewListener({
            getSubstitutions: function () {
                model.getSubstitutions();
            },
            updateSubstitutions: function (languagesSubstitutionXml) {
                model.updateSubstitutions(languagesSubstitutionXml);
            },
        });

        view.addListener(vlist);
        /**
         * listen to the model
         */
        var mlist = $.AccountModelListener({
            getSubstitutionsSuccess: function (xml) {
                view.getSubstitutionsSuccess(xml);
            },
        });

        model.addListener(mlist);

        this.initController = function () {
            view.initView();
            model.initModel();
        };
    }
});
