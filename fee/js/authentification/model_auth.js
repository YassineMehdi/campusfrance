jQuery.extend({
    EnterpriseP: function (data) {
        that = this;
        this.login = $(data).find("login").text();
        this.password = $(data).find("password").text();
        this.getLogin = function () {
            return that.login;
        };
        this.setLogin = function (login) {
            that.login = login;
        };
        this.getPassword = function () {
            return that.password;
        };
        this.setPassword = function (password) {
            that.password = password;
        };
        this.xmlData = function () {
            var xml = '<enterprisep>';
            xml += '<login>' + that.login + '</login>';
            xml += '<password>' + that.password + '</password>';
            xml += '</enterprisep>';
            return xml;
        };
    },
    AuthentificationModel: function () {
        var that = this;
        var listeners = new Array();
        this.initModel = function () {

        };

        this.authentificateSuccess = function (xml) {
            //document.location.href = "settings.html";
            var status = $(xml).find('status').text();
            if (status == "success") {
                $.cookie("enterpriseID", $(xml).find('enterpriseID')
                        .text());
                $.cookie("groupId", $(xml).find('groupId').text());
                $.cookie("enterprisepId", $(xml).find('enterprisepId')
                        .text());
                $.cookie("standId", $(xml).find('standId').text());
                var token = $(xml).find('token').text();
                if (token == null || token == "null")
                    token = "";
                $.cookie("token", token);
                if ($(xml).find('isAdmin').text() == 'true')
                    window.location.replace("index.html?idLang=" + getIdLangParam());
                else
                    window.location.replace("chats.html?idLang=" + getIdLangParam());

                //$('#loadingEds').hide();
            } else {
                that.notifyAuthentificationError();
            }
        };

        this.authentificateError = function (xhr, status, error) {
            //$.cookie('keeping', false);
        };

        this.authentificate = function (login, pwd) {
            //$.cookie('keeping', loginkeeping);
            pwd = $.sha1(pwd);
            var enterpriseP = new $.EnterpriseP();
            enterpriseP.setLogin(login);
            enterpriseP.setPassword(pwd);
            var url = staticVars.urlBackEnd + 'enterprisep/login';
            genericAjaxCall('POST', url, 'application/xml', 'xml', enterpriseP.xmlData(),
                    that.authentificateSuccess, that.authentificateError)
        };

        this.addListener = function (list) {
            listeners.push(list);
        };
        this.notifyAuthentificationError = function () {
            $.each(listeners, function (i) {
                listeners[i].authentificationError();
            });
        };
    }
    ,
    ModelListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }
});
