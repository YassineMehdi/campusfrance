jQuery.extend({
    TchatfeeView: function () {
        var that = this;
        var listeners = new Array();
        var dateBeginHistoryChatHandler = null;
        var dateEndHistoryChatHandler = null;
        var typeHistoryChatHandler = null;
        var languageHistoryChatHandler=null;
        var myVarInter=null;
		var chatComponentHandler = $('#chatComponent');

        this.initView = function () {
              $('.closePopup').click(function (e) {
                  e.preventDefault();
                  $('.eds-dialog-container').hide();
                  $('.backgroundPopup').hide();
              });

              $('#chatSettings a.chat').click(function (e) {
                  e.preventDefault();
                  if (!isSettingsFilled()) {
                      $('.fillSettingsPopup').modal();
                      //factoryChangeChatTab('#settingsLi', '#settingsTab');
                  } else {
                    if(!isConnectToChat) $('#chatComponent').empty();
                      factoryChangeChatTab('#chatLi', '#chatTab');
                  }
              });

              $('#chatSettings a.settings').click(function (e) {
                  e.preventDefault();
                  factoryChangeChatTab('#settingsLi', '#settingsTab');
              });

              $('#chatSettings a.moderationChat').click(function (e) {
                  e.preventDefault();
                  factoryChangeChatTab('#moderationChatLi', '#moderationChatTab');
                  $('#loadLogoRecruiterError').hide();
              });

              $('#chatSettings a.historicChat').click(function (e) {
                    var dateBeginHistoryChatValue = dateBeginHistoryChatHandler.val();
                    var dateEndHistoryChatValue = dateEndHistoryChatHandler.val();
                    e.preventDefault();
                    factoryChangeChatTab('#historicChatLi', '#historicChatTab');
                    that.notifyGetHistoryChat(dateBeginHistoryChatValue,dateEndHistoryChatValue);
              });
              $('#progressLogoRecruiterUpload,#loadLogoRecruiterError').hide();
                $('#logoRecruiterUpload').fileupload({
                    beforeSend : function(jqXHR, settings) {
                        beforeUploadFile('.saveChatSettingsButton', '#progressLogoRecruiterUpload', '#chatPage .filebutton,#loadLogoRecruiterError');
                    },
                    datatype: 'json',
                    cache : false,
                    add: function (e, data) {
                        var goUpload = true;
                        var uploadFile = data.files[0];
                        var fileName=uploadFile.name;
                        timeUploadLogoRecruiter=(new Date()).getTime();
                        data.url = getUrlPostUploadFile(timeUploadLogoRecruiter, fileName);
                        if (!(/\.(gif|jpg|jpeg|tiff|png)$/i).test(fileName)) {
                            goUpload = false;
                        }
                        if (uploadFile.size > MAXFILESIZE) { // 6mb
                            goUpload = false;
                        }
                        if (goUpload == true) {
                            data.submit();
                        }else $('#loadLogoRecruiterError').show();
                    },
                    progressall: function (e, data) {
                        progressBarUploadFile('#progressLogoRecruiterUpload', data, 98);
                    },
                    done: function (e, data) {
                        var file=data.files[0];
                        var fileName=file.name;
                        var hashFileName=getUploadFileName(timeUploadLogoRecruiter, fileName);
                        console.log("Add logo recruiter chat done, filename :"+fileName+", hashFileName : "+hashFileName);
                        enterprisepPhoto = getUploadFileUrl(hashFileName);
                        $('#recruiterPicture').attr('src', enterprisepPhoto);
                        afterUploadFile('.saveChatSettingsButton', '#chatPage .filebutton', '#progressLogoRecruiterUpload');
                    }
                });
                getRecruiterInfosChat();
                $('.saveChatSettingsButton').click(function () {
                    if ($("#accountForm").valid() && !isDisabled('.saveChatSettingsButton')) {
                        $('#loadingEds').show();
                        sendRecruiterInfosChat();
                    }
                });

                $('#connectToChatButton').click(function () {
                    if (!FlashDetect.installed){
                        var pageHost = ((document.location.protocol == "https:") ? "https://" : "http://"); 
                        $('#chatComponent').html("<a href='http://www.adobe.com/go/getflashplayer' target='_blank'><img src='" 
                        + pageHost + "www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Télécharger Adobe Flash player' /></a>" );
                    }else {
                        if (!isConnectToChat) {
				    		that.notifyGetCurrentsChat();
                            //connectToChatFlash();
                        } else {
                            isConnectToChat = false;
                            $('#connectToChatButton').empty();
                            $('#connectToChatButton').append('<i class="fui-chat"></i>'+CONNECTTOCHATLABEL+'');
                            $('#connectToChatButton').removeClass("disconnected");
                            $('#chatComponent').empty();
                        }
                    }

                });
                validateAccountForm();
        };
        
        this.initHistory = function () {
            dateBeginHistoryChatHandler = $('#dateBeginHistoryChat');
            dateEndHistoryChatHandler = $('#dateEndHistoryChat');
            typeHistoryChatHandler = $('#typeHistoryChat');
            //languageHistoryChatHandler=$('#languageHistoryChat');
            //initLanguageComboBoxHistory(languageHistoryChatHandler);
            var myDateBegin = new Date(2016,03,04); 
            var myDateEnd = new Date(); 
            var minDate = new Date(2016,03,04); 
            var maxDate = new Date(2016,03,07); 
            dateBeginHistoryChatHandler.datepicker({
                startDate: "-3d",
                endDate: "+3d",
            }).datepicker('setDate', myDateEnd);
            dateEndHistoryChatHandler.datepicker({
                startDate: "-3d",
                endDate: "+3d",
            }).datepicker('setDate', myDateEnd);
            
            $('#historySendButton').click(that.refreshHistoryChat);
        };

        this.refreshHistoryChat = function() {
            $("#historyListTable").empty();
            var dateFrom = dateBeginHistoryChatHandler.datepicker('getDate');
            if(dateFrom!=null) dateFrom.setHours(0, 0, 0, 0);
            var dateTo = dateEndHistoryChatHandler.datepicker('getDate');
            if(dateTo!=null) dateTo.setHours(23, 59, 59, 0);
            var typeHistoryChatValue=typeHistoryChatHandler.val();
            //var languageHistoryChatValue=languageHistoryChatHandler.val();
            var languageHistoryChatValue=ALL;
            var historicChatDate=null;
            $(listHistoricChatCache).each(function(index, historicChat) {
                historicChatDate=new Date(historicChat.date);
                if(dateFrom <= historicChatDate && historicChatDate <= dateTo ){
                    if((languageHistoryChatValue==ALL || languageHistoryChatValue==historicChat.languageId || historicChat.type==PRIVATE) && (typeHistoryChatValue==ALL || typeHistoryChatValue==historicChat.type)) {
                        var typeLabel="";
                        if(historicChat.type==PRIVATE) typeLabel=PRIVATECHATLABEL;
                        else if(historicChat.type==PUBLIC) typeLabel=PUBLICCHATLABEL;
                        that.addHistoryChat(historicChat.type+historicChat.id, typeLabel, historicChat.title, historicChat.date, historicChat.path, historicChat.languageId);
                    }
                }
            });
        };

		this.loadCurrentsChat = function(json){
			chatComponentHandler.empty();
			chatComponentHandler.append('<div id="currentChatsPopup"><select id="currentChatsSelect"><option value="0">'+PUBLICCHATLABEL+'</option></select></div><button id="currentChatsButton" style="cursor: pointer; margin-top: 20px;" class="btn btn-info sendLabel">'+SENDLABEL+'</button>'); 
			//json=JSON.parse(json);
			chatComponentHandler.find('#currentChatsButton').bind('click', function () {
				connectToChatFlash($('#chatComponent #currentChatsSelect').val());
			});
			$.each(json, function(index, eventInfo){
				chatComponentHandler.find('#currentChatsSelect').append('<option value="'+eventInfo.id+'">'+eventInfo.ack+'</option>');
			});
		},
		
        this.addHistoryChat = function(id, type, title, date, path, language){
            language=that.getLanguageName(language);
            date=new Date(date);
            date=date.toLocaleDateString(LANGAUGETAGLOCALELABEL.replace('_','-'));
            $("#historyListTable").append("<tr height='10'/>"
                    +"<tr>"
                        +"<td class='form-label' style='text-align: left; vertical-align: middle;width: 20%;'>"
                            +"<span style='color:black;'>"+type+"</span>"
                        +"</td>"
                        +"<td class='form-label' style='text-align: left; vertical-align: middle;width: 30%;''>"
                            +"<span style='color:black;'>"+title+"</span>"
                         +"<td class='form-label' style='text-align: left; vertical-align: middle;width: 35%;'>"
                            +"<span style='color:black;'>"+date+"</span>"
                         +"</td>"
                         /*+"<td class='form-label' style='text-align: left; vertical-align: middle;width: 35%;'>"
                            +"<span style='color:black;'>"+language+"</span>"
                         +"</td>"*/
                         +"<td class='form-label' style='text-align: center; vertical-align: middle;width: 15%;'>"
                            +"<a href='#' class='blueLink' style='color: #E9752A !important; ' src='#' id='openFile"+id+"'> "+OPENLABEL+" </a>"
                         +"</td>"
                    +"</tr>");
            $("#openFile"+id).click(function (){
                window.open(
                        'chathistory.html?path='+$.base64.encode(path),
                        '_blank');
            });
        };

        this.getLanguageName = function(languageId){
            if(languageId!="" && languageId!=null && listLangsAbrEnum[languageId] != null) return listLangsAbrEnum[languageId];
            else return "";
        };

        this.parseRecruiter = function (enterpriseP) {
            $('#recruiterLogin').val(enterpriseP.getEmail());
            $('#loadingEds').hide();
        };

        this.addListener = function (list) {
            listeners.push(list);
        };
        this.notifyGetCurrentsChat = function(){
			$.each(listeners, function(i){
				listeners[i].getCurrentsChat();
			});
		},
        this.notifyGetHistoryChat = function (startDate,endDate) {
            $.each(listeners, function (i) {
                listeners[i].notifyGetHistoryChat(startDate,endDate);
            });
        };
    },
    TchatfeeViewListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }



});
