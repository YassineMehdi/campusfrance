jQuery.extend({
    TchatfeeController: function (model, view) {
        var that = this;
        /**
         * listen to the view
         */
        var vlist = $.TchatfeeViewListener({
			getCurrentsChat : function() {
				model.getCurrentsChat();
			},
            notifyGetHistoryChat: function (startDate,endDate) {
                model.getHistoryChat(startDate,endDate);
            }
        });

        view.addListener(vlist);
        /**
         * listen to the model
         */
        var mlist = $.TchatfeeModelListener({
			loadCurrentsChat : function(json){
				view.loadCurrentsChat(json);
			},
            notifyRefreshHistoryChat: function () {
                view.refreshHistoryChat();
            },
        });

        model.addListener(mlist);

        this.initController = function () {
            view.initView();
            view.initHistory();
            model.initModel();
        };
    }
});
