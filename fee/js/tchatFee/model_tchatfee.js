jQuery.extend({
    TchatfeeModel: function () {
        var that = this;
        var listeners = new Array();
        this.initModel = function () {
            //that.getRecruiterInfos();
        };
        
		this.getCurrentsChatSuccess = function(json){
			that.notifyLoadCurrentsChat(json);
		},
		
		this.getCurrentsChatError = function(xhr, status, error){
			isSessionExpired(xhr.responseText);
		},
		this.getCurrentsChat = function(){
			genericAjaxCall('POST', staticVars.urlBackEnd+'agenda?forChat=true&idLang='+getIdLangParam(),
		    		'application/json', 'json', '', that.getCurrentsChatSuccess, that.getCurrentsChatError);
		},
        this.parseChatPublicHistoryListXml = function (xml) {
            $(xml).find("chatPublicHistory").each(function () {
                var historicChat=new Object();
                historicChat.id=$(this).find('chatPublicHistoryId').text();
                historicChat.title=$(this).find('eventTitle').text();
                historicChat.date=$(this).find('date').text();
                historicChat.path=$(this).find('path').text();
                historicChat.languageId=$(this).find('language > idlangage').text();
                historicChat.type=PUBLIC;
                listHistoricChatCache.push(historicChat);
            });

            that.notifyRefreshHistoryChat();
        };

        this.parseChatPrivateHistoryListXml = function (xml) {
            $(xml).find("chatPrivateHistory").each(function () {
                var historicChat=new Object();
                historicChat.id=$(this).find('chatPrivateHistoryId').text();
                historicChat.title=$(this).find('userProfileDTO2').find('prenom').text()+" "+$(this).find('userProfileDTO2').find('nom').text();
                historicChat.date=$(this).find('date').text();
                historicChat.path=$(this).find('path').text();
                historicChat.type=PRIVATE;
                listHistoricChatCache.push(historicChat);
            });

            that.notifyRefreshHistoryChat();
        };

        this.getRecruiterError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };
        this.getHistoryChat = function (startDate,endDate) {

            $("#historyListTable").empty();
            listHistoricChatCache = new Array();
            genericAjaxCall('POST', staticVars["urlBackEnd"]+"chatpublichistory/listchat?dateBegin="+startDate+'&dateEnd='+endDate,
                    'application/xml', 'xml', '', that.parseChatPublicHistoryListXml, that.getHistoryError);
            genericAjaxCall('POST', staticVars["urlBackEnd"]+"chatprivatehistory/listchat?dateBegin="+startDate+'&dateEnd='+endDate,
                    'application/xml', 'xml', '', that.parseChatPrivateHistoryListXml, that.getHistoryError);
        };

        this.addListener = function (list) {
            listeners.push(list);
        };

		this.notifyLoadCurrentsChat = function(json){
			$.each(listeners, function(i){			
				listeners[i].loadCurrentsChat(json);
			});
		};
        this.notifyRefreshHistoryChat = function () {
            $.each(listeners, function (i) {
                listeners[i].notifyRefreshHistoryChat();
            });
        };
    },
    TchatfeeModelListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }
});
