jQuery.extend({
    ProfilEntrepriseController: function (model, view) {
        var that = this;
        /**
         * listen to the view
         */
        var vlist = $.ProfilEntrepriseViewListener({
            updateStand: function (stand) {
                model.updateProfilStand(stand);
            }
        });

        view.addListener(vlist);
        /**
         * listen to the model
         */
        var mlist = $.ProfilEntrepriseModelListener({
            standLoad: function (stand) {
                view.parseStand(stand);
            },
            sectorsLoad: function (activities) {
                view.optionSectorsLoad(activities);
            },
            updateSuccess: function () {
                view.viewUpdateSuccess();
            }
        });

        model.addListener(mlist);

        this.initController = function () {
            view.initView();
            model.initModel();
        };
    }
});
