jQuery.extend({
    Stand: function (data) {
        var that = this;
        this.standName = $(data).find("stand > name").text();
        this.entreprise = new $.Entreprise($(data).find("enterpriseDTO"));
        that.websites = new Array();
        $(data).find("websiteDTOList website").each(function () {
            var website = new $.Website($(this));
            that.websites.push(website);
        });
        this.standId = new $(data).find("idstande").text();
        this.socialWallMessage = new $(data).find("socialWallMessage").text();
        that.activities = new Array();
        $(data).find("activitiesDTOList secteurActDTO").each(function () {
            var activitie = new $.Activitie($(this));
            that.activities.push(activitie);
        });
        this.profileSearched = $(data).find('profilesplusrecherche').text();


        this.getStandName = function () {
            return this.standName;
        };
        this.setStandName = function (standName) {
            that.standName = standName;
        };
        this.getEntreprise = function () {
            return this.entreprise;
        };
        this.setEntreprise = function (entreprise) {
            that.entreprise = entreprise;
        };
        this.getWebsites = function () {
            return this.websites;
        };
        this.setWebsites = function (websites) {
            that.websites = websites;
        };
        this.getSocialWallMessage = function () {
            return this.socialWallMessage;
        };
        this.setSocialWallMessage = function (socialWallMessage) {
            that.socialWallMessage = socialWallMessage;
        };
        this.getActivities = function () {
            return this.activities;
        };
        this.setActivities = function (activities) {
            that.activities = activities;
        };
        this.getContacts = function () {
            return this.contacts;
        };
        this.setContacts = function (contacts) {
            that.contacts = contacts;
        };
        this.getProfileSearched = function () {
            return this.profileSearched;
        };
        this.setProfileSearched = function (profileSearched) {
            that.profileSearched = profileSearched;
        };

        this.xmlData = function () {
            var xml = '<stand>';
            xml += '<name>' + htmlEncode(that.standName) + '</name>';
            if (that.entreprise != "") {
                xml += that.entreprise.xmlData();
            }
            if (that.websites != "") {
                xml += '<websiteDTOList>';
                for (var index in that.websites) {
                    var website = that.websites[index];
                    xml += website.xmlData();
                }
                xml += '</websiteDTOList>';
            }
            xml += '<socialWallMessage>' + htmlEncode(that.socialWallMessage) + '</socialWallMessage>';

            xml += '<activitiesDTOList>';
            if (that.activities != "") {
                for (var index in that.activities) {
                    var activitie = that.activities[index];
                    xml += activitie.xmlData();
                }
            }
            xml += '</activitiesDTOList>';
            if (that.contacts != "") {
                xml += '<contactDTOList>';
                for (var index in that.contacts) {
                    var contact = that.contacts[index];
                    xml += contact.xmlData();
                }
                xml += '</contactDTOList>';
            }
            xml += '<profilesplusrecherche>' + htmlEncode(that.profileSearched) + '</profilesplusrecherche>';
            xml += '</stand>';
            return xml;
        };
    },
    Entreprise: function (data) {
        var that = this;
        this.enterpriseName = $(data).find("nom").text();
        this.datecreation = $(data).find("datecreation").text();
        this.address = $(data).find("address").text();
        this.siegeAddress = $(data).find("siegeAddress").text();
        this.postalCode = $(data).find("postalCode").text();
        this.place = $(data).find("lieu").text();
        this.website = $(data).find("website").text();
        this.contactEmail = $(data).find("contactEmail").text();
        this.careerwebsite = $(data).find("careerwebsite").text();
        this.nbremploye = $(data).find("nbremploye").text();
        this.siegeName = $(data).find("siegeName").text();
        this.siegeAddress = $(data).find("siegeAddress").text();
        this.siegePostalCode = $(data).find("siegePostalCode").text();
        this.siegePlace = $(data).find("siegePlace").text();
        this.siegeCreationDate = $(data).find("siegeCreationDate").text();
        this.siegeNbremploye = $(data).find("siegeNbremployee").text();
        this.slogan = $(data).find("slogan").text();
        this.description = $(data).find("description").text();
        this.logo = $(data).find("logo").text();

        this.getEnterpriseName = function () {
            return this.enterpriseName;
        };
        this.setEnterpriseName = function (enterpriseName) {
            that.enterpriseName = enterpriseName;
        };
        this.getDatecreation = function () {
            return this.datecreation;
        };
        this.setDatecreation = function (datecreation) {
            that.datecreation = datecreation;
        };
        this.getSiegeAddress = function () {
            return this.siegeAddress;
        };
        this.setSiegeAddress = function (siegeAddress) {
            that.siegeAddress = siegeAddress;
        };
        this.getPostalCode = function () {
            return this.postalCode;
        };
        this.setPostalCode = function (postalCode) {
            that.postalCode = postalCode;
        };
        this.getPlace = function () {
            return this.place;
        };
        this.setPlace = function (place) {
            that.place = place;
        };
        this.getWebsite = function () {
            return this.website;
        };
        this.setWebsite = function (website) {
            that.website = website;
        };
        this.getContactEmail = function () {
            return this.contactEmail;
        };
        this.setContactEmail = function (contactEmail) {
            that.contactEmail = contactEmail;
        };
        this.getCareerwebsite = function () {
            return this.careerwebsite;
        };
        this.setCareerwebsite = function (careerwebsite) {
            that.careerwebsite = careerwebsite;
        };
        this.getNbremploye = function () {
            return this.nbremploye;
        };
        this.setNbremploye = function (nbremploye) {
            that.nbremploye = nbremploye;
        };
        this.getSiegeName = function () {
            return this.siegeName;
        };
        this.setSiegeName = function (siegeName) {
            that.siegeName = siegeName;
        };
        this.getAddress = function () {
            return this.address;
        };
        this.setAddress = function (address) {
            that.address = address;
        };
        this.getSiegeAddress = function () {
            return this.siegeAddress;
        };
        this.setSiegeAddress = function (siegeAddress) {
            that.siegeAddress = siegeAddress;
        };
        this.getSiegePostalCode = function () {
            return this.siegePostalCode;
        };
        this.setSiegePostalCode = function (siegePostalCode) {
            that.siegePostalCode = siegePostalCode;
        };
        this.getSiegePlace = function () {
            return this.siegePlace;
        };
        this.setSiegePlace = function (siegePlace) {
            that.siegePlace = siegePlace;
        };
        this.getSiegeCreationDate = function () {
            return this.siegeCreationDate;
        };
        this.setSiegeCreationDate = function (siegeCreationDate) {
            that.siegeCreationDate = siegeCreationDate;
        };
        this.getSiegeNbremploye = function () {
            return this.siegeNbremploye;
        };
        this.setSiegeNbremploye = function (siegeNbremploye) {
            that.siegeNbremploye = siegeNbremploye;
        };
        this.getSlogan = function () {
            return this.slogan;
        };
        this.setSlogan = function (slogan) {
            that.slogan = slogan;
        };
        this.getDescription = function () {
            return this.description;
        };
        this.setDescription = function (description) {
            that.description = description;
        };
        this.getLogo = function () {
            return this.logo;
        };
        this.setLogo = function (logo) {
            that.logo = logo;
        };

        this.xmlData = function () {
            var xml = '<enterpriseDTO>';
            xml += '<logo>' + htmlEncode(that.logo) + '</logo>';
            xml += '<nom>' + htmlEncode(that.enterpriseName) + '</nom>';
            xml += '<datecreation>' + htmlEncode(that.datecreation) + '</datecreation>';
            xml += '<address>' + htmlEncode(that.address) + '</address>';
            xml += '<postalCode>' + htmlEncode(that.postalCode) + '</postalCode>';
            xml += '<lieu>' + htmlEncode(that.place) + '</lieu>';
            xml += '<website>' + htmlEncode(that.website) + '</website>';
            xml += '<careerwebsite>' + htmlEncode(that.careerwebsite) + '</careerwebsite>';
            xml += '<contactEmail>' + htmlEncode(that.contactEmail) + '</contactEmail>';
            xml += '<siegeName>' + htmlEncode(that.siegeName) + '</siegeName>';
            xml += '<siegeAddress>' + htmlEncode(that.siegeAddress) + '</siegeAddress>';
            xml += '<siegePostalCode>' + htmlEncode(that.siegePostalCode) + '</siegePostalCode>';
            xml += '<siegePlace>' + htmlEncode(that.siegePlace) + '</siegePlace>';
            xml += '<siegeCreationDate>' + htmlEncode(that.siegeCreationDate) + '</siegeCreationDate>';
            xml += '<siegeNbremployee>' + htmlEncode(that.siegeNbremploye) + '</siegeNbremployee>';
            xml += '<nbremploye>' + htmlEncode(that.nbremploye) + '</nbremploye>';
            xml += '<slogan>' + htmlEncode(that.slogan) + '</slogan>';
            xml += '<description>' + htmlEncode(that.description) + '</description>';
            xml += '</enterpriseDTO>';
            return xml;
        };
    },
    Website: function (data) {
        var that = this;
        this.idWebsite = $(data).find("idwebsite").text();
        this.nameWebsite = $(data).find("nom").text();
        this.urlwebsite = $(data).find("url").text();

        this.getIdWebsite = function () {
            return this.idWebsite;
        };
        this.setIdWebsite = function (idWebsite) {
            that.idWebsite = idWebsite;
        };
        this.getNameWebsite = function () {
            return this.nameWebsite;
        };
        this.setNameWebsite = function (nameWebsite) {
            that.nameWebsite = nameWebsite;
        };
        this.getUrlWebsite = function () {
            return this.urlwebsite;
        };
        this.setUrlWebsite = function (urlwebsite) {
            that.urlwebsite = urlwebsite;
        };

        this.xmlData = function () {
            var xml = '<website>';
            xml += '<idwebsite>' + htmlEncode(that.idWebsite) + '</idwebsite>';
            xml += '<nom>' + htmlEncode(that.nameWebsite) + '</nom>';
            xml += '<url>' + htmlEncode(that.urlwebsite) + '</url>';
            xml += '</website>';
            return xml;
        };
    },
    Activitie: function (data) {
        var that = this;
        this.idSecteur = $(data).find("idsecteur").text();
        this.nameSecteur = $(data).find("name").text();

        this.getIdSecteur = function () {
            return this.idSecteur;
        };
        this.setIdSecteur = function (idSecteur) {
            that.idSecteur = idSecteur;
        };
        this.getNameSecteur = function () {
            return this.nameSecteur;
        };
        this.setNameSecteur = function (nameSecteur) {
            that.nameSecteur = nameSecteur;
        };

        this.xmlData = function () {
            var xml = '<secteurActDTO>';
            xml += '<idsecteur>' + htmlEncode(that.idSecteur) + '</idsecteur>';
            xml += '<name>' + htmlEncode(that.nameSecteur) + '</name>';
            xml += '</secteurActDTO>';
            return xml;
        };
    },
    ProfilEntrepriseModel: function () {
        var that = this;
        var listeners = new Array();
        this.initModel = function () {
            that.getSectors();
        };
        // Info profil
        this.updateProfilStandSuccess = function (xml) {
            //console.log(xml);
            that.notifyUpdateStandSuccess();
        };

        this.updateProfilStandError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };
        this.updateProfilStand = function (stand) {
            genericAjaxCall('POST', staticVars.urlBackEnd + 'stand/update?idLang=' + getIdLangParam(),
                    'application/xml', 'xml', stand.xmlData(), that.updateProfilStandSuccess, that.updateProfilStandError);
        };
        // Info profil
        this.getStandInfosSuccess = function (xml) {
            //console.log(xml);
            var stand = new $.Stand(xml);
            that.notifyStandSuccess(stand);
        };

        this.getStandInfosError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };
        this.getStandInfos = function () {
            genericAjaxCall('GET', staticVars.urlBackEnd + 'stand/standInfo?idLang=' + getIdLangParam(),
                    'application/xml', 'xml', '', that.getStandInfosSuccess, that.getStandInfosError);
        };
        // option sectors
        this.getSectorsSuccess = function (xml) {
            //console.log(xml);
            var activities = new Array();
            $(xml).find("secteurActDTO").each(function () {
                var activitie = new $.Activitie($(this));
                activities.push(activitie);
            });
            that.getStandInfos();
            that.notifySectorsSuccess(activities);
        };

        this.getSectorsError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };
        this.getSectors = function () {
            genericAjaxCall('GET', staticVars.urlBackEnd + 'secteuract?idLang=' + getIdLangParam(),
                    'application/xml', 'xml', '', that.getSectorsSuccess, that.getSectorsError);
        };

        this.addListener = function (list) {
            listeners.push(list);
        };

        this.notifyUpdateStandSuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].updateSuccess();
            });
        };
        this.notifyStandSuccess = function (stand) {
            $.each(listeners, function (i) {
                listeners[i].standLoad(stand);
            });
        };
        this.notifySectorsSuccess = function (activities) {
            $.each(listeners, function (i) {
                listeners[i].sectorsLoad(activities);
            });
        };
        this.notifyContactsSuccess = function (contacts) {
            $.each(listeners, function (i) {
                listeners[i].contactsLoad(contacts);
            });
        };

    },
    ProfilEntrepriseModelListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }
});
