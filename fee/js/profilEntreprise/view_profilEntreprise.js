jQuery.extend({
    ProfilEntrepriseView: function () {
        var that = this;
        that.update = false;
        var listeners = new Array();
        var enterpriseProfileForm = null;
        var contentHandler = null;
        var newContactForm = null;
        var saveProfileHandler = null;
        var contactContentHandler = null;
        var addContactBtnHandler = null;
        var modalContactHandler = null;
        var saveNewContacttHandler = null;
        var validatorContact = null;
        var idDeletecontactHandler = null;
        var contactManageList = null;
        var removeConatactBtnHandler = null;
        var contactManageList = null;
        var modalIDContactHandler = null;
        var currentStand = null;
        var isSiege=null;

        this.initView = function () {
            traduct();
            contentHandler = $("#mainBackEndContainer");
            enterpriseProfileForm = $("#enterpriseProfileForm");
            newContactForm = $("#contactForm");
            saveProfileHandler = $(".saveAccountButton");
            contactContentHandler = $(".contactG");
            addContactBtnHandler = $(".addContactLink");
            modalContactHandler = $("#addContact");
            saveNewContacttHandler = $("#saveContact");
            idDeletecontactHandler = $("#idDeleteContact");
            removeConatactBtnHandler = $("#btnDeletecontact");
            modalIDContactHandler = $('#idContact');
            isSiege=$("input[type=radio][name=isSiege]");
            contactManageList = new Array();

            //Custom checkboxes
            factorycheckbox();

            //Custom multiSelect
            that.factorySelect();

            //Custom validate
            that.validateProfileForm();
            that.validateNewContactForm();
            saveProfileHandler.unbind("click").bind("click", function (e) {
                that.factorySaveForm();
                //console.log($(enterpriseProfileForm).valid());
            });
            saveNewContacttHandler.unbind("click").bind("click", function (e) {
                that.saveContactDom();
            });

            isSiege.change(function(){
        		if(this.value=="oui")  {
        			that.factorySameData();
        		}else $(".sameData").val("");
            });

            //Custom file upload
            $('#logoUpload').fileupload({
                beforeSend: function (jqXHR, settings) {
                    beforeUploadFile('.saveStandButton', '#progressLogoUpload', '#logoUploadStand .filebutton,#loadLogoStandError');
                },
                datatype: 'json',
                cache: false,
                add: function (e, data) {
                    var goUpload = true;
                    var uploadFile = data.files[0];
                    var fileName = uploadFile.name;
                    timeUploadLogo = (new Date()).getTime();
                    data.url = getUrlPostUploadFile(timeUploadLogo, fileName);
                    if (!(/\.(gif|jpg|jpeg|tiff|png)$/i).test(fileName)) {
                        goUpload = false;
                    }
                    if (uploadFile.size > MAXFILESIZE) {// 6mb
                        goUpload = false;
                    }
                    if (goUpload == true) {
                        data.submit();
                    } else
                        $('#loadLogoStandError').show();
                },
                progressall: function (e, data) {
                    progressBarUploadFile('#progressLogoUpload', data, 100);
                },
                done: function (e, data) {
                    var file = data.files[0];
                    var fileName = file.name;
                    var hashFileName = getUploadFileName(timeUploadLogo, fileName);
                    //console.log("Add logo stand file done, filename :"+fileName+", hashFileName : "+hashFileName);
                    var enterpriseLogo = getUploadFileUrl(hashFileName);
                    $('#enterpriseLogo').attr('src', enterpriseLogo);
                    afterUploadFile('.saveStandButton', '#logoUploadStand .filebutton', '#progressLogoUpload');
                }
            });
            //Add Contact
            addContactBtnHandler.unbind("click").bind("click", function (e) {
                that.factoryAddConact();

            });
            //Delete Contact
            removeConatactBtnHandler.unbind("click").bind("click", function (e) {
                var contactManage = contactManageList[idDeletecontactHandler.val()];
                //console.log(contactManageList);
                contactManage.getDOM().remove();
                delete contactManageList[idDeletecontactHandler.val()];
                //console.log(contactManageList);

            });
            //Add Advice file upload avatar
            $('#uploadAvatar').fileupload({
                beforeSend: function (jqXHR, settings) {
                    beforeUploadFile('.saveStandButton', '#progressAvatarUpload', '#avatarUploadContact .filebutton, .loadError');
                },
                datatype: 'json',
                cache: false,
                add: function (e, data) {
                    var goUpload = true;
                    var uploadFile = data.files[0];
                    var fileName = uploadFile.name;
                    timeUploadLogo = (new Date()).getTime();
                    data.url = getUrlPostUploadFile(timeUploadLogo, fileName);
                    if (!(/\.(gif|jpg|jpeg|tiff|png)$/i).test(fileName)) {
                        goUpload = false;
                    }
                    if (uploadFile.size > MAXFILESIZE) {// 6mb
                        goUpload = false;
                    }
                    if (goUpload == true) {
                        data.submit();
                    } else
                        $('.loadError').show();
                },
                progressall: function (e, data) {
                    progressBarUploadFile('#progressAvatarUpload', data, 100);
                },
                done: function (e, data) {
                    var file = data.files[0];
                    var fileName = file.name;
                    var hashFileName = getUploadFileName(timeUploadLogo, fileName);
                    //console.log("Add logo stand file done, filename :"+fileName+", hashFileName : "+hashFileName);
                    var avatarContact = getUploadFileUrl(hashFileName);
                    $('#avatarNewContact').attr('src', avatarContact);
                    afterUploadFile('.saveStandButton', '#avatarUploadContact .filebutton', '#progressAvatarUpload');
                }
            });

        };
        this.factorySameData=function(){
          $("#siegeName").val($("#enterpriseName").val());
          $("#siegeCreationDate").val($("#creationDate").val());
          $("#siegeAddress").val($("#enterpriseAddress").val());
          $("#siegePostalCode").val($("#postalCode").val());
          $("#siegeNbremployee").val($("#employeeNumber").val());
          $("#siegePlace").val($("#enterprisePlace").val());
        };
        //Recruiter Xml
        this.parseStand = function (stand) {
            var menuTitle = $("#stand").find("span").text();
            $('.page-title-menu').text(menuTitle);

            currentStand = stand;
            var entreprise = stand.entreprise;
            var websites = stand.websites;
            var activities = stand.activities;
            //console.log(stand);
            standId = stand.standId;

            $('#enterpriseName').val(entreprise.getEnterpriseName());
            $('#standName').val(stand.getStandName());
            $('#creationDate').val(entreprise.getDatecreation());
            $('#enterpriseAddress').val(entreprise.getAddress());
            $('#postalCode').val(entreprise.getPostalCode());
            $('#enterprisePlace').val(entreprise.getPlace());
            $('#webSite').val(entreprise.getWebsite());
            $('#careerWebsite').val(entreprise.getCareerwebsite());
            $('#contactEmail').val(entreprise.getContactEmail());
            $('#employeeNumber').val(entreprise.getNbremploye());
            $('#enterpriseSlogan').val(entreprise.getSlogan());

            $('#siegeName').val(entreprise.getSiegeName());
            $('#siegeAddress').val(entreprise.getSiegeAddress());
            $('#siegePostalCode').val(entreprise.getSiegePostalCode());
            $('#siegePlace').val(entreprise.getSiegePlace());
            $('#siegeCreationDate').val(entreprise.getSiegeCreationDate());
            $('#siegeNbremployee').val(entreprise.getSiegeNbremploye());

            var activitiesId = new Array();
            if (activities != null) {
                for (var i = 0; i < activities.length; i++) {
                    //console.log(websites[i]);
                    var activitie = activities[i];
                    //console.log(activitie.getNameSecteur());
                    activitiesId.push(activitie.getIdSecteur());
                }
            }
            $("#sector").val(activitiesId).multiSelect('refresh');

            $('.uploadNewLogo').prepend('<span><img id="enterpriseLogo" src="' + entreprise.getLogo() + '" /></span>');

            $('#socialWallMessage').val(stand.getSocialWallMessage());

            if (websites != null) {
                for (var i = 0; i < websites.length; i++) {
                    var website = websites[i];
                    $('.' + website.getNameWebsite()).val(website.getUrlWebsite());
                }
            }
            $('#profileDesc').val(unescape(entreprise.getDescription()));
            $('#loadingEds').hide();

        };

        this.optionSectorsLoad = function (activities) {
            $("#sector").empty();
            if (activities != null) {
                for (var i = 0; i < activities.length; i++) {
                    var activitie = activities[i];
                    addOption("#sector", activitie.getIdSecteur(), activitie.getNameSecteur());
                }
            }
            that.factorymultiSelect('#sector');
        };

        //send Stand Info
        this.sendStandInfo = function (stand) {
            var entreprise = stand.entreprise;
            var websites = new Array();
            var activities = stand.activities;

            entreprise.setEnterpriseName($('#enterpriseName').val());
            stand.setStandName($('#standName').val());
            entreprise.setDatecreation($('#creationDate').val());
            entreprise.setAddress($('#enterpriseAddress').val());
            entreprise.setPostalCode($('#postalCode').val());
            entreprise.setPlace($('#enterprisePlace').val());
            entreprise.setWebsite($('#webSite').val());
            entreprise.setCareerwebsite($('#careerWebsite').val());
            entreprise.setContactEmail($('#contactEmail').val());
            entreprise.setNbremploye($('#employeeNumber').val());
            entreprise.setSlogan($('#enterpriseSlogan').val());

            entreprise.setSiegeName($('#siegeName').val());
            entreprise.setSiegeAddress($('#siegeAddress').val());
            entreprise.setSiegePostalCode($('#siegePostalCode').val());
            entreprise.setSiegePlace($('#siegePlace').val());
            entreprise.setSiegeCreationDate($('#siegeCreationDate').val());
            entreprise.setSiegeNbremploye($('#siegeNbremployee').val());


            var activitiesId = $("#sector").val();
            var activities = new Array();
            if (activitiesId != null) {
                for (var i = 0; i < activitiesId.length; i++) {
                    var activitieId = activitiesId[i];
                    var activitie = new $.Activitie();
                    activitie.setIdSecteur(activitieId);
                    activities.push(activitie);
                }
            }
            stand.setActivities(activities);

            entreprise.setLogo($('#enterpriseLogo').attr('src'));

            stand.setSocialWallMessage($('#socialWallMessage').val());

            var website = null;
            $(".webSiteUrl .reseauSocial").each(function () {
                if ($(this).val() != '') {
                    website = new $.Website();
                    website.setNameWebsite($(this).attr('name'));
                    website.setUrlWebsite($(this).val());
                    websites.push(website);
                }
            });
            stand.setWebsites(websites);

            entreprise.setDescription($('#profileDesc').val());

            var contacts = new Array();
            var contact = null;
            for (var index in contactManageList) {
                var contactManage = contactManageList[index];
                contact = contactManage.getContact();
                contacts.push(contact);
            }
            stand.setContacts(contacts);
            //stand.setContacts([]);
            //console.log(stand.xmlData());

            that.notifyUpdateStand(stand);

        };

        //Custom multiSelect
        this.factorymultiSelect = function (selector) {
            $(selector).multiSelect();
        };

        //Custom validate
        this.factorySaveForm = function () {
            if ($(enterpriseProfileForm).valid())
                that.sendStandInfo(currentStand);
        };
        //Custom select
        this.factorySelect = function () {
            for (var i = 1850; i <= 2016; i++)
                addOption("#enterpriseProfileForm .creationDate", i, i);
        };

        this.validateProfileForm = function () {
            $("#enterpriseProfileForm").validate({
                lang: 'en',
                rules: {
                    enterpriseName: {
                        required: true,
                        minlength: 3
                    },
                    standName: {
                        required: true
                    },
                    enterpriseAddress: {
                        required: true
                    },
                    postalCode: {
                        required: true
                    },
                    enterprisePlace: {
                        required: true
                    },
                    webSite: {
                        url: true,
                        required: true
                    },
                    contactEmail: {
                        required: true
                    },
                    siegeName: {
                        required: true
                    },
                    siegeAddress: {
                        required: true
                    },
                    siegePostalCode: {
                        required: true
                    },
                    siegePlace: {
                        required: true
                    }
                }
            });
        };
        this.validateNewContactForm = function () {
            $.validator.addMethod("phoneNumbre", function (phone_number, element) {
                phone_number = phone_number.replace(/\s+/g, "");
                return this.optional(element) || phone_number.length > 9 &&
                        (phone_number.match(/\(?\+([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/) || phone_number.match(/\(?(00[0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/));
            }, "Merci de respecter le format indiqué");
            validatorContact = $("#contactForm").validate({
                rules: {
                    contactName: {
                        required: true,
                        minlength: 6
                    },
                    emailContact: {
                        required: true,
                    },
                    contactTel: {
                        phoneNumbre: true,
                        minlength: 10
                    }
                }
            });
        };
        this.viewUpdateSuccess = function () {
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        this.addListener = function (list) {
            listeners.push(list);
        };
        this.notifyUpdateStand = function (stand) {
            $.each(listeners, function (i) {
                listeners[i].updateStand(stand);
            });
        };

    },
    ProfilEntrepriseViewListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }



});
