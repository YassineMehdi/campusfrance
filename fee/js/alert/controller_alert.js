jQuery.extend({
    AlertController: function (model, view) {
        var that = this;
        /**
         * listen to the view
         */
        var vlist = $.AlertViewListener({
            addPublication: function (publication) {
                model.modelAddPublication(publication);
            },
            updateEvent: function (publication) {
                model.modelUpdatePub(publication);
            },
            deletePub: function (idPublication) {
                model.deletePublication(idPublication);
            }

        });

        view.addListener(vlist);

        /**
         * listen to the model
         */
        var mlist = $.AlertModelListener({
            loadPublications: function (listPublications) {
                //$('#numberPublications').text(listPublications.length);
                view.loadPublications(listPublications);
            },
            addPubSuccess: function (publication) {
                view.viewAddPubSuccess(publication);
            },
            updatePubSuccess: function (publication) {
                view.viewUpdatePubSuccess(publication);
            },
            updatePubError: function (publication) {
                view.viewUpdatePubError(publication);
            },
            deletePubSuccess: function (publication) {
                view.viewDeletePubSuccess(publication);
            }
        });

        model.addListener(mlist);

        this.initController = function () {
            view.initView();
            model.initModel();
        };
    }
});
