jQuery.extend({
    PublicationManage: function (publication, view) {
        var that = this;
        var dom = $('<div class="onePublicationDisplay listing" id="publication' + publication.getIdPublication() + '">'
                + '<input class="idPub" type = "hidden" value = "' + publication.getIdPublication() + '" / >'
                + '<input id="typePublication' + publication.getIdPublication() + '" type = "hidden" value = "' + htmlEncode(publication.getSubjectPublication()) + '" / >'
                + '<div class="contentForm">'
                + '<div class="row no-margin">'
                + '<div class="labelF"><span class="subjectLabel"></span> :</div>'
                + '<form id="subjectPublicationForm' + publication.getIdPublication() + '" class="subjectPublicationForm">'
                + '<span id="subjectPublicationLabel' + publication.getIdPublication() + '" > ' + htmlEncode(publication.getSubjectPublication()) + ' </span>'
                + '<input class="inputTitle subjectPublication required" style="display:none;" maxlength="40" id="subjectPublicationInput' + publication.getIdPublication() + '" type = "text" / >'
                + '</form>'
                + '</div>'
                + '<div class="row no-margin">'
                + '<div class="labelF" ><span class="textLabel"></span> :</div>'
                + '<form id="contentPublicationForm' + publication.getIdPublication() + '" class="contentPublicationForm" >'
                + '<span id="contentPublicationLabel' + publication.getIdPublication() + '"> ' + htmlEncode(publication.getContentPublication()) + ' </span>'
                + '<input id="contentPublicationInput' + publication.getIdPublication() + '" maxlength="200" class="contentPublication required" type = "text" style="display:none;" / >'
                + '</form>'
                + '</div>'
                + '</div>'
                + '<div class="actionBtn">'
                + '<a class="editLabel btn btn-primary updatePub" id="editSubjectPublication' + publication.getIdPublication() + '" href = "javascript:void(0)" > modifier </a>'
                + '<a class="deleteLabel btn btn btn-danger removePublication" id="removePublication' + publication.getIdPublication() + '" href = "javascript:void(0)" > supprimer </a>'
                + '<div align = "right" class="restore" id="restoreContentDiv' + publication.getIdPublication() + '" >'
                + '<a class="modifExist btn btn-success' + publication.getIdPublication() + '" id="confirmEditContent' + publication.getIdPublication() + '" href = "javascript:void(0)" > sauvegarder </a> <span>/ </span> <a class="restoreLabel btn btn-danger" id="restoreEditContent' + publication.getIdPublication() + '" href="javascript:void(0)">annuler</a >'
                + '</div>'
                + '</div>'
                + '<hr class="mt0">'
                + '</div>');
        this.refresh = function () {
            dom.find('.updatePub').unbind("click").bind("click", function (e) {
                view.factoryUpdatePublication(publication);
            });
            dom.find('.removePublication').unbind("click").bind("click", function (e) {
                view.factoryModalRemovePub(publication);
            });
        };
        this.getDOM = function () {
            return dom;
        };
        this.refresh();
    },
    AlertView: function () {
        var that = this;
        var listeners = new Array();
        var contentHandler = null;
        var modalPubHandler = null;
        var forumPublicationContentHandler = null;
        var standPublicationContentHandler = null;
        var publicationFormHandler = null;
        var addForumPublicationHandler = null;
        var addStandPublicationHandler = null;
        var subjectPublicationHandler = null;
        var contentPublicationHandler = null;
        var savePubHandler = null;
        var updatePubHandler = null;
        var idPublicationHandler = null;
        var removePubBtnHandler = null;
        var idDeletePubHandler = null;

        this.update = false;
        this.isForumPub = false;

        this.initView = function () {
            contentHandler = $("#mainBackEndContainer");
            modalPubHandler = $("#addPub");
            forumPublicationContentHandler = $("#forumPublicationContent");
            standPublicationContentHandler = $("#standPublicationContent");
            publicationFormHandler = $("#publicationForm");
            addForumPublicationHandler = $("#addForumPublicationLink");
            addStandPublicationHandler = $("#addStandPublicationLink");
            subjectPublicationHandler = $("#subjectPublication");
            contentPublicationHandler = $("#contentPublication");
            savePubHandler = $("#savePub");
            updatePubHandler = $(".updatePub");
            idPublicationHandler = $("#idPublication");
            idDeletePubHandler = $("#idDeletePub");
            removePubBtnHandler = $("#btnDeletePub");
            traduct();

            //Add publication form
            addForumPublicationHandler.unbind("click").bind("click", function (e) {
                that.isForumPub = true;
                that.factoryAddPublication();
            });
            //Add publication stand
            addStandPublicationHandler.unbind("click").bind("click", function (e) {
                that.isForumPub = false;
                that.factoryAddPublication();
            });
            //Save publication
            savePubHandler.unbind("click").bind("click", function (e) {
                that.savePub();
            });
            //Delete Pub
            removePubBtnHandler.unbind("click").bind("click", function (e) {
                that.notifyDeletePub(idDeletePubHandler.val());
            });

            bindCheckMaxLength(subjectPublicationHandler, 40);
            bindCheckMaxLength(contentPublicationHandler, 200);
        };

        this.viewAddPubSuccess = function () {
            modalPubHandler.modal('hide');
            modalPubHandler.find('#publicationForm')[0].reset();
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        this.viewUpdatePubSuccess = function () {
            modalPubHandler.modal('hide');
            modalPubHandler.find('#publicationForm')[0].reset();
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        this.viewUpdatePubError = function () {
            modalPubHandler.modal('hide');
            modalPubHandler.find('#publicationForm')[0].reset();
            contentHandler.prepend("<div class='col-md-5 alertEvent alert alert-dismissable alert-danger'><i class='fa fa-fw fa-times'></i>&nbsp; "+ERRORACCOUNTLABEL+"<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button></div>");
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        this.viewDeletePubSuccess = function () {
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };

        this.addListener = function (list) {
            listeners.push(list);
        };

        this.loadPublications = function (listPublications) {
            forumPublicationContentHandler.empty();
            standPublicationContentHandler.empty();

            var listForumPublications = [];
            var listStandPublications = [];
            $(listPublications).each(
                    function(index, publication) {
                        if (publication.getForumPublication()) {
                            listForumPublications.push(publication);
                        }else {
                            listStandPublications.push(publication);
                        }
                    });

            var rightGroup = listStandRights[rightGroupEnum.FORUM_PUBLICATIONS];
            if (rightGroup != null) {
                var permittedForumPublicationsNbr = rightGroup.getRightCount();
                var forumPublicationsNbr = Object.keys(listForumPublications).length;
                if (permittedForumPublicationsNbr == -1) {
                    $('#addForumPublicationDiv #addForumPublicationLink').text("[ "+ADDLABEL+" ]");
                    $('#forumPublicationDiv').show();
                } else if (permittedForumPublicationsNbr == 0){
                    $('#forumPublicationDiv').hide();
                } else if (forumPublicationsNbr < permittedForumPublicationsNbr) {
                    $('#numberForumPublications').text(forumPublicationsNbr);
                    $('#numberPermittedForumPublications').text(permittedForumPublicationsNbr);
                    $('#forumPublicationDiv, #addForumPublicationDiv').show();
                } else $('#addForumPublicationDiv').hide();
            }
            rightGroup = listStandRights[rightGroupEnum.STAND_PUBLICATIONS];
            if (rightGroup != null) {
                var permittedStandPublicationsNbr = rightGroup.getRightCount();
                var standPublicationsNbr = Object.keys(listStandPublications).length;
                if (permittedStandPublicationsNbr == -1) {
                    $('#addStandPublicationDiv #addStandPublicationLink').text("[ "+ADDLABEL+" ]");
                    $('#standPublicationDiv').show();
                } else if (permittedStandPublicationsNbr == 0){
                    $('#standPublicationDiv').hide();
                } else if (standPublicationsNbr < permittedStandPublicationsNbr) {
                    $('#numberStandPublications').text(standPublicationsNbr);
                    $('#numberPermittedStandPublications').text(permittedStandPublicationsNbr);
                    $('#standPublicationDiv, #addStandPublicationDiv').show();
                } else {
                    $('#addStandPublicationDiv').hide();
                }
            }

            for (var i = 0; i < listPublications.length; i++) {
                var publication = listPublications[i];
                var publicationManage = new $.PublicationManage(publication, that);
                if (publication.getForumPublication())
                    forumPublicationContentHandler.append(publicationManage.getDOM());
                else
                    standPublicationContentHandler.append(publicationManage.getDOM());
            }
            $('.restore').hide();
            traduct();
            $('#loadingEds').hide();
        };

        this.factoryAddPublication = function () {
            that.update = false;
            $('#addPub').modal();
            publicationFormHandler[0].reset();
            if ($(".errorValidateField") != undefined) {
                $(".errorValidateField").remove();
            }
        };
        this.factoryModalRemovePub = function (publication) {
            $('#removePublication').modal();
            idDeletePubHandler.val(publication.getIdPublication());
        };
        this.factoryUpdatePublication = function (publication) {
            that.update = true;
            $('#addPub').modal();
            subjectPublicationHandler.val(publication.getSubjectPublication());
            contentPublicationHandler.val(publication.getContentPublication());
            idPublicationHandler.val(publication.getIdPublication());
        };

        this.savePub = function () {
            if ($(".errorValidateField") != undefined) {
                $(".errorValidateField").remove();
            }
            var isValid = true;
            if (subjectPublicationHandler.val() == "") {
                subjectPublicationHandler.after('<label class="error errorValidateField"> '+REQUIREDLABEL+' </label>');
                isValid = false;
            }

            if (contentPublicationHandler.val() == "") {
                contentPublicationHandler.after('<label class="error errorValidateField"> '+REQUIREDLABEL+' </label>');
                isValid = false;
            }

            if (isValid == true) {
                //$("#preloaderCustom").show();
                var publication = new $.Publication();
                publication.setSubjectPublication(subjectPublicationHandler.val());
                publication.setContentPublication(contentPublicationHandler.val());
                publication.setForumPublication(that.isForumPub);

                if (!that.update) {
                    that.notifyAddPub(publication);
                } else {
                    publication.setIdPublication($('#idPublication').val());
                    that.notifyUpdatePub(publication);
                }
            }
        };

        this.notifyAddPub = function (publication) {
            $.each(listeners, function (i) {
                listeners[i].addPublication(publication);
            });
        };
        this.notifyUpdatePub = function (publication) {
            $.each(listeners, function (i) {
                listeners[i].updateEvent(publication);
            });
        };
        this.notifyDeletePub = function (idPublication) {
            $.each(listeners, function (i) {
                listeners[i].deletePub(idPublication);
            });
        };
    },
    AlertViewListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }



});
