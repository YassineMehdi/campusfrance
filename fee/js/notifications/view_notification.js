jQuery.extend({
	NotificationView : function(){
				var listeners = new Array();
				var that = this;
				var MAX_NOTIFICATION_SHOW = 10;
				var MAIL_FROM_CANDIDATE_SEND="mail_from_candidate_send";
				var UNSOLICITED_CANDIDATURE="unsolicited_candidature";
				var SOLICITED_CANDIDATURE = "solicited_candidature";
				var MAX_CHARACTERS_SENTENCE_NOTIF=20;
				var MAX_CHARACTERS_WORD_NOTIF=20;
				var mailNotifHandler=null;
				var mailNotifInboxHandler=null;
				var candidateLeftHandler=null;
				var messageContainerHandler = null;
				var fuiMailContentHandler=null;
				var noMailNotificationsHandler=null;
				var clipNotifHandler=null;
				var candidatureContainerHandler=null;
				var fuiClipContentHandler=null;
				var noCalendarNotificationsHandler=null;
				var listNotifMessages = null;
				listMessageObjects= new Array();
				var listNotifCandidates = null;
				
				notificationGlobalCounter = null ;
				
			    this.initView = function(){
			    	
			    	messageContainerHandler = $('.message-container');
			    	messageLeftContainerHandler = $('#inbox');
			    	candidateContainerHandler = $('#candidatures');
			    	fuiMailContentHandler=$('.fui-mail-content');
					noMailNotificationsHandler=$('#no-mail-notifications');
					candidatureContainerHandler = $('.fui-calendar-solid');
					fuiClipContentHandler=$('.fui-clip-content');
					noCalendarNotificationsHandler=$('#no-calendar-notifications');
			    	
			    	messageContainerHandler.click(function(){
			    		that.factoryNotifIconHandler('.popover', mailNotifHandler, '.fui-mail-popup');
			    	});
			    	
			    	candidatureContainerHandler.click(function(){
			    		that.factoryNotifIconHandler('.popover', clipNotifHandler, '.fui-clip-popup');
			    	});

			    	$('#logoutProfil').click(function(){
			    		$('#popupDisconnect').modal();
			    	}); 

					$('#logout').click(function(){
			    		logout();
			    	}); 	

			    	$('#trigger-fullscreen').click(function () {
				        Utility.toggle_fullscreen(document.documentElement);
				    });    		    	
			    	
			    	$('.see-all-messages').click(function(){
		    			$('.inbox').trigger('click');
    				});
			    	
			    	$('.see-all-candidatures').click(function(){
    					$('.candidaturesBtn').trigger('click');
    				});

		            $('.selectLangOption').bind("click",function () {
		                $('.langVal').text($(this).attr('val'));
		                if (currentPage == PageEnum.BACKEND){
		                    showLanguagePopup();
		                }
		            });

					$('#confirmChangeLanguage').click(
						function() {
							window.location.replace("index.html?idLang=" + $('.langVal').text());
					});

					switch (getParam("idLang")){
						case '1': 
							$('.langChoiceDiv').text('Arabe');
						break;
						case '2': 
							$('.langChoiceDiv').text('English');
						break;
						case '3': 
							$('.langChoiceDiv').text('Français');
						break;
						case '4': 
							$('.langChoiceDiv').text('Russe');
						break;
						case '5': 
							$('.langChoiceDiv').text('Allemand');
						break;
						case '6': 
							$('.langChoiceDiv').text('Espagnol');
						break;
					}
			    	
			    };
			   
			    this.factoryNotifIconHandler = function(popoverSelector, notifIndexHandler, radioPopupSelector){
			    	$(popoverSelector).slideUp();
		    		if(notifIndexHandler != null){
		    			if($(radioPopupSelector).is( ":hidden" ))
		    				$(radioPopupSelector).slideDown();
		    			else $(popoverSelector).slideUp();
		    		}
			    };
				this.display = function(notifMessage){					
					var type = notifMessage.notifType;
					if(type != null) type=type.toLowerCase();
					//console.log("Type : "+type);
					that.decodeURINotif(notifMessage);
					switch(type) {
					    case MAIL_FROM_CANDIDATE_SEND :
							that.decodeURINotif(notifMessage);
							that.decodeURINotif(notifMessage);
					    	that.receivedMailFromCandidate(notifMessage);
					    	break;
				        
					    case UNSOLICITED_CANDIDATURE: case SOLICITED_CANDIDATURE:
							that.decodeURINotif(notifMessage);
							that.decodeURINotif(notifMessage);
					    	that.receivedUnsolicitedCandidature(notifMessage);
					    	break;
					}
					
				};
				this.receivedMailFromCandidate = function(notifMail){
					//console.log(notifMail);
					//console.log("receivedMailFromRecruiter");
					var id=notifMail.id;
					var notifId=notifMail.idNotif;
			    	var subject=unescape(notifMail.subject);
			    	var body=unescape(notifMail.body);
			    	var notifDate = notifMail.dateTime;
			    	var picture=notifMail.picture;
			    	subject=subject.replace("�", "e").replace("�", "e");				
					body=body.replace("�", "e").replace("�", "e");	
			    	//picture = decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(picture)))))))))));
			    	//body = decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(body)))))))))));
			    	//subject = decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(subject)))))))))));
			    	subject=that.factoryFormattingString(subject);
			    	body=that.factoryFormattingString(body);
			    	if($('.notifElement').length != 0){
						//console.log('inside if');
						$('.no-notification-style').parent().remove();
						listNotifMessages.prepend('<li class="notifElement">'
			                    +'<a href="#" id="linkMailNotif_'+id+'">'
			                    	+'<div class="notificationID">'+notifId+'</div>'
			                        +'<img class="msg-avatar" src="'+picture+'" alt="avatar" />'
			                        +'<div class="msg-content">'
			                            +'<span class="name">'+subject+'</span>'
			                            +'<span class="msg">'+body+'</span>'
			                        +'</div>'
			                        +'<span class="msg-time">'+timestampsToDateFormated(notifDate)+'</span>'
			                    +'</a>'
			                +'</li>');

						//if($('.notifElement').length <= 10 ) {
							var counter = parseInt(mailNotifHandler.text());
							mailNotifHandler.text(counter+1);
							mailNotifInboxHandler.text(counter+1);
							
						//}else{
							//$('.notifElement')[$('.notifElement').length-1].remove();
						//}
					}else{
						//console.log('inside else');
						//messageContainerHandler.append('<span id="mail-notif-index" class="mail-notif-index iconbar-unread">1</span>');

						messageContainerHandler.append('<span id="mail-notif-index" class="badge badge-info">1</span>');

						//messageLeftContainerHandler.append('<span id="mail-notif-inbox" class="mail-notif-index iconbar-unread" style=" position: absolute; top: 0px; right: 0px; ">1</span>');
						//messageLeftContainerHandler.append('<span id="mail-notif-inbox" class="badge badge-info">1</span>');
						mailNotifHandler=$('#mail-notif-index');	
						mailNotifInboxHandler=$('#mail-notif-inbox');
			    		noMailNotificationsHandler.slideUp();
			    		
						listNotifMessages = $('.fui-mail-content');
						/*listNotifMessages.append('<a href="#" id="linkMailNotif_'+id+'" class="notifElement"><div class="notificationID">'+notifId
								+'</div><div class="popover-content popover-content-mail"><div class="notifPic"> <img class="logoStandSearch" src="'+picture
								+'" alt="logo"> </div> <div class="notifHeader">'+subject+'</div> <div class="notifBody">'+body+'</div><div class="notifDate">'+timestampsToDate(notifDate)+'</div></div></a>');
						*/
						$('.no-notification-style').parent().remove();
						listNotifMessages.empty();
						listNotifMessages.append('<li class="notifElement">'
			                    +'<a href="#" id="linkMailNotif_'+id+'">'
			                    	+'<div class="notificationID">'+notifId+'</div>'
			                        +'<img class="msg-avatar" src="'+picture+'" alt="avatar" />'
			                        +'<div class="msg-content">'
			                            +'<span class="name">'+subject+'</span>'
			                            +'<span class="msg">'+body+'</span>'
			                        +'</div>'
			                        +'<span class="msg-time">'+timestampsToDateFormated(notifDate)+'</span>'
			                    +'</a>'
			                +'</li>');
					}
					
					notificationGlobalCounter++;
					document.title = "("+notificationGlobalCounter+") Gestion du stand";
					
		    		fuiMailContentHandler.append(listNotifMessages);
		    		
			    	$('#linkMailNotif_'+id).click(function(){
		    			var notificationClickedID = $(this).find('.notificationID').text();
		    			notificationConversationIndex = notificationClickedID ;
		    			$('.inbox').trigger('click');
		    			setTimeout(function(){ 
		    				$('#mail_row_'+notificationClickedID).trigger('click');
		    			}, 3000);

		    			for(var j = 0 ; j < listNotifMessages.length ; j++ ){
		    				if($(listNotifMessages[j]).find('.notificationID').text() == notificationClickedID) listNotifMessages.splice(j, 1);
		    			}
		    			$(this).parent().remove();
		    			var counter = parseInt(mailNotifHandler.text());
		    			if(counter == 1){
		    				mailNotifHandler.remove();
		    				listNotifMessages = $('.fui-mail-content');
		    				listNotifMessages.append('<li>'
					                    +'<a href="#" class="no-notification-style">'
					                        +'<span class="name">Vous n\'avez aucune notification !</span>'
					                    +'</a>'
					                +'</li>');
		    			}else{
		    				//if($('.notifElement').length < 10) {
		    					//$('#fui-mail-display').hide();
		    				//}
		    				mailNotifHandler.text(counter-1);
		    				mailNotifInboxHandler.text(counter-1);
		    			}
//			    		
		    			notificationGlobalCounter = notificationGlobalCounter-1;
		    			
		    			if(notificationGlobalCounter != 0){
		    				document.title = "("+notificationGlobalCounter+") Gestion du stand";
		    			}else{
		    				notificationGlobalCounter = 0;
		    				document.title = "Gestion du stand";
		    			}
				    	that.notifyShowNotif(id);
		    		});
			    	
			    	if($('.popover-content-mail').length > (MAX_NOTIFICATION_SHOW-1)){
			    		$('#fui-mail-display').show();
			    		notificationConversationIndex = -1 ;
			    	}
				};
				this.receivedUnsolicitedCandidature = function(notifUnsolicitedC){
					var type = notifUnsolicitedC.notifType;
					if(type != null) type=type.toLowerCase();
					var id=notifUnsolicitedC.id;
					var notifId=notifUnsolicitedC.idNotif;
			    	var subject=null;
			    	if(type==UNSOLICITED_CANDIDATURE) subject=UNSOLICITEDAPPLICATIONLABEL;
			    	else subject=unescape(notifUnsolicitedC.subject);
			    	if(subject=="") subject=UNSOLICITEDAPPLICATIONLABEL;
			    	var body=unescape(notifUnsolicitedC.body);
			    	var notifDate = notifUnsolicitedC.dateTime;
			    	var picture=notifUnsolicitedC.picture;
			    	subject=subject.replace("�", "e").replace("�", "e");				
					body=body.replace("�", "e").replace("�", "e");	
			    	//picture = decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(picture)))))))))));
			    	//body = decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(body)))))))))));
			    	//subject = decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(subject)))))))))));
			    	subject=that.factoryFormattingString(subject);
			    	body=that.factoryFormattingString(body);
			    	
			    	/////////////////////////////////////////////////////////////////////////////////////////
			    	
			    	if($('.notifCandidateElement').length != 0){

						$('.no-candidature-style').parent().remove();
						listNotifCandidates.prepend('<li class="notifCandidateElement">'
			                    +'<a href="#" id="linkRecruterNotif'+id+'">'
			                    	+'<div class="notificationID">'+notifId+'</div>'
			                        +'<img class="msg-avatar" src="'+picture+'" alt="avatar" />'
			                        +'<div class="msg-content">'
			                            +'<span class="name">'+subject+'</span>'
			                            +'<span class="msg">'+body+'</span>'
			                        +'</div>'
			                        +'<span class="msg-time">'+timestampsToDateFormated(notifDate)+'</span>'
			                    +'</a>'
			                +'</li>');

						//if($('.notifCandidateElement').length <= 10 ) {
							notificationGlobalCounter++;
							var counter = parseInt(clipNotifHandler.text());
				    		clipNotifHandler.text(counter+1);
				    		document.title = "("+notificationGlobalCounter+") Gestion du stand";
						//}else{
							//$('.notifCandidateElement')[$('.notifCandidateElement').length-1].remove();
							//document.title = "(10+) Gestion du stand";
						//}
					}else{
						//candidatureContainerHandler.append('<span id="clip-notif-index" class="iconbar-unread">1</span>');
						candidatureContainerHandler.append('<span id="clip-notif-index" class="badge badge-info">1</span>');
						//candidateContainerHandler.append('<span id="candidate-notif" class="candidate-notif iconbar-unread" style=" position: absolute; top: 0px; right: 0px; ">1</span>');
						clipNotifHandler=$('#clip-notif-index');
						//candidateLeftHandler=$('#candidate-notif');
						
			    		noCalendarNotificationsHandler.slideUp();
			    		
						listNotifCandidates = $('.fui-clip-content');
						$('.no-candidature-style').parent().remove();
						listNotifCandidates.append('<li class="notifCandidateElement">'
			                    +'<a href="#" id="linkRecruterNotif'+id+'">'
			                    	+'<div class="notificationID">'+notifId+'</div>'
			                        +'<img class="msg-avatar" src="'+picture+'" alt="avatar" />'
			                        +'<div class="msg-content">'
			                            +'<span class="name">'+subject+'</span>'
			                            +'<span class="msg">'+body+'</span>'
			                        +'</div>'
			                        +'<span class="msg-time">'+timestampsToDateFormated(notifDate)+'</span>'
			                    +'</a>'
			                +'</li>');

						notificationGlobalCounter++;
						document.title = "("+notificationGlobalCounter+") Gestion du stand";
					}
					
			    	
					fuiClipContentHandler.append(listNotifCandidates);
		    		
		    		////////////////////////////////////////////////////////////////////////
			    	
			    	$('#linkRecruterNotif'+id).click(function(){
		    			var notificationClickedID = $(this).find('.notificationID').text();
		    			$('#content_userMessages').load(
	    					'userMessages.html',function(){
	    						$('#content_userMessages').show();
	    						userMessagesController.getMessageByID(notificationClickedID);
	    					}
    					);
		    			$('.candidaturesBtn').trigger('click');
		    			
		    			for(var j = 0 ; j < listNotifCandidates.length ; j++ ){
		    				if($(listNotifCandidates[j]).find('.notificationID').text() == notificationClickedID){
		    					listNotifCandidates.splice(j, 1);
		    				}
		    			}
		    			
		    			$(this).parent().remove();
		    			var counter = parseInt(clipNotifHandler.text());
		    			if(counter == 1){
		    				clipNotifHandler.remove();
		    				listNotifCandidates = $('.fui-clip-content');
		    				listNotifCandidates.append('<li>'
					                    +'<a href="#" class="no-candidature-style">'
					                        +'<span class="name">Vous n\'avez aucune notification !</span>'
					                    +'</a>'
					                +'</li>');
		    			}else {
		    				clipNotifHandler.text(counter-1);
		    				//candidateLeftHandler.text(counter+1);
		    			}
		    			
						notificationGlobalCounter = notificationGlobalCounter-1;
		    			
		    			if(notificationGlobalCounter != 0){
		    				document.title = "("+notificationGlobalCounter+") Gestion du stand";
		    			}else{
		    				notificationGlobalCounter = 0;
		    				document.title = "Gestion du stand";
		    			}
		    			
				    	that.notifyShowNotif(id);
			    		
		    		});
			    	
			    	if($('.popover-content-clip').length > (MAX_NOTIFICATION_SHOW-1)){
			    		$('#fui-clip-display').show();
			    	}
				};
				this.factoryFormattingString = function(str){
					if(str!=null){
						str=that.replaceAllAdd(str);
						str=substringCustom(str, MAX_CHARACTERS_SENTENCE_NOTIF);
						str=formattingLongWords(str, MAX_CHARACTERS_WORD_NOTIF);
						str = htmlEncode(str);				
					}
					return str;
			    };
		    	this.replaceAllAdd = function(str){
		    		if(str!=null) return str.split('+').join(" ");
		    		else return str;
		    	};
				this.decodeURINotif = function(notif){
					try {
						notif.subject=decodeURIComponent(notif.subject);
				    }
				    catch(err) {
				        console.log("Error decode");
				    }
				    try {
						notif.body=decodeURIComponent(notif.body);
				    }
				    catch(err) {
				        console.log("Error decode");
				    }
				    try {
						notif.picture=decodeURIComponent(notif.picture);
				    }
				    catch(err) {
				        console.log("Error decode");
				    }
				};
				this.notifLoaded = function (object){					
					that.display(object);
				};
				
				this.notifyNotif = function(){
					$.each(listeners, function(i) {
						listeners[i].getNotifyNotif();
					});
				};	
				this.notifyShowNotif = function(idNotif){
					$.each(listeners, function(i) {
						listeners[i].showNotif(idNotif);
					});
				};
				
				this.addListener = function(list) {
					listeners.push(list);
				};
				//this.initView();
			},
			NotifViewList : function(list) {
				if (!list)
					list = {};
				return $.extend(list);
			},

		});