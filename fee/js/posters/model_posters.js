jQuery.extend({
    Poster: function (data) {
        var that = this;
        this.idPoster = $(data).find("idfile").text();
        this.posterTitle = $(data).find("title").text();
        this.posterDescription = $(data).find("description").text();
        this.posterUrl = $(data).find("url").text();
        this.posterImage = $(data).find("image").text();
        this.fileName = htmlDecode($(data).find("fileName").text());
        this.fileType = new $.FileType($(data).find("fileTypeDTO"));

        this.getIdPoster = function () {
            return this.idPoster;
        };

        this.getPosterTitle = function () {
            return this.posterTitle;
        };

        this.getPosterDescription = function () {
            return this.posterDescription;
        };

        this.getPosterUrl = function () {
            return this.posterUrl;
        };

        this.getPosterImage = function () {
            return this.posterImage;
        };

        this.getFileName = function () {
            return this.fileName;
        };
        this.getFileType = function () {
            return this.fileType;
        };

        this.setIdPoster = function (idPoster) {
            that.idPoster = idPoster;
        };

        this.setPosterTitle = function (posterTitle) {
            that.posterTitle = posterTitle;
        };

        this.setPosterDescription = function (posterDescription) {
            that.posterDescription = posterDescription;
        };

        this.setPosterUrl = function (posterUrl) {
            that.posterUrl = posterUrl;
        };


        this.setPosterImage = function (posterImage) {
            that.posterImage = posterImage;
        };

        this.setFileName = function (posterUrl) {
            that.fileName = posterUrl;
        };

        this.setFileType = function (fileType) {
            that.fileType = fileType;
        };

        this.xmlData = function () {
            var xml = '<allfiles>';
            xml += '<idfile>' + that.idPoster + '</idfile>';
            xml += '<title>' + that.posterTitle + '</title>';
            xml += '<description>' + that.posterDescription + '</description>';
            xml += '<url>' + that.posterUrl + '</url>';
            xml += '<image>' + that.posterImage + '</image>';
            xml += '<fileName>' + htmlEncode(that.fileName) + '</fileName>';
            xml += that.fileType.xmlData();
            xml += '</allfiles>';
            return xml;
        };
    },
    FileType: function (data) {
        var that = this;
        this.fileTypeId = $(data).find("idtypefile").text();
        this.getFileType = function () {
            return that.fileTypeId;
        };
        this.setFileTypeId = function (fileTypeId) {
            that.fileTypeId = fileTypeId;
        };
        this.xmlData = function () {
            var xml = '<fileTypeDTO>';
            xml += '<idtypefile>' + that.fileTypeId + '</idtypefile>';
            xml += '</fileTypeDTO>';
            return xml;
        };
    },
    PostersModel: function () {
        var that = this;
        var listeners = new Array();
        this.initModel = function () {
            that.getStandPosters();
        };
        // lister Poster
        this.getStandPostersSuccess = function (xml) {
            var listPosters = [];
            $(xml).find("allfiles").each(function () {
                var poster = new $.Poster($(this));
                listPosters[poster.idPoster] = poster;
            });
            that.notifyLoadPosters(listPosters);
        };

        this.getStandPostersError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.getStandPosters = function () {
            //$('#listStandPosters').empty();
            genericAjaxCall('GET', staticVars.urlBackEnd + 'allfiles/standPosters?idLang=' + getIdLangParam(), 'application/xml', 'xml', '',
                    that.getStandPostersSuccess, that.getStandPostersError);
        };
        // Add Poster
        this.addPosterSuccess = function (xml) {
            that.notifyAddPosterSuccess();
            that.getStandPosters();
        };

        this.addPosterError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.addPoster = function (poster) {
            genericAjaxCall('POST', staticVars.urlBackEnd + 'allfiles/addFile?idLang=' + getIdLangParam(), 'application/xml', 'xml', poster.xmlData(),
                    that.addPosterSuccess, that.addPosterError);
        };
        // Update Advice
        this.updatePosterSuccess = function (xml) {
            that.notifyUpdatePosterSuccess();
            that.getStandPosters();
        };

        this.updatePosterError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.updatePoster = function (poster) {
            genericAjaxCall('POST', staticVars.urlBackEnd
                    + 'allfiles/updateFile?idLang=' + getIdLangParam(), 'application/xml', 'xml',
                    poster.xmlData(), that.updatePosterSuccess, that.updatePosterError);
        };
        // Delete Advice
        this.deletePosterSuccess = function () {
            that.notifyDeletePosterSuccess();
            that.getStandPosters();
        };

        this.deletePosterError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.deletePoster = function (posterId) {
            genericAjaxCall('GET', staticVars.urlBackEnd
                    + 'allfiles/deleteMedia/' + posterId, 'application/xml', 'xml', '',
                    that.deletePosterSuccess, that.deletePosterError);
        };

        this.getAnalyticsPosters = function(callBack, path) {
            genericAjaxCall('GET', staticVars.urlBackEnd + path,
                    'application/xml', 'xml', '', function(xml) {
                        listPostersCache = [];
                        $(xml).find("allfiles").each(function() {
                            var poster = new $.Poster($(this));
                            listPostersCache[poster.idPoster] = poster;
                        });
                        callBack();
                    }, that.getStandPostersError);
        };

        this.getPosterById = function(posterId) {
            return listPostersCache[posterId];
        };

        this.addListener = function (list) {
            listeners.push(list);
        };
        this.notifyLoadPosters = function (listPosters) {
            $.each(listeners, function (i) {
                listeners[i].loadPosters(listPosters);
            });
        };
        this.notifyAddPosterSuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].addPosterSuccess();
            });
        };
        this.notifyDeletePosterSuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].deletePosterSuccess();
            });
        };
        this.notifyUpdatePosterSuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].updatePosterSuccess();
            });
        };

    },
    PostersModelListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }
});
