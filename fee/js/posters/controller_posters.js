jQuery.extend({
    PostersController: function (model, view) {
        var that = this;
        /**
         * listen to the view
         */
        var vlist = $.PostersViewListener({
            addPoster: function (poster) {
                model.addPoster(poster);
            },
            deletePoster: function (posterId) {
                model.deletePoster(posterId);
            },
            updatePoster: function (poster) {
                model.updatePoster(poster);
            }
        });

        view.addListener(vlist);

        /**
         * listen to the model
         */
        var mlist = $.PostersModelListener({
            loadPosters: function (listPosters) {
                view.displayPosters(listPosters);
            },
            addPosterSuccess: function () {
                view.addPosterSuccess();
            },
            deletePosterSuccess: function (poster) {
                view.viewDeletePosterSuccess(poster);
            },
            updatePosterSuccess: function (poster) {
                view.viewUpdatePosterSuccess(poster);
            }
        });

        model.addListener(mlist);

        this.getAnalyticsPosters = function(callBack, path) {
            model.getAnalyticsPosters(callBack, path);
        };

        this.getPosterById = function(posterId) {
            return model.getPosterById(posterId);
        };
        
        this.initController = function () {
            view.initView();
            model.initModel();
        };
    }
});
