jQuery.extend({
    PostersView: function () {
        var that = this;
        var listeners = new Array();
        var postersCache = [];
        var jqXHRPoster = null;
        var addPosterBtnHandler = null;
        var modalPosterHandler = null;
        var contentHandler = null;
        var loadAdviceErrorHandler = null;
        var progressAdviceHandler = null;
        var posterFormHandler = null;
        var fileinputHandler = null;
        var titlePosterHandler = null;
        var savePosterHandler = null;
        var btnDeleteAdviceHandler = null;
        var validatorPoster = null;

        this.initView = function () {
            traduct();
            contentHandler = $("#mainBackEndContainer");
            addPosterBtnHandler = $("#addPosterLink");
            modalPosterHandler = $("#addPoster");
            loadAdviceErrorHandler = $("#loadPosterError");
            progressAdviceHandler = $("#progressPoster");
            posterFormHandler = $("#posterForm");
            titlePosterHandler = $("#titlePoster");
            fileinputHandler = $(".fileinput-filename");
            savePosterHandler = $("#savePoster");
            btnDeleteAdviceHandler = $("#btnDeletePoster");
            jqXHRPoster = null;

            that.validatePosterForm();
            //Add Advice
            addPosterBtnHandler.unbind("click").bind("click", function (e) {
                that.factoryAddPoster();
            });
            //save advice
            savePosterHandler.unbind("click").bind("click", function (e) {
                that.savePoster();
            });
            //Delete Advice
            btnDeleteAdviceHandler.unbind("click").bind("click", function (e) {
                that.notifyDeletePoster($('#idDeletePoster').val());
            });
            //file input
            $("#posterUpload").fileupload({
                beforeSend: function (jqXHR, settings) {
                    beforeUploadFile('#savePoster', '#progressPoster', '#loadPosterError');
                },
                datatype: 'json',
                cache: false,
                add: function (e, data) {
                    var goUpload = true;
                    var uploadFile = data.files[0];
                    var fileName = uploadFile.name;
                    timeUploadPoster = (new Date()).getTime();
                    data.url = getUrlPostUploadFile(timeUploadPoster, fileName) + "&isPoster=true";
                    if (!(/\.(pdf|jpg|jpeg|png)$/i).test(fileName)) {
                        goUpload = false;
                    }
                    if (uploadFile.size > MAXFILESIZE) {
                        goUpload = false;
                    }
                    if (goUpload == true) {
                        jqXHRPoster = data.submit();
                    } else
                        $('#loadPosterError').show();
                },
                progressall: function (e, data) {
                    progressBarUploadFile('#progressPoster', data, 99);
                },
                done: function (e, data) {
                    var file = data.files[0];
                    var fileName = file.name;
                    //console.log(timeUploadPoster);
                    var hashFileName = getUploadFileName(timeUploadPoster, fileName);
                    console.log("Add poster file done, filename :" + fileName + ", hashFileName : " + hashFileName);
                    $('#loadPosterError').show();
                    $('#loadPoster .error').hide();
                    $('.fileInfo').text(fileName);
                    $('.fileInfo').attr('href', getUploadFileUrl(hashFileName));
                    //afterUploadFile('#savePoster', '#posterLoaded', '#progressPoster,#loadPosterError');
                }
            });


            //datatable
            $('#listPostersTable').dataTable({
                "language": {
                    url: 'plugins/datatables/'+getIdLangParam()+'.json'
                },
                "aaData": [],
                "aaSorting": [],
                "aoColumns": [
                    {
                        "sTitle": TITLELABEL
                    },
                    {
                        "sTitle": CONTENTLABEL
                    },
                    {
                        "sTitle": ACTIONSLABEL
                    }
                ],
                "bFilter": false,
                "bLengthChange": false,
                "bAutoWidth": false,
                "bRetrieve": false,
                "bDestroy": true,
                "iDisplayLength": 10,
                "fnDrawCallback": function () {
                    $('.viewAdviceLink').off();
                    $('.viewAdviceLink').on("click", function () {
                        var posterId = $(this).closest('div').attr('id');
                        that.factoryViewPoster(postersCache[posterId]);

                    });
//
                    $('.editAdviceLink').off();
                    $('.editAdviceLink').on("click", function () {
                        var posterId = $(this).closest('div').attr('id');
                        that.factoryUpdatePoster(postersCache[posterId]);
                    });
//
                    $('.deleteAdviceLink').off();
                    $('.deleteAdviceLink').on("click", function () {
                        var posterId = $(this).closest('div').attr('id');
                        $('#removePoster').modal();
                        $('#idDeletePoster').val(posterId);
                    });
                }
            });
        };
        this.displayPosters = function (posters) {
            postersCache = posters;

            var rightGroup = listStandRights[rightGroupEnum.POSTERS];
            if (rightGroup != null) {
                var permittedPostersNbr = rightGroup.getRightCount();
                var postersNbr = Object.keys(posters).length;
                if (permittedPostersNbr == -1) {
                    $('#addPosterDiv #addPosterLink').text(ADDLABEL);
                    $('#addPosterDiv').show();
                } else if (postersNbr < permittedPostersNbr) {
                    $('#numberPosters').text(postersNbr);
                    $('#numberPermittedPosters').text(permittedPostersNbr);
                    $('#addPosterDiv').show();
                } else {
                    $('#addPosterDiv').hide();
                }
            }

            var posterList = [];
            var poster = null;
            var posterId = null;
            var posterUrl = null;
            var posterTitle = null;
            var posterDescription = null;
            var posterImage = null;
            var data = null;
            var listActions = null;
            var posterImg = null;
            var posterContent = null;

            for (var index in posters) {
                poster = posters[index];
                posterId = poster.getIdPoster();
                posterUrl = poster.getPosterUrl();
                posterTitle = poster.getPosterTitle();
                posterDescription = poster.getPosterDescription();
                posterImage = poster.getPosterImage();
                posterImg = "<img class='apercuImg' src='" + posterImage + "' alt='" + posterTitle + "' />";
                posterContent = "<div class='contentTruncate'>" + posterTitle.replace(/<(?:.|\n)*?>/gm, '') + "</div>";
                listActions = "<div class='actionBtn' id='" + posterId + "'><a href='javascript:void(0);' class='editAdviceLink editLink actionBtn'><i class='fa fa-pencil-square-o'></i></a> <a href='javascript:void(0);' class='viewAdviceLink viewLink actionBtn'><i class='fa fa-eye'></i></a> <a href='javascript:void(0);' class='deleteAdviceLink deleteLink actionBtn'><i class='fa fa-trash-o'></i></a></div>";
                data = [posterImg, posterContent, listActions];
                posterList.push(data);
            }
            addTableDatas("#listPostersTable", posterList);
            //oTable.fnDraw();

            $('#loadingEds').hide();
        };
        //Add Poster
        this.factoryAddPoster = function () {
            that.update = false;
            modalPosterHandler.modal();
            validatorPoster.resetForm();
            loadAdviceErrorHandler.hide();
            progressAdviceHandler.hide();
            $('.fileInfo').attr('src');
            $('.fileInfo').text('');
            posterFormHandler[0].reset();
        };
        this.addPosterSuccess = function () {
            modalPosterHandler.modal('hide');
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        //Save Poster
        this.savePoster = function () {
            that.validatePosterForm();
            if ($("#posterForm").valid()) {
                var poster = new $.Poster();
                poster.setIdPoster(0);
                poster.setPosterTitle($('#posterTitle').val());
                poster.setPosterDescription($('#posterDescription').val());
                poster.setPosterUrl($('.fileInfo').attr('href'));
                poster.setFileName($('.fileInfo').text());

                if (getFileExtension(poster.getFileName()) == "pdf") {
                    poster.setPosterImage(poster.getPosterUrl() + '.png');
                } else {
                    poster.setPosterImage(poster.getPosterUrl());
                }

                var fileType = new $.FileType();
                fileType.setFileTypeId(fileTypeEnum.POSTER);
                poster.setFileType(fileType);

                if (!that.update) {
                    that.notifyAddPoster(poster);
                }
                else {
                    poster.setIdPoster($('#idPoster').val());
                    that.notifyUpdatePoster(poster);
                }

            }
            ;
        };
        // Update Advice
        this.factoryUpdatePoster = function (poster) {
            that.update = true;
            $('#addPoster').modal();
            validatorPoster.resetForm();            
            $('#idPoster').val(poster.getIdPoster());
            $('#posterTitle').val(poster.getPosterTitle());
            $('#posterDescription').val(poster.getPosterDescription());
            $('.fileInfo').attr('href', poster.getPosterUrl());
            $('.fileInfo').text(poster.getFileName());
        };

        this.viewUpdatePosterSuccess = function () {
            modalPosterHandler.modal('hide');
            modalPosterHandler.find('#posterForm')[0].reset();
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        // View Advice
        this.factoryViewPoster = function (poster) {
            var posterUrl = poster.getPosterUrl();
            var posterTitle = poster.getPosterTitle();
            $('#viewPoster').modal();
            $('#documentTitlePopup').text(posterTitle);
            var pdf_container = $('<object data="mozilla-pdf.js/web/viewer.html?file=' + posterUrl + '" type="text/html" width="100%" height="100%"></object>');
            var img_container = $('<img src="' + posterUrl + '" title="' + posterTitle + '" alt="' + posterTitle + '" />');
            if (compareToLowerString(posterUrl, ".pdf") != -1) {
                $('#contentViewPoster').html(pdf_container);
            } else {
                $('#contentViewPoster').html(img_container);
            }

        };
        // Delete Poster
        this.viewDeletePosterSuccess = function () {
            modalPosterHandler.modal('hide');
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        // Validator form
        this.validatePosterForm = function () {
            $.validator.addMethod("uploadFile", function (val, element) {
                //console.log(val);
                var fileName = $(".fileInfo").text();
                var fileUrl = $(".fileInfo").attr("href");
                if (fileName == '' || fileUrl == '') {
                    return false;
                } else {
                    return true;
                }

            }, "File type error");

            validatorPoster = $("#posterForm").validate({
                rules: {
                    posterTitle: {
                        required: true,
                        minlength: 6
                    }
                }
            });
            $("#posterForm #posterUpload").rules("add", {
                uploadFile: true
            });
        };
        this.addListener = function (list) {
            listeners.push(list);
        };
        this.notifyAddPoster = function (poster) {
            $.each(listeners, function (i) {
                listeners[i].addPoster(poster);
            });
        };
        this.notifyDeletePoster = function (posterId) {
            $.each(listeners, function (i) {
                listeners[i].deletePoster(posterId);
            });
        };
        this.notifyUpdatePoster = function (poster) {
            $.each(listeners, function (i) {
                listeners[i].updatePoster(poster);
            });
        };

    },
    PostersViewListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }

});
