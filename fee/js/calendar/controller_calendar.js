jQuery.extend({
    CalendarController: function (model, view) {
        var that = this;
        /**
         * listen to the view
         */
        var vlist = $.CalendarViewListener({
            addEvent: function (event) {
                model.modelAdddEvent(event);
            },
            updateEvent: function (event) {
                model.modelUpdateEvent(event);
            },
            deleteEvent: function (idEvent) {
                model.modelDeleteEvent(idEvent);
            }
        });

        view.addListener(vlist);

        /**
         * listen to the model
         */
        var mlist = $.CalendarModelListener({
            addEventSuccess: function (event) {
                view.viewAddEventSuccess(event);
            },
            updateEventSuccess: function (event) {
                view.viewUpdateEventSuccess(event);
            },
            updateEventError: function (event) {
                view.viewUpdateEventError(event);
            },
            deleteEventSuccess: function (event) {
                view.viewDeleteEventSuccess(event);
            },
            deleteEventError: function (event) {
                view.viewDeleteEventError(event);
            },
            listEventNotify: function (json) {
                view.viewListEventNotify(json);
            }
        });

        model.addListener(mlist);

        this.initController = function () {
            view.initView();
            model.initModel();
        };
    }
});
