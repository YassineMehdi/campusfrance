jQuery.extend({
    EventAgenda: function (data) {
        var that = this;
        this.idEvent = $(data).find("idEvent").text();
        this.eventTitle = $(data).find("eventTitle").text();
        this.recurrent = $(data).find("recurrent").text();
        this.eventType = $(data).find("idEventType").val();
        this.eventDescription = $(data).find("eventDescription").text();
        this.beginDate = $(data).find("beginDate").text();
        this.endDate = $(data).find("endDate").text();

        this.getIdEvent = function () {
            return that.idEvent;
        };
        this.setIdEvent = function (idEvent) {
            that.idEvent = idEvent;
        };
        this.getEventTitle = function () {
            return that.eventTitle;
        };
        this.setEventTitle = function (eventTitle) {
            that.eventTitle = eventTitle;
        };
        this.getRecurrent = function () {
            return that.recurrent;
        };
        this.setRecurrent = function (recurrent) {
            that.recurrent = recurrent;
        };
        this.getEventType = function () {
            return that.eventType;
        };
        this.setEventType = function (eventType) {
            that.eventType = eventType;
        };
        this.getEventDescription = function () {
            return that.eventDescription;
        };
        this.setEventDescription = function (eventDescription) {
            that.eventDescription = eventDescription;
        };
        this.getBeginDate = function () {
            return that.beginDate;
        };
        this.setBeginDate = function (beginDate) {
            that.beginDate = beginDate;
        };
        this.getEndDate = function () {
            return that.endDate;
        };
        this.setEndDate = function (endDate) {
            that.endDate = endDate;
        };
        this.xmlData = function () {
            var xml = "<event><idEvent>" + that.idEvent + "</idEvent><eventTitle>"
                    + that.eventTitle
                    + "</eventTitle><recurrent>"
                    + "</recurrent><idEventType>"
                    + that.eventType
                    + "</idEventType><eventDescription>"
                    + that.eventDescription
                    + "</eventDescription><beginDate>"
                    + getAgendaUTCDateEvent(that.beginDate)
                    + "</beginDate><endDate>"
                    + getAgendaUTCDateEvent(that.endDate)
                    + "</endDate></event>";
            return xml;
        };
    },
    CalendarModel: function () {
        var that = this;
        var listeners = new Array();
        this.initModel = function () {
            that.modelListEvent();
        };

        this.addEventSuccess = function (xml) {
            that.notifyAddEventSuccess();
            that.modelListEvent();
        };

        this.addEventError = function (xhr, status, error) {
            console.log(xhr);
            console.log(status);
            console.log(error);
        };

        this.modelAdddEvent = function (event) {
            var url = staticVars.urlBackEnd + 'agenda/add?idLang=' + getIdLangParam();
            genericAjaxCall('POST', url, 'application/xml', 'xml', event.xmlData(),
                    that.addEventSuccess, that.addEventError);
        };

        this.updateEventSuccess = function (xml) {
            that.notifyUpdateEventSuccess();
            that.modelListEvent();
        };

        this.updateEventError = function (xhr, status, error) {
            that.notifyUpdateEventError();
        };

        this.modelUpdateEvent = function (event) {
            var url = staticVars.urlBackEnd + 'agenda/update?forumId=' + forumId;
            genericAjaxCall('POST', url, 'application/xml', 'xml', event.xmlData(),
                    that.updateEventSuccess, that.updateEventError);
        };

        this.deleteEventSuccess = function (xml) {
            that.notifyDeleteEventSuccess();
            that.modelListEvent();
        };

        this.deleteEventError = function (xhr, status, error) {
            that.notifyDeleteEventError();
        };
        this.modelDeleteEvent = function (idEvent) {
            var url = staticVars.urlBackEnd + 'agenda/delete?idEvent=' + idEvent + '&forumId=' + forumId;
            genericAjaxCall('POST', url, 'application/xml', 'xml', '',
                    that.deleteEventSuccess, that.deleteEventError);
        };

        this.modelListEvent = function () {
            var url = staticVars.urlBackEnd + 'agenda?idLang=' + getIdLangParam();
            genericAjaxCall('GET', url, 'application/xml', 'json', '',
                    that.listEventSuccess, that.listEventError);
        };

        this.listEventSuccess = function (json) {
            that.notifyListEvent(json);
        };

        this.listEventError = function (xhr, status, error) {
            console.log("Error List");
        };

        this.addListener = function (list) {
            listeners.push(list);
        };
        this.notifyAddEventSuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].addEventSuccess();
            });
        };
        this.notifyUpdateEventSuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].updateEventSuccess();
            });
        };
        this.notifyUpdateEventError = function () {
            $.each(listeners, function (i) {
                listeners[i].updateEventError();
            });
        };
        this.notifyDeleteEventSuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].deleteEventSuccess();
            });
        };
        this.notifyDeleteEventError = function () {
            $.each(listeners, function (i) {
                listeners[i].deleteEventError();
            });
        };
        this.notifyListEvent = function (json) {
            $.each(listeners, function (i) {
                listeners[i].listEventNotify(json);
            });
        };

    },
    CalendarModelListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }
});
