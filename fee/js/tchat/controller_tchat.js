jQuery.extend({

	TchatController: function(model, view){
		var that = this;
		/**
		 * listen to the view
		 */
		var vlist = new $.ChatViewListener({
			getCurrentsChat : function() {
				model.getCurrentsChat();
			},
			updateEnterprisePInfo : function(enterpriseP){
				model.updateEnterprisePInfo(enterpriseP);
			}
		});
		view.addListener(vlist);

		/**
		 * listen to the model
		 */
		var mlist = $.ChatModelListener({			
			loadEnterprisePInfo : function(enterpriseP) {
				view.loadEnterprisePInfo(enterpriseP);
			},
			loadCurrentsChat : function(json){
				view.loadCurrentsChat(json);
			},
			updateEnterprisePInfoSuccess : function(){
				view.updateEnterprisePInfoSuccess();
			}
		});
		
		model.addListener(mlist);
		
		this.initController = function(){
			view.initView();
			model.initModel();
		};
		this.reconnectToChat = function(){
			view.reconnectToChat();
		};
	}
});