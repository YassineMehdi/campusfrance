jQuery.extend({
	
    UserProfile: function (data) {
        var that = this;
        that.userProfileId = '';
        that.firstName = '';
        that.secondName = '';
        that.avatar = '';
        that.pseudoSkype = '';
		that.defaultLanguage = null;

        this.userProfile = function (data) {
            if ($(data).find("userProfileId"))
                that.userProfileId = $(data).find("userProfileId").text();
            if ($(data).find("firstName"))
                that.firstName = $(data).find("firstName").text();
            if ($(data).find("secondName"))
                that.secondName = $(data).find("secondName").text();
            if ($(data).find("avatar"))
                that.avatar = $(data).find("avatar").text();
            if ($(data).find("pseudoSkype"))
                that.pseudoSkype = $(data).find("pseudoSkype").text();
			if ($(data).find("defaultLanguageDTO"))
				that.defaultLanguage = new $.Language($(data).find("defaultLanguageDTO"));
        },
        this.getUserProfileId = function () {
            return that.userProfileId;
        },
        this.getFirstName = function () {
            return that.firstName;
        },
        this.getSecondName = function () {
            return that.secondName;
        },
        this.getAvatar = function () {
            return that.avatar;
        },
        this.getPseudoSkype = function () {
            return that.pseudoSkype;
        },
		this.getDefaultLanguage = function() {
			return that.defaultLanguage;
		};
        this.setUserProfileId = function (userProfileId) {
            that.userProfileId = userProfileId;
        },
        this.setFirstName = function (firstName) {
            that.firstName = firstName;
        },
        this.setSecondName = function (secondName) {
            that.secondName = secondName;
        },
        this.setAvatar = function (avatar) {
            that.avatar = avatar;
        },
        this.setPseudoSkype = function (pseudoSkype) {
            that.pseudoSkype = pseudoSkype;
        },
		this.setDefaultLanguage = function(defaultLanguage) {
			that.defaultLanguage = defaultLanguage;
		};
        this.update = function (newData) {
            that.userProfile(newData);
        };
        this.xmlData = function () {
            var xml = '<userProfileDTO>';
            xml += '<userProfileId>' + that.userProfileId + '</userProfileId>';
            xml += '<firstName>' + htmlEncode(that.firstName) + '</firstName>';
            xml += '<secondName>' + htmlEncode(that.secondName) + '</secondName>';
            xml += '<avatar>' + htmlEncode(that.avatar) + '</avatar>';
            xml += '<pseudoSkype>' + htmlEncode(that.pseudoSkype) + '</pseudoSkype>';
            if (that.defaultLanguage != null) {
				xml += that.defaultLanguage.xmlData().replace(/languageDTO>/g, "defaultLanguageDTO>");
			}
            xml += '</userProfileDTO>';
            return xml;
        };

        this.userProfile(data);
    },
    
    EnterpriseP: function (data) {
        var that = this;
        that.enterprisePId = '';
        that.jobTitle = '';
        that.isAdmin = false;
        that.login='';
		that.password='';
        that.email='';
		that.description='';
	    that.cellPhone = '';
        that.userProfile = '';
        
        this.enterpriseP = function (data) {
            if ($(data).find("identrprise"))
                that.enterprisePId = $(data).find("identrprise").text();
			if($(data).find("login")) 
				that.login=$(data).find("login").text();
			if($(data).find("password")) 
				that.password=$(data).find("password").text();
            if ($(data).find("jobTitle"))
                that.jobTitle = $(data).find("jobTitle").text();
            if ($(data).find("isAdmin") && $(data).find("isAdmin").text() == "true")
                that.isAdmin = true;
			if($(data).find("email")) 
				that.email=$(data).find("email").text();
			if($(data).find("description")) 
				that.description=$(data).find("description").text();
	        if ($(data).find("cellPhone"))
			          that.cellPhone = $(data).find("cellPhone").text();
            if ($(data).find("userProfileDTO"))
                that.userProfile = new $.UserProfile($(data).find("userProfileDTO"));
        },
		this.getLogin = function(){	
			return that.login;
		};
		this.getPassword = function(){	
			return that.password;
		};
        this.getEnterprisePId = function () {
            return that.enterprisePId;
        },
        this.getJobTitle = function () {
            return that.jobTitle;
        },
        this.getIsAdmin = function () {
            return that.isAdmin;
        },
        this.getUserProfile = function () {
            return that.userProfile;
        },
		this.getEmail = function(){	
			return that.email;
		};
		this.getDescription = function(){	
			return that.description;
		};
	    this.getCellPhone = function () {
	      return that.cellPhone;
	    };	
		this.setLogin = function(login){
			that.login=login;
		};
		this.setPassword = function(password){
			that.password=password;
		};
        this.setEnterprisePId = function (enterprisePId) {
            that.enterprisePId = enterprisePId;
        },
		this.setEmail = function(email){
			that.email=email;
		};
        this.setJobTitle = function (jobTitle) {
            that.jobTitle = jobTitle;
        },
        this.setIsAdmin = function (isAdmin) {
            that.isAdmin = isAdmin;
        },
        this.setDescription = function(description){
			that.description=description;
		};
	    this.setCellPhone = function (cellPhone) {
	      that.cellPhone = cellPhone;
	    };
        this.setUserProfile = function (userProfile) {
            that.userProfile = userProfile;
        },
        this.xmlData = function () {
            var xml = '<enterprisep>';
            xml += '<identrprise>' + that.enterprisePId + '</identrprise>';
			xml+='<login>'+htmlEncode($.trim(that.login))+'</login>';
			xml+='<password>'+htmlEncode($.trim(that.password))+'</password>';
			xml+='<email>'+htmlEncode($.trim(that.email))+'</email>';
			xml+='<cellPhone>'+htmlEncode($.trim(that.cellPhone))+'</cellPhone>';
            xml += '<jobTitle>' + htmlEncode(that.jobTitle) + '</jobTitle>';
            xml += '<isAdmin>' + htmlEncode(that.isAdmin) + '</isAdmin>';
			xml+='<description>'+htmlEncode($.trim(that.description))+'</description>';
            if (that.userProfile != "")
                xml += that.userProfile.xmlData();
            xml += '</enterprisep>';
            return xml;
        };

        this.enterpriseP(data);
    },
    
	TchatModel: function(){
		var that = this;

		var listeners = new Array();
		
		this.initModel = function(){
			that.getEnterprisePInfo();
		};
		
		this.getEnterprisePInfoSuccess = function(data){
			var enterpriseP = new $.EnterpriseP($(data));
			/*if(enterpriseP.getIsAdmin()){
				window.location.replace("backend.html?idLang="+getIdLangParam());
			}*/
			that.notifyLoadEnterprisePInfo(enterpriseP);
		},
		
		this.getEnterprisePInfoError = function(xhr, status, error){
			isSessionExpired(xhr.responseText);
		},
		
		this.getEnterprisePInfo = function(){
			genericAjaxCall('GET', staticVars.urlBackEnd+'enterprisep/getInfos', 'application/xml', 'xml', 
					'', that.getEnterprisePInfoSuccess, that.getEnterprisePInfoError);
		},

		this.getCurrentsChatSuccess = function(json){
			that.notifyLoadCurrentsChat(json);
		},
		
		this.getCurrentsChatError = function(xhr, status, error){
			isSessionExpired(xhr.responseText);
		},
		this.getCurrentsChat = function(){
			genericAjaxCall('POST', staticVars.urlBackEnd+'agenda?forChat=true&idLang='+getIdLangParam(),
		    		'application/json', 'json', '', that.getCurrentsChatSuccess, that.getCurrentsChatError);
		},
		this.updateEnterprisePInfoSuccess = function(xml){
			that.notifyUpdateEnterprisePInfoSuccess();
		},
		this.updateEnterprisePInfoError = function(xhr, status, error){
			isSessionExpired(xhr.responseText);
		},
		this.updateEnterprisePInfo = function(enterpriseP){
			genericAjaxCall('POST', staticVars.urlBackEnd+'enterprisep/update/chat',
		    		'application/xml', 'xml', enterpriseP.xmlData(), that.updateEnterprisePInfoSuccess, that.updateEnterprisePInfoError);
		},
		this.notifyLoadEnterprisePInfo = function(enterpriseP){
			$.each(listeners, function(i){			
				listeners[i].loadEnterprisePInfo(enterpriseP);
			});
		};
		this.notifyLoadCurrentsChat = function(json){
			$.each(listeners, function(i){			
				listeners[i].loadCurrentsChat(json);
			});
		};
		this.notifyUpdateEnterprisePInfoSuccess = function(){
			$.each(listeners, function(i){			
				listeners[i].updateEnterprisePInfoSuccess();
			});
		};
		this.addListener = function(list){
			listeners.push(list);
		};
		
	},
		
	ChatModelListener: function(list) {
		if(!list) list = {};
		return $.extend(list);
	},
});
