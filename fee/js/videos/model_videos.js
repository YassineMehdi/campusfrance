jQuery.extend({
    VideosModel: function () {
        var that = this;
        var listeners = new Array();
        this.initModel = function () {
            that.getStandVideos();
        };

        // lister Videos
        this.getStandVideosSuccess = function (xml) {
            var listVideos = [];
            $(xml).find("allfiles").each(function () {
                var video = new $.File($(this));
                listVideos[video.getIdFile()] = video;
            });
            that.notifyLoadVideos(listVideos);
        };

        this.getStandVideosError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.getStandVideos = function () {
            //$('#listStandPosters').empty();
            genericAjaxCall('GET', staticVars.urlBackEnd + 'allfiles/standVideos?idLang=' + getIdLangParam(), 'application/xml', 'xml', '',
                    that.getStandVideosSuccess, that.getStandVideosError);
        };

        this.getAnalyticsVideos = function(callBack, path) {
            genericAjaxCall('GET', staticVars.urlBackEnd + path,
                    'application/xml', 'xml', '', function(xml) {
                        listVideosCache = [];
                        $(xml).find("allfiles").each(function() {
                            var video = new $.Video($(this));
                            listVideosCache[video.idVideo] = video;
                        });
                        callBack();
                    }, that.getStandVideosError);

        };

        this.getVideoById = function(videoId) {
            return listVideosCache[videoId];
        };
        // Add Videos
        this.addVideoSuccess = function (xml) {
            that.notifyAddVideoSuccess();
            that.getStandVideos();
        };

        this.addVideoError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.addVideo = function (video) {
            genericAjaxCall('POST', staticVars.urlBackEnd
                    + 'allfiles/addFile?idLang=' + getIdLangParam(), 'application/xml', 'xml', video.xmlData(),
                    that.addVideoSuccess, that.addVideoError);
        };
        // Update Videos
        this.updateVideoSuccess = function (xml) {
            that.notifyUpdateVideoSuccess();
            that.getStandVideos();
        };

        this.updateVideoError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.updateVideo = function (video) {
            genericAjaxCall('POST', staticVars.urlBackEnd
                    + 'allfiles/updateFile?idLang=' + getIdLangParam(), 'application/xml', 'xml', video.xmlData(),
                    that.updateVideoSuccess, that.updateVideoError);
        };
        // Delete Videos
        this.deleteVideoSuccess = function (xml) {
            that.notifyDeleteVideoSuccess();
            that.getStandVideos();
        };

        this.deleteVideoError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.deleteVideo = function (videoId) {
            genericAjaxCall('GET', staticVars.urlBackEnd + 'allfiles/deleteMedia/' + videoId, 'application/xml', 'xml', '',
                    that.deleteVideoSuccess, that.deleteVideoSuccess);
        };

        this.addListener = function (list) {
            listeners.push(list);
        };

        this.notifyLoadVideos = function (listVideos) {
            $.each(listeners, function (i) {
                listeners[i].loadVideos(listVideos);
            });
        };
        this.notifyAddVideoSuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].addVideoSuccess();
            });
        };
        this.notifyUpdateVideoSuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].updateVideoSuccess();
            });
        };
        this.notifyDeleteVideoSuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].deleteVideoSuccess();
            });
        };
    },
    VideosModelListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }
});
