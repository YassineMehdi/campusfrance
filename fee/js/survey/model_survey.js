
jQuery.extend({
	
	Survey:  function(data){
		var that = this;
		that.surveys='';
		
		this.survey = function(data){
			if(data){
				that.surveys = new Array();	
				$(data).find("formDTO").each(function(){	
					var survey = new $.Form($(this));							
					that.surveys.push(survey);	
				});
			}
		},
		
		this.getSurveys= function(){	
			return that.surveys;
		},
		
		this.setSurveys = function(surveys){
			that.surveys = surveys;
		};
		
		this.survey(data);
		
	},
	
	Form: function(data){
		var that = this;
		that.formId='';
		that.formTitle='';
		that.formBannerUrl='';
		that.formDescription='';
		that.formFields='';
		that.surveyType = '';
		that.formType = '';
		
		this.form = function(data){
			if(data){
				if($(data).find("formId")) that.formId=$(data).find("formId").text();
				if($(data).find("formTitle")) that.formTitle=$(data).find("formTitle").text();
				if($(data).find("formBannerUrl")) that.formBannerUrl=$(data).find("formBannerUrl").text();
				if($(data).find("formDescription")) that.formDescription=$(data).find("formDescription").text();
				that.formFields = new Array();	
				$(data).find("formFieldDTO").each(function(){	
					var formField = new $.FormField($(this));							
					that.formFields.push(formField);	
				});
				if($(data).find("surveyTypeDTO")) that.surveyType=new $.SurveyType($(data).find("surveyTypeDTO"));
				if($(data).find("formTypeDTO")) that.formType=new $.FormType($(data).find("formTypeDTO"));
			}
		},
		
		this.getFormId= function(){	
			return that.formId;
		},
		
		this.getFormTitle = function(){	
			return that.formTitle;
		},

		this.getFormBannerUrl = function(){	
			return that.formBannerUrl;
		},
		
		this.getFormDescription = function(){	
			return that.formDescription;
		},
		
		this.getFormFields = function(){	
			return that.formFields;
		},

		this.getSurveyType= function(){	
			return that.surveyType;
		},
		
		this.getFormType= function(){	
			return that.formType;
		},
		
		this.setFormId = function(formId){
			that.formId=formId;
		},
		
		this.setFormTitle = function(formTitle){
			that.formTitle=formTitle;
		},
		
		this.setFormBannerUrl = function(formBannerUrl){
			that.formBannerUrl=formBannerUrl;
		},
		
		this.setFormDescription = function(formDescription){
			that.formDescription=formDescription;
		},
		
		this.setFormFields = function(formFields){
			that.formFields=formFields;
		},

		this.setSurveyType = function(surveyType){
			that.surveyType = surveyType;
		},
		
		this.setFormType = function(formType){
			that.formType = formType;
		},
		
		this.update = function(newData){
			that.website(newData);
		};
		
		this.xmlData = function(){
			var xml='<formDTO>';
			xml+='<formId>'+that.formId+'</formId>';
			xml+='<formTitle>'+htmlEncode(that.formTitle)+'</formTitle>';
			xml+='<formBannerUrl>'+htmlEncode(that.formBannerUrl)+'</formBannerUrl>';
			xml+='<formDescription>'+htmlEncode(that.formDescription)+'</formDescription>';
			xml+='<formFieldDTOList>';
			for (index in that.formFields) {
				var formField = that.formFields[index];
				xml+=formField.xmlData();
			}
			xml+='</formFieldDTOList>';
			if(that.surveyType != "") 
				xml+=that.surveyType.xmlData();
			if(that.formType != "") 
				xml+=that.formType.xmlData();
			xml+='</formDTO>';
			return xml;
		};
		
		this.form(data);
	},

	FormField: function(data){
		var that = this;
		that.formFieldId = '';
		that.formFieldLabel = '';
		that.formFieldValue = '';
		that.required = '';
		that.generic = false;
		that.position = '';
		that.field = '';
		
		this.formField = function(data){
			if($(data).find("formFieldId")) that.formFieldId=$(data).find("formFieldId").text();
			if($(data).find("formFieldLabel")) that.formFieldLabel=$(data).find("formFieldLabel").text();
			if($(data).find("formFieldValue")) that.formFieldValue=$(data).find("formFieldValue").text();
			if($(data).find("required")) that.required=$(data).find("required").text();
			if($(data).find("generic") && $(data).find("generic:first").text() == "true") 
				that.generic=true;
			if($(data).find("position")) that.position=$(data).find("position").text();
			if($(data).find("fieldDTO")) that.field=new $.Field($(data).find("fieldDTO"));
		},
		
		this.getFormFieldId = function(){	
			return that.formFieldId;
		},
		
		this.getFormFieldLabel = function(){	
			return that.formFieldLabel;
		},
		
		this.getRequired = function(){		
			return that.required;
		},
		
		this.getGeneric = function(){		
			return that.generic;
		},
		
		this.getFormFieldValue = function(){	
			return that.formFieldValue;
		},
		
		this.getPosition = function(){		
			return that.position;
		},
		
		this.getField = function(){		
			return that.field;
		},
		
		this.setFormFieldId = function(formFieldId){
			that.formFieldId=formFieldId;
		},
		
		this.setFormFieldLabel = function(formFieldLabel){
			that.formFieldLabel=formFieldLabel;
		},
		
		this.setRequired = function(required){
			that.required=required;
		},

		this.setGeneric = function(generic){		
			that.generic = generic;
		},

		this.setFormFieldValue = function(formFieldValue){
			that.formFieldValue = formFieldValue;
		},
		
		this.setPosition = function(position){
			that.position=position;
		},
		
		this.setField = function(field){		
			that.field = field;
		},
		
		this.update = function(newData){
			that.websiteType(newData);
		};
		
		this.xmlData = function(){
			var xml='<formFieldDTO>';
			xml+='<formFieldId>'+that.formFieldId+'</formFieldId>';
			xml+='<formFieldLabel>'+htmlEncode(that.formFieldLabel)+'</formFieldLabel>';
			xml+='<required>'+that.required+'</required>';
			xml+='<generic>'+that.generic+'</generic>';
			xml+='<position>'+that.position+'</position>';
			xml+='<formFieldValue>'+htmlEncode(that.formFieldValue)+'</formFieldValue>';
			xml+=that.field.xmlData();
			xml+='</formFieldDTO>';
			return xml;
		};
		
		this.formField(data);
	},
	
	SurveyType: function(data){
		var that = this;
		that.surveyTypeId='';
		that.surveyTypeName='';
		
		this.surveyType = function(data){
			if(data){
				if($(data).find("surveyTypeId")) that.surveyTypeId=$(data).find("surveyTypeId").text();
				if($(data).find("surveyTypeName")) that.surveyTypeName=$(data).find("surveyTypeName").text();
			}
		},
		
		this.getSurveyTypeId= function(){	
			return that.surveyTypeId;
		},
		
		this.getSurveyTypeName = function(){	
			return that.surveyTypeName;
		},

		this.setSurveyTypeId = function(surveyTypeId){
			that.surveyTypeId=surveyTypeId;
		},
		
		this.setSurveyTypeName = function(surveyTypeName){
			that.surveyTypeName=surveyTypeName;
		},
		
		this.xmlData = function(){
			var xml='<surveyTypeDTO>';
			xml+='<surveyTypeId>'+that.surveyTypeId+'</surveyTypeId>';
			xml+='<surveyTypeName>'+htmlEncode(that.surveyTypeName)+'</surveyTypeName>';
			xml+='</surveyTypeDTO>';
			return xml;
		};
		
		this.surveyType(data);
	},
	
	FormType: function(data){
		var that = this;
		that.formTypeId='';
		that.formTypeName='';
		
		this.formType = function(data){
			if(data){
				if($(data).find("formTypeId")) that.formTypeId=$(data).find("formTypeId").text();
				if($(data).find("formTypeName")) that.formTypeName=$(data).find("formTypeName").text();
			}
		},
		
		this.getFormTypeId= function(){	
			return that.formTypeId;
		},
		
		this.getFormTypeName = function(){	
			return that.formTypeName;
		},

		this.setFormTypeId = function(formTypeId){
			that.formTypeId=formTypeId;
		},
		
		this.setFormTypeName = function(formTypeName){
			that.formTypeName=formTypeName;
		},
		
		this.xmlData = function(){
			var xml='<formTypeDTO>';
			xml+='<formTypeId>'+that.formTypeId+'</formTypeId>';
			xml+='<formTypeName>'+htmlEncode(that.formTypeName)+'</formTypeName>';
			xml+='</formTypeDTO>';
			return xml;
		};
		
		this.formType(data);
	},
	
	InputType: function(data){
		var that = this;
		that.inputTypeId = '';
		that.inputTypeName = '';
		
		this.inputType = function(data){
			if($(data).find("inputTypeId")) that.inputTypeId=$(data).find("inputTypeId").text();
			if($(data).find("inputTypeName")) that.inputTypeName=$(data).find("inputTypeName").text();
		},
		
		this.getInputTypeId = function(){	
			return that.inputTypeId;
		},		
		
		this.getInputTypeName = function(){	
			return that.inputTypeName;
		},
		
		this.setInputTypeId = function(inputTypeId){
			that.inputTypeId=inputTypeId;
		},
		
		this.setInputTypeName = function(inputTypeName){
			that.inputTypeName = inputTypeName;
		},
		
		this.update = function(newData){
			that.inputType(newData);
		};
		
		this.xmlData = function(){
			var xml='<inputTypeDTO>';
			xml+='<inputTypeId>'+that.inputTypeId+'</inputTypeId>';
			xml+='<inputTypeName>'+htmlEncode(that.inputTypeName)+'</inputTypeName>';
			xml+='</inputTypeDTO>';
			return xml;
		};
		
		this.inputType(data);
	},
	
	AttendData: function(data){
		var that = this;
		that.attendDataId = 0;
		that.value='';
		that.field='';
		
		this.attendData = function(data){
			if(data){
				if($(data).find("attendDataId")) that.attendDataId=$(data).find("attendDataId").text();
				if($(data).find("value")) that.value=$(data).find("value").text();
				if($(data).find("fieldDTO")) that.field=new $.Field($(data).find("fieldDTO"));
			}
		},
		
		this.getAttendDataId= function(){	
			return that.attendDataId;
		},
		
		this.getValue= function(){	
			return that.value;
		},
		this.getField = function(){		
			return that.field;
		},
		
		this.setAttendDataId = function(attendDataId){
			that.attendDataId = attendDataId;
		},
		
		this.setValue = function(value){
			that.value = value;
		},
		
		this.setField = function(field){		
			that.field = field;
		},
		
		this.update = function(newData){
			that.attendData(newData);
		};
		
		this.xmlData = function(){
			var xml='<attendDataDTO>';
			xml+='<attendDataId>'+that.attendDataId+'</attendDataId>';
			xml+='<value>'+htmlEncode(that.value)+'</value>';
			xml+='</attendDataDTO>';
			return xml;
		};
		
		this.attendData(data);
	},
	
	Field: function(data){
		var that = this;
		that.fieldId = '';
		that.fieldName = '';
		that.inputType = '';
		that.fieldValue = '';
		
		this.field = function(data){
			if($(data).find("fieldId")) that.fieldId=$(data).find("fieldId").text();
			if($(data).find("fieldName")) that.fieldName=$(data).find("fieldName").text();
			if($(data).find("fieldValue")) that.fieldName=$(data).find("fieldValue").text();
			if($(data).find("inputTypeDTO")) that.inputType=new $.InputType($(data).find("inputTypeDTO"));
		},
		
		this.getFieldId = function(){	
			return that.fieldId;
		},
		
		this.getFieldName = function(){	
			return that.fieldName;
		},
		
		this.getInputType = function(){		
			return that.inputType;
		},
		
		this.getFieldValue = function(){		
			return that.fieldValue;
		},
		
		this.setFieldId = function(fieldId){
			that.fieldId=fieldId;
		},
		
		this.setFieldName = function(fieldName){
			that.fieldName=fieldName;
		},
		
		this.setInputType = function(inputType){
			that.inputType=inputType;
		},
		
		this.setFieldValue = function(fieldValue){
			that.fieldValue=fieldValue;
		},
		
		this.update = function(newData){
			that.websiteType(newData);
		};
		
		this.xmlData = function(){
			var xml='<fieldDTO>';
			xml+='<fieldId>'+that.fieldId+'</fieldId>';
			xml+='<fieldName>'+htmlEncode(that.fieldName)+'</fieldName>';
			xml+='<fieldValue>'+htmlEncode(that.fieldValue)+'</fieldValue>';
			xml+=that.inputType.xmlData();
			xml+='</fieldDTO>';
			return xml;
		};
		
		this.field(data);
	},
	
	SurveyModel: function(){
		var that = this;

		var listeners = new Array();
		
		this.initModel = function(){
			that.getSurvey();
		};
		
		this.getSurveySuccess = function(data){
			that.notifyLoadSurvey(new $.Survey($(data)));
		},
		
		this.getSurveyError = function(xhr, status, error){
			$('#preloaderCustom').hide();
		},
		
		this.getSurvey = function(){
			genericAjaxCall('GET', staticVars.urlBackEnd+'survey/get/stand?idLang='+getParam('idLang'), 'application/xml', 'xml', 
					'', that.getSurveySuccess, that.getSurveyError);
		},
		
		this.updateSurveySuccess = function (data){	
			$('#loadingEds').hide();
		};
		
		this.updateSurveyError = function(xhr, status, error){
			isSessionExpired(xhr.responseText);
			$('#preloaderCustom').hide();
		};
		
		this.updateSurvey = function(form){
			genericAjaxCall('POST', staticVars.urlBackEnd+'survey/update?idLang='+getIdLangParam(), 'application/xml', 'xml',form.xmlData(), 
					that.updateSurveySuccess, that.updateSurveyError);
		};
				
		this.deleteSurveySuccess = function (surveyId){	
			that.notifySurveyDelete(surveyId);
		};
		
		this.deleteSurveyError = function(xhr, status, error){
			isSessionExpired(xhr.responseText);
			$('#preloaderCustom').hide();
		};
		
		this.deleteSurvey = function(surveyId){
			genericAjaxCall('POST', staticVars.urlBackEnd+'survey/del?idLang='+getIdLangParam()+'&formId='+surveyId, 'application/xml', 'xml','', 
					function(){ that.deleteSurveySuccess(surveyId); }, that.deleteSurveyError);
		};
		
		this.notifyLoadSurvey = function(surveys){
			$.each(listeners, function(i){			
				listeners[i].notifyLoadSurvey(surveys);
			});
		};
		
		this.notifySurveyDelete = function(surveyId){
			$.each(listeners, function(i){			
				listeners[i].notifySurveyDelete(surveyId);
			});
		};
		
		this.addListener = function(list){
			listeners.push(list);
		};
		
	},
		
	ModelListener: function(list) {
		if(!list) list = {};
		return $.extend(list);
	},
});
