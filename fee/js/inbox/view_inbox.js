jQuery.extend({
	
	MessageFoldersManage : function(messageFolders, messageFoldersContainer, view){
		//var that=this;
		var dom = messageFoldersContainer;
		
		this.refresh = function(){
			for(var index in messageFolders.getMessageFolders()){
				messageFolderManage = new $.MessageFolderManage(messageFolders.getMessageFolders()[index], view);
				dom.append(messageFolderManage.getDOM());
			}
			$('[data-toggle="tooltip"]').tooltip();
			$("#customfolders").append('<a href="#" class="inbox-menu-item add-folder"><i class="text-info fa fa-plus-circle mr-sm"></i><span class="addFolders" style="float:inherit;"></span></a>');

			
			$('.add-folder').unbind('click').bind('click',function(){
				$('#popupAddFolder').modal();
				
				$('.saveAddFolderButton').unbind('click').bind('click',function(e) {
					if($('#folderAddForm').valid()){
						var folder = new $.MessageFolder();
						folder.setFolderName($('#addFolderInput').val());
						view.addNewFolder(folder);
					}
				});
			});
			
			$('.changeFolderButton').unbind('click').bind('click',function(){
				var idFolder = $('.folderSelect').val();
				view.changeFolder(idFolder, $idConversationList);
			});
			
			$('.deleteConversationButton').unbind('click').bind('click',function(){
				view.deleteConversation($idConversationList);
			});
			
			$('.archiveConversationButton').unbind('click').bind('click',function(){
				view.archiveConversation($idConversationList);
			});

			$('.deleteFolderButton').unbind('click').bind('click',function(){
				view.deleteFolder($idFolderToRemove);
			});

			$('.back-to-inbox').unbind('click').bind('click',function(){
				$('.panel-inbox-read').hide();
				$('.panel-inbox-reception').show();
				$('.panel-inbox-write').hide();
			});

			$('.back-to-message-list').unbind('click').bind('click',function(){
				$('.panel-inbox-read').show();
				$('.panel-inbox-reception').hide();
				$('.panel-inbox-write').hide();
			});
			traduct();
		};
		
		

		this.getDOM = function(){ 
			return dom;
		},
		
		this.refresh();
	},
	
	MessageFolderManage : function(folder, view){
		//var that=this;
		var dom = $('<a id="folder_'+folder.getIdMessageFolder()+'" class="inbox-menu-item">'+folder.getFolderName()+'<span><i data-toggle="tooltip" title="Supprimer" class="fa fa-fw fa-trash-o" id="removeFolder"></i></span></a>');

		this.refresh = function(){
			$('.folderSelect').append('<option value="'+folder.getIdMessageFolder()+'">'+folder.getFolderName()+'</option>');
		},
		
		dom.unbind("click").bind('click',function(){
			$('#loadingEds').show();
			$('#folders').find('a').removeClass("active");
			$('#folder_'+folder.getIdMessageFolder()).addClass("active");
			$('.inbox-mail-heading').find('.archiveConversation').show();
			$('.inboxFolders').find('li').removeClass('active');
			$('.inboxFolders').find('#folder_'+folder.getIdMessageFolder()).addClass('active');
			
			$('.email-content-tab').find('.tab-pane').removeClass('active');
			
			$('#preview-email-wrapper').hide();

			$('.panel-inbox-read').hide();
			$('.panel-inbox-reception').show();
			$('.panel-inbox-write').hide();
			
			dom.find('#removeFolder').unbind('click').bind('click',function(e) {
				$idFolderToRemove = $(this).closest('a').attr('id').split('_')[1];			
				$('#popupDeleteFolder').modal();
			});
			
			view.loadConversationByFolderId(folder.getIdMessageFolder());
		});
		
		this.getDOM = function(){ //retourne une ligne HTML <a .....
			return dom;
		},
		
		this.refresh();
	},
	
	ConversationsManage : function(conversationList, conversationsContainer, view){
		$unreadNumber = 0;
		var folderId = '';
		if(conversationList.conversations[0] != null) {
			folderId =  conversationList.conversations[0].messageFolder.idMessageFolder;
		}
		
		/*var dom = $('<div class="tab-pane active" style="padding: 0;"> '
						+'<div class="row-fluid">' 
							+'<div class="span12">'
								+'<div id="email-list" style="border: 1px solid #ebebeb; border-top: 0px;"> '
									+'<table class="table table-fixed-layout table-hover" id="messageFolder_'+folderId+'"> <thead> <tr><th class="small-cell" style="padding-left: 13px; padding-top: 16px; "><input class="selectAllCheckbox" id="deleteAllCheckbox" type="checkbox" /> <label for="deleteAllCheckbox"></label></th> <th class="medium-cell"></th> <th style=" padding-left: 0px; "><div class="actionButtons" style="display:none;"><a class="moveConversation" style="     margin-right: 10px; "><button type="button"class="btn btn-small btn-white "><i class=""></i>Déplacer</button></a><a class="deleteConversation" style="     margin-right: 10px; "><button type="button"class="btn btn-small btn-white "><i class=""></i>Supprimer</button></a><a class="archiveConversation" style="margin-right: 10px; "><button type="button"class="btn btn-small btn-white "><i class=""></i>Archiver</button></a></div> </th> <th class="medium-cell"></th> </tr> </thead>'
									+'</table> '
								+'</div> '
							+'</div> '
						+'</div> '
					+'</div>');*/
		var dom = $('<table class="table table-striped table-hover table-inbox mb0 table-vam" id="messageFolder_'+folderId+'">'
						+'<tbody>'
						+'</tbody>'
					+'</table>');

		var conversation = null;
		
		this.refresh = function(){
			$conversationsContainer.empty();
			$conversationsContainer.append(dom);
			if(conversationList.conversations.length != 0){
				for(var index in conversationList.conversations){
					conversation = new $.ConversationManage(conversationList.conversations[index], view);
					dom.find('tbody').append(conversation.getDOM());
					$('.icheck input').iCheck({
						checkboxClass: 'icheckbox_minimal-blue',
						radioClass: 'iradio_minimal-blue'
					});
				}
			}else{
				dom.find('tbody').append('<tr class="unread conversation-status">'
											+'<td class="inbox-msg-from hidden-xs hidden-sm" width="100%">'
											+'<div>'
												+"Aucune conversation trouvée ."
											+'</div></td>'
										+'</tr>');
			}
			
			$('.moveConversation').unbind().bind("click", function(){
				//console.log(conversationList);
				var idFolder = conversationList.conversations[0].messageFolder.idMessageFolder;
		    	var checkedConversation = $('#messageFolder_'+idFolder).find('.icheck input');
		    	$idConversationList = new Array();
				for (var i = 0; i < checkedConversation.length; i++) {
		    		if($(checkedConversation[i]).is(':checked')){
		    			var idSelected = $(checkedConversation[i]).attr('id').split('_')[1];
		    			$idConversationList.push(idSelected);
		    		}
				}
				if($idConversationList.length != 0){
        			$("#popupMoveConversation").modal();
				}else{
					infoMessage('#archiveError');
				}
			});
			
			$('.deleteConversation').unbind().bind("click",function(){
				var idFolder = conversationList.conversations[0].messageFolder.idMessageFolder;
		    	var checkedConversation = $('#messageFolder_'+idFolder).find('.icheck input');
		    	$idConversationList = new Array();
				for (var i = 0; i < checkedConversation.length; i++) {
		    		if($(checkedConversation[i]).is(':checked')){
		    			var idSelected = $(checkedConversation[i]).attr('id').split('_')[1];
		    			$idConversationList.push(idSelected);
		    		}
				}
				if($idConversationList.length != 0){
        			$("#popupDeleteConversation").modal();
				}else{
					infoMessage('#archiveError');
				}
			});
			
			$('.archiveConversation').unbind().bind("click",function(){
				var idFolder = conversationList.conversations[0].messageFolder.idMessageFolder;
		    	var checkedConversation = $('#messageFolder_'+idFolder).find('.icheck input');
		    	$idConversationList = new Array();
				for (var i = 0; i < checkedConversation.length; i++) {
		    		if($(checkedConversation[i]).is(':checked')){
		    			var idSelected = $(checkedConversation[i]).attr('id').split('_')[1];
		    			$idConversationList.push(idSelected);
		    		}
				}
				if($idConversationList.length != 0){
        			$("#popupArchiveConversation").modal();
				}else{
					infoMessage('#archiveError');
				}
			});

			$('.selectAllCheckbox').on("ifChecked", function(){
				//console.log('ifChecked');
				var idFolder = conversationList.conversations[0].messageFolder.idMessageFolder;
				var checkedConversation = $('#messageFolder_'+idFolder).find('.icheck input');
				$idConversationList = new Array();
				for (var i = 0; i < checkedConversation.length; i++) {
					$(checkedConversation[i]).prop( "checked", true );
				}
				
				$('.table .icheck input').iCheck({
					checkboxClass: 'icheckbox_minimal-blue',
					radioClass: 'iradio_minimal-blue'
				});
			});

			$('.selectAllCheckbox').on("ifUnchecked", function(){
				//console.log('ifUnchecked');
				var idFolder = conversationList.conversations[0].messageFolder.idMessageFolder;
				var checkedConversation = $('#messageFolder_'+idFolder).find('.icheck input');
				$idConversationList = new Array();
				for (var i = 0; i < checkedConversation.length; i++) {
					$(checkedConversation[i]).prop( "checked", false );
				}

				$('.table .icheck input').iCheck({
					checkboxClass: 'icheckbox_minimal-blue',
					radioClass: 'iradio_minimal-blue'
				});
			});
		},
		
		this.getDOM = function(){ 
			return dom;
		},
		
		this.refresh();
	},

	ConversationManage : function(conversation, view){
		//var that=this;
		var lastUpdate = conversation.lastUpdate;
		var unread = false;
		/*console.log(conversation);
		var dom = $('<tr class="unread" id="mail_row_'+conversation.idConversation+'">'
						+'<td valign="middle" class="small-cell">'
						+'<input class="checkboxConversation" id="checkbox_'+conversation.idConversation+'" type="checkbox" /> '
						+'<label for="checkbox_'+conversation.idConversation+'"></label></td>' 
						+'<td valign="middle" class="clickable">'+htmlEncode(conversation.getCandidate().getFullName())+'</td>'
						+'<td valign="middle" class="clickable tablefull"><div class="label label-important"></div> <span class="muted" style="margin-top: 5px;">'+htmlEncode(conversation.subject)+'</span></td>' 
						+'<td class="clickable"><span class="timestamp">'+htmlEncode(lastUpdate)+'</span></td> '
					+'</tr>');*/
		var dom = $('<tr class="unread conversation-status"  id="mail_row_'+conversation.idConversation+'">'
						+'<td class="inbox-msg-check" width="5%">'
						+'<div class="checkbox-inline icheck">'
							+'<input type="checkbox" value="" id="checkbox_'+conversation.idConversation+'">'
						+'</div></td>'
						+'<td class="inbox-msg-from hidden-xs hidden-sm" width="25%">'
						+'<div>'
							+htmlEncode(conversation.getCandidate().getFullName())
						+'</div></td>'
						+'<td class="inbox-msg-snip"><span class="label label-primary label-important" width="45%"></span>'+htmlEncode(conversation.subject)+'</td>'
						+'<td class="inbox-msg-time" width="25%">'+htmlEncode(lastUpdate)+'</td>'
					+'</tr>');
		
		this.refresh = function(){			
			var messageList = conversation.messages.messages;
			for(var index in messageList){
				var receiver = messageList[index].receiver.userProfileId;
				if(receiver == ''){
					if(messageList[index].unread == true){
						dom.find('.label-important').html(NEWMESSAGELABEL);
						unread = true;
					}else{
					}
				}
				
			};
			if(unread == true){
				$unreadNumber++;
			}

			dom.unbind('click').bind('click',function(){
				$indexMessage = 0;
				$('.conversation-title-details').html(conversation.subject);
				$('.Message-list-container').empty();
				for(var index in messageList){
					var messagesDetail = new $.MessagesManage(messageList[index], view,conversation);
					$('.Message-list-container').append(messagesDetail.getDOM());
					$('.panel-inbox-read').show();
					$('.panel-inbox-reception').hide();
					$('.panel-inbox-write').hide();
				}
				if(unread) 
					view.markAsRead(conversation.idConversation);
				
				$('.response-button').unbind("click").bind("click",function(){
				    //console.log(conversation);

				    $('.panel-inbox-read').hide();
					$('.panel-inbox-reception').hide();
					$('.panel-inbox-write').show();

					$('.subject-send').val(conversation.getSubject());
					$('.to-email-send').val(conversation.getCandidate().getEmail());
					
					$('.send-response').unbind("click").bind("click",function(){
						//console.log(conversation);
				    	$('.panel-inbox-write .error').remove();
						if($(".email-body").val().trim() == ""){
							$(".email-body").after('<label class="error">' + REQUIREDLABEL + '</label>');
						}else{
							var message = new $.Message();
							var userProfile = new $.UserProfile();
							var messageList = conversation.messages.messages;
							var messageSender = messageList[0].sender;
							message.setContent($(".email-body").val());
							message.setIdmessage('');
							message.setUnread(true);
							userProfile.setUserProfileId(messageSender.userProfileId);
							userProfile.setFirstName(messageSender.firstName);
							userProfile.setSecondName(messageSender.secondName);
							message.setReceiver(userProfile);
							view.sendReply(message,conversation.idConversation);
						}
					});
				});
				
				$('.moveConversationIn').unbind().bind("click", function(){
			    	$idConversationList = new Array();					
	    			var idSelected = conversation.getIdConversation();
	    			$idConversationList.push(idSelected);
					if($idConversationList.length != 0){
	        			$("#popupMoveConversation").modal();
					}else{
						infoMessage('#archiveError');
					}
				});
				
				$('.deleteConversationIn').unbind().bind("click",function(){
			    	$idConversationList = new Array();					
	    			var idSelected = conversation.getIdConversation();
	    			$idConversationList.push(idSelected);
					if($idConversationList.length != 0){
	        			$("#popupDeleteConversation").modal();
					}else{
						infoMessage('#archiveError');
					}
				});
				
				$('.archiveConversationIn').unbind().bind("click",function(){
					$idConversationList = new Array();					
	    			var idSelected = conversation.getIdConversation();
	    			$idConversationList.push(idSelected);
					if($idConversationList.length != 0){
	        			$("#popupArchiveConversation").modal();
					}else{
						infoMessage('#archiveError');
					}
				});
			});
			
			dom.find('.checkboxConversation').unbind('click').bind('click',function(){
				if($('.inboxFolders').find('.active').attr('id') != "archiveFoder"){
					var idFolder = conversation.messageFolder.idMessageFolder;
					if ($(this).is(':checked')) {
						$('.actionButtons').show();
					} else {
						var checkedConversation = $('#messageFolder_'+idFolder).find('.checkboxConversation');
						var showButtons = false;
						for (var i = 0; i < checkedConversation.length; i++) {
							if($(checkedConversation[i]).is(':checked')){
								showButtons = true;
							}
						}
						
						if(showButtons == false){
							$('.actionButtons').hide();
						}else{
							$('.actionButtons').show();
						}
					}
				}
				
			});
		},
		
		this.getDOM = function(){ //retourne une ligne HTML <a .....
			return dom;
		},
		
		this.refresh();
	},

	MessagesNewAddedManage : function(message){
		//var that=this;
		$indexMessage ++;
		var firstName = message.sender.firstName;
		var secondName = message.sender.secondName;
		var receiver = message.receiver.userProfileId;
		var idMessage = message.getIdmessage();
		var picture = $(getRecruiterInfo).find('avatar').text();
		$('.message-single').removeClass('in');
		var collapse = "collapse in";

		var dom = $('<div class="collapsible-menu" style="border-top: 0;">'
						+'<span> '
							+'<div data-toggle="collapse" data-target="#message_'+idMessage+'" class="category-heading header-message">'
								+'<div class="inbox-read-details clearfix">'
									+'<div class="pull-left">'
										+'<img class="inbox-read-sender-avatar img-circle" src="'+picture+'" alt="Dangerfield">'
										+'<span class="inbox-read-sender-name pull-left">'+htmlEncode(firstName+' '+secondName)+'</span>&nbsp;&nbsp;'										
									+'</div>'
									+'<div class="pull-right">'
										+'<span class="inbox-read-sent-info">'+htmlEncode(message.messageDate)+'</span>'
									+'</div>'
								+'</div>'
							+'</div> '
						+'</span>'
						+'<div class="'+collapse+' message-single" id="message_'+idMessage+'">'
							+'<hr class=" mt-md">'

							+'<div class="msg-body">'
							+'<p>'+htmlEncode(message.content).replace(/[\n]/gi, "<br/>" )+'</p>'
							+'</div>'
						+'</div>'
					+'</div>'
					+'<hr class=" mb-md">');
		
		this.refresh = function(){
			
		},
		
		this.getDOM = function(){ //retourne une ligne HTML <a .....
			return dom;
		},
		
		this.refresh();
	},
	
	MessagesManage : function(message, view, conversation){
		//var that=this;
		$indexMessage ++;
		var firstName = message.sender.firstName;
		var secondName = message.sender.secondName;
		var receiver = message.receiver.userProfileId;
		var picture = '';
		var idMessage = message.getIdmessage();
		if(receiver == ''){
			picture = conversation.getCandidate().getUserProfile().getAvatar();
		}else{
			picture = $(getRecruiterInfo).find('avatar').text();
		}

		var collapse = "collapse";
		if($indexMessage == conversation.getMessages().getMessages().length){
			collapse = "collapse in";
		}
		//console.log(getRecruiterInfo);
		var dom = $('<div class="collapsible-menu" style="border-top: 0;">'
						+'<span> '
							+'<div data-toggle="collapse" data-target="#message_'+idMessage+'" class="category-heading header-message">'
								+'<div class="inbox-read-details clearfix">'
									+'<div class="pull-left">'
										+'<img class="inbox-read-sender-avatar img-circle" src="'+picture+'" alt="Dangerfield">'
										+'<span class="inbox-read-sender-name pull-left">'+htmlEncode(firstName+' '+secondName)+'</span>&nbsp;&nbsp;'										
									+'</div>'
									+'<div class="pull-right">'
										+'<span class="inbox-read-sent-info">'+htmlEncode(message.messageDate)+'</span>'
									+'</div>'
								+'</div>'
							+'</div> '
						+'</span>'
						+'<div class="'+collapse+' message-single" id="message_'+idMessage+'">'
							+'<hr class=" mt-md">'

							+'<div class="msg-body">'
							+'<p>'+htmlEncode(message.content).replace(/[\n]/gi, "<br/>" )+'</p>'
							+'</div>'
						+'</div>'
					+'</div>'
					+'<hr class=" mb-md">');
		
		this.refresh = function(){
			
		},
		
		this.getDOM = function(){ //retourne une ligne HTML <a .....
			return dom;
		},
		
		this.refresh();
	},
	
	InboxView: function(){
		var that = this;
		var listeners = new Array();
		$idConversationList = null;
		$idFolderToRemove   = null;
		$indexMessage = null;
		$unreadNumber = null;
		
		this.initView = function(){
			$messageFoldersContainer = $('#folders');
			$conversationsContainer = $('.email-content-tab');
			getRecruiterInfosChat();
			$('#receptionFoder').unbind("click").bind('click',function(){
				$('#loadingEds').show();
				$('#folders').find('a').removeClass("active");
				$('#receptionFoder').find('a').addClass("active");
				$('.inbox-mail-heading').find('.archiveConversation').show();
				$('.inboxFolders').find('li').removeClass('active');
				$('#receptionFoder').addClass('active');
				$('.email-content-tab').find('.tab-pane').removeClass('active');
				$('#preview-email-wrapper').hide();
				that.loadConversationByFolderId(1);
				$('.panel-inbox-read').hide();
				$('.panel-inbox-reception').show();
				$('.panel-inbox-write').hide();
			});
			
			$('#archiveFoder').unbind("click").bind('click',function(){
				$('#loadingEds').show();
				$('#folders').find('a').removeClass("active");
				$('#archiveFoder').find('a').addClass("active");
				$('.inbox-mail-heading').find('.archiveConversation').hide();
				$('.inboxFolders').find('li').removeClass('active');
				$('#archiveFoder').addClass('active');
				$('.email-content-tab').find('.tab-pane').removeClass('active');
				$('#preview-email-wrapper').hide();
				that.loadArchivedConversation();
				$('.panel-inbox-read').hide();
				$('.panel-inbox-reception').show();
				$('.panel-inbox-write').hide();
			});
			$('[data-toggle="tooltip"]').tooltip();
			$('.composer').summernote({
				height: 350
			});
		};
		
		this.initMessageFolder = function(messageFolders){
			new $.MessageFoldersManage(messageFolders, $messageFoldersContainer, that);
		},
		
		this.initConversation = function(conversations){
			// var reverseConversations = conversations.reverse();
			// console.log(reverseConversations);
			new $.ConversationsManage(conversations, $conversationsContainer, that);

			if($unreadNumber != 0){
				$('.unread-number').show();
				$('.unread-number').text($unreadNumber);
			}
			if($('.inboxFolders').find('.active').attr('id') == "archiveFoder" || conversations.conversations.length == 0){
				$('.selectAllCheckbox').hide();
			};
            $('#loadingEds').hide();
		},
		
		this.notifyAsRead = function(conversationId){
			$('#mail_row_'+conversationId).find('.label-important').html('');
			var number = parseInt($('.unread-number').text())-1;
			if(number != 0){
				$('.unread-number').text(number);
			}else{
				$('.unread-number').hide();
			}
			
		},
		
		this.notifySendReplySuccess = function(message){
			var messagesDetail = new $.MessagesNewAddedManage(message);
			$('.Message-list-container').append(messagesDetail.getDOM());
			$('.panel-inbox-read').show();
			$('.panel-inbox-reception').hide();
			$('.panel-inbox-write').hide();
		},
		
		this.notifyAddNewFolder = function(folder){
			$('#popupAddFolder .btn-danger').click();
            infoMessage('#addFolderSuccess');
            setTimeout(function(){
				$('.inbox').trigger('click');
            }, 500);
		},
		
		this.notifyDeleteFolder = function(){
            //infoMessage('#deleteFolderSuccess');
            setTimeout(function(){
				$('.inbox').trigger('click');
            }, 500);
		},
		
		this.notifyChangeFolder = function(){
            $('#popupMoveConversation .btn-danger').click();
            infoMessage('#moveSuccess');
            setTimeout(function(){
				$('#receptionFoder').trigger('click');
            }, 500);
		},
		
		this.notifyDeleteConversation = function(){
            $('#popupDeleteConversation .btn-danger').click();
            infoMessage('#deleteSuccess');
            setTimeout(function(){
				$('#receptionFoder').trigger('click');
            }, 500);
		},
		
		this.notifyArchivedConversation = function(){
            $('#popupArchiveConversation .btn-danger').click();
            infoMessage('#archiveSuccess');
            setTimeout(function(){
            	$('#receptionFoder').trigger('click');
            }, 500);
		},
		
		this.loadConversationByFolderId = function(folderId){
			$.each(listeners, function(i){
				listeners[i].loadConversationByFolderId(folderId);
			});
		},
		
		this.loadArchivedConversation = function(){
			$.each(listeners, function(i){
				listeners[i].loadArchivedConversation();
			});
		},
		
		this.markAsRead = function(conversationId){
			$.each(listeners, function(i){
				listeners[i].markAsRead(conversationId);
			});
		},

		this.sendReply = function(message,conversationId){
			$.each(listeners, function(i){
				listeners[i].sendReply(message,conversationId);
			});
		},
		
		this.deleteFolder = function(IdFolder){
			$.each(listeners, function(i){
				listeners[i].deleteFolder(IdFolder);
			});
		},
		
		this.addNewFolder = function(folder){
			$.each(listeners, function(i){
				listeners[i].addNewFolder(folder);
			});
		},

		this.changeFolder = function(idFolder, idConversationList){
			$.each(listeners, function(i){
				listeners[i].changeFolder(idFolder,idConversationList);
			});
		},

		this.deleteConversation = function(idConversationList){
			$.each(listeners, function(i){
				listeners[i].deleteConversation(idConversationList);
			});
		},

		this.archiveConversation = function(idConversationList){
			//console.log(idConversationList);
			$.each(listeners, function(i){
				listeners[i].archiveConversation(idConversationList);
			});
		},

		this.addListener = function(list){
			listeners.push(list);
		};
		
	},
	
	
	ViewListener: function(list) {
		if(!list) list = {};
		return $.extend(list);
	}
	
});

