jQuery.extend({
    ProfileSearchedView: function () {
        var that = this;
        var listeners = new Array();
        var profileSearchedHandler = null;
        var saveProfileSearchedHandler = null;
        var currentStand = null;
        var contentHandler = null;

        this.initView = function () {
            traduct();
            contentHandler = $("#mainBackEndContainer");
            profileSearchedHandler = $("#profileSearched");
            saveProfileSearchedHandler = $(".saveProfileSearchedButton ");
            //Profile Searched save
            saveProfileSearchedHandler.unbind("click").bind("click", function (e) {
                that.saveProfileSearched(currentStand);
            });
        };
        this.viewSaveProfileSearchedSuccess = function () {
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        //Profile Searched load Xml
        this.profileSearchedLoad = function (stand) {
            currentStand = stand;
            profileSearchedHandler.val(unescape(stand.getProfileSearched()));
            $('#loadingEds').hide();
        };
        //Profile Searched save
        this.saveProfileSearched = function (stand) {
            stand.setProfileSearched(profileSearchedHandler.val());
            that.notifysaveProfileSearched(stand);
        };

        this.addListener = function (list) {
            listeners.push(list);
        };
        this.notifysaveProfileSearched = function (stand) {
            $.each(listeners, function (i) {
                listeners[i].saveProfileSearched(stand);
            });
        };
    },
    ProfileSearchedViewListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }

});
