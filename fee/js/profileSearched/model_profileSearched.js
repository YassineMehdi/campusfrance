jQuery.extend({
    ProfileSearchedModel: function () {
        var that = this;
        var listeners = new Array();
        this.initModel = function () {
            that.getProfileSearched();
        };
        // profile Searched load
        this.getProfileSearchedSuccess = function (xml) {
            //console.log(xml);
            var stand = new $.Stand(xml);
            that.notifyProfileSearched(stand);
        };

        this.getProfileSearchedError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };
        this.getProfileSearched = function () {
            genericAjaxCall('GET', staticVars.urlBackEnd + 'stand/getProfileSearched?idLang=' + getIdLangParam(),
                    'application/xml', 'xml', '', that.getProfileSearchedSuccess, that.getProfileSearchedError);
        };
        // profile Searched save
        this.saveProfileSearchedSuccess = function (xml) {
            that.notifySaveProfileSearchedAlertSuccess();
        };

        this.saveProfileSearchedError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };
        this.saveProfileSearched = function (stand) {
            genericAjaxCall('POST', staticVars.urlBackEnd + 'stand/profilesSearched?idLang=' + getIdLangParam(),
                    'application/xml', 'xml', stand.xmlData(), that.saveProfileSearchedSuccess, that.saveProfileSearchedError);
        };

        this.addListener = function (list) {
            listeners.push(list);
        };

        this.notifyProfileSearched = function (stand) {
            $.each(listeners, function (i) {
                listeners[i].profileSearchedLoad(stand);
            });
        };
        this.notifySaveProfileSearchedAlertSuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].saveProfileSearchedAlertSuccess();
            });
        };

    }
    ,
    ProfileSearchedModelListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }
});
