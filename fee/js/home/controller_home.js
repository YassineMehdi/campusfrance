jQuery.extend({
    HomeController: function (model, view) {
        var that = this;
        /**
         * listen to the view
         */
        var vlist = $.HomeViewListener({
        });
        view.addListener(vlist);

        /**
         * listen to the model
         */
        var mlist = $.HomeModelListener({
        });

        model.addListener(mlist);

        this.initController = function () {
            view.initView();
            model.initModel();
        };
    }
});
