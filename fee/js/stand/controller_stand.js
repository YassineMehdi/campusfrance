jQuery.extend({
    StandController: function (model, view) {
        var that = this;
        /**
         * listen to the view
         */
        var vlist = $.StandViewListener({
        });
        view.addListener(vlist);

        /**
         * listen to the model
         */
        var mlist = $.StandModelListener({
        });

        model.addListener(mlist);

        this.initController = function () {
            view.initView();
            model.initModel();
        };
    }
});
