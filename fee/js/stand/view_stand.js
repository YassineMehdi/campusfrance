jQuery.extend({
    StandView: function () {
        var that = this;
        var listeners = new Array();
        var tabsHandler = null;
        var profileHandler = null;
        var contactHandler = null;
        var profileSearchedHandler = null;
        var postersHandler = null;
        var videosHandler = null;
        var videosRhHandler = null;
        var photosHandler = null;
        var documentsHandler = null;
        var testimonyHandler = null;
        var surveyHandler = null;
        var seoHandler = null;


        this.initView = function () {
            traduct();
            tabsHandler = $("#myTab li");
            profileHandler = $(".profile");
            contactHandler = $(".contact");
            profileSearchedHandler = $(".profileSearched");
            postersHandler = $(".affiches");
            videosHandler = $(".videos");
            videosRhHandler = $(".videosRh");
            photosHandler = $(".photos");
            documentsHandler = $(".documents");
            testimonyHandler = $(".testimonies");
            surveyHandler = $(".surveys");
            seoHandler = $(".seoLink");

            factorycheckbox();
            profileSearchedHandler.unbind("click").bind("click", function (e) {
                that.factoryProfileSearchedHandler();
            });
            profileHandler.unbind("click").bind("click", function (e) {
                that.factoryProfilEntreprise();
            });
            contactHandler.unbind("click").bind("click", function (e) {
                that.factoryProfilContact();
            });
            postersHandler.unbind("click").bind("click", function (e) {
                that.factoryPostersHandler();
            });
            videosHandler.unbind("click").bind("click", function (e) {
                that.factoryVideosHandler();
            });
            videosRhHandler.unbind("click").bind("click", function (e) {
                that.factoryVideosRhHandler();
            });
            photosHandler.unbind("click").bind("click", function (e) {
                that.factoryPhotosHandler();
            });
            documentsHandler.unbind("click").bind("click", function (e) {
                that.factoryDocumentsHandler();
            });
            testimonyHandler.unbind("click").bind("click", function (e) {
                that.factoryTestimonyHandler();
            });
            surveyHandler.unbind("click").bind("click", function (e) {
                that.factorySurveyHandler();
            });
            seoHandler.unbind("click").bind("click", function (e) {
                that.factorySeoHandler();
            });
            profileHandler.trigger("click");
        };

        this.factoryProfilEntreprise = function () {
            that.showTabNotChat('profilEntreprise.html', $(this));
        };
        this.factoryProfilContact = function () {
            that.showTabNotChat('contact.html', $(this));
        };
        this.factoryProfileSearchedHandler = function () {
            that.showTabNotChat('profilesearched.html', $(this));
        };
        this.factoryPostersHandler = function () {
            that.showTabNotChat('posters.html', $(this));
        };
        this.factoryVideosHandler = function () {
            that.showTabNotChat('videos.html', $(this));
        };
        this.factoryVideosRhHandler = function () {
            that.showTabNotChat('videosRh.html', $(this));
        };
        this.factoryPhotosHandler = function () {
            that.showTabNotChat('photos.html', $(this));
        };
        this.factoryDocumentsHandler = function () {
            that.showTabNotChat('documents.html', $(this));
        };
        this.factoryTestimonyHandler = function () {
            that.showTabNotChat('testimony.html', $(this));
        };
        this.factorySurveyHandler = function () {
            that.showTabNotChat('survey.html', $(this));
        };
        this.factorySeoHandler = function () {
            that.showTabNotChat('seo.html', $(this));
        };
        this.showTabNotChat = function (file, tabElement) {
            $('#loadingEds').show();
            $(tabsHandler).click(function () {
                $(tabsHandler).removeClass('active');
                $(this).addClass('active');
            });
            $('#mainBackEndContainer #standContent').load(file);
        };

        this.addListener = function (list) {
            listeners.push(list);
        };
    },
    StandViewListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }

});
