jQuery.extend({
    StandModel: function () {
        var that = this;
        var listeners = new Array();
        this.initModel = function () {

        };

        this.addListener = function (list) {
            listeners.push(list);
        };

    }
    ,
    StandModelListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }
});
