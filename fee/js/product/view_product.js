
jQuery.extend({
	ResourceRowManage : function(resource, view){
		//var that=this;
		var dom = $("<li><table><tbody>"
				+"<tr>"
					+"<td class='span7'>"
						+"<a class='resourceName' href='#' target='_blank'/>"
					+"</td>"
					+"<td class='span2'>"
						+"<img class='resource-action removeResource' src='images/delete.png' title='"+REMOVELABEL+"'>"
					+"</td>"
				+"</tr>"
				+"</tbody></table></li>");
		
		this.getResource = function(){
			return resource;
		};
		
		this.refresh = function(){
			dom.find(".resourceName").attr('href', resource.getResourceUrl());
			dom.find(".resourceName").text(resource.getResourceName());
			dom.find(".removeResource").text(REMOVELABEL)
							.attr("id", "removeResource_"+resource.getResourceId());
			dom.find('.removeResource').unbind('click').bind('click', function() {
				view.deleteResource(resource);
			});
		};
		
		this.getDOM = function(){ 
			return dom;
		};
		
		this.refresh();
	},
	ProductView: function(){
		var that = this;
		var listeners = new Array();
		var productsList = [];
        var validatorProduct = null;
		that.productId = 0;
		that.resourceId = 0;
		that.isTraduct = false;
		
		this.initView = function(){
		    traduct();
		    that.validateProductForm();
			$("#addProductLink").unbind("click").bind("click", function() {
				that.productId = 0;
				that.isTraduct = false;
				enabledButton('#saveProductBtn');
				$("#addProductTitlePopup").text(ADDLABEL);
				$("#productTitle").val("");
				$("#productDescription").val("");
				$('#loadProductImage').show();
				$('#productImageLoaded,#progressProductImage,#loadProductImageError,#productImageEdited, .traductBlock').hide();
				$("#popupAddProduct").modal();
				validatorProduct.resetForm();
			});
			$("#productsContent .draggable").draggable({
				cancel : ".eds-dialog-inside"
			});
			$('#productsContent .closePopup, #productsContent .eds-dialog-close').click(function() {
				hidePopups();
			});
			$("#saveProductBtn").unbind("click").bind("click", function() {
				if ($('#productForm').valid()
						&& !isDisabled('#saveProductBtn')) {
					that.saveProduct(that.productId, $("#productTitle").val(), 
							$("#productDescription").val(), $('#productImageEdited .productImage').attr("src"));
				} else {
					enabledButton('#saveProductBtn');
				}
			});
			$("#confirmDeleteProductBtn").unbind("click").bind("click", function() {
				if (!isDisabled('#deleteProductBtn')) {
					disabledButton('#deleteProductBtn');
					that.notifyDeleteProduct(that.productId);
				} else {
					enabledButton('#deleteProductBtn');
				}
			});
			$("#confirmDeleteResourceBtn").unbind("click").bind("click", function() {
				if (!isDisabled('#deleteResourceBtn')) {
					disabledButton('#deleteResourceBtn');
					that.notifyDeleteResource(that.resourceId, that.productId);
				} else {
					enabledButton('#deleteResourceBtn');
				}
			});
			that.initProductsTable();
			that.initResourcesTable();
			$("#fileUploadAddResource").fileupload(that.getResourceUpload());
			$("#fileUploadAddProductImage").fileupload(that.getProductImageUpload());
			that.validateProductForm();
		};
		this.initProductsTable = function(){
            
			$('#listProductsTable').dataTable(
					{
						"language": {
			                url: 'plugins/datatables/'+getIdLangParam()+'.json'
			            },
						"aaData" : [],
						"aoColumns" : [ {
							"sTitle" : LOGO_LABEL,
							"sWidth" : "10%",
						},{
							"sTitle" : TITLELABEL,
							"sWidth" : "15%",
						}, {
							"sTitle" : DESCRIPTIONLABEL,
							"sWidth" : "45%",
						}, {
							"sTitle" : LANGUAGESLABEL,
							"sWidth" : "10%"
						}, {
							"sTitle" : ACTIONSLABEL,
							"sWidth" : "20%"
						}, {
							"sTitle" : "",
						} ],
						"aaSorting" : [ [ 5, "asc" ] ],
						"aoColumnDefs" : [ {
							"bVisible" : false,
							"aTargets" : [ 5 ]
						} ],
						"bAutoWidth" : false,
						"bRetrieve" : false,
						"bFilter" : false,
						"bDestroy" : true,
						"iDisplayLength" : 10,
						"fnDrawCallback" : function() {
							$('.viewProductBtn, .editProductBtn, .deleteProductBtn, .attachFilesProductBtn, .traductProductBtn').unbind('click');
							$('.viewProductBtn').bind('click',
									function() {
										that.viewProductHandler($(this).attr("index"));
									});
							$('.editProductBtn').bind('click',
									function() {
										that.updateProductHandler($(this).attr("index"));
									});
							$('.deleteProductBtn').bind('click',
									function() {
										that.deleteProductHandler($(this).attr("index"));
									});
							$('.attachFilesProductBtn').bind('click',
									function() {
										that.attachFilesProductHandler($(this).attr("index"));
									});
							$('.traductProductBtn').bind('click',
									function() {
										that.traductProductHandler($(this).attr("index"));
									});
						}
					});
		};
		this.initResourcesTable = function(){
			$('#listResourcesTable')
				.dataTable(
					{
						"aaData" : [],
						"aoColumns" : [ {
							"sTitle" : TYPE_LABEL,
							"sWidth" : "15%",
						},{
							"sTitle" : TITLELABEL,
							"sWidth" : "60%",
						},{
							"sTitle" : ACTIONSLABEL,
							"sWidth" : "25%"
						},{
							"sTitle" : "",
						} ],
						"aaSorting" : [ [ 3, "asc" ] ],
						"aoColumnDefs" : [ {
							"bVisible" : false,
							"aTargets" : [ 3 ]
						} ],
						"bAutoWidth" : false,
						"bRetrieve" : false,
						"bFilter" : false,
						"bDestroy" : true,
						"iDisplayLength" : 5,
						"fnDrawCallback" : function() {
							$('.deleteResourceBtn').unbind('click');
							$('.deleteResourceBtn').bind('click',
									function() {
										that.deleteResource($(this).attr("index"));
									});
						}
					});
		};
		this.validateProductForm = function() {
			validatorProduct = $("#productForm").validate({
				errorPlacement : function errorPlacement(error, element) {
					element.after(error);
				},
				rules : {
					productTitle : {
						required : true
					}
				}
			});
		};
		this.getResourceUpload = function(){
			var timeUpload = [];
			return {
				beforeSend : function(jqXHR, settings) {
                	beforeUploadFile('', '#progressProductResource', '#resourceLoaded,#loadResourceError');
				},
                datatype: 'json',
                cache : false,
				add : function(e, data) {
					var goUpload = true;
					var uploadFile = data.files[0];
	        		var fileName=uploadFile.name;
	        		timeUpload[fileName]=(new Date()).getTime();
					data.url = getUrlPostUploadFile(timeUpload[fileName], fileName);
					if (!(FILE_EXTENSION_REGEX)
							.test(fileName)) {
						goUpload = false;
					}
					if (goUpload == true) {
						var fileSize = uploadFile.size;
						if((DOC_EXTENSION_REGEX).test(fileName) && fileSize > MAX_FILE_SIZE) {
							goUpload = false;
						}else if((VIDEO_EXTENSION_REGEX).test(fileName) && fileSize > MAX_VIDEO_SIZE) {
							goUpload = false;
						}else if((IMAGE_EXTENSION_REGEX).test(fileName) && fileSize > MAX_FILE_SIZE){
							goUpload = false;
						}
	                }
					if (goUpload == true) {
						jqXHRPhoto=data.submit();
					} else {
						$('.resourceSizeHintLabel').text(ACCEPT_PRODUCT_RESOURCE);
						$('#loadResourceError').show();
					}
				},
		        progressall: function (e, data) {
    	            progressBarUploadFile('#progressProductResource', data, 100);
		        },
				done : function(e, data) {
					var file=data.files[0];
                	var fileName=file.name;
                	var hashFileName=getUploadFileName(timeUpload[fileName], fileName);
                	// console.log("Add photo done, filename :
									// "+fileName+", hashFileName : "+hashFileName);
                	var resourceUrl = getUploadFileUrl(hashFileName);
					var resource = new $.Resource();
	            	var resourceTypeId = null;
	            	if((DOC_EXTENSION_REGEX).test(fileName) ) {
	                    resourceTypeId = resourceTypeEnum.DOCUMENT;
	                }else if((VIDEO_EXTENSION_REGEX).test(fileName)) {
	                	resourceTypeId = resourceTypeEnum.VIDEO;
	                }else if((IMAGE_EXTENSION_REGEX).test(fileName)){
	                	resourceTypeId = resourceTypeEnum.IMAGE;
	                }
	            	if(resourceTypeId != null){
		            	resource.setResourceName(fileName);
		            	resource.setResourceUrl(resourceUrl);
		            	resource.setCreationDate(new Date());
		            	
		            	var resourceType = new $.ResourceType();
		            	resourceType.setResourceTypeId(resourceTypeId);
		            	resource.setResourceType(resourceType);
		            	
						that.notifyAddResource(resource, that.productId);
	            	}
            		afterUploadFile('', '#resourceLoaded', '#progressProductResource,#loadResourceError');
				}
	        };
		};
		this.getProductImageUpload = function(){
			var timeUpload = [];
			return {
				beforeSend : function(jqXHR, settings) {
                	beforeUploadFile('#saveProductBtn', '#progressProductImage', '#loadProductImage,#loadProductImageError');
				},
                datatype: 'json',
                cache : false,
				add : function(e, data) {
					var goUpload = true;
					var uploadFile = data.files[0];
	        		var fileName=uploadFile.name;
	        		timeUpload[fileName]=(new Date()).getTime();
					data.url = getUrlPostUploadFile(timeUpload[fileName], fileName);
					if (!(/\.(pdf|jpg|jpeg|png)$/i).test(fileName)) {
						goUpload = false;
					}
					if (goUpload == true) {
						var fileSize = uploadFile.size;
						if(fileSize > MAXFILESIZE) {
							goUpload = false;
						}
	                }
					if (goUpload == true) {
						jqXHRPhoto=data.submit();
					}else 
						$('#loadProductImageError').show();
				},
		        progressall: function (e, data) {
    	            progressBarUploadFile('#progressProductImage', data, 100);
		        },
				done : function(e, data) {
					var file=data.files[0];
                	var fileName=file.name;
                	var hashFileName=getUploadFileName(timeUpload[fileName], fileName);
                	console.log("Add photo done, filename : "+fileName+", hashFileName : "+hashFileName);
                	var productImageUrl = getUploadFileUrl(hashFileName);
                	$('#productImageEdited .productImage').attr("src", productImageUrl);
            		afterUploadFile('#saveProductBtn', '#loadProductImage, #productImageEdited', '#progressProductImage,#loadProductImageError');
				}
	        };
		};
		this.saveProduct = function(productId, productTitle, productDescription, productImage){
			disabledButton('#saveProductBtn');
			$('#loadingEds').show();
			var product = null;
			if (productId != 0) {
				product = productsList[productId];
			}else product = new $.Product();
			product.setProductTitle(productTitle);
			product.setProductDescription(productDescription);
			product.setProductImage(productImage);
			if(that.isTraduct){
				that.notifyTraductProduct(product, $('#languageTranslateSelect').val());
			}else if (productId != 0) {
				that.notifyUpdateProduct(product);
			} else {
				that.notifAddProduct(product);
			}
			
		};
		this.viewProductHandler = function(index){
			var product = productsList[index];
			if (product != null) {
			 	$('#loadingEds').show();
				that.notifyGetProduct(product.getProductId());
			}
		};
		this.updateProductHandler = function(index){
			$('#popupAddProduct').find('#modalTitle').text(MODIFY_PRODUCT);
			var product = productsList[index];
			if (product != null) {
				that.productId = product.getProductId();
				that.isTraduct = false;
				enabledButton('#saveProductBtn');
				$("#addProductTitlePopup").text(EDITLABEL);
				$("#productTitle").val(product.getProductTitle());
				$("#productDescription").val(product.getProductDescription());
				$('#productImageEdited .productImage').attr("src", product.getProductImage());
				$('#loadProductImage,#productImageEdited').show();
				$('#productImageLoaded,#progressProductImage,#loadProductImageError, .traductBlock').hide();
				$("#popupAddProduct").modal();
				validatorProduct.resetForm();
			}
		};
		this.deleteProductHandler = function(index){
			var product = productsList[index];
			if (product != null) {
				that.productId = product.getProductId();
				enabledButton('#deleteProductBtn');
				$("#popupDeleteProduct").modal();
			}
		};
		this.attachFilesProductHandler = function(index){
			var product = productsList[index];
			if (product != null) {
				$('#loadingEds').show();
				that.productId = product.getProductId();
				that.notifyGetResources(that.productId);
			}
		};
		this.traductProductHandler = function(index){
			var product = productsList[index];
			$(".traductBlock").show();
			$('#popupAddProduct').find('#modalTitle').text(TRANSLATE_PRODUCT);
			if (product != null) {
				that.productId = product.getProductId();
				that.isTraduct = true;
				var languages = product.getLanguages();
				enabledButton('#saveProductBtn');
				$("#languageTranslateSelect").empty();
				for (var languageId in listLangsEnum) {
				   if(languages[languageId] == null){
					   $("#languageTranslateSelect").append(
							$('<option/>').val(languageId).html(listLangsEnum[languageId]));
				   }
				}
				$("#addProductTitlePopup").text(TRADUCTLABEL);
				$("#productTitle").val(product.getProductTitle());
				$("#productDescription").val(product.getProductDescription());
				$('#productImageEdited .productImage').attr("src", product.getProductImage());
				$('#loadProductImage,#productImageEdited').show();
				$('#productImageLoaded,#progressProductImage,#loadProductImageError').hide();
				$("#popupAddProduct").modal();
			}
		};
		this.displayProducts = function(products){
			var rightGroup = listStandRights[rightGroupEnum.PRODUCTS];
			if (rightGroup != null) {
				var permittedProductsNbr = rightGroup.getRightCount();
				var productsNbr = Object.keys(products).length;
				if (permittedProductsNbr == -1) {
					$('#addProductDiv #addProductLink').text(ADDLABEL);
					$('#addProductDiv').show();
				} else if (productsNbr < permittedProductsNbr) {
					$('#numberProducts').text(productsNbr);
					$('#numberPermittedProducts')
							.text(permittedProductsNbr);
					$('#addProductDiv').show();
				} else {
					$('#addProductDiv').hide();
				}
			}
			productsList = products;
			that.listingProducts(products);
		};
		this.listingProducts = function(products) {
			var datas = new Array();
			var product = null;
			var productName = null;
			var productDescription = null;
			var title = null;
			var description = null;
			var btnViewProductHandler = $("<a class='viewLink viewProductBtn actionBtn'><i class='fa fa-eye'></i></a>");
			var btnEditProductHandler = $("<a class='editProductBtn actionBtn'><i class='fa fa-pencil-square-o'></i></a>");
			var btnDeleteProductHandler = $("<a class='deleteLink deleteProductBtn actionBtn'><i class='fa fa-trash-o'></i></a>");
			var btnAttachFilesProductHandler = $("<a class='attachLink attachFilesProductBtn actionBtn'><i class='fa fa-cloud-upload'></i></a>");
			var btnTraductProductHandler = $("<a class='translateLink traductProductBtn actionBtn'><i class='fa fa-globe'></i></a>");
			var imageProductHandler = $("<img src='#' class='productImage'></a>");
			var listActions = $("<div></div>");
			var actions = null;
			/*btnViewProductHandler.text(VIEWLABEL);
			btnEditProductHandler.text(EDITLABEL);
			btnDeleteProductHandler.text(DELETELABEL);
			btnAttachFilesProductHandler.text(ATTACH_FILES);
			btnTraductProductHandler.text(TRADUCTLABEL);*/
			for (var index in products) {
				product = products[index];
				productName = product.getProductTitle();
				productDescription = product.getProductDescription();
				title = substringCustom(formattingLongWords(productName, 70), 200);
				description = substringCustom(formattingLongWords(productDescription, 70), 500);
				btnViewProductHandler.attr("index", index);
				btnEditProductHandler.attr("index", index);
				btnDeleteProductHandler.attr("index", index);
				btnAttachFilesProductHandler.attr("index", index);
				imageProductHandler.attr("index", index);
				imageProductHandler.attr("src", product.getProductImage());
				btnTraductProductHandler.attr("index", index);
				actions = getHtml(btnViewProductHandler); 
				if(product.getLanguages()[getIdLangParam()] != null){
					actions += getHtml(btnEditProductHandler); 
				}
				actions += getHtml(btnDeleteProductHandler); 
				actions += getHtml(btnAttachFilesProductHandler); 
				if(!product.getAlreadyTraduct()){	
					actions += getHtml(btnTraductProductHandler); 
				}
				listActions.html(actions);
				datas.push([ getHtml(imageProductHandler), title, description, product.getNameLanguages(), 
				             getHtml(listActions), 
				             product.getProductId()]);
			}
			addTableDatas('#listProductsTable', datas);
			$('#loadingEds').hide();
		};
		
		this.addProductSuccess = function(){
			enabledButton('#saveProductBtn');
			$("#popupAddProduct").modal('hide');
			//hidePopups();
		};
		
		this.updateProductSuccess = function(){
			enabledButton('#saveProductBtn');
			$("#popupAddProduct").modal('hide');
			//hidePopups();
		};
		
		this.deleteProductSuccess = function(){
			enabledButton('#popupDeleteResource');
			$("#popupDeleteResource").modal('hide');
			//hidePopups();
		};
		
		this.deleteResourceSuccess = function(){
			enabledButton('#deleteResourceBtn');
			$("#popupDeleteResource").modal('hide');
		};
		
		this.displayProduct = function(product){
			$("#productDetailTitle").text(product.getProductTitle());
			$("#productDetailDescription").text(product.getProductDescription());
			$("#productDetailImage").attr("src", product.getProductImage());
			$("#productDetailResources").empty();
			var resources = product.getResources();
			var resource = null;
			if(resources.length != 0){
				for(var index in resources){
					resource = resources[index];
					$("#productDetailResources")
						.append('<li><a href="' + replaceAllHtmlCast(resource.getResourceUrl()) + '" target="_blank">' + htmlEncode(resource.getResourceName()) + '</a></li>');
				}
				$('.ressourceList').show();
			}else{
				$('.ressourceList').hide();
			}
				
			$('#loadingEds').hide();
			$("#popupViewProduct").modal();
		};

		this.displayResources = function(resources){
			var datas = new Array();
			var resource = null;
			var resourceName = null;
			var resourceTypeId = null;
			var resourceTypeIconSrc = null;
			var resourceTypeIconTitle = null;
			var btnDeleteResourceHandler = $("<button class='btn btn-danger btn-middle deleteResourceBtn'/>");
			var titleResourceHandler = $("<a class='resourceName' href='#' target='_blank'/>");
			var iconResourceHandler = $("<img class='resourceIcon' src='#'/>");
			var listActions = $("<div></div>");
			btnDeleteResourceHandler.text(DELETELABEL);
			for (var index in resources) {
				resource = resources[index];
				resourceName = resource.getResourceName();
				resourceTypeId = resource.getResourceType().getResourceTypeId();
				if(resourceTypeId == resourceTypeEnum.IMAGE){
					resourceTypeIconSrc = 'img/image_icon.png';
					resourceTypeIconTitle = IMAGE_LABEL;
				}else if(resourceTypeId == resourceTypeEnum.VIDEO){
					resourceTypeIconSrc = 'img/video_icon.png';
					resourceTypeIconTitle = VIDEO_LABEL;
				}else if(resourceTypeId == resourceTypeEnum.DOCUMENT){
					resourceTypeIconSrc = 'img/doc_icon.png';
					resourceTypeIconTitle = DOCUMENT_LABEL;
				}
				resourceName = substringCustom(formattingLongWords(resourceName, 70), 200);
				titleResourceHandler.attr('href', resource.getResourceUrl());
				titleResourceHandler.text(resourceName);
				iconResourceHandler.attr('src', resourceTypeIconSrc);
				iconResourceHandler.attr('title', resourceTypeIconTitle);
				btnDeleteResourceHandler.attr("index", index);
				listActions.html(getHtml(btnDeleteResourceHandler));
				datas.push([ getHtml(iconResourceHandler), getHtml(titleResourceHandler), 
				             getHtml(listActions), resource.getResourceId()]);
			}
			addTableDatas('#listResourcesTable', datas);
			$('#loadingEds').hide();
			$('#loadResourceError').hide();
			$('#resourceLoaded').show();
			$('#progressProductResource').hide();
			$("#popupAttachFilesProduct").modal();
		};
		
		this.deleteResource = function(resourceId){
			$("#popupDeleteResource").modal();
			that.resourceId = resourceId;
		};
		
		this.notifAddProduct = function(product){
			$.each(listeners, function(i) {
				listeners[i].addProduct(product);
			});
		};
		
		this.notifyUpdateProduct = function(product){
			$.each(listeners, function(i){
				listeners[i].updateProduct(product);
			});
		};
		
		this.notifyTraductProduct = function(product, langId){
			$.each(listeners, function(i){
				listeners[i].traductProduct(product, langId);
			});
		};
		
		this.notifyDeleteProduct = function(productId){
			$.each(listeners, function(i){
				listeners[i].deleteProduct(productId);
			});
		};
		
		this.notifyGetProduct = function(productId){
			$.each(listeners, function(i){
				listeners[i].getProduct(productId);
			});
		};
		
		this.notifyGetResources = function(productId){
			$.each(listeners, function(i){
				listeners[i].getResources(productId);
			});
		};
		
		this.notifyAddResource = function(resource, productId){
			$.each(listeners, function(i){
				listeners[i].addResource(resource, productId);
			});
		};
		
		this.notifyDeleteResource = function(resourceId, productId){
			$.each(listeners, function(i){
				listeners[i].deleteResource(resourceId, productId);
			});
		};
		
		this.addListener = function(list){
			listeners.push(list);
		};
		
	},
	
	ProductViewListener: function(list) {
		if(!list) list = {};
		return $.extend(list);
	}
	
});

