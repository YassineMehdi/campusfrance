jQuery.extend({

	ProductController: function(model, view){
		//var that = this;
		/**
		 * listen to the view
		 */
		var vlist = $.ProductViewListener({
			loadProducts : function(products){
				view.displayProducts(products);
			},
			addProductSuccess : function(){
				view.addProductSuccess();
			},
			updateProductSuccess : function(){
				view.updateProductSuccess();
			},
			deleteProductSuccess : function(){
				view.deleteProductSuccess();
			},
			loadProduct : function(product){
				view.displayProduct(product);
			},
			loadResources : function(resources){
				view.displayResources(resources);
			},
			deleteResourceSuccess : function(){
				view.deleteResourceSuccess();
			},
		});
		view.addListener(vlist);

		/**
		 * listen to the model
		 */
		var mlist = $.ProductModelListener({		
			addProduct : function(product){
				model.addProduct(product);
			},
			updateProduct : function(product){
				model.updateProduct(product);
			},
			traductProduct : function(product, langId){
				model.traductProduct(product, langId);
			},
			deleteProduct : function(productId){
				model.deleteProduct(productId);
			},
			getProduct : function(productId){
				model.getProduct(productId);
			},
			getResources : function(productId){
				model.getResources(productId);
			},
			addResource : function(resource, productId){
				model.addResource(resource, productId);
			},
			deleteResource : function(resourceId, productId){
				model.deleteResource(resourceId, productId);
			},
		});
		
		model.addListener(mlist);
		
		this.initController = function(){
			view.initView();
			model.initModel();
		};
		this.getAnalyticsProducts = function(callBack, path) {
			model.getAnalyticsProducts(callBack, path);
		};
		this.getProductById = function(productId) {
			return model.getProductById(productId);
		};
	}

});
