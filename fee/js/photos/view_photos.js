jQuery.extend({
    PhotosView: function () {
        var that = this;
        var listeners = new Array();
        var photosCache = [];
        var jqXHRPoster = null;
        var addPhotoBtnHandler = null;
        var modalPhotoHandler = null;
        var savePhotoHandler = null;
        var btnDeletePhotoHandler = null;
        var jqXHRPhoto = null;

        var contentHandler = null;
        var loadAdviceErrorHandler = null;
        var progressAdviceHandler = null;
        var photoFormHandler = null;
        var fileinputHandler = null;
        var titlePosterHandler = null;
        var validatorImage = null;


        this.initView = function () {
            traduct();
            contentHandler = $("#mainBackEndContainer");
            addPhotoBtnHandler = $(".addPhotos");
            modalPhotoHandler = $("#addPhotos");
            savePhotoHandler = $("#savePhoto");

            loadAdviceErrorHandler = $("#loadPosterError");
            progressAdviceHandler = $("#progressPoster");
            photoFormHandler = $("#photoForm");
            titlePosterHandler = $("#titlePoster");
            fileinputHandler = $(".fileinput-filename");

            btnDeletePhotoHandler = $("#btnDeletePhoto");
            jqXHRPoster = null;

            that.validatePhotoForm();
            //Add Photo
            addPhotoBtnHandler.unbind("click").bind("click", function (e) {
                that.factoryAddPhoto();
            });
            //save Photo
            savePhotoHandler.unbind("click").bind("click", function (e) {
                that.savePoster();
            });
            //Delete Photo
            btnDeletePhotoHandler.unbind("click").bind("click", function (e) {
                that.notifyDeletePhoto($('#idDeletePhoto').val());
            });
            //file input
            $("#photoUpload").fileupload({
                beforeSend: function (jqXHR, settings) {
                    beforeUploadFile('#savePhoto', '#progressPhoto', '#loadPhotoError');
                },
                datatype: 'json',
                cache: false,
                add: function (e, data) {
                    var goUpload = true;
                    var uploadFile = data.files[0];
                    var fileName = uploadFile.name;
                    timeUploadPoster = (new Date()).getTime();
                    data.url = getUrlPostUploadFile(timeUploadPoster, fileName) + "&isPoster=true";
                    if (!(/\.(pdf|jpg|jpeg|png)$/i).test(fileName)) {
                        goUpload = false;
                    }
                    if (uploadFile.size > MAXFILESIZE) {
                        goUpload = false;
                    }
                    if (goUpload == true) {
                        jqXHRPoster = data.submit();
                    } else
                        $('#loadPhotoError').show();
                },
                progressall: function (e, data) {
                    progressBarUploadFile('#progressPhoto', data, 99);
                },
                done: function (e, data) {
                    var file = data.files[0];
                    var fileName = file.name;
                    //console.log(timeUploadPoster);
                    var hashFileName = getUploadFileName(timeUploadPoster, fileName);
                    console.log("Add poster file done, filename :" + fileName + ", hashFileName : " + hashFileName);
                    $('#loadPhotoError').show();
                    $('#loadPhoto .error').hide();
                    $('.fileInfo').text(fileName);
                    $('.fileInfo').attr('href', getUploadFileUrl(hashFileName));
                    afterUploadFile('#savePhoto', '#photoLoaded', '#progressPhoto,#loadPhotoError');
                }
            });


            //datatable
            $('#listPhotosTable').dataTable({
                "language": {
                    url: 'plugins/datatables/'+getIdLangParam()+'.json'
                },
                "aaData": [],
                "aaSorting": [],
                "aoColumns": [
                    {
                        "sTitle": TITLELABEL
                    },
                    {
                        "sTitle": CONTENTLABEL
                    },
                    {
                        "sTitle": ACTIONSLABEL
                    }
                ],
                "bFilter": false,
                "bLengthChange": false,
                "bAutoWidth": false,
                "bRetrieve": false,
                "bDestroy": true,
                "iDisplayLength": 10,
                "fnDrawCallback": function () {
                    $('.viewPhotoLink').off();
                    $('.viewPhotoLink').on("click", function () {
                        var photoId = $(this).closest('div').attr('id');
                        that.factoryViewPhoto(photosCache[photoId]);

                    });
//
                    $('.editPhotosLink').off();
                    $('.editPhotosLink').on("click", function () {
                        var photoId = $(this).closest('div').attr('id');
                        that.factoryUpdatePhoto(photosCache[photoId]);
                    });
//
                    $('.deletePhotsLink').off();
                    $('.deletePhotsLink').on("click", function () {
                        var photoId = $(this).closest('div').attr('id');
                        $('#removePhoto').modal();
                        $('#idDeletePhoto').val(photoId);
                    });
                }
            });

            //Dismiss video
            $('#addPhotos').on('hide.bs.modal', function (e) {
                $('.progress').hide();
                if (jqXHRPhoto != null)
                    jqXHRPhoto.abort();
            });
        };
        this.displayPhotos = function (photos) {
            photosCache = photos;
            
            var rightGroup = listStandRights[rightGroupEnum.PHOTOS];
            if (rightGroup != null) {
                var permittedPhotosNbr = rightGroup.getRightCount();
                var photosNbr = Object.keys(photos).length;
                if (permittedPhotosNbr == -1) {
                    $('#addPhotoDiv #addPhotoLink').text(ADDLABEL);
                    $('#addPhotoDiv').show();
                } else if (photosNbr < permittedPhotosNbr) {
                    $('#numberPhotos').text(photosNbr);
                    $('#numberPermittedPhotos').text(permittedPhotosNbr);
                    $('#addPhotoDiv').show();
                } else {
                    $('#addPhotoDiv').hide();
                }
            }

            var photoList = [];
            var photo = null;
            var photoId = null;
            var photoUrl = null;
            var photoTitle = null;
            var photoDescription = null;
            var photoImage = null;
            var data = null;
            var listActions = null;
            var photoImg = null;
            var photoContent = null;

            for (var index in photos) {
                photo = photos[index];
                photoId = photo.getIdFile();
                photoUrl = photo.getFileUrl();
                photoTitle = photo.getFileTitle();
                photoDescription = photo.getFileDescription();
                photoImage = photo.getFileImage();
                photoImg = "<img class='apercuImg' src='" + photoUrl + "' alt='" + photoTitle + "' />";
                photoContent = "<div class='contentTruncate'>" + photoTitle.replace(/<(?:.|\n)*?>/gm, '') + "</div>";
                listActions = "<div class='actionBtn' id='" + photoId + "'><a href='javascript:void(0);' class='editPhotosLink editLink actionBtn'><i class='fa fa-pencil-square-o'></i></a> <a href='javascript:void(0);' class='viewPhotoLink viewLink actionBtn'><i class='fa fa-eye'></i></a> <a href='javascript:void(0);' class='deletePhotsLink deleteLink actionBtn'><i class='fa fa-trash-o'></i></a></div>";
                data = [photoImg, photoContent, listActions];
                photoList.push(data);
            }
            addTableDatas("#listPhotosTable", photoList);
            $('#loadingEds').hide();
        };
//        //Add Poster
        this.factoryAddPhoto = function () {
            that.update = false;
            modalPhotoHandler.modal();
            validatorImage.resetForm();
            loadAdviceErrorHandler.hide();
            progressAdviceHandler.hide();
            $('.fileInfo').attr('src');
            $('.fileInfo').text('');
            photoFormHandler[0].reset();
        };
        this.addPhotoSuccess = function () {
            modalPhotoHandler.modal('hide');
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
//        //Save Poster
        this.savePoster = function () {
            that.validatePhotoForm();
            if ($("#photoForm").valid()) {
                var photo = new $.File();
                photo.setIdFile(0);
                photo.setFileTitle($('#photoTitle').val());
                photo.setFileDescription($('#photoDescription').val());
                photo.setFileUrl($('.fileInfo').attr('href'));
                photo.setFileName($('.fileInfo').text());

                photo.setFileImage(photo.getFileUrl());

                var fileType = new $.FileType();
                fileType.setFileTypeId(fileTypeEnum.PHOTO);
                photo.setFileType(fileType);

                if (!that.update) {
                    that.notifyAddPhoto(photo);
                }
                else {
                    photo.setIdFile($('#idPhoto').val());
                    that.notifyUpdatePhoto(photo);
                }

            }
            ;
        };
//        // Update Advice
        this.factoryUpdatePhoto = function (photo) {
            that.update = true;
            modalPhotoHandler.modal();
            validatorImage.resetForm();
            $('#idPhoto').val(photo.getIdFile());
            $('#photoTitle').val(photo.getFileTitle());
            $('#photoDescription').val(photo.getFileDescription());
            $('#addPhotos .fileInfo').attr('href', photo.getFileUrl());
            $('#addPhotos .fileInfo').text(photo.getFileName());
        };

        this.updatePhotoSuccess = function () {
            modalPhotoHandler.modal('hide');
            modalPhotoHandler.find('#photoForm')[0].reset();
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
//        // View Advice
        this.factoryViewPhoto = function (photo) {
            var photoUrl = photo.getFileUrl();
            var photoTitle = photo.getFileTitle();
            $('#viewPhoto').modal();
            $('#titlePhoto').text(photoTitle);
            var img_container = $('<img src="' + photoUrl + '" title="' + photoTitle + '" alt="' + photoTitle + '" />');
            $('#contentViewPhoto').html(img_container);


        };
//        // Delete Poster
        this.deletePhotoSuccess = function () {
            modalPhotoHandler.modal('hide');
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
//        // Validator form
        this.validatePhotoForm = function () {
            $.validator.addMethod("uploadFile", function (val, element) {
                //console.log(val);
                var fileName = $(".fileInfo").text();
                var fileUrl = $(".fileInfo").attr("href");
                if (fileName == '' || fileUrl == '') {
                    return false;
                } else {
                    return true;
                }

            }, "File type error");

            validatorImage = $("#photoForm").validate({
                rules: {
                    photoTitle: {
                        required: true
                    }
                }
            });
            $("#photoUpload").rules("add", {
                uploadFile: true
            });
        };

        this.addListener = function (list) {
            listeners.push(list);
        };
        this.notifyAddPhoto = function (photo) {
            $.each(listeners, function (i) {
                listeners[i].addPhoto(photo);
            });
        };
        this.notifyDeletePhoto = function (photoId) {
            $.each(listeners, function (i) {
                listeners[i].deletePhoto(photoId);
            });
        };
        this.notifyUpdatePhoto = function (photo) {
            $.each(listeners, function (i) {
                listeners[i].updatePhoto(photo);
            });
        };

    },
    PhotosViewListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }



});
