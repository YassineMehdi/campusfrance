jQuery.extend({

	VisitorModel : function() {
		var listeners = new Array();
		var that = this;
		this.addListener = function(list) {
			listeners.push(list);
		};
		this.initModel = function(){
			//that.getSearchProfiles();
			//that.getStandVisitors();
			//that.getSkypeMailTemplate();
		};
		this.beforeInitModel = function(){
			getUserFavorites();
			getActivitySectors();
			getRegions();
			getStudyLevels();
			getFunctionsCandidate();
			getExperienceYears();
			getAreaExpertises();
			getJobsSought();
			getCandidateStates();
			getCountries();
		};
		that.beforeInitModel();
		this.objToString = function(obj) {
		    var str = '';
		    for (var p in obj) {
		        if (obj.hasOwnProperty(p)) {
		            str += p + '::' + obj[p] + '\n';
		        }
		    }
		    return str;
		};
		this.notifyGetCandidatesByCVKeywords = function(candidates) {
			$.each(listeners, function(i) {
				listeners[i].displayGetCandidatesByCVKeywords(candidates);
			});
		};
		this.getCandidatesByCVKeywordsSuccess = function(xml) {
			candidates = new Array();
			$(xml).find("candidate").each(function() {
				var candidate = new $.Candidate($(this));
				candidates[candidate.getCandidateId()] = candidate;
			});
			that.notifyGetCandidatesByCVKeywords(candidates);
		};
		this.getCandidatesByCVKeywords = function(cvKeywordValue) {
			genericAjaxCall("GET", staticVars["urlBackEnd"] + 'cvkeyword/getCandidateByCVKeywordValue?cvKeywordValue='+cvKeywordValue, 'application/xml', 'xml', '', that.getCandidatesByCVKeywordsSuccess, '');
		};
		this.notifyLoadForumEnrolled = function(candidates) {
			$.each(listeners, function(i) {
				listeners[i].displayForumEnrolled(candidates);
			});
		};
		this.notifyLoadFavoriteUsers = function(candidates) {
			$.each(listeners, function(i) {
				listeners[i].displayFavoriteUsers(candidates);
			});
		};
		this.getCandidateListFromXML = function(xml) {
			candidates = new Array();
			$(xml).find("candidateDTO").each(function() {
				var candidate = new $.Candidate($(this));
				candidates[candidate.getCandidateId()] = candidate;
			});
			return candidates;
		};
		this.getForumEnrolledSuccess = function(xml) {
			that.notifyLoadForumEnrolled(that.getCandidateListFromXML(xml));
		};
		this.updateOnlyCandidates = function(xml) {
			that.notifyLoadFavoriteUsers(that.getCandidateListFromXML(xml));
		};
		this.getForumEnrolled = function() {
			console.log('inside initModel');
			genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'candidate/getCandidatesInfo?idLang='+getIdLangParam(), 'application/xml', 'xml', '', that.getForumEnrolledSuccess, '');
		};
		this.getFavoriteUsers = function() {
			genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'candidate/getCandidatesInfo?idLang='+getIdLangParam(), 'application/xml', 'xml', '', that.updateOnlyCandidates, '');
		};
		this.getSearchProfilesSuccess = function(xml) {
			that.notifyLoadSearchProfiles(that.getCandidateListFromXML(xml));
		};
		this.getSearchProfiles = function() {
			genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'candidate/getCandidatesInfo?idLang='+getIdLangParam(), 'application/xml', 'xml', '', that.getSearchProfilesSuccess, '');
		};

		this.getConnectedUsersSuccess = function(xml, candidates) {
			var visitors = new Array();
			$(xml).find("candidate").each(function() {
				var candidate = new $.Candidate($(this));
				visitors[candidate.getCandidateId()] = candidate;
			});
			that.notifyLoadStandVisitors(visitors, candidates);
		};
		this.getConnectedUsers = function(candidates){
			/*genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'userprofile/getUserConnectedList?standId='+$.cookie('standId'), 'application/xml', 'xml', '', function(xml){
				that.notifyLoadStandVisitors(xml);
			}, '');*/
			genericAjaxCall('GET', staticVars["urlBackEnd"] + 'candidate/standVisitors?idLang='+getIdLangParam(), 'application/xml', 'xml', '', function(xml){
				that.getConnectedUsersSuccess(xml, candidates);
			});
		};
		this.getStandVisitorsSuccess = function(xml) {
			candidates = that.getCandidateListFromXML(xml);
			that.getConnectedUsers(candidates);
		};
		this.getStandVisitorsError = function(xhr, status, error) {
			isSessionExpired(xhr.responseText);
		};
		this.getStandVisitors = function() {
			genericAjaxCall('GET', staticVars["urlBackEndCandidate"] + 'candidate/getCandidatesInfo?idLang='+getIdLangParam(), 'application/xml', 
					'xml', '', that.getStandVisitorsSuccess, that.getStandVisitorsError);
		};
		this.skypeMailTemplateSuccess = function(xml){
			that.notifSkypeMailTemplateSuccess(xml);
		};
		this.getSkypeMailTemplate = function(){
			genericAjaxCall('GET', staticVars["urlBackEnd"]+'mailTemplate/skype?idLang='+getIdLangParam(), 'application/xml', 
					'xml', '', that.skypeMailTemplateSuccess, that.skypeMailTemplateError);
		};
		this.sendMessageSuccess = function(xml){
			$(".backgroundPopup").hide();
			messageSent();
		};
		this.sendMessageError = function(xhr,status,error){
			isSessionExpired(xhr.responseText);
		};
		this.sendMessage = function(message, candidateId){
			genericAjaxCall('POST', staticVars["urlBackEnd"]+'message/sendMessage?candidateId='+candidateId, 'application/xml', 
					'xml', message.xmlData(), that.sendMessageSuccess, that.sendMessageError);
		};
		this.sendSkypeMessageSuccess = function(xml){
			$(".backgroundPopup").hide();
			messageSent();
		};
		this.sendSkypeMessageError = function(xhr,status,error){
			isSessionExpired(xhr.responseText);
		};
		this.sendSkypeMessage = function(message, candidateId){
			genericAjaxCall('POST', staticVars["urlBackEnd"]+'message/sendSkypeMessage?candidateId='+candidateId+'&idLang='+getIdLangParam(), 'application/xml', 
					'xml', message.xmlData(), that.sendSkypeMessageSuccess, that.sendSkypeMessageError);
		};
		this.sendShareProfileSuccess = function(xml){
			showPopupMessageSent();
		};
		this.sendShareProfileError = function(xhr,status,error){
			isSessionExpired(xhr.responseText);
		};
		this.sendShareProfile = function(email){
			genericAjaxCall('POST', staticVars["urlBackEnd"]+'candidate/sendShareProfile?idLang='+getIdLangParam(), 'application/xml', 
					'xml', email.xmlData(), that.sendShareProfileSuccess, that.sendShareProfileError);
		};
		this.updateCandidateStateSuccess = function(index, candidateStateId){
			that.notifyUpdateCandidateStateSuccess(index, candidateStateId);
		};

		this.updateCandidateStateError = function(xhr, status, error){
		   isSessionExpired(xhr.responseText);
		};
		
		this.updateCandidateState = function(candidateId, candidateStateId, index) {
			genericAjaxCall('POST', staticVars["urlBackEnd"]+' candidate/updateState?candidateId='+candidateId+'&candidateStateId='+candidateStateId+'&idLang='+getIdLangParam(), 
					'application/xml', 'xml','', function(){
					that.updateCandidateStateSuccess(index, candidateStateId);
				}, that.updateCandidateStateError);
		};
		this.notifyLoadSearchProfiles = function(candidates) {
			$.each(listeners, function(i) {
				listeners[i].displaySearchProfiles(candidates);
			});
		};
		this.notifSkypeMailTemplateSuccess = function(xml){
			$.each(listeners, function(i){
				listeners[i].skypeMailTemplateSuccess(xml);
			});
		};
		this.notifyUpdateCandidateStateSuccess = function(candidateId, candidateStateId){
			$.each(listeners, function(i){
				listeners[i].updateCandidateStateSuccess(candidateId, candidateStateId);
			});
		};
		this.notifyLoadStandVisitors = function(visitors, candidates) {
			$.each(listeners, function(i) {
				listeners[i].displayLoadStandVisitors(visitors, candidates);
			});
		};
	},

	VisitorModelListener : function(list) {
		if (!list)
			list = {};
		return $.extend(list);
	},

});

