jQuery.extend({
    ContactController: function (model, view) {
        var that = this;
        /**
         * listen to the view
         */
        var vlist = $.ContactViewListener({
            addContact: function (contact) {
                model.addContact(contact);
            },
            updateContact: function (contact) {
                model.updateContact(contact);
            },
            deleteContact: function (idContact) {
                model.deleteContact(idContact);
            }
        });

        view.addListener(vlist);
        /**
         * listen to the model
         */
        var mlist = $.ContactModelListener({
            contactsLoad: function (contacts) {
                view.parseContacts(contacts);
            },
            updateContactSuccess: function () {
                view.updateContactSuccess();
            },
            addContactSuccess: function () {
                view.addContactSuccess();
            },
            deleteContactSuccess: function () {
                view.deleteContactSuccess();
            }
        });

        model.addListener(mlist);

        this.initController = function () {
            view.initView();
            model.initModel();
        };
    }
});
