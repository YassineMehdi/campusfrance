jQuery.extend({

	SEOController: function(model, view){
		//var that = this;
		/**
		 * listen to the view
		 */
		var vlist = $.SEOViewListener({
			loadSEO : function(seo){
				view.displaySEO(seo);
			},
			updateSEOSuccess : function(seo){
				view.updateSEOSuccess(seo);
			},
		});
		view.addListener(vlist);

		/**
		 * listen to the model
		 */
		var mlist = $.SEOModelListener({		
			updateSEO : function(seo){
				model.updateSEO(seo);
			},
		});
		
		model.addListener(mlist);
		
		this.initController = function(){
			view.initView();
			model.initModel();
		};
	}

});