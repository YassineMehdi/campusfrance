
jQuery.extend({
	SEOView: function(){
		var that = this;
        var contentHandler = null;
		var listeners = new Array();
		
		this.initView = function(){
		    traduct();
            contentHandler = $("#mainBackEndContainer");
            
		    $("#seoForm td.jqEasyCounterMsg").css('width', '55%');
			$("#seoContent .draggable").draggable({
				cancel : ".eds-dialog-inside"
			});
			$('#seoContent .closePopup, #seoContent .eds-dialog-close').click(function() {
				hidePopups();
			});
			$("#saveSeoBtn").unbind("click").bind("click", function() {
				if($("#seoForm").valid()){
					$('#loadingEds').show();
					that.updateSEO();
				}
			});
			switch($.cookie('groupId')) {
				case '1':
					$("#posterLink2").closest('.row').hide();
				break;
				case '2':
					$("#posterLink2").closest('.row').hide();
				break;
				case '3':
					$("#posterLink2").closest('.row').hide();
				break;
				case '4':
					$("#posterLink2").closest('.row').hide();
				break;
			}
			if($.cookie('standId') == '13'){
				$("#posterLink2").closest('tr').show();
			}
			that.validateSEOForm();
		};
		this.validateSEOForm = function() {
			/*$("#seoForm").validate({
				errorPlacement : function errorPlacement(error, element) {
					element.after(error);
				},
				rules : {
					seoTitle : {
						required : true
					},
					seoMetadata : {
						required : true
					},
					seoKeywords : {
						required : true
					}
				}
			});*/
		};
		this.displaySEO = function(seo){
			$("#seoTitle").val(seo.getSeoTitle());
			$("#seoMetadata").val(seo.getSeoMetadata());
			$("#seoKeywords").val(seo.getSeoKeywords());
			$("#posterLink1").val(seo.getSeoFirstLink());
			$("#posterLink2").val(seo.getSeoSecondeLink());
			$("#posterLink3").val(seo.getSeoThirdLink());
		};
		this.updateSEO = function(){
			var seo = new $.SEO();
			seo.setSeoTitle($("#seoTitle").val());
			seo.setSeoMetadata($("#seoMetadata").val());
			seo.setSeoKeywords($("#seoKeywords").val());
			seo.setSeoFirstLink($("#posterLink1").val());
			seo.setSeoSecondeLink($("#posterLink2").val());
			seo.setSeoThirdLink($("#posterLink3").val());
			that.notifyUpdateSEO(seo);
		};
		this.updateSEOSuccess = function(seo){
			$('#loadingEds').hide();
			contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
			hidePopups();
		};
		this.notifyUpdateSEO = function(seo){
			$.each(listeners, function(i){
				listeners[i].updateSEO(seo);
			});
		};
		this.addListener = function(list){
			listeners.push(list);
		};
		
	},
	
	SEOViewListener: function(list) {
		if(!list) list = {};
		return $.extend(list);
	}
	
});

