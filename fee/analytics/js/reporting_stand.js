// Copyright 2012 Google Inc. All Rights Reserved.

/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Reference example for the Core Reporting API. This example
 *               demonstrates how to access the important information from
 *               version 3 of the Google Analytics Core Reporting API.
 * @author api.nickm@gmail.com (Nick Mihailovski)
 */

function makeApiCallVisits() {
	// setTimeout(makeApiCallZoneViews, 0);
	setTimeout(makeApiCallVisitsDate, 0);
	setTimeout(makeApiCallVisitorsVSReturnVisitor, 2000);
	setTimeout(makeApiCallVisitorsStand, 4000);
}
function factoryFiltersZoneViewStand(){
	var filters="";
	if(languageIdAnalytics=='0') filters='ga:eventCategory=@' + STANDID + ':' + standId + '/'+LANGUAGEID+';ga:eventCategory=@/' + ZONE + ':';
	else filters='ga:eventCategory=@' + STANDID + ':' + standId + '/'+LANGUAGEID+':'+languageIdAnalytics+ "/" + ZONE + ':';
	return filters;
}
function factoryFiltersMultimediaStand(module){
	return factoryFiltersModuleStand(MULTIMEDIA, module);
}
function factoryFiltersModuleStand(moduleName, moduleValue){
	console.log('standID : '+standId);
	var filters="";
	if(languageIdAnalytics=='0') filters='ga:eventCategory=@' + STANDID + ':' + standId + '/'+LANGUAGEID+';ga:eventCategory=@/' + moduleName + ':' + moduleValue;
	else filters='ga:eventCategory=@' + STANDID + ':' + standId + '/'+LANGUAGEID+':'+languageIdAnalytics+ "/" + moduleName + ':' + moduleValue;
	return filters;
}
function makeApiCallZoneViews(tryCallNbr) {
	var filters=factoryFiltersZoneViewStand();
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
				dimension : 'ga:date',
				metrics : 'ga:totalEvents',
				filters : filters,
				startDate : beginDateAnalytics,
				endDate : endDateAnalytics,
				maxResults : 10000
			},
			function(response) {
				if (response.body != 'No entries found') {
					var rows = jQuery.parseJSON(response.body);
					drawTableReportingZoneViews(rows, ZONEVIEWDATE,
						'string', DATELABEL, 'number', AREAVIEWSLABEL);
				}
				$('#loadingEds').hide();
			}, function(){
				onErrorMakeCallApi(tryCallNbr, makeApiCallZoneViews);
			});
}

function makeApiCallVisitsDate(tryCallNbr) {
	var filters=factoryFiltersZoneViewStand();
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
				dimension : 'ga:date',
				metrics : 'ga:totalEvents',
				filters : filters,
				startDate : beginDateAnalytics,
				endDate : endDateAnalytics,
				maxResults : 10000
			},
			function(response) {
				if (response.body != 'No entries found') {
					var rows = jQuery.parseJSON(response.body);
					drawTableReportingVisitsDate(rows, VISITSSTAND, VISITSSTAND,
							'string', DATELABEL, 'number', VISITSLABEL);
					drawArrayTableReportingVisitsDay(rows, VISITSSTAND, VISITSSTAND,
							'string', DATELABEL, 'number', VISITSLABEL);
				}
				$('#loadingEds').hide();
			}, function(){
				onErrorMakeCallApi(tryCallNbr, makeApiCallVisitsDate);
			});
}

function makeApiCallVisitorsVSReturnVisitor(tryCallNbr) {
	var filters="";
	if(languageIdAnalytics=='0') filters='ga:eventCategory=@' + STANDID + ':' + standId + '/';
	else filters='ga:eventCategory=@' + STANDID + ':' + standId + '/'+LANGUAGEID+':'+languageIdAnalytics+ '/';
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
				dimension : 'ga:visitorType',
				metrics : 'ga:visitors',
				filters : filters,
				startDate : beginDateAnalytics,
				endDate : endDateAnalytics,
				maxResults : 10000
			},
			function(response) {
				if (response.body != 'No entries found') {
					var rows = jQuery.parseJSON(response.body);
					drawChartReportingVisitorsVSReturnVisitor(rows, VISITSSTAND,
						NEWVISITORSVSRETURNINGVISITORS, 'string',
						VISITORTYPELABEL, 'number', VISITORSLABEL);
				}
				$('#loadingEds').hide();
			}, function(){
				onErrorMakeCallApi(tryCallNbr, makeApiCallVisitorsVSReturnVisitor);
			});
}

function makeApiCallVisitorsStand(tryCallNbr) {
	var filters="";
	if(languageIdAnalytics=='0') filters='ga:eventCategory=@' + STANDID + ':' + standId + '/;ga:eventCategory=@/Profile:Candidate';
	else filters='ga:eventCategory=@' + STANDID + ':' + standId + '/'+LANGUAGEID+':'+languageIdAnalytics+ '/Profile:Candidate';
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
				dimension : 'ga:eventAction,ga:eventLabel',
				metrics : 'ga:totalEvents',
				filters : filters,
				startDate : beginDateAnalytics,
				endDate : endDateAnalytics,
				maxResults : 10000
			},
			function(response) {
				if (response.body != 'No entries found') {
					var rows = jQuery.parseJSON(response.body);
					launchReportingProfile(function(){
						drawTableVisitorsStand(rows, VISITSSTAND, BOOTHVISITORS, BOOTHVISITORSLABEL);
					});
				}
				$('#loadingEds').hide();
			}, function(){
				onErrorMakeCallApi(tryCallNbr, makeApiCallVisitorsStand);
			});
}

function makeApiCallVisitsByFunction(tryCallNbr) {
	var filters=factoryFiltersZoneViewStand();
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
				dimension : 'ga:eventCategory',
				metrics : 'ga:totalEvents,ga:uniqueEvents',
				filters : filters,
				startDate : beginDateAnalytics,
				endDate : endDateAnalytics,
				maxResults : 10000
			},
			function(response) {
				if (response.body != 'No entries found') {
					var rows = jQuery.parseJSON(response.body);
					drawTableReportingFunctionalitiesZoneViews(rows, VISITSBYFUNCTION,
						AREAS, 'string', AREALABEL,
						'number', AREAVIEWSLABEL, 'number', UNIQUEAREAVIEWSLABEL,
						'number', AVGTIMEAREAVIEWSLABEL);
				}
				$('#loadingEds').hide();
			}, function(){
				onErrorMakeCallApi(tryCallNbr, makeApiCallVisitsByFunction);
			});
}

function makeApiCallContentStand() {
	//console.log("makeApiCallContentStand, languageIdAnalytics : "+languageIdAnalytics);
	if (listPhotosCache == null || reportingType != STAND || isRefleshLang) {
    	if(photosController==null){
       		//$("body").append("<script type='text/javascript' src='js/photos.js' charset='utf-8'><\/script>");
       		photosController = new $.PhotosController(new $.PhotosModel(), new $.PhotosView());
       	}
    	photosController.getAnalyticsPhotos(makeApiCallPhotosStand, 'allfiles/standPhotos?idLang='+languageIdAnalytics);
	} else
		setTimeout(makeApiCallPhotosStand, 0);
	if (listVideosCache == null || reportingType != STAND || isRefleshLang) {
        if(videosController==null){
	   		//$("body").append("<script type='text/javascript' src='js/videos.js' charset='utf-8'><\/script>");
	           videosController = new $.VideosController(new $.VideosModel(), new $.VideosView());
	   	}
        videosController.getAnalyticsVideos(makeApiCallVideosStand, 'allfiles/standVideos?idLang='+languageIdAnalytics);
	} else
		setTimeout(makeApiCallVideosStand, 1500);
	if (listDocumentsCache == null || reportingType != STAND || isRefleshLang) {
    	if(documentController==null){
    		//$("body").append("<script type='text/javascript' src='js/documents.js' charset='utf-8'><\/script>");
    		documentController = new $.DocumentsController(new $.DocumentsModel(), new $.DocumentsView());
    	}
		documentController.getAnalyticsDocuments(makeApiCallDocumentsStand, 'allfiles/standDocuments?idLang='+languageIdAnalytics);
	} else
		setTimeout(makeApiCallDocumentsStand, 3000);
	if (listPostersCache == null || reportingType != STAND || isRefleshLang) {
       	if(postersController==null){
       		//$("body").append("<script type='text/javascript' src='js/posters.js' charset='utf-8'><\/script>");
       		postersController = new $.PostersController(new $.PostersModel(), new $.PostersView());
		}
		postersController.getAnalyticsPosters(makeApiCallPostersStand, 'allfiles/standPosters?idLang='+languageIdAnalytics);
	} else
		setTimeout(makeApiCallPostersStand, 4500);
	if (listJobOffersCache == null || reportingType != STAND || isRefleshLang) {
		if(jobOfferController == null){
    		//$("body").append("<script type='text/javascript' src='js/jobOffers.js' charset='utf-8'><\/script>");
    		jobOfferController = new $.JobOfferController(new $.JobOfferModel(), new $.JobOfferView());
    	}
		jobOfferController.getAnalyticsJobOffers(makeApiCallJobOffersStand, 'jobOffer/standJobOffers?idLang='+languageIdAnalytics);
	} else
		setTimeout(makeApiCallJobOffersStand, 6000);
	setTimeout(makeApiCallUnsolicitedJobApplications, 6000);
	setTimeout(makeApiCallJobOfferStand, 7500);//7500
	if (listWebsitesCache == null || reportingType != STAND || isRefleshLang) {
		getStandWebsiteByPath(makeApiCallWebsitesStand, 'stand/standWebSites?idLang='+languageIdAnalytics);
	} else setTimeout(makeApiCallWebsitesStand, 7500);
	if (listSurveysCache == null || reportingType != STAND || isRefleshLang) {
		getSurveysByPath(makeApiCallZoneSurveysStand, 'survey/get/stand?idLang='+languageIdAnalytics);
	} else setTimeout(makeApiCallZoneSurveysStand, 9000);
	if (listAdvicesCache == null || reportingType != STAND || isRefleshLang) {
       	if(conseilController==null){
    		//$("body").append("<script type='text/javascript' src='js/advices.js' charset='utf-8'><\/script>");
    		conseilController = new $.ConseilController(new $.ConseilModel(), new $.ConseilView());
		}
       	conseilController.getAnalyticsAdvices(makeApiCallAdvicesStand, 'advice/standAdvices?idLang='+languageIdAnalytics);
	} else
		setTimeout(makeApiCallAdvicesStand, 10500);
	reportingType=STAND;
	isRefleshLang=false;
}
function makeApiCallPhotosStand(tryCallNbr) {
	var filters=factoryFiltersMultimediaStand(PHOTO);
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
				dimension : 'ga:eventCategory',
				metrics : 'ga:totalEvents',
				filters : filters,
				startDate : beginDateAnalytics,
				endDate : endDateAnalytics,
				maxResults : 10000
			},
			function(response) {
				if (response.body != 'No entries found') {
					var rows = jQuery.parseJSON(response.body);
					drawTableReportingMultimedia(rows, CONTENTSTAND, PHOTOS,
							'string', PHOTOSLABEL, 'number', NUMBERVIEWSLABEL, null, null, PHOTO);
				}
				$('#loadingEds').hide();
			}, function(){
				$('#loadingEds').hide();
				onErrorMakeCallApi(tryCallNbr, makeApiCallPhotosStand);
			});
}

function makeApiCallDocumentsStand(tryCallNbr) {
	var filters=factoryFiltersMultimediaStand(DOCUMENT);
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
				dimension : 'ga:eventCategory',
				metrics : 'ga:totalEvents',
				filters : filters,
				startDate : beginDateAnalytics,
				endDate : endDateAnalytics,
				maxResults : 10000
			},
			function(response) {
				if (response.body != 'No entries found') {
					var rows = jQuery.parseJSON(response.body);
					drawTableReportingMultimedia(rows, CONTENTSTAND, DOCUMENTS,
							'string', DOCUMENTSLABEL, 'number', NUMBERVIEWSLABEL,
							null, null, DOCUMENT);
				}
				$('#loadingEds').hide();
			}, function(){
				onErrorMakeCallApi(tryCallNbr, makeApiCallDocumentsStand);
			});
}

function makeApiCallVideosStand(tryCallNbr) {
	var filters=factoryFiltersMultimediaStand(VIDEO);
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
				dimension : 'ga:eventCategory',
				metrics : 'ga:totalEvents',
				filters : filters,
				startDate : beginDateAnalytics,
				endDate : endDateAnalytics,
				maxResults : 10000
			},
			function(response) {
				if (response.body != 'No entries found') {
					var rows = jQuery.parseJSON(response.body);
					drawTableReportingMultimedia(rows, CONTENTSTAND, VIDEOS,
							'string', VIDEOSLABEL, 'number', NUMBERVIEWSLABEL, null, null, VIDEO);
				}
				$('#loadingEds').hide();
			}, function(){
				onErrorMakeCallApi(tryCallNbr, makeApiCallVideosStand);
			});
}

function makeApiCallPostersStand(tryCallNbr) {
	var filters=factoryFiltersMultimediaStand(POSTER);
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
				dimension : 'ga:eventCategory',
				metrics : 'ga:totalEvents',
				filters : filters,
				startDate : beginDateAnalytics,
				endDate : endDateAnalytics,
				maxResults : 10000
			},
			function(response) {
				if (response.body != 'No entries found') {
					var rows = jQuery.parseJSON(response.body);
					drawTableReportingMultimedia(rows, CONTENTSTAND, POSTERS,
							'string', POSTERSLABEL, 'number', NUMBERVIEWSLABEL,
							null, null, POSTER);
				}
				$('#loadingEds').hide();
			}, function(){
				onErrorMakeCallApi(tryCallNbr, makeApiCallPostersStand);
			});
}

function makeApiCallAdvicesStand(tryCallNbr) {
	var filters=factoryFiltersMultimediaStand(ADVICE);
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
				dimension : 'ga:eventCategory',
				metrics : 'ga:totalEvents',
				filters : filters,
				startDate : beginDateAnalytics,
				endDate : endDateAnalytics,
				maxResults : 10000
			},
			function(response) {
				if (response.body != 'No entries found') {
					var rows = jQuery.parseJSON(response.body);
					drawTableReportingMultimedia(rows, CONTENTSTAND, ADVICES,
							'string', ADVICESLABEL, 'number', NUMBERVIEWSLABEL,
							null, null, ADVICE);
				}
				$('#loadingEds').hide();
			}, function(){
				onErrorMakeCallApi(tryCallNbr, makeApiCallAdvicesStand);
			});
}

function makeApiCallJobOffersStand(tryCallNbr) {
	var filters=factoryFiltersMultimediaStand(JOBOFFER);
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
				dimension : 'ga:eventCategory',
				metrics : 'ga:totalEvents',
				filters : filters,
				startDate : beginDateAnalytics,
				endDate : endDateAnalytics,
				maxResults : 10000
			},
			function(response) {
				if (response.body != 'No entries found') {
					var rows = jQuery.parseJSON(response.body);
					drawTableReportingMultimedia(rows, CONTENTSTAND, JOBOFFERS,
							'string', JOBOFFERSLABEL, 'number',
							NUMBERVIEWSLABEL, null, null, JOBOFFER);
				}
				$('#loadingEds').hide();
			}, function(){
				onErrorMakeCallApi(tryCallNbr, makeApiCallJobOffersStand);
			});
}


function makeApiCallJobOfferStand(tryCallNbr) {
		genericAjaxCall('GET', staticVars.urlBackEnd
				+ '/jobOffer/standJobOffersAnalytics?idLang='+languageIdAnalytics, 'application/xml', 'xml', '',
				function(response) {
					if ($(response).text() != '') {
						drawTableReportingJobOffer(response, CONTENTSTAND, JOBOFFERSPOSTULATE,	'string', JOBOFFERSLABEL, 
								'number', CVSSUBMITEDNBRLABEL, null, null);
					}
					$('#loadingEds').hide();
				}, function(){
					onErrorMakeCallApi(tryCallNbr, makeApiCallJobOfferStand);
				});
}
 

function makeApiCallZoneSurveysStand(tryCallNbr) {
	var filters=factoryFiltersMultimediaStand(SURVEY);
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
				dimension : 'ga:eventCategory',
				metrics : 'ga:totalEvents',
				filters : filters,
				startDate : beginDateAnalytics,
				endDate : endDateAnalytics,
				maxResults : 10000
			},
			function(response) {
				if (response.body != 'No entries found') {
					var rows = jQuery.parseJSON(response.body);
					drawTableReportingSurveys(rows, CONTENTSTAND, SURVEYS, 'string',
							SURVEYLABEL, 'number', NUMBERVIEWSLABEL, null, null, SURVEY);
				}
				$('#loadingEds').hide();
			}, function(){
				onErrorMakeCallApi(tryCallNbr, makeApiCallZoneSurveysStand);
			});
}

function makeApiCallWebsitesStand(tryCallNbr) {
	var filters=factoryFiltersModuleStand(WEBSITE, "");
	//console.log(" makeApiCallWebsitesStand : "+filters);
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
				dimension : 'ga:eventCategory',
				metrics : 'ga:totalEvents',
				filters : filters,
				startDate : beginDateAnalytics,
				endDate : endDateAnalytics,
				maxResults : 10000
			},
			function(response) {
				if (response.body != 'No entries found') {
					var rows = jQuery.parseJSON(response.body);
					drawTableReportingWebsites(rows, CONTENTSTAND, WEBSITES, 'string',
							WEBSITESLABEL, 'string', TYPE, 'number', NUMBERVIEWSLABEL, null, null, WEBSITE);
				}
				$('#loadingEds').hide();
			}, function(){
				onErrorMakeCallApi(tryCallNbr, makeApiCallWebsitesStand);
			});
}

function makeApiCallUnsolicitedJobApplications(tryCallNbr) {
	genericAjaxCall('GET', staticVars.urlBackEnd
			+ 'jobApplication/allUnsolicitedJobApplications/', 'application/xml', 'xml', '',
			function(response) {
				if ($(response).text() != '') {
					launchReportingProfile(function(){
						drawTableReportingUnsolicitedJobApplications(response, CONTENTSTAND,
								UNSOLICITEDJOBAPPLICATION, CVSUBMITTEDLABEL, 'string', USERLABEL,
								'string', DATEOFAPPLICATIONLABEL);
					});
				}
				$('#loadingEds').hide();
			}, function(){
				onErrorMakeCallApi(tryCallNbr, makeApiCallUnsolicitedJobApplications);
			});
}

function makeApiCallCandidatesProfilStand(tryCallNbr) {
	if(languageIdAnalytics=='0') filters='ga:eventCategory=@' + STANDID + ':' + standId+';ga:eventCategory=@/Profile:Candidate';
	else filters='ga:eventCategory=='+ STANDID + ':' + standId+'/'+LANGUAGEID+':'+languageIdAnalytics+'/Profile:Candidate';
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
				dimension : 'ga:eventAction',
				metrics : 'ga:totalEvents',
				filters : filters,
				startDate : beginDateAnalytics,
				endDate : endDateAnalytics,
				maxResults : 10000
			},
			function(response) {
				if (response.body == 'No entries found') {
					$('#loadingEds').hide();
				}else {
					var rows = jQuery.parseJSON(response.body);
					candidateProfileList = new Array();
					for ( var i = 0, row; row = rows[i]; ++i) {
						candidateProfileList.push(row[0].split('/')[0]);
					}
	
					launchReportingProfile(drawTablesStand);
				}
			}, function(){
				onErrorMakeCallApi(tryCallNbr, makeApiCallCandidatesProfileStand);
			});
}

function makeApiCallProfilStand() {
	//console.log('makeApiProfileStand');
	makeApiCallCandidatesProfilStand();
	//console.log('end');
}

