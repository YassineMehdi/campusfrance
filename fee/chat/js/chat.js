
$(document).ready(function () {
    initHistory();
});

function initLanguageComboBoxHistory(languageHistoryChatHandler){
	languageHistoryChatHandler.empty();
	languageHistoryChatHandler.append($('<option/>').val(ALL).html(ALLLABEL));
	for (var languageId in listLangsEnum) {
		languageHistoryChatHandler.append($('<option/>').val(languageId).html(getLanguageName(languageId)));
	}
}

function parseChatPublicHistoryListXml(xml) {
    $(xml).find("chatPublicHistory").each(function () {
    	var historicChat=new Object();
    	historicChat.id=$(this).find('chatPublicHistoryId').text();
    	historicChat.title=$(this).find('eventTitle').text();
    	historicChat.date=$(this).find('date').text();
    	historicChat.path=$(this).find('path').text();
    	historicChat.languageId=$(this).find('language > idlangage').text();
    	historicChat.type=PUBLIC;
    	listHistoricChatCache.push(historicChat);
    });
    refreshHistoryChat();
}
function parseChatPrivateHistoryListXml(xml) {
    $(xml).find("chatPrivateHistory").each(function () {
    	var historicChat=new Object();
    	historicChat.id=$(this).find('chatPrivateHistoryId').text();
    	historicChat.title=$(this).find('userProfileDTO2').find('prenom').text()+" "+$(this).find('userProfileDTO2').find('nom').text();
    	historicChat.date=$(this).find('date').text();
    	historicChat.path=$(this).find('path').text();
    	historicChat.type=PRIVATE;
    	listHistoricChatCache.push(historicChat);
    });
    refreshHistoryChat();
}
function getHistoryChat() {
	  var dateBeginHistoryChatValue = dateBeginHistoryChatHandler.val();
    var dateEndHistoryChatValue = dateEndHistoryChatHandler.val();
    
    $("#historyListTable").empty();
    listHistoricChatCache = new Array();
	genericAjaxCall('POST', staticVars["urlBackEnd"]+"chatpublichistory/listchat?dateBegin="+dateBeginHistoryChatValue+'&dateEnd='+dateEndHistoryChatValue,
    		'application/xml', 'xml', '', parseChatPublicHistoryListXml, getHistoryError);
	genericAjaxCall('POST', staticVars["urlBackEnd"]+"chatprivatehistory/listchat?dateBegin="+dateBeginHistoryChatValue+'&dateEnd='+dateEndHistoryChatValue,
    		'application/xml', 'xml', '', parseChatPrivateHistoryListXml, getHistoryError);
}
function refreshHistoryChat() {
	console.log('refreshHistoryChat');
    $("#historyListTable").empty();
	var dateFrom = dateBeginHistoryChatHandler.datepicker('getDate');
	if(dateFrom!=null) dateFrom.setHours(0, 0, 0, 0);
    var dateTo = dateEndHistoryChatHandler.datepicker('getDate');
    if(dateTo!=null) dateTo.setHours(23, 59, 59, 0);
    var typeHistoryChatValue=typeHistoryChatHandler.val();
    //var languageHistoryChatValue=languageHistoryChatHandler.val();
    var languageHistoryChatValue=ALL;
    var historicChatDate=null;
	$(listHistoricChatCache)
	.each(
			function(index, historicChat) {
				historicChatDate=new Date(historicChat.date);
				if(dateFrom <= historicChatDate && historicChatDate <= dateTo ){
					if((languageHistoryChatValue==ALL || languageHistoryChatValue==historicChat.languageId || historicChat.type==PRIVATE) && (typeHistoryChatValue==ALL || typeHistoryChatValue==historicChat.type)) {
						var typeLabel="";
						if(historicChat.type==PRIVATE) typeLabel=PRIVATECHATLABEL;
						else if(historicChat.type==PUBLIC) typeLabel=PUBLICCHATLABEL;
						addHistoryChat(historicChat.type+historicChat.id, typeLabel, historicChat.title, historicChat.date, historicChat.path, historicChat.languageId);
					}
				}
    });
}
function addHistoryChat(id, type, title, date, path, language){
	language=getLanguageName(language);
	date=new Date(date);
	date=date.toLocaleDateString(LANGAUGETAGLOCALELABEL.replace('_','-'));
	$("#historyListTable").append("<tr height='10'/>"
    		+"<tr>"
    			+"<td class='form-label' style='text-align: left; vertical-align: middle;width: 20%;'>"
                	+"<span style='color:black;'>"+type+"</span>"
                +"</td>"
                +"<td class='form-label' style='text-align: left; vertical-align: middle;width: 30%;''>"
                	+"<span style='color:black;'>"+title+"</span>"
                 +"<td class='form-label' style='text-align: left; vertical-align: middle;width: 35%;'>"
               		+"<span style='color:black;'>"+date+"</span>"
                 +"</td>"
                 /*+"<td class='form-label' style='text-align: left; vertical-align: middle;width: 35%;'>"
            		+"<span style='color:black;'>"+language+"</span>"
            	 +"</td>"*/
               	 +"<td class='form-label' style='text-align: center; vertical-align: middle;width: 15%;'>"
               	 	+"<a href='#' class='blueLink' style='color: #E9752A !important; ' src='#' id='openFile"+id+"'> "+OPENLABEL+" </a>"
                 +"</td>"
            +"</tr>");
	$("#openFile"+id).click(function (){
		window.open(
				'chathistory.html?path='+$.base64.encode(path),
				'_blank');
	});
}

function getLanguageName(languageId){
	if(languageId!="" && languageId!=null && listLangsAbrEnum[languageId] != null) return listLangsAbrEnum[languageId];
	else return "";
}

function getCurrentsChat(){
	genericAjaxCall('POST', staticVars["urlBackEnd"]+'agenda?forChat=true&idLang='+getIdLangParam(),
    		'application/json', 'json', '', getCurrentsChatSuccess, getCurrentsChatError);
}
function getCurrentsChatSuccess(json) {
    //$("#loading").hide();
	chatComponentHanlder=$('#chatComponent');
	chatComponentHanlder.empty();
	chatComponentHanlder.append('<div id="currentChatsPopup" class="col-xs-3"><select class="form-control" id="currentChatsSelect"><option value="0">'+PUBLICCHATLABEL+'</option></select></div><button id="currentChatsButton" class="btn btn-info sendLabel">'+SENDLABEL+'</button>'); 
	//json=JSON.parse(json);
	$('#chatComponent #currentChatsButton').bind('click', function () {
		connectToChatFlash($('#chatComponent #currentChatsSelect').val());
	});
	$.each(json, function(index, eventInfo){
		$('#chatComponent #currentChatsSelect').append('<option value="'+eventInfo.id+'">'+eventInfo.ack+'</option>');
	});
};
function getCurrentsChatError(xhr,status,error) {
    isSessionExpired(xhr.responseText);
};
function getHistoryError(xhr,status,error) {
    //$("#loading").hide();
    isSessionExpired(xhr.responseText);
};

