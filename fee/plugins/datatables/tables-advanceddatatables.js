$(document).ready(function () {

    // Wijets
    $.wijets().make();

    var adviceList = [];
    var listActions = "<a href='javascript:void(0);' class='editAdviceLink actionBtn'><i class='fa fa-pencil-square-o'></i></a> <a href='javascript:void(0);' class='viewAdviceLink actionBtn'><i class='fa fa-eye'></i></a> <a href='javascript:void(0);' class='deleteAdviceLink actionBtn'><i class='fa fa-trash-o'></i></a>";
    var data = ["AAAAAA", "BBBBBBB", listActions];
    adviceList.push(data);

    var oTable = $('#listAdvicesTable').dataTable({
        "language": {
            "lengthMenu": "_MENU_"
        },
        "aaData": adviceList,
        "aaSorting": [],
        "aoColumns": [
            {
                "sTitle": TITLELABEL
            },
            {
                "sTitle": CONTENTLABEL
            },
            {
                "sTitle": ACTIONSLABEL
            }
        ],
        "bAutoWidth": false,
        "bRetrieve": false,
        "bDestroy": true,
        "iDisplayLength": 10
    });

    var tableTools = new $.fn.dataTable.TableTools(oTable, {
        "buttons": [
            //"copy",
            //"csv",
            //"xls",
            //"pdf",
            {"type": "print", "buttonText": "Print me!"}
        ],
        "sSwfPath": "assets/plugins/datatables/TableTools/swf/copy_csv_xls_pdf.swf"
    });



    //DOM Manipulation to move datatable elements integrate to panel
    $('#panel-tabletools .panel-ctrls').append($('.dataTables_filter').addClass("pull-right"));
    $('#panel-tabletools .panel-ctrls .dataTables_filter').find("label").addClass("panel-ctrls-center");

    $('#panel-tabletools .panel-ctrls').append("<i class='separator pull-right '></i>");

    $('#panel-tabletools .panel-ctrls').append($('.dataTables_length').addClass("pull-right"));
    $('#panel-tabletools .panel-ctrls .dataTables_length label').addClass("mb0");

    $('#panel-tabletools .panel-ctrls').append("<i class='separator pull-right '></i>");
    $(tableTools.fnContainer()).appendTo('#panel-tabletools .panel-ctrls').addClass("pull-right mt10");

    $('#panel-tabletools .panel-footer').append($(".dataTable+.row"));
    $('.dataTables_paginate>ul.pagination').addClass("pull-right");


});