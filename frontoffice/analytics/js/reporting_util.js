/**
 * Utility method to return the lastNdays from today in the format yyyy-MM-dd.
 * 
 * @param {Number}
 *            n The number of days in the past from tpday that we should return
 *            a date. Value of 0 returns today.
 */
function factoryShowComponent(menuName, menuSelector){
	if(menuName==DEMOGRAPHICSDATA)  demographicsDataIsRun=true;
	else if(menuName==OVERVIEW)  overviewIsRun=true;
	else if(menuName==TRAFFICSOURCES)  trafficSourcesIsRun=true;
	else if(menuName==CONTENTFORUM)  contentForumIsRun=true;
	else if(menuName==CONTENTSTAND)  contentStandIsRun=true;
	else if(menuName==PROFILFORUM)  profilForumIsRun=true;
	else if(menuName==PROFILSTAND)  profilStandIsRun=true;
	else if(menuName==VISITSSTAND)  visitsStandIsRun=true;
	else if(menuName==VISITSBYFUNCTION)  visitsByFunctionIsRun=true;
	$('#loadingEds').show();
	$(menuSelector).empty();
	$(menuSelector).show();
	beginDateAnalytics = getAnalyticStringDate(beginDateAnalyticsHandler.val());
	endDateAnalytics = getAnalyticStringDate(endDateAnalyticsHandler.val());
}
function openElementMenu(idElement){
	$('#'+idElement).text(DELETE); 
	$('#'+idElement).attr("Title",REDUIRE);
}
function toggleElementMenu(idElement){
	if($('#'+idElement).text()==ADD) {
		$('#'+idElement).text(DELETE);
		$('#'+idElement).attr("title",REDUIRE); 
	}else {
		$('#'+idElement).text(ADD); 
		$('#'+idElement).attr("title",AFFICHER); 
	}
}
function launchReportingProfile(callBack) {
	//console.log('launchReportingProfile : '+ getCandidateProfileList);
	if(getCandidateProfileList==false) {
		genericAjaxCall('GET', urlServerManager + '/candidate/analyticsInfosForReporting?idLang='+getIdLangParam(), 'application/xml', 'xml', '',
				function(candidatesDTOList) {
					candidatesListXml=candidatesDTOList;
					$('#loadingEds').hide();
					if ($(candidatesListXml).text() != null
							&& $(candidatesListXml).text() != ''){
						parseXmlCandidates(candidatesListXml);
						getCandidateProfileList=true;
						callBack();//drawTablesForum, drawTablesStand
					}
				}, function(){
					$('#loadingEds').hide();
					setTimeout(launchReportingProfile, 2000, callBack);
				});
	}else {
		parseXmlCandidates(candidatesListXml);
		callBack();
	}
};
function getArrayListProfileType(that, arrayListProfile, selector){
	var value = $(that).find(selector).text();
	if (arrayListProfile[value] != null)
		arrayListProfile[value]++;
	else
		arrayListProfile[value] = 1;
	return arrayListProfile;
}

function parseXmlCandidates(candidatesListXml) {
	console.log('parseXmlCandidates');
	areaExpertiseList = new Array();
	countryOriginList = new Array();
	experienceYearsList = new Array();
	functionCandidateList = new Array();
	jobSoughtList = new Array();
	regionList = new Array();
	sectorActList = new Array();
	fullNameList = new Array();
	candidateIdList = new Array();
	candidateLanguageList = new Array();
	candidateLanguageLevelList = new Array();
	$(candidatesListXml).find("candidateDTO")
			.each(
					function() {
						fullNameList[$(this).find('email').text()] = $(this).find('firstName').text()+" "+$(this).find('secondName').text();
						candidateIdList[$(this).find('email').text()] = $(this).find('candidateId').text();
						
						if (candidateProfileList!=null && candidateProfileList.indexOf($(this).find('email').text()) != -1) {
							areaExpertiseList = getArrayListProfileType(this, areaExpertiseList, 'areaExpertiseDTO areaExpertiseName');
							countryOriginList = getArrayListProfileType(this, countryOriginList, 'countryOriginDTO regionName');
							experienceYearsList = getArrayListProfileType(this, experienceYearsList, 'experienceYearsDTO experienceYearsName');
							functionCandidateList = getArrayListProfileType(this, functionCandidateList, 'functionCandidateDTO functionName');
							jobSoughtList = getArrayListProfileType(this, jobSoughtList, 'jobSoughtDTO jobSoughtName');

							$(this).find('regionDTOlist regionDTO').each(function() {
								regionList = getArrayListProfileType(this, regionList, 'regionName');
							});
							$(this).find('secteurActDTOlist secteurActDTO').each(function() {
								sectorActList = getArrayListProfileType(this, sectorActList, 'name');
							});

							$(this).find('candidateLanguagesDTOList candidateLanguages').each(function() {
								var languageUserProfileName=$(this).find('languageUserProfile languageUserProfileName').text();
								if(languageUserProfileName!=null && languageUserProfileName!=""){
									var languageLevelName=$(this).find("languageLevel languageLevelName").text();
									
									if (candidateLanguageList[languageUserProfileName] != null)
										candidateLanguageList[languageUserProfileName]++;
									else
										candidateLanguageList[languageUserProfileName] = 1;
									var languageLevelList=candidateLanguageLevelList[languageUserProfileName];
									if(languageLevelList==null) {
										languageLevelList = new Array();
										candidateLanguageLevelList[languageUserProfileName]=languageLevelList;
									}
									if(languageLevelName!=null && languageLevelName!=""){
										if (languageLevelList[languageLevelName] != null)
											languageLevelList[languageLevelName]++;
										else
											languageLevelList[languageLevelName] = 1;
									}
								}
							});
						}
					});
}
function getTranslateValue(value) {
		if (value == 'Advices') {
			return ADVICESLABEL;
		}else if (value == 'JobOffers') {
			return JOBOFFERSLABEL;
		}else if (value == 'Posters') {
			return POSTERSLABEL;
		}else if (value == 'PostulateCV') {
			return SUBMITCVLABEL;
		}else if (value == 'Postulate') {
			return APPLYLABEL;
		}else if (value == 'SendMessage') {
			return SENDMESSAGELABEL;
		}else if (value == 'Surveys') {
			return SURVEYSLABEL;
		}else if (value == 'Testimonies') {
			return TESTIMONIESLABEL;
		}else if (value == 'Websites') {
			return WEBSITESLABEL;
		}else if (value == 'Advices') {
			return ADVICESLABEL;
		}
		return value;
}
function getValueFromList(objectId, type) {
	if(objectId!=undefined && objectId!=null){
		if (type == PHOTO) {
			var photo = photosController.getPhotoById(objectId);
			if (photo != null) {
				return photo.getPhotoTitle();
			} else
				return null;
		} else if (type == VIDEO) {
			var video = videosController.getVideoById(objectId);
			if (video != null) {
				return video.getVideoTitle();
			} else
				return null;
		} else if (type == POSTER) {
			var poster = postersController.getPosterById(objectId);
			if (poster != null) {
				return poster.getPosterTitle();
			} else
				return null;
		} else if (type == DOCUMENT) {
			var document = documentController.getDocumentById(objectId);
			if (document != null) {
				return document.getDocumentTitle();
			} else
				return null;
		} else if (type == JOBOFFER) {
			var jobObffer =  jobOfferController.getJobOfferById(objectId);
			if (jobObffer != null) {
				return jobObffer.getJobTitle();
			} else
				return null;
		} else if (type == SURVEY) {
			var xml = listSurveysCache[objectId];
			if (xml != null) {
				return $(xml).find("formTitle").text();
			} else
				return null;
		}else if (type == STAND) {
			var xml = listStandsCache[objectId];
			if (xml != null) {
				return $(xml).find("> name:first").text();
			} else
				return null;
		}else if (type == WEBSITE) {
			var website = listWebsitesCache[objectId];
			if (website != null) {
				return website.getUrl();
			} else
				return null;
		}else if (type == ADVICE) {
			var advice = listAdvicesCache[objectId];
			if (advice != null) {
				return advice.getTitle();
			} else
				return null;
		}
	}return null;
}

function refreshAnalytics() {
	demographicsDataIsRun = false;
	overviewIsRun=false;
	trafficSourcesIsRun = false;
	contentForumIsRun = false;
	contentStandIsRun = false;
	profilForumIsRun = false;
	profilStandIsRun = false;
	visitsStandIsRun = false;
	visitsByFunctionIsRun = false;
}

function getSelectionValue(selection, data) {
	var value = '';
	for ( var i = 0; i < selection.length; i++) {
		var item = selection[i];
		if (item.row != null && item.column != null) {
			value = data.getFormattedValue(item.row, item.column);
		} else if (item.row != null) {
			value = data.getFormattedValue(item.row, 0);
		} else if (item.column != null) {
			value = data.getFormattedValue(0, item.column);
		}
	}

	return value;
}

function analyticsDateToString(stringDate) {
	return [ stringDate.substring(0, 4), stringDate.substring(4, 6),
			stringDate.substring(6, 8) ].join('-');
}

function getStringDate(n) {
	var arrayDate = beginDateAnalytics.split('-');
	var date = new Date();
	date.setFullYear(arrayDate[0], arrayDate[1], arrayDate[2]);
	if (n >= date.getDate())
		date.setDate(n);
	else {
		date.setMonth(date.getMonth() + 1, n);
	}

	var year = date.getFullYear();

	var month = date.getMonth();
	if (month < 10) {
		month = '0' + month;
	}

	var day = date.getDate();
	if (day < 10) {
		day = '0' + day;
	}

	return [ year, month, day ].join('-');
}

function getAnalyticStringDate(date) {

	var arrayDate = date.split('/');

	var year = arrayDate[2];

	var month = arrayDate[1];
	/*
	 * if (month < 10) { month = '0' + month; }
	 */

	var day = arrayDate[0];
	/*
	 * if (day < 10) { day = '0' + day; }
	 */

	return [ year, month, day ].join('-');
}

function htmlEncode(value) {
	// create a in-memory div, set it's inner text(which jQuery automatically
	// encodes)
	// then grab the encoded contents back out. The div never exists on the
	// page.
	return $('<div/>').text(value).html();
}

function htmlDecode(value) {
	return $('<div/>').html(value).text();
}

function capitalize(s) {
	return s[0].toUpperCase() + s.slice(1);
}

function createMenuModuleReportingDiv(menuName, moduleName){
	var container=$('<div id="'+moduleName+'Reporting"></div>');
	$('#'+menuName+'Menu').append(container);
}

function createDemographicsReportingDiv(moduleName, moduleLabel){
	if(document.getElementById(moduleName+'Reporting')==null){
		createMenuModuleReportingDiv("demographicsData", moduleName);
		var container=$('<div class="row">'
							+'<div class="col-md-6">'
								+'<div class="panel panel-default">'
									+'<div class="panel-heading">'
										+'<h2>'+moduleLabel+'</h2>'
									+'</div>'
									+'<div class="panel-body">'
										+'<div id="table_div_'+moduleName+'"></div>'
									+'</div>'
								+'</div>'
							+'</div>'
							+'<div class="col-md-6">'
								+'<div class="panel panel-default">'
									+'<div class="panel-heading">'
										+'<h2>'+moduleLabel+' Chart</h2>'
									+'</div>'
									+'<div class="panel-body">'
										+'<div id="chart_div_'+moduleName+'"></div>'
									+'</div>'
								+'</div>'
							+'</div>'
						+'</div>'
						+'<br/>'
						+'<br/>');
		$('#'+moduleName+'Reporting').append(container);
	}
}

function createOverviewReportingDiv(moduleName,moduleLabel){
	if(document.getElementById(moduleName+'Reporting')==null){
		createMenuModuleReportingDiv('overview', moduleName);
		var container=$('<div class="row">'
							+'<div class="col-md-6">'
								+'<div class="panel panel-default">'
									+'<div class="panel-heading">'
										+'<h2>'+moduleLabel+'</h2>'
									+'</div>'
									+'<div class="panel-body">'
										+'<div id="table_div_'+moduleName+'Day"></div>'
									+'</div>'
								+'</div>'
							+'</div>'
							+'<div class="col-md-6">'
								+'<div class="panel panel-default">'
									+'<div class="panel-heading">'
										+'<h2>'+moduleLabel+' Chart</h2>'
									+'</div>'
									+'<div class="panel-body">'
										+'<div id="lineChart_div_'+moduleName+'Hour"></div>'
									+'</div>'
								+'</div>'
							+'</div>'
						+'</div>'
						+'<br/>'
						+'<br/>');

		$('#'+moduleName+'Reporting').append(container);
	}
}
function createTrafficSourcesReportingDiv(moduleName, moduleLabel){
	if(document.getElementById(moduleName+'Reporting')==null){
		createMenuModuleReportingDiv('trafficSources', moduleName);
		var container=$('<div class="row">'
							+'<div class="col-md-7">'
								+'<div class="panel panel-default">'
									+'<div class="panel-heading">'
										+'<h2>'+moduleLabel+'</h2>'
									+'</div>'
									+'<div class="panel-body">'
										+'<div id="table_div_'+moduleName+'"></div>'
									+'</div>'
								+'</div>'
							+'</div>'
						+'</div>'
						+'<br/>'
						+'<br/>');

		$('#'+moduleName+'Reporting').append(container);
	}
}
function createMultimediaReportingDiv(menuName, moduleName, moduleLabel){
	//console.log('createMultimediaReportingDiv menuName : '+menuName+', moduleName : '+moduleName);
	if(document.getElementById(menuName+'_'+moduleName+'Reporting')==null){
		createMenuModuleReportingDiv(menuName, menuName+'_'+moduleName);
		var container=$('<div class="row">'
							+'<div class="col-md-6">'
								+'<div class="panel panel-default">'
									+'<div class="panel-heading">'
										+'<h2>'+moduleLabel+'</h2>'
									+'</div>'
									+'<div class="panel-body">'
										+'<div id="table_div_'+menuName+'_'+moduleName+'"></div>'
									+'</div>'
								+'</div>'
							+'</div>'
						+'</div>'
						+'<br/>'
						+'<br/>');
		$('#'+menuName+'_'+moduleName+'Reporting').append(container);
	}
}
function createProfileReportingDiv(menuName, moduleName, moduleLabel, parent){
	//console.log('createProfileReportingDiv : moduleName '+moduleName);
	if(document.getElementById(menuName+"_"+moduleName+'Reporting')==null){
		if(parent==null) createMenuModuleReportingDiv(menuName, menuName+'_'+moduleName);
		var container=$('<div class="row">'
							+'<div class="col-md-6">'
								+'<div class="panel panel-default">'
									+'<div class="panel-heading">'
										+'<h2>'+htmlEncode(formattingLongWords(moduleLabel, 15))+'</h2>'
									+'</div>'
									+'<div class="panel-body">'
										+'<div id="table_div_'+menuName+'_'+moduleName+'"></div>'
									+'</div>'
								+'</div>'
							+'</div>'
							+'<div class="col-md-6">'
								+'<div class="panel panel-default">'
									+'<div class="panel-heading">'
										+'<h2>'+htmlEncode(formattingLongWords(moduleLabel, 15))+' Chart</h2>'
									+'</div>'
									+'<div class="panel-body">'
										+'<div id="pieChart_div_'+menuName+'_'+moduleName+'"></div>'
									+'</div>'
								+'</div>'
							+'</div>'
						+'</div>'
						+'<br/>'
						+'<br/>');
		if(parent==null) $('#'+menuName+"_"+moduleName+'Reporting').append(container);
		else $(container).insertAfter(parent);
		
	}
}

function createModuleStandReportingDiv(menuName, moduleName, moduleLabel){
	if(document.getElementById(menuName+'_'+moduleName+'Reporting')==null){
		createMenuModuleReportingDiv(menuName, menuName+'_'+moduleName);
		var container=$('<div class="row">'
							+'<div class="col-md-6">'
								+'<div class="panel panel-default">'
									+'<div class="panel-heading">'
										+'<h2>'+moduleLabel+'</h2>'
									+'</div>'
									+'<div class="panel-body">'
										+'<div id="table_div_'+menuName+'_'+moduleName+'"></div>'
									+'</div>'
								+'</div>'
							+'</div>'
						+'</div>'
						+'<br/>'
						+'<br/>');
		$('#'+menuName+'_'+moduleName+'Reporting').append(container);
	}
}
function createNewVisitorsVSReturningVisitorsReportingDiv(menuName, moduleName, moduleLabel){
	if(document.getElementById(menuName+'_'+moduleName+'Reporting')==null){
		createMenuModuleReportingDiv(menuName, menuName+'_'+moduleName);
		var container=$('<div class="row">'
							+'<div class="col-md-6">'
								+'<div class="panel panel-default">'
									+'<div class="panel-heading">'
										+'<h2>'+moduleLabel+'</h2>'
									+'</div>'
									+'<div class="panel-body">'
										+'<div id="pieChart_div_'+menuName+'_'+moduleName+'"></div>'
									+'</div>'
								+'</div>'
							+'</div>'
						+'</div>'
						+'<br/>'
						+'<br/>');
		$('#'+menuName+'_'+moduleName+'Reporting').append(container);
	}
}
function createVisitsStandReportingDiv(menuName, moduleName, moduleLabel){
	if(document.getElementById(menuName+'_'+moduleName+'Reporting')==null){
		createMenuModuleReportingDiv(menuName, menuName+'_'+moduleName);
		var container=$('<li class="'+moduleName+'Label">'+moduleLabel+'</li>'
				+'<table cellspacing="0">'
				+'<tbody>'
				+'	<td>'
				+'		<div id="table_div_'+menuName+'_'+moduleName+'Date"></div>'
				+'	</td>'
				+'	<td>'
				+'		<div id="lineChart_div_'+menuName+'_'+moduleName+'Day"></div>'
				+'	</td>'
				+'</tbody>'
				+'</table>'
				+'<br/>'
				+'<br/>');
		var container=$('<div class="row">'
							+'<div class="col-md-6">'
								+'<div class="panel panel-default">'
									+'<div class="panel-heading">'
										+'<h2>'+moduleLabel+'</h2>'
									+'</div>'
									+'<div class="panel-body">'
										+'<div id="table_div_'+menuName+'_'+moduleName+'Date"></div>'
									+'</div>'
								+'</div>'
							+'</div>'
							+'<div class="col-md-6">'
								+'<div class="panel panel-default">'
									+'<div class="panel-heading">'
										+'<h2>'+moduleLabel+' Chart</h2>'
									+'</div>'
									+'<div class="panel-body">'
										+'<div id="lineChart_div_'+menuName+'_'+moduleName+'Day"></div>'
									+'</div>'
								+'</div>'
							+'</div>'
						+'</div>'
						+'<br/>'
						+'<br/>');
		$('#'+menuName+'_'+moduleName+'Reporting').append(container);
	}
}
function initForumAnalyticsView(){
	var forumReportingBodySelector="#forumReportingTab .reportingBody";
	createMenuReporting(forumReportingBodySelector, DEMOGRAPHICSDATA, DEMOGRAPHICSDATALABEL, makeApiCallDemographics);
	createMenuReporting(forumReportingBodySelector, OVERVIEW, OVERVIEWLABEL, makeApiCallOverview);
	createMenuReporting(forumReportingBodySelector, TRAFFICSOURCES, TRAFFICSOURCESLABEL, makeApiCallTrafficSources);
	createMenuReporting(forumReportingBodySelector, CONTENTFORUM, CONTENTLABEL, beforeMakeApiCallContentForum);
	createMenuReporting(forumReportingBodySelector, PROFILFORUM, PROFILLABEL, makeApiCallProfilForum);
}
function initStandAnalyticsView(){
	var standReportingBodySelector="#standReportingTab .reportingBody";
	createMenuReporting(standReportingBodySelector, VISITSSTAND, VISITSLABEL, makeApiCallVisits);
	createMenuReporting(standReportingBodySelector, VISITSBYFUNCTION, VISITSBYFUNCTIONLABEL, makeApiCallVisitsByFunction);
	createMenuReporting(standReportingBodySelector, CONTENTSTAND, CONTENTLABEL, makeApiCallContentStand);
	createMenuReporting(standReportingBodySelector, PROFILSTAND, PROFILLABEL, makeApiCallProfilStand);	
}

function createMenuReporting(parentSelector, menuName, menuLabel, callBack){
	var container=$('<h2 id="'+menuName+'Title"><span class="'+menuName+'Label">'+menuLabel+'</span> <span id="'+menuName+'Add">+</span></h2>'
			+'<ol id="'+menuName+'Menu" class="reportingMenu"></ol>');
	$(parentSelector).append(container);
	addListenerMenu(menuName, callBack);
}
function addListenerMenu(menuName, callBack){
	$('#'+menuName+'Title').bind('click',function(){
			var menuSelector='#'+menuName+'Menu';
			var menuNameAdd=menuName+'Add';
			if(!factoryIsRun(menuName)) {
				  factoryShowComponent(menuName, menuSelector);
				  callBack();
				  openElementMenu(menuNameAdd);
			}else {
				  $(menuSelector).toggle();
				  toggleElementMenu(menuNameAdd);
			}
	  });
}

function onErrorMakeCallApi(tryCallNbr, callBack, params){
	$('#loadingEds').hide();
	if(tryCallNbr == undefined || tryCallNbr == null) tryCallNbr=0;
	tryCallNbr++;
	if(tryCallNbr < MAXTRYCALLNBR) setTimeout(callBack, 2000, tryCallNbr, params);
}

function factoryIsRun(menuName){
	var isRun=false;
	if(menuName==DEMOGRAPHICSDATA)  isRun=demographicsDataIsRun;
	else if(menuName==OVERVIEW)  isRun=overviewIsRun;
	else if(menuName==TRAFFICSOURCES)  isRun=trafficSourcesIsRun;
	else if(menuName==CONTENTFORUM)  isRun=contentForumIsRun;
	else if(menuName==CONTENTSTAND)  isRun=contentStandIsRun;
	else if(menuName==PROFILFORUM)  isRun=profilForumIsRun;
	else if(menuName==PROFILSTAND)  isRun=profilStandIsRun;
	else if(menuName==VISITSSTAND)  isRun=visitsStandIsRun;
	else if(menuName==VISITSBYFUNCTION)  isRun=visitsByFunctionIsRun;
	return isRun;
}
