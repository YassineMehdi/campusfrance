// Copyright 2012 Google Inc. All Rights Reserved.

/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Reference example for the Core Reporting API. This example
 *               demonstrates how to access the important information from
 *               version 3 of the Google Analytics Core Reporting API.
 * @author api.nickm@gmail.com (Nick Mihailovski)
 */

// Load the Visualization API and the piechart package.
/**
 * Executes a Core Reporting API query to retrieve the top 25 organic search
 * terms. Once complete, handleCoreReportingResults is executed. Note: A user
 * must have gone through the Google APIs authorization routine and the Google
 * Anaytics client library must be loaded before this function is called.
 */

function factoryFiltersMultimediaForum(module){
	return factoryFiltersModuleForum(MULTIMEDIA, module);
}
function factoryFiltersModuleForum(moduleName, moduleValue){
	var filters="";
	if(languageIdAnalytics=='0') filters='ga:eventCategory=@/' + moduleName + ':' + moduleValue;
	else filters='ga:eventCategory=@/'+LANGUAGEID+':'+languageIdAnalytics+ "/" + moduleName + ':' + moduleValue;
	return filters;
}
function makeApiCallDemographics() {
	//console.log("makeApiCallDemographics");
	setTimeout(makeApiCallLanguage, 0);
	setTimeout(makeApiCallCountry, 2000);
	setTimeout(makeApiCallCity, 4000);
	setTimeout(makeApiCallBrowser, 6000);
	setTimeout(makeApiCallOperatingSystem, 8000);
}

function makeApiCallLanguage(tryCallNbr) {
	//console.log("makeApiCallLanguage");
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
				dimension : 'ga:language',
				metrics : 'ga:visits',
				filters : '',
				sort : '-ga:visits',
				startDate : beginDateAnalytics,
				endDate : endDateAnalytics,
				maxResults : 100000
			},
			function(response) {
				$('#loadingEds').hide();
				var rows = jQuery.parseJSON(response.body);
				drawTableReportingGeo(rows, 'language', 'string',
						LANGUAGELABEL, 'number', VISITSLABEL, 'number', '% '+VISITSLABEL);
				drawChartReportingGeo(rows, 'language', 'string', LANGUAGELABEL, 'number', VISITSLABEL);

			}, function(){
				onErrorMakeCallApi(tryCallNbr, makeApiCallLanguage);
			});
}
function makeApiCallCountry(tryCallNbr) {
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
				dimension : 'ga:country',
				metrics : 'ga:visits',
				filters : '',
				sort : '-ga:visits',
				startDate : beginDateAnalytics,
				endDate : endDateAnalytics,
				maxResults : 100000
			},
			function(response) {
				$('#loadingEds').hide();
				var rows = jQuery.parseJSON(response.body);
				drawTableReportingGeo(rows, 'countryTerritory', 'string',
						COUNTRYTERRITORYLABEL, 'number', VISITSLABEL, 'number',
						'% '+VISITSLABEL);
				drawChartReportingGeo(rows, 'countryTerritory', 'string', COUNTRYTERRITORYLABEL, 'number',
						VISITSLABEL);
			}, function(){
				onErrorMakeCallApi(tryCallNbr, makeApiCallCountry);
			});
}
function makeApiCallCity(tryCallNbr) {
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
				dimension : 'ga:city',
				metrics : 'ga:visits',
				filters : '',
				sort : '-ga:visits',
				startDate : beginDateAnalytics,
				endDate : endDateAnalytics,
				maxResults : 100000
			},
			function(response) {
				$('#loadingEds').hide();
				var rows = jQuery.parseJSON(response.body);
				drawTableReportingGeo(rows, 'city', 'string', CITYLABEL,
						'number', VISITSLABEL, 'number', '% '+VISITSLABEL);
				drawChartReportingGeo(rows, 'city', 'string', CITYLABEL, 'number', VISITSLABEL);
			}, function(){
				onErrorMakeCallApi(tryCallNbr, makeApiCallCity);
			});
}
function makeApiCallBrowser(tryCallNbr) {
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
				dimension : 'ga:browser',
				metrics : 'ga:visits',
				filters : '',
				sort : '-ga:visits',
				startDate : beginDateAnalytics,
				endDate : endDateAnalytics,
				maxResults : 100000
			},
			function(response) {
				$('#loadingEds').hide();
				var rows = jQuery.parseJSON(response.body);
				drawTableReportingGeo(rows, 'browser', 'string',
						BROWSERLABEL, 'number', VISITSLABEL, 'number', '% '+VISITSLABEL);
				drawChartReportingGeo(rows, 'browser', 'string', BROWSERLABEL, 'number', VISITSLABEL);
			}, function(){
				onErrorMakeCallApi(tryCallNbr, makeApiCallBrowser);
			});
}
function makeApiCallOperatingSystem(tryCallNbr) {
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
				dimension : 'ga:operatingSystem',
				metrics : 'ga:visits',
				filters : '',
				sort : '-ga:visits',
				startDate : beginDateAnalytics,
				endDate : endDateAnalytics,
				maxResults : 100000
			},
			function(response) {
				$('#loadingEds').hide();
				var rows = jQuery.parseJSON(response.body);
				drawTableReportingGeo(rows, 'operatingSystem',
						'string', OPERATINGSYSTEMLABEL, 'number', VISITSLABEL,
						'number', '% '+VISITSLABEL);

				drawChartReportingGeo(rows, "operatingSystem",	'string',
						OPERATINGSYSTEMLABEL, 'number', VISITSLABEL);
			}, function(){
				onErrorMakeCallApi(tryCallNbr, makeApiCallOperatingSystem);
			});
}

function makeApiCallOverview() {
	setTimeout(makeApiCallVisitsForum, 500);
	setTimeout(makeApiCallBounces, 2000);
	setTimeout(makeApiCallNewVisits, 4000);
	setTimeout(makeApiCallAvgTimeSite, 8000);
}

function makeApiCallVisitsForum(tryCallNbr) {
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
				dimension : 'ga:day',
				metrics : 'ga:visits',
				filters : '',
				startDate : beginDateAnalytics,
				endDate : endDateAnalytics,
				maxResults : 100000
			},
			function(response) {
				$('#loadingEds').hide();
				var rows = jQuery.parseJSON(response.body);
				drawTableReportingSession(rows, 'visits', 'string', DAYLABEL, 'number', VISITSLABEL);
			}, function(){
				onErrorMakeCallApi(tryCallNbr, makeApiCallVisitsForum);
			});
}

function makeApiCallBounces(tryCallNbr) {
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
				dimension : 'ga:day',
				metrics : 'ga:bounces',
				filters : '',
				startDate : beginDateAnalytics,
				endDate : endDateAnalytics,
				maxResults : 100000
			},
			function(response) {
				$('#loadingEds').hide();
				var rows = jQuery.parseJSON(response.body);
				drawTableReportingSession(rows, 'bounces', 'string', DAYLABEL, 'number',
						AVGTIMEONSITELABEL);
			},  function(){
				onErrorMakeCallApi(tryCallNbr, makeApiCallBounces);
			});
}

function makeApiCallNewVisits(tryCallNbr) {
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
				dimension : 'ga:day',
				metrics : 'ga:percentNewVisits',
				filters : '',
				startDate : beginDateAnalytics,
				endDate : endDateAnalytics,
				maxResults : 100000
			},
			function(response) {
				$('#loadingEds').hide();
				var rows = jQuery.parseJSON(response.body);
				drawTableReportingSession(rows, 'newVisits', 'string', DAYLABEL, 'number',
						'% '+NEWVISITSLABEL);
			},  function(){
				onErrorMakeCallApi(tryCallNbr, makeApiCallNewVisits);
			});
}

function makeApiCallAvgTimeSite(tryCallNbr) {
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
				dimension : 'ga:day',
				metrics : 'ga:avgTimeOnSite',
				filters : '',
				startDate : beginDateAnalytics,
				endDate : endDateAnalytics,
				maxResults : 100000
			},
			function(response) {
				$('#loadingEds').hide();
				var rows = jQuery.parseJSON(response.body);
				drawTableReportingAvgTimeSite(rows,
						'avgTimeOnSite', 'string', DAYLABEL, 'number',
						AVGTIMEONSITELABEL);
			},  function(){
				onErrorMakeCallApi(tryCallNbr, makeApiCallAvgTimeSite);
			});
}

function makeApiCallTrafficSources(tryCallNbr) {
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
				dimension : 'ga:source',
				metrics : 'ga:visits,ga:avgTimeOnSite',
				filters : '',
				sort : '-ga:visits',
				startDate : beginDateAnalytics,
				endDate : endDateAnalytics,
				maxResults : 100000
			},
			function(response) {
				$('#loadingEds').hide();
				var rows = jQuery.parseJSON(response.body);
				drawTableReportingTrafficSource(rows, 'trafficSources', TRAFFICSOURCESLABEL,
						'string', 'Source', 'number', VISITSLABEL, 'number',
						AVGTIMEONSITELABEL);
			},  function(){
				onErrorMakeCallApi(tryCallNbr, makeApiCallTrafficSources);
			});
}
function beforeMakeApiCallContentForum(){
	if (listStandsCache == null) {
	    listStandsCache = new Array();
		genericAjaxCall('GET', staticVars.urlBackEnd+ 'stand/allStands', 'application/xml', 'xml','',
				function(xml){
					$(xml).find("stand").each(function() {
						listStandsCache[$(this).find("idstande").text()] = $(this);
					});
					makeApiCallContentForum();
				},'');
	} else
		makeApiCallContentForum();
}
function makeApiCallContentForum() {
	if (listPhotosCache == null || reportingType!=FORUM) {
    	if(photosController==null){
       		//$("body").append("<script type='text/javascript' src='js/photos.js' charset='utf-8'></script>");
       		photosController = new $.PhotosController(new $.PhotosModel(), new $.PhotosView());
       	}
    	photosController.getAnalyticsPhotos(makeApiCallPhotosForum, 'allfiles/allPhotos');
	} else
		setTimeout(makeApiCallPhotosForum, 0);
	if (listVideosCache == null || reportingType!=FORUM) {
        if(videosController==null){
	   		//$("body").append("<script type='text/javascript' src='js/videos.js' charset='utf-8'><\/script>");
	           videosController = new $.VideosController(new $.VideosModel(), new $.VideosView());
	   	}
        videosController.getAnalyticsVideos(makeApiCallVideosForum, 'allfiles/allVideos');
	} else
		setTimeout(makeApiCallVideosForum, 1500);
	if (listDocumentsCache == null || reportingType!=FORUM) {
    	if(documentController==null){
    		//$("body").append("<script type='text/javascript' src='js/documents.js' charset='utf-8'><\/script>");
    		documentController = new $.DocumentsController(new $.DocumentsModel(), new $.DocumentsView());
    	}
		documentController.getAnalyticsDocuments(makeApiCallDocumentsForum, 'allfiles/allDocuments');
	} else
		setTimeout(makeApiCallDocumentsForum, 3000);
	if (listPostersCache == null || reportingType!=FORUM) {
       	if(postersController==null){
       		//$("body").append("<script type='text/javascript' src='js/posters.js' charset='utf-8'><\/script>");
       		postersController = new $.PostersController(new $.PostersModel(), new $.PostersView());
		}
		postersController.getAnalyticsPosters(makeApiCallPostersForum, 'allfiles/allPosters');
	} else
		setTimeout(makeApiCallPostersForum, 4500);
	if (listJobOffersCache == null || reportingType!=FORUM) {
		if(jobOfferController == null){
    		//$("body").append("<script type='text/javascript' src='js/jobOffers.js' charset='utf-8'><\/script>");
    		jobOfferController = new $.JobOfferController(new $.JobOfferModel(), new $.JobOfferView());
    	}
		jobOfferController.getAnalyticsJobOffers(makeApiCallJobOffersForum, 'jobOffer/allJobOffers');
	} else
		setTimeout(makeApiCallJobOffersForum, 6000);
	setTimeout(makeApiCallJobOfferForum, 0);//6000
	if (listWebsitesCache == null || reportingType != FORUM) {
		getStandWebsiteByPath(makeApiCallWebsitesForum, 'website/allWebSites');
	} else setTimeout(makeApiCallWebsitesForum, 7500);
	if (listSurveysCache == null || reportingType != FORUM) {
		getSurveysByPath(makeApiCallSurveysForum, 'survey/get/analytics/all');
	} else
		setTimeout(makeApiCallSurveysForum, 9500);
	if (listAdvicesCache == null || reportingType != STAND) {
       	if(conseilController==null){
    		//$("body").append("<script type='text/javascript' src='js/advices.js' charset='utf-8'><\/script>");
    		conseilController = new $.ConseilController(new $.ConseilModel(), new $.ConseilView());
		}
       	conseilController.getAnalyticsAdvices(makeApiCallAdvicesForum, 'advice/allAdvices');
	} else
		setTimeout(makeApiCallAdvicesForum, 10500);
	reportingType=FORUM;
}
function makeApiCallPhotosForum(tryCallNbr) {
	var filters=factoryFiltersMultimediaForum(PHOTO);
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
				dimension : 'ga:eventCategory',
				metrics : 'ga:totalEvents',
				filters : filters,
				startDate : beginDateAnalytics,
				endDate : endDateAnalytics,
				maxResults : 10000
			},
			function(response) {
				if (response.body != 'No entries found') {
					var rows = jQuery.parseJSON(response.body);
					drawTableReportingMultimedia(rows, CONTENTFORUM, PHOTOS,
							'string', PHOTOSLABEL, 'string', ENTERPRISELABEL, 'number', NUMBERVIEWSLABEL, PHOTO);
				}
				$('#loadingEds').hide();
			},  function(){
				onErrorMakeCallApi(tryCallNbr, makeApiCallPhotosForum);
			});
}

function makeApiCallDocumentsForum(tryCallNbr) {
	var filters=factoryFiltersMultimediaForum(DOCUMENT);
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
				dimension : 'ga:eventCategory',
				metrics : 'ga:totalEvents',
				filters : filters,
				startDate : beginDateAnalytics,
				endDate : endDateAnalytics,
				maxResults : 10000
			},
			function(response) {
				if (response.body != 'No entries found') {
					var rows = jQuery.parseJSON(response.body);
					drawTableReportingMultimedia(rows, CONTENTFORUM, DOCUMENTS,
							'string', DOCUMENTSLABEL, 'string', ENTERPRISELABEL, 'number', NUMBERVIEWSLABEL,
							DOCUMENT);
				}
				$('#loadingEds').hide();
			},  function(){
				onErrorMakeCallApi(tryCallNbr, makeApiCallDocumentsForum);
			});
}

function makeApiCallVideosForum(tryCallNbr) {
	var filters=factoryFiltersMultimediaForum(VIDEO);
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
				dimension : 'ga:eventCategory',
				metrics : 'ga:totalEvents',
				filters : filters,
				startDate : beginDateAnalytics,
				endDate : endDateAnalytics,
				maxResults : 10000
			},
			function(response) {
				if (response.body != 'No entries found') {
					var rows = jQuery.parseJSON(response.body);
					drawTableReportingMultimedia(rows, CONTENTFORUM, VIDEOS,
							'string', VIDEOSLABEL, 'string', ENTERPRISELABEL, 'number', NUMBERVIEWSLABEL, VIDEO);
				}
				$('#loadingEds').hide();
			},  function(){
				onErrorMakeCallApi(tryCallNbr, makeApiCallVideosForum);
			});
}

function makeApiCallPostersForum(tryCallNbr) {
	var filters=factoryFiltersMultimediaForum(POSTER);
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
				dimension : 'ga:eventCategory',
				metrics : 'ga:totalEvents',
				filters : filters,
				startDate : beginDateAnalytics,
				endDate : endDateAnalytics,
				maxResults : 10000
			},
			function(response) {
				if (response.body != 'No entries found') {
					var rows = jQuery.parseJSON(response.body);
					drawTableReportingMultimedia(rows, CONTENTFORUM, POSTERS,
							'string', POSTERSLABEL, 'string', ENTERPRISELABEL, 'number', NUMBERVIEWSLABEL,
							POSTER);
				}
				$('#loadingEds').hide();
			},  function(){
				onErrorMakeCallApi(tryCallNbr, makeApiCallPostersForum);
			});
}

function makeApiCallAdvicesForum(tryCallNbr) {
	var filters=factoryFiltersMultimediaForum(ADVICE);
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
				dimension : 'ga:eventCategory',
				metrics : 'ga:totalEvents',
				filters : filters,
				startDate : beginDateAnalytics,
				endDate : endDateAnalytics,
				maxResults : 10000
			},
			function(response) {
				if (response.body != 'No entries found') {
					var rows = jQuery.parseJSON(response.body);
					drawTableReportingMultimedia(rows, CONTENTFORUM, ADVICES,
							'string', ADVICESLABEL, 'string', ENTERPRISELABEL, 'number', NUMBERVIEWSLABEL,
							ADVICE);
				}
				$('#loadingEds').hide();
			},  function(){
				onErrorMakeCallApi(tryCallNbr, makeApiCallAdvicesForum);
			});
}

function makeApiCallJobOffersForum(tryCallNbr) {
	var filters=factoryFiltersMultimediaForum(JOBOFFER);
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
				dimension : 'ga:eventCategory',
				metrics : 'ga:totalEvents',
				filters : filters,
				startDate : beginDateAnalytics,
				endDate : endDateAnalytics,
				maxResults : 10000
			},
			function(response) {
				if (response.body != 'No entries found') {
					var rows = jQuery.parseJSON(response.body);
					drawTableReportingMultimedia(rows, CONTENTFORUM, JOBOFFERS,
						'string', JOBOFFERSLABEL, 'string', ENTERPRISELABEL, 'number',
						NUMBERVIEWSLABEL, JOBOFFER);
				}
				$('#loadingEds').hide();
			},  function(){
				onErrorMakeCallApi(tryCallNbr, makeApiCallJobOffersForum);
			});
}
function makeApiCallJobOfferForum(tryCallNbr) {
	genericAjaxCall('GET', staticVars.urlBackEnd
			+ '/jobOffer/allJobOffersAnalytics?idLang='+languageIdAnalytics, 'application/xml', 'xml', '',
			function(response) {
				if ($(response).text() != '') {
					drawTableReportingJobOffer(response, CONTENTFORUM, JOBOFFERSPOSTULATE, 'string', JOBOFFERSLABEL, 
							'string', ENTERPRISELABEL, 'number', CVSSUBMITEDNBRLABEL);
				}
				$('#loadingEds').hide();
			}, function(){
				onErrorMakeCallApi(tryCallNbr, makeApiCallJobOfferForum);
			});
}
function makeApiCallSurveysForum(tryCallNbr) {
	var filters=factoryFiltersMultimediaForum(SURVEY);
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
				dimension : 'ga:eventCategory',
				metrics : 'ga:totalEvents',
				filters : filters,
				startDate : beginDateAnalytics,
				endDate : endDateAnalytics,
				maxResults : 10000
			},
			function(response) {
				if (response.body != 'No entries found') {
					var rows = jQuery.parseJSON(response.body);
					drawTableReportingSurveys(rows, CONTENTFORUM, SURVEYS,
						'string', SURVEYLABEL, 'string', ENTERPRISELABEL, 'number',
						NUMBERVIEWSLABEL, SURVEY);
				}
				$('#loadingEds').hide();
			},  function(){
				onErrorMakeCallApi(tryCallNbr, makeApiCallSurveysForum);
			});
}

function makeApiCallWebsitesForum(tryCallNbr) {
	var filters=factoryFiltersModuleForum(WEBSITE, "");
	//console.log(" makeApiCallWebsitesStand : "+filters);
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
				dimension : 'ga:eventCategory',
				metrics : 'ga:totalEvents',
				filters : filters,
				startDate : beginDateAnalytics,
				endDate : endDateAnalytics,
				maxResults : 10000
			},
			function(response) {
				if (response.body != 'No entries found') {
					var rows = jQuery.parseJSON(response.body);
					drawTableReportingWebsites(rows, CONTENTFORUM, WEBSITES, 'string',
							WEBSITESLABEL, 'string', TYPE, 'string', ENTERPRISELABEL, 'number', NUMBERVIEWSLABEL, WEBSITE);
				}
				$('#loadingEds').hide();
			}, function(){
				onErrorMakeCallApi(tryCallNbr, makeApiCallWebsitesStand);
			});
}
function makeApiCallProfilForum() {
	//console.log('makeApiProfileForum');
	makeApiCallCandidatesProfilForum();
}

function makeApiCallCandidatesProfilForum(tryCallNbr) {
	//console.log('makeApiCallCandidatesProfile');
	if(languageIdAnalytics=='0') filters='ga:eventCategory=@Forum/;ga:eventCategory=@/Profile:Candidate';
	else filters='ga:eventCategory=@Forum/'+LANGUAGEID+':'+languageIdAnalytics+'/Profile:Candidate';
	//console.log('filters : '+filters);
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
				dimension : 'ga:eventAction',
				metrics : 'ga:totalEvents',
				filters : filters,
				startDate : beginDateAnalytics,
				endDate : endDateAnalytics,
				maxResults : 10000
			},
			function(response) {
				if (response.body == 'No entries found') {
					$('#loadingEds').hide();
				}else {
					var rows = jQuery.parseJSON(response.body);
					//console.log('makeApiCallCandidatesProfile success'+JSON.stringif(rows));
					candidateProfileList = new Array();
					for ( var i = 0, row; row = rows[i]; ++i) {
						//console.log(row[0].split('/')[0]);
						candidateProfileList.push(row[0].split('/')[0]);
					}
	
					launchReportingProfile(drawTablesForum);
				}
			},  function(){
				onErrorMakeCallApi(tryCallNbr, makeApiCallCandidatesProfileForum);
			});
}

function makeApiCallOverviewHour(tableName, dateValue) {
	//console.log('tableName : '+tableName+', startDate : '+beginDateAnalytics);
	if (tableName == 'visits') {
		makeApiCallOverviewVisitsHour(0, dateValue);
	} else if (tableName == 'bounces') {
		makeApiCallOverviewBouncesHour(0, dateValue);
	} else if (tableName == 'newVisits') {
		makeApiCallOverviewNewVisitsHour(0, dateValue);
	}
}
function makeApiCallOverviewVisitsHour(tryCallNbr, dateValue) {
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
		dimension : 'ga:hour',
		metrics : 'ga:visits',
		filters : '',
		startDate : dateValue,
		endDate : dateValue,
		maxResults : 100000
	},
	function(response) {
		var rows = jQuery.parseJSON(response.body);
		drawArrayTableReportingSession(rows,
				'visits', 'string', HOURLABEL, 'number',
				VISITSLABEL);
	},  function(){
		$('#loadingEds').hide();
		onErrorMakeCallApi(tryCallNbr, makeApiCallOverviewVisitsHour, dateValue);
	});
}
function makeApiCallOverviewBouncesHour(tryCallNbr, dateValue) {
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
		dimension : 'ga:hour',
		metrics : 'ga:bounces',
		filters : '',
		startDate : dateValue,
		endDate : dateValue,
		maxResults : 100000
	},
	function(response) {
		var rows = jQuery.parseJSON(response.body);
		drawArrayTableReportingSession(rows,
				'bounces', 'string', HOURLABEL,
				'number', BOUNCESLABEL);
	},  function(){
		setTimeout(makeApiCallOverviewBouncesHour, 2000, dateValue);
	});
}
function makeApiCallOverviewNewVisitsHour(tryCallNbr, dateValue) {
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
		dimension : 'ga:hour',
		metrics : 'ga:percentNewVisits',
		filters : '',
		startDate : dateValue,
		endDate : dateValue,
		maxResults : 100000
	},
	function(response) {
		var rows = jQuery.parseJSON(response.body);
		drawArrayTableReportingSession(rows,
				'newVisits', 'string', HOURLABEL,
				'number', '% '+NEWVISITSLABEL);
	},  function(){
		onErrorMakeCallApi(tryCallNbr, makeApiCallOverviewNewVisitsHour, dateValue);
	});
}
function makeApiCallAvgTimeOnSiteHour(tryCallNbr, dateValue) {
	genericAjaxCall('GET', urlWSAnalytics, 'application/json', 'json', {
				dimension : 'ga:hour',
				metrics : 'ga:avgTimeOnSite',
				filters : '',
				startDate : dateValue,
				endDate : dateValue,
				maxResults : 100000
			},
			function(response) {
				var rows = jQuery.parseJSON(response.body);
				drawArrayTableReportingAvgTimeOnSiteHour(rows,
						'avgTimeOnSite', 'string', HOURLABEL,
						'number', AVGTIMEONSITELABEL);
			},  function(){
				onErrorMakeCallApi(tryCallNbr, makeApiCallAvgTimeOnSiteHour, dateValue);
			});
}
