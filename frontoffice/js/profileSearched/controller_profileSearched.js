jQuery.extend({
    ProfileSearchedController: function (model, view) {
        var that = this;
        /**
         * listen to the view
         */
        var vlist = $.ProfileSearchedViewListener({
            saveProfileSearched: function (stand) {
                model.saveProfileSearched(stand);
            }
        });
        view.addListener(vlist);

        /**
         * listen to the model
         */
        var mlist = $.ProfileSearchedModelListener({
            profileSearchedLoad: function (stand) {
                view.profileSearchedLoad(stand);
            },
            saveProfileSearchedAlertSuccess: function (stand) {
                view.viewSaveProfileSearchedSuccess();
            }
        });

        model.addListener(mlist);

        this.initController = function () {
            view.initView();
            model.initModel();
        };
    }
});
