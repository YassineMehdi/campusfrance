jQuery.extend({
    ProfilEntrepriseController: function (model, view) {
        var that = this;
        /**
         * listen to the view
         */
        var vlist = $.ProfilEntrepriseViewListener({
            updateStand: function (stand) {
                model.updateProfilStand(stand);
            },
            getStandInfos : function (langId) {
                model.getStandInfos(langId);
            },
        });

        view.addListener(vlist);
        /**
         * listen to the model
         */
        var mlist = $.ProfilEntrepriseModelListener({
            standLoad: function (stand) {
                view.parseStand(stand);
            },
            sectorsLoad: function (sectors) {
                view.optionSectorsLoad(sectors);
            },
            updateSuccess: function () {
                view.viewUpdateSuccess();
            },
            groupStandLoad:function(groupStands){
            	view.groupStandLoad(groupStands);
            },
            standLanguagesLoad:function(languages){
            	view.loadLanguages(languages);
            }
        });

        model.addListener(mlist);

        this.initController = function () {
            view.initView();
            model.initModel();
        };
    }
});
