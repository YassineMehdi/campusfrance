jQuery.extend({
    
    ProfilEntrepriseModel: function () {
        var that = this;
        var listeners = new Array();
        this.initModel = function () {
            that.getSectors();
            that.getStandGroups();
        	that.getLanguages();
        	setTimeout(function(){
                that.getStandInfos(getIdLangParam());
        	}, 1000);
        };
        // Info profil
        this.updateProfilStandSuccess = function (xml) {
            //console.log(xml);
            that.notifyUpdateStandSuccess();
        };

        this.updateProfilStandError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };
        this.updateProfilStand = function (stand) {
            genericAjaxCall('POST', staticVars.urlBackEndManager + 'stand/update?idLang=' + getIdLangParam()+'&idEnterprise='+enterpriseId+'&idStand='+standId,
                    'application/xml', 'xml', stand.xmlData(), that.updateProfilStandSuccess, that.updateProfilStandError);
        };
        // Info profil
        this.getStandInfosSuccess = function (xml) {
        	that.notifyStandSuccess(new $.Stand(xml));
        };

        this.getStandInfosError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };
        this.getStandInfos = function (langId) {
        	genericAjaxCall('GET', staticVars.urlBackEndManager + 'stand/standInfo?idLang=' + langId+'&enterpriseId='+enterpriseId,
                    'application/xml', 'xml', '', that.getStandInfosSuccess, that.getStandInfosError);
        };
        //Group stand
        this.getStandGroups=function(){
        	genericAjaxCall('GET', staticVars.urlBackEndManager + 'groupStand/all',
                    'application/xml', 'xml', '', that.getStandGroupsSuccess, that.getStandGroupsError);
        };
        this.getStandGroupsSuccess=function(xml){
        	var groupStands=new Array();
        	$(xml).find('groupStandDTO').each(function(){
        		var standGroup=new $.StandGroup($(this));
        		groupStands.push(standGroup);
        	});
        	that.notifyGroupStandSuccess(groupStands);
        };
        this.getStandGroupsError=function(){
        	
        };
        this.getLanguages=function(){
        	genericAjaxCall('GET', staticVars.urlBackEndManager + 'languaget/listLanguages?idLang=' + getIdLangParam(),
                    'application/xml', 'xml', '', that.getLanguagesSuccess, that.getLanguagesError);
        };
        this.getLanguagesSuccess=function(xml){
        	var languages=new Array();
        	$(xml).find("languaget").each(function(){
        		var language=new $.LanguageT($(this));
        		languages.push(language);
        	});
        	that.notifyLanguageSuccess(languages);
        };
        this.getLanguagesError=function(){
        	
        };
        // option sectors
        this.getSectorsSuccess = function (xml) {
            //console.log(xml);
            var sectors = new Array();
            var sector = null;
            $(xml).find("sectorDTO").each(function () {
                sector = new $.Sector($(this));
                sectors.push(sector);
            });
            that.notifySectorsSuccess(sectors);
        };

        this.getSectorsError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };
        this.getSectors = function () {
            genericAjaxCall('GET', staticVars.urlBackEndManager + 'sector?idLang=' + getIdLangParam(),
                    'application/xml', 'xml', '', that.getSectorsSuccess, that.getSectorsError);
        };

        this.addListener = function (list) {
            listeners.push(list);
        };

        this.notifyUpdateStandSuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].updateSuccess();
            });
        };
        this.notifyStandSuccess = function (stand) {
            $.each(listeners, function (i) {
                listeners[i].standLoad(stand);
            });
        };
        this.notifyGroupStandSuccess=function(groupStands){
        	$.each(listeners,function(i){
        		listeners[i].groupStandLoad(groupStands);
        	});
        };
        this.notifyLanguageSuccess=function(languages){
        	$.each(listeners,function(i){
        		listeners[i].standLanguagesLoad(languages);
        	});
        };
        this.notifySectorsSuccess = function (sectors) {
            $.each(listeners, function (i) {
                listeners[i].sectorsLoad(sectors);
            });
        };
        this.notifyContactsSuccess = function (contacts) {
            $.each(listeners, function (i) {
                listeners[i].contactsLoad(contacts);
            });
        };

    },
    ProfilEntrepriseModelListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }
});
