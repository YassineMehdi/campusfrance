jQuery.extend({
    CandidatureView: function () {
        var that = this;
        var listeners = new Array();
        var contentHandler = null;
        var listUnsolicitedCandidaturesCache = null;
        var listCandidaturesCache = null;
        var currentUnsolicitedCandidaturesCache = null;
        var currentCandidaturesCache = null;
        var typeJobApplicationToChange = null;
        var SOLICITED = "solicited";
        var UNSOLICITED = "unsolicited";
        var validator = null;
        var saveMessageCandidatureHandler = null;

        this.initView = function () {
            contentHandler = $("#mainBackEndContainer");
            /*************************************************************************************
             Candidatures
             *************************************************************************************/
            //datatable
            var oTableCandidature = $('#listCandidaturesTable').dataTable({
                "language": {
                    "lengthMenu": "_MENU_"
                },
                "aaData": [],
                "aoColumns": [{
                        "sTitle": FIRSTANDLASTNAMELABEL, "sWidth": "35%"
                    },
                    {
                        "sTitle": TITLELABEL, "sWidth": "50%"
                    }, {
                        "sTitle": ACTIONSLABEL, "sWidth": "15%"
                    }, {
                        "sTitle": "id job", "sWidth": "0%"
                    }, {
                        "sTitle": "", "sWidth": "0%"
                    }],
                "aaSorting": [[3, "desc"]],
                "aoColumnDefs": [{"bVisible": false, "aTargets": [3, 4]}],
                "bAutoWidth": false,
                "bRetrieve": false,
                "bDestroy": true,
                "iDisplayLength": 10,
                "fnDrawCallback": function () {
                    $('#listCandidaturesTable .readCandidate').off("click");
                    $('#listCandidaturesTable .viewProfile').on("click", function () {
                        var jobApplicationId = $(this).attr('index');
                        var jobApplication = listCandidaturesCache[jobApplicationId];
                        viewUserProfile(jobApplication.getCandidateId());
                    });
                    $('#listCandidaturesTable .viewCv').on("click", function () {
                        var jobApplicationId = $(this).attr('index');
                        var jobApplication = listCandidaturesCache[jobApplicationId];
                        if (jobApplication != null) {
                            window.open(jobApplication.getUrlCv(), '_blank');
                        }
                    });
                    $('#listCandidaturesTable .viewLetter').on("click", function () {
                        var jobApplicationId = $(this).attr('index');
                        var jobApplication = listCandidaturesCache[jobApplicationId];
                        if (jobApplication != null) {
                            that.viewLetter(jobApplication.getMotivationLetter(), jobApplication.getUserId(), jobApplication.getFullName(), jobApplication.getJobApplicationId(), SOLICITED, jobApplication.response);
                        }
                    });
                    $('#listCandidaturesTable .readCandidate').on("click", function () {
                        var jobApplicationId = $(this).attr('index');
                        var jobApplication = listCandidaturesCache[jobApplicationId];
                        if (jobApplication.getUnread()) {
                            $('#listCandidaturesTable #notReaded_' + jobApplicationId).hide();
                            that.notifyReadJobApplication(jobApplicationId);
                            jobApplication.setUnread(false);
                        }
                    });
                }
            });

            that.toolsTable(oTableCandidature, '#candidatures1');
            $("#candidatureSearchDiv #jobOfferSelect").unbind("change").bind("change", that.filterJobApplication);
            /*************************************************************************************
             Candidatures spontanées
             *************************************************************************************/
            saveMessageCandidatureHandler = $("#sendMessageCandidature");
            //save Job
            saveMessageCandidatureHandler.unbind("click").bind("click", function (e) {
                that.saveMessageCandidature();
            });
            //datatable
            var oTable = $('#listUnsolicitedCandidaturesTable').dataTable({
                "language": {
                    "lengthMenu": "_MENU_"
                },
                "aaData": [],
                "aoColumns": [{
                        "sTitle": FIRSTANDLASTNAMELABEL, "sWidth": "85%",
                    }, {
                        "sTitle": ACTIONSLABEL, "sWidth": "15%",
                    }, {
                        "sTitle": 'id job',
                    }, {
                        "sTitle": "", "sWidth": "0%"
                    }],
                "aaSorting": [[2, "desc"]],
                "aoColumnDefs": [{"bVisible": false, "aTargets": [2, 3]}],
                "bAutoWidth": false,
                "bRetrieve": false,
                "bDestroy": true,
                "iDisplayLength": 10,
                "fnDrawCallback": function () {
                    $('#listUnsolicitedCandidaturesTable .readCandidate').off("click");
                    $('#listUnsolicitedCandidaturesTable .viewProfile').off('click').on("click", function () {
                        var unsolicitedJobApplicationId = $(this).attr('index');
                        var unsolicitedJobApplication = listUnsolicitedCandidaturesCache[unsolicitedJobApplicationId];
                        viewUserProfile(unsolicitedJobApplication.getCandidateId());

                    });
//
                    $('#listUnsolicitedCandidaturesTable .viewCv').off('click').on("click", function () {
                        var unsolicitedJobApplicationId = $(this).attr('index');
                        var unsolicitedJobApplication = listUnsolicitedCandidaturesCache[unsolicitedJobApplicationId];
                        if (unsolicitedJobApplication != null) {
                            window.open(unsolicitedJobApplication.getUrlCv(), '_blank');
                        }
                    });
                    $('#listUnsolicitedCandidaturesTable .viewLetter').off('click').on("click", function () {
                        var unsolicitedJobApplicationId = $(this).attr('index');
                        var unsolicitedJobApplication = listUnsolicitedCandidaturesCache[unsolicitedJobApplicationId];
                        if (unsolicitedJobApplication != null) {
                            that.viewLetter(unsolicitedJobApplication.getMotivationLetter(), unsolicitedJobApplication.getUserId(), unsolicitedJobApplication.getFullName(), unsolicitedJobApplicationId, UNSOLICITED, unsolicitedJobApplication.response);
                        }
                    });
                    $('#listUnsolicitedCandidaturesTable .readCandidate').on("click", function () {
                        var unsolicitedJobApplicationId = $(this).attr('index');
                        var unsolicitedJobApplication = listUnsolicitedCandidaturesCache[unsolicitedJobApplicationId];
                        if (unsolicitedJobApplication.getUnread()) {
                            $('#listUnsolicitedCandidaturesTable #notReaded_' + unsolicitedJobApplicationId).hide();
                            that.notifyReadUnsolicitedJobApplication(unsolicitedJobApplicationId);
                            unsolicitedJobApplication.setUnread(false);
                        }
                    });
                }
            });
            that.toolsTable(oTable, '#unsolicitedCandidatures');
            $("#candidatureSearchDiv").appendTo("#candidatures1 .panel-heading");
        };
        this.toolsTable = function (oTable, parent) {
            var tableTools = new $.fn.dataTable.TableTools(oTable, {
                "buttons": [
                    //{"type": "print", "buttonText": "Print me!"}
                ],
                "sSwfPath": "assets/plugins/datatables/TableTools/swf/copy_csv_xls_pdf.swf"
            });
            //DOM Manipulation to move datatable elements integrate to panel
            $(parent).find('#panel-tabletools .panel-ctrls').append($(parent).find('.dataTables_filter').addClass("pull-right"));
            $(parent).find('#panel-tabletools .panel-ctrls .dataTables_filter').find("label").addClass("panel-ctrls-center");

            $(parent).find('#panel-tabletools .panel-ctrls').append("<i class='separator pull-right '></i>");

            $(parent).find('#panel-tabletools .panel-ctrls').append($(parent).find('.dataTables_length').addClass("pull-right"));
            $(parent).find('#panel-tabletools .panel-ctrls .dataTables_length label').addClass("mb0");

            $(parent).find('#panel-tabletools .panel-ctrls').append("<i class='separator pull-right '></i>");
            $(tableTools.fnContainer()).appendTo($(parent).find('#panel-tabletools .panel-ctrls')).addClass("pull-right mt10");

            $(parent).find('#panel-tabletools .panel-footer').append($(".dataTable+.row"));
            $(parent).find('.dataTables_paginate>ul.pagination').addClass("pull-right");
        };
        // Candidatures
        this.filterJobApplication = function () {
            var jobApplications = [];
            var jobOfferId = $("#candidatureSearchDiv #jobOfferSelect").val();
            for (var index in listCandidaturesCache) {
                var jobApplication = listCandidaturesCache[index];
                if (jobOfferId == 0 || jobApplication.jobOfferId == jobOfferId)
                    jobApplications.push(jobApplication);
            }
            that.listingJobApplications(jobApplications);
        };
        this.displayJobOffers = function (jobOffers) {
            var jobOfferSelectHandler = $('#candidatureSearchDiv #jobOfferSelect');
            jobOfferSelectHandler.empty();
            jobOfferSelectHandler.append($('<option/>').val(0).html(ALLLABEL));
            jobOffers.sort(function (a, b) {
                if (a.getJobTitle() > b.getJobTitle()) {
                    return 1;
                } else if (a.getJobTitle() < b.getJobTitle()) {
                    return -1;
                } else {
                    return 0;
                }
            });
            for (var index in jobOffers) {
                var jobOffer = jobOffers[index];
                jobOfferSelectHandler.append($('<option/>').val(jobOffer.getJobId()).html(substringCustom(jobOffer.getJobTitle(), 50)));
            }
            $('#loadingEds').hide();
        };

        this.displayJobApplications = function (listJobApplications) {
            listCandidaturesCache = listJobApplications;
            that.listingJobApplications(listJobApplications);
        };

        this.listingJobApplications = function (listJobApplications) {
            currentCandidaturesCache = listJobApplications;
            var datas = new Array();
            var btnProfileHandler = $("<a data-toggle='tooltip' title='"+SEEPROFILELABEL+"' href='javascript:void(0);' class='readCandidate viewProfile editLink actionBtn'><i class='fa fa-info-circle'></i></a> ");
            var btnCvHandler = $("<a data-toggle='tooltip' title='"+SEECVLABEL+"' href='javascript:void(0);' class='readCandidate viewCv viewLink actionBtn'><i class='fa fa-file-pdf-o'></i></a>");
            var btnLetterHandler = $("<a data-toggle='tooltip' title='"+SEELETTERLABEL+"' href='javascript:void(0);' class='readCandidate viewLetter deleteLink actionBtn'><i class='fa fa-newspaper-o'></i></a>");
            var listActions = $("<div></div>");
            var newHandler = $('<span class="label label-primary label-important new-candidature">'+NEWLABEL+'</span>');
            for (var index in listJobApplications) {
                var jobApplication = listJobApplications[index];
                var jobApplicationId = jobApplication.getJobApplicationId();
                var urlCv = jobApplication.getUrlCv();
                var btnCv = null;
                var fullName = jobApplication.getFullName();
                if (jobApplication.getUnread()) {
                    fullName = getHtml(newHandler.attr("id", "notReaded_" + jobApplicationId)) + fullName;
                }
                btnProfileHandler.attr("index", index);
                btnLetterHandler.attr("index", index);
                if (urlCv.trim() != '' && urlCv != null) {
                    btnCvHandler.attr("index", index);
                    btnCv = getHtml(btnCvHandler);
                }

                listActions.attr("index", jobApplicationId);
                listActions.html(getHtml(btnProfileHandler)
                        + getHtml(btnCv)
                        + getHtml(btnLetterHandler));
                var data = [fullName, jobApplication.getJobOfferTitle(), getHtml(listActions),
                    jobApplicationId, jobApplication.getMotivationLetter()];
                datas.push(data);
            }
            addTableDatas('#listCandidaturesTable', datas);
            $('[data-toggle="tooltip"]').tooltip();
            $('#loadingEds').hide();
        };

        // Candidatures spontanées
        this.displayUnsolicitedJobApplications = function (listUnsolicitedJobApplication) {
            listUnsolicitedCandidaturesCache = listUnsolicitedJobApplication;
            that.listingUnsolicitedJobApplications(listUnsolicitedJobApplication);
        };
        this.listingUnsolicitedJobApplications = function (listUnsolicitedJobApplication) {
            currentUnsolicitedCandidaturesCache = listUnsolicitedJobApplication;
            var datas = new Array();
            var btnProfileHandler = $("<a data-toggle='tooltip' title='"+SEEPROFILELABEL+"' href='javascript:void(0);' class='readCandidate viewProfile editLink actionBtn'><i class='fa fa-info-circle'></i></a> ");
            var btnCvHandler = $("<a data-toggle='tooltip' title='"+SEECVLABEL+"'  href='javascript:void(0);' class='readCandidate viewCv viewLink actionBtn'><i class='fa fa-file-pdf-o'></i></a>");
            var btnLetterHandler = $("<a data-toggle='tooltip' title='"+SEELETTERLABEL+"'  href='javascript:void(0);' class='readCandidate viewLetter deleteLink actionBtn'><i class='fa fa-newspaper-o'></i></a>");
            var listActions = $("<div></div>");
            var newHandler = $('<span class="label label-primary label-important new-candidature">'+NEWLABEL+'</span>');
            for (var index in listUnsolicitedJobApplication) {
                var unsolicitedJobApplication = listUnsolicitedJobApplication[index];
                var unsolicitedJobApplicationId = unsolicitedJobApplication.getJobApplicationId();
                var urlCv = unsolicitedJobApplication.getUrlCv();
                var btnCv = null;
                var fullName = unsolicitedJobApplication.getFullName();
                if (unsolicitedJobApplication.getUnread()) {
                    fullName = getHtml(newHandler.attr("id", "notReaded_" + unsolicitedJobApplicationId)) + fullName;
                }
                btnProfileHandler.attr("index", index);
                btnLetterHandler.attr("index", index);
                if (urlCv.trim() != '' && urlCv != null) {
                    btnCvHandler.attr("index", index);
                    btnCv = getHtml(btnCvHandler);
                }

                listActions.attr("index", unsolicitedJobApplicationId);
                listActions.html(getHtml(btnProfileHandler)
                        + getHtml(btnCv)
                        + getHtml(btnLetterHandler));
                var data = [fullName, getHtml(listActions), unsolicitedJobApplicationId,
                    unsolicitedJobApplication.getMotivationLetter()];
                datas.push(data);
            }
            addTableDatas('#listUnsolicitedCandidaturesTable', datas);
            $('[data-toggle="tooltip"]').tooltip();
            $('#loadingEds').hide();
        };
        this.viewLetter = function (motivationLetter, userId, fullName, jobApplicationId, type, response) {
            $('.popupMotivationLetter').modal();
            //$('.popupMessageCandidature').hide();
            $('#letterContentCandidature').text(motivationLetter);
            $('#messageContentCandidature').val(response);
            $('#letterReply').unbind('click').bind('click', function () {
                $('#idJobApplication').val(jobApplicationId);
                $('#typeJobApplication').val(type);
                $('.popupMotivationLetter').modal('hide');
                $('#messageReceiverCandidature').val(fullName);
                $('.popupMessageCandidature').modal();
                if (validator != null)
                    validator.resetForm();
            });
        };
        //Save Message Candidature
        this.saveMessageCandidature = function () {
            that.validateMessageCandidature();
            if ($("#messageFormCandidature").valid()) {
                var type = $('#typeJobApplication').val();
                if (type == SOLICITED) {
                    var jobApplication = listCandidaturesCache[$('#idJobApplication').val()];
                    jobApplication.setResponse($('#messageContentCandidature').val());
                    that.notifyUpdateJobApplication(jobApplication);
                } else if (type == UNSOLICITED) {
                    var unsolicitedJobApplication = listUnsolicitedCandidaturesCache[$('#idJobApplication').val()];
                    unsolicitedJobApplication.setResponse($('#messageContentCandidature').val());
                    that.notifyUpdateUnsolicitedJobApplication(unsolicitedJobApplication);
                }
            }
            ;
        };
        this.sendMessageUnsolicitedJobSuccess = function () {
            $('.popupMessageCandidature').modal('hide');
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; <strong>Job</strong> bien enregistrer.<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        // Validator form
        this.validateMessageCandidature = function () {
            validator = $("#messageFormCandidature").validate({
                rules: {
                    messageReceiverCandidature: {
                        required: true
                    },
                    messageContentCandidature: {
                        required: true
                    }
                }
            });
        };

        this.addListener = function (list) {
            listeners.push(list);
        };

        this.notifyUpdateJobApplication = function (jobApplication) {
            $.each(listeners, function (i) {
                listeners[i].updateJobApplication(jobApplication);
            });
        };

        this.notifyReadJobApplication = function(jobApplicationId) {
            $.each(listeners, function(i) {
                listeners[i].readJobApplication(jobApplicationId);
            });
        };

        this.notifyUpdateUnsolicitedJobApplication = function (unsolicitedJobApplication) {
            $.each(listeners, function (i) {
                listeners[i].updateUnsolicitedJobApplication(unsolicitedJobApplication);
            });
        };
        
        this.notifyReadUnsolicitedJobApplication = function(unsolicitedJobApplicationId) {
            $.each(listeners, function(i) {
                listeners[i].readUnsolicitedJobApplication(unsolicitedJobApplicationId);
            });
        };
    }
    ,
    CandidatureViewListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }

});
