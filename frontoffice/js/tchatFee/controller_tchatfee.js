jQuery.extend({
    TchatfeeController: function (model, view) {
        var that = this;
        /**
         * listen to the view
         */
        var vlist = $.TchatfeeViewListener({
            notifyGetHistoryChat: function (startDate,endDate) {
                model.getHistoryChat(startDate,endDate);
            }
        });

        view.addListener(vlist);
        /**
         * listen to the model
         */
        var mlist = $.TchatfeeModelListener({
            notifyRefreshHistoryChat: function () {
                view.refreshHistoryChat();
            },
        });

        model.addListener(mlist);

        this.initController = function () {
            view.initView();
            view.initHistory();
            model.initModel();
        };
    }
});
