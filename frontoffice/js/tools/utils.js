var browserLang = window.navigator.language;
function getParam(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    if (results == null)
        return "";
    else
        return results[1];
}
function getIdLangParam() {
    var idLang = getParam("idLang");
    // console.log("idLang : "+idLang);
    if (idLang == "" || idLang == null || (idLang != 3 && idLang != 2)) {
        /*switch (browserLang) {
         case 'en':
         idLang = 2;
         break;
         case 'de':
         idLang=1;
         break;
         default:
         idLang = 3;
         break;
         }
         */idLang = 3;
        window.location.replace("?idLang=" + idLang);
    }
    return idLang;
}

function getLocalePDFJS() {
    var idLang = getParam("idLang");
    var locale = null;
    switch (idLang) {
        case 1:
            locale = 'de';
            break;
        case 2:
            locale = 'en';
            break;
        default:
            locale = 'fr';
            break;
    }
    return locale;
}