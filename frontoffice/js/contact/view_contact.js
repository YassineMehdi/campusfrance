jQuery.extend({
    ContactManage: function (contact, view) {
        var that = this;
        var dom = $('<div class="col-xs-6 col-md-4 newContact" id="contact' + contact.getIdContact() + '">'
                + '<div class= "panel panel-default" data - widget = "">'
                + '<div class="hide contactID contactID_' + contact.getIdContact() + '">' + contact.getIdContact() + '</div>'
                + '<div class = "contactOptions">'
                + '<a href = "javascript:void(0)" class = "button-icon editContact"> <i class = "fa fa-pencil"> </i></a>'
                + '<a href = "javascript:void(0)" class = "button-icon deleteContact"> <i class = "fa fa-times"> </i></a>'
                + '</div>'
                + '<div class = "panel-body">'
                + '<img class="contactPhoto" alt = "" src = "' + contact.getPhotoContact() + '">'
                + '<ul class = "contactUl">'
                + '<li class="contactName">' + contact.getNameContact() + '</li>'
                + '<li class="contactFunction">' + contact.getFunctionContact() + '</li>'
                + '<li class="contactEmail">' + contact.getEmailContact() + '</li>'
                + '<li class="contactPhone">' + contact.getPhoneContact() + '</li>'
                + '</ul>'
                + '</div>'
                + '</div></div>'
                + '</div>'
                );
        this.refresh = function () {
            dom.find('.contactPhoto').attr('src', contact.getPhotoContact());
            dom.find('.contactName').text(contact.getNameContact());
            dom.find('.contactFunction').text(contact.getFunctionContact());
            dom.find('.contactEmail').text(contact.getEmailContact());
            dom.find('.contactPhone').text(contact.getPhoneContact());

            dom.find('.deleteContact').unbind("click").bind("click", function (e) {
                view.factoryModalRemoveContact(contact);
            });
            //Delete Contact
            dom.find('.editContact').unbind("click").bind("click", function (e) {
                view.factoryUpdateConact(contact);
            });
        };
        this.getDOM = function () {
            return dom;
        };
        this.getContact = function () {
            return contact;
        };
        this.refresh();
    },
    ContactView: function () {
        var that = this;
        that.update = false;
        var listeners = new Array();
        var enterpriseProfileForm = null;
        var contentHandler = null;
        var newContactForm = null;
        var saveProfileHandler = null;
        var contactContentHandler = null;
        var addContactBtnHandler = null;
        var modalContactHandler = null;
        var saveNewContacttHandler = null;
        var validatorContact = null;
        var idDeletecontactHandler = null;
        var contactManageList = null;
        var removeConatactBtnHandler = null;
        var modalIDContactHandler = null;

        this.initView = function () {
            traduct();
            contentHandler = $("#mainBackEndContainer");
            enterpriseProfileForm = $("#enterpriseProfileForm");
            newContactForm = $("#contactForm");
            saveProfileHandler = $(".saveAccountButton");
            contactContentHandler = $(".contactG");
            addContactBtnHandler = $(".addContactLink");
            modalContactHandler = $("#addContact");
            saveNewContacttHandler = $("#saveContact");
            idDeletecontactHandler = $("#idDeleteContact");
            removeConatactBtnHandler = $("#btnDeletecontact");
            modalIDContactHandler = $('#idContact');
            contactManageList = new Array();

            that.validateNewContactForm();
            saveNewContacttHandler.unbind("click").bind("click", function (e) {
                that.saveContactDom();
            });
            //Custom file upload
            $('#logoUpload').fileupload({
                beforeSend: function (jqXHR, settings) {
                    beforeUploadFile('.saveStandButton', '#progressLogoUpload', '#logoUploadStand .filebutton,#loadLogoStandError');
                },
                datatype: 'json',
                cache: false,
                add: function (e, data) {
                    var goUpload = true;
                    var uploadFile = data.files[0];
                    var fileName = uploadFile.name;
                    timeUploadLogo = (new Date()).getTime();
                    data.url = getUrlPostUploadFile(timeUploadLogo, fileName);
                    if (!(/\.(gif|jpg|jpeg|tiff|png)$/i).test(fileName)) {
                        goUpload = false;
                    }
                    if (uploadFile.size > MAXFILESIZE) {// 6mb
                        goUpload = false;
                    }
                    if (goUpload == true) {
                        data.submit();
                    } else
                        $('#loadLogoStandError').show();
                },
                progressall: function (e, data) {
                    progressBarUploadFile('#progressLogoUpload', data, 100);
                },
                done: function (e, data) {
                    var file = data.files[0];
                    var fileName = file.name;
                    var hashFileName = getUploadFileName(timeUploadLogo, fileName);
                    //console.log("Add logo stand file done, filename :"+fileName+", hashFileName : "+hashFileName);
                    var enterpriseLogo = getUploadFileUrl(hashFileName);
                    $('#enterpriseLogo').attr('src', enterpriseLogo);
                    afterUploadFile('.saveStandButton', '#logoUploadStand .filebutton', '#progressLogoUpload');
                }
            });
            //Add Contact
            addContactBtnHandler.unbind("click").bind("click", function (e) {
                that.factoryAddConact();

            });
            //Delete Contact
            removeConatactBtnHandler.unbind("click").bind("click", function (e) {
                //var contactManage = contactManageList[idDeletecontactHandler.val()];
                //console.log(contactManageList);
                //contactManage.getDOM().remove();
                //delete contactManageList[idDeletecontactHandler.val()];
                //console.log(contactManageList);
                that.notifyDeleteContact(idDeletecontactHandler.val());

            });
            //Add Advice file upload avatar
            $('#uploadAvatar').fileupload({
                beforeSend: function (jqXHR, settings) {
                    beforeUploadFile('.saveStandButton', '#progressAvatarUpload', '#avatarUploadContact .filebutton, .loadError');
                },
                datatype: 'json',
                cache: false,
                add: function (e, data) {
                    var goUpload = true;
                    var uploadFile = data.files[0];
                    var fileName = uploadFile.name;
                    timeUploadLogo = (new Date()).getTime();
                    data.url = getUrlPostUploadFile(timeUploadLogo, fileName);
                    if (!(/\.(gif|jpg|jpeg|tiff|png)$/i).test(fileName)) {
                        goUpload = false;
                    }
                    if (uploadFile.size > MAXFILESIZE) {// 6mb
                        goUpload = false;
                    }
                    if (goUpload == true) {
                        data.submit();
                    } else
                        $('.loadError').show();
                },
                progressall: function (e, data) {
                    progressBarUploadFile('#progressAvatarUpload', data, 100);
                },
                done: function (e, data) {
                    var file = data.files[0];
                    var fileName = file.name;
                    var hashFileName = getUploadFileName(timeUploadLogo, fileName);
                    //console.log("Add logo stand file done, filename :"+fileName+", hashFileName : "+hashFileName);
                    var avatarContact = getUploadFileUrl(hashFileName);
                    $('#avatarNewContact').attr('src', avatarContact);
                    afterUploadFile('.saveStandButton', '#avatarUploadContact .filebutton', '#progressAvatarUpload');
                }
            });

        };

        //Contacts Xml
        this.parseContacts = function (contacts) {
            contactContentHandler.empty();
            if (contacts != null) {
                for (var i = 0; i < contacts.length; i++) {
                    var contact = contacts[i];
                    var contactManage = new $.ContactManage(contact, that);
                    //console.log(activitieManage.getDOM());
                    contactContentHandler.append(contactManage.getDOM());
                    contactManageList[contact.getIdContact()] = contactManage;
                }
            }
            $('#loadingEds').hide();
        };
        this.saveContactDom = function (contact) {
            if (newContactForm.valid()) {
                if (!that.update) {
                    var contactObject = new $.Contact();
                    contactObject.setIdContact(new Date().getTime());
                    contactObject.setPhotoContact($('#avatarNewContact').attr('src'));
                    contactObject.setNameContact($('#contactName').val());
                    contactObject.setFunctionContact($('#contactJobTitle').val());
                    contactObject.setEmailContact($('#emailContact').val());
                    contactObject.setPhoneContact($('#contactTel').val());

                    //var contactManage = new $.ContactManage(contactObject, that);
                    //contactContentHandler.append(contactManage.getDOM());
                    //contactManageList.push(contactManage);
                    that.notifyAddContact(contactObject);
                } else {
                    //console.log(modalIDContactHandler.closest('.newContact'));
                    var contactManage = contactManageList[modalIDContactHandler.text()];
                    var contactObject = contactManage.getContact();
                    contactObject.setPhotoContact($('#avatarNewContact').attr('src'));
                    contactObject.setNameContact($('#contactName').val());
                    contactObject.setFunctionContact($('#contactJobTitle').val());
                    contactObject.setEmailContact($('#emailContact').val());
                    contactObject.setPhoneContact($('#contactTel').val());
                    contactManage.refresh();

                    //contact.setIdContact($('#idContact').val());
                    that.notifyUpdateContact(contactObject);

                    //contactContainer.find('.contactPhoto').attr('src', contact.getPhotoContact());
                }
                modalContactHandler.modal('hide');
            }
        };
        //Add Contact
        this.factoryAddConact = function () {
            that.update = false;
            modalContactHandler.modal();
            $('#addContact').find("#idContact").text("");
            $('#avatarNewContact').attr('src', 'img/avatar/default_avatar.jpg');
            newContactForm[0].reset();
            validatorContact.resetForm();
        };
        //Delete Contact
        this.factoryModalRemoveContact = function (contact) {
            $('#removeContact').modal();
            idDeletecontactHandler.val(contact.getIdContact());
        };
        //Update Contact
        this.factoryUpdateConact = function (contact) {
            that.update = true;
            modalContactHandler.modal();
            validatorContact.resetForm();
            $('#idContact').text(contact.getIdContact());
            $('#contactPhoto').attr('src', contact.getPhotoContact());
            $('#contactName').val(contact.getNameContact());
            $('#contactJobTitle').val(contact.getFunctionContact());
            $('#emailContact').val(contact.getEmailContact());
            $('#contactTel').val(contact.getPhoneContact());
        };

        this.validateNewContactForm = function () {
            $.validator.addMethod("phoneNumbre", function (phone_number, element) {
                phone_number = phone_number.replace(/\s+/g, "");
                return this.optional(element) || phone_number.length > 9 &&
                        (phone_number.match(/\(?\+([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/) || phone_number.match(/\(?(00[0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/));
            }, "Merci de respecter le format indiqué");
            validatorContact = $("#contactForm").validate({
                rules: {
                    contactName: {
                        required: true,
                        minlength: 6
                    },
                    emailContact: {
                        required: true,
                    },
                    contactTel: {
                        phoneNumbre: true,
                        minlength: 10
                    }
                }
            });
        };
        this.updateContactSuccess = function () {
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        this.addContactSuccess = function () {
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        this.deleteContactSuccess = function () {
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };

        this.addListener = function (list) {
            listeners.push(list);
        };

        this.notifyAddContact = function (contact) {
            $.each(listeners, function (i) {
                listeners[i].addContact(contact);
            });
        };

        this.notifyUpdateContact = function (contact) {
            $.each(listeners, function (i) {
                listeners[i].updateContact(contact);
            });
        };

        this.notifyDeleteContact = function (idContact) {
            $.each(listeners, function (i) {
                listeners[i].deleteContact(idContact);
            });
        };

    },
    ContactViewListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }



});
