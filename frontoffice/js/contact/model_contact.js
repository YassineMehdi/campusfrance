jQuery.extend({
    Contact: function (data) {
        var that = this;
        this.idContact = $(data).find('idcontact').text();
        this.photoContact = $(data).find('photo').text();
        this.nameContact = $(data).find('nom').text();
        this.functionContact = $(data).find('jobTitle').text();
        this.emailContact = $(data).find('email').text();
        this.phoneContact = $(data).find('phone').text();

        this.getIdContact = function () {
            return this.idContact;
        };
        this.setIdContact = function (idContact) {
            that.idContact = idContact;
        };
        this.getPhotoContact = function () {
            return this.photoContact;
        };
        this.setPhotoContact = function (photoContact) {
            that.photoContact = photoContact;
        };
        this.getNameContact = function () {
            return this.nameContact;
        };
        this.setNameContact = function (nameContact) {
            that.nameContact = nameContact;
        };
        this.getFunctionContact = function () {
            return this.functionContact;
        };
        this.setFunctionContact = function (functionContact) {
            that.functionContact = functionContact;
        };
        this.getEmailContact = function () {
            return this.emailContact;
        };
        this.setEmailContact = function (emailContact) {
            that.emailContact = emailContact;
        };
        this.getPhoneContact = function () {
            return this.phoneContact;
        };
        this.setPhoneContact = function (phoneContact) {
            that.phoneContact = phoneContact;
        };

        this.xmlData = function () {
            var xml = '<contact>';
            xml += '<email>' + htmlEncode(that.emailContact) + '</email>';
            xml += '<idcontact>' + htmlEncode(that.idContact) + '</idcontact>';
            xml += '<jobTitle>' + htmlEncode(that.functionContact) + '</jobTitle>';
            xml += '<nom>' + htmlEncode(that.nameContact) + '</nom>';
            xml += '<phone>' + htmlEncode(that.phoneContact) + '</phone>';
            xml += '<photo>' + htmlEncode(that.photoContact) + '</photo>';
            xml += '</contact>';
            return xml;
        };
    },
    ContactModel: function () {
        var that = this;
        var listeners = new Array();
        this.initModel = function () {
        	console.log("ContactModel");
            that.getContacts();
        };
        // Contacts
        this.getContactsSuccess = function (xml) {
            //console.log(xml);
            var contacts = new Array();
            $(xml).find("contact").each(function () {
                var contact = new $.Contact($(this));
                contacts.push(contact);
            });
            that.notifyContactsSuccess(contacts);
        };

        this.getContactsError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };
        this.getContacts = function () {
        	//console.log("POST: getContacts");
            genericAjaxCall('GET', staticVars.urlBackEndManager + 'contact?idLang=' + getIdLangParam()+'&idStand='+standId,
                    'application/xml', 'xml', '', that.getContactsSuccess, that.getContactsError);
        };
        // Add Contact
        this.addContactSuccess = function (xml) {
            that.notifyAddContactSuccess();
            that.getContacts();
        };

        this.addContactError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.addContact = function (contact) {
            genericAjaxCall('POST', staticVars.urlBackEndManager
                    + 'contact/add?idLang=' + getIdLangParam()+'&idStand='+standId, 'application/xml', 'xml',
                    contact.xmlData(), that.addContactSuccess, that.addContactError);
        };
        // Update Contact
        this.updateContactSuccess = function (xml) {
            that.notifyUpdateContactSuccess();
            that.getContacts();
        };

        this.updateContactError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.updateContact = function (contact) {
            genericAjaxCall('POST', staticVars.urlBackEndManager
                    + 'contact/update', 'application/xml', 'xml',
                    contact.xmlData(), that.updateContactSuccess, that.updateContactError);
        };
        // Delete Contact
        this.deleteContactSuccess = function (xml) {
            that.notifyDeleteContactSuccess();
            that.getContacts();
        };

        this.deleteContactError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.deleteContact = function (idContact) {
            //$('#contact' + idContact).remove();
            genericAjaxCall('POST', staticVars.urlBackEndManager + 'contact/delete?idContact=' + idContact, 'application/xml', 'xml', '',
                    that.deleteContactSuccess, that.deleteContactError);
        };

        this.addListener = function (list) {
            listeners.push(list);
        };

        this.notifyContactsSuccess = function (contacts) {
            $.each(listeners, function (i) {
                listeners[i].contactsLoad(contacts);
            });
        };
        this.notifyUpdateContactSuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].updateContactSuccess();
            });
        };
        this.notifyAddContactSuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].addContactSuccess();
            });
        };
        this.notifyDeleteContactSuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].deleteContactSuccess();
            });
        };

    },
    ContactModelListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }
});
