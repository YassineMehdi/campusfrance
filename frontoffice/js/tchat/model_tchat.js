jQuery.extend({
    
	TchatModel: function(){
		var that = this;

		var listeners = new Array();
		
		this.initModel = function(){
			that.getEnterprisePInfo();
		};
		
		this.getEnterprisePInfoSuccess = function(data){
			var enterpriseP = new $.EnterpriseP($(data));
			/*if(enterpriseP.getIsAdmin()){
				window.location.replace("backend.html?idLang="+getIdLangParam());
			}*/
			that.notifyLoadEnterprisePInfo(enterpriseP);
		},
		
		this.getEnterprisePInfoError = function(xhr, status, error){
			isSessionExpired(xhr.responseText);
		},
		
		this.getEnterprisePInfo = function(){
//			genericAjaxCall('GET', staticVars.urlBackEnd+'enterprisep/getInfos', 'application/xml', 'xml', 
//					'', that.getEnterprisePInfoSuccess, that.getEnterprisePInfoError);
		},

		this.getCurrentsChatSuccess = function(json){
			that.notifyLoadCurrentsChat(json);
		},
		
		this.getCurrentsChatError = function(xhr, status, error){
			isSessionExpired(xhr.responseText);
		},
		this.getCurrentsChat = function(){
			genericAjaxCall('POST', staticVars.urlBackEnd+'agenda?forChat=true&idLang='+getIdLangParam(),
		    		'application/json', 'json', '', that.getCurrentsChatSuccess, that.getCurrentsChatError);
		},
		this.updateEnterprisePInfoSuccess = function(xml){
			that.notifyUpdateEnterprisePInfoSuccess();
		},
		this.updateEnterprisePInfoError = function(xhr, status, error){
			isSessionExpired(xhr.responseText);
		},
		this.updateEnterprisePInfo = function(enterpriseP){
			genericAjaxCall('POST', staticVars.urlBackEnd+'enterprisep/update/chat',
		    		'application/xml', 'xml', enterpriseP.xmlData(), that.updateEnterprisePInfoSuccess, that.updateEnterprisePInfoError);
		},
		this.notifyLoadEnterprisePInfo = function(enterpriseP){
			$.each(listeners, function(i){			
				listeners[i].loadEnterprisePInfo(enterpriseP);
			});
		};
		this.notifyLoadCurrentsChat = function(json){
			$.each(listeners, function(i){			
				listeners[i].loadCurrentsChat(json);
			});
		};
		this.notifyUpdateEnterprisePInfoSuccess = function(){
			$.each(listeners, function(i){			
				listeners[i].updateEnterprisePInfoSuccess();
			});
		};
		this.addListener = function(list){
			listeners.push(list);
		};
		
	},
		
	ChatModelListener: function(list) {
		if(!list) list = {};
		return $.extend(list);
	},
});
