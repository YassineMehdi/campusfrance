jQuery.extend({
	
	TchatView: function(){
		var that = this;
		var listeners = new Array();
		var chatComponentHandler = $('#chatComponent');
		
		this.initView = function(){
			//factoryChangeChatTab('#settingsLi', '#settingsTab');
			$('#confirmLogout').click(function (e) {
		        $('#popupDisconnect').modal();
		    });

			$('.closePopup').click(function (e) {
		        e.preventDefault();
		        $('.eds-dialog-container').hide();
		        $('.backgroundPopup').hide();
		    });

		    $('#chatSettings a.chat').click(function (e) {
		        e.preventDefault();
		        if (!isSettingsFilled()) {
		            $('.fillSettingsPopup').modal();
		        } else {
		        	if(!isConnectToChat) $('#chatComponent').empty();
		            factoryChangeChatTab('#chatLi', '#chatTab');
		        }
		    });

		    $('#chatSettings a.settings').click(function (e) {
		        e.preventDefault();
	            factoryChangeChatTab('#settingsLi', '#settingsTab');
		    });

		    $('#progressLogoRecruiterUpload,#loadLogoRecruiterError').hide();
			$('#logoRecruiterUpload').fileupload({
		    	beforeSend : function(jqXHR, settings) {
		        	beforeUploadFile('.saveChatSettingsButton', '#progressLogoRecruiterUpload', '#chatPage .filebutton,#loadLogoRecruiterError');
		        },
		        datatype: 'json',
		        cache : false,
		        add: function (e, data) {
		        	var goUpload = true;
		            var uploadFile = data.files[0];
		            var fileName=uploadFile.name;
		    		timeUploadLogoRecruiter=(new Date()).getTime();
		    		data.url = getUrlPostUploadFile(timeUploadLogoRecruiter, fileName);
		            if (!(/\.(gif|jpg|jpeg|tiff|png)$/i).test(fileName)) {
		                goUpload = false;
		            }
		            if (uploadFile.size > MAXFILESIZE) { // 6mb
		                goUpload = false;
		            }
		            if (goUpload == true) {
		                data.submit();
		            }else $('#loadLogoRecruiterError').show();
		        },
		        progressall: function (e, data) {
		            progressBarUploadFile('#progressLogoRecruiterUpload', data, 98);
		        },
		        done: function (e, data) {
		        	var file=data.files[0];
		        	var fileName=file.name;
		        	var hashFileName=getUploadFileName(timeUploadLogoRecruiter, fileName);
		        	console.log("Add logo recruiter chat done, filename :"+fileName+", hashFileName : "+hashFileName);
		        	enterprisePPhoto = getUploadFileUrl(hashFileName);
		        	$('#recruiterPicture').attr('src', enterprisePPhoto);
		    		afterUploadFile('.saveChatSettingsButton', '#chatPage .filebutton', '#progressLogoRecruiterUpload');
		        }
		    });
            //getRecruiterInfosChat();

		    $('.saveChatSettingsButton').click(function () {
		        if ($("#accountForm").valid() && !isDisabled('.saveChatSettingsButton')) {
		        	var enterpriseP = new $.EnterpriseP();
		        	var userProfile = new $.UserProfile();
		        	userProfile.setFirstName($('#recruiterFirstName').val());
					userProfile.setSecondName($('#recruiterLastName').val());
					enterpriseP.setJobTitle($('#recruiterJobTitle').val());
					userProfile.setAvatar($('#recruiterPicture').attr('src'));
					userProfile.setPseudoSkype(htmlEncode($('#recruiterPseudoSkype').val()));
					enterpriseP.setUserProfile(userProfile);
		        	that.notifyUpdateEnterprisePInfo(enterpriseP);
		        }
		    });

		    $('#connectToChatButton').click(function () {
			    if (!FlashDetect.installed){
					var pageHost = ((document.location.protocol == "https:") ? "https://" : "http://"); 
		    			document.write("<a href='http://www.adobe.com/go/getflashplayer' target='_blank'><img src='" 
				    + pageHost + "www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Télécharger Adobe Flash player' /></a>" );
				}else {
				if (!isConnectToChat) {
				    that.notifyGetCurrentsChat();
				    //connectToChatFlash();
				} else {
				    isConnectToChat = false;
				    $('#connectToChatButton').empty();
				    $('#connectToChatButton').append('<i class="fui-chat"></i>'+CONNECTTOCHATLABEL+'');
				    $('#chatComponent').empty();
				}
			}
		    });

            $('#logout').click(function() {
                logout();
            });
            
            validateAccountForm();
		};
		
		this.loadEnterprisePInfo = function(enterpriseP){
			var userProfile = enterpriseP.getUserProfile();
			$('#recruiterFirstName').val(userProfile.getFirstName());
			$('#recruiterLastName').val(userProfile.getSecondName());
			$('#recruiterJobTitle').val(enterpriseP.getJobTitle());
    		$('#recruiterPseudoSkype').val(userProfile.getPseudoSkype());
			if(userProfile.getAvatar() != "") 
				$('#recruiterPicture').attr('src', userProfile.getAvatar());
			if ($('#recruiterFirstName').val() == ""
					|| $('#recruiterLastName').val() == ""
					|| $('#recruiterJobTitle').val() == "") {
				$('#chatSettings li').removeClass('active');
				$('#settingsLi').addClass('active');
				$('.chatTabs').css('visibility', 'hidden').css('height', '0px').css('display', 'none');
				$('#settingsTab').css('visibility', 'visible').css('height', '100%').css('display', 'block');
			} else {
				$('#chatSettings li').removeClass('active');
				$('#chatLi').addClass('active');
				$('.chatTabs').css('visibility', 'hidden').css('height', '0px').css('display', 'none');
				$('#chatTab').css('visibility', 'visible').css('height', '100%').css('display', 'block');
				return true;
			}
			$('#loadingEds').hide();
		},

		this.loadCurrentsChat = function(json){
			chatComponentHandler.empty();
			chatComponentHandler.append('<div id="currentChatsPopup"><select id="currentChatsSelect" style="float: left;width: 150px;" class="form-control"><option value="0">'+PUBLICCHATLABEL+'</option></select></div><button id="currentChatsButton" style="cursor: pointer; margin-left: 20px;" class="btn btn-info sendLabel">'+SENDLABEL+'</button>'); 
			//json=JSON.parse(json);
			chatComponentHandler.find('#currentChatsButton').bind('click', function () {
				connectToChatFlash($('#chatComponent #currentChatsSelect').val());
			});
			$.each(json, function(index, eventInfo){
				chatComponentHandler.find('#currentChatsSelect').append('<option value="'+eventInfo.id+'">'+eventInfo.ack+'</option>');
			});
		},
		
		this.updateEnterprisePInfoSuccess = function(){
			showUpdateSuccess();
		    that.reconnectToChat();
		},

		this.reconnectToChat = function () {
		    if (isConnectToChat) {
		    	tryToReconnect++;
		    	if(tryToReconnect<2) {
		    		isConnectToChat=false;
		    	    $('#connectToChatButton').text(CONNECTTOCHATLABEL);
		    	    $('#connectToChatButton').removeClass("disconnected");
		    		//that.notifyGetCurrentsChat();
		    	}else window.location.reload();
		    }
		},
		
		this.notifyGetCurrentsChat = function(){
			$.each(listeners, function(i){
				listeners[i].getCurrentsChat();
			});
		},
		
		this.notifyUpdateEnterprisePInfo = function(enterpriseP){
			$.each(listeners, function(i){
				listeners[i].updateEnterprisePInfo(enterpriseP);
			});
		},
		
		this.addListener = function(list){
			listeners.push(list);
		};
		
	},
	
	
	ChatViewListener: function(list) {
		if(!list) list = {};
		return $.extend(list);
	}
	
});

