jQuery.extend({
    VideosView: function () {
        var that = this;
        var listeners = new Array();
        var videoCache = [];
        var addVideoBtnHandler = null;
        var adviceCheckboxHandler = null;
        var saveVideoHandler = null;
        var titleAdviceHandler = null;
        var contentAdviceHandler = null;
        var fileinputHandler = null;
        var modalVideoHandler = null;
        var contentHandler = null;
        var loadAdviceErrorHandler = null;
        var progressAdviceHandler = null;
        var btnDeleteVideoHandler = null;
        var videoFormHandler = null;
        var jqXHRVideo = null;


        this.initView = function () {
            traduct();
            contentHandler = $("#mainBackEndContainer");
            modalVideoHandler = $("#addVideo");
            addVideoBtnHandler = $(".addVideoLink");
            adviceCheckboxHandler = $(".choiceBox");
            saveVideoHandler = $("#saveVideo");
            titleAdviceHandler = $("#titleAdvice");
            contentAdviceHandler = $("#contentAdvice");
            fileinputHandler = $(".fileinput-filename");
            loadAdviceErrorHandler = $("#loadAdviceError");
            progressAdviceHandler = $("#progressAdvice");
            videoFormHandler = $("#videoForm");
            btnDeleteVideoHandler = $("#btnDeleteVideo");
            jqXHRVideo = null;
            //file input
            $("#videoUpload").fileupload({
                beforeSend: function (jqXHR, settings) {
                    beforeUploadFile('#saveVideo', '#progressVideo', '#loadVideoError');
                },
                datatype: 'json',
                cache: false,
                add: function (e, data) {
                    var goUpload = true;
                    var uploadFile = data.files[0];
                    var fileName = uploadFile.name;
                    timeUploadVideo = (new Date()).getTime();
                    console.log(timeUploadVideo);
                    data.url = getUrlPostUploadFile(timeUploadVideo, fileName);
                    if (!(/\.(flv|mp4|mov|m4v|f4v|wmv)$/i).test(fileName)) {
                        goUpload = false;

                    }
                    if (uploadFile.size > MAXFILESIZE) {
                        goUpload = false;
                    }
                    if (goUpload == true) {
                        jqXHRAdvice = data.submit();
                    } else
                        $('#loadVideoError').show();
                },
                progressall: function (e, data) {
                    progressBarUploadFile('#progressVideo', data, 99);
                },
                done: function (e, data) {
                    var file = data.files[0];
                    var fileName = file.name;
                    console.log(timeUploadVideo);
                    var hashFileName = getUploadFileName(timeUploadVideo, fileName);
                    console.log("Add Video file done, filename :" + fileName + ", hashFileName : " + hashFileName);
                    $('#loadVideoError').show();
                    $('.fileInfo').text(fileName);
                    $('.fileInfo').attr('href', getUploadFileUrl(hashFileName));
                    afterUploadFile('#saveVideo', '#videoLoaded', '#progressVideo,#loadVideoError');
                }
            });
            //$.wijets().make();
            $('#listVideosTable').dataTable({
                "language": {
                    url: 'plugins/datatables/'+getIdLangParam()+'.json'
                },
                "aaData": [],
                "aaSorting": [],
                "aoColumns": [
                    {
                        "sTitle": TITLELABEL
                    },
                    {
                        "sTitle": CONTENTLABEL
                    },
                    {
                        "sTitle": ACTIONSLABEL
                    }
                ],
                "bFilter": false,
                "bLengthChange": false,
                "bAutoWidth": false,
                "bRetrieve": false,
                "bDestroy": true,
                "iDisplayLength": 10,
                "fnDrawCallback": function () {
                    $('.viewVideoLink').off();
                    $('.viewVideoLink').on("click", function () {
                        var videoId = $(this).closest('div').attr('id');
                        that.factoryViewVideo(videoCache[videoId]);

                    });

                    $('.editVideoLink').off();
                    $('.editVideoLink').on("click", function () {
                        var videoId = $(this).closest('div').attr('id');
                        that.updateVideo(videoCache[videoId]);
                    });

                    $('.deleteVideoLink').off();
                    $('.deleteVideoLink').on("click", function () {

                        var videoId = $(this).closest('div').attr('id');
                        $('#removeVideo').modal();
                        $('#idDeleteVideo').val(videoId);
                    });
                }
            });
            //save video
            saveVideoHandler.unbind("click").bind("click", function (e) {
                that.saveVideo();
            });
            //Choice Upload
            adviceCheckboxHandler.unbind("click").bind("click", function (e) {
                that.factoryChoiceCheckbox();
            });
            //Add video
            addVideoBtnHandler.unbind("click").bind("click", function (e) {
                that.factoryAddVideo();
            });

            //Delete video
            btnDeleteVideoHandler.unbind("click").bind("click", function (e) {
                that.notifyDeleteVideo($('#idDeleteVideo').val());
            });

            //Dismiss video
            $('#addVideo').on('hide.bs.modal', function (e) {
                $('.progress').hide();
                if (jqXHRVideo != null)
                    jqXHRVideo.abort();
            });

        };
        //Save Video
        this.saveVideo = function () {
            that.validateVideoForm();
            if ($("#videoForm").valid()) {
                var video = new $.File();
                video.setIdFile(0);
                video.setFileTitle($('#videoTitle').val());
                video.setFileDescription($('#videoDescription').val());
                if ($('#uploadVideo').is(":checked")) {
                    video.setFileUrl($('.fileInfo').attr('href'));
                    $('.fileInfo').attr('target', '_blanck');
                    video.setFileName($('.fileInfo').text());
                } else {
                    video.setFileUrl($('#videoUrl').val());
                }
                var fileType = new $.FileType();
                fileType.setFileTypeId(fileTypeEnum.VIDEO);
                video.setFileType(fileType);

                if (!that.update) {
                    that.notifyAddVideo(video);
                } else {
                    video.setIdFile($('#idVideo').val());
                    that.notifyUpdateVideo(video);
                }

            }
            ;
        };
        //Add Video
        this.factoryAddVideo = function () {
            that.update = false;
            modalVideoHandler.modal();
            loadAdviceErrorHandler.hide();
            progressAdviceHandler.hide();
            $('.videoUrlDiv').show();
            $('.uploadOptionDiv').hide();
            $('.fileInfo').attr('src');
            $('.fileInfo').text('');
            videoFormHandler[0].reset();
            $("label.error").hide();
            $(".error").removeClass("error");
        };
        this.addPosterSuccess = function () {
            modalVideoHandler.modal('hide');
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        //Choice Upload
        this.factoryChoiceCheckbox = function () {
            if ($('#urlOption').is(":checked")) {
                $('.videoUrlDiv').show();
                $('.uploadOptionDiv').hide();
            } else {
                $('.videoUrlDiv').hide();
                $('.uploadOptionDiv').show();
            }
        };
        this.addVideoSuccess = function () {
            modalVideoHandler.modal('hide');
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        // Delete Poster
        // Update Advice
        this.updateVideo = function (video) {
            that.update = true;
            $('#addVideo').modal();
            videoFormHandler[0].reset();
            $('#idVideo').val(video.getIdFile());
            $('#videoTitle').val(video.getFileTitle());
            $('#videoDescription').val(video.getFileDescription());
            if ((video.getFileName() != undefined) && (video.getFileName() != '')) {
                $('.fileInfo').attr('href', video.getFileUrl());
                $('.fileInfo').text(video.getFileName());
                $('#uploadVideo').click();
            } else {
                $('#videoUrl').val(video.getFileUrl());
                $('#urlOption').click();
            }
            that.factoryChoiceCheckbox();

        };

        this.updateVideoSuccess = function () {
            modalVideoHandler.modal('hide');
            modalVideoHandler.find('#videoForm')[0].reset();
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        // Delete Poster
        this.deleteVideoSuccess = function () {
            modalVideoHandler.modal('hide');
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };

        this.displayVideos = function (videos) {
            videoCache = videos;
            
          //var rightGroup = listStandRights[rightGroupEnum.VIDEOS];
            console.log("in displayVideos***************");
                //var permittedVideosNbr = rightGroup.getRightCount();
                //var videosNbr = Object.keys(videos).length;
                //console.log(videosNbr);
               // if (permittedVideosNbr == -1) {
                    $('#addVideoDiv #addVideoLink').text(ADDLABEL);
                    $('#addVideoDiv').show();
               /* } else if (videosNbr < permittedVideosNbr) {
                    $('#numberVideos').text(videosNbr);
                    $('#numberPermittedVideos').text(permittedVideosNbr);
                    $('#addVideoDiv').show();
                } else {
                    $('#addVideoDiv').hide();
                }*/
            

            var listVideos = [];
            var video = null;
            var videoId = null;
            var videoUrl = null;
            var videoTitle = null;
            var videoDescription = null;
            var videoImage = null;
            var data = null;
            var listActions = null;
            var videoImg = null;
            var videoContent = null;

            for (var index in videos) {
                video = videos[index];
                videoId = video.getIdFile();
                videoUrl = video.getFileUrl();
                videoTitle = video.getFileTitle();
                if (videoTitle == undefined)
                    videoTitle = '';
                videoDescription = video.getFileDescription();
                videoImage = video.getFileImage();
                videoImg = "<img class='apercuImg' src='img/video.png' alt='' />";
                videoContent = "<div class='contentTruncate'>" + videoTitle.replace(/<(?:.|\n)*?>/gm, '') + "</div>";
                listActions = "<div class='actionBtn' id='" + videoId + "'><a href='javascript:void(0);' class='editVideoLink editLink actionBtn'><i class='fa fa-pencil-square-o'></i></a> <a href='javascript:void(0);' class='viewVideoLink viewLink actionBtn'><i class='fa fa-eye'></i></a> <a href='javascript:void(0);' class='deleteVideoLink deleteLink actionBtn'><i class='fa fa-trash-o'></i></a></div>";
                data = [videoImg, videoContent, listActions];
                listVideos.push(data);
            }
            addTableDatas("#listVideosTable", listVideos);
            $('#loadingEds').hide();
        };
        // View Video
        this.factoryViewVideo = function (video) {
            var videoUrl = video.getFileUrl();
            console.log(videoUrl);
            var videoTitle = video.getFileTitle();
            $('#viewVideo').modal();
            $('#videoTitlePopup').text(videoTitle);
            playVideoPopup(videoUrl, videoTitle);


        };
        // Validator form
        this.validateVideoForm = function () {
            $.validator.addMethod("uploadFile", function (val, element) {
                //console.log(val);
                var fileName = $(".fileInfo").text();
                var fileUrl = $(".fileInfo").attr("href");
                if ((fileName == '' || fileUrl == '') && ($('#uploadVideo').is(":checked"))) {
                    return false;
                } else {
                    return true;
                }

            }, "File type error");

            $("#videoForm").validate({
                rules: {
                    videoTitle: {
                        required: true
                    },
                    videoUrl: {
                        required: "#urlOption:checked",
                        url: true
                    }
                }
            });
            $("#videoForm #videoUpload").rules("add", {
                uploadFile: true
            });
        };

        this.addListener = function (list) {
            listeners.push(list);
        };

        this.notifyAddVideo = function (video) {
            $.each(listeners, function (i) {
                listeners[i].addVideo(video);
            });
        };

        this.notifyUpdateVideo = function (video) {
            $.each(listeners, function (i) {
                listeners[i].updateVideo(video);
            });
        };

        this.notifyDeleteVideo = function (videoId) {
            $.each(listeners, function (i) {
                listeners[i].deleteVideo(videoId);
            });
        };

    },
    VideosViewListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }



});
