jQuery.extend({
    VideosController: function (model, view) {
        var that = this;
        /**
         * listen to the view
         */
        var vlist = $.VideosViewListener({
            addVideo: function (video) {
                model.addVideo(video);
            },
            updateVideo: function (video) {
                model.updateVideo(video);
            },
            deleteVideo: function (videoId) {
                model.deleteVideo(videoId);
            }
        });

        view.addListener(vlist);

        /**
         * listen to the model
         */
        var mlist = $.VideosModelListener({
            loadVideos: function (listVideos) {
                view.displayVideos(listVideos);
            },
            addVideoSuccess: function () {
                view.addVideoSuccess();
            },
            updateVideoSuccess: function () {
                view.updateVideoSuccess();
            },
            deleteVideoSuccess: function () {
                view.deleteVideoSuccess();
            }
        });

        model.addListener(mlist);

        this.getAnalyticsVideos = function(callBack, path) {
            model.getAnalyticsVideos(callBack, path);
        };
        this.getVideoById = function(videoId){
            return model.getVideoById(videoId);
        };
        this.initController = function () {
            view.initView();
            model.initModel();
        };
    }
});
