jQuery.extend({
    EnterprisesModel:function(){
    	var that=this;
    	var listeners = new Array();
    	this.initModel=function(){
            that.getStandGroups();
        	that.getLanguages();
    		that.getEnterprises();
    	};
    	this.getEnterprises = function(){
    		genericAjaxCall('GET', staticVars.urlBackEndManager + 'enterprise/list', 'application/xml', 'xml', '',
                    that.getEnterprisesSuccess, that.getEnterprisesError);
    	};
    	this.getEnterprisesSuccess=function(xml){
    		 var listEnterprises=[];
    		 $(xml).find("enterprise").each(function(){
    			 var enterprise=new $.Enterprise($(this));
    			 listEnterprises[enterprise.enterpriseId]=enterprise;
    		 });
    		 that.notifyLoadEnterprises(listEnterprises);
    	};
    	this.getEnterprisesError=function(){
    	};
    	this.addStand=function(stand){
    		//console.log(enterprise);
    		genericAjaxCall('POST', staticVars.urlBackEndManager + 'stand/add', 'application/xml', 'xml', stand.xmlData(),
                    that.addStandSuccess, that.addStandError);
    	};
    	this.addStandSuccess=function(enterprise){
    		that.getEnterprises();
    	};
    	this.addStandError=function(){
    	};
    	/*********************delete enterprise********************************/
    	this.deleteEnterprise=function(enterpriseId){
    		genericAjaxCall('POST', staticVars.urlBackEndManager + 'enterprise/delete?enterpriseId='+enterpriseId, 'application/xml', 'xml', '',
                    that.deleteEnterpriseSuccess, that.deleteEnterpriseError);
    	};
    	this.deleteEnterpriseSuccess=function(){
    		that.getEnterprises();
    	};
    	this.deleteEnterpriseError=function(){
    	};
        this.getStandGroupsSuccess=function(xml){
        	var groupStands=new Array();
        	$(xml).find('groupStandDTO').each(function(){
        		var standGroup=new $.StandGroup($(this));
        		groupStands.push(standGroup);
        	});
        	that.notifyGroupStandSuccess(groupStands);
        };
        this.getStandGroupsError=function(){
        };
        this.getStandGroups=function(){
        	genericAjaxCall('GET', staticVars.urlBackEndManager + 'groupStand/all',
                    'application/xml', 'xml', '', that.getStandGroupsSuccess, that.getStandGroupsError);
        };
        this.getLanguagesSuccess=function(xml){
        	var languages=new Array();
        	$(xml).find("languaget").each(function(){
        		var language = new $.LanguageT($(this));
        		languages.push(language);
        	});
        	that.notifyLanguageSuccess(languages);
        };
        this.getLanguagesError=function(){
        };
        this.getLanguages=function(){
        	genericAjaxCall('GET', staticVars.urlBackEndManager + 'languaget/listLanguages?idLang=' + getIdLangParam(),
                    'application/xml', 'xml', '', that.getLanguagesSuccess, that.getLanguagesError);
        };
    	this.addListener=function(list){
    		listeners.push(list);
    	};
        this.notifyGroupStandSuccess=function(groupStands){
        	$.each(listeners,function(i){
        		listeners[i].groupStandLoad(groupStands);
        	});
        };
    	this.notifyLoadEnterprises=function(listEnterprises){
        	$.each(listeners,function(i){
        		listeners[i].loadEnterprises(listEnterprises);
        	});
    	};
        this.notifyLanguageSuccess=function(languages){
        	$.each(listeners,function(i){
        		listeners[i].standLanguagesLoad(languages);
        	});
        };
    },
    EnterprisesModelListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    },
    
});