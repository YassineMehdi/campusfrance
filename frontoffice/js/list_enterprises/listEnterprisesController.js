jQuery.extend({
    EnterprisesController: function (model, view) {
        var that = this;
        /**
         * listen to the view
         */
        var vlist = $.EnterprisesViewListener({
        	addStand:function(stand){
	        	model.addStand(stand);
	        },
	        deleteEnterprise:function(stand){
	        	model.deleteEnterprise(stand);
	        }
        });
        view.addListener(vlist);

        /**
         * listen to the model
         */
        var mlist = $.EnterprisesModelListener({
        	loadEnterprises:function(list){
        		view.displayEnterprises(list);	
	        },
            groupStandLoad:function(groupStands){
            	view.groupStandLoad(groupStands);
            },
            standLanguagesLoad:function(languages){
            	view.loadLanguages(languages);
            }
        });

        model.addListener(mlist);

        this.initController = function () {
            view.initView();
            model.initModel();
        };
    }
});