jQuery
		.extend({
			EnterprisesView : function() {
				var that = this;
				var listeners = new Array();
				var addEnterpriseBtnHandler = null;
				var modalPosterHandler = null;
				var siegeRadioButton = null;
				var enterpriseNameHandler = null;
				var website = null;
				var adresse = null;
				var contactEmail = null;
				var dateCreation = null;
				var lieu = null;
				var nbEmploye = null;
				var postalCode = null;
				var nomSiege = null;
				var addressSiege = null;
				var dateCreationSiege = null;
				var lieuSiege = null;
				var nbEmployeSiege = null;
				var postalCodeSiege = null;
				var saveEnterpriseBtnHandler = null;
				var editBtnHandler = null;
				var deleteBtnHandler = null;
				var avatarNewEnterpriseHandler = null;
				var enterpriseNameHandler = null;
				var standNameHandler = null;
				var creationDateHandler = null;
				var enterpriseAddressHandler = null;
				var postalCodeHandler = null;
				var enterprisePlaceHandler = null;
				var webSiteHandler = null;
				var careerWebsiteHandler = null;
				var contactEmailHandler = null;
				var employeeNumberHandler = null;
				var groupStandHandler = null;
				var languageStandHandler = null;
				var siegeNameHandler = null;
				var siegeAddressHandler = null;
				var siegePostalCodeHandler = null;
				var siegePlaceHandler = null;
				var siegeCreationDateHandler = null;
				var siegeNbremployeeHandler = null;

				var tabsHandler = null;
				var validatorEnterprise = null;
				var newEnterpriseForm = null;
				this.initView = function() {
					/**
					 * **********************Add enterprise
					 * form**********************************
					 */
					// enterprise data
					avatarNewEnterpriseHandler = $("#addEnterprise #avatarNewEnterprise");
					enterpriseNameHandler = $("#addEnterprise #enterpriseName");
					standNameHandler = $("#addEnterprise #standName");
					creationDateHandler = $("#addEnterprise #creationDate");
					enterpriseAddressHandler = $("#addEnterprise #enterpriseAddress");
					postalCodeHandler = $("#addEnterprise #postalCode");
					enterprisePlaceHandler = $("#addEnterprise #enterprisePlace");
					webSiteHandler = $("#addEnterprise #webSite");
					careerWebsiteHandler = $("#addEnterprise #careerWebsite");
					contactEmailHandler = $("#addEnterprise #contactEmail");
					employeeNumberHandler = $("#addEnterprise #employeeNumber");
					groupStandHandler = $("#addEnterprise #groupStand");
					languageStandHandler = $("#addEnterprise #languageStand");

					// siege data
					siegeNameHandler = $("#addEnterprise #siegeName");
					siegeAddressHandler = $("#addEnterprise #siegeAddress");
					siegePostalCodeHandler = $("#addEnterprise #siegePostalCode");
					siegePlaceHandler = $("#addEnterprise #siegePlace");
					siegeCreationDateHandler = $("#addEnterprise #siegeCreationDate");
					siegeNbremployeeHandler = $("#addEnterprise #siegeNbremployee");

					// Other Component
					enterprises = $("#enterprises_table");
					addEnterpriseBtnHandler = $(".addEnterpriseLabel");
					modalPosterHandler = $("#addEnterprise");
					saveEnterpriseBtnHandler = $("#saveEnterprise");
					newEnterpriseForm = $("#enterpriseForm");
					siegeRadioButton = $("input[type=radio][name=siegeRadioButton]");
					siegeRadioButton.change(function() {
						if (this.value == "oui") {
							siegeNameHandler.val(enterpriseNameHandler.val());
							siegeAddressHandler.val(enterpriseAddressHandler.val());
							siegeCreationDateHandler.val(creationDateHandler.val());
							siegePlaceHandler.val(enterprisePlaceHandler.val());
							siegeNbremployeeHandler.val(employeeNumberHandler.val());
							siegePostalCodeHandler.val(postalCodeHandler.val());
						} else {
							siegeNameHandler.val("");
							siegeAddressHandler.val("");
							siegeCreationDateHandler.val("");
							siegePlaceHandler.val("");
							siegeNbremployeeHandler.val("");
							siegePostalCodeHandler.val("");
						}
					});
					/**
					 * *************************************end form
					 * enterprise*******************************************************
					 */
					tabsHandler = $("#myTab li");
					addEnterpriseBtnHandler.unbind("click").bind("click",
							function(e) {
								that.factoryAddStand();
							});
					saveEnterpriseBtnHandler.unbind("click").bind("click",
							function(e) {
								that.factorySaveStand();
							});
					$('#enterprises_table')
							.dataTable(
									{
										"language" : {
											url : 'plugins/datatables/'+ getIdLangParam() + '.json'
										},
										"aaData" : [],
										"aaSorting" : [],
										"aoColumns" : [
												{
													"sTitle" : "Identifiant"
												},
												{
													"sTitle" : "Nom d'entreprise"
												},
												{
													"sTitle" : "Adresse d'entreprise"
												},
												{
													"sTitle" : "Site web d'enterprise"
												},
												{
													"sTitle" : "standId",
													"visible" : false
												},
												{
													"sTitle" : "Action",
													"mData" : null,
													"bSortable" : false,
													"mRender" : function(data,
															type, full) {
														return '<a class="btn btn-info btn-sm edit" href="#" standId=' + full[4]
																+ ' enterpriseId=' + full[0] + ' enterpriseName=' + htmlEncode(full[1])
																+ '>' + 'Edit' + '</a>  <a class="btn btn-danger btn-sm delete" href="#" enterpriseId='
																+ full[0] + '>'	+ 'delete' + '</a>';
													}
												} ],
										"bFilter" : false,
										"bLengthChange" : false,
										"bAutoWidth" : false,
										"bRetrieve" : false,
										"bDestroy" : true,
										"iDisplayLength" : 10,
										"fnDrawCallback" : function(oSettings) {
											// edit and delete buttons Handler
											that.factoryEditEnterprise();
											that.factoryDeleteEnterprise();
										}
									});
					that.validateNewEnterpriseForm();
					$('#uploadAvatar')
							.fileupload(
									{
										beforeSend : function(jqXHR, settings) {
											beforeUploadFile(
													'.saveStandButton',
													'#progressAvatarUpload',
													'#avatarUploadEnterprise .filebutton, .loadError');
										},
										datatype : 'json',
										cache : false,
										add : function(e, data) {
											var goUpload = true;
											var uploadFile = data.files[0];
											var fileName = uploadFile.name;
											timeUploadLogo = (new Date())
													.getTime();
											data.url = getUrlPostUploadFile(
													timeUploadLogo, fileName);
											if (!(/\.(gif|jpg|jpeg|tiff|png)$/i)
													.test(fileName)) {
												goUpload = false;
											}
											if (uploadFile.size > MAXFILESIZE) {// 6mb
												goUpload = false;
											}
											if (goUpload == true) {
												data.submit();
											} else
												$('.loadError').show();
										},
										progressall : function(e, data) {
											progressBarUploadFile(
													'#progressAvatarUpload',
													data, 100);
										},
										done : function(e, data) {
											var file = data.files[0];
											var fileName = file.name;
											var hashFileName = getUploadFileName(
													timeUploadLogo, fileName);
											console
													.log("Add logo stand file done, filename :"
															+ fileName
															+ ", hashFileName : "
															+ hashFileName);
											var avatarEnterpriseP = getUploadFileUrl(hashFileName);
											$('#avatarNewEnterprise').attr(
													'src', avatarEnterpriseP);
											afterUploadFile(
													'.saveStandButton',
													'#avatarUploadEnterprise .filebutton',
													'#progressAvatarUpload');
										}
									});
					for (var i = 1850; i <= new Date().getFullYear(); i++)
		                addOption("#addEnterprise .creationDate", i, i);
				};
				this.factoryEditEnterprise = function() {
					editBtnHandler = $(".edit");
					editBtnHandler.unbind("click").bind("click", function(e) {
						enterpriseId = $(this).attr("enterpriseId");
						standId = $(this).attr("standId");
						$(".editEnterprise").show();
						$(".listEnterprises").hide();
						$(".profile").trigger("click");
					});
				};

				this.factoryDeleteEnterprise = function() {
					deleteBtnHandler = $(".delete");
					deleteBtnHandler.unbind("click").bind(
							"click",
							function(e) {
								that.notifyDeleteEnterprise($(this).attr(
										"enterpriseId"));
							});
				};

				this.factoryAddStand = function() {
					modalPosterHandler.modal();
					$('#avatarNewEnterprise').attr('src', 'img/avatar/default_avatar.jpg');
					$("#enterpriseForm")[0].reset();
					validatorEnterprise.resetForm();
				};
				this.factorySaveStand = function() {
					if (newEnterpriseForm.valid()) {
						var stand = new $.Stand();
						var enterprise = new $.Enterprise();
			            var standGroup = new $.StandGroup();
			            var language = new $.LanguageT();
						// console.log("lieu********"+lieu.val());
						enterprise.setEnterpriseId(0);
						enterprise.setEnterpriseName(enterpriseNameHandler.val());
						enterprise.setDatecreation(creationDateHandler.val());
						enterprise.setNbremploye(employeeNumberHandler.val());
						enterprise.setPostalCode(postalCodeHandler.val());
						enterprise.setWebsite(webSiteHandler.val());
						enterprise.setCareerwebsite(careerWebsiteHandler.val());
						//enterprise.setSlogan(slogan.val());
						enterprise.setLogo(avatarNewEnterpriseHandler.attr('src'));
						enterprise.setPlace(enterprisePlaceHandler.val());
						enterprise.setContactEmail(contactEmailHandler.val());
						enterprise.setAddress(enterpriseAddressHandler.val());

						enterprise.setSiegeName(siegeNameHandler.val());
						enterprise.setSiegeAddress(siegeAddressHandler.val());
						enterprise.setSiegePlace(siegePlaceHandler.val());
						enterprise.setSiegeCreationDate(siegeCreationDateHandler.val());
						enterprise.setSiegePostalCode(siegePostalCodeHandler.val());
						enterprise.setSiegeNbremploye(siegeNbremployeeHandler.val());
						
			            stand.setStandName(standNameHandler.val());
						stand.setEntreprise(enterprise);
			            standGroup.setGroupId(groupStandHandler.val());
		            	stand.setStandGroup(standGroup);
			            language.setIdLangage(languageStandHandler.val());
		            	stand.setLanguage(language);
						that.notifyAddStand(stand);
						modalPosterHandler.modal('hide');
					}
				};
				this.displayEnterprises = function(enterprises) {
					var enterpriseList = [];
					var enterprise = null;
					enterpriseId = null;
					enterpriseName = null;
					enterpriseAddress = null;
					enterpriseWebsite = null;
					enterpriseContactEmail = null;
					enterpriseDatecreation = null;
					var standId = null;
					for ( var index in enterprises) {
						enterprise = enterprises[index];
						enterpriseId = enterprise.getEnterpriseId();
						enterpriseName = enterprise.getEnterpriseName();
						enterpriseAddress = enterprise.getAddress();
						enterpriseWebsite = enterprise.getWebsite();
						enterpriseContactEmail = enterprise
								.getContactEmail();
						enterpriseDatecreation = enterprise
								.getDatecreation();
						standId = enterprise.getStandId();
						// console.log("stand**************");
						// console.log(enterprise.getStandId());
						data = [ enterpriseId, enterpriseName,
								enterpriseAddress, enterpriseWebsite, standId ];
						enterpriseList.push(data);
					}
					// edit and delete buttons Handler

					addTableDatas("#enterprises_table", enterpriseList);
					// console.log(enterpriseList);
					// console.log("Add DATA Tables*******************");

				};
				this.validateNewEnterpriseForm = function() {
					validatorEnterprise = $("#enterpriseForm").validate({
						rules : {
		                    enterpriseName: {
		                        required: true,
		                        minlength: 3
		                    },
		                    standName: {
		                        required: true
		                    },
		                    enterpriseAddress: {
		                        required: true
		                    },
		                    postalCode: {
		                        required: true
		                    },
		                    enterprisePlace: {
		                        required: true
		                    },
		                    webSite: {
		                        url: true,
		                        required: true
		                    },
		                    contactEmail: {
		                        required: true
		                    },
		                    siegeName: {
		                        required: true
		                    },
		                    siegeAddress: {
		                        required: true
		                    },
		                    siegePostalCode: {
		                        required: true
		                    },
		                    siegePlace: {
		                        required: true
		                    },
		                    languageStand:{
		                    	required: true
		                    },
		                    groupStand:{
		                    	required: true
		                    }
						}
					});
				};
		         //Group Stand load
		        this.groupStandLoad=function(groupStands){
		       	 	var standGroup = null;
		       	 	groupStandHandler.empty();
			        $.each(groupStands,function(i){
			        	standGroup = groupStands[i];
			        	groupStandHandler.append("<option value='"+standGroup.getGroupId()+"' id_group="+standGroup.getGroupId()+">"+htmlEncode(standGroup.getGroupName())+"</option>");
			        });
		        };
		        //load languages
		        this.loadLanguages=function(languages){
		        	 var language = null;
		        	 languageStandHandler.empty();
		        	 $.each(languages,function(i){
		        		language = languages[i]; 
		             	languageStandHandler.append("<option value='"+language.getIdLangage()+"' id_language="+language.getIdLangage()+">"+htmlEncode(language.getName())+"</option>");
		             });
		        };
				this.notifyAddStand = function(enterprise) {
					$.each(listeners, function(i) {
						listeners[i].addStand(enterprise);
					});
				};
				this.notifyDeleteEnterprise = function(enterprise) {
					$.each(listeners, function(i) {
						listeners[i].deleteEnterprise(enterprise);
					});
				};
				this.addListener = function(list) {
					listeners.push(list);
				};
			},
			EnterprisesViewListener : function(list) {
				if (!list)
					list = {};
				return $.extend(list);
			}
		});