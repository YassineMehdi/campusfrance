jQuery.extend({
    DocumentsModel: function () {
        var that = this;
        var listeners = new Array();
        this.initModel = function () {
            that.getStandDocuments();
        };
        // lister Poster
        this.getStandDocumentsSuccess = function (xml) {
            var listDocuments = [];
            $(xml).find("allfiles").each(function () {
                var document = new $.File($(this));
                listDocuments[document.idFile] = document;
            });
            that.notifyLoadDocuments(listDocuments);

        };

        this.getStandDocumentsError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.getStandDocuments = function () {
            genericAjaxCall('GET', staticVars.urlBackEndManager + 'allfiles/standDocuments?idLang=' + getIdLangParam()+'&idStand='+standId, 'application/xml', 'xml', '',
                    that.getStandDocumentsSuccess, that.getStandDocumentsError);
        };
        // Add Poster
        this.addDocumentSuccess = function (xml) {
            that.notifyAddDocumentSuccess();
            that.getStandDocuments();
        };

        this.addDocumentError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.addDocument = function (photo) {
            genericAjaxCall('POST', staticVars.urlBackEndManager + 'allfiles/addFile?idLang=' + getIdLangParam()+'&idStand='+standId, 'application/xml', 'xml', photo.xmlData(),
                    that.addDocumentSuccess, that.addDocumentError);
        };
//        // Update Advice
        this.updateDocumentSuccess = function (xml) {
            that.notifyUpdateDocumentSuccess();
            that.getStandDocuments();
        };

        this.updateDocumentError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.updateDocument = function (document) {
            genericAjaxCall('POST', staticVars.urlBackEndManager
                    + 'allfiles/updateFile?idLang=' + getIdLangParam()+'&idStand='+standId, 'application/xml', 'xml', document.xmlData(),
                    that.updateDocumentSuccess, that.updateDocumentError);
        };
//        // Delete Advice
        this.deleteDocumentSuccess = function () {
            that.notifyDeleteDocumentSuccess();
            that.getStandDocuments();
        };

        this.deleteDocumentError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.deleteDocument = function (documentId) {
            genericAjaxCall('GET', staticVars.urlBackEndManager
                    + 'allfiles/deleteMedia/' + documentId, 'application/xml', 'xml', '',
                    that.deleteDocumentSuccess, that.deleteDocumentError);
        };

        this.getAnalyticsDocuments = function(callBack, path) {
            genericAjaxCall('GET', staticVars.urlBackEndManager + path,
                    'application/xml', 'xml', '', function(xml) {
                        listDocumentsCache = [];
                        var document = null;
                        $(xml).find("allfiles").each(function() {
                            document = new $.Document($(this));
                            listDocumentsCache[document.idDocument] = document;
                        });
                        callBack();
                    }, that.getStandDocumentsError);
        };
        
        this.getDocumentById = function(documentId) {
            return listDocumentsCache[documentId];
        };

        this.addListener = function (list) {
            listeners.push(list);
        };
        this.notifyLoadDocuments = function (listDocuments) {
            $.each(listeners, function (i) {
                listeners[i].loadDocuments(listDocuments);
            });
        };
        this.notifyAddDocumentSuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].addDocumentSuccess();
            });
        };
        this.notifyDeleteDocumentSuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].deleteDocumentSuccess();
            });
        };
        this.notifyUpdateDocumentSuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].updateDocumentSuccess();
            });
        };

    },
    DocumentsModelListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }
});
