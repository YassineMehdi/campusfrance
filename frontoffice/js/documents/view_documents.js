jQuery.extend({
    DocumentsView: function () {
        var that = this;
        var listeners = new Array();
        var documentCache = [];
        var jqXHRPoster = null;
        var addDocBtnHandler = null;
        var modalDocHandler = null;
        var saveDocHandler = null;
        var btnDeleteDocHandler = null;
        var docCheckboxHandler = null;
        var jqXHRPhoto = null;

        var contentHandler = null;
        var loadAdviceErrorHandler = null;
        var progressAdviceHandler = null;
        var docFormHandler = null;
        var fileinputHandler = null;
        var titlePosterHandler = null;


        this.initView = function () {
            traduct();
            contentHandler = $("#mainBackEndContainer");
            addDocBtnHandler = $(".addDocument");
            modalDocHandler = $("#addDocument");
            saveDocHandler = $("#saveDocument");
            docCheckboxHandler = $(".choiceBox");
            btnDeleteDocHandler = $("#btnDeleteDocument");

            loadAdviceErrorHandler = $("#loadPosterError");
            progressAdviceHandler = $("#progressPoster");
            docFormHandler = $("#documentForm");
            titlePosterHandler = $("#titlePoster");
            fileinputHandler = $(".fileinput-filename");


            jqXHRPoster = null;

            //Choice Upload
            docCheckboxHandler.unbind("click").bind("click", function (e) {
                that.factoryChoiceCheckbox();
            });
            //Add Photo
            addDocBtnHandler.unbind("click").bind("click", function (e) {
                that.factoryAddDoc();
            });
            //save Photo
            saveDocHandler.unbind("click").bind("click", function (e) {
                that.saveDocument();
            });
            //Delete Photo
            btnDeleteDocHandler.unbind("click").bind("click", function (e) {
                that.notifyDeleteDocument($('#idDeleteDocument').val());
            });
            //file input
            $("#documentUpload").fileupload({
                beforeSend: function (jqXHR, settings) {
                    beforeUploadFile('#saveDocument', '#progressDocument', '#loadDocumentError');
                },
                datatype: 'json',
                cache: false,
                add: function (e, data) {
                    var goUpload = true;
                    var uploadFile = data.files[0];
                    var fileName = uploadFile.name;
                    timeUploadPoster = (new Date()).getTime();
                    data.url = getUrlPostUploadFile(timeUploadDocument, fileName) + "&isPoster=true";
                    if (!(/\.(pdf)$/i).test(fileName)) {
                        goUpload = false;
                    }
                    if (uploadFile.size > MAXFILESIZE) {
                        goUpload = false;
                    }
                    if (goUpload == true) {
                        jqXHRPoster = data.submit();
                    } else
                        $('#loadDocumentError').show();
                },
                progressall: function (e, data) {
                    progressBarUploadFile('#progressDocument', data, 99);
                },
                done: function (e, data) {
                    var file = data.files[0];
                    var fileName = file.name;
                    var hashFileName = getUploadFileName(timeUploadDocument, fileName);
                    console.log("Add Document file done, filename :" + fileName + ", hashFileName : " + hashFileName);
                    $('#loadDocumentError').show();
                    $('#loadDocument .error').hide();
                    $('.fileInfo').text(fileName);
                    $('.fileInfo').attr('href', getUploadFileUrl(hashFileName));
                    afterUploadFile('#saveDocument', '#documentLoaded', '#progressDocument,#loadDocumentError');
                }
            });


            //datatable
            $('#listDocumentsTable').dataTable({
                "language": {
                    url: 'plugins/datatables/'+getIdLangParam()+'.json'
                },
                "aaData": [],
                "aaSorting": [],
                "aoColumns": [
                    {
                        "sTitle": TITLELABEL
                    },
                    {
                        "sTitle": CONTENTLABEL
                    },
                    {
                        "sTitle": ACTIONSLABEL
                    }
                ],
                "bFilter": false,
                "bLengthChange": false,
                "bAutoWidth": false,
                "bRetrieve": false,
                "bDestroy": true,
                "iDisplayLength": 10,
                "fnDrawCallback": function () {
                    $('.viewDocumentLink').off();
                    $('.viewDocumentLink').on("click", function () {
                        var documentId = $(this).closest('div').attr('id');
                        that.factoryViewDocument(documentCache[documentId]);

                    });
//
                    $('.editDocumentLink').off();
                    $('.editDocumentLink').on("click", function () {
                        var documentId = $(this).closest('div').attr('id');
                        that.factoryUpdateDocument(documentCache[documentId]);
                    });
//
                    $('.deleteDocumentLink').off();
                    $('.deleteDocumentLink').on("click", function () {
                        var documentId = $(this).closest('div').attr('id');
                        $('#removeDocument').modal();
                        $('#idDeleteDocument').val(documentId);
                    });
                }
            });

            //Dismiss video
            $('#addDocument').on('hide.bs.modal', function (e) {
                $('.progress').hide();
                if (jqXHRPhoto != null)
                    jqXHRPhoto.abort();
            });
        };
        //Choice Upload
        this.factoryChoiceCheckbox = function () {
            if ($('#urlOptionDoc').is(":checked")) {
                $('.urlDiv').show();
                $('.uploadDiv').hide();
            } else {
                $('.urlDiv').hide();
                $('.uploadDiv').show();
            }
        };
        this.displayDocuments = function (documents) {
            documentCache = documents;

          //  var rightGroup = listStandRights[rightGroupEnum.DOCUMENTS];
            //if (rightGroup != null) {
              //  var permittedDocumentsNbr = rightGroup.getRightCount();
                //var documentsNbr = Object.keys(documents).length;
                //if (permittedDocumentsNbr == -1) {
                    $('#addDocumentDiv #addDocumentLink').text(ADDLABEL);
                    $('#addDocumentDiv').show();
                /*} else if (documentsNbr < permittedDocumentsNbr) {
                    $('#numberDocuments').text(documentsNbr);
                    $('#numberPermittedDocuments').text(permittedDocumentsNbr);
                    $('#addDocumentDiv').show();
                } else {
                    $('#addDocumentDiv').hide();
                }
            }*/

            var documentList = [];
            var document = null;
            var documentId = null;
            var documentUrl = null;
            var documentTitle = null;
            var documentDescription = null;
            var documentImage = null;
            var data = null;
            var listActions = null;
            var documentImg = null;
            var documentContent = null;

            for (var index in documents) {
                document = documents[index];
                documentId = document.getIdFile();
                documentUrl = document.getFileUrl();
                documentTitle = document.getFileTitle();
                documentDescription = document.getFileDescription();
                documentImage = document.getFileImage();
                documentImg = "<img class='apercuImg' src='img/video.png' alt='' />";
                documentContent = "<div class='contentTruncate'>" + documentTitle.replace(/<(?:.|\n)*?>/gm, '') + "</div>";
                listActions = "<div class='actionBtn' id='" + documentId + "'><a href='javascript:void(0);' class='editDocumentLink editLink actionBtn'><i class='fa fa-pencil-square-o'></i></a> <a href='javascript:void(0);' class='viewDocumentLink viewLink actionBtn'><i class='fa fa-eye'></i></a> <a href='javascript:void(0);' class='deleteDocumentLink deleteLink actionBtn'><i class='fa fa-trash-o'></i></a></div>";
                data = [documentImg, documentContent, listActions];
                documentList.push(data);
            }
            addTableDatas("#listDocumentsTable", documentList);
            $('#loadingEds').hide();
        };
        //Add Documents
        this.factoryAddDoc = function () {
            that.update = false;
            modalDocHandler.modal();
            loadAdviceErrorHandler.hide();
            progressAdviceHandler.hide();
            $('.fileInfo').attr('src');
            $('.fileInfo').text('');
            docFormHandler[0].reset();
        };
        this.addDocumentSuccess = function () {
            modalDocHandler.modal('hide');
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        //Save Poster
        this.saveDocument = function () {
            that.validateDocumentForm();
            if ($("#documentForm").valid()) {
                var document = new $.File();
                document.setIdFile(0);
                document.setFileTitle($('#documentTitle').val());
                document.setFileDescription($('#documentDescription').val());
                document.setFileImage(document.getFileUrl());

                var fileType = new $.FileType();
                fileType.setFileTypeId(fileTypeEnum.DOCUMENT);
                document.setFileType(fileType);

                if ($('#urlOptionDoc').is(":checked")) {
                    var urlDocument = $('#documentUrl').val();
                    $('#loadDocumentValidError').hide();
                    if (getExtension(urlDocument) == "pdf") {
                        genericAjaxCall(
                                'GET',
                                staticVars.urlServerUpload + "?url=" + urlDocument, 'text/plain', 'text', '',
                                function (filename) {
                                    console.log("Response : " + filename);
                                    if (filename != 'ko') {
                                        var urlDocument = staticVars.urlServerStorage + filename;
                                        document.setFileUrl(urlDocument);
                                        document.setFileName(filename);
                                        if (!that.update) {
                                            that.notifyAddDocument(document);

                                        }
                                        else {
                                            document.setIdFile($('#idDocument').val());
                                            that.notifyUpdateDocument(document);
                                        }
                                    } else {
                                        $('#loadDocumentValidError').show();
                                        enabledButton('#saveDocument');
                                        //$('#loadingEds').hide();
                                    }
                                },
                                function (xml) {
                                    $('#loadDocumentValidError').show();
                                    enabledButton('.saveDocumentButton');
                                    $('#loadingEds').hide();
                                });
                    } else {
                        $('#loadDocumentValidError').show();
                        enabledButton('.saveDocumentButton');
                        $('#loadingEds').hide();
                    }

                } else {
                    $('.fileInfo').attr('target', '_blanck');
                    document.setFileUrl($('.fileInfo').attr('href'));
                    document.setFileName($('.fileInfo').text());
                    if (!that.update) {
                        that.notifyAddDocument(document);
                    }
                    else {
                        document.setIdFile($('#idDocument').val());
                        that.notifyUpdateDocument(document);
                    }
                }

            }
            ;
        };
        // Update Advice
        this.factoryUpdateDocument = function (document) {
            that.update = true;
            modalDocHandler.modal();
            $('#idDocument').val(document.getIdFile());
            $('#documentTitle').val(document.getFileTitle());
            $('#documentDescription').val(document.getFileDescription());
            if ((document.getFileName() != undefined) && (document.getFileName() != '')) {
                $('.fileInfo').attr('target', '_blanck');
                $('.fileInfo').attr('href', document.getFileUrl());
                $('.fileInfo').text(document.getFileName());
                $('#uploadVideoDoc').click();
            } else {
                $('#documentUrl').val(document.getFileUrl());
                $('#urlOptionDoc').click();
            }
            that.factoryChoiceCheckbox();
        };

        this.updateDocumentSuccess = function () {
            modalDocHandler.modal('hide');
            modalDocHandler.find('#documentForm')[0].reset();
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        // View Advice
        this.factoryViewDocument = function (photo) {
            var documentUrl = photo.getFileUrl();
            var documentTitle = photo.getFileTitle();
            $('#viewDocument').modal();
            $('#titleDocument').text(documentTitle);
            var img_container = $('<object data="mozilla-pdf.js/web/viewer.html?file=' + documentUrl + '#locale=' + localePDFJS + '" type="text/html" width="100%" height="100%"></object>');
            $('#contentViewDocument').html(img_container);


        };
        // Delete Poster
        this.deleteDocumentSuccess = function () {
            modalDocHandler.modal('hide');
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        // Validator form
        this.validateDocumentForm = function () {
            $.validator.addMethod("uploadFile", function (val, element) {
                //console.log(val);
                var fileName = $(".fileInfo").text();
                var fileUrl = $(".fileInfo").attr("href");
                if ((fileName == '' || fileUrl == '') && ($('#uploadVideoDoc').is(":checked"))) {
                    return false;
                } else {
                    return true;
                }

            }, "File type error");

            $("#documentForm").validate({
                rules: {
                    documentTitle: {
                        required: true
                    },
                    documentUrl: {
                        required: "#urlOptionDoc:checked",
                        url: true
                    }
                }
            });
            $("#documentUpload").rules("add", {
                uploadFile: true
            });
        };
        this.addListener = function (list) {
            listeners.push(list);
        };
        this.notifyAddDocument = function (document) {
            $.each(listeners, function (i) {
                listeners[i].addDocument(document);
            });
        };
        this.notifyDeleteDocument = function (documentId) {
            $.each(listeners, function (i) {
                listeners[i].deleteDocument(documentId);
            });
        };
        this.notifyUpdateDocument = function (document) {
            $.each(listeners, function (i) {
                listeners[i].updateDocument(document);
            });
        };

    },
    DocumentsViewListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }



});
