jQuery.extend({
    PhotosController: function (model, view) {
        var that = this;
        /**
         * listen to the view
         */
        var vlist = $.PhotosViewListener({
            addPhoto: function (photo) {
                model.addPhoto(photo);
            },
            deletePhoto: function (photoId) {
                model.deletePhoto(photoId);
            },
            updatePhoto: function (photo) {
                model.updatePhoto(photo);
            },
        });

        view.addListener(vlist);

        /**
         * listen to the model
         */
        var mlist = $.PhotosModelListener({
            loadPhotos: function (listPhotos) {
                view.displayPhotos(listPhotos);
            },
            addPhotoSuccess: function () {
                view.addPhotoSuccess();
            },
            deletePhotoSuccess: function () {
                view.deletePhotoSuccess();
            },
            updatePhotoSuccess: function (photo) {
                view.updatePhotoSuccess(photo);
            }
        });

        model.addListener(mlist);

        this.getAnalyticsPhotos = function(callBack, path){
            model.getAnalyticsPhotos(callBack, path);
        };
        
        this.getPhotoById = function(photoId){
            return model.getPhotoById(photoId);
        };

        this.initController = function () {
        	console.log("init controller photos***********");
            view.initView();
            model.initModel();
        };
    }
});
