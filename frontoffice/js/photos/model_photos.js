jQuery.extend({
    PhotosModel: function () {
        var that = this;
        var listeners = new Array();
        this.initModel = function () {
        	console.log("initModel Photos***********");
           that.getStandPhotos();
        };
        // lister Poster
        this.getStandPhotosSuccess = function (xml) {
            var listPhotos = [];
            $(xml).find("allfiles").each(function () {
                var photo = new $.File($(this));
                listPhotos[photo.idFile] = photo;
            });
            that.notifyLoadPhotos(listPhotos);

        };

        this.getStandPhotosError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.getStandPhotos = function () {
            //$('#listStandPosters').empty();
            genericAjaxCall('GET', staticVars.urlBackEndManager + 'allfiles/standPhotos?idLang=' + getIdLangParam()+'&idStand='+standId, 'application/xml', 'xml', '',
                    that.getStandPhotosSuccess, that.getStandPhotosError);
        };
        // Add Poster
        this.addPhotoSuccess = function (xml) {
            that.notifyAddPhotoSuccess();
            that.getStandPhotos();
        };

        this.addPhotoError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.addPhoto = function (photo) {
            genericAjaxCall('POST', staticVars.urlBackEndManager + 'allfiles/addFile?idLang=' + getIdLangParam()+'&idStand='+standId, 'application/xml', 'xml', photo.xmlData(),
                    that.addPhotoSuccess, that.addPhotoError);
        };
//        // Update Advice
        this.updatePhotoSuccess = function (xml) {
            that.notifyUpdatePhotoSuccess();
            that.getStandPhotos();
        };

        this.updatePhotoError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.updatePhoto = function (photo) {
            genericAjaxCall('POST', staticVars.urlBackEndManager
                    + 'allfiles/updateFile?idLang=' + getIdLangParam()+'&idStand='+standId, 'application/xml', 'xml', photo.xmlData(),
                    that.updatePhotoSuccess, that.updatePhotoError);
        };
//        // Delete Advice
        this.deletePhotoSuccess = function () {
            that.notifyDeletePhotoSuccess();
            that.getStandPhotos();
        };

        this.deletePhotoError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.deletePhoto = function (photoId) {
            genericAjaxCall('GET', staticVars.urlBackEndManager
                    + 'allfiles/deleteMedia/' + photoId, 'application/xml', 'xml', '',
                    that.deletePhotoSuccess, that.deletePhotoError);
        };

        this.getAnalyticsPhotos = function(callBack, path) {
            // console.log('getAnalyticsPhotos : '+path);
            genericAjaxCall('GET', staticVars.urlBackEndManager
                    + path, 'application/xml', 'xml', '',
                    function(xml){
                        listPhotosCache = new Array();
                        $(xml).find("allfiles").each(function() {
                            var photo = new $.Photo($(this));
                            listPhotosCache[photo.idPhoto]=photo;
                        });
                        callBack();
                    }, that.getStandPhotosError);
        };

        this.getPhotoById = function(photoId) {
            return listPhotosCache[photoId];
        };

        this.addListener = function (list) {
            listeners.push(list);
        };
        this.notifyLoadPhotos = function (listPhotos) {
            $.each(listeners, function (i) {
                listeners[i].loadPhotos(listPhotos);
            });
        };
        this.notifyAddPhotoSuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].addPhotoSuccess();
            });
        };
        this.notifyDeletePhotoSuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].deletePhotoSuccess();
            });
        };
        this.notifyUpdatePhotoSuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].updatePhotoSuccess();
            });
        };

    },
    PhotosModelListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }
});
