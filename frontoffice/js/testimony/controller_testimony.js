jQuery.extend({
    TestimonyController: function (model, view) {
        var that = this;
        /**
         * listen to the view
         */
        var vlist = $.TestimonyViewListener({
            addTestimony: function (testimony) {
                model.addTestimony(testimony);
            },
            updateTestimony: function (testimony) {
                model.updateTestimony(testimony);
            },
            deleteTestimony: function (testimonyId) {
                model.deleteTestimony(testimonyId);
            }

        });

        view.addListener(vlist);

        /**
         * listen to the model
         */
        var mlist = $.TestimonyModelListener({
            loadTestimonies: function (listTestimonies) {
                view.displayTestimony(listTestimonies);
            },
            addTestimonySuccess: function () {
                view.addTestimonySuccess();
            },
            updateTestimonySuccess: function () {
                view.updateTestimonySuccess();
            },
            deleteTestimonySuccess: function () {
                view.deleteTestimonySuccess();
            }
        });

        model.addListener(mlist);

        this.initController = function () {
            view.initView();
            model.initModel();
        }
        ;
    }
});
