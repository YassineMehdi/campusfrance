jQuery.extend({
    TestimonyView: function () {
        var that = this;
        var listeners = new Array();
        var contentHandler = null;
        var testimonyCache = [];
        var addTestimonyBtnHandler = null;
        var modalTestimonyHandler = null;
        var testimonyFormHandler = null;
        var saveTestimonyHandler = null;
        var btnDeleteTestimonyHandler = null;
        var validator = null;



        var loadAdviceErrorHandler = null;
        var progressAdviceHandler = null;
        var fileinputHandler = null;
        var titlePosterHandler = null;
        var jqXHRPoster = null;

        this.initView = function () {
            traduct();
            contentHandler = $("#mainBackEndContainer");
            addTestimonyBtnHandler = $(".addTestimonyLink");
            modalTestimonyHandler = $("#addTestimony");
            testimonyFormHandler = $("#testimonyForm");
            saveTestimonyHandler = $("#saveTestimony");
            btnDeleteTestimonyHandler = $("#btnDeleteTestimony");


            loadAdviceErrorHandler = $("#loadPosterError");
            progressAdviceHandler = $("#progressPoster");
            titlePosterHandler = $("#titlePoster");
            fileinputHandler = $(".fileinput-filename");

            jqXHRPoster = null;


            //Add Advice
            addTestimonyBtnHandler.unbind("click").bind("click", function (e) {
                that.factoryAddTestimony();
            });
            //save advice
            saveTestimonyHandler.unbind("click").bind("click", function (e) {
                that.saveTestimony();
            });
            //Delete Advice
            btnDeleteTestimonyHandler.unbind("click").bind("click", function (e) {
                that.notifyDeleteTestimony($('#idDeleteTestimony').val());
            });
            //Add Advice file upload avatar
            $('#uploadAvatar').fileupload({
                beforeSend: function (jqXHR, settings) {
                    beforeUploadFile('.saveStandButton', '#progressAvatarUpload', '#avatarUploadContact .filebutton, .loadError');
                },
                datatype: 'json',
                cache: false,
                add: function (e, data) {
                    var goUpload = true;
                    var uploadFile = data.files[0];
                    var fileName = uploadFile.name;
                    timeUploadLogo = (new Date()).getTime();
                    data.url = getUrlPostUploadFile(timeUploadLogo, fileName);
                    if (!(/\.(gif|jpg|jpeg|tiff|png)$/i).test(fileName)) {
                        goUpload = false;
                    }
                    if (uploadFile.size > MAXFILESIZE) {// 6mb
                        goUpload = false;
                    }
                    if (goUpload == true) {
                        data.submit();
                    } else
                        $('.loadError').show();
                },
                progressall: function (e, data) {
                    progressBarUploadFile('#progressAvatarUpload', data, 100);
                },
                done: function (e, data) {
                    var file = data.files[0];
                    var fileName = file.name;
                    var hashFileName = getUploadFileName(timeUploadLogo, fileName);
                    //console.log("Add logo stand file done, filename :"+fileName+", hashFileName : "+hashFileName);
                    var avatarContact = getUploadFileUrl(hashFileName);
                    $('#avatarTestimony').attr('src', avatarContact);
                    afterUploadFile('.saveStandButton', '#avatarUploadContact .filebutton', '#progressAvatarUpload');
                }
            });


            //datatable
            var oTable = $('#listTestimonyTable').dataTable({
                "language": {
                    url: 'plugins/datatables/'+getIdLangParam()+'.json'
                },
                "aaData": [],
                "aaSorting": [],
                "aoColumns": [
                    {
                        "sTitle": PHOTOLABEL
                    },
                    {
                        "sTitle": FIRSTANDLASTNAMELABEL
                    },
                    {
                        "sTitle": JOBTITLELABEL
                    },
                    {
                        "sTitle": TESTIMONYLABEL
                    },
                    {
                        "sTitle": ACTIONSLABEL
                    }
                ],
                "bFilter": false,
                "bAutoWidth": false,
                "bRetrieve": false,
                "bDestroy": true,
                "iDisplayLength": 10,
                "fnDrawCallback": function () {
                    $('.viewTestimonyLink').off('click').on("click", function () {
                        var testimonyId = $(this).closest('div').attr('id');
                        that.factoryViewTestimony(testimonyCache[testimonyId]);

                    });
//
                    $('.editTestimonyLink').off('click').on("click", function () {
                        var testimonyId = $(this).closest('div').attr('id');
                        that.factoryUpdateTestimony(testimonyCache[testimonyId]);
                    });

                    $('.deleteTestimonyLink').off('click').on("click", function () {
                        var testimonyId = $(this).closest('div').attr('id');
                        $('#removeTestimony').modal();
                        $('#idDeleteTestimony').val(testimonyId);
                    });
                }
            });

            /*var tableTools = new $.fn.dataTable.TableTools(oTable, {
                "sSwfPath": "assets/plugins/datatables/TableTools/swf/copy_csv_xls_pdf.swf"
            });
            //DOM Manipulation to move datatable elements integrate to panel
            /*$('#panel-tabletools .panel-ctrls').append($('.dataTables_filter').addClass("pull-right"));
            $('#panel-tabletools .panel-ctrls .dataTables_filter').find("label").addClass("panel-ctrls-center");

            $('#panel-tabletools .panel-ctrls').append("<i class='separator pull-right '></i>");

            $('#panel-tabletools .panel-ctrls').append($('.dataTables_length').addClass("pull-right"));
            $('#panel-tabletools .panel-ctrls .dataTables_length label').addClass("mb0");

            $('#panel-tabletools .panel-ctrls').append("<i class='separator pull-right '></i>");
            $(tableTools.fnContainer()).appendTo('#panel-tabletools .panel-ctrls').addClass("pull-right mt10");*/

            //$('#panel-tabletools .panel-footer').append($(".dataTable+.row"));
            //$('.dataTables_paginate>ul.pagination').addClass("pull-right");
        };
        this.displayTestimony = function (testimonys) {
            testimonyCache = testimonys;

            //var rightGroup = listStandRights[rightGroupEnum.TESTIMONIES];
            //if (rightGroup != null) {
               // var permittedTestimoniesNbr = rightGroup.getRightCount();
                //var testimoniesNbr = Object.keys(testimonys).length;
                //if (permittedTestimoniesNbr == -1) {
                    $('#addTestimoniesDiv #addTestimonyLink').text(ADDLABEL);
                    $('#addTestimoniesDiv').show();
                /*} else if (testimoniesNbr < permittedTestimoniesNbr) {
                    $('#numberTestimonies').text(testimoniesNbr);
                    $('#numberPermittedTestimonies').text(permittedTestimoniesNbr);
                    $('#addTestimoniesDiv').show();
                } else {
                    $('#addTestimoniesDiv').hide();
                }
            }*/

            var testimonyList = [];
            var testimony = null;
            var testimonyId = null;
            var testimonyJob = null;
            var testimonyName = null;
            var testimonyImage = null;
            var testimonyContent = null;
            var data = null;

            var listActions = null;
            var testimonyImg = null;
            var testimonyNameT = null;
            var testimonyJobT = null;
            var testimonyContentDes = null;

            for (var index in testimonys) {
                testimony = testimonys[index];
                testimonyId = testimony.getTestimonyId();
                testimonyJob = testimony.getJobTitle();
                testimonyName = testimony.getName();
                testimonyImage = testimony.getPhoto();
                testimonyContent = testimony.getTestimonyContent();

                testimonyImg = "<img class='apercuImg' src='" + testimonyImage + "' alt='' />";
                testimonyNameT = "<div class='contentTruncate'>" + testimonyName.replace(/<(?:.|\n)*?>/gm, '') + "</div>";
                testimonyJobT = "<div class='contentTruncate'>" + testimonyJob.replace(/<(?:.|\n)*?>/gm, '') + "</div>";
                testimonyContentDes = "<div class='contentTruncate'>" + testimonyContent.replace(/<(?:.|\n)*?>/gm, '') + "</div>";
                listActions = "<div class='actionBtn' id='" + testimonyId + "'><a href='javascript:void(0);' class='editTestimonyLink editLink actionBtn'><i class='fa fa-pencil-square-o'></i></a> <a href='javascript:void(0);' class='viewTestimonyLink viewLink actionBtn'><i class='fa fa-eye'></i></a> <a href='javascript:void(0);' class='deleteTestimonyLink deleteLink actionBtn'><i class='fa fa-trash-o'></i></a></div>";

                data = [testimonyImg, testimonyNameT, testimonyJobT, testimonyContentDes, listActions];
                testimonyList.push(data);
            }
            addTableDatas("#listTestimonyTable", testimonyList);
            // function to truncate text
            $('.contentTruncate').expander({
                slicePoint: 80, // default is 100
                expandPrefix: ' ', // default is '... '
                expandText: '[...]', // default is 'read more'
                //collapseTimer:    5000, // re-collapses after 5 seconds; default is 0, so no re-collapsing
                userCollapseText: '[^]'  // default is 'read less'
            });
            $('#loadingEds').hide();
        };
        //Add Testimony
        this.factoryAddTestimony = function () {
            that.update = false;
            modalTestimonyHandler.modal();
            loadAdviceErrorHandler.hide();
            progressAdviceHandler.hide();
            $('.fileInfo').attr('src');
            $('.fileInfo').text('');
            $('#avatarTestimony').attr('src', 'img/avatar/default_avatar.jpg');
            testimonyFormHandler[0].reset();
            if(validator != null)
                validator.resetForm();
        };
        this.addTestimonySuccess = function () {
            modalTestimonyHandler.modal('hide');
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        //Save Testimony
        this.saveTestimony = function () {
            that.validateTestimonyForm();
            if ($("#testimonyForm").valid()) {
                var testimony = new $.Testimony();
                testimony.setTestimonyId(0);
                testimony.setPhoto($('#avatarTestimony').attr('src'));
                testimony.setName($('#testimonyName').val());
                testimony.setJobTitle($('#testimonyJobTitle').val());
                testimony.setTestimonyContent($('#testimonyContent').val());

                if (!that.update) {
                    that.notifyAddTestimony(testimony);
                }
                else {
                    testimony.setTestimonyId($('#idTestimony').val());
                    that.notifyUpdateTestimony(testimony);
                }

            }
            ;
        };
        // Update Testimony
        this.factoryUpdateTestimony = function (testimony) {
            that.update = true;
            $('#addTestimony').modal();
            $('#idTestimony').val(testimony.getTestimonyId());
            $('#avatarTestimony').attr('src', testimony.getPhoto());
            $('#testimonyName').val(testimony.getName());
            $('#testimonyJobTitle').val(testimony.getJobTitle());
            $('#testimonyContent').val(testimony.getTestimonyContent());

        };

        this.updateTestimonySuccess = function () {
            modalTestimonyHandler.modal('hide');
            modalTestimonyHandler.find('#testimonyForm')[0].reset();
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        // View Testimony
        this.factoryViewTestimony = function (testimony) {
            var testimonyName = testimony.getName();
            var testimonyJobTitle = testimony.getJobTitle();
            var testimonyContent = testimony.getTestimonyContent();
            $('#viewTestimony').modal();
            $('#testimonyPhotoView').attr('src', testimony.getPhoto());
            $('#testimonyNameView').text(testimonyName);
            $('#testimonyJobTitleView').text(testimonyJobTitle);
            $('#testimonyContentView').text(testimonyContent);
        };
        // Delete Testimony
        this.deleteTestimonySuccess = function () {
            modalTestimonyHandler.modal('hide');
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        // Validator form
        this.validateTestimonyForm = function () {
            validator = $("#testimonyForm").validate({
                rules: {
                    testimonyName: {
                        required: true
                    },
                    testimonyJobTitle: {
                        required: true
                    },
                    testimonyContent: {
                        required: true
                    }
                }
            });
        };
        this.addListener = function (list) {
            listeners.push(list);
        };
        this.notifyAddTestimony = function (testimony) {
            $.each(listeners, function (i) {
                listeners[i].addTestimony(testimony);
            });
        };
        this.notifyDeleteTestimony = function (testimonyId) {
            $.each(listeners, function (i) {
                listeners[i].deleteTestimony(testimonyId);
            });
        };
        this.notifyUpdateTestimony = function (testimony) {
            $.each(listeners, function (i) {
                listeners[i].updateTestimony(testimony);
            });
        };

    },
    TestimonyViewListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }



});
