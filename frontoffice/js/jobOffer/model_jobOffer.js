jQuery.extend({
    JobOfferModel: function () {
        var that = this;
        var listeners = new Array();
        this.initModel = function () {
            validateJobOfferForm();
            getActivitySectors();
            getRegions();
            getStudyLevels();
            getFunctionsCandidate();
            getExperienceYears();
            that.getStandJobOffers();
        };

        this.deleteJobOfferSuccess = function() {
            that.getStandJobOffers();
            that.notifyJobOfferDeleted();
        };

        this.deleteJobOfferError = function(xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.deleteJobOffer = function(jobOfferId) {
            genericAjaxCall('POST', staticVars.urlBackEnd
                    + 'jobOffer/deleteJobOffer', 'application/xml', 'xml',
                    jobOfferId, that.deleteJobOfferSuccess, that.deleteJobOfferError);
        };

        this.updateJobofferSuccess = function() {
            $('#addPhotoModal .btn-danger').click();
            that.getStandJobOffers();
            infoMessage('#jobOfferUpdated');
        };

        this.updateJobofferError = function(xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.updateJoboffer = function(jobOfferXml) {
            genericAjaxCall('POST', staticVars.urlBackEnd
                    + 'jobOffer/updateJobOffer', 'application/xml', 'xml',
                    jobOfferXml, that.updateJobofferSuccess, that.updateJobofferError);
        };
        
        this.addJobofferSuccess = function() {
            listJobOffersCache = [];
            $('#addPhotoModal .btn-danger').click();
            that.getStandJobOffers();
            infoMessage('#jobOfferAdded');
        };

        this.getAnalyticsJobOffers = function(callBack, path) {
            // console.log('getAnalyticsJobOffers : '+path);
            listJobOffersCache = [];
            genericAjaxCall('GET', staticVars.urlBackEnd + path,
                    'application/xml', 'xml', '', function(xml) {
                        $(xml).find("joboffer").each(function() {
                            var jobOffer = new $.JobOffer($(this));
                            listJobOffersCache[jobOffer.getJobId()] = jobOffer;
                        });
                        callBack();
                    }, that.getStandJobOffersError);
        };

        this.getJobOfferById = function(jobOfferId) {
            return listJobOffersCache[jobOfferId];
        };

        this.addJobofferError = function(xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.addJoboffer = function(jobOfferXml) {
            genericAjaxCall('POST', staticVars.urlBackEnd
                    + 'jobOffer/addJobOffer?idLang=' + getIdLangParam(),
                    'application/xml', 'xml', jobOfferXml, that.addJobofferSuccess,
                    that.addJobofferError);
        };
        this.getStandJobOffersSuccess = function (xml) {
            var jobOffers = [];
            $(xml).find("joboffer").each(function () {
                var jobOffer = new $.JobOffer($(this));
                jobOffers[jobOffer.getJobId()] = jobOffer;
            });
            that.notifyLoadJobOffers(jobOffers);
        };

        this.getStandJobOffersError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.getStandJobOffers = function () {
            genericAjaxCall('GET', staticVars.urlBackEnd
                    + 'jobOffer/standJobOffers?idLang=' + getIdLangParam(), 'application/xml', 'xml', '',
                    that.getStandJobOffersSuccess, that.getStandJobOffersError);
        };
				
        this.addListener = function (list) {
            listeners.push(list);
        };

        this.notifyLoadJobOffers = function (jobOffers) {
            $.each(listeners, function (i) {
                listeners[i].loadJobOffers(jobOffers);
            });
        };

        this.notifyAddJobOfferSuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].addJobOfferSuccess();
            });
        };

        this.notifyJobOfferSaved = function() {
            $.each(listeners, function(i) {
                listeners[i].jobOfferSaved();
            });
        };

        this.notifyJobOfferDeleted = function() {
            $.each(listeners, function(i) {
                listeners[i].jobOfferDeleted();
            });
        };
    },
    JobOfferModelListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }
});

