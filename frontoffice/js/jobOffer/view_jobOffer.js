jQuery.extend({
    JobOfferView: function () {
        var that = this;
        var listeners = new Array();
        var $jobCreationForm = null;
        var $editorJobOffer = null;
        var jobOfferCache = [];
        var isUpdate = false;
        var idJobOfferToChange = 0;

        this.initView = function () {
            /*************************************************************************************
             JobOffers
             *************************************************************************************/
            //datatable
            traduct();
            $jobCreationForm = $('#jobOfferForm');
            $jobTitle = $('#jobTitle');
            $editorJobOffer = $("#jobDescription");
            $jobUrl = $('#jobUrl');
            $jobSkills = $('#jobSkills');
            $jobUrlDiv = $('#jobUrlDiv');
            $externalUrl = $('#externalUrl');

            //Custom validate
            that.validateJobOfferForm();

            var oTableJobOffers = $('#listJobOffersTable').dataTable({
                "language": {
                    url: 'plugins/datatables/'+getIdLangParam()+'.json'
                },
                "aaData": [],
                "aoColumns": [{
                        "sTitle": TITLELABEL, "sWidth": "35%"
                    },
                    {
                        "sTitle": DESCRIPTIONLABEL, "sWidth": "50%"
                    }, {
                        "sTitle": ACTIONSLABEL, "sWidth": "15%"
                    }, {
                        "sTitle": "id job", "sWidth": "0%"
                    }],
                "aaSorting": [[3, "desc"]],
                "aoColumnDefs": [{"bVisible": false, "aTargets": [3]}],
                "bAutoWidth": false,
                "bRetrieve": false,
                "bDestroy": true,
                "iDisplayLength": 10,
                "bFilter": false,
                "fnDrawCallback": function () {
                    $('.viewJobOffer').off();
                    $('.viewJobOffer').on("click", function () {
                        var jobOfferId = $(this).closest('div').attr('index');
                        that.factoryViewJobOffer(jobOfferCache[jobOfferId]);
                    });

                    $('.editJobOffer').off();
                    $('.editJobOffer').on("click", function () {
                        var jobOfferId = $(this).closest('div').attr('index');
                        $('#jobUrl').val("");
                        that.factoryUpdateJobOffer(jobOfferCache[jobOfferId]);
                    });

                    $('.deleteJobOffer').off();
                    $('.deleteJobOffer').on("click", function () {
                        var jobOfferId = $(this).closest('div').attr('index');
                        that.idJobOfferToChange = jobOfferId;
                        $('#popupDeleteJobOffer').modal();
                    });
                }
            });

            that.toolsTable(oTableJobOffers, '#jobOffersContent');
            $("#externalUrl").change(function() {
                console.log('change');
                if($("#externalUrl").is(":checked")){
                    $("#jobUrl").prop('disabled', false);
                }else{
                    $("#jobUrl").prop('disabled', true);
                }
            }); 

            $('#confirmDeleteJob').click(function() {
                that.notifyDeleteJobOffer(that.idJobOfferToChange);
            });

            $("#addJobOfferLink").unbind("click").bind("click", function() {
                that.fillCombobox();
				$('#jobTitle').val("");
                $('#jobSkills').val("");
                $('#studyLevelSelect').val("");
                $('#functionSelect').val("");
                $('#experienceYearsSelect').val("");
                $('#jobDescription').text("");
                $('#jobUrl').text("");

                $("#externalUrl").prop("checked",false)
                $("#jobUrl").prop('disabled', true);
                $('#jobUrl').val("");

                $("#addPhotoModal").modal("show");  
			});

            $("#saveJobOffer").click(function() {
                if ($jobCreationForm.valid() && !isDisabled('#saveJob')) {
                    disabledButton('#saveJob');
                    $.cookie("isJobOffer", true);
                    var jobTitle = $jobTitle.val().trim();
                    var jobDescription = $editorJobOffer.val().trim();
                    var jobSkills = $jobSkills.val().trim();
                    var internalOffer = true;
                    var jobUrl = " ";
                    if ($externalUrl.is(':checked')){
                        internalOffer = false;
                        jobUrl = $jobUrl.val().trim();
                    }
                    var sectXml = "";
                    var cantonXml = "";
                    $($('#activitySectorSelect').val()).each(
                            function() {
                                console.log($(this)[0]);
                                sectXml = sectXml + "<secteurActDTO><idsecteur>"
                                        + $(this)[0]
                                        + "</idsecteur></secteurActDTO>";
                            });
                    $($('#cantonSelect').val()).each(
                            function() {
                                console.log($(this)[0]);
                                cantonXml = cantonXml + "<regionDTO><idRegion>"
                                        + $(this)[0]
                                        + "</idRegion></regionDTO>";
                            });

                    var jobOfferXml = "<internalApplication>"
                            + internalOffer
                            + "</internalApplication><secteurActs>"
                            + sectXml
                            + "</secteurActs><regions>"
                            + cantonXml
                            + "</regions><studyLevelDTO><idStudyLevel>"
                            + $('#studyLevelSelect').val()
                            + "</idStudyLevel></studyLevelDTO><functionCandidateDTO><idFunction>"
                            + $('#functionSelect').val()
                            + "</idFunction></functionCandidateDTO><experienceYearsDTO><idExperienceYears>"
                            + $('#experienceYearsSelect').val()
                            + "</idExperienceYears></experienceYearsDTO><title>"
                            + htmlEncode(jobTitle) + "</title>><description>"
                            + escape(jobDescription) + "</description><url>"
                            + htmlEncode(jobUrl) + "</url><tags>"
                            + htmlEncode(jobSkills) + "</tags>";

                    if (that.isUpdate) {
                        var languageId = parseInt(getIdLangParam());
                        jobOfferXml = "<jobOffer><languageId>" + languageId
                            + "</languageId><idmetier>"
                            + that.idJobOfferToChange + "</idmetier>"
                            + jobOfferXml + "</jobOffer>";
                        that.notifyUpdateJobClicked(jobOfferXml);
                    } else {
                        jobOfferXml = "<jobOffer>" + jobOfferXml
                                + "</jobOffer>";
                        that.notifySaveJobClicked(jobOfferXml);
                    }
                }
            });
        };

        this.validateJobOfferForm = function () {
            $("#jobOfferForm").validate({
                rules: {
                    jobUrl: {
                        url: true,
                        required: true
                    },
                }
            });
        };

        this.factoryViewJobOffer = function (jobOffer) {
            console.log(jobOffer);
            $('#jobOfferTitle').text(jobOffer.getJobTitle());
            $('#studyLevelSelectView').text(replaceSpecialChars(jobOffer.getStudyLevelName()));
            $('#activitySectorSelectView').text(replaceSpecialChars(jobOffer.getNameSectors()));
            $('#cantonSelectView').text(replaceSpecialChars(jobOffer.getRegionNames()));
            $('#functionSelectView').text(replaceSpecialChars(jobOffer.getFunctionCandidateName()));
            $('#experienceYearsSelectView').text(replaceSpecialChars(jobOffer.getExperienceYearsName()));

            if(jobOffer.getJobSkills() != ""){
                $('#jobSkillsView').text(replaceSpecialChars(jobOffer.getJobSkills()));
            }else{
                $('#jobSkillsView').closest('.form-group').hide()
            }

            if(jobOffer.getJobUrl() != ""){
                $('#jobUrlView').text(replaceSpecialChars(jobOffer.getJobUrl()));
            }else{
                $('#jobUrlView').closest('.form-group').hide()
            }

            if(jobOffer.getJobDescription() != ""){
                $('#jobDescriptionView').text(replaceSpecialChars(unescape(jobOffer.getJobDescription())));
            }else{
                $('#jobDescriptionView').closest('.form-group').hide()
            }

            console.log(jobOffer.getJobDescription());

            $('#popupJobOfferView').modal();
        };

        this.factoryUpdateJobOffer = function (jobOffer) {
            that.isUpdate = true;
            $("#addPhotoModal").modal("show");  
            that.fillCombobox();
            that.idJobOfferToChange = jobOffer.getJobId();

            $('#jobTitle').val(jobOffer.getJobTitle());
            $('#jobSkills').val(jobOffer.getJobSkills());
            $('#studyLevelSelect').val(jobOffer.getStudyLevel().getIdStudyLevel());
            $('#functionSelect').val(jobOffer.getFunctionCandidate().getIdFunction());
            $('#experienceYearsSelect').val(jobOffer.getExperienceYears().getIdExperienceYears());
            $('#jobDescription').text(jobOffer.getJobDescription());

            if(jobOffer.getJobUrl() != ""){
                $("#externalUrl").prop("checked",true)
                $("#jobUrl").prop('disabled', false);
                $('#jobUrl').val(jobOffer.getJobUrl());
            }else{
                $("#externalUrl").prop("checked",false)
                $("#jobUrl").prop('disabled', true);
                $('#jobUrl').text("");
            }

            var jobSectors = [];
            for (var i = 0; i < jobOffer.getSectors().length; i++) {
                jobSectors.push(jobOffer.getSectors()[i].getIdsecteur());
            }

            $('#activitySectorSelect').val(jobSectors);

            var jobConton = [];
            for (var i = 0; i < jobOffer.getRegions().length; i++) {
                jobSectors.push(jobOffer.getRegions()[i].getIdRegion());
            }

            $('#cantonSelect').val(jobSectors);
        };

        this.toolsTable = function (oTable, parent) {
            var tableTools = new $.fn.dataTable.TableTools(oTable, {
                "buttons": [
                    //{"type": "print", "buttonText": "Print me!"}
                ],
                "sSwfPath": "assets/plugins/datatables/TableTools/swf/copy_csv_xls_pdf.swf"
            });
            /*DOM Manipulation to move datatable elements integrate to panel
            $(parent).find('#panel-tabletools .panel-ctrls').append($(parent).find('.dataTables_filter').addClass("pull-right"));
            $(parent).find('#panel-tabletools .panel-ctrls .dataTables_filter').find("label").addClass("panel-ctrls-center");

            $(parent).find('#panel-tabletools .panel-ctrls').append("<i class='separator pull-right '></i>");

            $(parent).find('#panel-tabletools .panel-ctrls').append($(parent).find('.dataTables_length').addClass("pull-right"));
            $(parent).find('#panel-tabletools .panel-ctrls .dataTables_length label').addClass("mb0");

            $(parent).find('#panel-tabletools .panel-ctrls').append("<i class='separator pull-right '></i>");
            $(tableTools.fnContainer()).appendTo($(parent).find('#panel-tabletools .panel-ctrls')).addClass("pull-right mt10");

            $(parent).find('#panel-tabletools .panel-footer').append($(".dataTable+.row"));
            $(parent).find('.dataTables_paginate>ul.pagination').addClass("pull-right");*/
        };

        this.displayJobOffers = function(listJobOffers){ 
        	datas = [];
            jobOfferCache = listJobOffers;
        	var btnEditHandler = $("<a href='javascript:void(0);' class='editJobOffer editLink actionBtn'><i class='fa fa-pencil-square-o'></i></a> ");
            var btnViewHandler = $("<a href='javascript:void(0);' class='viewJobOffer viewLink actionBtn'><i class='fa fa-eye'></i></a>");
            var btnDeleteHandler = $("<a href='javascript:void(0);' class='deleteJobOffer deleteLink actionBtn'><i class='fa fa-trash-o'></i></a>");
            var listActions = $("<div></div>");
            var jobOffer = null;
            var languages = null;
            var jobOfferId = null;
            var jobOfferTitle = null;
            var jobDescription = null;
					
            for (var index in listJobOffers) {
				jobOffer = listJobOffers[index];
				languages = jobOffer.getLanguagesId();
				jobOfferId = jobOffer.getJobId();
				jobOfferTitle = jobOffer.getJobTitle();
				jobDescription = jobOffer.getJobDescription();
				jobDescription = "<div class='contentTruncate'>" + jobDescription + "</div>";
				btnEditHandler.attr("index", index);
				btnViewHandler.attr("index", index);
				btnDeleteHandler.attr("index", index);
            
				if (languages[getIdLangParam()] != null) {
	               listActions.html(getHtml(btnViewHandler)
                  + getHtml(btnEditHandler)
                  + getHtml(btnDeleteHandler));
                } else {
	               listActions.html(getHtml(btnViewHandler)
                  + getHtml(btnDeleteHandler));
				}

                listActions.attr("index", index);
				var data = [ jobOfferTitle,jobDescription,
				             getHtml(listActions), jobOfferId ];
				datas.push(data);
			}
            addTableDatas('#listJobOffersTable', datas);
            $('#loadingEds').hide();
        };

        this.fillCombobox = function() {
            fillActivitySectors();
            fillRegions();
            fillStudyLevels();
            fillFunctionsCandidate();
            fillExperienceYears();
        };

        this.jobOfferDeleted = function() {
            $('#popupDeleteJobOffer .btn-danger').click();
            infoMessage('#jobOfferDeleted');
        };

        this.notifySaveJobClicked= function(jobOfferXml) {
            $.each(listeners, function(i) {
                listeners[i].saveJob(jobOfferXml);
            });
        };

        this.notifyUpdateJobClicked= function(jobOfferXml) {
            $.each(listeners, function(i) {
                listeners[i].updateJob(jobOfferXml);
            });
        };

        this.notifyDeleteJobOffer = function(jobOfferId) {
            $.each(listeners, function(i) {
                listeners[i].deleteJobOffer(jobOfferId);
            });
        };

        this.addListener = function (list) {
            listeners.push(list);
        };

    }
    ,
    JobOfferViewListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }

});

