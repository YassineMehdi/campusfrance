
jQuery.extend({
	
	SurveysManage : function(surveys, surveyContainer, view){
		var dom = surveyContainer;
		var that=this;
		var surveyManage = null;
		
		this.getSurveys = function(){
			return surveys;
		},
		
		this.refresh = function(){
			for(var index in surveys.getSurveys()){
				surveyManage = new $.SurveyManage(surveys.getSurveys()[index], view);
				$("#survey").append(surveyManage.getDOM());
			}
			$("#survey").append('<button type="button" class="btn btn-block btn-primary btn-cons" id="createNewSurvey"><i class="fa fa-file mr5"></i><span class="addLabel"> Ajouter</span> <span class="surveysLabel"></span></button>');

			$("#survey").find('#createNewSurvey').unbind();
			$("#survey").find('#createNewSurvey').bind('click', function() {
				surveyManage = new $.SurveyManage(null, view);
				$("#survey").find('#createNewSurvey').before(surveyManage.getDOM());
				traduct();
			});
			traduct();
		},

		this.getDOM = function(){ 
			return dom;
		},
		
		this.refresh();
	},
	
	SurveyManage : function(survey, view){
		var that=this;
		var inputType = null;
		var formId = '0';
		var formTitle = '<span class="addLabel"></span> <span class="titleLabel"></span>';
		var surveyTitle = "";
		var collapseId = Math.floor((Math.random() * 10000000) + 1);
		if(survey!=null && survey!=undefined && survey!=0){
			formId = survey.getFormId();
			surveyTitle = survey.getFormTitle();
			formTitle   = survey.getFormTitle();
		}
		var dom = $('<div class="accordion-group">'
						+'<div class="accordion-heading">'
							+'<div class="surveyId" style="display: none;">'+formId+'</div>'
							+'<div class="switch" style="height: 45px;border-bottom: 1px solid #E5E5E5;">'
								+'<span class="surveyTitle1" style="margin-left: 20px;top: 12px;position: relative;">'+formTitle+'</span>'
								+'<a class="deleteSurvey fa fa-trash-o" id="removeSurvey_'+formId+'"></a><span data-toggle="collapse" data-target="#surveyCollapse_'+collapseId+'" class="cmn-toggle-title-surveys fa fa-plus" for="cmn-toggle-surveys"></span>'
							+'</div>'
						+'</div>'
						+'<div class="accordion-body1 collapse" id="surveyCollapse_'+collapseId+'" style="height: 0px;">'
							+'<div class="accordion-inner">'
								+'<div class="survey-interactive-container">'
									+'<div class="row-fluid survey-div">'
										+'<div class="span12 surveyHeaderContainer">'
											+'<h4><span class="titleLabel"></span> : </h4>'
											+'<input class="form-control surveyTitle span12" style="height: 41px;" id="fieldID" type="text" value="'+surveyTitle+'">'
										+'</div>'
										+'<div class="span12 surveyHeaderContainer" style="margin-left: 0;display:none;">'
											+'<h4>Type : </h4>'
											+'<select class="form-control selectpicker surveyType" style="margin: 10px 0px;"><option value="'+SurveyTypeEnum.PRIVATE+'">Privé</option><option value="'+SurveyTypeEnum.PUBLIC+'">Public</option></select>'
										+'</div>'
										+'<div class="span12" style="margin-bottom: 20px; margin-left: 0px;">'
											+'<h4><span class="questionsLabel"></span> :</h4>'
											+'<p class="desc addSurveyQuestionLabel" style="font-size: 14px; padding-top: 4px;"></p>'
											+'<table class="table table-striped survey-field margin-top-20">'
												+'<thead>'
													+'<th width="25%"><span class="questionsLabel"></span></th>'
													+'<th width="15%">Type</th>'
													+'<th width="40%"><span class="responseLabel"></span></th>'
													+'<th width="10%"><span class="deleteLabel"></span></th>'
												+'</thead>'
												+'<tbody>'
												+'</tbody>'
											+'</table>'
											+'<button type="button" id="addQuestion" class="btn btn-block btn-white btn-cons margin-top-5">'
												+'<i class="fa fa-plus-circle mr5"></i><span class="addQuestionLabel"></span>'
											+'</button>'
										+'</div>'
									+'</div>'
								+'</div>'
							+'</div>'
							+'<div class="span12" style="text-align:center">'
								+'<button type="button" id="saveSurvey" class="btn btn-success btn-cons saveLabel" style="margin-bottom: 20px;margin-right: 10px;">Sauvegarder</button>'
								+'<button type="button" id="cancelSurvey" class="btn btn-white btn-cons cancelLabel" style="margin-bottom: 20px;">Annuler</button>'
							+'</div>'
						+'</div>'
					+'</div>');
		
		if(survey!=null && survey!=undefined && survey!=0){
			var formFields = survey.getFormFields();
			var surveyTypeId = survey.getSurveyType().getSurveyTypeId();
			dom.find('.surveyType').val(surveyTypeId);
			for(index in survey.getFormFields()){
				var formField = survey.getFormFields()[index];
				var field = formField.getField();
				var inputType = field.getInputType();
				var responseSurveyManage = new $.ResponseSurveyManage(formField, that);
				var domContainer = responseSurveyManage.getDOM();
				dom.find('.survey-field').append(domContainer);
				
				var responceTypeRowManage = new $.ResponseTypeManage();
				var inputTypeId = inputType.getInputTypeId();
				domContainer.find('.responseType').append(responceTypeRowManage.getDOM());
				domContainer.find('.responseType').val(inputTypeId);
				if(inputTypeId == InputTypeEnum.SELECT || inputTypeId == InputTypeEnum.RADIO){
					defaultValueFields = formField.getFormFieldValue();
					if(defaultValueFields != null && defaultValueFields.length > 0) {
						domContainer.find(".tokenfield").tokenfield('setTokens', defaultValueFields);
						domContainer.find('.defaultValueFieldId').html(defaultValueFields);
					}
				}else {
					domContainer.find('.tokenfield').hide();
				}
			}			
		}
		
		dom.find('.cmn-toggle-title-surveys').unbind();
		dom.find('.cmn-toggle-title-surveys').bind('click', function() {
			if($(this).attr('class').indexOf('fa-minus') == -1){
				$(this).removeClass('fa-plus').addClass('fa-minus');
			}else{
				$(this).removeClass('fa-minus').addClass('fa-plus');
			}
		});
		
		dom.find('.deleteSurvey').unbind();
		dom.find('.deleteSurvey').bind('click', function() {
			var surveyId = $(this).attr('id').split('_')[1];
			if(surveyId == 0){
				dom.remove();
			}else{
				view.deleteSurvey(surveyId);
			}
		});
		
		dom.find('#saveSurvey').unbind();
		dom.find('#saveSurvey').bind('click', function() {
			var fields = new Array();
			var formFields = new Array();
			var form = new $.Form();
			var surveyType = new $.SurveyType();
			var formType = new $.FormType();

			var isValid = true ;
			if($(".webcast-field-error") != undefined || $(".webcast-field-error").length != 0){
				$(".webcast-field-error").remove();
			}
			dom.find('.surveyTitle1').text(dom.find(".surveyTitle").val());
			form.setFormId(dom.find('.surveyId')[0].textContent);
			surveyType.setSurveyTypeId(dom.find('.surveyType').val());
			if (dom.find('.surveyTitle').val() != '') {
				
			}else{
				isValid = false;		
				dom.find(".surveyTitle").parent().append('<label class="webcast-field-error" style="display: block;">' + REQUIREDLABEL + '</label>');
			}
			
			formType.setFormTypeId(FormTypeEnum.SURVEY);
			form.setFormTitle(dom.find(".surveyTitle").val());
			form.setSurveyType(surveyType);
			form.setFormType(formType);

			if(dom.find('tbody tr').length > 0){
				dom.find('tbody tr').each(function(){
					if($.trim($(this).find('.fieldnameInput')[0].value) !='' ){ 
						var defaultValueFields = new Array();
						var formField = new $.FormField();
						var field = new $.Field();	
						var inputTypeSelect = new $.InputType();
						var inputTypeId = $(this).find('.responseType').val();
											
						formField.setFormFieldId($(this).find('.formFieldId')[0].textContent);
						formField.setFormFieldLabel($.trim($(this).find('.fieldnameInput')[0].value));
						
						field.setFieldId($(this).find('.fieldId')[0].textContent);
						field.setFieldName($.trim($(this).find('.fieldnameInput')[0].value));
						inputTypeSelect.setInputTypeId(inputTypeId);
						
						var tagInputList = $(this).find(".tokenfield").find('input').tokenfield('getTokens');
						console.log(tagInputList)
						var tagInputFinalField = '';
						for(index in tagInputList){
							console.log(tagInputList[index]);
							tagInputFinalField += tagInputList[index].value+',';
						}
						if(inputTypeId == InputTypeEnum.SELECT || inputTypeId == InputTypeEnum.RADIO){
							if(tagInputFinalField == ""){
								isValid = false;
								$(this).find('.defaultValueFieldId').parent().append('<label class="webcast-field-error" style="display: block;">' + REQUIREDLABEL + '</label>');
							}else {
								field.setFieldValue(tagInputFinalField);
								formField.setFormFieldValue(tagInputFinalField);
							}
						}
						
						if(isValid) {
							field.setInputType(inputTypeSelect);
							formField.setField(field);
							formFields.push(formField);
						}
					}else{
						isValid = false;
						$(this).find('.fieldnameInput').parent().append('<label class="webcast-field-error" style="display: block;">' + REQUIREDLABEL + '</label>');
					}
				});
			}else{
				isValid = false;
				dom.find('.survey-field').append('<label class="webcast-field-error" style="display: block;">'+ATLEASTONELABEL+'</label>');
			}
			
			if(isValid) {
				form.setFormFields(formFields);
			}
			
			if(isValid == true)	{
				//console.log(form);
				$('#loadingEds').show();
				view.notifyUpdateSurvey(form);
			}
		});
		
		dom.find('#cancelSurvey').unbind();
		dom.find('#cancelSurvey').bind('click', function() {
		});

		dom.find('#addQuestion').unbind();
		dom.find('#addQuestion').bind('click', function() {
			var responseSurveyManage = new $.ResponseSurveyManage(null, that);
			var domContainer = responseSurveyManage.getDOM();
			var responceTypeRowManage = new $.ResponseTypeManage();
			dom.find('.survey-field').append(domContainer);
			domContainer.find(".responseType").append(responceTypeRowManage.getDOM());
			domContainer.find('.tokenfield').tokenfield();
			domContainer.find('.tokenfield').find('input').attr('style','width:100% !important;');
			domContainer.find('.tokenfield').hide();
			traduct();
		});
		
		
		
		this.refresh = function(){
		},
		

		this.getDOM = function(){ 
			return dom;
		},
		
		this.refresh();
	},
	
	ResponseSurveyManage : function(formField, view){
		$indexSurvey++;
		var dom = $('<tr id="rowSurvey">'
					+'<td><div class="formFieldId required" style=" display: none; "></div><div class="fieldId" style=" display: none; "></div><input class="form-control fieldnameInput" id="fieldID" type="text"></td>'
					+'<td><select class="form-control selectpicker responseType" id=""></select></td>'
					+'<td><div class="defaultValueFieldId" style=" display: none; "></div><input type="text" class="form-control tokenfield tagsinput"/></td>'
					+'<td valign="middle" class="padding-10"><a class="removeQuestionSurvey delete fa fa-trash-o" id="removeItem"></a></td>'
					+'</tr>');
		var that=this;
		
		this.getField = function(){
			return formField;
		},
		
		this.refresh = function(){
			if(formField!=null && formField.getFormFieldId()!=undefined && formField.getFormFieldId()!=0){
				var formFieldId = formField.getFormFieldId();
				var fieldId = formField.getField().getFieldId();
				var formFieldLabel = formField.getFormFieldLabel();
				dom.find('.formFieldId').html(formFieldId);
				dom.find('.fieldId').html(fieldId);
				dom.find('.fieldnameInput').val($.trim(formFieldLabel));
				dom.find('.fieldnameInput').attr('id', 'fieldName'+formFieldId);
				dom.find('.fieldnameInput').attr('alt', $.trim(formFieldLabel));
				dom.find('.responseType').attr('id','formFieldID_'+formFieldId);

				dom.find('.delete').attr('id', 'delete'+formFieldId);
				dom.find('.delete').unbind('click');
				dom.find('.delete').bind('click', function () {
					dom.remove();
				});
				

				dom.find('.tokenfield').tokenfield();

			}else {
				dom.find('.fieldnameInput').show();
				dom.find('.formFieldId').html(0);
				dom.find('.fieldId').html(0);
				
				dom.find('.defaultValueFieldId').html(0);
				dom.find('.delete').unbind('click');
				dom.find('.delete').bind('click', function () {
					dom.remove();
				});
			}

			dom.find('.responseType').bind('change', function(){
				var selectValue = parseInt(dom.find('.responseType').val());
				if(selectValue == InputTypeEnum.SELECT || selectValue == InputTypeEnum.RADIO){
					dom.find('.tokenfield').show();
					$(dom.find('.tokenfield input')[0]).hide();
					$(dom.find('.tokenfield input')[1]).hide();
				}else{
					dom.find('.tokenfield').hide();
				}
			});
		},
		
		this.getDOM = function(){ //retourne une ligne HTML <a .....
			return dom;
		},
		
		this.refresh();
	},
	
	QuestionSurveyManage : function(question, view){
		var that=this;
		that.dom = surveyContainer;
		
		this.getSurveys = function(){
			return surveys;
		},
		
		this.refresh = function(){
		},
		

		this.getDOM = function(){ 
			return dom;
		},
		
		this.refresh();
	},
		
	ResponseTypeManage : function(){
		var that = this;
		var dom = "";		
		
		this.refresh = function(){
			for (index in InputTypes) {
				switch (InputTypes[index]) {
					case "TEXT_AREA":
						var option = '<option value="'+InputTypeEnum.TEXT_AREA+'" class="inputTextLabel"></option>';
					break;
					case "SELECT":
						var option = '<option value="'+InputTypeEnum.SELECT+'" class="multipleChoiceLabel"></option>';
					break;
					case "RADIO":
						var option = '<option value="'+InputTypeEnum.RADIO+'" class="singleChoiceLabel"></option>';
					break;
				}
				dom = dom+''+option;
			}
		},
		
		this.getDOM = function(){ //retourne une ligne HTML <a .....
			return dom;
		},
		
		this.refresh();
	},
	
	SurveyView: function(){
		var that = this;
		var listeners = new Array();
		var surveyContainer = $("#survey");
		$indexSurvey = 1;
		
		this.initView = function(){
            traduct();
		};
		
		this.initSurveys = function(surveys){
			new $.SurveysManage(surveys, surveyContainer, that);
			$('#loadingEds').hide();
		},
		
		this.notifySurveyDelete = function(surveyId){
			$('#surveyTabLabel').click();
			$('#loadingEds').hide();
		},
		
		this.notifyUpdateSurvey = function(form){
			$.each(listeners, function(i){
				listeners[i].notifyUpdateSurvey(form);
			});
		},
		
		this.deleteSurvey = function(surveyId){
			$.each(listeners, function(i){
				listeners[i].deleteSurvey(surveyId);
			});
		},
		
		this.addListener = function(list){
			listeners.push(list);
		};
		
	},
	
	
	ViewListener: function(list) {
		if(!list) list = {};
		return $.extend(list);
	}
	
});
