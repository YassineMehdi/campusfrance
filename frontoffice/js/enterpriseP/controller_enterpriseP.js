jQuery.extend({
    EnterprisePController: function (model, view) {
        var that = this;
        /**
         * listen to the view
         */
        var vlist = $.EnterprisePViewListener({
            addEnterpriseP: function (enterpriseP, enterpriseId) {
                model.addEnterpriseP(enterpriseP, enterpriseId);
            },
            updateEnterpriseP: function (enterpriseP) {
                model.updateEnterpriseP(enterpriseP);
            },
            deleteEnterpriseP: function (enterprisePId) {
                model.deleteEnterpriseP(enterprisePId);
            }
        });

        view.addListener(vlist);
        /**
         * listen to the model
         */
        var mlist = $.EnterprisePModelListener({
        	enterprisePsLoad: function (enterprisePs) {
                view.parseEnterprisePs(enterprisePs);
            },
            updateEnterprisePSuccess: function () {
                view.updateEnterprisePSuccess();
            },
            addEnterprisePSuccess: function () {
                view.addEnterprisePSuccess();
            },
            deleteEnterprisePSuccess: function () {
                view.deleteEnterprisePSuccess();
            }
        });

        model.addListener(mlist);

        this.initController = function () {
            view.initView();
            model.initModel();
        };
    }
});
