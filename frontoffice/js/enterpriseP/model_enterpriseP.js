jQuery.extend({    
    EnterprisePModel: function () {
        var that = this;
        var listeners = new Array();
        
        this.initModel = function () {
        	//console.log("EnterprisePModel");
            that.getEnterprisePs();
        };
        // EnterprisePs
        this.getEnterprisePsSuccess = function (xml) {
            //console.log(xml);
            var enterprisePs = new Array();
            $(xml).find("enterprisep").each(function () {
                var enterpriseP = new $.EnterpriseP($(this));
                //console.log("*****************enterpriseP");
                //console.log(EnterpriseP);
                enterprisePs.push(enterpriseP);
            });
            //console.log(enterprisePs);
            that.notifyEnterprisePsSuccess(enterprisePs);
        };

        this.getEnterprisePsError = function (xhr, status, error) {
        	//console.log("errooor get enterpriseP"+error);
            isSessionExpired(xhr.responseText);
        };
        this.getEnterprisePs = function () {
        	//console.log("GET: getEnterprisePs****"+enterpriseId);
            genericAjaxCall('GET', staticVars.urlBackEndManager + 'enterprisep/list?enterpriseId=' + enterpriseId,
                    'application/xml', 'xml', '', that.getEnterprisePsSuccess, that.getEnterprisePsError);
        };
        // Add EnterpriseP
        this.addEnterprisePSuccess = function (xml) {
        	//console.log("ADD: addEnterprisePs****"+enterpriseId);
            that.notifyAddEnterprisePSuccess();
            that.getEnterprisePs();
        };

        this.addEnterprisePError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
            //console.log("erooooor:")
            //console.log(error);
        };

        this.addEnterpriseP = function (enterpriseP, enterpriseId) {
        	//console.log("add enterprise****************");
        	//console.log(EnterpriseP.xmlData());
            genericAjaxCall('POST', staticVars.urlBackEndManager
                    + 'enterprisep/add?enterpriseId='+enterpriseId, 'application/xml', 'xml',
                    enterpriseP.xmlData(), that.addEnterprisePSuccess, that.addEnterprisePError);
        };
        // Update EnterpriseP
        this.updateEnterprisePSuccess = function (xml) {
            that.notifyUpdateEnterprisePSuccess();
            that.getEnterprisePs();
        };

        this.updateEnterprisePError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.updateEnterpriseP = function (enterpriseP) {
            genericAjaxCall('POST', staticVars.urlBackEndManager
                    + 'enterprisep/update', 'application/xml', 'xml',
                    enterpriseP.xmlData(), that.updateEnterprisePSuccess, that.updateEnterprisePError);
        };
        // Delete EnterpriseP
        this.deleteEnterprisePSuccess = function (xml) {
            that.notifyDeleteEnterprisePSuccess();
            that.getEnterprisePs();
        };

        this.deleteEnterprisePError = function (xhr, status, error) {
        	//console.log("error:"+error);
            isSessionExpired(xhr.responseText);
        };

        this.deleteEnterpriseP = function (enterprisePId) {
            //$('#EnterpriseP' + enterprisePId).remove();
            genericAjaxCall('POST', staticVars.urlBackEndManager + 'enterprisep/delete?enterprisePId=' + enterprisePId, 'application/xml', 'xml', '',
                    that.deleteEnterprisePSuccess, that.deleteEnterprisePError);
        };

        this.addListener = function (list) {
            listeners.push(list);
        };

        this.notifyEnterprisePsSuccess = function (enterprisePs) {
            $.each(listeners, function (i) {
                listeners[i].enterprisePsLoad(enterprisePs);
            });
        };
        this.notifyUpdateEnterprisePSuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].updateEnterprisePSuccess();
            });
        };
        this.notifyAddEnterprisePSuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].addEnterprisePSuccess();
            });
        };
        this.notifyDeleteEnterprisePSuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].deleteEnterprisePSuccess();
            });
        };

    },
    EnterprisePModelListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }
});
