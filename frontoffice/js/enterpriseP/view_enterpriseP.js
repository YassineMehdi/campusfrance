jQuery.extend({
    EnterprisePManage: function (enterpriseP, view) {
        //var that = this;
        var dom = $('<div class="col-xs-6 col-md-4 newEnterpriseP" id="enterpriseP' + enterpriseP.getEnterprisePId() + '">'
                + '<div class= "panel panel-default" data - widget = "">'
                + '<div class="hide enterprisePID_' + enterpriseP.getEnterprisePId() + '">' + enterpriseP.getEnterprisePId() + '</div>'
                + '<div class = "enterprisePOptions">'
                + '<a href = "javascript:void(0)" class = "button-icon editEnterpriseP"> <i class = "fa fa-pencil"> </i></a>'
                + '<a href = "javascript:void(0)" class = "button-icon deleteEnterpriseP"> <i class = "fa fa-times"> </i></a>'
                + '</div>'
                + '<div class = "panel-body">'
                + '<img class="enterprisePPhoto" alt = "">'
                + '<ul class = "EnterprisePUl">'
                + '<li class="enterprisePName"></li>'
                + '<li class="enterprisePFunction"></li>'
                + '<li class="enterprisePEmail"></li>'
                + '</ul>'
                + '</div>'
                + '</div></div>'
                + '</div>');
        
        this.refresh = function () {
            dom.find('.enterprisePPhoto').attr('src', enterpriseP.getUserProfile().getAvatar());
            dom.find('.enterprisePName').text(enterpriseP.getFullName());
            dom.find('.enterprisePFunction').text(enterpriseP.getJobTitle());
            dom.find('.enterprisePEmail').text(enterpriseP.getEmail());

            dom.find('.deleteEnterpriseP').unbind("click").bind("click", function (e) {
                view.factoryModalRemoveEnterpriseP(enterpriseP);
            });
            //Delete enterpriseP
            dom.find('.editEnterpriseP').unbind("click").bind("click", function (e) {
                view.factoryUpdateEnterpriseP(enterpriseP);
                $("#idEnterpriseP").attr("value", enterpriseP.getEnterprisePId());
            });
        };
        this.getDOM = function () {
            return dom;
        };
        this.getEnterpriseP = function () {
            return enterpriseP;
        };
        this.refresh();
    },
    EnterprisePView: function () {
        var that = this;
        that.update = false;
        var listeners = new Array();
        //var enterpriseProfileForm = null;
        var contentHandler = null;
        var newEnterprisePForm = null;
        //var saveProfileHandler = null;
        var enterprisePContentHandler = null;
        var addEnterprisePBtnHandler = null;
        var modalEnterprisePHandler = null;
        var saveNewEnterprisePtHandler = null;
        var validatorEnterpriseP = null;
        var idDeleteEnterprisePHandler = null;
        var enterprisePManageList = null;
        var removeConatactBtnHandler = null;
        var modalIDEnterprisePHandler = null;

        this.initView = function () {
            traduct();
            contentHandler = $("#mainBackEndContainer");
            //enterpriseProfileForm = $("#enterpriseProfileForm");
            newEnterprisePForm = $("#EnterprisePForm");
            //saveProfileHandler = $(".saveAccountButton");
            enterprisePContentHandler = $(".enterprisePG");
            addEnterprisePBtnHandler = $(".addEnterprisePLink");
            modalEnterprisePHandler = $("#addEnterpriseP");
            saveNewEnterprisePtHandler = $("#saveEnterpriseP");
            idDeleteEnterprisePHandler = $("#idDeleteEnterpriseP");
            removeConatactBtnHandler = $("#btnDeleteEnterpriseP");
            modalIDEnterprisePHandler = $('#idEnterpriseP');
            enterprisePManageList = new Array();
            that.validateNewEnterprisePForm();
            saveNewEnterprisePtHandler.unbind("click").bind("click", function (e) {
                that.saveEnterprisePDom();
            });
            //Custom file upload
            $('#logoUpload').fileupload({
                beforeSend: function (jqXHR, settings) {
                    beforeUploadFile('.saveStandButton', '#progressLogoUpload', '#logoUploadStand .filebutton,#loadLogoStandError');
                },
                datatype: 'json',
                cache: false,
                add: function (e, data) {
                    var goUpload = true;
                    var uploadFile = data.files[0];
                    var fileName = uploadFile.name;
                    timeUploadLogo = (new Date()).getTime();
                    data.url = getUrlPostUploadFile(timeUploadLogo, fileName);
                    if (!(/\.(gif|jpg|jpeg|tiff|png)$/i).test(fileName)) {
                        goUpload = false;
                    }
                    if (uploadFile.size > MAXFILESIZE) {// 6mb
                        goUpload = false;
                    }
                    if (goUpload == true) {
                        data.submit();
                    } else
                        $('#loadLogoStandError').show();
                },
                progressall: function (e, data) {
                    progressBarUploadFile('#progressLogoUpload', data, 100);
                },
                done: function (e, data) {
                    var file = data.files[0];
                    var fileName = file.name;
                    var hashFileName = getUploadFileName(timeUploadLogo, fileName);
                    //console.log("Add logo stand file done, filename :"+fileName+", hashFileName : "+hashFileName);
                    var enterpriseLogo = getUploadFileUrl(hashFileName);
                    $('#enterpriseLogo').attr('src', enterpriseLogo);
                    afterUploadFile('.saveStandButton', '#logoUploadStand .filebutton', '#progressLogoUpload');
                }
            });
            //Add enterpriseP
            addEnterprisePBtnHandler.unbind("click").bind("click", function (e) {
                that.factoryAddEnterpriseP();

            });
            //Delete enterpriseP
            removeConatactBtnHandler.unbind("click").bind("click", function (e) {
                that.notifyDeleteEnterpriseP(idDeleteEnterprisePHandler.val());

            });
            //Add Advice file upload avatar
            $('#uploadAvatar').fileupload({
                beforeSend: function (jqXHR, settings) {
                    beforeUploadFile('.saveStandButton', '#progressAvatarUpload', '#avatarUploadEnterprise .filebutton, .loadError');
                },
                datatype: 'json',
                cache: false,
                add: function (e, data) {
                    var goUpload = true;
                    var uploadFile = data.files[0];
                    var fileName = uploadFile.name;
                    timeUploadLogo = (new Date()).getTime();
                    data.url = getUrlPostUploadFile(timeUploadLogo, fileName);
                    if (!(/\.(gif|jpg|jpeg|tiff|png)$/i).test(fileName)) {
                        goUpload = false;
                    }
                    if (uploadFile.size > MAXFILESIZE) {// 6mb
                        goUpload = false;
                    }
                    if (goUpload == true) {
                        data.submit();
                    } else
                        $('.loadError').show();
                },
                progressall: function (e, data) {
                    progressBarUploadFile('#progressAvatarUpload', data, 100);
                },
                done: function (e, data) {
                    var file = data.files[0];
                    var fileName = file.name;
                    var hashFileName = getUploadFileName(timeUploadLogo, fileName);
                    console.log("Add logo stand file done, filename :"+fileName+", hashFileName : "+hashFileName);
                    var avatarEnterpriseP = getUploadFileUrl(hashFileName);
                    $('#avatarNewEnterpriseP').attr('src', avatarEnterpriseP);
                    afterUploadFile('.saveStandButton', '#avatarUploadEnterpriseP .filebutton', '#progressAvatarUpload');
                }
            });

            factorycheckbox();
            $('#addEnterpriseP .changePasswordEnterpriseP').click(function (e) {
                if ($('.icheckboxEnterpriseP .icheckbox_minimal-blue').hasClass('checked')) {
                    $('.passwordFieldEnterpriseP').prop("disabled", false);
                    //$('.saveAccountButton').removeClass('disabled');
                } else {
                    $('.passwordFieldEnterpriseP').val("");
                    $('.passwordFieldEnterpriseP').prop("disabled", true);
                    //$('.saveAccountButton').addClass('disabled');
                }
            });
            $('.passwordFieldEnterpriseP').prop("disabled", true);
        };

        //EnterprisePs Xml
        this.parseEnterprisePs = function (enterprisePs) {
            enterprisePContentHandler.empty();
            if (enterprisePs != null) {
                for (var i = 0; i < enterprisePs.length; i++) {
                    var enterpriseP = enterprisePs[i];
                    var enterprisePManage = new $.EnterprisePManage(enterpriseP, that);
                    enterprisePContentHandler.append(enterprisePManage.getDOM());
                    enterprisePManageList[enterpriseP.getEnterprisePId()] = enterprisePManage;
                }
            }
            $('#loadingEds').hide();
        };
        this.saveEnterprisePDom = function () {
            if (newEnterprisePForm.valid()) {
                var userProfile = null;
                var enterpriseP = null;
                var enterprisePManage = null;
                if (!that.update) {
                    enterpriseP = new $.EnterpriseP();
                    userProfile = new $.UserProfile();
                    enterpriseP.setEnterprisePId(new Date().getTime());
                    enterpriseP.setUserProfile(userProfile);
                } else {
                    enterprisePManage = enterprisePManageList[modalIDEnterprisePHandler.attr("value")];
                    enterpriseP = enterprisePManage.getEnterpriseP();
                    userProfile = enterpriseP.getUserProfile();
                }
                enterpriseP.setLogin($('#loginEnterpriseP').val());
                enterpriseP.setPassword($.sha1($('#passwordEnterpriseP').val()));
                enterpriseP.setJobTitle($('#enterprisePJobTitle').val());
                enterpriseP.setEmail($('#emailEnterpriseP').val());
                userProfile.setAvatar($('#avatarNewEnterpriseP').attr('src'));
                userProfile.setPseudoSkype($('#enterprisePPseudoSkype').val());
                userProfile.setFirstName($('#enterprisePFirstName').val());
                userProfile.setSecondName($('#enterprisePName').val());
                enterpriseP.setEmail($('#emailEnterpriseP').val());
                enterpriseP.setIsAdmin($("input[type='radio'][name='adminRadioButton'][value='oui']").is(":checked"));
                if (!that.update) {
                    that.notifyAddEnterpriseP(enterpriseP, enterpriseId);
                } else {
                    enterprisePManage.refresh();
                    that.notifyUpdateEnterpriseP(enterpriseP);
                }
                modalEnterprisePHandler.modal('hide');
            }
        };
        //Add enterpriseP
        this.factoryAddEnterpriseP = function () {
            that.update = false;
            modalEnterprisePHandler.modal();
            $('#addEnterpriseP').find("#idEnterpriseP").text("");
            $('#avatarNewEnterpriseP').attr('src', 'img/avatar/default_avatar.jpg');
            newEnterprisePForm[0].reset();
            validatorEnterpriseP.resetForm();
        };
        //Delete enterpriseP
        this.factoryModalRemoveEnterpriseP = function (enterpriseP) {
            $('#removeEnterpriseP').modal();
            idDeleteEnterprisePHandler.val(enterpriseP.getEnterprisePId());
        };
        //Update enterpriseP
        this.factoryUpdateEnterpriseP = function (enterpriseP) {
            that.update = true;
            modalEnterprisePHandler.modal();
            newEnterprisePForm[0].reset();
            validatorEnterpriseP.resetForm();
            // $('#idEnterpriseP').text(enterpriseP.getEnterprisePId());
            $('#avatarNewEnterpriseP').attr('src', enterpriseP.getUserProfile().getAvatar());
            $('#enterprisePFirstName').val(enterpriseP.getUserProfile().getFirstName());
            $('#enterprisePName').val(enterpriseP.getUserProfile().getSecondName());
            $('#enterprisePJobTitle').val(enterpriseP.getJobTitle());
            $('#emailEnterpriseP').val(enterpriseP.getEmail());
            $('#enterprisePPseudoSkype').val(enterpriseP.getUserProfile().getPseudoSkype());
            $('#loginEnterpriseP').val(enterpriseP.getLogin());
            if(enterpriseP.getIsAdmin())
            	$("input[type='radio'][name='adminRadioButton'][value='oui']").prop("checked", true);
            else $("input[type='radio'][name='adminRadioButton'][value='non']").prop("checked", true);
            // $('#passwordEnterpriseP').val(enterpriseP.getPassword());
        };

        this.validateNewEnterprisePForm = function () {
            $.validator.addMethod("phoneNumbre", function (phone_number, element) {
                phone_number = phone_number.replace(/\s+/g, "");
                return this.optional(element) || phone_number.length > 9 &&
                        (phone_number.match(/\(?\+([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/) || phone_number.match(/\(?(00[0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/));
            }, "Merci de respecter le format indiqué");
            validatorEnterpriseP = $("#EnterprisePForm").validate({
                rules: {
                    enterprisePName: {
                        required: true
                    },
                    enterprisePFirstName: {
                        required: true
                    },
                    emailEnterpriseP: {
                        required: true
                    },
                    passwordEnterpriseP: {
                        required: true
                    },
                    confirmPasswordEnterpriseP: {
                    	required:true,
                    	equalTo:"#passwordEnterpriseP"
                    }
                }
            });
        };
        this.updateEnterprisePSuccess = function () {
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        this.addEnterprisePSuccess = function () {
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        this.deleteEnterprisePSuccess = function () {
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };

        this.addListener = function (list) {
            listeners.push(list);
        };

        this.notifyAddEnterpriseP = function (enterpriseP, enterpriseId) {
            $.each(listeners, function (i) {
                listeners[i].addEnterpriseP(enterpriseP, enterpriseId);
            });
        };

        this.notifyUpdateEnterpriseP = function (enterpriseP) {
            $.each(listeners, function (i) {
                listeners[i].updateEnterpriseP(enterpriseP);
            });
        };

        this.notifyDeleteEnterpriseP = function (enterprisePId) {
            $.each(listeners, function (i) {
                listeners[i].deleteEnterpriseP(enterprisePId);
            });
        };

    },
    EnterprisePViewListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }



});
