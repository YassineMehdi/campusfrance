jQuery.extend({

	InboxController: function(model, view){
		var that = this;
		/**
		 * listen to the view
		 */
		var vlist = $.ViewListener({
			loadConversationByFolderId : function(folderId) {
				model.getReceivedConversations(folderId);
			},
			loadArchivedConversation : function() {
				model.loadArchivedConversation();
			},
			markAsRead : function(conversationId) {
				model.markAsRead(conversationId);
			},
			sendReply : function(message,conversationId) {
				model.sendReply(message,conversationId);
			},
			addNewFolder : function(folder) {
				model.addNewFolder(folder);
			},
			deleteFolder : function(idFolder) {
				model.deleteFolder(idFolder);
			},
			changeFolder : function(idFolder, idConversationList) {
				model.changeFolder(idFolder, idConversationList);
			},
			deleteConversation : function(idConversationList) {
				model.deleteConversation(idConversationList);
			},
			archiveConversation : function(idConversationList) {
				model.archiveConversation(idConversationList);
			},
		});
		view.addListener(vlist);

		/**
		 * listen to the model
		 */
		var mlist = $.ModelListener({			
			notifyLoadMessageFolder : function(messageFolder) {
				view.initMessageFolder(messageFolder);
			},
			notifyLoadConversation : function(conversation) {
				view.initConversation(conversation);
			},
			notifyAsRead : function(conversationId) {
				view.notifyAsRead(conversationId);
			},
			notifySendReplySuccess : function(message) {
				view.notifySendReplySuccess(message);
			},
			notifyAddNewFolder : function(folder) {
				view.notifyAddNewFolder(folder);
			},
			notifyDeleteFolder : function() {
				view.notifyDeleteFolder();
			},
			notifyChangeFolder : function() {
				view.notifyChangeFolder();
			},
			notifyDeleteConversation : function() {
				view.notifyDeleteConversation();
			},
			notifyArchivedConversation : function() {
				view.notifyArchivedConversation();
			},
		});
		
		model.addListener(mlist);
		
		this.initController = function(){
			view.initView();
			model.initModel();
		};
	}
});