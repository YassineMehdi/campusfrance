jQuery.extend({
    VideosRhController: function (model, view) {
        var that = this;
        /**
         * listen to the view
         */
        var vlist = $.VideosRhViewListener({
            addVideoRh: function (videoRh) {
                model.addVideoRh(videoRh);
            },
            updateVideoRh: function (videoRh) {
                model.updateVideoRh(videoRh);
            },
            deleteVideoRh: function (videoRhId) {
                model.deleteVideoRh(videoRhId);
            }
        });

        view.addListener(vlist);

        /**
         * listen to the model
         */
        var mlist = $.VideosRhModelListener({
            loadVideosRh: function (listVideosRh) {
                view.displayVideosRh(listVideosRh);
            },
            addVideoRhSuccess: function () {
                view.addVideoRhSuccess();
            },
            updateVideoRhSuccess: function () {
                view.updateVideoRhSuccess();
            },
            deleteVideoRhSuccess: function () {
                view.deleteVideoRhSuccess();
            }
        });

        model.addListener(mlist);

        this.initController = function () {
            view.initView();
            model.initModel();
        };
    }
});
