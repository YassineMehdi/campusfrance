jQuery.extend({
    VideosRhView: function () {
        var that = this;
        var listeners = new Array();
        var videoCache = [];
        var addVideoBtnHandler = null;
        var adviceCheckboxHandler = null;
        var saveVideoHandler = null;
        var titleVideoRhHandler = null;
        var fileinputHandler = null;
        var modalVideoHandler = null;
        var contentHandler = null;
        var loadAdviceErrorHandler = null;
        var progressAdviceHandler = null;
        var btnDeleteVideoHandler = null;
        var videoFormHandler = null;
        var jqXHRVideo = null;


        this.initView = function () {
            traduct();
            contentHandler = $("#mainBackEndContainer");
            modalVideoHandler = $("#addVideoRh");
            addVideoBtnHandler = $(".addVideoRhLink");
            adviceCheckboxHandler = $(".choiceBox");
            saveVideoHandler = $("#saveVideoRh");
            titleVideoRhHandler = $("#titleAdviceRh");
            videoFormHandler = $("#videoRhForm");
            btnDeleteVideoHandler = $("#btnDeleteVideoRh");
            jqXHRVideo = null;
            //file input
            $("#videoUpload").fileupload({
                beforeSend: function (jqXHR, settings) {
                    beforeUploadFile('#saveVideoRh', '#progressVideoRh', '#loadVideoRhError');
                },
                datatype: 'json',
                cache: false,
                add: function (e, data) {
                    var goUpload = true;
                    var uploadFile = data.files[0];
                    var fileName = uploadFile.name;
                    timeUploadVideo = (new Date()).getTime();
                    console.log(timeUploadVideo);
                    data.url = getUrlPostUploadFile(timeUploadVideo, fileName);
                    if (!(/\.(flv|mp4|mov|m4v|f4v|wmv)$/i).test(fileName)) {
                        goUpload = false;

                    }
                    if (uploadFile.size > MAXFILESIZE) {
                        goUpload = false;
                    }
                    if (goUpload == true) {
                        jqXHRAdvice = data.submit();
                    } else
                        $('#loadVideoRhError').show();
                },
                progressall: function (e, data) {
                    progressBarUploadFile('#progressVideoRh', data, 99);
                },
                done: function (e, data) {
                    var file = data.files[0];
                    var fileName = file.name;
                    console.log(timeUploadVideo);
                    var hashFileName = getUploadFileName(timeUploadVideo, fileName);
                    console.log("Add Video file done, filename :" + fileName + ", hashFileName : " + hashFileName);
                    $('#loadVideoError').show();
                    $('.fileInfo').text(fileName);
                    $('.fileInfo').attr('href', getUploadFileUrl(hashFileName));
                    afterUploadFile('#saveVideoRh', '#videoRhLoaded', '#progressVideoRh,#loadVideoRhError');
                }
            });
            //$.wijets().make();
            $('#listVideosRhTable').dataTable({
                "language": {
                    url: 'plugins/datatables/'+getIdLangParam()+'.json'
                },
                "aaData": [],
                "aaSorting": [],
                "aoColumns": [
                    {
                        "sTitle": TITLELABEL
                    },
                    {
                        "sTitle": CONTENTLABEL
                    },
                    {
                        "sTitle": ACTIONSLABEL
                    }
                ],
                "bFilter": false,
                "bLengthChange": false,
                "bAutoWidth": false,
                "bRetrieve": false,
                "bDestroy": true,
                "iDisplayLength": 10,
                "fnDrawCallback": function () {
                    $('.viewVideoRhLink').off();
                    $('.viewVideoRhLink').on("click", function () {
                        var videoId = $(this).closest('div').attr('id');
                        that.factoryViewVideoRh(videoCache[videoId]);

                    });

                    $('.editVideoRhLink').off();
                    $('.editVideoRhLink').on("click", function () {
                        var videoId = $(this).closest('div').attr('id');
                        that.updateVideoRh(videoCache[videoId]);
                    });

                    $('.deleteVideoRhLink').off();
                    $('.deleteVideoRhLink').on("click", function () {

                        var videoId = $(this).closest('div').attr('id');
                        $('#removeVideoRh').modal();
                        $('#idDeleteVideoRh').val(videoId);
                    });
                }
            });
            //save video
            saveVideoHandler.unbind("click").bind("click", function (e) {
                that.saveVideoRh();
            });
            //Choice Upload
            adviceCheckboxHandler.unbind("click").bind("click", function (e) {
                that.factoryChoiceCheckbox();
            });
            //Add video
            addVideoBtnHandler.unbind("click").bind("click", function (e) {
                that.factoryAddVideoRh();
            });

            //Delete video
            btnDeleteVideoHandler.unbind("click").bind("click", function (e) {
                that.notifyDeleteVideoRh($('#idDeleteVideoRh').val());
            });

            //Dismiss video
            $('#addVideo').on('hide.bs.modal', function (e) {
                $('.progress').hide();
                if (jqXHRVideo != null)
                    jqXHRVideo.abort();
            });

        };
        //Save Video
        this.saveVideoRh = function () {
            that.validateVideoRhForm();
            if ($("#videoRhForm").valid()) {
                var video = new $.File();
                video.setIdFile(0);
                video.setFileTitle($('#videoRhTitle').val());
                video.setFileDescription($('#videoRhDescription').val());
                if ($('#uploadVideoRh').is(":checked")) {
                    video.setFileUrl($('.fileInfo').attr('href'));
                    $('.fileInfo').attr('target', '_blanck');
                    video.setFileName($('.fileInfo').text());
                } else {
                    video.setFileUrl($('#videoRhUrl').val());
                }
                var fileType = new $.FileType();
                fileType.setFileTypeId(fileTypeEnum.VIDEORH);
                video.setFileType(fileType);

                if (!that.update) {
                    that.notifyAddVideoRh(video);
                } else {
                    video.setIdFile($('#idVideoRh').val());
                    that.notifyUpdateVideoRh(video);
                }

            }
            ;
        };
        //Add Video
        this.factoryAddVideoRh = function () {
            that.update = false;
            modalVideoHandler.modal();
            //loadAdviceErrorHandler.hide();
            //progressAdviceHandler.hide();
            $('.videoUrlDiv').show();
            $('.uploadOptionDiv').hide();
            $('.fileInfo').attr('src');
            $('.fileInfo').text('');
            videoFormHandler[0].reset();
            $("label.error").hide();
            $(".error").removeClass("error");
        };
        //Choice Upload
        this.factoryChoiceCheckbox = function () {
            if ($('#urlOption').is(":checked")) {
                $('.videoUrlDiv').show();
                $('.uploadOptionDiv').hide();
            } else {
                $('.videoUrlDiv').hide();
                $('.uploadOptionDiv').show();
            }
        };
        this.addVideoRhSuccess = function () {
            modalVideoHandler.modal('hide');
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        // Update Advice
        this.updateVideoRh = function (video) {
            that.update = true;
            $('#addVideoRh').modal();
            videoFormHandler[0].reset();
            $('#idVideoRh').val(video.getIdFile());
            $('#videoRhTitle').val(video.getFileTitle());
            $('#videoRhDescription').val(video.getFileDescription());
            if ((video.getFileName() != undefined) && (video.getFileName() != '')) {
                $('.fileInfo').attr('href', video.getFileUrl());
                $('.fileInfo').text(video.getFileName());
                $('#uploadVideoRh').click();
            } else {
                $('#videoRhUrl').val(video.getFileUrl());
                $('#urlOptionRh').click();
            }
            that.factoryChoiceCheckbox();

        };

        this.updateVideoRhSuccess = function () {
            modalVideoHandler.modal('hide');
            modalVideoHandler.find('#videoRhForm')[0].reset();
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        // Delete Poster
        this.deleteVideoRhSuccess = function () {
            modalVideoHandler.modal('hide');
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };

        this.displayVideosRh = function (videos) {
            videoCache = videos;
            //todo
            //var rightGroup = listStandRights[rightGroupEnum.VIDEOS_RH];
            //if (rightGroup != null) {
              //  var permittedVideoRhsNbr = rightGroup.getRightCount();
                //var videoRhsNbr = Object.keys(videos).length;
                //if (permittedVideoRhsNbr == -1) {
                    $('#addVideoRhDiv #addVideoRhLink').text(ADDLABEL);
                    $('#addVideoRhDiv').show();
                /*} else if (videoRhsNbr < permittedVideoRhsNbr) {
                    $('#numberVideoRhs').text(videoRhsNbr);
                    $('#numberPermittedVideoRhs').text(permittedVideoRhsNbr);
                    $('#addVideoRhDiv').show();
                } else {
                    $('#addVideoRhDiv').hide();
                }
            }*/

            var listVideos = [];
            var video = null;
            var videoId = null;
            var videoUrl = null;
            var videoTitle = null;
            var videoDescription = null;
            var videoImage = null;
            var data = null;
            var listActions = null;
            var videoImg = null;
            var videoContent = null;

            for (var index in videos) {
                video = videos[index];
                videoId = video.getIdFile();
                videoUrl = video.getFileUrl();
                videoTitle = video.getFileTitle();
                if (videoTitle == undefined)
                    videoTitle = '';
                videoDescription = video.getFileDescription();
                videoImage = video.getFileImage();
                videoImg = "<img class='apercuImg' src='img/video.png' alt='' />";
                videoContent = "<div class='contentTruncate'>" + videoTitle.replace(/<(?:.|\n)*?>/gm, '') + "</div>";
                listActions = "<div class='actionBtn' id='" + videoId + "'><a href='javascript:void(0);' class='editVideoRhLink editLink actionBtn'><i class='fa fa-pencil-square-o'></i></a> <a href='javascript:void(0);' class='viewVideoRhLink viewLink actionBtn'><i class='fa fa-eye'></i></a> <a href='javascript:void(0);' class='deleteVideoRhLink deleteLink actionBtn'><i class='fa fa-trash-o'></i></a></div>";
                data = [videoImg, videoContent, listActions];
                listVideos.push(data);
            }
            addTableDatas("#listVideosRhTable", listVideos);
            $('#loadingEds').hide();
        };
        // View Video
        this.factoryViewVideoRh = function (video) {
            var videoUrl = video.getFileUrl();
            console.log(videoUrl);
            var videoTitle = video.getFileTitle();
            $('#viewVideoRh').modal();
            $('#videoRhTitlePopup').text(videoTitle);
            playVideoPopup(videoUrl, videoTitle);


        };
        // Validator form
        this.validateVideoRhForm = function () {
            $.validator.addMethod("uploadFile", function (val, element) {
                //console.log(val);
                var fileName = $(".fileInfo").text();
                var fileUrl = $(".fileInfo").attr("href");
                if ((fileName == '' || fileUrl == '') && ($('#uploadVideoRh').is(":checked"))) {
                    return false;
                } else {
                    return true;
                }

            }, "File type error");

            $("#videoRhForm").validate({
                rules: {
                    videoRhTitle: {
                        required: true
                    },
                    videoRhUrl: {
                        required: "#urlOptionRh:checked",
                        url: true
                    }
                }
            });
            $("#videoRhForm #videoRhUpload").rules("add", {
                uploadFile: true
            });
        };

        this.addListener = function (list) {
            listeners.push(list);
        };

        this.notifyAddVideoRh = function (video) {
            $.each(listeners, function (i) {
                listeners[i].addVideoRh(video);
            });
        };

        this.notifyUpdateVideoRh = function (video) {
            $.each(listeners, function (i) {
                listeners[i].updateVideoRh(video);
            });
        };

        this.notifyDeleteVideoRh = function (videoId) {
            $.each(listeners, function (i) {
                listeners[i].deleteVideoRh(videoId);
            });
        };

    },
    VideosRhViewListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }



});
