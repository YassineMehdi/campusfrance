jQuery.extend({
    VideosRhModel: function () {
        var that = this;
        var listeners = new Array();
        this.initModel = function () {
            that.getStandVideosRh();
        };

        // lister Videos
        this.getStandVideosRhSuccess = function (xml) {
            var listVideos = [];
            $(xml).find("allfiles").each(function () {
                var video = new $.File($(this));
                listVideos[video.getIdFile()] = video;
            });
            that.notifyLoadVideosRh(listVideos);
        };

        this.getStandVideosRhError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.getStandVideosRh = function () {
            //$('#listStandPosters').empty();
            genericAjaxCall('GET', staticVars.urlBackEndManager + 'allfiles/standVideoRh?idLang=' + getIdLangParam()+'&idStand='+standId, 'application/xml', 'xml', '',
                    that.getStandVideosRhSuccess, that.getStandVideoRhsError);
        };

        // Add Videos
        this.addVideoRhSuccess = function (xml) {
            that.notifyAddVideoRhSuccess();
            that.getStandVideosRh();
        };

        this.addVideoRhError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.addVideoRh = function (video) {
        	console.log("video xml data ******");
        	console.log(video.xmlData());
            genericAjaxCall('POST', staticVars.urlBackEndManager
                    + 'allfiles/addFile?idLang=' + getIdLangParam()+'&idStand='+standId, 'application/xml', 'xml', video.xmlData(),
                    that.addVideoRhSuccess, that.addVideoRhError);
        };
        // Update Videos
        this.updateVideoRhSuccess = function (xml) {
            that.notifyUpdateVideoRhSuccess();
            that.getStandVideosRh();
        };

        this.updateVideoRhError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.updateVideoRh = function (video) {
            genericAjaxCall('POST', staticVars.urlBackEndManager
                    + 'allfiles/updateFile?idLang=' + getIdLangParam()+'&idStand='+standId, 'application/xml', 'xml', video.xmlData(),
                    that.updateVideoRhSuccess, that.updateVideoRhError);
        };
        // Delete Videos
        this.deleteVideoRhSuccess = function (xml) {
            that.notifyDeleteVideoRhSuccess();
            that.getStandVideosRh();
        };

        this.deleteVideoRhError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.deleteVideoRh = function (videoId) {
            genericAjaxCall('GET', staticVars.urlBackEndManager + 'allfiles/deleteMedia/' + videoId, 'application/xml', 'xml', '',
                    that.deleteVideoRhSuccess, that.deleteVideoRhSuccess);
        };

        this.addListener = function (list) {
            listeners.push(list);
        };

        this.notifyLoadVideosRh = function (listVideos) {
            $.each(listeners, function (i) {
                listeners[i].loadVideosRh(listVideos);
            });
        };
        this.notifyAddVideoRhSuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].addVideoRhSuccess();
            });
        };
        this.notifyUpdateVideoRhSuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].updateVideoRhSuccess();
            });
        };
        this.notifyDeleteVideoRhSuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].deleteVideoRhSuccess();
            });
        };
    },
    VideosRhModelListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }
});
