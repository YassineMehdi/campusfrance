jQuery.extend({
    HomeModel: function () {
        var that = this;
        var listeners = new Array();
        this.initModel = function () {

        };

        this.addListener = function (list) {
            listeners.push(list);
        };

    }
    ,
    HomeModelListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }
});
