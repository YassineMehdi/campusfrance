jQuery.extend({
    HomeView: function () {
        var that = this;
        var listeners = new Array();
        var prerequisitesHandler = $("#prerequisites");
        var calendarHandler = $(".fullCalendar");
        var alertHandler = $(".alertBtn");
        var conseilHandler = $(".conseilNavBtn");
        var accountOptionHandler = $(".accountOption");
        var standHandler = $(".stand");
        var candidaturesBtnHandler = $(".candidaturesBtn");
        var jobOfferBtnHandler = $(".jobOffers");
        var visitorsBtnHandler = $(".visitorsBtn");
        var languagesOptionsBtnHandler = $(".languagesOptions");
        var inboxBtnHandler = $(".inbox");
        var tchatBtnHandler = $(".tchatBtn");
        var dashboardBtnHandler = $(".dashboardBtn");
        var backButton=$("#back-button");
        this.initView = function () {
        	backButton.unbind("click").bind("click", function (e) {
        		 $(".editEnterprise").hide();
       		  
       		  $(".listEnterprises").show();
       		  $(".enterprisesList").trigger("click");
        	});
            standHandler.unbind("click").bind("click", function (e) {
                $('html, body').animate({
                    scrollTop: 0
                }, 'slow');
                that.factoryStand();
            });
            
            tchatBtnHandler.unbind("click").bind("click", function (e) {
                console.log('inside tchatBtn 1');
                $('html, body').animate({
                    scrollTop: 0
                }, 'slow');
                that.factoryTchat();
            });

            accountOptionHandler.unbind("click").bind("click", function (e) {
                $('html, body').animate({
                    scrollTop: 0
                }, 'slow');
                that.factoryAccountOption();
            });

            conseilHandler.unbind("click").bind("click", function (e) {
                $('html, body').animate({
                    scrollTop: 0
                }, 'slow');
                that.factoryConseil();
            });

            prerequisitesHandler.unbind("click").bind("click", function (e) {
                $('html, body').animate({
                    scrollTop: 0
                }, 'slow');
                that.factoryPrerequisites();
            });

            calendarHandler.unbind("click").bind("click", function (e) {
                $('html, body').animate({
                    scrollTop: 0
                }, 'slow');
                that.factoryCalendar();
            });

            alertHandler.unbind("click").bind("click", function (e) {
                $('html, body').animate({
                    scrollTop: 0
                }, 'slow');
                that.factoryAlert();
            });

            candidaturesBtnHandler.unbind("click").bind("click", function (e) {
                $('html, body').animate({
                    scrollTop: 0
                }, 'slow');
                that.factoryCandidatures();
            });

            dashboardBtnHandler.unbind("click").bind("click", function (e) {
                $('html, body').animate({
                    scrollTop: 0
                }, 'slow');
                that.factoryDashBoard();
            });

            jobOfferBtnHandler.unbind("click").bind("click", function (e) {
                $('html, body').animate({
                    scrollTop: 0
                }, 'slow');
                that.factoryJobOffers();
            });

            visitorsBtnHandler.unbind("click").bind("click", function (e) {
                $('html, body').animate({
                    scrollTop: 0
                }, 'slow');
                that.factoryVisitors();
            });

            languagesOptionsBtnHandler.unbind("click").bind("click", function (e) {
                $('html, body').animate({
                    scrollTop: 0
                }, 'slow');
                that.factoryLanguages();
            });

            inboxBtnHandler.unbind("click").bind("click", function (e) {
                $('html, body').animate({
                    scrollTop: 0
                }, 'slow');
                that.factoryInbox();
            });

            $("#wrapper .widget-body .acc-menu li").bind("click", function(){
                $(this).closest("ul").find("li").removeClass("active");
                $(this).addClass("active");
                var menuTitle = $(this).find("span").text();
                $('.page-title-menu').text(menuTitle);
            });

            $('#stand').trigger('click');
            //todo
            //$("#notificationPage").load("notification.html");
            $('#divChat').load('chat.html');
            getRecruiterInfosChat();
        };

        this.factoryPrerequisites = function () {
            $(".tab-container").removeClass("show");
            $(".tab-container").addClass("hide");
            switch (getIdLangParam()) {
                case '3':
                    that.showTabNotChat('prerequisites_fr.html',
                            $(this));
                    break;
                case '2':
                    that.showTabNotChat('prerequisites_en.html',
                            $(this));
                    break;
                    /*case '1':
                     showTabNotChat('prerequisites_de.html',
                     $(this));
                     break;*/
                default:
                    break;
            }
            /*$('#loadingEds').hide();
             hideTabChat();*/
        };

        this.factoryCalendar = function () {
            that.showTabNotChat('calendar.html', $(this));
        };
        this.factoryAlert = function () {
            that.showTabNotChat('alert.html', $(this));
        };
        this.factoryDashBoard = function () {
            that.showTabNotChat('dashboard.html', $(this));
        };
        this.factoryConseil = function () {
            that.showTabNotChat('conseil.html', $(this));
        };
        this.factoryAccountOption = function () {
            that.showTabNotChat('account.html', $(this));
        };
        this.factoryStand = function () {
            that.showTabNotChat('stand.html', $(this));
        };
        this.factoryCandidatures = function () {
            that.showTabNotChat('candidatures.html', $(this));
        };
        this.factoryJobOffers = function () {
            that.showTabNotChat('jobOffer.html', $(this));
        };
        this.factoryVisitors = function () {
            that.showTabNotChat('visitorsSection.html', $(this));
        };
        this.factoryLanguages = function () {
            that.showTabNotChat('languages.html', $(this));
        };
        this.factoryInbox= function () {
            that.showTabNotChat('inbox.html', $(this));
        };

        this.factoryTchat= function () {
            console.log('inside factoryTchat');
            $('#mainBackEndContainer').hide();
            $('#divChat').show();
            $('#chatPage').removeClass("hide");
            isSettingsFilled();
        };
        this.showTabNotChat = function (file, tabElement) { 
            $('#divChat').hide();
            $('#loadingEds').show();
            $(".tab-container").removeClass("show");
            $(".tab-container").addClass("hide");
            $('#mainBackEndContainer').load(file);
            $('#mainBackEndContainer').show();
        };

        this.addListener = function (list) {
            listeners.push(list);
        };
    },
    HomeViewListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }

});
