jQuery.extend({
    LanguagesModel: function () {
        var that = this;
        var listeners = new Array();
        this.initModel = function () {
            //that.getFilledLanguages();
        };

        this.updateSubstitutions = function(languagesSubstitutionXml) {
            genericAjaxCall('POST', staticVars.urlBackEnd
                    + 'languaget/updateSubstitutions', 'application/xml', 'xml',
                    languagesSubstitutionXml, that.updateSubstitutionsSuccess,
                    that.updateSubstitutionsError);
        }

        this.updateSubstitutionsSuccess = function() {
            infoMessage('#langUpdated');
        }

        this.updateSubstitutionsError = function(xhr, status, error) {
            isSessionExpired(xhr.responseText);
        }

        this.getSubstitutionsSuccess = function(xml) {
            that.notifyGetSubstitutionsSuccess(xml);
        };

        this.getSubstitutionsError = function(xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.getSubstitutions = function () {
            genericAjaxCall('GET', staticVars.urlBackEnd + 'languaget/substitutions',
                'application/xml', 'xml', '', that.getSubstitutionsSuccess,
                that.getSubstitutionsError);
        };

        this.notifyGetSubstitutionsSuccess = function (xml) {
            $.each(listeners, function (i) {
                listeners[i].getSubstitutionsSuccess(xml);
            });
        };

        this.addListener = function (list) {
            listeners.push(list);
        };

    },
    AccountModelListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }
});
