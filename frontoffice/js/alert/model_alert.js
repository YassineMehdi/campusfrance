jQuery.extend({
    Publication: function (data) {
        var that = this;
        this.idPublication = $(data).find("idpublication").text();
        this.subjectPublication = $(data).find("subjectPublication").text();
        this.contentPublication = $(data).find("contentPublication").text();
        this.forumPublication = $(data).find("forumPublication").text();
        if (that.forumPublication == "true")
            that.forumPublication = true;
        else
            that.forumPublication = false;

        this.getIdPublication = function () {
            return that.idPublication;
        };
        this.setIdPublication = function (idPublication) {
            that.idPublication = idPublication;
        };

        this.getSubjectPublication = function () {
            return that.subjectPublication;
        };
        this.setSubjectPublication = function (subjectPublication) {
            that.subjectPublication = subjectPublication;
        };

        this.getContentPublication = function () {
            return that.contentPublication;
        };
        this.setContentPublication = function (contentPublication) {
            that.contentPublication = contentPublication;
        };

        this.getForumPublication = function () {
            return that.forumPublication;
        };
        this.setForumPublication = function (forumPublication) {
            that.forumPublication = forumPublication;
        };
        this.xmlData = function () {
            var xml = "<publication>"
                    + "<idpublication>" + that.idPublication + "</idpublication>"
                    + "<subjectPublication>" + htmlEncode(that.subjectPublication) + "</subjectPublication>"
                    + "<contentPublication>" + htmlEncode(that.contentPublication) + "</contentPublication>"
                    + "<forumPublication>" + that.forumPublication + "</forumPublication>"
                    + "</publication>";
            return xml;
        };
    },
    AlertModel: function () {
        var that = this;
        var listeners = new Array();
        this.initModel = function () {
            that.getPublications();
        };
        this.addListener = function (list) {
            listeners.push(list);
        };
        // listing Publication
        this.getPublicationsSuccess = function (xml) {
            var listPublications = new Array();
            $(xml).find("publication").each(function () {
                var publication = new $.Publication($(this));
                listPublications.push(publication);
            });
            that.notifyLoadPublications(listPublications);
        };

        this.getPublicationsError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.getPublications = function () {
            var url = staticVars["urlBackEnd"] + 'publication/allPublications?idLang=' + getIdLangParam();
            genericAjaxCall('GET', url, 'application/xml', 'xml',
                    '', that.getPublicationsSuccess, that.getPublicationsError);
        };
        // Add Publication
        this.addPublicationError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };
        this.addPublicationSuccess = function (xml) {
            //console.log(xml);
            that.notifyAddPublicationSuccess();
            that.getPublications();
        };
        this.modelAddPublication = function (publication) {
            var publicationVisibility = "forum";
            if (!publication.getForumPublication()) {
                publicationVisibility = "stand";
            }
            var url = 'publication/add?type=' + publicationVisibility + '&idLang=' + getIdLangParam();
            genericAjaxCall('POST', staticVars["urlBackEnd"] + url, 'application/xml', 'xml', publication.xmlData(),
                    that.addPublicationSuccess, that.addPublicationError);
        };

        // Update Publication
        this.updatePubSuccess = function (xml) {
            that.notifyUpdatePubSuccess();
            that.getPublications();
        };

        this.updatePubError = function (xhr, status, error) {
            that.notifyUpdatePubError();
        };

        this.modelUpdatePub = function (pub) {
            var url = staticVars.urlBackEnd + 'publication/updatepub';
            genericAjaxCall('POST', url, 'application/xml', 'xml', pub.xmlData(),
                    that.updatePubSuccess, that.updatePubError);
        };
        // Delete Publication
        this.deletePublicationError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };
        this.deletePublicationSuccess = function (xml) {
            that.notifyDeletePubSuccess();
            that.getPublications();
        };
        this.deletePublication = function (idPublication) {
            var url = staticVars["urlBackEnd"] + 'publication/delete?idPublication=' + idPublication;
            genericAjaxCall('POST', url, 'application/xml', 'xml', '',
                    that.deletePublicationSuccess, that.deletePublicationError);
        };

        this.notifyLoadPublications = function (listPublications) {
            $.each(listeners, function (i) {
                listeners[i].loadPublications(listPublications);
            });
        };

        this.notifyAddPublicationSuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].addPubSuccess();
            });
        };
        this.notifyUpdatePubSuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].updatePubSuccess();
            });
        };

        this.notifyUpdatePubError = function () {
            $.each(listeners, function (i) {
                listeners[i].updatePubError();
            });
        };
        this.notifyDeletePubSuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].deletePubSuccess();
            });
        };

    },
    AlertModelListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }
});
