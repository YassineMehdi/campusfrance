jQuery.extend({
    AccountController: function (model, view) {
        var that = this;
        /**
         * listen to the view
         */
        var vlist = $.AccountViewListener({
            updatePassword: function (enterpriseP) {
                model.updatePassword(enterpriseP);
            }
        });

        view.addListener(vlist);
        /**
         * listen to the model
         */
        var mlist = $.AccountModelListener({
            recruiterLoad: function (enterpriseP) {
                view.parseRecruiter(enterpriseP);
            },
            updatePasswordSuccess: function () {
                view.viewUpdatePasswordSuccess();
            },
            updatePasswordError: function (enterpriseP) {
                view.viewUpdatePasswordError(enterpriseP);
            }
        });

        model.addListener(mlist);

        this.initController = function () {
            view.initView();
            model.initModel();
        };
    }
});
