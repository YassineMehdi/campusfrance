jQuery.extend({
    AccountView: function () {
        var that = this;
        var listeners = new Array();
        var contentHandler = null;
        var saveAccountHandler = null;

        this.initView = function () {
            contentHandler = $("#mainBackEndContainer");
            saveAccountHandler = $(".saveAccountButton");
            //Custom checkboxes
            factorycheckbox();
            that.validateAccountForm();
            $('#account .changePassword').click(function (e) {
                if ($('.icheckbox_minimal-blue').hasClass('checked')) {
                    $('.passwordField').prop("disabled", false);
                    $('.saveAccountButton').removeClass('disabled');
                } else {
                    $('.passwordField').val("");
                    $('.passwordField').prop("disabled", true);
                    $('.saveAccountButton').addClass('disabled');

                }
            });
            // Update Password
            saveAccountHandler.unbind("click").bind("click", function () {
                that.updateEnterpriseP();
            });

        };
        //Recruiter Xmld
        this.parseRecruiter = function (enterpriseP) {
            //console.log(enterpriseP);
            //$('#recruiterLogin').val(enterpriseP.getEmail());
            $('#loadingEds').hide();
        };
        // Update Password
        this.updateEnterpriseP = function () {
            if ($("#accountForm").valid()) {
                var enterpriseP = new $.AccountProfil();
                enterpriseP.setPassword($.sha1($('#newPassword').val()));
                that.notifyUpdatePassword(enterpriseP);
            }
        };
        this.viewUpdatePasswordSuccess = function () {
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        this.viewUpdatePasswordError = function () {
            contentHandler.prepend("<div class='col-md-5 alertEvent alert alert-dismissable alert-danger'><i class='fa fa-fw fa-times'></i>&nbsp; "+ERRORACCOUNTLABEL+"<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button></div>");
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        //Custom checkboxes
        this.validateAccountForm = function () {
            $("#accountForm").validate({
                rules: {
                    newPassword: {
                        required: true,
                        minlength: 6
                    },
                    passwordConfirmation: {
                        equalTo: '#newPassword',
                    }
                }
            });
        };
        this.addListener = function (list) {
            listeners.push(list);
        };
        this.notifyUpdatePassword = function (enterpriseP) {
            $.each(listeners, function (i) {
                listeners[i].updatePassword(enterpriseP);
            });
        };
    },
    AccountViewListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }



});
