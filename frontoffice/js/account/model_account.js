jQuery.extend({
    AccountProfil: function (data) {
        var that = this;
        that.email = $(data).find("email").text();
        that.password = $(data).find("password").text();

        this.getEmail = function () {
            return this.email;
        };
        this.setEmail = function (email) {
            that.email = email;
        };
        this.getPassword = function () {
            return this.password;
        };
        this.setPassword = function (password) {
            that.password = password;
        };

        this.xmlData = function () {
            var xml = '<enterprisep>';
            xml += '<email>' + htmlEncode(that.email) + '</email>';
            xml += '<password>' + htmlEncode(that.password) + '</password>';
            xml += '</enterprisep>';
            return xml;
        };
    },
    AccountModel: function () {
        var that = this;
        var listeners = new Array();
        this.initModel = function () {
            that.getRecruiterInfos();
        };
        // Info profil
        this.getRecruiterSuccess = function (xml) {
            var enterpriseP = new $.EnterpriseP(xml);
            that.notifyRecruiterSuccess(enterpriseP);
        };

        this.getRecruiterError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };
        this.getRecruiterInfos = function () {
//            genericAjaxCall('GET', staticVars.urlBackEnd + 'enterprisep/getInfos',
//                    'application/xml', 'xml', '', that.getRecruiterSuccess, that.getRecruiterError);
        };
        // Update Password
        this.updatePasswordSuccess = function (xml) {
            that.notifyUpdatePasswordSuccess();
        };

        this.updatePasswordError = function (xhr, status, error) {
            that.notifyUpdatePasswordError();
        };
        this.updatePassword = function (enterpriseP) {
            console.log(enterpriseP);
            genericAjaxCall('POST', staticVars.urlBackEnd + 'enterprisep/update/account',
                    'application/xml', 'xml', enterpriseP.xmlData(),
                    that.updatePasswordSuccess, that.updatePasswordError);
        }

        this.addListener = function (list) {
            listeners.push(list);
        };

        this.notifyRecruiterSuccess = function (enterpriseP) {
            $.each(listeners, function (i) {
                listeners[i].recruiterLoad(enterpriseP);
            });
        };

        this.notifyUpdatePasswordSuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].updatePasswordSuccess();
            });
        };

        this.notifyUpdatePasswordError = function () {
            $.each(listeners, function (i) {
                listeners[i].updatePasswordError();
            });
        };


    },
    AccountModelListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }
});
