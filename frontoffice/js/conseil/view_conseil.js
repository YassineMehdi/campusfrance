jQuery.extend({
    ConseilView: function () {
        var that = this;
        var listeners = new Array();
        var advicesCache = [];
        var addadviceBtnHandler = null;
        var adviceFormHandler = null;
        var adviceCheckboxHandler = null;
        var saveAdviceHandler = null;
        var titleAdviceHandler = null;
        var contentAdviceHandler = null;
        var fileinputHandler = null;
        var fileinputHandler = null;
        var fileinputHandler = null;
        var modalAdviceHandler = null;
        var contentHandler = null;
        var loadAdviceErrorHandler = null;
        var progressAdviceHandler = null;
        var btnDeleteAdviceHandler = null;


        this.initView = function () {
            contentHandler = $("#mainBackEndContainer");
            modalAdviceHandler = $("#addAdvice");
            addadviceBtnHandler = $(".addAdviceLabel");
            adviceFormHandler = $("#adviceForm");
            adviceCheckboxHandler = $(".choiceBox");
            saveAdviceHandler = $("#saveAdvice");
            titleAdviceHandler = $("#titleAdvice");
            contentAdviceHandler = $("#contentAdvice");
            fileinputHandler = $(".fileinput-filename");
            loadAdviceErrorHandler = $("#loadAdviceError");
            progressAdviceHandler = $("#progressAdvice");
            btnDeleteAdviceHandler = $("#btnDeleteAdvice");
            //file input
            $("#adviceUpload").fileupload({
                beforeSend: function (jqXHR, settings) {
                    beforeUploadFile('#saveAdvice', '#progressAdvice', '#loadAdviceError');
                },
                datatype: 'json',
                cache: false,
                add: function (e, data) {
                    var goUpload = true;
                    var uploadFile = data.files[0];
                    var fileName = uploadFile.name;
                    timeUploadAdvice = (new Date()).getTime();
                    console.log(timeUploadAdvice);
                    data.url = getUrlPostUploadFile(timeUploadAdvice, fileName);
                    if (!(/\.(pdf)$/i).test(fileName)) {
                        goUpload = false;

                    }
                    if (uploadFile.size > MAXFILESIZE) {
                        goUpload = false;
                    }
                    if (goUpload == true) {
                        jqXHRAdvice = data.submit();
                    } else
                        $('#loadAdviceError').show();
                },
                progressall: function (e, data) {
                    progressBarUploadFile('#progressAdvice', data, 99);
                },
                done: function (e, data) {
                    var file = data.files[0];
                    var fileName = file.name;
                    console.log(timeUploadAdvice);
                    var hashFileName = getUploadFileName(timeUploadAdvice, fileName);
                    console.log("Add advice file done, filename :" + fileName + ", hashFileName : " + hashFileName);
                    $('#loadAdviceError').show();
                    $('.fileInfo').text(fileName);
                    $('.fileInfo').attr('href', getUploadFileUrl(hashFileName));
                    afterUploadFile('#saveAdvice', '#adviceLoaded', '#progressAdvice,#loadAdviceError');
                }
            });
            //save advice
            saveAdviceHandler.unbind("click").bind("click", function (e) {
                that.saveAdvice();
            });
            //Choice advice
            adviceCheckboxHandler.unbind("click").bind("click", function (e) {
                that.factoryChoiceCheckbox();
            });
            //Delete Advice
            btnDeleteAdviceHandler.unbind("click").bind("click", function (e) {
                that.notifyDeleteAdvice($('#idDeleteAdvice').val());
            });


            // Wijets
            //$.wijets().make();
            var oTable = $('#listAdvicesTable').dataTable({
                "language": {
                    url: 'plugins/datatables/'+getIdLangParam()+'.json'
                },
                "aaData": [],
                "aaSorting": [],
                "aoColumns": [
                    {
                        "sTitle": TITLELABEL
                    },
                    {
                        "sTitle": CONTENTLABEL
                    },
                    {
                        "sTitle": ACTIONSLABEL
                    }
                ],
                "bFilter": false,
                "bAutoWidth": false,
                "bRetrieve": false,
                "bDestroy": true,
                "iDisplayLength": 10,
                "fnDrawCallback": function () {
                    $('.viewAdviceLink').off();
                    $('.viewAdviceLink').on("click", function () {
                        var adviceId = $(this).closest('div').attr('id');
                        that.factoryViewAdvice(advicesCache[adviceId]);

                    });

                    $('.editAdviceLink').off();
                    $('.editAdviceLink').on("click", function () {
                        var adviceId = $(this).closest('div').attr('id');
                        that.factoryUpdateAdvice(advicesCache[adviceId]);
                    });

                    $('.deleteAdviceLink').off();
                    $('.deleteAdviceLink').on("click", function () {
                        var adviceId = $(this).closest('div').attr('id');
                        $('#removeAdvice').modal();
                        $('#idDeleteAdvice').val(adviceId);
                        //that.notifyDeleteAdvice(adviceId);
                    });
                }
            });

            var tableTools = new $.fn.dataTable.TableTools(oTable, {
                "buttons": [
                    //"copy",
                    //"csv",
                    //"xls",
                    //"pdf",
                    {"type": "print", "buttonText": "Print me!"}
                ],
                "sSwfPath": "assets/plugins/datatables/TableTools/swf/copy_csv_xls_pdf.swf"
            });



            /*DOM Manipulation to move datatable elements integrate to panel
            $('#panel-tabletools .panel-ctrls').append($('.dataTables_filter').addClass("pull-right"));
            $('#panel-tabletools .panel-ctrls .dataTables_filter').find("label").addClass("panel-ctrls-center");

            $('#panel-tabletools .panel-ctrls').append("<i class='separator pull-right '></i>");

            $('#panel-tabletools .panel-ctrls').append($('.dataTables_length').addClass("pull-right"));
            $('#panel-tabletools .panel-ctrls .dataTables_length label').addClass("mb0");

            $('#panel-tabletools .panel-ctrls').append("<i class='separator pull-right '></i>");
            $(tableTools.fnContainer()).appendTo('#panel-tabletools .panel-ctrls').addClass("pull-right mt10");

            $('#panel-tabletools .panel-footer').append($(".dataTable+.row"));
            $('.dataTables_paginate>ul.pagination').addClass("pull-right");*/

            bindCheckMaxLength(titleAdviceHandler, 40);
            bindCheckMaxLength(contentAdviceHandler, 200);

        };
        this.displayAdvices = function (advices) {
            advicesCache = advices;

            var rightGroup = listStandRights[rightGroupEnum.ADVICES];
            if (rightGroup != null) {
                var permittedAdvicesNbr = rightGroup.getRightCount();
                var advicesNbr = Object.keys(advices).length;
                if (permittedAdvicesNbr == -1) {
                    $('#addAdviceDiv #addAdviceLink').text(ADDLABEL);
                    $('#addAdviceDiv').show();
                } else if (advicesNbr < permittedAdvicesNbr) {
                    $('#numberAdvices').text(advicesNbr);
                    $('#numberPermittedAdvices').text(permittedAdvicesNbr);
                    $('#addAdviceDiv').show();
                } else {
                    $('#addAdviceDiv').hide();
                }
            }

            var adviceList = [];
            var advice = null;
            var adviceId = null;
            var adviceTitle = null;
            var formatAdviceTitle = null;
            var customAdviceTitle = null;
            var htmlAdviceTitle = null;
            var adviceContent = null;
            var listActions = null;
            var data = null;

            //var listActions = "<a href='javascript:void(0);' class='editAdviceLink actionBtn'><i class='fa fa-pencil-square-o'></i></a> <a href='javascript:void(0);' class='viewAdviceLink actionBtn'><i class='fa fa-eye'></i></a> <a href='javascript:void(0);' class='deleteAdviceLink actionBtn'><i class='fa fa-trash-o'></i></a>";
            for (var index in advices) {
                advice = advices[index];
                adviceId = advice.getIdAdvice();
                adviceTitle = advice.getTitle();
                formatAdviceTitle = formattingLongWords(adviceTitle, 70);
                customAdviceTitle = substringCustom(formatAdviceTitle, 200);
                htmlAdviceTitle = htmlEncode(customAdviceTitle);
                if (advice.getUrl().length != 0)
                    adviceContent = unescape(advice.fileName);
                else
                    adviceContent = unescape(advice.getContent());
                listActions = "<div id='" + adviceId + "'> <a href='javascript:void(0);' class='viewAdviceLink actionBtn viewLink'><i class='fa fa-eye'></i></a><a href='javascript:void(0);' class='editAdviceLink actionBtn'><i class='fa fa-pencil-square-o'></i></a> <a href='javascript:void(0);' class='deleteAdviceLink actionBtn deleteLink'><i class='fa fa-trash-o'></i></a></div>";
                data = [htmlAdviceTitle,
                    "<div class='contentTruncate'>" + adviceContent.replace(/<(?:.|\n)*?>/gm, '') + "</div>",
                    listActions
                ];
                adviceList.push(data);
            }
            addTableDatas("#listAdvicesTable", adviceList);

            //plugin which allow as to truncate text
            $('.contentTruncate').expander({
                slicePoint: 80, // default is 100
                expandPrefix: ' ', // default is '... '
                expandText: '[...]', // default is 'read more'
                //collapseTimer:    5000, // re-collapses after 5 seconds; default is 0, so no re-collapsing
                //userCollapseText: '[^]'  // default is 'read less'
            });

            //Add Advice
            addadviceBtnHandler.unbind("click").bind("click", function (e) {
                that.factoryAddAdvice();
            });
            $('#loadingEds').hide();
        };
        //Add Advice
        this.factoryAddAdvice = function () {
            that.update = false;
            modalAdviceHandler.modal();
            loadAdviceErrorHandler.hide();
            progressAdviceHandler.hide();
            adviceFormHandler[0].reset();
            $('#uploadDivContent').show();
            $('#uploadOptionDiv').hide();
            $('.fileInfo').text("").attr('href', '');
            if ($('#textOption').is(":checked")) {
                $('#uploadDivContent').show();
                $('#uploadOptionDiv').hide();
            } else {
                $('#uploadDivContent').hide();
                $('#uploadOptionDiv').show();
            }
            if ($(".errorValidateField") != undefined) {
                $(".errorValidateField").remove();
            }
        };
        this.factoryChoiceCheckbox = function () {
            if ($('#textOption').is(":checked")) {
                $('#uploadDivContent').show();
                $('#uploadOptionDiv').hide();
            } else {
                $('#uploadDivContent').hide();
                $('#uploadOptionDiv').show();
            }
        };

        this.addAdviceSuccess = function () {
            modalAdviceHandler.modal('hide');
            modalAdviceHandler.find('#adviceForm')[0].reset();
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        this.saveAdvice = function () {
            if ($(".errorValidateField") != undefined) {
                $(".errorValidateField").remove();
            }
            var isValid = true;
            var title = titleAdviceHandler.val();
            var content = '';
            var nameDocument = '';
            var urlDocument = '';

            if (title == "") {
                titleAdviceHandler.after('<label class="error errorValidateField"> ' + REQUIREDLABEL + ' </label>');
                isValid = false;
            }
            if ($('#textOption').is(":checked")) {
                content = contentAdviceHandler.val();
                if (content == "") {
                    contentAdviceHandler.after('<label class="error errorValidateField"> ' + REQUIREDLABEL + ' </label>');
                    isValid = false;
                }
            } else {
                nameDocument = $('.fileInfo').text();
                urlDocument = $('.fileInfo').attr("href");
                if (nameDocument == "" || urlDocument == "") {
                    fileinputHandler.after('<label class="error errorValidateField">' + REQUIREDLABEL + '</label>');
                    isValid = false;
                }
            }

            if (isValid == true) {
                //$("#preloaderCustom").show();
                var advice = new $.Advice();
                advice.setTitle(title);
                advice.setContent(content);
                advice.setFileName(nameDocument);
                advice.setUrl(urlDocument);


                if (!that.update) {
                    that.notifyAddAdvice(advice);
                }
                else {
                    advice.setAdviceId($('#idAdvice').val());
                    that.notifyUpdateAdvice(advice);
                }
            }
        };
        // Update Advice
        this.factoryUpdateAdvice = function (advice) {
            that.update = true;
            $('#loadAdviceError').hide();
            $('.errorValidateField').hide();
            $('#addAdvice').modal();
            $('#idAdvice').val(advice.getIdAdvice());
            titleAdviceHandler.val(advice.getTitle());
            contentAdviceHandler.val(unescape(advice.getContent()));
            $('.fileInfo').text(advice.getFileName());
            $('.fileInfo').attr("href", advice.getUrl());
            if (advice.getUrl().length != 0) {
                $('#uploadOption').click();
            } else {
                $('#textOption').click();
            }
            that.factoryChoiceCheckbox();
        };

        this.viewUpdateAdviceSuccess = function () {
            modalAdviceHandler.modal('hide');
            modalAdviceHandler.find('#adviceForm')[0].reset();
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        this.viewUpdateAdviceError = function () {
            modalAdviceHandler.modal('hide');
            modalAdviceHandler.find('#adviceForm')[0].reset();
            contentHandler.prepend("<div class='col-md-5 alertEvent alert alert-dismissable alert-danger'><i class='fa fa-fw fa-times'></i>&nbsp; "+ERRORACCOUNTLABEL+"<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button></div>");
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        // View Advice
        this.factoryViewAdvice = function (advice) {
            //var advice = listAdvices[adviceId];
            var adviceId = advice.getIdAdvice();
            var adviceTitle = advice.getTitle();
            var adviceUrl = advice.getUrl();
            var adviceContent = advice.getContent();
            $('#viewAdvice').modal();
            $('#documentTitlePopup').text(adviceTitle);
//            $('.fileInfo').text(advice.getFileName());
//            $('.fileInfo').attr("href", advice.getUrl());

            if (advice.getUrl().length != 0) {
                var img_container = $('<object data="mozilla-pdf.js/web/viewer.html?file=' + adviceUrl + '" type="text/html" width="100%" height="100%"></object>');
                $('#contentViewAdvice').html(img_container);
            } else {
                $('#contentViewAdvice').html(unescape(adviceContent));
            }
        };
        // Delete Advice
        this.viewDeleteAdviceSuccess = function () {
            modalAdviceHandler.modal('hide');
            modalAdviceHandler.find('#adviceForm')[0].reset();
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        this.viewDeleteAdviceError = function () {
            modalAdviceHandler.modal('hide');
            modalAdviceHandler.find('#adviceForm')[0].reset();
            contentHandler.prepend("<div class='col-md-5 alertEvent alert alert-dismissable alert-danger'><i class='fa fa-fw fa-times'></i>&nbsp; "+ERRORACCOUNTLABEL+"<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button></div>");
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        this.addListener = function (list) {
            listeners.push(list);
        };

        this.notifyAddAdvice = function (advice) {
            $.each(listeners, function (i) {
                listeners[i].addAdvice(advice);
            });
        };
        this.notifyUpdateAdvice = function (advice) {
            $.each(listeners, function (i) {
                listeners[i].updateAdvice(advice);
            });
        };
        this.notifyDeleteAdvice = function (adviceId) {
            $.each(listeners, function (i) {
                listeners[i].deleteAdvice(adviceId);
            });
        };

    },
    ConseilViewListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }



});
