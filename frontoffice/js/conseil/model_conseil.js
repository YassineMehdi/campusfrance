jQuery.extend({
    Advice: function (data) {
        var that = this;

        this.idAdvice = $(data).find("idAdvice").text();
        this.title = $(data).find("title").text();
        this.content = $(data).find("content").text();
        this.url = $(data).find("url").text();
        this.fileName = htmlDecode($(data).find("fileName").text());
        this.getIdAdvice = function () {
            return this.idAdvice;
        };
        this.getTitle = function () {
            return this.title;
        };
        this.getContent = function () {
            return this.content;
        };
        this.getUrl = function () {
            return this.url;
        };
        this.getFileName = function () {
            return this.fileName;
        };
        this.setAdviceId = function (idAdvice) {
            that.idAdvice = idAdvice;
        };
        this.setTitle = function (title) {
            that.title = title;
        };
        this.setContent = function (content) {
            that.content = content;
        };
        this.setUrl = function (url) {
            that.url = url;
        };
        this.setFileName = function (fileName) {
            that.fileName = fileName;
        };

        this.xmlData = function () {
            var xml = '<advice>';
            xml += '<idAdvice>' + that.idAdvice + '</idAdvice>';
            xml += '<title>' + htmlEncode(that.title) + '</title>';
            xml += '<content>' + htmlEncode(that.content) + '</content>';
            xml += '<url>' + that.url + '</url>';
            xml += '<fileName>' + htmlEncode(that.fileName) + '</fileName>';
            xml += '</advice>';
            return xml;
        };
    },
    ConseilModel: function () {
        var that = this;
        var listeners = new Array();
        this.initModel = function () {
            that.getStandAdvices();
        };

        this.getStandAdvicesSuccess = function (xml) {
            var listAdvices = new Array();
            $(xml).find("advice").each(function () {
                var advice = new $.Advice($(this));
                listAdvices[advice.idAdvice] = advice;
            });
            that.notifyLoadAdvices(listAdvices);
        };

        this.getStandAdvicesError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };


        this.getStandAdvices = function () {
            genericAjaxCall('GET', staticVars.urlBackEnd
                    + 'advice/standAdvices?idLang=' + getIdLangParam(), 'application/xml', 'xml',
                    '', that.getStandAdvicesSuccess, that.getStandAdvicesError);
        };

        this.addAdviceSuccess = function () {
            that.notifyAddAdviceSuccess();
            that.getStandAdvices();
        };

        this.addAdviceError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };
        this.addAdvice = function (advice) {
            genericAjaxCall('POST', staticVars.urlBackEnd
                    + 'advice/addOrUpdateAdvice?idLang=' + getIdLangParam(), 'application/xml', 'xml',
                    advice.xmlData(), that.addAdviceSuccess, that.addAdviceError);
        };

        // Update Advice
        this.updateAdviceSuccess = function (xml) {
            that.notifyUpdateAdviceSuccess();
            that.getStandAdvices();
        };

        this.updateAdviceError = function (xhr, status, error) {
            that.notifyUpdateAdviceError();
        };

        this.modelUpdateAdvice = function (advice) {
            genericAjaxCall('POST', staticVars.urlBackEnd
                    + 'advice/addOrUpdateAdvice?idLang=' + getIdLangParam(), 'application/xml', 'xml',
                    advice.xmlData(), that.updateAdviceSuccess, that.updateAdviceError);
        };
        // View Advice
        this.viewAdvice = function () {
            that.notifyViewAdvice();
        };
        // Delete Advice
        this.deleteAdviceSuccess = function () {
            that.notifyDeleteAdviceSuccess();
            that.getStandAdvices();
        };

        this.deleteAdviceError = function (xhr, status, error) {
            isSessionExpired(xhr.responseText);
        };

        this.modelDeleteAdvice = function (adviceId) {
            genericAjaxCall('POST', staticVars.urlBackEnd
                    + 'advice/deleteAdvice', 'application/xml', 'xml',
                    adviceId, that.deleteAdviceSuccess, that.deleteAdviceError);
        };
        
        this.getAnalyticsAdvices = function(callBack, path) {
            // console.log('getAnalyticsAdvices : '+path);
            genericAjaxCall('GET', staticVars.urlBackEnd + path,
                    'application/xml', 'xml', '', function(xml) {
                        listAdvicesCache = new Array();
                        $(xml).find("advice").each(function() {
                            var advice = new $.Advice($(this));
                            listAdvicesCache[advice.idAdvice] = advice;
                        });
                        callBack();
                    }, that.getStandAdvicesError);
        };

        this.addListener = function (list) {
            listeners.push(list);
        };

        this.notifyLoadAdvices = function (listAdvices) {
            $.each(listeners, function (i) {
                listeners[i].loadAdvices(listAdvices);
            });
        };

        this.notifyAddAdviceSuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].addAdviceSuccess();
            });
        };

        this.notifyUpdateAdviceSuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].updateAdviceSuccess();
            });
        };

        this.notifyUpdateAdviceError = function () {
            $.each(listeners, function (i) {
                listeners[i].updateAdviceError();
            });
        };

        this.notifyDeleteAdviceSuccess = function () {
            $.each(listeners, function (i) {
                listeners[i].deleteAdviceSuccess();
            });
        };

        this.notifyDeleteAdviceError = function () {
            $.each(listeners, function (i) {
                listeners[i].deleteAdviceError();
            });
        };

        this.notifyViewAdvice = function () {
            $.each(listeners, function (i) {
                listeners[i].viewAdvice();
            });
        };
    },
    ConseilModelListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }
});
