jQuery.extend({
    CalendarView: function () {
        var that = this;
        var listeners = new Array();
        var fullCalModalHandler = null;
        var btnAddEventHandler = null;
        var saveEventHandler = null;
        var titleEventHandler = null;
        var startEventHandler = null;
        var endEventHandler = null;
        var contentHandler = null;
        var btnDeleteEventHandler = null;
        that.update = false;
        this.initView = function () {
            fullCalModalHandler = $("#fullCalModal");
            btnAddEventHandler = $("#btnAddEvent");
            saveEventHandler = $("#saveEvent");
            titleEventHandler = $("#titleEvent");
            startEventHandler = $("#startEvent");
            endEventHandler = $("#endEvent");
            contentHandler = $("#mainBackEndContainer");
            btnDeleteEventHandler = $("#btnDeleteEvent");
            //Save Event
            saveEventHandler.unbind("click").bind("click", function (e) {
                that.saveEvent();
            });
            //Eternicode Datepicker
            $('#startEvent, #endEvent').datetimepicker({
                format: 'yyyy-mm-dd hh:ii'
            });
            //Delete Event
            btnDeleteEventHandler.unbind("click").bind("click", function (e) {
                that.notifyDeleteEvent($('#idDeleteEvent').val());
            });
            // FullCalendar with Drag/Drop internal
            var calendar = $('#calendar-drag').fullCalendar({
                lang: LANGUAGECURRENTLABEL,
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                selectable: true,
                selectHelper: true,
                select: that.factorySelectEvent,
                eventRender: function (event, element) {
                    var closeBtnHandler = $('<button type="button" class="close" data-toggle="modal" data-target="#removeEvent">×</button>');
                    element.find('.fc-content').append(closeBtnHandler);
                },
                eventClick: function (eventjson, jsEvent, view) {
                    //console.log(jsEvent);
                    //console.log(view);
                    //console.log(jsEvent.target);
                    if (!$(jsEvent.target).hasClass("close")) {
                        that.update = true;
                        $('#modalTitle').html();
                        $('#modalBody').html();
                        $('#fullCalModal').modal();
                        titleEventHandler.val(eventjson.title);
                        $('#eventDescription').val(eventjson.description);
                        $('#startEvent').datetimepicker('setDate', eventjson.start);
                        $('#endEvent').datetimepicker('setDate', eventjson.end);
                        $('#idEventCalendar').val(eventjson.id);
                    } else {
                        $('#idDeleteEvent').val(eventjson.id);
                    }
                },
                editable: true,
                /*buttonText: {
                    prev: '<i class="fa fa-angle-left"></i>',
                    next: '<i class="fa fa-angle-right"></i>',
                    prevYear: '<i class="fa fa-angle-double-left"></i>', // <<
                    nextYear: '<i class="fa fa-angle-double-right"></i>', // >>
                    today: '<span class="todayLabel"></span>',
                    month: '<span class="monthLabel"></span>',
                    week: '<span class="weekLabel"></span>',
                    day: '<span class="dayLabel"></span>'
                }*/
            });
            // FullCalendar with Drag/Drop internal
            //Add Events Btn
            btnAddEventHandler.unbind("click").bind("click", function (e) {
                that.factorySelectEvent();
            });
            traduct();
        };
        this.viewAddEventSuccess = function () {
            fullCalModalHandler.modal('hide');
            fullCalModalHandler.find('#addEventForm')[0].reset();
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        this.viewUpdateEventSuccess = function () {
            fullCalModalHandler.modal('hide');
            //fullCalModalHandler.find('#addEventForm')[0].reset();
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        this.viewUpdateEventError = function () {
            fullCalModalHandler.modal('hide');
            //fullCalModalHandler.find('#addEventForm')[0].reset();
            contentHandler.prepend("<div class='col-md-5 alertEvent alert alert-dismissable alert-danger'><i class='fa fa-fw fa-times'></i>&nbsp; "+ERRORACCOUNTLABEL+"<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button></div>");
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        this.viewDeleteEventSuccess = function () {
            fullCalModalHandler.modal('hide');
            //fullCalModalHandler.find('#addEventForm')[0].reset();
            contentHandler.prepend('<div class="col-md-5 alertEvent alert alert-dismissable alert-success"><i class="fa fa-fw fa-times"></i>&nbsp; '+EVENTLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        this.viewDeleteEventError = function () {
            fullCalModalHandler.modal('hide');
            //fullCalModalHandler.find('#addEventForm')[0].reset();
            contentHandler.prepend("<div class='col-md-5 alertEvent alert alert-dismissable alert-danger'><i class='fa fa-fw fa-times'></i>&nbsp; "+ERRORACCOUNTLABEL+"<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button></div>");
            contentHandler.find('.alertEvent').fadeOut(7000);
        };
        this.viewListEventNotify = function (json) {
            var eventsJson = convertAgendaEventsData(json);
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();
            $('#calendar-drag').fullCalendar('removeEvents');
            $('#calendar-drag').fullCalendar('addEventSource', eventsJson);
            $('#loadingEds').hide();
        };
        this.saveEvent = function () {
            if ($(".errorValidateField") != undefined) {
                $(".errorValidateField").remove();
            }
            var isValid = true;
            if (titleEventHandler.val() == "") {
                titleEventHandler.after('<label class="error errorValidateField"> '+REQUIREDLABEL+' </label>');
                isValid = false;
            }

            if (that.factoryValidateDate() && isValid == true) {
                //$("#preloaderCustom").show();
                var event = new $.EventAgenda();
                event.setEventTitle(titleEventHandler.val());
                event.setEventType($('#eventType').val());
                event.setEventDescription($('#eventDescription').val());
                event.setBeginDate($('#startEvent').datetimepicker('getDate'));
                event.setEndDate($('#endEvent').datetimepicker('getDate'));
                if (!that.update) {
                    that.notifyAddEvent(event);
                } else {
                    event.setIdEvent($('#idEventCalendar').val());
                    that.notifyUpdateEvent(event);
                }
                ;
            }
        };
        this.factorySelectEvent = function (start, end, allDay) {
            that.update = false;
            $('#modalTitle').html();
            $('#modalBody').html();
            $('#fullCalModal').modal();
            fullCalModalHandler.find('#addEventForm')[0].reset();
        };
        this.factoryValidateDate = function () {
            var isValidDate = true;
            if (startEventHandler.val() == "") {
                startEventHandler.after('<label class="error errorValidateField requiredLabel">'+REQUIREDLABEL+'</label>');
                isValidDate = false;
            }

            if (endEventHandler.val() == "") {
                endEventHandler.after('<label class="error errorValidateField requiredLabel"> '+REQUIREDLABEL+' </label>');
                isValidDate = false;
            }
            if (isValidDate) {
                var startEventDate = stringToDate($('#startEvent').val());
                var endEventDate = stringToDate($('#endEvent').val());
//                var startEventDate = $('#startEvent').datetimepicker('getDate');
//                var endEventDate = $('#endEvent').datetimepicker('getDate');

                if (!validateDateChamp(startEventDate)) {
                    startEventHandler.after('<label class="error errorValidateField">'+DATEINVALIDLABEL+'</label>');
                    isValidDate = false;
                }
                if (!validateDateChamp(endEventDate)) {
                    endEventHandler.after('<label class="error errorValidateField">'+DATEINVALIDLABEL+' </label>');
                    isValidDate = false;
                }
                if (isValidDate) {
                    if (startEventDate.valueOf() >= endEventDate.valueOf()) {
                        endEventHandler.after('<label class="error errorValidateField">'+LOWERDATELABEL+'</label>');
                        isValidDate = false;
                    }
                }
            }


            return isValidDate;
        };
        this.addListener = function (list) {
            listeners.push(list);
        };
        this.notifyAddEvent = function (event) {
            $.each(listeners, function (i) {
                listeners[i].addEvent(event);
            });
        };
        this.notifyUpdateEvent = function (event) {
            $.each(listeners, function (i) {
                listeners[i].updateEvent(event);
            });
        };
        this.notifyDeleteEvent = function (idEvent) {
            $.each(listeners, function (i) {
                listeners[i].deleteEvent(idEvent);
            });
        };
    },
    CalendarViewListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }



});
