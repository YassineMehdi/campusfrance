jQuery.extend({
    EnterpriseP: function (data) {
        that = this;
        this.login = $(data).find("login").text();
        this.password = $(data).find("password").text();
        this.getLogin = function () {
            return that.login;
        };
        this.setLogin = function (login) {
            that.login = login;
        };
        this.getPassword = function () {
            return that.password;
        };
        this.setPassword = function (password) {
            that.password = password;
        };
        this.xmlData = function () {
            var xml = '<enterprisep>';
            xml += '<login>' + htmlEncode(that.login) + '</login>';
            xml += '<password>' + htmlEncode(that.password) + '</password>';
            xml += '</enterprisep>';
            return xml;
        };
    },
    AuthentificationModel: function () {
        var that = this;
        var listeners = new Array();
        this.initModel = function () {
        };

        this.authentificateSuccess = function (xml) {
            //document.location.href = "settings.html";
//        	console.log("success**************");
//        	console.log(xml);
           /* $.cookie("enterpriseID", $(xml).find('enterpriseID')
                    .text());
            $.cookie("groupId", $(xml).find('groupId').text());
            $.cookie("enterprisePId", $(xml).find('enterprisePId')
                    .text());
            $.cookie("standId", $(xml).find('standId').text());
            var token = $(xml).find('token').text();
            if (token == null || token == "null")
                token = "";
            $.cookie("token", token);*/
//            if ($(xml).find('supAdmin').text() == 'true'){
            	window.location.replace("index.html?idLang=" + getIdLangParam());
//                console.log("SupAdmin**********");
//            }else console.log('Simple admin');
                //window.location.replace("chats.html?idLang=" + getIdLangParam());

            //$('#loadingEds').hide();
        };

        this.authentificateError = function (xhr, status, error) {
            //$.cookie('keeping', false);
        	that.notifyAuthentificationError(xhr, status, error);
        };

        this.authentificate = function (login, pwd) {
            //$.cookie('keeping', loginkeeping);
            pwd = $.sha1(pwd);
            var url = staticVars.urlBackEndManager + 'admin/login?login='+htmlEncode(login)+'&password='+htmlEncode(pwd);
            genericAjaxCall('POST', url, 'application/xml', 'xml', '',
                    that.authentificateSuccess, that.authentificateError);
        };

        this.addListener = function (list) {
            listeners.push(list);
        };
        this.notifyAuthentificationError = function (xhr, status, error) {
            $.each(listeners, function (i) {
                listeners[i].authentificationError(xhr, status, error);
            });
        };
    },
    ModelListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }
});
