jQuery.extend({
    AuthentificationController: function (model, view) {
        var that = this;
        /**
         * listen to the view
         */
        var vlist = $.ViewListener({
            authentificate: function (userName, pwd) {
                model.authentificate(userName, pwd);
            }
        });
        view.addListener(vlist);

        /**
         * listen to the model
         */
        var mlist = $.ModelListener({
            authentificationError: function (xhr, status, error) {
                view.authentificationError(xhr, status, error);
            }
        });

        model.addListener(mlist);

        this.initController = function () {
            view.initView();
            model.initModel();
        };
    }
});
