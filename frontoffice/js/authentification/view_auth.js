jQuery.extend({
    AuthentificationView: function () {
        var that = this;
        var listeners = new Array();
        var formHandler = $("#loginform");
        var loginBtnHandler = $("#loginBtn");
        var userNameHandler = $("#userName");
        var passwordHandler = $("#password");
        var langueHandler = $("#languageSelect");

        this.initView = function () {
        	
            langueHandler.val(getIdLangParam());

            loginBtnHandler.unbind("click").bind("click", function (e) {
                that.factoryLogin();
            });

            userNameHandler.unbind("keydown").bind("keydown", function (e) {
                if (e.keyCode == 13) {
                    that.factoryLogin();
                }
            });
            passwordHandler.unbind("keydown").bind("keydown", function (e) {
                if (e.keyCode == 13) {
                    that.factoryLogin();
                }
            });
            langueHandler.on('change', function (e) {
                var idLang = $(this).val();
                window.location.href = "login.html?idLang=" + idLang;
            });
        };

        this.authentificationError = function (xhr, status, error) {
            formHandler.prepend('<div class="col-md-6 col-md-offset-3 alertlogin alert alert-dismissable alert-danger"><i class="fa fa-fw fa-times"></i>&nbsp; '+ERRORCONNECTIONLABEL+'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>');
            $('#loadingEds').hide();
        };

        this.factoryLogin = function () {
            $('#loadingEds').show();
            if ($(".errorValidateField") != undefined) {
                $(".errorValidateField").remove();
            }
            if ($(".alertlogin") != undefined) {
                $(".alertlogin").remove();
            }
            var isValid = true;

            if (userNameHandler.val() == "") {
                userNameHandler.after('<label class="error errorValidateField">' + REQUIREDLABEL + '</label>');
                isValid = false;
            }

            if (passwordHandler.val() == "") {
                passwordHandler.after('<label class="error errorValidateField">' + REQUIREDLABEL + '</label>');
                isValid = false;
            }

            if (isValid == true) {
                //$("#preloaderCustom").show();
                that.notifyAuthentificate(userNameHandler.val(), passwordHandler.val());
            }else{
                $('#loadingEds').hide();
            }
        };

        this.addListener = function (list) {
            listeners.push(list);
        };

        this.notifyAuthentificate = function (userName, pwd) {
            $.each(listeners, function (i) {
                listeners[i].authentificate(userName, pwd);
            });
        };
    },
    ViewListener: function (list) {
        if (!list)
            list = {};
        return $.extend(list);
    }

});
