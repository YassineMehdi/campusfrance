jQuery
		.extend({
			VisitorView : function() {
				var that = this;
				var listeners = new Array();
				var candidatesCache = null;
				var currentCandidatesCache = null;
        		var modalContactHandler = null;
                var messageForm = null;
                var messageSkypeForm = null;
                var shareMessageForm = null;

				this.addListener = function(list) {
					listeners.push(list);
				};

				this.initView = function() {
            		modalExportCandidates = $("#popupExportCandidates");
            		messageForm =  $("#messageForm");
            		SkypeForm =  $("#messageSkypeForm");
            		shareMessageForm =  $("#shareProfileForm");
            		that.validateNewContactForm();
            		that.validateNewSkypeForm();
            		that.validateShareForm();
					$('#cvSearchButton').unbind("click").bind("click", function() {
						// visitorsController.getCandidatesByCVKeywords($('#cvSearchInput').val());
						that.notifyFilterSearchProfiles();
					});
					$('#exportCandidatesButton').unbind("click").bind(
							"click",
							function() {
								/*$('#loadingEds').show(
										function() {*/
											modalExportCandidates.modal();
											//$('#popupExportCandidates, .backgroundPopup').show();
											factoryExportVisitors(currentCandidatesCache);
											//$('#loadingEds').hide();
											$("#popupExportCandidates #candidateSortableColumns")
													.sortable().disableSelection();
										//});
							});
					$(
							'#popupExportCandidates .closePopup, #popupExportCandidates .eds-dialog-close')
							.click(function(e) {
								e.preventDefault();
								$('.eds-dialog-container').hide();
								$('.backgroundPopup').hide();
							});
					$(
							'#candidateProfileKeyWordInput,#formationSearchInput,#experienceSearchInput,#projectSearchInput')
							.unbind('keydown').bind('keydown', function(e) {
								if (e.keyCode == 13) {
									that.notifyFilterSearchProfiles();
								}
							});
					$("#candidateProfileAdvanceCriteria").on('show.bs.collapse',
							function() {
								that.reIntAdvanceSearch();
							});
					$("#candidateProfileAdvanceCriteria").on('hide.bs.collapse',
							function() {
								that.reIntAdvanceSearch();
							});
					// $(document).on(
					// 'keyup',
					// '#candidateProfileKeyWordInput,#formationSearchInput,#experienceSearchInput,#projectSearchInput',
					// that.notifyFilterSearchProfiles);
					// $(document).on('change','.candidateProfileCriteria select',
					// that.notifyFilterSearchProfiles);

					// todo
					//$('#loadingEds')
					//		.show(
					//				function() {
										$('#popupExportCandidates').hide();
										$(".draggable").draggable({
											cancel : ".eds-dialog-inside"
										});
										traduct();
										that.initCandidatesTable();
										var firstTabActive = false;
										rightGroup = listStandRights[rightGroupEnum.REGISTER_VISITORS];
										if (rightGroup != null && rightGroup.getRightCount() != 0) {
											$('#myTab2 a.searchProfiles').show();
											$('#myTab2 a.searchProfiles').click(function() {
												$('#loadingEds').show();
												that.showSearchProfiles();
											});
											that.showSearchProfiles();
											firstTabActive = true;
										} else {
											console.log('here else');
											$('#myTab2 a.searchProfiles').closest("li").hide();
										}
										rightGroup = listStandRights[rightGroupEnum.FORUM_VISITORS];
										if (rightGroup != null && rightGroup.getRightCount() != 0) {
											$('#myTab2 a.forumVisitors').click(function() {
												console.log('inside forumVisitors');
												$('#loadingEds').show();
												that.showForumEnrolledUsers();
											});
											if (!firstTabActive) {
												that.showForumEnrolledUsers();
												firstTabActive = true;
											}
										} else {
											$('#myTab2 a.forumVisitors').closest("li").hide();
										}
										rightGroup = listStandRights[rightGroupEnum.STAND_VISITORS];
										if (rightGroup != null && rightGroup.getRightCount() != 0) {
											$('#myTab2 a.standVisitors').click(function(e) {
												$("#participantsStat").hide();
												$('#loadingEds').show();
												that.showStandVisitors();
											});
											if (!firstTabActive) {
												that.showStandVisitors();
												firstTabActive = true;
											}
										} else {
											$('#myTab2 a.standVisitors').closest("li").hide();
										}
										rightGroup = listStandRights[rightGroupEnum.FAVORITS_VISITORS];
										if (rightGroup != null && rightGroup.getRightCount() != 0) {
											$('#myTab2 a.favoriteUsers').click(function() {
												$("#participantsStat").hide();
												$('#loadingEds').show();
												that.showFavoritsVisitors();
											});
											if (!firstTabActive) {
												that.showFavoritsVisitors();
												firstTabActive = true;
											}
										} else {
											$('#myTab2 a.favoriteUsers').closest("li").hide();
										}
										setTimeout(function() {
											that.fillActivitySectors();
											that.fillCountryResidences();
											that.fillCountryOrigins();
											that.fillFunctionsCandidate();
											that.fillExperienceYears();
											that.fillAreaExpertise();
											that.fillJobSought();
											that.fillCandidateStates();
										}, 2000);
									//});
				};

				this.validateNewContactForm = function () {
		            validatorContact = $("#messageForm").validate({
		                rules: {
		                    messageSubject: {
		                        required: true,
		                    },
		                    messageContent: {
		                        required: true,
		                    }
		                }
		            });
		        };
				this.validateNewSkypeForm = function () {
		            validatorContact = $("#messageSkypeForm").validate({
		                rules: {
		                    skypeMessageSubject: {
		                        required: true,
		                    },
		                    skypeMessageContent: {
		                        required: true,
		                    }
		                }
		            });
		        };
		        this.validateShareForm = function () {
		            validatorContact = $("#shareProfileForm").validate({
		                rules: {
		                    shareProfileTo: {
		                        required: true,
		                    },
		                    shareProfileSubject: {
		                        required: true,
		                    },
		                    ShareProfileContent: {
		                        required: true,
		                    }
		                }
		            });
		        };

				this.showSearchProfiles = function() {
					$("#participantsStat").show();
					$('#myTab2 a.searchProfiles').closest('ul').find('li').removeClass(
							'active');
					$('#myTab2 a.searchProfiles').closest('li').addClass('active');
					$('#loadingEds').show();
					that.notifyGetSearchProfiles();
				};
				this.showForumEnrolledUsers = function() {
					$("#participantsStat").hide();
					$('#myTab2 a.forumVisitors').closest('ul').find('li')
							.removeClass('active');
					$('#myTab2 a.forumVisitors').closest('li').addClass('active');
					$('#loadingEds').show();
					that.notifyGetForumEnrolled();
				};
				this.showStandVisitors = function() {
					$("#participantsStat").hide();
					$('#myTab2 a.standVisitors').closest('ul').find('li')
							.removeClass('active');
					$('#myTab2 a.standVisitors').closest('li').addClass('active');
					$('#loadingEds').show();
					that.notifyGetStandVisitors();
				};
				this.showFavoritsVisitors = function() {
					$("#participantsStat").hide();
					$('#myTab2 a.favoriteUsers').closest('ul').find('li')
							.removeClass('active');
					$('#myTab2 a.favoriteUsers').closest('li').addClass('active');
					$('#loadingEds').show();
					that.notifyGetFavoriteUsers();
				};
				this.beforeInitView = function() {
					$("#shareProfileForm").validate({
						rules : {
							shareProfileTo : {
								required : true,
								email : true
							}
						}
					});

					$('#sendMessage').unbind("click").bind(
							"click",
							function() {
								if ($('#messageForm').valid()) {
									var messageSubject = $('#messageForm #messageSubject').val()
											.trim();
									var messageContent = $('#messageForm #messageContent').val();
									var message = new $.Message();
									message.setSubject(messageSubject);
									message.setContent(messageContent);
									that.notifySendMessage(message, receiverId);
									$('.cancelSendMessage').click();
								}
							});

					$('#skypeSendMessage').unbind("click")
							.bind(
									"click",
									function() {
										if ($('#messageSkypeForm').valid()) {
											var messageSubject = $(
													'#skypeMessageSubject').val()
													.trim();
											var messageContent = $(
													'#skypeMessageContent').val();
											var message = new $.Message();
											message.setSubject(messageSubject);
											message.setContent(messageContent);
											that.notifySendSkypeMessage(message, receiverId);
											$('.cancelSkypeSendMessage').click();
										}
									});

					$('#sendShareProfile').unbind("click").bind(
							"click",
							function() {
								if ($('#shareProfileForm').valid()) {
									var email = new $.Email();
									var address = $('#shareProfileTo').val()
											.trim();
									email.setAddress(that.toStringListAddress(address));
									email.setSubject($('#shareProfileSubject')
											.val());
									email.setBody($('#ShareProfileContent')
											.val().split("\n").join("<br/>"));
									that.notifySendShareProfile(email);
									$('.cancelShareProfile').click();
								}
							});
					$('.popupMessage .cancelSendMessage, .cancelShareProfile').click(
							function() {
								$('.popupMessage, .backgroundPopup, .popupShare').hide();
							});
					$(".popupMessage").draggable({
						cancel : ".borderPopupMessageDiv"
					});
					/*customJqEasyCounter($('#messageForm #messageContent'), 65535, 65535,
							'#0796BE', LEFTCARACTERES);
					customJqEasyCounter($('#messageForm #messageSubject'), 255, 255,
							'#0796BE', LEFTCARACTERES);*/
					$('#messageForm div.jqEasyCounterMsg').css('width', '95%');
				};
				this.beforeInitView();
				this.initCandidatesTable = function() {
					$('#listCandidatesTable')
							.dataTable(
									{
										"language": {
						                    url: 'plugins/datatables/'+getIdLangParam()+'.json'
						                },
										"aaData" : [],
										"aoColumns" : [ {
											"sTitle" : STATELABEL,
											"sWidth" : "5%",
										}, {
											"sTitle" : LASTNAMELABEL,
											"sWidth" : "10%",
										}, {
											"sTitle" : FIRSTNAMELABEL,
											"sWidth" : "10%",
										}, {
											"sTitle" : ACTIVITYSECTORLABEL,
											"sWidth" : "20%",
										}, {
											"sTitle" : FUNCTIONLABEL,
											"sWidth" : "10%",
										}, {
											"sTitle" : EXPERIENCEYEARSLABEL,
											"sWidth" : "10%",
										}, {
											"sTitle" : ACTIONSLABEL,
											"sWidth" : "35%"
										}, {
											"sTitle" : "",
										} ],
										"aaSorting" : [ [ 7, "desc" ] ],
										"aoColumnDefs" : [ {
											"bVisible" : false,
											"aTargets" : [ 7 ]
										} ],
										"bAutoWidth" : false,
										"bRetrieve" : false,
										"bFilter" : false,
										"bDestroy" : true,
										"iDisplayLength" : 10,
										"fnDrawCallback" : function() {
											$('.addFavoriteButton, .deleteFavoriteButton, .sendMessageButton, .inviteSkypeButton, .approveButton, .disapproveButton, .profileButton, .sendByEmailButton').off('click');
											$('.addFavoriteButton').on(
													'click',
													function() {
														var index = $(this).attr('id');
														var candidate = currentCandidatesCache[index];
														var favoriteXml = factoryXMLFavory(candidate
																.getUserProfile().getUserProfileId(),
																FavoriteEnum.USER);
														addFavorite(favoriteXml);
														$(this).hide().closest('td').find(
																'.deleteFavoriteButton').show();
													});
											$('.deleteFavoriteButton').on(
													'click',
													function() {
														var index = $(this).attr('id');
														var candidate = currentCandidatesCache[index];
														var favoriteXml = factoryXMLFavory(candidate
																.getUserProfile().getUserProfileId(),
																FavoriteEnum.USER);
														deleteFavorite(favoriteXml);
														$(this).hide().closest('td').find(
																'.addFavoriteButton').show();
													});
											$('.sendMessageButton')
													.on(
															'click',
															function() {
																var index = $(this).attr('id');
																var candidate = currentCandidatesCache[index];
																receiverId = candidate.getCandidateId();
																$(
																		'#messageForm #messageContent, #messageForm #messageSubject')
																		.val("");
																$('#popupMessage').modal();
															});
											$('.inviteSkypeButton')
													.on(
															'click',
															function() {
																var index = $(this).attr('id');
																var candidate = currentCandidatesCache[index];
																receiverId = candidate.getCandidateId();
																$('#skypeMessageTo').val(candidate.getFullName());
																$('#skypeMessageSubject, #skypeMessageContent') .val("");
																$('#popupSendSkypeInvitation').modal();
															});
											$('.approveButton').on(
													'click',
													function() {
														var index = $(this).attr('id');
														var candidate = currentCandidatesCache[index];
														that.notifyUpdateCandidateState(candidate
																.getCandidateId(), CandidateStateEnum.APPROVED,
																index);
													});
											$('.disapproveButton').on(
													'click',
													function() {
														var index = $(this).attr('id');
														var candidate = currentCandidatesCache[index];
														that.notifyUpdateCandidateState(candidate
																.getCandidateId(),
																CandidateStateEnum.DISAPPROVED, index);
													});
											$('.profileButton').on('click', function() {
												var index = $(this).attr('id');
												var candidate = currentCandidatesCache[index];
												viewUserProfile(candidate.getCandidateId());
												that.openProfile(candidate);
											});
											$('.sendByEmailButton')
													.on(
															'click',
															function() {
																var index = $(this).attr('id');
																var candidate = currentCandidatesCache[index];
																$('#ShareProfileContent').val(
																		staticVars.urlvisitor
																				+ "profileCandidate.html?candidateId="
																				+ candidate.getCandidateId()
																				+ "&idLang=" + getParam('idLang'));
																$('#shareProfileTo, #shareProfileForm').val("");
																$('#popupShare').modal();
															});
										}
									});
				};
				this.reIntAdvanceSearch = function() {
					$(".candidateProfileCriteria #candidateStateSelect").val(0);
					$(".candidateProfileCriteria #sectorSelect").val(0);
					// $(".candidateProfileCriteria #functionSelect").val(0);
					$(".candidateProfileCriteria #countryResidenceSelect").val(0);
					$(".candidateProfileCriteria #countryOriginSelect").val(0);
					$(".candidateProfileCriteria #experienceYearsSelect").val(0);
					$(".candidateProfileCriteria #areaExpertiseSelect").val(0);
					$(".candidateProfileCriteria #jobSoughtSelect").val(0);
				};
				this.toStringListAddress = function(listAddress) {
					if (listAddress != null && listAddress != "") {
						listAddress = listAddress.split(";").join(",");
						var addressArray = listAddress.split(",");
						listAddress = addressArray[0];
						var address = null;
						for ( var i = 1; i < addressArray.length; i++) {
							address = addressArray[i];
							if (address != "" && regexEmail.test(address))
								listAddress += ", " + addressArray[i].trim();
						}
						// console.log(listAddress);
					}
					return listAddress;
				};
				this.fillActivitySectors = function() {
					var activitySectorSelectHandler = $('.candidateProfileCriteria #sectorSelect');
					activitySectorSelectHandler.empty();
					activitySectorSelectHandler.append($('<option/>').val(0).html(
							ALLLABEL));
					for (index in listActivitySectorsCache) {
						var sector = listActivitySectorsCache[index];
						activitySectorSelectHandler.append($('<option/>').val(
								sector.idsecteur).html(sector.name));
					}
				};
				this.fillCountryResidences = function() {
					var countryResidenceSelectHandler = $('.candidateProfileCriteria #countryResidenceSelect');
					countryResidenceSelectHandler.empty();
					countryResidenceSelectHandler.append($('<option/>').val(0).html(
							ALLLABEL));
					for (index in listRegionsCache) {
						var region = listRegionsCache[index];
						countryResidenceSelectHandler.append($('<option/>').val(
								region.idRegion).html(region.regionName));
					}
				};
				this.fillCountryOrigins = function() {
					var countryOriginSelectHandler = $('.candidateProfileCriteria #countryOriginSelect');
					countryOriginSelectHandler.empty();
					countryOriginSelectHandler.append($('<option/>').val(0)
							.html(ALLLABEL));
					for (index in listRegionsCache) {
						var region = listRegionsCache[index];
						countryOriginSelectHandler.append($('<option/>').val(
								region.idRegion).html(region.regionName));
					}
				};
				this.fillFunctionsCandidate = function() {
					var functionSelectSelectHandler = $('.candidateProfileCriteria #functionSelect');
					functionSelectSelectHandler.empty();
					functionSelectSelectHandler.append($('<option/>').val(0).html(
							ALLLABEL));
					for (index in listFunctionsCandidateCache) {
						var functionCandidate = listFunctionsCandidateCache[index];
						functionSelectSelectHandler.append($('<option/>').val(
								functionCandidate.idFunction).html(
								functionCandidate.functionName));
					}
				};
				this.fillExperienceYears = function() {
					var experienceYearsSelectHandler = $('.candidateProfileCriteria #experienceYearsSelect');
					experienceYearsSelectHandler.empty();
					experienceYearsSelectHandler.append($('<option/>').val(0).html(
							ALLLABEL));
					for (index in listExperienceYearsCache) {
						var experienceYears = listExperienceYearsCache[index];
						experienceYearsSelectHandler.append($('<option/>').val(
								experienceYears.getIdExperienceYears()).html(
								experienceYears.getExperienceYearsName()));
					}
				};
				this.fillAreaExpertise = function() {
					var areaExpertiseSelectHandler = $('.candidateProfileCriteria #areaExpertiseSelect');
					areaExpertiseSelectHandler.empty();
					areaExpertiseSelectHandler.append($('<option/>').val(0)
							.html(ALLLABEL));
					for (index in listAreaExpertisesCache) {
						var areaExpertise = listAreaExpertisesCache[index];
						areaExpertiseSelectHandler.append($('<option/>').val(
								areaExpertise.getAreaExpertiseId()).html(
								areaExpertise.getAreaExpertiseName()));
					}
				};
				this.fillJobSought = function() {
					var jobSoughtSelectHandler = $('.candidateProfileCriteria #jobSoughtSelect');
					jobSoughtSelectHandler.empty();
					jobSoughtSelectHandler.append($('<option/>').val(0).html(ALLLABEL));
					for (index in listJobsSoughtCache) {
						var jobSought = listJobsSoughtCache[index];
						var disabled = "";
						if (jobSought.enable == "false") {
							disabled = " disabled=disabled";
						}
						jobSoughtSelectHandler.append($('<option' + disabled + '/>').val(
								jobSought.getJobSoughtId()).html(jobSought.getJobSoughtName()));
					}
				};
				this.fillCandidateStates = function() {
					var candidateStateSelectHandler = $('.candidateProfileCriteria #candidateStateSelect');
					candidateStateSelectHandler.empty();
					candidateStateSelectHandler.append($('<option/>').val(0).html(
							ALLLABEL));
					if (listCandidateStatesCache != null)
						listCandidateStatesCache
								.sort(function(a, b) {
									if (a.getCandidateStateName() > b.getCandidateStateName()) {
										return 1;
									} else if (a.getCandidateStateName() < b
											.getCandidateStateName()) {
										return -1;
									} else {
										return 0;
									}
								});
					for (index in listCandidateStatesCache) {
						var candidateState = listCandidateStatesCache[index];
						if (candidateState.getCandidateStateId() != CandidateStateEnum.DISAPPROVED)
							candidateStateSelectHandler.append($('<option/>').val(
									candidateState.getCandidateStateId()).html(
									candidateState.getCandidateStateName()));
					}
				};
				this.addTableDatas = function(selector, datas) {
					var oTable = $(selector).dataTable();
					oTable.fnClearTable();
					if (datas.length > 0) {
						oTable.fnAddData(datas);
					}
					oTable.fnDraw();
				};
				this.getHtml = function(element) {
					if (element != null)
						return $("<div></div>").html(element).html();
				};
				this.displayCandidates = function(candidates, defaultSorting) {
					currentCandidatesCache = candidates;
					var datas = new Array();
					var nbrRegisteredParticipants = 0;
					var nbrApprovedParticipants = 0;
					var nbrParticipantsDisapproved = 0;
					var userFavorites = [];
					for ( var i in listUserFavorites) {
						var userFavorite = listUserFavorites[i];
						userFavorites[userFavorite.getComponentId()] = true;
					}
					var btnFavHandler =  		$("<a data-toggle='tooltip' title='"+ADDTOFAVORITELABEL+"' class='addFavoriteButton actionBtn'><i class='fa fa-star-o yellowText'></i></a>");
					var btnDelFavHandler = 		$("<a data-toggle='tooltip' title='"+DELETEFROMFAVORITELABEL+"' class='deleteFavoriteButton actionBtn'><i class='fa fa-star yellowText'></i></a>");
					var btnProfileHandler = 	$("<a data-toggle='tooltip' title='"+SEEPROFILELABEL+"' class='profileButton actionBtn'><i class='fa fa-eye blueText'></i></a>");
					var btnSendMsgHandler = 	$("<a data-toggle='tooltip' title='"+SENDMESSAGELABEL+"' class='sendMessageButton actionBtn'><i class='fa fa-envelope-o orangeText'></i></a>");
					var btnApproveHandler = 	$("<a data-toggle='tooltip' title='"+APPROVELABEL+"'  class='approveButton actionBtn'><i class='fa fa-check greenText'></i></a>");
					var btnDisapproveHandler = 	$("<a data-toggle='tooltip' title='"+DISAPPROVELABEL+"'  class='disapproveButton actionBtn'><i class='fa fa-times redText'></i></a>");
					var btnShareProfileHandler =$("<a data-toggle='tooltip' title='"+SHAREFRIENDLABEL+"'  class='sendByEmailButton actionBtn'><i class='fa fa-share-alt greyText'></i></a>");
					var btnInviteSkypeHandler = $("<a data-toggle='tooltip' title='"+SKYPEINVITATIONLABEL+"'  class='inviteSkypeButton actionBtn'><i class='fa fa-skype blue2Text'></i></a>");
					var listActions = $("<div></div>");
					var newBadgeHandler = $('<div class="new-badge">'
							+ htmlEncode(NEWLABEL) + '</div>');


					/*
					 * updatedHtml = '<div class="new-badge">' + htmlEncode(NEWLABEL) + '</div><i
					 * class="material-icons" title="' + htmlEncode(UPDATELABEL) + '"><i
					 * class="material-icons">person_add</i></i><div
					 * class="caption-updated">' + htmlEncode(NEWLABEL) + '</div>';
					 */
					var updateBadgeHandler = $('<div class="updated-badge">'
							+ htmlEncode(UPDATEDLABEL) + '</div>');
					/*
					 * updatedHtml = '<div class="update-badge">' +
					 * htmlEncode(UPDATELABEL) + '</div><i class="material-icons"
					 * title="' + htmlEncode(UPDATELABEL) + '">update</i><div
					 * class="caption-updated">' + htmlEncode(UPDATELABEL) + '</div>';
					 */
					var notUpdateBadgeHandler = $('<div class="not-updated-badge">'
							+ htmlEncode(NOTUPDATEDLABEL) + '</div>');
					//btnFavHandler.text(ADDTOFAVORITELABEL);
					//btnDelFavHandler.text(DELETEFROMFAVORITELABEL);
					//btnProfileHandler.text(VIEWPROFILELABEL);
					//btnSendMsgHandler.text(SENDMESSAGELABEL);
					//btnShareProfileHandler.text(SHARE_WITH_FRIEND_LABEL);
					//btnInviteSkypeHandler.text(SKYPEINVITATIONLABEL);
					//btnApproveHandler.text(APPROVELABEL);
					//btnDisapproveHandler.text(DISAPPROVELABEL);
					var candidatesNbr = Object.keys(candidates).length;
					for (index in candidates) {
						var candidate = candidates[index];
						var candidateState = candidate.getCandidateState();
						if (candidateState.getCandidateStateId() != CandidateStateEnum.DISAPPROVED) {
							var userProfile = candidate.getUserProfile();
							var candidateId = candidate.getCandidateId();
							var userProfileId = userProfile.getUserProfileId();
							var firstName = userProfile.getFirstName();
							var secondName = userProfile.getSecondName();
							var experienceYearsName = candidate.getExperienceYears()
									.getExperienceYearsName();
							var secteuract = candidate.getNameSectors();
							var candidateFunctionName = candidate.getFunctionCandidate()
									.getFunctionName();
							var updatedHtml = "";
							var btnActive = "";
							var btnProfile = "";
							var sortIndex = null;
							if(defaultSorting == true) sortIndex = candidatesNbr - index;
							else sortIndex = candidateId;
							btnInviteSkypeHandler.attr("id", index);
							btnShareProfileHandler.attr("id", index);
							btnSendMsgHandler.attr("id", index);
							if (userFavorites[userProfileId] == true) {
								btnFavHandler.attr("id", index).hide();
								btnDelFavHandler.attr("id", index).show();
							} else {
								btnFavHandler.attr("id", index).show();
								btnDelFavHandler.attr("id", index).hide();
							}
							if (that.checkProfile(candidate)) {
								btnProfileHandler.attr("id", index);
								btnProfile = that.getHtml(btnProfileHandler);
							}
							if (candidateState.getCandidateStateId() != CandidateStateEnum.APPROVED) {
								btnApproveHandler.attr("id", index).show();
								btnDisapproveHandler.attr("id", index).show();
								btnActive = that.getHtml(btnApproveHandler)
										+ that.getHtml(btnDisapproveHandler);
							} else
								nbrApprovedParticipants++;
							listActions.attr("id", userProfileId);
							listActions.html(btnProfile + that.getHtml(btnFavHandler)
									+ that.getHtml(btnDelFavHandler)
									+ that.getHtml(btnSendMsgHandler)
									+ that.getHtml(btnInviteSkypeHandler) + btnActive
									+ that.getHtml(btnShareProfileHandler));
							if (candidateState.getCandidateStateId() == CandidateStateEnum.REGISTER)
								updatedHtml = that.getHtml(newBadgeHandler);
							else if (candidateState.getCandidateStateId() == CandidateStateEnum.UPDATED)
								updatedHtml = that.getHtml(updateBadgeHandler);
							else
								updatedHtml = that.getHtml(notUpdateBadgeHandler);
							datas.push([ updatedHtml, firstName, secondName, secteuract,
									candidateFunctionName, experienceYearsName,
									that.getHtml(listActions), sortIndex ]);
						} else
							nbrParticipantsDisapproved++;
						nbrRegisteredParticipants++;
					}
					$(".nbrRegisteredParticipants").text(nbrRegisteredParticipants);
					$(".nbrApprovedParticipants").text(nbrApprovedParticipants);
					$(".nbrParticipantsDisapproved").text(nbrParticipantsDisapproved);
					that.addTableDatas('#listCandidatesTable', datas);
					$('#loadingEds').hide();
				};
				this.displaySearchProfiles = function(registerCandidates) {
					candidatesCache = registerCandidates;
					that.displayCandidates(candidatesCache);
					$('[data-toggle="tooltip"]').tooltip();
				};
				this.displayForumEnrolled = function(forumVisitors) {
					candidatesCache = forumVisitors;
					that.displayCandidates(forumVisitors);
					$('[data-toggle="tooltip"]').tooltip();
				};
				this.displayLoadStandVisitors = function(standVisitors) {
					candidatesCache = standVisitors;
					that.displayCandidates(candidatesCache, true);
					$('[data-toggle="tooltip"]').tooltip();
				};
				this.displayFavoriteUsers = function(favoriteUsers) {
					candidatesCache = favoriteUsers;
					that.displayCandidates(candidatesCache);
					$('[data-toggle="tooltip"]').tooltip();
				};
				this.getCandidatesByCVKeywords = function(cvKeywordValue) {
					// console.log('cvKeywordValue: '+cvKeywordValue);
				};
				this.checkProfile = function(candidate) {
					if (candidate != null) {
						var pdfCvs = candidate.getPdfCvs();
						if (pdfCvs != null) {
							if (pdfCvs.length > 0) {
								return true;
							}
						}
					}
					return false;
				};
				this.openProfile = function(candidate) {
					if (candidate != null) {
						var pdfCvs = candidate.getPdfCvs();
						if (pdfCvs != null) {
							if (pdfCvs.length > 0) {
								var pdfCv = null;
								for ( var i = 0; i < pdfCvs.length; i++) {
									pdfCv = pdfCvs[i];
									window.open(pdfCv.getUrlCv(), '_blank');
								}
							}
						}
					}
				};
				this.skypeMailTemplateSuccess = function(xml) {
					$('.skypeMessageForm .skypeMessageContent').val("");
				};
				this.updateCandidateStateSuccess = function(index, candidateStateId) {
					if (candidateStateId == CandidateStateEnum.APPROVED) {
						$("#" + index + ".approveButton, #" + index + ".disapproveButton")
								.hide();
						$(".nbrApprovedParticipants").text(
								parseInt($(".nbrApprovedParticipants").text()) + 1);
					} else if (candidateStateId == CandidateStateEnum.DISAPPROVED) {
						$(".nbrParticipantsDisapproved").text(
								parseInt($(".nbrParticipantsDisapproved").text()) + 1);
						$("#" + index + ".approveButton, #" + index + ".approveButton")
								.hide();
						$("#" + index + ".approveButton, #" + index + ".approveButton")
								.closest("tr").remove();
					}
				};
				this.notifySendMessage = function(message, candidateId) {
					$.each(listeners, function(i) {
						listeners[i].sendMessage(message, candidateId);
					});
				};
				this.notifySendSkypeMessage = function(message, candidateId) {
					$.each(listeners, function(i) {
						listeners[i].sendSkypeMessage(message, candidateId);
					});
				};
				this.notifySendShareProfile = function(email) {
					$.each(listeners, function(i) {
						listeners[i].sendShareProfile(email);
					});
				};
				this.notifyUpdateCandidateState = function(candidateId,
						candidateStateId, index) {
					$.each(listeners, function(i) {
						listeners[i].updateCandidateState(candidateId, candidateStateId,
								index);
					});
				};
				this.notifyFilterSearchProfiles = function() {
					/*$('#loadingEds')
							.show(
									function() {*/
										var keyWord = $(
												".candidateProfileCriteria #candidateProfileKeyWordInput")
												.val();
										var formation = $(
												".candidateProfileCriteria #formationSearchInput")
												.val();
										var experience = $(
												".candidateProfileCriteria #experienceSearchInput")
												.val();
										var project = $(
												".candidateProfileCriteria #projectSearchInput").val();
										var candidateStateId = $(
												".candidateProfileCriteria #candidateStateSelect option:selected")
												.val();
										var activitySectorId = $(
												".candidateProfileCriteria #sectorSelect option:selected")
												.val();
										var functionCandidateId = $(
												".candidateProfileCriteria #functionSelect option:selected")
												.val();
										var countryResidenceId = $(
												".candidateProfileCriteria #countryResidenceSelect option:selected")
												.val();
										var countryOriginId = $(
												".candidateProfileCriteria #countryOriginSelect option:selected")
												.val();
										var experienceYearsId = $(
												".candidateProfileCriteria #experienceYearsSelect option:selected")
												.val();
										var areaExpertiseId = $(
												".candidateProfileCriteria #areaExpertiseSelect option:selected")
												.val();
										var jobSoughtId = $(
												".candidateProfileCriteria #jobSoughtSelect option:selected")
												.val();
										// console.log(keyWord+" : "+formation+" : "+experience+" :
										// "+project + " : " +
										// candidateStateId + " : "
										// + activitySectorId+" : " +functionCandidateId+" :
										// "+countryResidenceId+" :
										// "+countryOriginId
										// + " : " + experienceYearsId + " : " + jobSoughtId + " : "
										// + jobSoughtId);
										var listFilterCandidates = new Array();
										var candidate = null;
										for ( var index in candidatesCache) {
											candidate = candidatesCache[index];
											if (candidate.searchWithKeyWord(keyWord)
													&& candidate.searchInCandidateState(candidateStateId)
													&& candidate
															.searchInActivitySectors(activitySectorId)
													&& candidate
															.searchInFunctionCandidate(functionCandidateId)
													&& candidate
															.searchInCountryResidence(countryResidenceId)
													&& candidate.searchInCountryOrigin(countryOriginId)
													&& candidate
															.searchInExperienceYears(experienceYearsId)
													&& candidate.searchInAreaExpertise(areaExpertiseId)
													&& candidate.searchInJobSought(jobSoughtId)
													&& candidate.searchInExperience(experience)
													&& candidate.searchInProject(project)
													&& candidate.searchInFormation(formation))
												listFilterCandidates.push(candidate);
										}
										that.displayCandidates(listFilterCandidates);
									//});
				};
				this.notifyGetFavoriteUsers = function() {
					$.each(listeners, function(i) {
						listeners[i].getFavoriteUsers();
					});
				};
				this.notifyGetForumEnrolled = function() {
					$.each(listeners, function(i) {
						listeners[i].getForumEnrolled();
					});
				};
				this.notifyGetStandVisitors = function() {
					$.each(listeners, function(i) {
						listeners[i].getStandVisitors();
					});
				};
				this.notifyGetSearchProfiles = function() {
					$.each(listeners, function(i) {
						listeners[i].getSearchProfiles();
					});
				};
			},

			VisitorViewListener : function(list) {
				if (!list)
					list = {};
				return $.extend(list);
			},

		});

