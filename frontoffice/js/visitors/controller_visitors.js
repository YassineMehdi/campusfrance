jQuery.extend({

	VisitorController : function(model, view) {
		var that = this;
		this.listCandidatesCache = null;
		// listen to the model
		var indexModelListener = new $.VisitorModelListener({
			displayGetCandidatesByCVKeywords : function(candidatesSearched) {
				var CandidatesFound = new Array();
				if(candidatesSearched.length!=0)
					$.each(candidates, function( i, candidate) {
						$.each(candidatesSearched, function( j, candidateSearched) {
							if(candidate.getCandidateId()==candidateSearched.candidateId){
								candidate.cvKeywords = candidateSearched.cvKeywords;
								CandidatesFound.push(candidate);
							}
						});
					});
				view.displaySearchProfiles(CandidatesFound);
			},
			displayForumEnrolled : function(candidates){
				that.listCandidatesCache = candidates;
				view.displayForumEnrolled(candidates);
			},
			displayLoadStandVisitors : function(visitors, candidates){
				that.listCandidatesCache = candidates;
				for (var candidateId in visitors) {
					visitors[candidateId] = that.listCandidatesCache[candidateId];
				}
				view.displayLoadStandVisitors(visitors);
			},
			displayFavoriteUsers : function(candidates){
				that.listCandidatesCache = candidates;
				var favoriteUsers = new Array();
				var candidateVisitors = new Array();
				for (var i in that.listCandidatesCache) {
					var candidate = that.listCandidatesCache[i];
					candidateVisitors[candidate.getUserProfile().getUserProfileId()] = candidate;
				}
				$.each(listUserFavorites, function( j, favorite) {
					var candidate = candidateVisitors[favorite.getComponentId()];
					if(candidate != null) 
						favoriteUsers.push(candidate);
				});
				view.displayFavoriteUsers(favoriteUsers);
			},
			displaySearchProfiles : function(candidates){
				that.listCandidatesCache = candidates;
				view.displaySearchProfiles(candidates);
			},
			skypeMailTemplateSuccess : function(xml){
				view.skypeMailTemplateSuccess(xml);
			},
			updateCandidateStateSuccess : function(index, candidateStateId){
				view.updateCandidateStateSuccess(index, candidateStateId);
			},
		});
		model.addListener(indexModelListener);
		
		// listen to the view
		var indexViewList = new $.VisitorViewListener({
			sendMessage : function(message, candidateId) {
        		model.sendMessage(message, candidateId);
			},
			sendSkypeMessage : function(message, candidateId){
				model.sendSkypeMessage(message, candidateId);
			},
			sendShareProfile : function(email){
				model.sendShareProfile(email);
			},
			getFavoriteUsers : function(){
				if(that.listCandidatesCache == null) {
					model.getFavoriteUsers();
				}else {
					indexModelListener.displayFavoriteUsers(that.listCandidatesCache);
				}
			},
			getForumEnrolled : function(){
				if(that.listCandidatesCache == null) {
					console.log('inside if');
					model.getForumEnrolled();
				}else {
					console.log(that.listCandidatesCache);
					view.displayForumEnrolled(that.listCandidatesCache);
				}
			},
			getStandVisitors : function() {
				if(that.listCandidatesCache == null) 
					model.getStandVisitors();
				else {
					model.getConnectedUsers(that.listCandidatesCache);
				}
			},
			getSearchProfiles : function() {
				if(that.listCandidatesCache == null) 
					model.getSearchProfiles();
				else {
					view.displaySearchProfiles(that.listCandidatesCache);
				}
			},
			updateCandidateState : function(candidateId, candidateStateId, index){
				model.updateCandidateState(candidateId, candidateStateId, index);
				var candidate = that.listCandidatesCache[candidateId];
				if(candidate != null){
					var candidateState = candidate.getCandidateState();
					candidateState.setCandidateStateId(candidateStateId);
				}
			},
			filterSearchProfiles : function(candidatesCache, keyWord, formation, experience, project, candidateStateId, activitySectorId, 
					functionCandidateId, countryResidenceId, countryOriginId, experienceYearsId, areaExpertiseId, jobSoughtId){
//				console.log(keyWord+" : "+formation+" : "+experience+" : "+project + " : " + candidateStateId + " : " 
//						+ activitySectorId+" : " +functionCandidateId+" : "+countryResidenceId+" : "+countryOriginId
//						+ " : " + experienceYearsId + " : " + jobSoughtId + " : " + jobSoughtId);
				var listFilterCandidates = new Array();
				var candidate = null;
				for (var index in candidatesCache) {
					candidate = candidatesCache[index];
					if(candidate.searchWithKeyWord(keyWord) && candidate.searchInCandidateState(candidateStateId) && candidate.searchInActivitySectors(activitySectorId)
							&& candidate.searchInFunctionCandidate(functionCandidateId) && candidate.searchInCountryResidence(countryResidenceId) 
							&& candidate.searchInCountryOrigin(countryOriginId) && candidate.searchInExperienceYears(experienceYearsId) 
							&& candidate.searchInAreaExpertise(areaExpertiseId) && candidate.searchInJobSought(jobSoughtId) 
							&& candidate.searchInExperience(experience) && candidate.searchInProject(project) 
							&& candidate.searchInFormation(formation)) listFilterCandidates.push(candidate);
				}
				view.displaySearchProfiles(listFilterCandidates);
			}
		});
		view.addListener(indexViewList);
		this.initController = function(){
			view.initView();
			model.initModel();
		};
		this.getCandidatesByCVKeywords = function(cvKeywordValue) {
			model.getCandidatesByCVKeywords(cvKeywordValue);
		};
		setInterval(function(){
			that.listCandidatesCache = null;
		}, UPDATE_CANDIDATES_CACHE_TIME_STAMP);
	},
});

